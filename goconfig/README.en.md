# goconfig
$ go install golang.org/x/tools/cmd/godoc@latest

* 功能简介

** 支持环境变量

** 支持不同配置文件

** 支持加密

** 常用固定配置项

** 扩展配置项解析 ReadIchubStruct


* 配置文件

** 配置文件根目录：自动匹配 /config找到根目录
   配置环境变量：BasePath

** 环境根文件： /config/app-env.yml

** 环境文件：  /config/app-dev.yml

* go环境变量

go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct
go env -w GOPRIVATE=git.ichub.com,gitee.com
go env -w GOINSECURE=git.ichub.com

* 使用
   go get -u gitee.com/leijmdas/gobase/goconfig

  * 加密工具
  ./cmd/enc.exe 
  加密 enc.exe enc xxxx
  解密 enc.exe dec xxxx

代码中可以注册其它加密工具，只要实现相同接口：
NewConfig().RegisterEncDec(encrypt.EncDecInst)

#### Description
{**When you're done, you can delete the content in this README and update the file with details for others getting started with your repository**}

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
