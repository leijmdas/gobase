package consts

const (
	CMD_COMMIT = iota + 1
	CMD_PB
	CMD_GOGET
	CMD_GOINSTALL
	CMD_GOBUILD
	WebCode   = "/code"
	WebDocker = "/docker"
	WebDocx   = "/docx"
	WebGorpc  = "/gorpc"
	WebNats   = "/gonatssample"

	WebDomain = "/domain"
	Webcmd    = "/cmd/goweb"
	Webcmdbat = "/cmd/bat"
	Websample = "/websample"
	Webconfig = "/config"

	WebTest = "/test"

	WebPbin         = "/bin"
	WebCmd          = "/cmd"
	WebcmdWebcli120 = "/cmd/webcli120"
	Webcmdbatgit    = "/cmd/bat_git"
)
