package dto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cmc_result_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-07-07 10:47:34)
	@Update 作者: leijmdas@163.com 时间(2024-07-07 10:47:34)

* *********************************************************/

const singleNameCmcResult = "dto.CmcResult"

// init register load
func init() {
	registerBeanCmcResult()
}

// register CmcResult
func registerBeanCmcResult() {
	basedi.RegisterLoadBean(singleNameCmcResult, LoadCmcResult)
}

func FindBeanCmcResult() *CmcResult {
	bean, ok := basedi.FindBean(singleNameCmcResult).(*CmcResult)
	if !ok {
		logrus.Errorf("FindBeanCmcResult: failed to cast bean to *CmcResult")
		return nil
	}
	return bean

}

func LoadCmcResult() baseiface.ISingleton {
	var s = NewCmcResult()
	InjectCmcResult(s)
	return s

}

func InjectCmcResult(s *CmcResult) {

	// // logrus.Debug("inject")
}
