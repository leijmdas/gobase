package dto

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type CmcResult struct {
	basedto.BaseEntity
}

func NewCmcResult() *CmcResult {
	return &CmcResult{}
}
