package batcmd

import (
	"context"
	"errors"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"github.com/sirupsen/logrus"
	"os"
	"os/exec"
)

type CmdService struct {
	basedto.BaseEntitySingle
}

func NewCmdService() *CmdService {
	var c = &CmdService{}
	c.RootDir = fileutils.FindRootDir()
	var basepath = gocontext.FindBeanGoRulefileCtx().BasePath
	var _ = basepath + "/bin/pb.bat"
	return c
}

func (c *CmdService) DoCmd(cmd string) ([]byte, error) {
	var rootDir = c.RootDir
	if rootDir == "" {
		return nil, errors.New("rootDir dir is empty!")
	}
	var bat = rootDir + "/cmd/bat/" + cmd
	if fileutils.CheckFileExist(bat) {
		os.Chdir(bat)

	}
	var cmds = exec.CommandContext(context.Background(), bat)
	logrus.Info(jsonutils.ToJsonPretty(cmds))
	out, _ := cmds.Output()

	return out, cmds.Err
}
