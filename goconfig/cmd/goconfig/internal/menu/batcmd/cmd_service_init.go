package batcmd

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cmd_service_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-07-07 10:47:34)
	@Update 作者: leijmdas@163.com 时间(2024-07-07 10:47:34)

* *********************************************************/

const singleNameCmdService = "service.CmdService"

// init register load
func init() {
	registerBeanCmdService()
}

// register CmdService
func registerBeanCmdService() {
	basedi.RegisterLoadBean(singleNameCmdService, LoadCmdService)
}

func FindBeanCmdService() *CmdService {
	bean, ok := basedi.FindBean(singleNameCmdService).(*CmdService)
	if !ok {
		logrus.Errorf("FindBeanCmdService: failed to cast bean to *CmdService")
		return nil
	}
	return bean

}

func LoadCmdService() baseiface.ISingleton {
	var s = NewCmdService()
	InjectCmdService(s)
	return s

}

func InjectCmdService(s *CmdService) {

	// // logrus.Debug("inject")
}
