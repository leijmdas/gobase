package batcmd

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestCommandServiceSuite struct {
	suite.Suite
	inst *CmdService

	rootdir string
}

func (this *TestCommandServiceSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanCommandService()

	this.rootdir = fileutils.FindRootDir()

}
func (this *TestCommandServiceSuite) TearDownTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanCommandService()

	this.rootdir = fileutils.FindRootDir()

}

func TestCommandServiceSuites(t *testing.T) {
	suite.Run(t, new(TestCommandServiceSuite))
}

func (this *TestCommandServiceSuite) Test001_DoCmd() {
	logrus.Info(1)
	var o, err = FindBeanCmdService().DoCmd("commit.bat")
	logrus.Info(string(o), err)

}
