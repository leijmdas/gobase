package batcmd

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

//	var PbCmd = &cobra.Command{
//		Use:   "pb",
//		Short: "pb",
//		Run:   pb,
//	}
var CommitCmd = &cobra.Command{
	Use:   "commit",
	Short: "commit",
	Run:   commit,
}

func pb(cmd *cobra.Command, args []string) {
	fmt.Println("exec pb.bat")
	var o, err = FindBeanCmdService().DoCmd("commit.bat")
	logrus.Info(string(o), err)
}
func commit(cmd *cobra.Command, args []string) {
	fmt.Println("git commit")
	var o, err = FindBeanCmdService().DoCmd("commit.bat")
	logrus.Info(string(o), err)

}
