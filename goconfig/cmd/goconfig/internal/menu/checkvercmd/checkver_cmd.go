package checkvercmd

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/checkver"
	"github.com/spf13/cobra"
)

var CheckVerDockerCmd = &cobra.Command{Use: "docker", Short: "check docker dir's files for git merge conflict!", Run: checkVerDocker}
var CheckVerAllCmd = &cobra.Command{Use: "all", Short: "check all files for git merge conflict!", Run: checkVerAll}
var CheckVerAllGoCmd = &cobra.Command{Use: "go", Short: "check all go files for git merge conflict!", Run: checkVerAllGo}

func checkVerDocker(cmd *cobra.Command, args []string) {
	fmt.Println("start checkVer docker")
	checkver.FindBeanCheckMergeInf().CheckDockerFiles()
}

func checkVerAll(cmd *cobra.Command, args []string) {
	fmt.Println("start checkVer all files ")
	checkver.FindBeanCheckMergeInf().CheckAllFiles()
}
func checkVerAllGo(cmd *cobra.Command, args []string) {
	fmt.Println("start checkVer all go files")
	checkver.FindBeanCheckMergeInf().CheckAllGoFiles()
}
