package cmdenc

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/encrypt"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/spf13/cobra"
)

func EncCmds() []*cobra.Command {
	ichublog.InitLogrus()
	var decCmd = &cobra.Command{
		Use:   "dec [flag]",
		Short: "dec password",
		Run: func(cmd *cobra.Command, args []string) {
			flag := cmd.Flags().Arg(0)
			gocontext.FindBeanGoClientFactroy().RegisterEncDec(encrypt.FindBeanEncDec())

			pwd := encrypt.FindBeanEncDec().AesDecBase64(flag)
			fmt.Println(pwd)
		},
	}

	var encCmd = &cobra.Command{
		Use:   "enc [flag]",
		Short: "enc password",
		Run: func(cmd *cobra.Command, args []string) {
			flag := cmd.Flags().Arg(0)
			pwd := encrypt.FindBeanEncDec().AesEncBase64(flag)
			fmt.Println(pwd)
		},
	}
	return []*cobra.Command{decCmd, encCmd}

}
