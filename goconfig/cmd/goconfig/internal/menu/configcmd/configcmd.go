package configcmd

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"

	"github.com/spf13/cobra"
)

var ConfigListCmd = &cobra.Command{Use: "cfg", Short: "cfg software swag escli webcli webserver web rpc es etcd db factroy redis nats", Run: ListCfg}

func ListCfg(cmd *cobra.Command, args []string) {
	fmt.Println("cfg list for software swag escli webcli webserver web rpc es etcd db factroy redis nats")
	var cfg = ichubconfig.FindBeanIchubConfig().Read()

	if len(cmd.Flags().Args()) == 0 {
		baseutils.IfProxy(cfg.Software)
		fmt.Println(jsonutils.ToJsonPretty(cfg.Software))
		return
	}
	var item = cmd.Flags().Arg(0)
	switch item {
	case "rpc":
		fmt.Println(jsonutils.ToJsonPretty(cfg.ReadIchubRpc()))
	case "web":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Web))
	case "es":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Es))
	case "etcd":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Etcd))
	case "db":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Datasource))
	case "factroy":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Factroy))
	case "redis":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Redis))
	case "nats":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Natscli))
	case "swag":
		fmt.Println(jsonutils.ToJsonPretty(cfg.Web.Swagger))
	case "escli":
		fmt.Println(jsonutils.ToJsonPretty(cfg.ReadIchubEs()))
	case "webcli":
		fmt.Println(jsonutils.ToJsonPretty(cfg.ReadIchubWebClient()))
	case "webserver":
		fmt.Println(jsonutils.ToJsonPretty(cfg.ReadIchubWebServer()))

	}
}
