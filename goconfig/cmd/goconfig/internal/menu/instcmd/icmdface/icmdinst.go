package icmdface

import "github.com/spf13/cobra"

type IcmdInst interface {
	Setup(cmd *cobra.Command, args []string)
	Do(cmd *cobra.Command, args []string)
	TearDown()

	Inst(cmd *cobra.Command, args []string)
}
