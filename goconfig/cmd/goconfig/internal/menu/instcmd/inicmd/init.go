package inicmd

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/consts"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cmdutils"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/webfile"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"log"
	"os"
)

var gomod = `
module  websample

go 1.20

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0 // indirect
require gitee.com/leijmdas/goweb v0.0.0-20240720075641-8970856fa90e // indirect

`

func CreateGomod() error {
	var curpath, _ = os.Getwd()
	return fileutils.WriteBytesFile(curpath+"/readme", []byte(gomod))
}

var InstallConfigCmd = &cobra.Command{Use: "config", Short: "i config", Run: InstallConfigServer}

var InstallPbinCmd = &cobra.Command{Use: "pbin", Short: "i pbin", Run: InstallPbin}

var InstallTestCmd = &cobra.Command{Use: "test", Short: "i test", Run: InstallTest}

var InstallCodeCmd = &cobra.Command{Use: "code", Short: "i code", Run: InstallCode}

func InstallCode(cmd *cobra.Command, args []string) {
	fmt.Println("install code of project")
	err := webfile.FindBeanWebFile().Copy2Sample(consts.WebCode)
	if err != nil {
		logrus.Fatalf("Failed to install code config: %v", err)
		return
	}
	cmdutils.NewCodeUtils().ReplaceAllPkg()
}

var InstallWeb = &cobra.Command{
	Use:   "websample",
	Short: "i websample of goweb",
	Run:   InstallWebServer,
}

var InstallConfig = &cobra.Command{
	Use:   "config",
	Short: "inst config of goweb",
	Run:   InstallConfigServer,
}
var InstallCmd = &cobra.Command{
	Use:   "cmd",
	Short: "inst cmd of goweb",
	Run:   InstallCmdServer,
}
var InstallDockerCmd = &cobra.Command{
	Use:   "docker",
	Short: "install docker of goweb",
	Run:   InstallDocker,
}
var InstallDocxCmd = &cobra.Command{
	Use:   "docx",
	Short: "inst docx of goweb",
	Run:   InstallDocx,
}
var InstallAllCmd = &cobra.Command{
	Use:   "all",
	Short: "all for cmd webserver",
	Run:   InstallAll,
}
var InstallDomainCmd = &cobra.Command{
	Use:   "domain",
	Short: "install domain of goweb",
	Run:   InstallDomain,
}
var InstallNatsSampleCmd = &cobra.Command{
	Use:   "nastssample",
	Short: "install natssample of goweb",
	Run:   InstallNatsSample,
}
var InstallGorpcCmd = &cobra.Command{Use: "gorpc", Short: "i gorpc", Run: InstallGorpc}

func InstallAll(cmd *cobra.Command, args []string) {
	InstallConfigServer(cmd, args)
	InstallWebServer(cmd, args)
	InstallCmdServer(cmd, args)
	InstallDocker(cmd, args)
	InstallDocx(cmd, args)
	InstallDomain(cmd, args)

}
func InstallTest(cmd *cobra.Command, args []string) {
	fmt.Println("install website-admin of goweb")
	err := webfile.FindBeanWebFile().Copy2Sample(consts.WebTest)
	if err != nil {
		logrus.Fatalf("Failed to install website-admin: %v", err)

	}
}
func InstallCmdServer(cmd *cobra.Command, args []string) {
	fmt.Println("install cmd of goweb")

	err := webfile.FindBeanWebFile().CopyCmd2Sample()
	if err != nil {
		log.Fatalf("Failed to install cmd server: %v", err)
	}
}
func InstallDocker(cmd *cobra.Command, args []string) {
	fmt.Println("install docker of goweb")

	err := webfile.FindBeanWebFile().CopyDocker2Sample()
	if err != nil {
		log.Fatalf("Failed to install Docker ： %v", err)
	}
}

func InstallDocx(cmd *cobra.Command, args []string) {
	fmt.Println("install docx of goweb")

	err := webfile.FindBeanWebFile().CopyDocx2Sample()
	if err != nil {
		log.Fatalf("Failed to install Docx : %v", err)
	}
}
func InstallDomain(cmd *cobra.Command, args []string) {
	fmt.Println("install domain of goweb")

	err := webfile.FindBeanWebFile().CopyDomain2Sample()
	if err != nil {
		log.Fatalf("Failed to install Domain : %v", err)
	}
}
func InstallWebServer(cmd *cobra.Command, args []string) {
	fmt.Println("install web of goweb")
	err := webfile.FindBeanWebFile().CopyWeb2Sample()
	if err != nil {
		log.Fatalf("Failed to install web server: %v", err)
	}
}

func InstallConfigServer(cmd *cobra.Command, args []string) {
	fmt.Println("install config of goweb")
	err := webfile.FindBeanWebFile().CopyConfig2Sample()
	if err != nil {
		log.Fatalf("Failed to install config of  webserver: %v", err)
	}
}
func InstallGorpc(cmd *cobra.Command, args []string) {
	fmt.Println("install gorpc")
	err := webfile.FindBeanWebFile().Copy2Sample(consts.WebGorpc)
	if err != nil {
		logrus.Fatalf("Failed to install grpc config: %v", err)
		return
	}
	cmdutils.NewGorpcUtils().ReplaceAllPkg()
}

func InstallPbin(cmd *cobra.Command, args []string) {
	fmt.Println("install pb bin of gorpc")
	err := webfile.FindBeanWebFile().Copy2Sample(consts.WebPbin)
	if err != nil {
		logrus.Fatalf("Failed to install pbin config: %v", err)
	}
}
func InstallNatsSample(cmd *cobra.Command, args []string) {
	fmt.Println("install docx of goweb")
	err := webfile.FindBeanWebFile().Copy2Sample(consts.WebNats)
	if err != nil {
		logrus.Fatalf("Failed to install docker: %v", err)

	}
}
