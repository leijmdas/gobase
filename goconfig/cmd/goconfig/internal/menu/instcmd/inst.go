package instcmd

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/templatemethod"
	"github.com/spf13/cobra"
)

//安装项目

var InstallGonatsSampleCmd = &cobra.Command{Use: "nats", Short: "i nats", Run: inicmd.InstallNatsSample}

var InstallProjectRpcCmd = &cobra.Command{Use: "rpc", Short: "install project gorpc", Run: installProjectRpc}
var InstallProjectApiCmd = &cobra.Command{Use: "api", Short: "install project api", Run: installProjectApi}
var InstallProjectAdminCmd = &cobra.Command{Use: "admin", Short: "install project admin", Run: installProjectAdmin}
var InstallProjectWebCmd = &cobra.Command{Use: "web", Short: "install project web", Run: installProjectWeb}
var InstallProjectMobileCmd = &cobra.Command{Use: "mobile", Short: "install project mobile", Run: installMobileWeb}
var InstallProjectCmd = &cobra.Command{Use: "cmd", Short: "install project cmd", Run: inicmd.InstallCmdServer}

func installProjectApi(cmd *cobra.Command, args []string) {

	templatemethod.FindBeanCmdInstApi().Inst(cmd, args)

}

func installProjectRpc(cmd *cobra.Command, args []string) {
	templatemethod.FindBeanCmdInstRpc().Inst(cmd, args)

}

func installProjectAdmin(cmd *cobra.Command, args []string) {
	templatemethod.FindBeanCmdInstAdmin().Inst(cmd, args)

}
func installProjectWeb(cmd *cobra.Command, args []string) {
	templatemethod.FindBeanCmdInstWeb().Inst(cmd, args)

}
func installMobileWeb(cmd *cobra.Command, args []string) {

}
