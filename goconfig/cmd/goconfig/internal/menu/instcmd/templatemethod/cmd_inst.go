package templatemethod

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/icmdface"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"

	"github.com/gogf/gf/v2/os/gfile"
	"github.com/spf13/cobra"
)

type CmdInst struct {
	basedto.BaseEntitySingle
	Cmdtype string
	IfRmWeb bool
}

func NewCmdInst() *CmdInst {
	return &CmdInst{}
}

func (c CmdInst) Setup(cmd *cobra.Command, args []string) {
	var file = fileutils.FindRootDir() + "/config/app-env.yml"
	if !fileutils.TryFileExist(file) {
		inicmd.InstallConfigServer(cmd, args)
	}
}
func (c CmdInst) Do(cmd *cobra.Command, args []string) {
	fmt.Println("do...")
}

func (c CmdInst) TearDown() {
	if c.IfRmWeb {
		c.RemoveWebSample()
	}
}
func (c CmdInst) Inst(cmd *cobra.Command, args []string) {
	c.Setup(cmd, args)
	if icmdinst, ok := c.Proxy().Some().(icmdface.IcmdInst); ok {
		icmdinst.Do(cmd, args)
	} else {
		c.Do(cmd, args)
	}
	c.TearDown()
}
func (c CmdInst) RemoveWebSample() {
	gfile.Remove(fileutils.FindRootDir() + "/websample")
}
