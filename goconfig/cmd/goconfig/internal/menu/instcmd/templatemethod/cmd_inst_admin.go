package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"github.com/spf13/cobra"
)

type CmdInstAdmin struct {
	CmdInst
}

func NewCmdInstAdmin() *CmdInstAdmin {
	var ca = &CmdInstAdmin{
		//	CmdInst: NewCmdInst(),
	}
	ca.IfRmWeb = false
	ca.InitProxy(ca)
	return ca
}

func (c CmdInstAdmin) Do(cmd *cobra.Command, args []string) {
	inicmd.InstallWebServer(cmd, args)
	inicmd.InstallCmdServer(cmd, args)
	inicmd.InstallDocker(cmd, args)
	inicmd.InstallDocx(cmd, args)
	inicmd.InstallTest(cmd, args)
}
