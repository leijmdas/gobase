package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cmd_inst_admin_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-08 09:41:39)
	@Update 作者: leijmdas@163.com 时间(2024-09-08 09:41:39)

* *********************************************************/

const singleNameCmdInstAdmin = "*instcmd.CmdInstAdmin-e746e3d1c9054ebbb26af8c469ff1b54"

// init register load
func init() {
	registerBeanCmdInstAdmin()
}

// register CmdInstAdmin
func registerBeanCmdInstAdmin() {
	basedi.RegisterLoadBean(singleNameCmdInstAdmin, LoadCmdInstAdmin)
}

func FindBeanCmdInstAdmin() *CmdInstAdmin {
	bean, ok := basedi.FindBean(singleNameCmdInstAdmin).(*CmdInstAdmin)
	if !ok {
		logrus.Errorf("FindBeanCmdInstAdmin: failed to cast bean to *CmdInstAdmin")
		return nil
	}
	return bean

}

func LoadCmdInstAdmin() baseiface.ISingleton {
	var s = NewCmdInstAdmin()
	InjectCmdInstAdmin(s)
	return s

}

func InjectCmdInstAdmin(s *CmdInstAdmin) {

	// // logrus.Debug("inject")
}
