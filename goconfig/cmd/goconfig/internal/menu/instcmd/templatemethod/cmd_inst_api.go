package templatemethod

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"github.com/spf13/cobra"
)

type CmdInstApi struct {
	CmdInst
}

func NewCmdInstApi() *CmdInstApi {
	var ca = &CmdInstApi{}
	ca.InitProxy(ca)
	ca.IfRmWeb = true
	return ca
}

func (c CmdInstApi) Do(cmd *cobra.Command, args []string) {
	fmt.Println("do...")
	inicmd.InstallWebServer(cmd, args)
	inicmd.InstallCmdServer(cmd, args)
	inicmd.InstallCode(cmd, args)
	inicmd.InstallDocker(cmd, args)
	inicmd.InstallDocx(cmd, args)
	inicmd.InstallDomain(cmd, args)
	inicmd.InstallPbin(cmd, args)
	inicmd.InstallGorpc(cmd, args)
	//installNatsSample(cmd, args)

	inicmd.InstallTest(cmd, args)
}
