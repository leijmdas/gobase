package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cmd_inst_api_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-08 09:43:37)
	@Update 作者: leijmdas@163.com 时间(2024-09-08 09:43:37)

* *********************************************************/

const singleNameCmdInstApi = "*templatemethod.CmdInstApi-dc2b5c26c4de4b9b9fdbfc3afa086404"

// init register load
func init() {
	registerBeanCmdInstApi()
}

// register CmdInstApi
func registerBeanCmdInstApi() {
	basedi.RegisterLoadBean(singleNameCmdInstApi, LoadCmdInstApi)
}

func FindBeanCmdInstApi() *CmdInstApi {
	bean, ok := basedi.FindBean(singleNameCmdInstApi).(*CmdInstApi)
	if !ok {
		logrus.Errorf("FindBeanCmdInstApi: failed to cast bean to *CmdInstApi")
		return nil
	}
	return bean

}

func LoadCmdInstApi() baseiface.ISingleton {
	var s = NewCmdInstApi()
	InjectCmdInstApi(s)
	return s

}

func InjectCmdInstApi(s *CmdInstApi) {

	// // logrus.Debug("inject")
}
