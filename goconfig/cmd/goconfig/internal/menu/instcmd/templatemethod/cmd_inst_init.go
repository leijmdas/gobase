package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cmd_inst_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-08 09:40:04)
	@Update 作者: leijmdas@163.com 时间(2024-09-08 09:40:04)

* *********************************************************/

const singleNameCmdInst = "*instcmd.CmdInst-5e8802bcbe414f538799a9c3fce8f145"

// init register load
func init() {
	registerBeanCmdInst()
}

// register CmdInst
func registerBeanCmdInst() {
	basedi.RegisterLoadBean(singleNameCmdInst, LoadCmdInst)
}

func FindBeanCmdInst() *CmdInst {
	bean, ok := basedi.FindBean(singleNameCmdInst).(*CmdInst)
	if !ok {
		logrus.Errorf("FindBeanCmdInst: failed to cast bean to *CmdInst")
		return nil
	}
	return bean

}

func LoadCmdInst() baseiface.ISingleton {
	var s = &CmdInst{}
	InjectCmdInst(s)
	return s

}

func InjectCmdInst(s *CmdInst) {

	// // logrus.Debug("inject")
}
