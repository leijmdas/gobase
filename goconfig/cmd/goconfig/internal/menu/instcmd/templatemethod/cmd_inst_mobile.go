package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"github.com/spf13/cobra"
)

type CmdInstMobile struct {
	CmdInst
}

func NewCmdInstMobile() *CmdInstMobile {
	var ca = &CmdInstMobile{}
	ca.InitProxy(ca)
	return ca
}

func (c CmdInstMobile) Do(cmd *cobra.Command, args []string) {
	inicmd.InstallWebServer(cmd, args)
	inicmd.InstallCmdServer(cmd, args)
	inicmd.InstallDocker(cmd, args)
	inicmd.InstallDocx(cmd, args)
	inicmd.InstallTest(cmd, args)

}
