package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"github.com/spf13/cobra"
)

type CmdInstRpc struct {
	CmdInst
}

func NewCmdInstRpc() *CmdInstRpc {
	var ca = &CmdInstRpc{}
	ca.InitProxy(ca)
	ca.IfRmWeb = true
	return ca
}

func (c CmdInstRpc) Do(cmd *cobra.Command, args []string) {
	inicmd.InstallWebServer(cmd, args)
	inicmd.InstallCmdServer(cmd, args)
	inicmd.InstallDocker(cmd, args)
	inicmd.InstallDomain(cmd, args)
	inicmd.InstallDocx(cmd, args)
	inicmd.InstallTest(cmd, args)
	inicmd.InstallGorpc(cmd, args)
	inicmd.InstallPbin(cmd, args)
	inicmd.InstallNatsSample(cmd, args)
}
