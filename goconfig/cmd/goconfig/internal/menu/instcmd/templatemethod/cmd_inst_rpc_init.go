package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cmd_inst_rpc_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-08 09:44:48)
	@Update 作者: leijmdas@163.com 时间(2024-09-08 09:44:48)

* *********************************************************/

const singleNameCmdInstRpc = "*templatemethod.CmdInstRpc-28d561535d2e4e5aac3e0fa3107053e6"

// init register load
func init() {
	registerBeanCmdInstRpc()
}

// register CmdInstRpc
func registerBeanCmdInstRpc() {
	basedi.RegisterLoadBean(singleNameCmdInstRpc, LoadCmdInstRpc)
}

func FindBeanCmdInstRpc() *CmdInstRpc {
	bean, ok := basedi.FindBean(singleNameCmdInstRpc).(*CmdInstRpc)
	if !ok {
		logrus.Errorf("FindBeanCmdInstRpc: failed to cast bean to *CmdInstRpc")
		return nil
	}
	return bean

}

func LoadCmdInstRpc() baseiface.ISingleton {
	var s = NewCmdInstRpc()
	InjectCmdInstRpc(s)
	return s

}

func InjectCmdInstRpc(s *CmdInstRpc) {

	// // logrus.Debug("inject")
}
