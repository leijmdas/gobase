package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"github.com/spf13/cobra"
)

type CmdInstWeb struct {
	CmdInst
}

func NewCmdInstWeb() *CmdInstWeb {
	var ca = &CmdInstWeb{}
	ca.InitProxy(ca)
	ca.IfRmWeb = false
	return ca
}

func (c CmdInstWeb) Do(cmd *cobra.Command, args []string) {
	inicmd.InstallWebServer(cmd, args)
	inicmd.InstallCmdServer(cmd, args)
	inicmd.InstallDocker(cmd, args)
	inicmd.InstallDocx(cmd, args)
	inicmd.InstallTest(cmd, args)
}
