package templatemethod

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cmd_inst_web_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-08 09:44:48)
	@Update 作者: leijmdas@163.com 时间(2024-09-08 09:44:48)

* *********************************************************/

const singleNameCmdInstWeb = "*templatemethod.CmdInstWeb-0b7ef6154561459290702596127865f1"

// init register load
func init() {
	registerBeanCmdInstWeb()
}

// register CmdInstWeb
func registerBeanCmdInstWeb() {
	basedi.RegisterLoadBean(singleNameCmdInstWeb, LoadCmdInstWeb)
}

func FindBeanCmdInstWeb() *CmdInstWeb {
	bean, ok := basedi.FindBean(singleNameCmdInstWeb).(*CmdInstWeb)
	if !ok {
		logrus.Errorf("FindBeanCmdInstWeb: failed to cast bean to *CmdInstWeb")
		return nil
	}
	return bean

}

func LoadCmdInstWeb() baseiface.ISingleton {
	var s = NewCmdInstWeb()
	InjectCmdInstWeb(s)
	return s

}

func InjectCmdInstWeb(s *CmdInstWeb) {

	// // logrus.Debug("inject")
}
