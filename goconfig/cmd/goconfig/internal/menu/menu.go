package menu

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/checkvercmd"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/cmdenc"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/configcmd"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu/instcmd/inicmd"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cyclocomplex"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gomenufactroy"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"

	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/webfile"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/spf13/cobra"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "start web server",
	Run:   startServer,
}
var LisCfgCmd = &cobra.Command{
	Use:   "cfg",
	Short: "list config of project",
	Run:   ListCfg,
}

var ccCheckAllCmd = &cobra.Command{
	Use:   "ccall",
	Short: "ccall : a tool of gomini menu",
	Long:  `This is a gomini cc check using Cobra`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(cmd.Flags().Args()) > 0 {
			flag := cmd.Flags().Arg(0)
			cyclocomplex.FindBeanCycloComplex().CcCheckScan(gconv.Int(flag))
			return
		}

		cyclocomplex.FindBeanCycloComplex().CcCheckAllScan()
	},
}
var ccCheckCmd = &cobra.Command{
	Use:   "cc",
	Short: "cc : a tool of gomini menu",
	Long:  `This is a gomini cc check using Cobra`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(cmd.Flags().Args()) > 0 {
			flag := cmd.Flags().Arg(0)
			cyclocomplex.FindBeanCycloComplex().CcCheckScan(gconv.Int(flag))
			return
		}

		cyclocomplex.FindBeanCycloComplex().CcCheckScan()
	},
}
var ccCheckFileCmd = &cobra.Command{
	Use:   "ccfile",
	Short: "ccfile filename : a tool of gomini menu",
	Long:  `This is a gomini cc check using Cobra`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(cmd.Flags().Args()) == 0 {
			fmt.Println("please enter file ")
			return
		}
		file := cmd.Flags().Arg(0)
		if len(cmd.Flags().Args()) > 1 {
			flag := cmd.Flags().Arg(1)
			cyclocomplex.FindBeanCycloComplex().CcCheckFileScan(file, gconv.Int(flag))
			return
		}

		cyclocomplex.FindBeanCycloComplex().CcCheckFileScan(file)
	},
}

func ListCfg(cmd *cobra.Command, args []string) {
	fmt.Println("list config of project")
	var cfg = ichubconfig.FindBeanIchubConfig()
	fmt.Println(cfg.ReadIchubWebClient())
}

var ListServiceCmd = &cobra.Command{
	Use:   "service",
	Short: "list service of project",
	Run:   ListService,
}

func ListService(cmd *cobra.Command, args []string) {

}

var ListPathCmd = &cobra.Command{
	Use:   "path",
	Short: "list path of project",
	Run:   ListPath,
}

func ListPath(cmd *cobra.Command, args []string) {
	fmt.Println("view path of goweb")
	webfile.FindBeanWebFile().Ls()
}

var testCmd = &cobra.Command{
	Use:   "test",
	Short: "test web server",
	Run:   TestWeb,
}

func init() {

	var factroy = gomenufactroy.FindBeanGomenuFactroy()
	factroy.SetVersion("v1.0")
	factroy.SetCmdName("goweb")
	factroy.SetBasePkg("gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/menu")
	factroy.Init()
	factroy.AddCommands(cmdenc.EncCmds()...)
	factroy.AddCommands(configcmd.ConfigListCmd, ccCheckCmd, ccCheckFileCmd, ccCheckAllCmd)
	factroy.AddSubCmds("web", serverCmd, testCmd)
	factroy.AddSubCmds("list", ListPathCmd, ListServiceCmd, LisCfgCmd)

	factroy.AddSubCmds("cv", checkvercmd.CheckVerDockerCmd, checkvercmd.CheckVerAllCmd, checkvercmd.CheckVerAllGoCmd)

	// inst
	factroy.AddSub("inst", "一键生成工程",
		inicmd.InstallConfigCmd, inicmd.InstallDomainCmd,
		instcmd.InstallProjectCmd, instcmd.InstallProjectApiCmd, instcmd.InstallProjectRpcCmd,
		instcmd.InstallProjectAdminCmd, instcmd.InstallProjectWebCmd, instcmd.InstallProjectMobileCmd)

	// install
	factroy.AddSub("i", "一键安装子项",
		inicmd.InstallDomainCmd, inicmd.InstallGorpcCmd, inicmd.InstallTestCmd,
		inicmd.InstallNatsSampleCmd, inicmd.InstallDocxCmd, inicmd.InstallDockerCmd,
		inicmd.InstallAllCmd, inicmd.InstallPbinCmd, inicmd.InstallCmd,
		inicmd.InstallCodeCmd, inicmd.InstallGorpcCmd, inicmd.InstallWeb)

}

func Execute() {
	gomenufactroy.FindBeanGomenuFactroy().Execute()
}

func startServer(cmd *cobra.Command, args []string) {
	// 启动服务逻辑
	//webstart.StartWeb()

}

func TestWeb(cmd *cobra.Command, args []string) {
	//var webClient = webclient.Default()
	//var result = webClient.Get("/")
	//logrus.Info(result)
}
