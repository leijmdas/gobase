package checkver

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"github.com/sirupsen/logrus"
	"strings"
)

const ERR_FLAG = ">>>>>>>"

type CheckVer struct {
	basedto.BaseEntitySingle
	*fileutils.FindFileService
}

func NewCheckVer() *CheckVer {
	return &CheckVer{
		FindFileService: fileutils.NewFindFileService(),
	}
}
func (self *CheckVer) CheckAllFiles() error {
	var fs, err = self.FindAllFile()
	if err != nil {
		logrus.Error("CheckAllFiles is err! " + err.Error())
		return err
	}

	err = self.CheckFiles(fs)

	if err != nil {
		logrus.Error("\r\n" + err.Error())
		return err
	}
	fmt.Println("CheckAllFiles is ok! ")
	return nil
}
func (self *CheckVer) CheckAllGoFiles() error {
	var fs, err = self.FindAllGoFile()
	if err != nil {
		logrus.Error("CheckAllGoFiles is err! " + err.Error())
		return err
	}

	err = self.CheckFiles(fs)

	if err != nil {
		logrus.Error("\r\n" + err.Error())
		return err
	}
	fmt.Println("CheckAllGoFiles is ok! ")
	return nil
}
func (self *CheckVer) CheckDockerFiles() error {
	var fs, err = self.FindDockerFile()
	if err != nil {
		logrus.Error("CheckDockerFiles is err! " + err.Error())
		return err
	}

	err = self.CheckFiles(fs)

	if err != nil {
		logrus.Error("\r\n" + err.Error())
		return err
	}
	fmt.Println("CheckDockerFiles is ok! ")
	return nil
}

func (self *CheckVer) CheckFiles(files []string) error {
	for _, file := range files {

		var err = self.CheckOneFile(file)
		if err != nil {
			return err
		}

	}
	return nil
}
func (self *CheckVer) FindDockerFile() ([]string, error) {
	//check all , dir ,  one dir
	return self.FindAllFiles(fileutils.FindRootDir() + "/docker")

}
func (self *CheckVer) FindAllFile() ([]string, error) {

	return self.FindAllFiles(fileutils.FindRootDir())

}
func (self *CheckVer) FindAllGoFile() ([]string, error) {

	return self.FindAllGoFiles(fileutils.FindRootDir())

}
func (self *CheckVer) CheckOneFile(file string) error {
	//check all , dir ,  one dir
	str, err := fileutils.ReadFileToString(file)
	if err != nil {
		return errors.New(err.Error() + file)
	}
	if strings.Contains(str, ERR_FLAG) && !strings.Contains(str, "ERR_FLAG") {
		return errors.New("check merge file error! " + file + " has merge error ! " + ERR_FLAG)
	}
	return nil //fileutils.FindAllFiles(fileutils.FindRootDir() + "/docker")

}
func (self *CheckVer) FindAllFiles(dirPath string) ([]string, error) {
	self.SetDir(dirPath)
	self.SetExcludeSuffix(".exe,.docx")
	return self.FindFiles()

	//
	////var dirPath = fileutils.FindRootDirGoMod() + consts.Webcmd
	//var files = make([]string, 0)
	//// 指定需要遍历的目录
	//// 使用filepath.Walk遍历目录
	//err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
	//	if err != nil {
	//		logrus.Error(err)
	//		return err
	//	}
	//	// 如果是文件，可以进行额外操作，比如读取文件内容
	//	if !info.IsDir() {
	//		if strings.HasSuffix(path, ".exe") ||
	//			strings.HasSuffix(path, ".docx") {
	//			return nil
	//		}
	//		files = append(files, path)
	//
	//	}
	//	// 返回nil继续遍历
	//	return nil
	//})
	//
	//if err != nil {
	//	logrus.Error("Error walking the path:", err)
	//}
	//return files, err
}
func (self *CheckVer) FindAllGoFiles(dirPath string) ([]string, error) {
	self.SetDir(dirPath)
	self.SetIncludeSuffix(".go")
	return self.FindFiles()
	//var dirPath = fileutils.FindRootDirGoMod() + consts.Webcmd
	//var files = make([]string, 0)
	//// 指定需要遍历的目录
	//// 使用filepath.Walk遍历目录
	//err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
	//	if err != nil {
	//		logrus.Error(err)
	//		return err
	//	}
	//	// 如果是文件，可以进行额外操作，比如读取文件内容
	//	if !info.IsDir() {
	//		if !strings.HasSuffix(path, ".go") {
	//			return nil
	//		}
	//		files = append(files, path)
	//
	//	}
	//	// 返回nil继续遍历
	//	return nil
	//})
	//
	//if err != nil {
	//	logrus.Error("Error walking the path:", err)
	//}
	//return files, err
}
