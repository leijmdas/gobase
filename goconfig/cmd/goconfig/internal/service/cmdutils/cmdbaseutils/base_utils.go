package cmdbaseutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/consts"
	"github.com/gogf/gf/text/gstr"
	"github.com/sirupsen/logrus"
)

const (
	NOW_PKG_DOMAIN       = "gitee.com/leijmdas/goweb/domain"
	NOW_PKG_CODE         = "gitee.com/leijmdas/goweb/code"
	NOW_PKG_GORPC        = "gitee.com/leijmdas/goweb/gorpc"
	NOW_PKG_GONATSSAMPLE = "gitee.com/leijmdas/goweb/gonatssample"
	//NOW_PKG_DOCKER     = "gitee.com/leijmdas/goweb/docker"

)

type BaseUtils struct {
	*CmdUtils
	NowPkg        string
	WebSimplePath string
}

func NewBaseUtils() *BaseUtils {
	return &BaseUtils{
		CmdUtils:      NewCmdUtils(),
		WebSimplePath: consts.WebDomain,
		NowPkg:        NOW_PKG_DOMAIN,
	}
}

func (self *BaseUtils) ReplaceAllPkg() {
	var fs, err = self.FindFiles()
	if err != nil {
		logrus.Error(err)
		return
	}
	for _, pathfile := range fs {
		self.replacePkg(pathfile)
	}

}
func (self *BaseUtils) FindFiles() ([]string, error) {
	return self.FindGoFiles(fileutils.FindRootDirGoMod() + self.WebSimplePath)
}

func (self *BaseUtils) replacePkg(file string) error {
	var c, err = fileutils.ReadFileToString(file)
	if err != nil {
		logrus.Error(err)
		return err
	}
	c = gstr.Replace(c, self.NowPkg, self.pkg+self.WebSimplePath)
	return fileutils.WriteBytesFile(file, []byte(c))
}
