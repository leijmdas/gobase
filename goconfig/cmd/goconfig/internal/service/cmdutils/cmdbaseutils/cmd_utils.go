package cmdbaseutils

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/consts"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gominifactroy"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"

	"github.com/duke-git/lancet/fileutil"
	"github.com/gogf/gf/os/gfile"
	"github.com/gogf/gf/text/gstr"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"strings"
)

const NOW_PKG_CMD = "gitee.com/leijmdas/gobase/goconfig/cmd/goconfig"
const NOW_PKG = "gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/dto"
const NOW_PKG_SIMPLE = "goweb"

type CmdUtils struct {
	basedto.BaseEntitySingle
	pkgSmple string
	pkg      string
}

func NewCmdUtils() *CmdUtils {
	return &CmdUtils{
		pkgSmple: gominifactroy.FindBeanGominiFactroy().FindProjectPkgNameSimple(),
		pkg:      gominifactroy.FindBeanGominiFactroy().FindProjectPkgName(),
	}

}

func (self *CmdUtils) MoveCmdDirGoweb() {
	var cmdDir = fileutils.FindRootDirGoMod() + consts.Webcmd
	var newDir = gstr.Replace(cmdDir, NOW_PKG_SIMPLE, self.pkgSmple)
	logrus.Info(cmdDir, newDir)
	if cmdDir != newDir {
		var e = gfile.Move(cmdDir, newDir)
		if e != nil {
			logrus.Error(e)
		}
	} else {
		logrus.Info("\r\n", cmdDir, newDir, "\r\n dir is same!")
	}

}
func (self *CmdUtils) ReplaceCmdAll() {
	var cmdDir = fileutils.FindRootDirGoMod() + consts.Webcmdbat
	var s, e = fileutil.ListFileNames(cmdDir)
	logrus.Info(s, e)
	for index, f := range s {
		var pathfile = cmdDir + "/" + f
		fmt.Println(index, pathfile)
		self.replaceFile(pathfile)
	}

}
func (self *CmdUtils) ReplacePkgCmd2New() {
	var fs, err = self.FindFiles()
	if err != nil {
		logrus.Error(err)
		return
	}

	for index, pathfile := range fs {
		fmt.Println(index, pathfile)
		self.replacePkgCmdNewFile(pathfile)
	}

}
func (self *CmdUtils) replacePkgCmdNewFile(file string) {
	var c, err = fileutils.ReadFileToString(file)
	if err != nil {
		logrus.Error(err)
		return
	}
	c = gstr.Replace(c, NOW_PKG_CMD, self.pkg+"/cmd/"+self.pkgSmple)
	err = fileutils.WriteBytesFile(file, []byte(c))
	goutils.Error(err)

}
func (self *CmdUtils) FindFiles() ([]string, error) {
	return self.FindGoFiles(fileutils.FindRootDirGoMod() + consts.Webcmd)
}
func (this *CmdUtils) FindGoFiles(dirPath string) ([]string, error) {
	//var dirPath = fileutils.FindRootDirGoMod() + consts.Webcmd
	var files = make([]string, 0)
	// 使用filepath.Walk遍历目录
	err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			goutils.Error(err)
			return err
		}
		// 如果是文件，可以进行额外操作，比如读取文件内容
		if !info.IsDir() {
			if strings.HasSuffix(path, ".go") {

				files = append(files, path)
			}
		}
		// 返回nil继续遍历
		return nil
	})

	if err != nil {
		logrus.Error("Error walking the path:", err)
	}
	return files, err
}
func (self *CmdUtils) ReplaceCmdBatGoweb() error {
	var cmdDir = fileutils.FindRootDirGoMod() + consts.Webcmdbat
	var s, err = fileutil.ListFileNames(cmdDir)

	for _, f := range s {
		var pathfile = cmdDir + "/" + f
		self.replaceFile(pathfile)
	}
	return err
}

func (self *CmdUtils) replaceFile(file string) {
	var c, err = fileutils.ReadFileToString(file)
	if err != nil {
		logrus.Error(err)
		return
	}
	c = gstr.Replace(c, NOW_PKG_SIMPLE, self.pkgSmple)
	fileutils.WriteBytesFile(file, []byte(c))
}

func (self *CmdUtils) ReplaceCmdGoinstallWebcli() {
	var cmdDir = fileutils.FindRootDir()
	var s, e = fileutil.ListFileNames(cmdDir)
	logrus.Info(s, e)
	for index, f := range s {

		if gstr.Contains(f, "goinstall") {
			var pathfile = cmdDir + "/" + f
			fmt.Println(index, pathfile)
			self.replaceFile(pathfile)
		}
	}

}
