package cmdbaseutils

import (
	//"github.com/sirupsen/logrus"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

/*
	@Title   文件名称: cmd_utils_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-07-25 13:54:48
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-07-25 13:54:48
*/

const singleNameCmdUtils = "install.CmdUtils"

// init register load
func init() {
	registerBeanCmdUtils()
}

// register CmdUtils
func registerBeanCmdUtils() {
	basedi.RegisterLoadBean(singleNameCmdUtils, LoadCmdUtils)
}

func FindBeanCmdUtils() *CmdUtils {
	return basedi.FindBean(singleNameCmdUtils).(*CmdUtils)
}

func LoadCmdUtils() baseiface.ISingleton {
	var inst = NewCmdUtils()
	InjectCmdUtils(inst)
	return inst

}

func InjectCmdUtils(s *CmdUtils) {

	//// // logrus.Debug("inject")
}
