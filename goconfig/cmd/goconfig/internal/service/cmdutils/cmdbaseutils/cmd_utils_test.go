package cmdbaseutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestCmdUtilsSuite struct {
	suite.Suite
	inst *CmdUtils

	rootdir string
}

func (this *TestCmdUtilsSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanCmdUtils()
	this.rootdir = fileutils.FindRootDir()

}
func (suite *TestCmdUtilsSuite) TearDownTest() {
	logrus.Println(" teardown")
}
func (suite *TestCmdUtilsSuite) TearDownSuite() {
	logrus.Println(" teardown")
}

func TestCmdUtilsSuites(t *testing.T) {
	suite.Run(t, new(TestCmdUtilsSuite))
}

func (this *TestCmdUtilsSuite) Test001_MoveCmdDirWebcli() {
	logrus.Info(1)
	FindBeanCmdUtils().MoveCmdDirGoweb()

}

func (this *TestCmdUtilsSuite) Test002_ReplaceCmdBatWebcli() {
	logrus.Info(2)
	FindBeanCmdUtils().ReplaceCmdBatGoweb()
}
func (this *TestCmdUtilsSuite) Test003_FindFiles() {
	logrus.Info(2)
	var fs, err = FindBeanCmdUtils().FindFiles()
	logrus.Info(fs, err)
	for _, f := range fs {
		logrus.Info("\r\n", f)
	}
}
