package cmdutils

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/consts"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cmdutils/cmdbaseutils"
)

type CodeUtils struct {
	*cmdbaseutils.BaseUtils
}

func NewCodeUtils() *CodeUtils {

	var cu = &CodeUtils{
		BaseUtils: cmdbaseutils.NewBaseUtils(),
	}
	cu.WebSimplePath = consts.WebCode
	cu.NowPkg = cmdbaseutils.NOW_PKG_CODE
	return cu
}
