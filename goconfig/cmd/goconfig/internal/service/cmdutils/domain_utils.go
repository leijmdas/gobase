package cmdutils

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/consts"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cmdutils/cmdbaseutils"
)

type DomainUtils struct {
	*cmdbaseutils.BaseUtils
}

func NewDomainUtils() *DomainUtils {
	var du = &DomainUtils{
		BaseUtils: cmdbaseutils.NewBaseUtils(),
	}
	du.WebSimplePath = consts.WebDomain
	du.NowPkg = cmdbaseutils.NOW_PKG_DOMAIN
	return du
}
