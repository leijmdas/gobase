package cmdutils

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/consts"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cmdutils/cmdbaseutils"
)

type GorpcUtils struct {
	*cmdbaseutils.BaseUtils
}

func NewGorpcUtils() *GorpcUtils {

	var gru = &GorpcUtils{
		BaseUtils: cmdbaseutils.NewBaseUtils(),
	}
	gru.WebSimplePath = consts.WebGorpc
	gru.NowPkg = cmdbaseutils.NOW_PKG_GORPC
	return gru
}
