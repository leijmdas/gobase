package cyclocomplex

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cyclo_complex_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-13 13:34:53)
	@Update 作者: leijmdas@163.com 时间(2024-09-13 13:34:53)

* *********************************************************/

const singleNameCycloComplex = "*cyclocomplex.CycloComplex-80d61045ccd24d3d952ebe75bb01db4a"

// init register load
func init() {
	registerBeanCycloComplex()
}

// register CycloComplex
func registerBeanCycloComplex() {
	basedi.RegisterLoadBean(singleNameCycloComplex, LoadCycloComplex)
}

func FindBeanCycloComplex() *CycloComplex {
	bean, ok := basedi.FindBean(singleNameCycloComplex).(*CycloComplex)
	if !ok {
		logrus.Errorf("FindBeanCycloComplex: failed to cast bean to *CycloComplex")
		return nil
	}
	return bean

}

func LoadCycloComplex() baseiface.ISingleton {
	var s = NewCycloComplex()
	InjectCycloComplex(s)
	return s

}

func InjectCycloComplex(s *CycloComplex) {

	// logrus.Debug("inject")
}
