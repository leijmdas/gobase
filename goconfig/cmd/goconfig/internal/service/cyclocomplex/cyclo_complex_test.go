package cyclocomplex

import (
	"bufio"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cyclocomplex/internal"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"os"
	"os/exec"
	"testing"
)

//go install github.com/fzipp/gocyclo/cmd/gocyclo@latest
//gocyclo -top 5 .\core

type TestCyclomaticComplexSuite struct {
	suite.Suite
	inst *CycloComplex

	rootdir string
}

func (this *TestCyclomaticComplexSuite) SetupTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanCycloComplex()

	this.rootdir = fileutils.FindRootDir()

}
func (this *TestCyclomaticComplexSuite) TearDownTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanCycloComplex()
	this.rootdir = fileutils.FindRootDir()

}

func TestCyclomaticComplexSuites(t *testing.T) {
	suite.Run(t, new(TestCyclomaticComplexSuite))
}

func (this *TestCyclomaticComplexSuite) Test001_FindDirs() {

	var err = this.inst.FindDirs(fileutils.FindRootDir())

	logrus.Info(err, this.inst)
	var r, _ = this.inst.Scan()
	logrus.Info(jsonutils.ToJsonPretty(r))

}

func (this *TestCyclomaticComplexSuite) Test002_CmdPipe(t *testing.T) {
	// 创建一个命令，例如使用echo命令输出一些文本
	cmd := exec.Command("cat 1.txt")

	// 获取命令的标准输出管道
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		logrus.Error(err)
		return
	}
	defer stdout.Close()

	// 启动命令
	if err := cmd.Start(); err != nil {
		logrus.Error("Error starting command:", err)
		return
	}

	// 使用bufio.NewScanner读取输出
	scanner := bufio.NewScanner(stdout)
	for scanner.Scan() {
		fmt.Println(scanner.Text()) // 打印每一行输出
	}

	// 检查是否有可能的错误发生在扫描过程中
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading stdout:", err)
	}

	// 等待命令执行完毕
	if err := cmd.Wait(); err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for command:", err)
	}
}
func (this *TestCyclomaticComplexSuite) Test003_CmdDir(t *testing.T) {
	internal.FindBeanOsCmd().ExecCmd("cd")
}
func (this *TestCyclomaticComplexSuite) Test004_FindDirsscan() {

	//var err = this.inst.FindDirs(fileutils.FindRootDir())
	//logrus.Info(err, this.inst)
	this.inst.CcCheckScan()
}
