package iface

type CyclomaticComplex interface {
	CcCheckFileScan(file string, topNums ...int) error
	CcCheckScan(topNums ...int)
	CcCheckAllScan(topNums ...int)
}
