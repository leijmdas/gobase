package internal

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: os_cmd_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-25 07:44:15)
	@Update 作者: leijmdas@163.com 时间(2024-08-25 07:44:15)

* *********************************************************/

const singleNameOsCmd = "*cyclocomplex.OsCmd-05e50eb1bc3e47f490057a665103fc2e"

// init register load
func init() {
	registerBeanOsCmd()
}

// register OsCmd
func registerBeanOsCmd() {
	basedi.RegisterLoadBean(singleNameOsCmd, LoadOsCmd)
}

func FindBeanOsCmd() *OsCmd {
	bean, ok := basedi.FindBean(singleNameOsCmd).(*OsCmd)
	if !ok {
		logrus.Errorf("FindBeanOsCmd: failed to cast bean to *OsCmd")
		return nil
	}
	return bean

}

func LoadOsCmd() baseiface.ISingleton {
	var s = NewOsCmd()
	InjectOsCmd(s)
	return s

}

func InjectOsCmd(s *OsCmd) {

	// logrus.Debug("inject")
}
