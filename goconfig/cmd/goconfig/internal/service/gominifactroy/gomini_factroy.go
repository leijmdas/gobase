package gominifactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gominifactroy/internal/minidto"
	mini "gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gominifactroy/internal/minifile"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gominifactroy/internal/minigomod"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gominifactroy/internal/minipkg"
	"github.com/gogf/gf/text/gstr"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"strings"
)

type PkgPathDto struct {
	MiniPathCmd         string
	MiniPathBat         string
	MiniPathConfig      string
	MiniPathData        string
	MiniPathData_output string
	MiniPathData_input  string // = "data/input"
	MiniPathWebsample   string // = "websample"
	MiniPathTest        string //= "test"
}
type GominiFactroy struct {
	//basedto.BaseEntitySingle
	*minidto.MiniPath
	PkgPathDto `json:"pkgPathDto"`

	InstallPath string `json:"installPath"`
}

func NewGominiFactroy() *GominiFactroy {
	var mf = &GominiFactroy{
		MiniPath: minidto.NewMiniPath(),
		PkgPathDto: PkgPathDto{
			MiniPathCmd:         "cmd",
			MiniPathBat:         "cmd/bat",
			MiniPathConfig:      "config",
			MiniPathData:        "data",
			MiniPathData_output: "data/output",
			MiniPathData_input:  "data/input",
			MiniPathWebsample:   "websample",
			MiniPathTest:        "test",
		},
	}
	mf.InstallPath = mf.FindRootDir()
	mf.RootDir = mf.FindRootDir()
	mf.InitProxy(mf)
	return mf
}

// copy pkg path2app
func (self *GominiFactroy) InstallTo() {

}

func (self *GominiFactroy) FindProjectPkgName() string {
	return mini.FindBeanMini().FindPkgName()
}
func (self *GominiFactroy) FindProjectPkgNameSimple() string {

	var projectRootPkg = self.FindProjectPkgName()
	var pkgs = gstr.Split(projectRootPkg, "/")
	return pkgs[len(pkgs)-1]
}

func (self *GominiFactroy) Finds(rootdir string, suffix string) ([]string, error) {
	return mini.FindBeanMini().Finds(rootdir, suffix)
}
func (this *GominiFactroy) FindFiles(rootdir string, suffix string) ([]string, error) {
	var files = make([]string, 0)
	// 指定需要遍历的目录
	// 使用filepath.Walk遍历目录
	err := filepath.Walk(rootdir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			logrus.Error(err)
			return err
		}
		// 如果是文件，可以进行额外操作，比如读取文件内容
		if !info.IsDir() && strings.HasSuffix(path, suffix) {
			files = append(files, path)

		}
		// 返回nil继续遍历
		return nil
	})

	return files, err
}
func (self *GominiFactroy) FindRootDir() string {
	return mini.FindBeanMini().FindRootDir()
}

func (self *GominiFactroy) FindPkgRootDir() string {
	return minipkg.FindBeanMinipkg().FindRootDir()
}

func (self *GominiFactroy) FindPkgRootDirOf(pkgname string) string {
	minipkg.FindBeanMinipkg().PkgName = pkgname
	return minipkg.FindBeanMinipkg().FindRootDir()
}

func (self *GominiFactroy) FindGomodRootDir() string {
	return minigomod.FindBeanMiniGomod().FindRootDir()
}

func (self *GominiFactroy) FindMiniPath(PkgNames ...string) *minidto.MiniPath {

	var miniPath = minidto.NewMiniPath()
	miniPath.RootDir = self.FindRootDir()
	miniPath.GomodRootDir = self.FindGomodRootDir()
	var PkgName = "gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	if len(PkgNames) > 0 {
		PkgName = PkgNames[0]
	}
	miniPath.PkgRootDir = self.FindPkgRootDirOf(PkgName)
	miniPath.PkgName = self.FindProjectPkgName()
	//mini.FindBeanMini().Write(miniPath)
	return miniPath
}
