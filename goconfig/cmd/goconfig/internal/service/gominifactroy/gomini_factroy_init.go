package gominifactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gomini_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-10-13 09:48:16)
	@Update 作者: leijmdas@163.com 时间(2024-10-13 09:48:16)

* *********************************************************/

const singleNameGominiFactroy = "*gominifactroy.GominiFactroy-80090281b23449828f66a969e82a4573"

// init register load
func init() {
	registerBeanGominiFactroy()
}

// register GominiFactroy
func registerBeanGominiFactroy() {
	basedi.RegisterLoadBean(singleNameGominiFactroy, LoadGominiFactroy)
}

func FindBeanGominiFactroy() *GominiFactroy {
	bean, ok := basedi.FindBean(singleNameGominiFactroy).(*GominiFactroy)
	if !ok {
		logrus.Errorf("FindBeanGominiFactroy: failed to cast bean to *GominiFactroy")
		return nil
	}
	return bean

}

func LoadGominiFactroy() baseiface.ISingleton {
	var s = NewGominiFactroy()
	InjectGominiFactroy(s)
	return s

}

func InjectGominiFactroy(s *GominiFactroy) {

	//// goutils.Debug("inject")
}
