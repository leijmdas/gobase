package gominifactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestMiniFactroySuite struct {
	suite.Suite
	inst *GominiFactroy

	rootdir string
}

func (this *TestMiniFactroySuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanMiniFactroy()

	this.rootdir = fileutils.FindRootDir()

}
func (this *TestMiniFactroySuite) TearDownTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanMiniFactroy()

	this.rootdir = fileutils.FindRootDir()

}

func TestMiniFactroySuites(t *testing.T) {
	suite.Run(t, new(TestMiniFactroySuite))
}

func (this *TestMiniFactroySuite) Test001_InstallTo() {
	logrus.Info(1)

}

func (this *TestMiniFactroySuite) Test002_FindProjectPkgName() {
	logrus.Info(1)

}

func (this *TestMiniFactroySuite) Test003_Finds() {
	logrus.Info(1)

}

func (this *TestMiniFactroySuite) Test004_FindRootDir() {
	logrus.Info(1)

}

func (this *TestMiniFactroySuite) Test005_FindPkgRootDir() {
	logrus.Info(1)

}

func (this *TestMiniFactroySuite) Test006_FindPkgRootDirOf() {
	logrus.Info(1)

}

func (this *TestMiniFactroySuite) Test007_FindGomodRootDir() {
	logrus.Info(1)

}

func (this *TestMiniFactroySuite) Test008_FindMiniPath() {
	logrus.Info(1)

}
