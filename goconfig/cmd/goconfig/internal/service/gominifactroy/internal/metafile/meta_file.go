package metafile

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"github.com/duke-git/lancet/fileutil"
	"github.com/gogf/gf/v2/os/gfile"
)

const (
	meta_out = "/data/output/gometa"

	meta_table_out = meta_out + "/dbdict"
	meta_goast_out = meta_out + "/goast"
	meta__out      = meta_out + "/"
)

type Meta struct {
	ObjName string
}

func NewMeta() *Meta {
	return &Meta{}
}

func (this *Meta) Write(minf any) error {
	var path = fileutils.FindRootDir() + meta__out + "/.gometa"

	return this.WriteBytesTo(path, []byte(jsonutils.ToJsonPretty(minf)))
}
func (this *Meta) WriteBytesTo(path string, minf []byte) error {
	path = gfile.Dir(path)
	if err := fileutil.CreateDir(path); err != nil {
		return err

	}

	return fileutils.WriteBytesFile(path, minf)
}
