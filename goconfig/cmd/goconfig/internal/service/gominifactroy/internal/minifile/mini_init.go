package mini

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: {{.Name}}.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-07-20 15:04:39)
	@Update 作者: leijmdas@163.com 时间(2024-07-20 15:04:39)

* *********************************************************/

const singleNameMini = "*mini.Mini"

// init register load
func init() {
	registerBeanMini()
}

// register Mini
func registerBeanMini() {
	basedi.RegisterLoadBean(singleNameMini, LoadMini)
}

func FindBeanMini() *Mini {
	bean, ok := basedi.FindBean(singleNameMini).(*Mini)
	if !ok {
		logrus.Errorf("FindBeanMini: failed to cast bean to *Mini")
		return nil
	}
	return bean

}

func LoadMini() baseiface.ISingleton {
	var s = NewMini()
	InjectMini(s)
	return s

}

func InjectMini(s *Mini) {

	// logrus.Debug("inject")
}
