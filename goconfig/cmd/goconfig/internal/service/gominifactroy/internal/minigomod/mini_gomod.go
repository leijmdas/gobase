package minigomod

import (
	mini "gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gominifactroy/internal/minifile"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
)

const _GOMOD = "go.mod"

type MiniGomod struct {
	basedto.BaseEntitySingle
	*mini.Mini
}

func NewMiniGomod() *MiniGomod {
	return &MiniGomod{
		Mini: mini.NewMini(),
	}

}
func (this *MiniGomod) FindRootDir() string {
	var r = this.FindRootDirGoMod()
	this.RootDir = r
	return r

}

func (this *MiniGomod) FindRootDirGoMod() string {
	return this.FindRoot(this.GetCurPath(), _GOMOD)

}
