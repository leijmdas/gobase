package minigomod

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: {{.Name}}.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-07-20 15:04:39)
	@Update 作者: leijmdas@163.com 时间(2024-07-20 15:04:39)

* *********************************************************/

const singleNameMiniGomod = "*minigomod.MiniGomod"

// init register load
func init() {
	registerBeanMiniGomod()
}

// register MiniGomod
func registerBeanMiniGomod() {
	basedi.RegisterLoadBean(singleNameMiniGomod, LoadMiniGomod)
}

func FindBeanMiniGomod() *MiniGomod {
	bean, ok := basedi.FindBean(singleNameMiniGomod).(*MiniGomod)
	if !ok {
		logrus.Errorf("FindBeanMiniGomod: failed to cast bean to *MiniGomod")
		return nil
	}
	return bean

}

func LoadMiniGomod() baseiface.ISingleton {
	var s = NewMiniGomod()
	InjectMiniGomod(s)
	return s

}

func InjectMiniGomod(s *MiniGomod) {

	// logrus.Debug("inject")
}
