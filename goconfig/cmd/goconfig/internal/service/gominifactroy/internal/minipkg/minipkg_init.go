package minipkg

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: {{.Name}}.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-07-20 15:04:39)
	@Update 作者: leijmdas@163.com 时间(2024-07-20 15:04:39)

* *********************************************************/

const singleNameMinipkg = "*minipkg.Minipkg"

// init register load
func init() {
	registerBeanMinipkg()
}

// register Minipkg
func registerBeanMinipkg() {
	basedi.RegisterLoadBean(singleNameMinipkg, LoadMinipkg)
}

func FindBeanMinipkg() *Minipkg {
	bean, ok := basedi.FindBean(singleNameMinipkg).(*Minipkg)
	if !ok {
		logrus.Errorf("FindBeanMinipkg: failed to cast bean to *Minipkg")
		return nil
	}
	return bean

}

func LoadMinipkg() baseiface.ISingleton {
	var s = NewMinipkg()
	InjectMinipkg(s)
	return s

}

func InjectMinipkg(s *Minipkg) {

	// logrus.Debug("inject")
}
