package webfile

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/consts"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cmdutils"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/cmdutils/cmdbaseutils"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/gominifactroy"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"github.com/duke-git/lancet/fileutil"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/sirupsen/logrus"
	"os"
	"strings"
)

const (
	pkgrootName = "gitee.com/leijmdas/goweb"
	PkgName     = "gitee.com/leijmdas/gobase/goconfig/common/base/goutils"

	websample = "/websample"
	webconfig = "/config"
	webdocker = "/docker"
	webdocx   = "/docx"
	webdomain = "/domain"
	webcmd    = "/cmd"
	webcmdbat = "/cmd/bat"
)

type WebFile struct {
	basedto.BaseEntitySingle

	Pkgname  string
	Pkgroot  string
	Destroot string
	Pkgdir   string
	Destdir  string

	ProjectPkgName string
}

func NewWebIni() *WebFile {
	var w = &WebFile{
		Pkgname:        PkgName,
		Pkgroot:        gominifactroy.FindBeanGominiFactroy().FindPkgRootDirOf(PkgName),
		Destroot:       gominifactroy.FindBeanGominiFactroy().FindRootDir(),
		ProjectPkgName: gominifactroy.FindBeanGominiFactroy().FindProjectPkgName(),
	}
	if w.ProjectPkgName == "" {
		w.Destroot = fileutils.GetCurPath()
		os.Setenv("BasePath", w.Destroot)
		w.ProjectPkgName = gominifactroy.NewGominiFactroy().FindProjectPkgName()
	}
	w.Pkgdir = w.Pkgroot + websample
	w.Destdir = w.Destroot + websample
	return w
}

func (this *WebFile) FindPkgRoot() string {
	return gominifactroy.FindBeanGominiFactroy().FindPkgRootDirOf(this.Pkgname)
}
func (this *WebFile) FullDestDir() string {
	if this.Pkgdir == this.Destdir {
		return this.Destdir + "new"
	}
	return this.Destdir
}

func (wi *WebFile) ChangePkgMpath(modulepath string) error {
	var projectRootPkg = gominifactroy.FindBeanGominiFactroy().FindProjectPkgName()
	goutils.Info("ChangePkg Pkgroot =", projectRootPkg)
	var files, err = gominifactroy.FindBeanGominiFactroy().FindFiles(wi.Destdir, ".go")
	if err != nil {
		goutils.Error(err)
		return err

	}
	for _, file := range files {
		goutils.Info("file =", file)
		var content, err = fileutil.ReadFileToString(file)
		if err != nil {
			goutils.Error(err)
			return err
		}
		content = strings.ReplaceAll(content, pkgrootName+modulepath, projectRootPkg+modulepath)
		err = fileutil.WriteBytesToFile(file, []byte(content))
		if err != nil {
			goutils.Error(err)
			return err
		}
	}
	return err

}
func (this *WebFile) ChangePkg() error {
	var projectRootPkg = gominifactroy.FindBeanGominiFactroy().FindProjectPkgName()
	goutils.Info("ChangePkg Pkgroot =", projectRootPkg)
	var fs, err = gominifactroy.FindBeanGominiFactroy().Finds(this.Destdir, ".go")
	if err != nil {
		goutils.Error(err)
		return err

	}
	for _, f := range fs {
		goutils.Info("f =", f)
		var content, err = fileutil.ReadFileToString(f)
		if err != nil {
			goutils.Error(err)
			return err
		}
		content = strings.ReplaceAll(content, pkgrootName+websample, projectRootPkg+websample)

		err = fileutil.WriteBytesToFile(f, []byte(content))
		if err != nil {
			goutils.Error(err)
			return err
		}

	}
	return err

}
func (this *WebFile) CopyWeb2Sample() error {
	goutils.Info("WebFile =", this.ToPrettyString())
	logrus.Info("CopyWeb2Sample ", this.FullDestDir(), "\r\n", this.Pkgdir)
	if this.IfExistWebsample() {
		fmt.Println("CopyWeb2Sample exists!")
		return errors.New("CopyWeb2Sample exists!")
	}
	if err := gfile.CopyDir(this.Pkgdir, this.FullDestDir()); err != nil {
		goutils.Error(err)
		return err
	}
	return this.ChangePkg()

}
func (this *WebFile) CopyConfig2Sample() error {
	goutils.Info("Webini =", this.ToPrettyString())

	if this.IfExistConfig() {

		fmt.Println("config already exists!")
		return nil
	}
	return gfile.CopyDir(this.Pkgroot+webconfig, this.FullDestDir()+webconfig)

}

func (this *WebFile) CopyCmd2Sample() error {
	goutils.Info("Webini  =", this.ToPrettyString())
	var webCmdPath = this.Destroot + webcmd
	if fileutil.IsExist(webCmdPath) {

		fmt.Println("cmd already exists!", webCmdPath)
		return errors.New("cmd dir already exist!")
	}

	fmt.Println("will install cmd!", webCmdPath)

	var err = gfile.CopyDir(this.Pkgroot+webcmd, this.Destroot+webcmd)
	cmdbaseutils.FindBeanCmdUtils().ReplaceCmdBatGoweb()
	gfile.CopyDir(this.Destroot+webcmdbat, this.Destroot)

	//change path
	cmdbaseutils.FindBeanCmdUtils().ReplacePkgCmd2New()
	cmdbaseutils.FindBeanCmdUtils().MoveCmdDirGoweb()

	return err

}
func (this *WebFile) CopyDocker2Sample() error {
	goutils.Info("copyDocker Webini  =", this.ToPrettyString())
	var pathdocker = this.Destroot + webdocker
	if fileutil.IsExist(pathdocker) {

		fmt.Println("docker already exists!", pathdocker)
		return errors.New("docker already exist!")
	}

	fmt.Println("will install docker!", pathdocker)
	return gfile.CopyDir(this.Pkgroot+webdocker, this.Destroot+webdocker)

}
func (this *WebFile) CopyDocx2Sample() error {
	goutils.Info("copyDocker Webini  =", this.ToPrettyString())
	var pathdocx = this.Destroot + webdocx
	if fileutil.IsExist(pathdocx) {

		fmt.Println("docker already exists!", pathdocx)
		return errors.New("docker already exist!")
	}

	fmt.Println("will install docker!", pathdocx)
	return gfile.CopyDir(this.Pkgroot+webdocx, this.Destroot+webdocx)

}
func (wf *WebFile) CopyDomain2Sample() error {
	var err = wf.Copy2Sample(consts.WebDomain)
	if err != nil {
		return err
	}

	cmdutils.NewDomainUtils().ReplaceAllPkg()
	return nil
}

// copy client
func (this *WebFile) IniCli() {
	goutils.Info("Pkgname root =", this.FindPkgRoot())
}

// copy server
func (this *WebFile) IniServer() {
	goutils.Info("Pkgname root =", this.FindPkgRoot())
}

func (this *WebFile) IfExistConfig() bool {
	return fileutils.CheckFileExist(this.Destroot + "/config/app-env.yml")
}
func (this *WebFile) IfExistWebsample() bool {

	return fileutils.CheckFileExist(this.Destroot + "/websample/webstart.go")
}

func (this *WebFile) CopyConfigIfNotExist() bool {

	if !this.IfExistConfig() {
		if err := gfile.CopyDir(this.Pkgroot+webconfig, this.Destroot+webconfig); err != nil {
			goutils.Error(err)
			return true
		}
	}

	return false

}
func (this *WebFile) Ls() {
	logrus.Info(this)
}

func (wi *WebFile) Copy2Sample(path string) error {
	goutils.Info("Copy2Sample   =", wi.ToPrettyString())
	goutils.Info(wi.Pkgroot+path, wi.Destroot+path)
	var genPath = wi.Destroot + path
	if fileutil.IsExist(genPath) {
		fmt.Println("path already exists!", genPath)
		return errors.New("path already exist!")
	}

	if err := gfile.CopyDir(wi.Pkgroot+path, wi.Destroot+path); err != nil {
		goutils.Error(err)
		return err
	}
	return wi.ChangePkgMpath(path)

}
