package webfile

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: web_file_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-07 18:28:48)
	@Update 作者: leijmdas@163.com 时间(2024-09-07 18:28:48)

* *********************************************************/

const singleNameWebFile = "*webfile.WebFile-c067a823c2c441ab836f2503377cd183"

// init register load
func init() {
	registerBeanWebFile()
}

// register WebFile
func registerBeanWebFile() {
	basedi.RegisterLoadBean(singleNameWebFile, LoadWebFile)
}

func FindBeanWebFile() *WebFile {
	bean, ok := basedi.FindBean(singleNameWebFile).(*WebFile)
	if !ok {
		logrus.Errorf("FindBeanWebFile: failed to cast bean to *WebFile")
		return nil
	}
	return bean

}

func LoadWebFile() baseiface.ISingleton {
	var s = NewWebIni()
	InjectWebFile(s)
	return s

}

func InjectWebFile(s *WebFile) {

	// // logrus.Debug("inject")
}
