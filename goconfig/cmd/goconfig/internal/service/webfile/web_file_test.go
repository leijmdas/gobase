package webfile

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"github.com/gogf/gf/v2/os/gfile"
	"testing"
)

func Test001_inicli(t *testing.T) {
	FindBeanWebFile().IniCli()
	FindBeanWebFile().CopyWeb2Sample()
}
func Test002_iniserver(t *testing.T) {
	FindBeanWebFile().IniServer()

	err := gfile.CopyDir("../server/webcommon", "../server/webcommon/webcommon")
	if err != nil {
		goutils.Error(err)
	}
}
func Test003_inicli(t *testing.T) {
	FindBeanWebFile().IniCli()
	FindBeanWebFile().CopyWeb2Sample()
}
