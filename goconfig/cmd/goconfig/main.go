package main

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/menu"
	"gitee.com/leijmdas/gobase/goconfig/cmd/goconfig/internal/service/webfile"
	"github.com/duke-git/lancet/fileutil"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/sirupsen/logrus"
	"os"
)

func rmCfg() {
	var cfgfile = fileutils.FindRootDir() + "/websample/config"
	if fileutils.CheckFileExist(cfgfile) {
		fmt.Println("rm websample/config...")
		gfile.Remove(cfgfile)
	}

}
func FindRootMod() error {
	if fileutil.IsExist(fileutils.GetCurPath() + "/go.mod") {
		os.Setenv(baseconsts.IchubBasePath, fileutils.GetCurPath())
		return nil
	}
	return errors.New("no found gomod rootdir!")
}
func main() {
	goutils.Info("goweb")
	if err := FindRootMod(); err != nil {
		logrus.Error(err)
		panic("please init go.mod and enter mod path!!")
	}
	if webfile.FindBeanWebFile().CopyConfigIfNotExist() {
		fmt.Println("config not exist! copy finished and redo!")
		rmCfg()
		fmt.Println("config not exist! copy finished and redo!")

	}

	menu.Execute()
}
