package configdomain

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
)

type ConfigAgg struct {
	// Bypass *baseconfig.BypassDto
	basedto.BaseEntity

	DbClient      *baseconfig.DbClientDto      `json:"db_client,omitempty"`
	Datasource    *baseconfig.DbClientDto      `json:"datasource"`
	WebClient     *baseconfig.WebClientDto     `json:"web_client"`
	Gateway       *baseconfig.GatewayDto       `json:"gateway,omitempty"`
	ElasticClient *baseconfig.ElasticClientDto `json:"elastic_client"`
	RpcServer     *baseconfig.RpcServerDto     `json:"rpc_server"`
	Gocenter      *baseconfig.GocenterDto      `json:"gocenter,omitempty"`
	RedisClient   *baseconfig.RedisClientDto   `json:"redis_client"`
	SwaggerClient *baseconfig.SwaggerClientDto `json:"swagger_client,omitempty"`
	FactroyClient *baseconfig.FactroyClientDto `json:"factroy_client,omitempty"`
	NatsClient    *baseconfig.NatsClientDto    `json:"nats_client,omitempty"`
	WebServer     *baseconfig.WebServerDto     `json:"web_server,omitempty"`
}

func NewConfigAgg() *ConfigAgg {
	var ca = &ConfigAgg{}
	ca.InitProxy(ca)
	return ca
}
