package configdomain

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameConfigAgg = "ConfigAgg"

// init
// register
// load
func init() {
	registerBeanConfigAgg()
}

// register ConfigAgg
func registerBeanConfigAgg() {
	basedi.RegisterLoadBean(singleNameConfigAgg, LoadConfigAgg)
}

func FindBeanConfigAgg() *ConfigAgg {
	return basedi.FindBean(singleNameConfigAgg).(*ConfigAgg)
}

func LoadConfigAgg() baseiface.ISingleton {
	return NewConfigAgg()
}
