package configdto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/encrypt"
	"sync"
)

/*
@Title    文件名称: config_context.go
@Description  描述: 配置上下文

@Author  作者: leijianming@163.com  时间(2024-03-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-03-18 22:38:21)
*/
var Context = NewConfigContext()
var lock sync.RWMutex

type ConfigContext struct {
	basedto.BaseEntity
	env_vars map[string]any
	encdec   baseiface.IEncDec
}

func NewConfigContext() *ConfigContext {
	return &ConfigContext{
		encdec: encrypt.FindBeanEncDec(),
	}
}

func (this *ConfigContext) Env_vars() map[string]any {
	return this.env_vars
}

func (this *ConfigContext) SetEnv_vars(env_vars map[string]any) {
	this.env_vars = env_vars
}

func (this *ConfigContext) Encdec() baseiface.IEncDec {
	return this.encdec
}

func (this *ConfigContext) SetEncdec(encdec baseiface.IEncDec) {
	this.encdec = encdec
}

func (this *ConfigContext) RegisterEncDec(encdec baseiface.IEncDec) {
	lock.Lock()
	this.encdec = encdec
	lock.Unlock()
}
func (this *ConfigContext) Ini() {

}

func (this *ConfigContext) FindVar(key string) {

}

func (this *ConfigContext) PutVar(key string, value any) {

}
