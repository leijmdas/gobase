package configdto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameConfigContext = "ConfigContext"

// init
// register
// load
func init() {
	registerBeanConfigContext()
}

// register ConfigContext
func registerBeanConfigContext() {
	basedi.RegisterLoadBean(singleNameConfigContext, LoadConfigContext)
}

func FindBeanConfigContext() *ConfigContext {
	return basedi.FindBean(singleNameConfigContext).(*ConfigContext)
}

func LoadConfigContext() baseiface.ISingleton {
	return NewConfigContext()
}
