package configdto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameConfigItem = "ConfigItem"

// init
// register
// load
func init() {
	registerBeanConfigItem()
}

// register ConfigItem
func registerBeanConfigItem() {
	basedi.RegisterLoadBean(singleNameConfigItem, LoadConfigItem)
}

func FindBeanConfigItem() *ConfigItem {
	return basedi.FindBean(singleNameConfigItem).(*ConfigItem)
}

func LoadConfigItem() baseiface.ISingleton {
	return &ConfigItem{}
}
