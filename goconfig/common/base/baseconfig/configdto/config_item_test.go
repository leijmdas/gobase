package configdto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func init() {
	ichublog.InitLogrus()
}

func Test0001_item_notSetEnv(t *testing.T) {
	var item = NewConfigItem("ETCD_HOSTURL", "${ETCD_HOSTURL:huawei.akunlong.top:2379}")
	item.ParseValue()
	item.Log()
}

func Test0002_item_setEnv(t *testing.T) {
	os.Setenv("ETCD_HOSTURL", "http://127.0.0.1:2379")
	var item = NewConfigItem("hosturl", "${ETCD_HOSTURL:huawei.akunlong.top:2379}")
	item.ParseValue()
	item.Log()
}

func Test0003_item_checkNoSetEnv(t *testing.T) {
	os.Setenv("ETCD_HOSTURL", "http://127.0.0.1:2379")
	var item = NewConfigItem("hosturl", "huawei.akunlong.top:2379")
	item.ParseValue()
	item.Log()
	assert.Equal(t, true, item.Check())

}
func Test0004_item_checkSetEnv(t *testing.T) {
	os.Setenv("ETCD_HOSTURL", "http://127.0.0.1:2379")
	var item = NewConfigItem("ETCD_HOSTURL", "${ETCD_HOSTURL:huawei.akunlong.top:2379}")
	item.ParseValue()
	item.Log()
	assert.Equal(t, true, item.Check())

}

//go get -u gitee.com/leijmdas/gobaseichubengine-client
