package configdto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/morrisxyang/xreflect"
	"github.com/sirupsen/logrus"
	"reflect"
)

type IchubClientDto struct {
	basedto.BaseEntity
}

func NewIchubClientDto() *IchubClientDto {
	return &IchubClientDto{}
}

func (this *IchubClientDto) ParseValue(key, value string) string {
	item := &ConfigItem{
		Key:   key,
		Value: value,
	}
	return item.ParseValue().EndValue
}
func (this *IchubClientDto) Parse() configiface.IchubClient {
	var proxy = this.Proxy()
	this.ParseSome(proxy.Some())
	return this.Proxy().Some().(configiface.IchubClient)
}
func (this *IchubClientDto) ParseSome(some any) {
	if some == nil {
		logrus.Error("some is nil!")
		return
	}
	Values, _ := xreflect.Fields(some)
	for k, v := range Values {

		if v.Kind() == reflect.String {

			var vv = this.ParseValue(k, v.String())
			v.SetString(vv)

			xreflect.SetField(this, k, v)
		} else if v.Kind() == reflect.Struct {
			this.ParseStruct(v)
		}

	}

}
func (this *IchubClientDto) ParseStruct(value reflect.Value) {

	Values, _ := xreflect.Fields(value)
	for k, v := range Values {

		if v.Kind() == reflect.String {

			var vv = this.ParseValue(k, v.String())
			v.SetString(vv)

			xreflect.SetField(value, k, v)
		}
		if v.Kind() == reflect.Struct {
			this.ParseStruct(v)
		}

	}
}

func (this *IchubClientDto) ParseValues(key string, values ...*string) {

	for _, v := range values {

		*v = this.ParseValue("key", *v)
	}
	logrus.Info(values)
}
