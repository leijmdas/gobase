package configdto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameIchubClientDto = "IchubClientDto"

// init
// register
// load
func init() {
	registerBeanIchubClientDto()
}

// register IchubClientDto
func registerBeanIchubClientDto() {
	basedi.RegisterLoadBean(singleNameIchubClientDto, LoadIchubClientDto)
}

func FindBeanIchubClientDto() *IchubClientDto {
	return basedi.FindBean(singleNameIchubClientDto).(*IchubClientDto)
}

func LoadIchubClientDto() baseiface.ISingleton {
	return NewIchubClientDto()
}
