package configiface

type IchubClient interface {
	ParseValue(key, value string) string
	CopyWithOption(from any)
	Parse() IchubClient
}
