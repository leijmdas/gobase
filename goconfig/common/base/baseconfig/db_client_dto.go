package baseconfig

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
)

type DbClientDto struct {
	configdto.IchubClientDto `json:"-"`

	Dbtype string `json:"dbtype"`
	Dbname string `json:"dbname"`
	Host   string `json:"host"`
	Port   string `json:"port"`

	Username string `json:"username"`
	Password string `json:"password"`
	Sslmode  string `json:"sslmode"`
	Charset  string `json:"charset"`

	GormClient *GormClientDto `json:"gorm_client"`
}

func NewDbClientDto() *DbClientDto {
	var dbClient = &DbClientDto{}
	dbClient.InitProxy(dbClient)
	return dbClient
}

func (this *DbClientDto) MakeMysqlUrl() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&mb4&parseTime=True&loc=Local",
		this.Username, this.Password, this.Host, this.Port, this.Dbname)
}

func (this *DbClientDto) MakePostgresUrl() string {
	return fmt.Sprintf("postgresql://%s:%s@tcp(%s:%s)/%s?charset=utf8&mb4&parseTime=True&loc=Local&sslmode=%s",
		this.Username, this.Password, this.Host, this.Port, this.Dbname, this.Sslmode)
}

func (this *DbClientDto) MakeCockroachUrl() string {
	return fmt.Sprintf("postgresql://%s:%s@tcp(%s:%s)/%s?charset=utf8&mb4&parseTime=True&loc=Local&sslmode=%s",
		this.Username, this.Password, this.Host, this.Port, this.Dbname, this.Sslmode)
}
func (this *DbClientDto) IsMysql() bool {
	return this.Dbtype == baseconsts.DB_TYPE_MYSQL

}
func (this *DbClientDto) IfMysql() bool {
	return this.Dbtype == baseconsts.DB_TYPE_MYSQL

}
func (this *DbClientDto) IfPostgres() bool {
	return this.Dbtype == baseconsts.DB_TYPE_POSTGRES

}
func (this *DbClientDto) IfCockdb() bool {
	return this.Dbtype == baseconsts.DB_TYPE_COCKROACH

}
func (this *DbClientDto) CacheKey() string {
	return this.Dbtype + "::" + this.Dbname + this.Host + this.Port

}
