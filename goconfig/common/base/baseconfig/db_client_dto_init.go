package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: db_client_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameDbClientDto = "*baseconfig.DbClientDto-499c3596e7064dc8ab9597ddce45b164"

// init register load
func init() {
	registerBeanDbClientDto()
}

// register DbClientDto
func registerBeanDbClientDto() {
	basedi.RegisterLoadBean(singleNameDbClientDto, LoadDbClientDto)
}

func FindBeanDbClientDto() *DbClientDto {
	bean, ok := basedi.FindBean(singleNameDbClientDto).(*DbClientDto)
	if !ok {
		logrus.Errorf("FindBeanDbClientDto: failed to cast bean to *DbClientDto")
		return nil
	}
	return bean

}

func LoadDbClientDto() baseiface.ISingleton {
	var s = NewDbClientDto()
	InjectDbClientDto(s)
	return s

}

func InjectDbClientDto(s *DbClientDto) {

	// goutils.Debug("inject")
}
