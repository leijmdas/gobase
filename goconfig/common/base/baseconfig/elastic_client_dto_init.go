package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: elastic_client_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameElasticClientDto = "*baseconfig.ElasticClientDto-c075008fe85943f69775b6fe555f162d"

// init register load
func init() {
	registerBeanElasticClientDto()
}

// register ElasticClientDto
func registerBeanElasticClientDto() {
	basedi.RegisterLoadBean(singleNameElasticClientDto, LoadElasticClientDto)
}

func FindBeanElasticClientDto() *ElasticClientDto {
	bean, ok := basedi.FindBean(singleNameElasticClientDto).(*ElasticClientDto)
	if !ok {
		logrus.Errorf("FindBeanElasticClientDto: failed to cast bean to *ElasticClientDto")
		return nil
	}
	return bean

}

func LoadElasticClientDto() baseiface.ISingleton {
	var s = NewElasticClientDto()
	InjectElasticClientDto(s)
	return s

}

func InjectElasticClientDto(s *ElasticClientDto) {

	// goutils.Debug("inject")
}
