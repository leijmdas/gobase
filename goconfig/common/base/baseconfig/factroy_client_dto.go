package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type FactroyClientDto struct {
	basedto.BaseEntitySingle

	Author string

	PkgNow    string
	PkgNew    string
	PkgApp    string
	PkgAppNew string

	Time2Int  string
	JsonCamel bool
}

func NewFactroyClientDto() *FactroyClientDto {

	var this = &FactroyClientDto{}
	this.InitProxy(this)
	return this
}
