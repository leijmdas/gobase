package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameFactroyClientDto = "FactroyClientDto"

// init register  load
func init() {
	registerBeanFactroyClientDto()
}

// register FactroyClientDto
func registerBeanFactroyClientDto() {
	basedi.RegisterLoadBean(singleNameFactroyClientDto, LoadFactroyClientDto)
}

func FindBeanFactroyClientDto() *FactroyClientDto {
	return basedi.FindBean(singleNameFactroyClientDto).(*FactroyClientDto)
}

func LoadFactroyClientDto() baseiface.ISingleton {
	return NewFactroyClientDto()
}
