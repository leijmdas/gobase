package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
)

/*
@Title    文件名称: gateway_dto.go
@Description  描述: GatewayDto配置信息

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
const SEVICE_NAME_NONE = "web.none.com"

// - path: /datadict
// serviceId: web.platform.com
type RouteDto struct {
	ServiceId string `json:"service_id"`
	Path      string `json:"path"`
}
type BypassDto struct {
	Path string `json:"path"`
}
type GatewayDto struct {
	ByPass []BypassDto `json:"by_pass"`
	Routes []RouteDto  `json:"routes"`

	//- path: /datadict
	//serviceId: web.platform.com
	MapPath2Service map[string]string `json:"map_path_2_service,omitempty"`

	configdto.IchubClientDto `json:"-"`
}

func NewGatewayDto() *GatewayDto {
	var dto = &GatewayDto{
		MapPath2Service: map[string]string{},
	}
	dto.InitProxy(dto)
	return dto
}

// - path: /datadict
// serviceId: web.platform.com
func (this *GatewayDto) ToMap() *GatewayDto {
	if this.MapPath2Service == nil {
		this.MapPath2Service = make(map[string]string)
	}
	for _, v := range this.Routes {
		this.MapPath2Service[v.Path] = v.ServiceId
	}
	return this
}
func (this *GatewayDto) FindServiceName(path string) string {

	//path: /datadict
	//serviceId: web.platform.com
	service, found := this.MapPath2Service[path]
	if found {
		ichublog.Log("find ServiceName byPath is ", service, path)
		return service
	}

	golog.Info("find ServiceName byPath not found!  ", path)
	return SEVICE_NAME_NONE
}

func (this *GatewayDto) Bypass(path string) bool {
	for _, v := range this.ByPass {
		if v.Path == path {
			return true
		}
	}

	return false
}
