package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gateway_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameGatewayDto = "*baseconfig.GatewayDto-326443f8d33341f0898b11054ba34967"

// init register load
func init() {
	registerBeanGatewayDto()
}

// register GatewayDto
func registerBeanGatewayDto() {
	basedi.RegisterLoadBean(singleNameGatewayDto, LoadGatewayDto)
}

func FindBeanGatewayDto() *GatewayDto {
	bean, ok := basedi.FindBean(singleNameGatewayDto).(*GatewayDto)
	if !ok {
		logrus.Errorf("FindBeanGatewayDto: failed to cast bean to *GatewayDto")
		return nil
	}
	return bean

}

func LoadGatewayDto() baseiface.ISingleton {
	var s = NewGatewayDto()
	InjectGatewayDto(s)
	return s

}

func InjectGatewayDto(s *GatewayDto) {

	// goutils.Debug("inject")
}
