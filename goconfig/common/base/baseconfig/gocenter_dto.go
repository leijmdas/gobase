package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type GocenterDto struct {
	basedto.BaseEntity
	Centertype string
	Hosturl    string
}

func NewGocenterDto() *GocenterDto {
	return &GocenterDto{}
}
