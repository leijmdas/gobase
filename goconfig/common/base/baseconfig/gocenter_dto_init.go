package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameGocenterDto = "GocenterDto"

// init
// register
// load
func init() {
	registerBeanGocenterDto()
}

// register GocenterDto
func registerBeanGocenterDto() {
	basedi.RegisterLoadBean(singleNameGocenterDto, LoadGocenterDto)
}

func FindBeanGocenterDto() *GocenterDto {
	return basedi.FindBean(singleNameGocenterDto).(*GocenterDto)
}

func LoadGocenterDto() baseiface.ISingleton {
	return NewGocenterDto()
}
