package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"

type GormClientDto struct {
	configdto.IchubClientDto `json:"-"`

	Debug  string `json:"debug"`
	DbType string `json:"db_type"`
	Enable string `json:"enabled"`

	MaxLifetime  string `json:"max_lifetime"`
	MaxOpenConns string `json:"max_open_conns"`
	MaxIdleConns string `json:"max_idle_conns"`
}

func NewGormClientDto() *GormClientDto {
	var dto = &GormClientDto{}
	dto.InitProxy(dto)
	return dto
}

//func (this *GormClientDto) Parse() *GormClientDto {
//	//this.Debug = this.ParseValue("Debug", this.Debug)
//	//this.DbType = this.ParseValue("DbType", this.DbType)
//	//this.Enable = this.ParseValue("Enabled", this.Enable)
//	//
//	//this.MaxLifetime = this.ParseValue("MaxLifetime", this.MaxLifetime)
//	//this.MaxOpenConns = this.ParseValue("MaxOpenConns", this.MaxOpenConns)
//	//this.MaxIdleConns = this.ParseValue("MaxIdleConns", this.MaxIdleConns)
//
//	this.IchubClientDto.Parse()
//	return this
//}
