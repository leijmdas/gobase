package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gorm_client_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameGormClientDto = "*baseconfig.GormClientDto-e33f2930f17f4a21b4fd3dfdc50da7a4"

// init register load
func init() {
	registerBeanGormClientDto()
}

// register GormClientDto
func registerBeanGormClientDto() {
	basedi.RegisterLoadBean(singleNameGormClientDto, LoadGormClientDto)
}

func FindBeanGormClientDto() *GormClientDto {
	bean, ok := basedi.FindBean(singleNameGormClientDto).(*GormClientDto)
	if !ok {
		logrus.Errorf("FindBeanGormClientDto: failed to cast bean to *GormClientDto")
		return nil
	}
	return bean

}

func LoadGormClientDto() baseiface.ISingleton {
	var s = NewGormClientDto()
	InjectGormClientDto(s)
	return s

}

func InjectGormClientDto(s *GormClientDto) {

	// goutils.Debug("inject")
}
