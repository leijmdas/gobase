package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"

type NatsClientDto struct {
	configdto.IchubClientDto

	Url       string `json:"url"`
	Timeout   string `json:"timeout"`
	Subscribe struct {
		TopicSync  string
		TopicAsync string
	}
}

func NewNatsClientDto() *NatsClientDto {
	var ds = &NatsClientDto{}
	ds.InitProxy(ds)
	return ds
}
