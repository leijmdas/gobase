package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: nats_client_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameNatsClientDto = "*baseconfig.NatsClientDto-b5c7ec6ed55a42a19b94bb951f42b1b0"

// init register load
func init() {
	registerBeanNatsClientDto()
}

// register NatsClientDto
func registerBeanNatsClientDto() {
	basedi.RegisterLoadBean(singleNameNatsClientDto, LoadNatsClientDto)
}

func FindBeanNatsClientDto() *NatsClientDto {
	bean, ok := basedi.FindBean(singleNameNatsClientDto).(*NatsClientDto)
	if !ok {
		logrus.Errorf("FindBeanNatsClientDto: failed to cast bean to *NatsClientDto")
		return nil
	}
	return bean

}

func LoadNatsClientDto() baseiface.ISingleton {
	var s = NewNatsClientDto()
	InjectNatsClientDto(s)
	return s

}

func InjectNatsClientDto(s *NatsClientDto) {

	// goutils.Debug("inject")
}
