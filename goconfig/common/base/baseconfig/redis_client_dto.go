package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"

type RedisClientDto struct {
	configdto.IchubClientDto

	Addr     string `json:"addr"`     //  "192.168.14.58:6379",
	Password string `json:"password"` // "KiZ9dJBSk1cju",
	DB       int    `json:"DB"`
}

func NewRedisClientDto() *RedisClientDto {
	var dto = &RedisClientDto{}

	dto.InitProxy(dto)

	return dto
}

func (this *RedisClientDto) Parse() *RedisClientDto {
	this.Addr = this.ParseValue("Addr", this.Addr)
	this.Password = this.ParseValue("Password", this.Password)

	return this
}
