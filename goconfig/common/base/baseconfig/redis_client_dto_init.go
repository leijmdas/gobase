package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: redis_client_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameRedisClientDto = "*baseconfig.RedisClientDto-831e9263ed2746d6adce1bf07b7fbba6"

// init register load
func init() {
	registerBeanRedisClientDto()
}

// register RedisClientDto
func registerBeanRedisClientDto() {
	basedi.RegisterLoadBean(singleNameRedisClientDto, LoadRedisClientDto)
}

func FindBeanRedisClientDto() *RedisClientDto {
	bean, ok := basedi.FindBean(singleNameRedisClientDto).(*RedisClientDto)
	if !ok {
		logrus.Errorf("FindBeanRedisClientDto: failed to cast bean to *RedisClientDto")
		return nil
	}
	return bean

}

func LoadRedisClientDto() baseiface.ISingleton {
	var s = NewRedisClientDto()
	InjectRedisClientDto(s)
	return s

}

func InjectRedisClientDto(s *RedisClientDto) {

	// goutils.Debug("inject")
}
