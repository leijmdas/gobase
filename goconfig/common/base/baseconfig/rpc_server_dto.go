package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"

/*
@Title    文件名称: rpc_server_dto.go
@Description  描述: Rpc服务配置信息

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type RpcServerDto struct {
	configdto.IchubClientDto
	EtcdHost   string `json:"etcd_host"`
	ServerName string `json:"server_name"`
	ClientName string `json:"-"`
	ServerPort int    `json:"server_port"`
}

func NewRpcServerDto() *RpcServerDto {
	return &RpcServerDto{}
}

func (this *RpcServerDto) Parse() *RpcServerDto {
	this.EtcdHost = this.ParseValue("EtcdHost", this.EtcdHost)
	this.ServerName = this.ParseValue("ServerName", this.ServerName)
	this.ClientName = this.ParseValue("ClientName", this.ClientName)

	return this
}
