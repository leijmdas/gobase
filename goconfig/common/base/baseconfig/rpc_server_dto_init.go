package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: rpc_server_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameRpcServerDto = "*baseconfig.RpcServerDto-06e6695b79794e508afd557cd58d2200"

// init register load
func init() {
	registerBeanRpcServerDto()
}

// register RpcServerDto
func registerBeanRpcServerDto() {
	basedi.RegisterLoadBean(singleNameRpcServerDto, LoadRpcServerDto)
}

func FindBeanRpcServerDto() *RpcServerDto {
	bean, ok := basedi.FindBean(singleNameRpcServerDto).(*RpcServerDto)
	if !ok {
		logrus.Errorf("FindBeanRpcServerDto: failed to cast bean to *RpcServerDto")
		return nil
	}
	return bean

}

func LoadRpcServerDto() baseiface.ISingleton {
	var s = NewRpcServerDto()
	InjectRpcServerDto(s)
	return s

}

func InjectRpcServerDto(s *RpcServerDto) {

	// goutils.Debug("inject")
}
