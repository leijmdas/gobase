package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"

type SoftwareDto struct {
	configdto.IchubClientDto
	Env       string
	WebPrefix string
	Author    string
	Corp      string
	Salt      string
	Version   string
	Name      string
}

func NewSoftwareDto() *SoftwareDto {
	return &SoftwareDto{}
}
