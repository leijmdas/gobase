package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: software_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameSoftwareDto = "*baseconfig.SoftwareDto-30c0129980584f24b232c35c582e3548"

// init register load
func init() {
	registerBeanSoftwareDto()
}

// register SoftwareDto
func registerBeanSoftwareDto() {
	basedi.RegisterLoadBean(singleNameSoftwareDto, LoadSoftwareDto)
}

func FindBeanSoftwareDto() *SoftwareDto {
	bean, ok := basedi.FindBean(singleNameSoftwareDto).(*SoftwareDto)
	if !ok {
		logrus.Errorf("FindBeanSoftwareDto: failed to cast bean to *SoftwareDto")
		return nil
	}
	return bean

}

func LoadSoftwareDto() baseiface.ISingleton {
	var s = NewSoftwareDto()
	InjectSoftwareDto(s)
	return s

}

func InjectSoftwareDto(s *SoftwareDto) {

	// goutils.Debug("inject")
}
