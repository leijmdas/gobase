package baseconfig

import "gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"

type SwaggerClientDto struct {
	configdto.IchubClientDto `json:"-"`
	Host                     string `json:"host"`
	BasePath                 string `json:"base_path"`
	Version                  string `json:"version"`
	Title                    string `json:"title"`
	Enable                   string `json:"enable"`
}

func NewSwaggerClientDto() *SwaggerClientDto {
	var dto = &SwaggerClientDto{}
	dto.InitProxy(dto)
	return dto
}

//func (this *SwaggerClientDto) Parse() *SwaggerClientDto {
//	//this.Host = this.ParseValue("Host", this.Host)
//	//this.BasePath = this.ParseValue("BasePath", this.BasePath)
//	//this.Version = this.ParseValue("Version", this.Version)
//	//this.Title = this.ParseValue("Title", this.Title)
//	//this.Enable = this.ParseValue("Enable", this.Enable)
//	this.IchubClientDto.Parse()
//	return this
//}
