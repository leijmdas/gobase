package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: swagger_client_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameSwaggerClientDto = "*baseconfig.SwaggerClientDto-a0776bb8241a4980bd94832a10f6cb2c"

// init register load
func init() {
	registerBeanSwaggerClientDto()
}

// register SwaggerClientDto
func registerBeanSwaggerClientDto() {
	basedi.RegisterLoadBean(singleNameSwaggerClientDto, LoadSwaggerClientDto)
}

func FindBeanSwaggerClientDto() *SwaggerClientDto {
	bean, ok := basedi.FindBean(singleNameSwaggerClientDto).(*SwaggerClientDto)
	if !ok {
		logrus.Errorf("FindBeanSwaggerClientDto: failed to cast bean to *SwaggerClientDto")
		return nil
	}
	return bean

}

func LoadSwaggerClientDto() baseiface.ISingleton {
	var s = NewSwaggerClientDto()
	InjectSwaggerClientDto(s)
	return s

}

func InjectSwaggerClientDto(s *SwaggerClientDto) {

	// goutils.Debug("inject")
}
