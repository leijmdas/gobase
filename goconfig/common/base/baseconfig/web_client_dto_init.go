package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: web_client_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameWebClientDto = "*baseconfig.WebClientDto-b80bbe40c992488c8a792edb5f85780c"

// init register load
func init() {
	registerBeanWebClientDto()
}

// register WebClientDto
func registerBeanWebClientDto() {
	basedi.RegisterLoadBean(singleNameWebClientDto, LoadWebClientDto)
}

func FindBeanWebClientDto() *WebClientDto {
	bean, ok := basedi.FindBean(singleNameWebClientDto).(*WebClientDto)
	if !ok {
		logrus.Errorf("FindBeanWebClientDto: failed to cast bean to *WebClientDto")
		return nil
	}
	return bean

}

func LoadWebClientDto() baseiface.ISingleton {
	var s = NewWebClientDto()
	InjectWebClientDto(s)
	return s

}

func InjectWebClientDto(s *WebClientDto) {

	// goutils.Debug("inject")
}
