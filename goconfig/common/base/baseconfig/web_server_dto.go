package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
)

/*
@Title    文件名称: web_server_dto.go
@Description  描述: Web服务配置信息

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type WebServerDto struct {
	EtcdHost   string `json:"etcd_host"`
	ServerName string `json:"server_name"`
	ServerPort int    `json:"server_port"`

	ServerIp string `json:"server_ip,omitempty"`
	configdto.IchubClientDto
}

func NewWebServerDto() *WebServerDto {
	var w = &WebServerDto{}
	w.InitProxy(w)
	w.InitProxy(w.IchubClientDto)
	return w
}

func (this *WebServerDto) String() string {
	return jsonutils.ToJsonPretty(this)
}

func (this *WebServerDto) Parse() *WebServerDto {
	this.EtcdHost = this.ParseValue("EtcdHost", this.EtcdHost)
	this.ServerName = this.ParseValue("ServerName", this.ServerName)
	//this.ServerPort = this.parseValue("KEY", this.ServerPort)

	return this
}
