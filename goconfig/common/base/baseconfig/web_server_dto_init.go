package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: web_server_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 19:57:39)

* *********************************************************/

const singleNameWebServerDto = "*baseconfig.WebServerDto-b9d2b4bf966b4b348816b102bc77aeb6"

// init register load
func init() {
	registerBeanWebServerDto()
}

// register WebServerDto
func registerBeanWebServerDto() {
	basedi.RegisterLoadBean(singleNameWebServerDto, LoadWebServerDto)
}

func FindBeanWebServerDto() *WebServerDto {
	bean, ok := basedi.FindBean(singleNameWebServerDto).(*WebServerDto)
	if !ok {
		logrus.Errorf("FindBeanWebServerDto: failed to cast bean to *WebServerDto")
		return nil
	}
	return bean

}

func LoadWebServerDto() baseiface.ISingleton {
	var s = NewWebServerDto()
	InjectWebServerDto(s)
	return s

}

func InjectWebServerDto(s *WebServerDto) {

	// goutils.Debug("inject")
}
