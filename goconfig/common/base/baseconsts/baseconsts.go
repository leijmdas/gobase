package baseconsts

type RetCode int

const (
	ConfigfileAppEnv    = "/config/app-env.yml"
	DEFINE_ENV_PATHFILE = ConfigfileAppEnv

	ENV_EMPTY   = ""
	ENV_DEFAULT = "default"
	ENV_DEV     = "dev"
	ENV_TEST    = "test"
	ENV_RELEASE = "release"
	ENV_MASTER  = "master"
)
const (
	JsonContentType = "application/json"

	AUTH_HEADER_TOKEN     = "Access-Token"
	AUTH_HEADER_APIKEY    = "Access-ApiKey"
	WEBCLIENT_TYPE_ENGINE = iota
	WEBCLIENT_TYPE_DBAGENT
	WEBCLIENT_TYPE_ESAGENT

	DB_TYPE_MYSQL     = "mysql"
	DB_TYPE_POSTGRES  = "postgres"
	DB_TYPE_COCKROACH = "cockroach"

	//path file os
	IchubBasePath    = "BasePath"
	IchubBasePathPkg = "BasePathPkg"

	//code
	RetCode_SUCCESS RetCode = 200
	RetCode_ERROR   RetCode = 500
	//page size
	PAGE_CURRENT  = 1
	PAGE_SIZE_ALL = -1

	PAGE_SIZE_ZERO = 0

	PAGE_SIZE_DEFAULT = 20

	PAGE_SIZE_MAX = 500

	//datetime
	DATETIME_UTC = iota
	DATETIME_GMT8

	ZERO_TIME_INT64 = -62135596800

	FormatDate     = "2006-01-02"
	FormatDateTime = "2006-01-02 15:04:05"
	FormatUTCTime  = "2006-01-02T15:04:05.000Z"

	//baseconfig enc
	ENC_FLAG = "enc(*)"
)
