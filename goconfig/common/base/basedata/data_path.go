package basedata

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"os"
)

type DataPath struct {
	basedto.BaseEntitySingle
	RootDir      string
	DataPath     string
	DataInPath   string
	DataInPathEs string
	DataOutPath  string
	DataCode     string
	DataOutCc    string
}

func NewDataPath() *DataPath {
	var t = &DataPath{}
	t.init()
	t.InitProxy(t)
	return t
}

func (this *DataPath) init() {
	this.RootDir = fileutils.FindRootDir()
	this.DataPath = this.RootDir + "/data"
	this.DataInPath = this.DataPath + "/input"
	this.DataInPathEs = this.DataInPath + "/es"
	this.DataOutCc = this.DataPath + "/cc"
	this.DataCode = this.DataPath + "/code/"
	this.DataOutPath = this.DataPath + "/output"
	this.MakePathNotExist()
}

func (this *DataPath) WriteFile(filename string, model any) error {
	var bytes, _ = jsonutils.ToJsonBytes(model)
	return fileutils.WriteBytesFile(this.DataOutPath+filename, bytes)

}

func (this *DataPath) ReadFile(filename string) (string, error) {
	return fileutils.ReadFileToString(this.DataInPath + filename)
}
func (this *DataPath) ReadEsFile(filename string) (string, error) {
	return fileutils.ReadFileToString(this.DataInPathEs + filename)
}
func (this *DataPath) WriteFiles(req any, res any) error {

	this.WriteFile("req.json", req)
	return this.WriteFile("res.json", res)

}
func (self *DataPath) MakePathNotExist() error {
	var entity = self.DataCode + "entity"
	var esentity = self.DataCode + "esentity"

	var paths = []string{esentity, entity, self.DataInPath, self.DataInPathEs, self.DataOutPath, self.DataCode}

	return self.MakePaths(paths...)
}
func (self *DataPath) MakePaths(paths ...string) error {
	for _, path := range paths {
		if err := self.makePath(path); err != nil {
			return err
		}
	}
	return nil
}
func (self *DataPath) makePath(path string) error {
	if !fileutils.CheckFileExist(path) {
		var err = os.MkdirAll(path, 0777)
		return err
	}
	return nil
}
