package basedata

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: data_path_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-09-12 13:37:11
	@Update  作者:    leijmdas@163.com  时间: 2024-09-12 13:37:11
*/

var singleNameDataPath = "*basedata.DataPath-8d798644-94a2-4301-82a2-8f44c8b22069"

// init register load
func init() {
	registerBeanDataPath()
}

// register DataPath
func registerBeanDataPath() {
	basedi.RegisterLoadBean(singleNameDataPath, LoadDataPath)
}

// FindBeanDataPath
func FindBeanDataPath() *DataPath {

	if bean, ok := basedi.FindBeanOk(singleNameDataPath); ok {
		return bean.(*DataPath)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadDataPath() baseiface.ISingleton {
	var inst = NewDataPath()
	InjectDataPath(inst)
	return inst

}

func InjectDataPath(s *DataPath) {

	//logrus.Info("inject")
}
