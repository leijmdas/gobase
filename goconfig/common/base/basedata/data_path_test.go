package basedata

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestDataPathSuite struct {
	suite.Suite

	inst    *DataPath
	rootdir string

	dbtype string
	cfg    *ichubconfig.IchubConfig
}

func TestDataPathSuites(t *testing.T) {
	suite.Run(t, new(TestDataPathSuite))
}

func (suite *TestDataPathSuite) SetupSuite() {
	logrus.Info(" setup suite")
	//ichublog.InitLogrus()
	suite.inst = FindBeanDataPath()
	suite.rootdir = fileutils.FindRootDir()
	suite.cfg = ichubconfig.FindBeanIchubConfig()
	suite.dbtype = suite.cfg.ReadIchubDb().Dbtype
	// suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_MYSQL
	suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_COCKROACH

}

func (suite *TestDataPathSuite) TearDownSuite() {
	logrus.Info(" teardown suite")
	suite.cfg.Gorm.DbType = suite.dbtype
	var dto = suite.cfg.ReadIchubDb()
	logrus.Info(dto)

}
func (suite *TestDataPathSuite) SetupTest() {
	logrus.Info(" setup test")
}

func (suite *TestDataPathSuite) TearDownTest() {
	logrus.Info(" teardown test")
}

func (this *TestDataPathSuite) Test001_init() {
	logrus.Info(1)

}

func (this *TestDataPathSuite) Test002_WriteFile() {
	logrus.Info(2)

}

func (this *TestDataPathSuite) Test003_ReadFile() {
	logrus.Info(3)

}

func (this *TestDataPathSuite) Test004_ReadEsFile() {
	logrus.Info(4)

}

func (this *TestDataPathSuite) Test005_WriteFiles() {
	logrus.Info(5)

}
