package basedto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
)

type IchubError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

func NewIchubError(code int, msg string) *IchubError {
	return &IchubError{Code: code, Msg: msg}
}

func NewIchubErrorServer() *IchubError {
	return NewIchubError(500, "fail")
}

func (this *IchubError) Error() string {
	return jsonutils.ToJsonPretty(this)
}
