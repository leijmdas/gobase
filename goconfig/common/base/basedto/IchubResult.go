package basedto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gookit/goutil/strutil"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
)

/*
	@Title    文件名称: ichubresult.go
	@Description  描述: 统一返回结构

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

const CODE_SUCCESS = 200

const CODE_REQUEST_BAD = 400
const CODE_SERVER_ERR = 500
const CODE_NOFOUND_RECORD = 501

const CODE_FAIL = CODE_SERVER_ERR

type IchubResult struct {
	BaseEntity

	//返回码
	Code int `json:"code,omitempty"`
	//信息
	Msg string `json:"msg,omitempty"`
	//数据
	Data any `json:"data,omitempty"`
}

func ResultCodeMsg(code int, msg string) *IchubResult {
	var result = NewIchubResult()

	result.Code = code
	result.Msg = msg
	return result

}
func ResultFailMsg(msg string) *IchubResult {
	return ResultCodeMsg(CODE_FAIL, msg)
}
func ResultSuccessData(data interface{}) *IchubResult {
	var result = ResultCodeMsg(CODE_SUCCESS, "成功")

	result.Data = data
	return result
}
func ResultError(err error) *IchubResult {
	return ResultCodeMsg(CODE_FAIL, err.Error())
}
func NewIchubResult() *IchubResult {
	var result = &IchubResult{
		Code: 200,
		Msg:  "成功",
	}
	result.InitProxy(result)
	return result

}

//func ValueOf(resp *resty.Response) *IchubResult {
//	var result IchubResult
//	result.InitProxy(&result)
//	if resp.StatusCode() != 200 {
//		return result.CodeMsg(resp.StatusCode(), string(resp.Body()))
//	}
//	jsonutils.FromJsonByte(resp.Body(), &result)
//	return &result
//}

func ParseIchubResult(body []byte) *IchubResult {
	var result IchubResult
	jsonutils.FromJsonByte(body, &result)
	return &result
}

func (self *IchubResult) CheckCode(suite suite.Suite, code int) {

	suite.Equal(code, self.Code)
}

func (self *IchubResult) Check(suite suite.Suite, keyVal string) {
	var kv = strutil.Split(keyVal, "=")
	suite.Equal(baseutils.Any2Str(self.ValueByKey(kv[0])), kv[1])
}

// keyVal : "Pay=12|l=2"
func (self *IchubResult) Checks(suite suite.Suite, keyVals string) {
	kvs := strutil.Split(keyVals, "|")
	for _, keyval := range kvs {
		self.Check(suite, keyval)
	}

}

// func DataOfIndex(1)
func (self *IchubResult) FromJson(body []byte) (*IchubResult, error) {

	var err = jsonutils.FromJsonByte(body, self)
	if err != nil {
		logrus.Error(err)

	}
	self.InitProxy(self)
	return self, err
}
func (self *IchubResult) DataIfResultParams() *IchubResultParams {

	var m = self.Data.(map[string]interface{})
	return NewIchubResultParams().ValueOf(m)
}

func (self *IchubResult) DataIfMap() map[string]interface{} {

	return self.Data.(map[string]interface{})
}
func (self *IchubResult) ValueByKey(key string) any {
	return self.Data.(map[string]any)[key]

}
func (self *IchubResult) ValueOfKey(key string) *gvar.Var {
	var value = self.Data.(map[string]any)[key]
	return gvar.New(value)
}

func (self *IchubResult) CheckValueByKey(key string, expect any) bool {
	var value = self.ValueByKey(key)

	v, _ := strutil.String(value)
	exp, _ := strutil.String(expect)
	return v == exp
}
func (self *IchubResult) ToBytes() []byte {

	return []byte(self.ToString())

}

func (self *IchubResult) ToString() string {

	return jsonutils.ToJsonPretty(self)

}

func (self *IchubResult) IsSuccess() bool {
	return self.Code == CODE_SUCCESS

}

func (self *IchubResult) Success() *IchubResult {
	self.Code = CODE_SUCCESS
	self.Msg = "成功"
	return self
}

func (self *IchubResult) SuccessData(data any) *IchubResult {
	self.Code = CODE_SUCCESS
	self.Msg = "成功"
	self.Data = data
	return self
}
func (self *IchubResult) SuccessMessage(msg string, data any) *IchubResult {
	self.Code = CODE_SUCCESS
	self.Msg = msg

	self.Data = data
	return self
}

func (self *IchubResult) Fail() *IchubResult {
	self.Code = CODE_FAIL
	self.Msg = "失败"
	return self
}

func (self *IchubResult) FailMsg(msg string) *IchubResult {
	self.Code = CODE_FAIL
	self.Msg = msg
	return self
}

func (self *IchubResult) FailMessage(msg string) *IchubResult {
	self.Code = CODE_FAIL
	self.Msg = msg
	return self
}

func (self *IchubResult) CodeMsg(code int, msg string) *IchubResult {
	self.Code = code
	self.Msg = msg
	return self
}

func (self *IchubResult) SetData(s any) {
	self.Data = s
}

func (self *IchubResult) GetData() any {

	return self.Data
}

func (self *IchubResult) String() string {
	return jsonutils.ToJsonPretty(self)
}

func FailResult(msg string) *IchubResult {
	return &IchubResult{
		Code: CODE_FAIL,
		Msg:  msg,
	}
}

func (self *IchubResult) GetDbResult() map[string]any {
	if self.Data == nil {
		return map[string]any{}
	}
	var mapData = self.Data.(map[string]any)
	var dbResult = mapData["DbResult"].(map[string]any)
	return dbResult

}
func (self *IchubResult) GetEsResult() map[string]any {
	if self.Data == nil {
		return map[string]any{}
	}
	var mapData = self.Data.(map[string]any)
	var esResult = mapData["EsResult"].(map[string]any)
	return esResult

}
func (self *IchubResult) To(out interface{}) {
	var res = jsonutils.ToJsonPretty(self.Data)

	jsonutils.FromJson(res, &out)

}

func (self *IchubResult) ParseResult() *IchubResult {
	if !self.IsSuccess() {
		return self
	}
	var data = self.Data.(map[string]any)
	var body, err = jsonutils.ToJsonBytes(data)
	if err != nil {
		logrus.Info(err)
		return NewIchubResult().FailMessage(err.Error())
	}
	self = NewIchubResult()
	self.FromJson(body)
	if self.IsSuccess() {
		return self
	}
	return self
}
func (self *IchubResult) ParseData(model interface{}) *IchubResult {
	if !self.IsSuccess() {
		return self
	}
	var data = self.Data.(map[string]any)
	var body, err = jsonutils.ToJsonBytes(data)
	if err != nil {
		logrus.Info(err)
		return NewIchubResult().FailMessage(err.Error())
	}

	err = jsonutils.FromJsonByte(body, model)
	if err != nil {
		return self.FailMessage(err.Error())
	}
	self.Data = model
	return self
}
func (self *IchubResult) ParseRemote(model interface{}) *IchubResult {
	var result = self.ParseResult()
	return result.ParseData(model)

}

func (self *IchubResult) Init() {
	self.Code = 200
	self.Msg = "成功"
	self.Data = nil
}
func (self *IchubResult) Shutdown() {
	//	self.Code = 200
	//	self.Msg = "成功"
	self.Data = nil
}

func (self *IchubResult) IsFailed() bool {
	return !self.IsSuccess()
}
