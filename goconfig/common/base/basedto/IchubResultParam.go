package basedto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"github.com/gookit/goutil/strutil"
	"reflect"
	"strconv"
)

/*
@Title    文件名称: ichub_param.go
@Description  描述: 有芯规则引擎执行的参数值对象
		应该进行值类型转换，转成string,再转成各类型

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
// 规则体支持的基础数据类型
// string bool
// int, int8, int16, int32, int64
// uint uint8, uint16,uint32, uint64
// float32 float64
type IchubResultParam struct {
	value any
	BaseEntity
}

func (p *IchubResultParam) Value() any {
	return p.value
}

func (p *IchubResultParam) SetValue(value any) {
	p.value = value
}

func NewIchubResultParam(value any) *IchubResultParam {
	var p = &IchubResultParam{value: value}
	p.InitProxy(p)
	return p
}

func (p *IchubResultParam) From(value any) *IchubResultParam {
	p.value = value
	return p
}
func (p *IchubResultParam) IfBool() bool {
	return p.CheckTypeParam() == "bool"
}
func (p *IchubResultParam) IfInt() bool {
	return p.CheckTypeParam() == "int"
}
func (p *IchubResultParam) IfInt8() bool {
	return p.CheckTypeParam() == "int8"
}
func (p *IchubResultParam) IfInt16() bool {
	return p.CheckTypeParam() == "int16"
}
func (p *IchubResultParam) IfInt32() bool {
	return p.CheckTypeParam() == "int32"
}
func (p *IchubResultParam) IfInt64() bool {
	return p.CheckTypeParam() == "int64"
}
func (p *IchubResultParam) IfString() bool {
	return p.CheckTypeParam() == "string"
}
func (p *IchubResultParam) IfFloat64() bool {
	return p.CheckTypeParam() == "float64"
}
func (p *IchubResultParam) IfFloat32() bool {
	return p.CheckTypeParam() == "float32"
}
func (p *IchubResultParam) IfByte() bool {
	return p.CheckTypeParam() == "byte"
}
func (p *IchubResultParam) IfStr() bool {
	return reflect.TypeOf(p.value).Kind() == reflect.String
}
func (p *IchubResultParam) CheckTypeParam() string {
	return p.CheckType(p.value)
}
func (p *IchubResultParam) IfUInt8() bool {
	return p.CheckTypeParam() == "uint8"
}
func (p *IchubResultParam) IfUInt16() bool {
	return p.CheckTypeParam() == "uint16"
}
func (p *IchubResultParam) IfUInt32() bool {
	return p.CheckTypeParam() == "uint32"
}
func (p *IchubResultParam) IfUInt64() bool {
	return p.CheckTypeParam() == "uint64"
}
func (p *IchubResultParam) CheckType(i interface{}) string {

	switch i.(type) {
	case string:
		return "string"
	case bool:
		return "bool"

	case uint16:
		return "uint16"
	case uint32:
		return "uint32"
	case uint64:
		return "uint64"
	case int8:
		return "int8"
	case int16:
		return "int16"
	case int32:
		return "int32"
	case int64:
		return "int64"
	case byte:
		return "byte"
	case float32:
		return "float32"
	case float64:
		return "float64"
	}

	return "string"

}

func (p *IchubResultParam) ToBool() (ivalue bool) {
	if p.IfBool() {
		ivalue = p.value.(bool)
	}
	v, _ := strutil.String(p.value)
	ivalue, _ = strconv.ParseBool(v)
	return
}
func (p *IchubResultParam) ToUInt() uint {
	return p.value.(uint)
}
func (p *IchubResultParam) ToUIn8() uint8 {
	return p.value.(uint8)
}
func (p *IchubResultParam) ToUInt16() (ivalue uint16) {
	if p.IfUInt16() {
		ivalue = p.value.(uint16)
	}
	v, _ := strutil.String(p.value)
	i, _ := strconv.ParseUint(v, 10, 16)
	ivalue = uint16(i)
	return
}

func (p *IchubResultParam) ToUInt32() (ivalue uint32) {
	if p.IfUInt32() {
		ivalue = p.value.(uint32)
	}
	v, _ := strutil.String(p.value)
	i, _ := strconv.ParseUint(v, 10, 32)
	ivalue = uint32(i)
	return
}

// 非int64也支持强制转换
func (p *IchubResultParam) ToUInt64() (ivalue uint64) {
	if p.IfUInt64() {
		ivalue = p.value.(uint64)
	}
	v, _ := strutil.String(p.value)
	i, _ := strconv.ParseUint(v, 10, 64)
	ivalue = uint64(i)
	return

}

func (p *IchubResultParam) ToInt() int {
	if p.IfInt() {
		return p.value.(int)
	}
	v, _ := strutil.String(p.value)
	r, _ := strutil.Int(v)
	return r
}

func (p *IchubResultParam) ToInt8() (ivalue int8) {
	if p.IfInt8() {
		ivalue = p.value.(int8)
	}
	v, _ := strutil.String(p.value)
	i, _ := strconv.ParseInt(v, 10, 8)
	ivalue = int8(i)
	return
}

func (p *IchubResultParam) ToInt16() (ivalue int16) {
	if p.IfInt16() {
		ivalue = p.value.(int16)
	}
	v, _ := strutil.String(p.value)
	i, _ := strconv.ParseInt(v, 10, 16)
	ivalue = int16(i)
	return
}

func (p *IchubResultParam) ToInt32() (ivalue int32) {
	if p.IfInt32() {
		ivalue = p.value.(int32)
	}
	v, _ := strutil.String(p.value)
	i, _ := strconv.ParseInt(v, 10, 32)
	ivalue = int32(i)
	return
}

func (p *IchubResultParam) ToInt64() (ivalue int64) {
	if p.IfInt64() {
		ivalue = p.value.(int64)
	}
	v, _ := strutil.String(p.value)
	ivalue = strutil.Int64(v)
	return
}

func (p *IchubResultParam) ToFloat32() (ivalue float32) {

	if p.IfFloat32() {
		ivalue = p.value.(float32)
	}
	v, _ := strutil.String(p.value)
	float, _ := strconv.ParseFloat(v, 32)
	ivalue = float32(float)
	return
}

func (p *IchubResultParam) ToFloat64() (ivalue float64) {
	if p.IfFloat64() {
		ivalue = p.value.(float64)
	}
	v, _ := strutil.String(p.value)
	ivalue, _ = strconv.ParseFloat(v, 64)
	return
}

func (p *IchubResultParam) ToString() string {
	if p.IfString() {
		return p.value.(string)
	}
	v, _ := strutil.String(p.value)
	return v
}

func (p *IchubResultParam) ToByte() byte {
	return p.value.(byte)
}

func (p *IchubResultParam) ToBytes() []byte {
	return p.value.([]byte)
}

func (p *IchubResultParam) String() string {
	return jsonutils.ToJsonPretty(*p)
}
