package basedto

type IchubResultParams struct {
	Params map[string]any `json:"params"`
	BaseEntity
}

func NewIchubResultParams() *IchubResultParams {
	var p = &IchubResultParams{
		Params: make(map[string]any),
	}
	p.InitProxy(p)
	return p
}

func (this *IchubResultParams) ValueOf(m map[string]any) *IchubResultParams {
	this.Params = m
	return this
}
func (p *IchubResultParams) GetParams(key string) *IchubResultParams {
	params := NewIchubResultParams().ValueOf(p.Params[key].(map[string]any))
	params.InitProxy(params)
	return params

}

func (p *IchubResultParams) GetParam(key string) *IchubResultParam {
	return NewIchubResultParam(p.Params[key])

}
func (p *IchubResultParams) GetMap(key string) map[string]any {
	return p.Params[key].(map[string]any)

}

func (p *IchubResultParams) SetParam(key string, value any) {
	p.Params[key] = value

}
