package basedto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseindex"
)

type IndexObjectAdapter struct {
	baseindex.IndexMapping
}

func DefaultAdapter(indexObject baseindex.IndexObject) *IndexObjectAdapter {
	return &IndexObjectAdapter{IndexMapping: indexObject}
}

func NewIndexObjectAdapter(indexObject baseindex.IndexObject) *IndexObjectAdapter {
	return DefaultAdapter(indexObject)
}
func (b IndexObjectAdapter) IndexName() string {

	return b.IndexMapping.TableName()
}

func (b IndexObjectAdapter) Mapping() string {

	return b.IndexMapping.GetMapping()
}

func (b IndexObjectAdapter) IndexAlaisName() string {

	return b.TableName() + "_alias"

}
