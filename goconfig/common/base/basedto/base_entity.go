package basedto

import (
	"errors"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
)

type BaseEntity struct {
	Proxy    func() *BaseProxy             `json:"-"`
	Bindings map[string]baseiface.LoadBean `json:"-"`
	RootDir  string                        `json:"-"`
}

func NewBaseEntity() *BaseEntity {
	var entity = &BaseEntity{
		Bindings: map[string]baseiface.LoadBean{},
	}

	entity.InitProxy(entity)
	return entity
}

func (self *BaseEntity) InitProxy(some any) {
	self.Proxy = func() *BaseProxy {

		return NewBaseProxy(some)
	}

}

func (self *BaseEntity) String() string {
	if self.Proxy != nil {
		return self.Proxy().String()
	}
	return "nil"
}

func (self *BaseEntity) ToString() string {
	if self.Proxy == nil {
		return "nil"
	}
	return self.Proxy().ToString()
}

func (self *BaseEntity) Log() {

	self.Proxy().Log()

}
func (self *BaseEntity) ContainsType(name string) bool {
	return self.Proxy().ContainsType(name)
}
func (self *BaseEntity) Clone() interface{} {
	return self.Proxy().Clone()
}

func (self *BaseEntity) ValueOf(that any) {
	self.Proxy().ValueOf(that)
}

func (self *BaseEntity) FromJsonAny(body []byte) interface{} {
	return self.Proxy().FromJson(body)

}
func (self *BaseEntity) ToJson() string {
	return self.Proxy().ToJson()

}

func (self *BaseEntity) ToJsonBytes() []byte {
	return self.Proxy().ToJsonBytes()
}
func (self *BaseEntity) ToPrettyString() string {
	if self.Proxy() == nil {
		return "{}"
	}
	return self.Proxy().ToPrettyString()
}

func (self *BaseEntity) Id2Str(id int64) string {
	return baseutils.Any2Str(id)
}

func (self *BaseEntity) ValueFrom(from any) {
	self.Proxy().ValueFrom(from)

}

func (self *BaseEntity) Str2Int(from string) int {

	return stringutils.Str2Int(from)
}
func (self *BaseEntity) CopyWithOption(from any) {
	self.Proxy().CopyWithOption(from)

}
func (self *BaseEntity) Bind() {
	//self.BindUp()

}
func (self *BaseEntity) BindUp(key string, load baseiface.LoadBean) {
	if self.Bindings == nil {
		self.Bindings = map[string]baseiface.LoadBean{}
	}
	//key = strings.ToLower(key)
	self.Bindings[key] = load

}
func (self *BaseEntity) FindBinding(key string) baseiface.LoadBean {

	return self.Bindings[key]

}

func (self *BaseEntity) Single() bool {
	return false //self.Proxy().Single()
}
func (self *BaseEntity) Autoload() bool {
	return false //self.Proxy().Autoload()

}
func (self *BaseEntity) AutoInject() bool {
	return false //self.Proxy().AutoInject()

}
func (self *BaseEntity) Init() {

}
func (self *BaseEntity) Shutdown() {

}
func LoadBeanProxy[T baseiface.IbaseProxy]() T {

	return baseutils.LoadBeanProxy[T]()

}

func (self *BaseEntity) Parse2Map() (map[string]any, error) {
	if self.Proxy != nil {
		return self.Proxy().Parse2Map()
	}
	return nil, errors.New("BaseEntity self.Proxy is nil!")
}

func (self *BaseEntity) IfAutowire() bool {
	if self.Proxy != nil {
		return self.Proxy().IfAutowire()
	}
	return false

}
func (self *BaseEntity) Autowire() {
	if self.Proxy != nil {
		self.Proxy().Autowire()
	}

}

func (self *BaseEntity) ContainsStru(v any) bool {
	if self.Proxy != nil {
		return self.Proxy().ContainsStru(v)
	}
	return false
}
