package basedto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameBaseEntity = "BaseEntity"

func init() {
	registerBeanBaseEntity()
}

// register BaseEntity
func registerBeanBaseEntity() {
	basedi.RegisterLoadBean(singleNameBaseEntity, LoadBaseEntity)
}

func FindBeanBaseEntity() *BaseEntity {
	return basedi.FindBean(singleNameBaseEntity).(*BaseEntity)
}

func LoadBaseEntity() baseiface.ISingleton {
	return NewBaseEntity()
}
