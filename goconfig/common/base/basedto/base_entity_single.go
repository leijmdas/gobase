package basedto

type BaseEntitySingle struct {
	BaseEntity
}

func (this *BaseEntitySingle) Single() bool {

	return true
}
func (this *BaseEntitySingle) InitProxy(some any) {
	this.Proxy = func() *BaseProxy {

		return NewBaseProxy(some)
	}

}
