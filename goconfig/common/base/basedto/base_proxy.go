package basedto

import (
	"errors"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/gometa"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"github.com/huandu/go-clone"
	"github.com/jinzhu/copier"
	"github.com/sirupsen/logrus"
	"reflect"
)

type BaseProxy struct {
	some any
}

func NewBaseProxy(some any) *BaseProxy {
	var proxy = new(BaseProxy)
	proxy.some = some
	return proxy
}
func (this *BaseProxy) Some() any {
	return this.some
}

func (this *BaseProxy) SetSome(some any) {
	this.some = some
}

func (this *BaseProxy) String() string {

	return jsonutils.ToJsonPretty(this.some)
}

func (this *BaseProxy) ToString() string {

	return jsonutils.ToJsonPretty(this.some)
}
func (this *BaseProxy) ToJsonBytes() []byte {

	var r, _ = jsonutils.ToJsonBytes(this.some)
	return r
}

func (this *BaseProxy) Log() {
	var name = reflect.TypeOf(this.some).String()
	logrus.Info(name, " =", this.String())

}
func (this *BaseProxy) Check() error {
	if this.some == nil {
		return errors.New("some is nil!")
	}
	return nil

}

func (this *BaseProxy) Clone() any {

	return clone.Clone(this.some)

}

func (this *BaseProxy) FromJson(body []byte) interface{} {
	jsonutils.FromJsonByte(body, this.some)
	return this.some

}
func (this *BaseProxy) ToJson() string {
	return jsonutils.ToJsonPretty(this.some)

}

func (this *BaseProxy) ValueOf(another interface{}) {
	this.some = another
}
func (this *BaseProxy) ToPrettyString() string {
	if this.some == nil {
		return "{}"
	}
	return jsonutils.ToJsonPretty(this.some)
}
func (this *BaseProxy) ContainsType(name string) bool {
	value := reflect.ValueOf(this.some)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}

	if value.Kind() != reflect.Struct {
		return false
	}

	for i := 0; i < value.NumField(); i++ {
		logrus.Info(value.Field(i).Type().Name())
		if value.Field(i).Type().Name() == name {
			return true
		}
	}

	return false
}

func (this *BaseProxy) ValueFrom(from any) {
	if v, ok := from.(string); ok {
		jsonutils.FromJson(v, this.some)
		return
	}
	var fromstr = jsonutils.ToJsonPretty(from)
	jsonutils.FromJson(fromstr, this.some)

}

func (this *BaseProxy) CopyWithOption(from any) {
	copier.CopyWithOption(this.some, from, copier.Option{IgnoreEmpty: true})

}
func (this *BaseProxy) Single() bool {
	return false

}
func (this *BaseProxy) Autoload() bool {
	return false

}
func (this *BaseProxy) AutoInject() bool {
	return false

}

func (this *BaseProxy) IfBaseEntity() bool {

	return baseutils.ContainsType(this.some, "BaseEntity") ||
		baseutils.ContainsType(this.some, "BaseEntitySingle") ||
		baseutils.ContainsType(this.some, "BaseConfig") ||
		baseutils.ContainsTypeOfSub(this.some, "BaseEntity") ||
		baseutils.ContainsTypeOfSub(this.some, "BaseEntitySingle")

}
func (this *BaseProxy) Init() {
	if this.some != nil {
		this.some.(baseiface.IpoolObj).Init()
	}
}
func (this *BaseProxy) Shutdown() {
	if this.some != nil {
		this.some.(baseiface.IpoolObj).Shutdown()
	}
}

func (self *BaseProxy) Parse2Map() (map[string]any, error) {
	if self.Some != nil {
		return jsonutils.Parse2Map(jsonutils.ToJsonPretty(self.Some))
	}
	return nil, errors.New("BaseProxy Some is nil!")

}
func (self *BaseProxy) IfAutowire() bool {
	if self.Some() == nil {
		return false
	}
	return baseutils.ContainsStru[gometa.Gometa](self.Some())

}
func (self *BaseProxy) Autowire() {
	if self.Some() != nil {
		baseutils.Autowired(self.Some())
	}

}
func (self *BaseProxy) ContainsStru(chiildStru any) bool {
	if self.Some() != nil {
		var t = reflect.TypeOf(self.Some())
		if t.Kind() == reflect.Ptr {
			t = t.Elem()
		}
		if t.Kind() != reflect.Struct {
			return false
		}
		return baseutils.ContainsStruPtr(self.Some(), chiildStru)

	}
	return false

}
