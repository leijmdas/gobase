package baseiface

type IEncDec interface {
	EncBase64(text string) string
	DecBase64(text string) string
}
