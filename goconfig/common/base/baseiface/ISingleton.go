package baseiface

// NewBean
type ISingleton interface {
	Single() bool
	IfAutowire() bool
	Autowire()
	ContainsStru(v any) bool

	InitProxy(some any)
}
