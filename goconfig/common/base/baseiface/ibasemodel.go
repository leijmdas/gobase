package baseiface

type IbaseModel interface {
	BaseInt2Time()

	BaseTime2Int()
	IbizModel
}

type IbizModel interface {
	HandleCheck() error
	HandleRequest() error
	HandleResult() error
}
