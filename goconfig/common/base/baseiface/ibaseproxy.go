package baseiface

type IbaseProxy interface {
	ISingleton
	IBinding
	IpoolObj

	String() string
	ToString() string
	ToPrettyString() string

	FromJsonAny(body []byte) any

	ToJson() string
	ToJsonBytes() []byte
	ValueOf(another any)
	ValueFrom(from any)
	Parse2Map() (map[string]any, error)

	Log()
	Clone() any
	ContainsType(name string) bool
}
