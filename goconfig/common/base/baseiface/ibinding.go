package baseiface

// 创建bean的函数
type LoadBean func() ISingleton

type IBinding interface {
	//绑定成员注入NEW方法
	Bind()
	FindBinding(key string) LoadBean
}
