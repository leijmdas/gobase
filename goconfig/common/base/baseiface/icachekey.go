package baseiface

type IcacheKey interface {
	CacheKey() string
}
