package baseiface

type IgofileUtil interface {
}
type GofileUtil interface {
	FindRootPkg() string
	SetRootPkgEnv(p string)
	CheckConfigFileExist(root, f string) bool
	FindRootDir() string
	FindRootDirGoMod() string
	GetCurPath() string
	CheckFileExist(filename string) bool
	TryFileExist(filename string) bool
	UnZip(zipFile string, destPath string) error
	Zip(srcPath string, destFile string) error
	Dir(pathfile string) string
	FindPkgPath(pkg string) string
}
