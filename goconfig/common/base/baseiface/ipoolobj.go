package baseiface

type IpoolObj interface {
	Init()
	Shutdown()
}
