package baseindex

type IndexIface interface {
	IndexPkey
	IndexMapping
}
