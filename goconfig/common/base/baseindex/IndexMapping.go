package baseindex

import "gorm.io/gorm/schema"

type IndexMapping interface {
	schema.Tabler
	GetMapping() string
}
