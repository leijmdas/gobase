package baseindex

type IndexObject interface {
	IndexName() string
	Mapping() string
	IndexAlaisName() string
	IndexMapping
}
