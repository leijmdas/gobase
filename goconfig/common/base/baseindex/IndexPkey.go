package baseindex

type IndexPkey interface {
	PkeyName() string
	PkeyValue() any
}
