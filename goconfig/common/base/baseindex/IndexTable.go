package baseindex

import "gorm.io/gorm/schema"

type IndexTable interface {
	schema.Tabler
}
