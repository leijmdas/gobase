package basemodel

import (
	"gitee.com/dromara/carbon/v2"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"time"
)

const (
	DateLayout = "2006-01-02"
	TimeLayout = "2006-01-02 15:04:05"
)

type IndexSimple struct {
	//Id            string `json:"id"`
	// 时间类型
	CreatedAtTime time.Time `json:"created_at_time"`
	UpdatedAtTime time.Time `json:"updated_at_time"`
	//	DeletedAt *time.Time `json:"deleted_at"`
	//ui used 时间整型
	CreatedAt int64 `json:"created_at" gorm:"-"`
	UpdatedAt int64 `json:"updated_at" gorm:"-"`
	//	DeleteAtInt  *int64 `json:"deleted_at_int" gorm:"-"`
	// 创建与更新者
	CreatedBy int64 `json:"created_by"`
	UpdatedBy int64 `json:"updated_by"`
	//DeletedBy int64 `json:"deleted_by"`
	// biz set2ui 创建与更新人名称
	CreatedByName string `json:"created_by_name" gorm:"-"`
	UpdatedByName string `json:"updated_by_name" gorm:"-"`
	//DeletedByName string `json:"deleted_by_name" gorm:"-"`

	baseiface.IbizModel `json:"-" gorm:"-"`
}

func NewIndexSimple() *IndexSimple {
	return &IndexSimple{}
}

func (self *IndexSimple) BaseInt2Time() {
	if self.CreatedAt != 0 {
		self.CreatedAtTime = self.Int2Time(self.CreatedAt)
		self.CreatedAtTime = carbon.CreateFromStdTime(self.CreatedAtTime, carbon.PRC).StdTime()

	}
	if self.UpdatedAt != 0 {
		self.UpdatedAtTime = self.Int2Time(self.UpdatedAt)
		self.UpdatedAtTime = carbon.CreateFromStdTime(self.UpdatedAtTime, carbon.PRC).StdTime()
	}

}
func (self *IndexSimple) BaseTime2Int() {
	if !self.CreatedAtTime.IsZero() {
		self.CreatedAt = self.Time2Int(self.CreatedAtTime)

	}
	if !self.UpdatedAtTime.IsZero() {
		self.UpdatedAt = self.Time2Int(self.UpdatedAtTime)
	}

}
func (self *IndexSimple) Time2Int(intime time.Time) int64 {
	return intime.UnixNano()
}

func (self *IndexSimple) Int2Time(intime int64) time.Time {
	return time.Unix(intime/1e6/1000, intime%1e6)
}
func (self *IndexSimple) HandleRequest() error {
	if self.IbizModel != nil {
		return self.IbizModel.HandleRequest()
	}
	return nil
}

func (self *IndexSimple) HandleResult() error {
	if self.IbizModel != nil {
		return self.IbizModel.HandleResult()
	}
	return nil
}
func (self *IndexSimple) HandleCheck() error {
	if self.IbizModel != nil {
		return self.IbizModel.HandleCheck()
	}
	return nil
}

func (bd *IndexSimple) formatTime(sec int64) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (bd *IndexSimple) formatDate(sec int64) string {
	//timeLayout := "2006-01-02"
	datetime := time.Unix(sec, 0).Format(DateLayout)
	return datetime
}

func (bd *IndexSimple) localTimeFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.Format(timeLayout)

}

func (bd *IndexSimple) localTimeUTCFormat(ptime time.Time) string {
	//	timeLayout := "2006-01-02 15:04:05"
	return ptime.UTC().Format(time.DateTime)

}
