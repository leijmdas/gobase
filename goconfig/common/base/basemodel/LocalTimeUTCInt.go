package basemodel

import (
	"database/sql/driver"
	"fmt"
	"strconv"
	"time"
)

type LocalTimeUTCInt struct {
	time.Time
}

func (t LocalTimeUTCInt) MarshalJSON() ([]byte, error) {
	//格式化秒
	seconds := t.Unix()
	return []byte(strconv.FormatInt(seconds, 10)), nil
	//return []byte(fmt.Sprintf("%d",seconds)), nil
}

func (t *LocalTimeUTCInt) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		t.Time = time.Unix(0, 0)
		return nil
	}

	i64, err := strconv.ParseInt(string(data), 10, 64)
	t.Time = time.Unix(i64, 0)
	return err
}

func (t LocalTimeUTCInt) Value() (driver.Value, error) {
	var zeroTime time.Time
	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return t.Time, nil
}
func (t *LocalTimeUTCInt) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = LocalTimeUTCInt{Time: value}
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}

func (t *LocalTimeUTCInt) FormatDatetime() string {
	return t.Time.Format("2006-01-02 15:04:05")
}

func (t *LocalTimeUTCInt) FormatDate() string {
	return t.Time.Format("2006-01-02")
}

func (t *LocalTimeUTCInt) Zero() LocalTimeUTCInt {
	return LocalTimeUTCInt{}
}
