package basemodel

import (
	"time"
)

type BaseModel struct {
	*BaseModelSimple
	// 时间类型
	DeletedAt *time.Time `json:"deleted_at"`
	//ui used 时间整型
	DeleteAtInt *int64 `json:"deleted_at_int" gorm:"-"`
	// 创建与更新者
	DeletedBy int64 `json:"deleted_by"`
	// biz set2ui 创建与更新人名称
	DeletedByName string `json:"deleted_by_name" gorm:"-"`
}

func NewBaseModel() *BaseModel {
	return &BaseModel{
		BaseModelSimple: NewBaseModelSimple(),
	}
}

func (self *BaseModel) BaseInt2Time() {
	if self.CreatedAtInt != 0 {
		self.CreatedAt = self.Int2Time(self.CreatedAtInt)
	}
	if self.UpdatedAtInt != 0 {
		self.UpdatedAt = self.Int2Time(self.UpdatedAtInt)
	}
	if self.DeleteAtInt != nil {
		self.DeletedAt = &time.Time{}
		*self.DeletedAt = self.Int2Time(*self.DeleteAtInt)
	}

}
func (self *BaseModel) BaseTime2Int() {
	if !self.CreatedAt.IsZero() {
		self.CreatedAtInt = self.Time2Int(self.CreatedAt)
	}
	if !self.UpdatedAt.IsZero() {
		self.UpdatedAtInt = self.Time2Int(self.UpdatedAt)
	}
	if self.DeletedAt != nil {
		self.DeleteAtInt = new(int64)
		*self.DeleteAtInt = self.Time2Int(*self.DeletedAt)
	}

}
