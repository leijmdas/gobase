package basemodel

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"time"
)

type BaseModelSimple struct {
	//Id            string `json:"id"`
	// 时间类型
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	//	DeletedAt *time.Time `json:"deleted_at"`
	//ui used 时间整型
	CreatedAtInt int64 `json:"created_at_int" gorm:"-"`
	UpdatedAtInt int64 `json:"updated_at_int" gorm:"-"`
	//	DeleteAtInt  *int64 `json:"deleted_at_int" gorm:"-"`
	// 创建与更新者
	CreatedBy int64 `json:"created_by"`
	UpdatedBy int64 `json:"updated_by"`
	//DeletedBy int64 `json:"deleted_by"`
	// biz set2ui 创建与更新人名称
	CreatedByName string `json:"created_by_name" gorm:"-"`
	UpdatedByName string `json:"updated_by_name" gorm:"-"`
	//DeletedByName string `json:"deleted_by_name" gorm:"-"`

	baseiface.IbizModel `json:"-" gorm:"-"`
}

func NewBaseModelSimple() *BaseModelSimple {
	return &BaseModelSimple{}
}

func (self *BaseModelSimple) BaseInt2Time() {
	if self.CreatedAtInt != 0 {
		self.CreatedAt = self.Int2Time(self.CreatedAtInt)
	}
	if self.UpdatedAtInt != 0 {
		self.UpdatedAt = self.Int2Time(self.UpdatedAtInt)
	}

}
func (self *BaseModelSimple) BaseTime2Int() {
	if !self.CreatedAt.IsZero() {
		self.CreatedAtInt = self.Time2Int(self.CreatedAt)
	}
	if !self.UpdatedAt.IsZero() {
		self.UpdatedAtInt = self.Time2Int(self.UpdatedAt)
	}

}
func (self *BaseModelSimple) Time2Int(intime time.Time) int64 {
	return intime.UnixNano() / 1e6
}

func (self *BaseModelSimple) Int2Time(intime int64) time.Time {
	return time.Unix(intime/1000, 0)
}
func (self *BaseModelSimple) HandleRequest() error {
	if self.IbizModel != nil {
		return self.IbizModel.HandleRequest()
	}
	return nil
}

func (self *BaseModelSimple) HandleResult() error {
	if self.IbizModel != nil {
		return self.IbizModel.HandleResult()
	}
	return nil
}
func (self *BaseModelSimple) HandleCheck() error {
	if self.IbizModel != nil {
		return self.IbizModel.HandleCheck()
	}
	return nil
}

func (bd *BaseModelSimple) formatTime(sec int64) string {
	timeLayout := "2006-01-02 15:04:05"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (bd *BaseModelSimple) formatDate(sec int64) string {
	timeLayout := "2006-01-02"
	datetime := time.Unix(sec, 0).Format(timeLayout)
	return datetime
}

func (bd *BaseModelSimple) localTimeFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.Format(timeLayout)

}

func (bd *BaseModelSimple) localTimeUTCFormat(ptime time.Time) string {
	timeLayout := "2006-01-02 15:04:05"
	return ptime.UTC().Format(timeLayout)

}
