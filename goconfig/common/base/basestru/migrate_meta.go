package basestru

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basemodel"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"github.com/gogf/gf/v2/container/garray"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/jinzhu/gorm"
	"gorm.io/gorm/schema"
	"reflect"
	"strings"
	"time"
)

type MigrateMeta struct {
	basedto.BaseEntity
	FieldTypes map[string]string
	FieldTags  map[string]string
	body       string
	filter     string
}

var ColTypNil = reflect.TypeOf(basedto.IchubResult{})

func NewMigrateMeta() *MigrateMeta {
	return &MigrateMeta{
		FieldTags:  make(map[string]string),
		FieldTypes: make(map[string]string),
	}
}
func (self *MigrateMeta) Encode() {
	self.body = ""
	self.body = jsonutils.ToJsonPretty(self)
}
func (self *MigrateMeta) Decode() {
	jsonutils.FromJson(self.body, self)
}
func (self *MigrateMeta) AddField(field string, fieldType string, fieldTag string) {
	self.FieldTypes[field] = fieldType
	self.FieldTags[field] = fieldTag
}
func (self *MigrateMeta) FromStru(stru schema.Tabler) {
	var typ = reflect.TypeOf(stru)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}
	for i := 0; i < typ.NumField(); i++ {
		var field = typ.Field(i)
		self.FieldTypes[field.Name] = field.Type.String()
		self.FieldTags[field.Name] = string(field.Tag)
	}
}

// fields f1,f2
func (self *MigrateMeta) FromStruFilter(stru schema.Tabler, filterFields string) {
	var typ = reflect.TypeOf(stru)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}
	self.filter = filterFields
	var a = self.Str2Garray(filterFields)

	for i := 0; i < typ.NumField(); i++ {
		var field = typ.Field(i)
		if a.Contains(stringutils.Camel2Case(field.Name)) {

			self.FieldTypes[field.Name] = field.Type.String()
			self.FieldTags[field.Name] = string(field.Tag)
		}
	}
}
func (self *MigrateMeta) Str2Garray(fields string) *garray.Array {
	var outfields = strings.Split(fields, ",")
	var a = garray.NewFromCopy(gconv.SliceAny(outfields), true)

	return a
}
func (self *MigrateMeta) CreateStruMetaRecord() any {
	return self.CreateStruMeta().Addr().Interface()
}
func (self *MigrateMeta) CreateStruMeta() reflect.Value {
	var meta = self
	dbFields := []reflect.StructField{}

	for fieldName, colType := range meta.FieldTypes {
		var columnName = stringutils.Camel2Case(fieldName)

		var ct, anonymous = self.Col2ColType(colType, fieldName)

		if ColTypNil == ct {
			golog.Warn("coltype not define")
			continue
		}

		var name = stringutils.Case2Camel(columnName)
		name = strings.ReplaceAll(name, ",", "")
		var structField = reflect.StructField{
			Name:      name,
			Type:      ct,
			Tag:       reflect.StructTag(meta.FieldTags[fieldName]),
			Anonymous: anonymous,
		}
		dbFields = append(dbFields, structField)

	}

	dbtableType := reflect.StructOf(dbFields)
	return reflect.New(dbtableType).Elem()
}

func (self *MigrateMeta) Col2ColType(colType string, colName string) (reflect.Type, bool) {
	var anonymous = false
	var columnType = ColTypNil
	switch colType {
	case "float32":
		columnType = reflect.TypeOf(float32(0))
	case "float64":
		columnType = reflect.TypeOf(float64(0))
	case "int64":
		columnType = reflect.TypeOf(int64(0))
	case "int32":
		columnType = reflect.TypeOf(int32(0))
	case "int16":
		columnType = reflect.TypeOf(int16(0))
	case "uint64":
		columnType = reflect.TypeOf(uint64(0))
	case "uint32":
		columnType = reflect.TypeOf(uint32(0))
	case "uint16":
		columnType = reflect.TypeOf(uint16(0))
	case "string":
		columnType = reflect.TypeOf("")
	case "bool":
		columnType = reflect.TypeOf(true)
	case "byte":
		columnType = reflect.TypeOf(byte(0))
	case "[]byte":
		columnType = reflect.TypeOf([]byte{})

	case "time.Time":
		columnType = reflect.TypeOf(time.Now())
		if colName == "deleted_at" {
			var t *time.Time
			columnType = reflect.TypeOf(t)

		}

	case "basedto.BaseEntity":
		columnType = reflect.TypeOf(basedto.BaseEntity{})
		anonymous = true
	case "*basemodel.BaseModel", "basemodel.BaseMode":
		columnType = reflect.TypeOf(basemodel.BaseModel{})
		anonymous = true

	case "*gorm.Model", "gorm.Model":
		columnType = reflect.TypeOf(gorm.Model{})
		anonymous = true

	case "*basemodel.BaseModelSimple", "basemodel.BaseModelSimple":
		columnType = reflect.TypeOf(basemodel.BaseModelSimple{})
		anonymous = true
	}
	return columnType, anonymous
}
