package basestru

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: migrate_meta_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-10-22 17:53:33
	@Update  作者:    leijmdas@163.com  时间: 2024-10-22 17:53:33
*/

var singleNameMigrateMeta = "*pagedb.MigrateMeta-849697d4-ddf4-423e-818d-c437e617e661"

// init register load
func init() {
	registerBeanMigrateMeta()
}

// register MigrateMeta
func registerBeanMigrateMeta() {
	err := basedi.RegisterLoadBean(singleNameMigrateMeta, LoadMigrateMeta)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanMigrateMeta
func FindBeanMigrateMeta() *MigrateMeta {

	if bean, ok := basedi.FindBeanOk(singleNameMigrateMeta); ok {
		return bean.(*MigrateMeta)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadMigrateMeta() baseiface.ISingleton {
	var inst = NewMigrateMeta()
	InjectMigrateMeta(inst)
	return inst

}

func InjectMigrateMeta(s *MigrateMeta) {

}
