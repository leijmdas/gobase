package basestru

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/model"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestMigrateMetaSuite struct {
	suite.Suite

	inst    *MigrateMeta
	rootdir string

	dbtype string
	cfg    *ichubconfig.IchubConfig
}

func TestMigrateMetaSuites(t *testing.T) {
	suite.Run(t, new(TestMigrateMetaSuite))
}

func (suite *TestMigrateMetaSuite) SetupSuite() {
	logrus.Info(" setup suite")

	suite.inst = FindBeanMigrateMeta()
	suite.rootdir = fileutils.FindRootDir()
	suite.cfg = ichubconfig.FindBeanIchubConfig()
	suite.dbtype = suite.cfg.ReadIchubDb().Dbtype
	// suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_MYSQL
	suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_COCKROACH

}

func (suite *TestMigrateMetaSuite) TearDownSuite() {
	logrus.Info(" teardown suite")
	suite.cfg.Gorm.DbType = suite.dbtype
	//var dto = suite.cfg.ReadIchubDb()
	//golog.Info(dto)

}
func (suite *TestMigrateMetaSuite) SetupTest() {
	logrus.Info(" setup test")
}

func (suite *TestMigrateMetaSuite) TearDownTest() {
	logrus.Info(" teardown test")
}

func (this *TestMigrateMetaSuite) Test001_Encode() {
	golog.Info(1)

}

func (this *TestMigrateMetaSuite) Test002_Decode() {
	golog.Info(2)

}

func (this *TestMigrateMetaSuite) Test003_AddField() {
	golog.Info(3)

}

func (this *TestMigrateMetaSuite) Test004_FromStru() {
	golog.Info(4)

}

func (this *TestMigrateMetaSuite) Test005_FromStruFilter() {
	golog.Info(5)

}

func (this *TestMigrateMetaSuite) Test006_Str2Garray() {
	golog.Info(6)

}

func (this *TestMigrateMetaSuite) Test007_CreateDbTableStruMeta() {
	var m = FindBeanMigrateMeta()
	m.FromStruFilter(&model.Employee{}, "id,name")
	m.Encode()
	golog.Info(m.body)
	var m1 = FindBeanMigrateMeta()
	m1.body = m.body
	m1.Decode()
	golog.Info(m1)
}

func (this *TestMigrateMetaSuite) Test008_Col2ColType() {
	var m = FindBeanMigrateMeta()
	m.FromStruFilter(&model.Employee{}, "id,name")
	m.Encode()
	golog.Info(m.body)
	var r = m.CreateStruMetaRecord()
	var emp = &model.Employee{Id: 19, Name: "11leij", IdCard: "1"}
	jsonutils.FromJson(jsonutils.ToJsonPretty(emp), r)

	golog.Info("r=1", r)
}
