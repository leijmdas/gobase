package encrypt

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameEncDec = "EncDec"

// init
// register
// load
func init() {
	registerBeanEncDec()
}

// register EncDec
func registerBeanEncDec() {
	basedi.RegisterLoadBean(singleNameEncDec, LoadEncDec)
}

func FindBeanEncDec() *EncDec {
	return basedi.FindBean(singleNameEncDec).(*EncDec)
}

func LoadEncDec() baseiface.ISingleton {
	return NewEncDec()
}
