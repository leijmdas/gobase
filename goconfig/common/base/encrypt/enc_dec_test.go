package encrypt

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"testing"
)

func init() {
	ichublog.InitLogrus()
}
func Test001_Md5Enc(t *testing.T) {
	var r = FindBeanEncDec().Md5Encrypt("123456")
	logrus.Info(r)
}

func Test002_AesEncBase64(t *testing.T) {
	var ret = FindBeanEncDec().AesEncBase64("leijmdas@163.comL")
	logrus.Info("\r\n", ret)
}
func Test002_AesDecBase64(t *testing.T) {
	var ret = FindBeanEncDec().AesEncBase64("leijmdas@163.comL")
	logrus.Info("\r\n", ret)
	var pwd = FindBeanEncDec().AesDecBase64(ret)
	logrus.Info("\r\n", pwd)
}
