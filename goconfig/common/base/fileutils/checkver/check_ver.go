package checkver

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"github.com/sirupsen/logrus"
	"os"
	"strings"
)

const ERR_FLAG = ">>>>>>>"

type CheckVer struct {
	basedto.BaseEntitySingle
	*fileutils.FindFileService
}

func NewCheckVer() *CheckVer {
	return &CheckVer{
		FindFileService: fileutils.NewFindFileService(),
	}
}
func (self *CheckVer) CheckAllFiles() error {
	var fs, err = self.FindAllFile()
	if err != nil {
		logrus.Error("CheckAllFiles is err! " + err.Error())
		return err
	}

	err = self.CheckFiles(fs)

	if err != nil {
		logrus.Error("\r\n" + err.Error())
		return err
	}
	fmt.Println("CheckAllFiles is ok! ")
	return nil
}
func (self *CheckVer) CheckAllGoFiles() error {
	var fs, err = self.FindAllGoFile()
	if err != nil {
		logrus.Error("CheckAllGoFiles is err! " + err.Error())
		return err
	}

	err = self.CheckFiles(fs)

	if err != nil {
		logrus.Error("\r\n" + err.Error())
		return err
	}
	fmt.Println("CheckAllGoFiles is ok! ")
	return nil
}
func (self *CheckVer) CheckDockerFiles() error {
	var fs, err = self.FindDockerFile()
	if err != nil {
		logrus.Error("CheckDockerFiles is err! " + err.Error())
		return err
	}

	err = self.CheckFiles(fs)

	if err != nil {
		logrus.Error("\r\n" + err.Error())
		return err
	}
	fmt.Println("CheckDockerFiles is ok! ")
	return nil
}

func (self *CheckVer) CheckFiles(files []string) error {
	for _, file := range files {

		var err = self.CheckOneFile(file)
		if err != nil {
			return err
		}

	}
	return nil
}
func (self *CheckVer) FindDockerFile() ([]string, error) {
	//check all , dir ,  one dir
	return self.FindAllFiles(fileutils.FindRootDir() + "/docker")

}
func (self *CheckVer) FindAllFile() ([]string, error) {

	return self.FindAllFiles(fileutils.FindRootDir())

}
func (self *CheckVer) FindAllGoFile() ([]string, error) {

	return self.FindAllGoFiles(fileutils.FindRootDir())

}
func (self *CheckVer) CheckOneFile(file string) error {
	//check all , dir ,  one dir
	str, err := fileutils.ReadFileToString(file)
	if err != nil {
		return errors.New(err.Error() + file)
	}
	if strings.Contains(str, ERR_FLAG) && !strings.Contains(str, "ERR_FLAG") {
		return errors.New("check merge file error! " + file + " has merge error ! " + ERR_FLAG)
	}
	return nil //fileutils.FindAllFiles(fileutils.FindRootDir() + "/docker")

}
func (self *CheckVer) FindAllFiles(dirPath string) ([]string, error) {
	self.SetDir(dirPath)
	self.SetExcludeSuffix(".exe,.docx")
	return self.FindFiles()

}
func (self *CheckVer) FindAllGoFiles(dirPath string) ([]string, error) {
	self.SetDir(dirPath)
	self.SetIncludeSuffix(".go")
	return self.FindFiles()

}
func (self *CheckVer) CountAllGoFile() ([]string, error) {

	return self.FindAllGoFiles(fileutils.FindRootDir())

}

func (self *CheckVer) CountOneFile(filePath string) int64 {

	// 使用os.Stat获取文件信息
	fileInfo, err := os.Stat(filePath)
	if err != nil {
		golog.Error("Error getting file info:", err.Error())
		return 0
	}
	// 获取文件大小
	size := fileInfo.Size()
	golog.Info("The size of the file is bytes : ", size)
	return size
}
func (self *CheckVer) CountFiles(filePaths []string) int64 {

	var c int64
	for _, file := range filePaths {
		c += self.CountOneFile(file)
	}
	return c
}
func (self *CheckVer) CountAllFiles() int64 {
	var filePaths, err = self.FindAllGoFile()
	if err != nil {
		golog.Error(err)
		return 0
	}
	var c int64
	for _, file := range filePaths {
		c += self.CountOneFile(file)
	}
	return c
}
