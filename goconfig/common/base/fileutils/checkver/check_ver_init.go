package checkver

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: check_merge_inf_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-06 16:29:47)
	@Update 作者: leijmdas@163.com 时间(2024-09-06 16:29:47)

* *********************************************************/

const singleNameCheckMergeInf = "*checkver.CheckVer-9c706d23fe124659b59805d0afd0f34c"

// init register load
func init() {
	registerBeanCheckMergeInf()
}

// register CheckVer
func registerBeanCheckMergeInf() {
	basedi.RegisterLoadBean(singleNameCheckMergeInf, LoadCheckMergeInf)
}

func FindBeanCheckMergeInf() *CheckVer {
	bean, ok := basedi.FindBean(singleNameCheckMergeInf).(*CheckVer)
	if !ok {
		logrus.Errorf("FindBeanCheckMergeInf: failed to cast bean to *CheckVer")
		return nil
	}
	return bean

}

func LoadCheckMergeInf() baseiface.ISingleton {
	var s = NewCheckVer()
	InjectCheckMergeInf(s)
	return s

}

func InjectCheckMergeInf(s *CheckVer) {

	// // logrus.Debug("inject")
}
