package checkver

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"os"
	"testing"
)

type TestCheckMergeInfSuite struct {
	suite.Suite

	inst    *CheckVer
	rootdir string
	dbtype  string
	cfg     *ichubconfig.IchubConfig
}

func TestCheckMergeInfSuites(t *testing.T) {
	suite.Run(t, new(TestCheckMergeInfSuite))
}

func (suite *TestCheckMergeInfSuite) SetupSuite() {
	logrus.Info(" setup suite")

	suite.inst = FindBeanCheckMergeInf()
	suite.rootdir = fileutils.FindRootDir()
	suite.cfg = ichubconfig.FindBeanIchubConfig()
	suite.dbtype = suite.cfg.ReadIchubDb().Dbtype
	// suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_MYSQL
	suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_COCKROACH

}

func (suite *TestCheckMergeInfSuite) TearDownSuite() {
	logrus.Info(" teardown suite")
	//	suite.cfg.Gorm.DbType = suite.dbtype
	//	var dto = suite.cfg.ReadIchubDb()
	//	logrus.Info(dto)

}
func (suite *TestCheckMergeInfSuite) SetupTest() {
	logrus.Info(" setup test")
}

func (suite *TestCheckMergeInfSuite) TearDownTest() {
	logrus.Info(" teardown test")
}

func (suite *TestCheckMergeInfSuite) Test001_findAllfiles() {
	logrus.Info("   Test001_findAllfiles")
	var fs, err = suite.inst.FindAllFiles(suite.rootdir + "/docker")
	if err != nil {
		logrus.Error(err)
		return
	}
	logrus.Info(fs)
}
func (suite *TestCheckMergeInfSuite) Test002_checkDockerfiles() {
	logrus.Info("   Test002_checkDockerfiles")

	suite.inst.CheckDockerFiles()
}
func (suite *TestCheckMergeInfSuite) Test003_checkAllfiles() {
	logrus.Info("   Test003_checkAllfiles")

	suite.inst.CheckAllFiles()
}
func (suite *TestCheckMergeInfSuite) Test004_checkAllgofiles() {
	logrus.Info("  Test004_checkAllgofiles")

	suite.inst.CheckAllGoFiles()
}
func (suite *TestCheckMergeInfSuite) Test004_countWorld() {
	var files, err = suite.inst.FindAllGoFile()
	if err != nil {
		golog.Error(err)
		return
	}
	// 替换为你要统计大小的文件路径
	filePath := files[0] //"path/to/your/file"

	// 使用os.Stat获取文件信息
	fileInfo, err := os.Stat(filePath)
	if err != nil {
		golog.Error("Error getting file info:", err.Error())
		return
	}
	// 获取文件大小
	size := fileInfo.Size()
	fmt.Printf("The size of the file is: %d bytes\n", size)

}
func (suite *TestCheckMergeInfSuite) Test005_countWorld() {
	var files, err = suite.inst.FindAllGoFile()
	if err != nil {
		golog.Error(err)
		return
	}

	var c = suite.inst.CountFiles(files)
	golog.Info(c / 1024)

}
func (suite *TestCheckMergeInfSuite) Test005_countAllWorld() {

	var c = suite.inst.CountAllFiles()
	golog.Info(c / 1024)

}
