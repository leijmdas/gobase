package goconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gomini_filecfg_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-05 16:57:51)
	@Update 作者: leijmdas@163.com 时间(2024-05-05 16:57:51)

* *********************************************************/

const singleNameGominiFilecfg = "goconfig.GominiFilecfg"

// init register load
func init() {
	registerBeanGominiFilecfg()
}

// register GominiFilecfg
func registerBeanGominiFilecfg() {
	basedi.RegisterLoadBean(singleNameGominiFilecfg, LoadGominiFilecfg)
}

func FindBeanGominiFilecfg() *GominiFilecfg {
	bean, ok := basedi.FindBean(singleNameGominiFilecfg).(*GominiFilecfg)
	if !ok {
		logrus.Errorf("FindBeanGominiFilecfg: failed to cast bean to *GominiFilecfg")
		return nil
	}
	return bean

}

func LoadGominiFilecfg() baseiface.ISingleton {
	var s = Default()
	InjectGominiFilecfg(s)
	return s

}

func InjectGominiFilecfg(s *GominiFilecfg) {

	// logrus.Debug("inject")
}
