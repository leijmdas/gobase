package goconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"github.com/stretchr/testify/assert"
	"testing"
)

const PkgName = "gitee.com/leijmdas/gobase/goconfig/common/configdto/baseutils"

var gf *GominiFilecfg //= FindBeanGominiFilecfg()
func init() {
	gf = FindBeanGominiFilecfg()
}
func Test001_FindPkgPath(t *testing.T) {

	var pkgPath = gf.FindRootDir()
	goutils.Info(pkgPath)
	assert.Equal(t, true, pkgPath != "")
}

func Test002_FindPkg(t *testing.T) {

	var pkgPath, _ = gf.FindPkg("gitee.com/leijmdas/gobase/godi/di/godifactroy")
	goutils.Info("pkg name=", pkgPath.Dir)
	assert.Equal(t, true, pkgPath.Dir != "")
}
func Test003_FindPkgDir(t *testing.T) {
	var pkgPath, _ = gf.FindPkg("gitee.com/leijmdas/gobase/goconfig/common/configdto/baseutils")
	goutils.Info("pkg name=", pkgPath.Dir)
	assert.Equal(t, true, pkgPath.Dir != "")
}
func Test004_FindPkg(t *testing.T) {
	//gf = FindBeanGominiFilecfg()
	gf.BasePkgName = PkgName
	var pkgPath, _ = gf.FindPkgRoot()
	goutils.Info("pkg name=", pkgPath)
	assert.Equal(t, true, pkgPath != "")

}
func Test005_FindPkg(t *testing.T) {
	//goconfig.InitPkg(PkgName)
	goutils.Info(1)
}
