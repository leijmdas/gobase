package fileutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameGofileUtil = "GofileUtil"

// init
// register
// load
func init() {
	registerBeanGofileUtil()
}

// register GofileUtil
func registerBeanGofileUtil() {
	basedi.RegisterLoadBean(singleNameGofileUtil, LoadGofileUtil)
}

func FindBeanGofileUtil() *GofileUtil {
	return basedi.FindBean(singleNameGofileUtil).(*GofileUtil)
}

func LoadGofileUtil() baseiface.ISingleton {
	return NewGofileUtil()
}
