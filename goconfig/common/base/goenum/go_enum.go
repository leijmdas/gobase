package goenum

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

func MakeEnum(codes []string, stype ...string) {

}

type GoEnum struct {
	basedto.BaseEntity
	Id       int
	Code     string
	Msg      string //remark
	EnumType string
}

func NewGoEnum() *GoEnum {
	var ge = &GoEnum{}
	ge.InitProxy(ge)
	return ge
}
