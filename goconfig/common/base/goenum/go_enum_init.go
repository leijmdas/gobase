package goenum

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_enum_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-11 10:33:57)
	@Update 作者: leijmdas@163.com 时间(2024-08-11 10:33:57)

* *********************************************************/

const singleNameGoEnum = "*goenum.GoEnum-0f42b8fec1b24162ac505e414f1fe211"

// init register load
func init() {
	registerBeanGoEnum()
}

// register GoEnum
func registerBeanGoEnum() {
	basedi.RegisterLoadBean(singleNameGoEnum, LoadGoEnum)
}

func FindBeanGoEnum() *GoEnum {
	bean, ok := basedi.FindBean(singleNameGoEnum).(*GoEnum)
	if !ok {
		logrus.Errorf("FindBeanGoEnum: failed to cast bean to *GoEnum")
		return nil
	}
	return bean

}

func LoadGoEnum() baseiface.ISingleton {
	var s = NewGoEnum()
	InjectGoEnum(s)
	return s

}

func InjectGoEnum(s *GoEnum) {

	// goutils.Debug("inject")
}
