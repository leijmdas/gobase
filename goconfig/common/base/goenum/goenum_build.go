package goenum

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

type GoenumBuild struct {
	basedto.BaseEntitySingle
	basedi.BeanInfo
}

func NewGoenumBuild() *GoenumBuild {
	return &GoenumBuild{
		//	container: map[string]string{},
	}
}

func (self *GoenumBuild) Build(code string, msgs ...string) {
	var msg = code
	if len(msgs) > 0 {
		msg = msgs[0]
	}
	var enum = FindBeanGoEnum()
	enum.Code = code
	enum.Msg = msg
	//self.container[code] = msg
}
