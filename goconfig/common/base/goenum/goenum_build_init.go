package goenum

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: goenum_build_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-11 10:37:30)
	@Update 作者: leijmdas@163.com 时间(2024-08-11 10:37:30)

* *********************************************************/

const singleNameGoenumBuild = "*goenum.GoenumBuild-0aacbf8cdeae4cd483d55ef6b4665c73"

// init register load
func init() {
	registerBeanGoenumBuild()
}

// register GoenumBuild
func registerBeanGoenumBuild() {
	basedi.RegisterLoadBean(singleNameGoenumBuild, LoadGoenumBuild)
}

func FindBeanGoenumBuild() *GoenumBuild {
	bean, ok := basedi.FindBean(singleNameGoenumBuild).(*GoenumBuild)
	if !ok {
		logrus.Errorf("FindBeanGoenumBuild: failed to cast bean to *GoenumBuild")
		return nil
	}
	return bean

}

func LoadGoenumBuild() baseiface.ISingleton {
	var s = NewGoenumBuild()
	InjectGoenumBuild(s)
	return s

}

func InjectGoenumBuild(s *GoenumBuild) {

	// goutils.Debug("inject")
}
