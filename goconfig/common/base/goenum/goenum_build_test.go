package goenum

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestGoenumBuildSuite struct {
	suite.Suite
	inst *GoenumBuild

	rootdir string
}

func (this *TestGoenumBuildSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanGoenumBuild()

	this.rootdir = fileutils.FindRootDir()

}

func TestGoenumBuildSuites(t *testing.T) {
	suite.Run(t, new(TestGoenumBuildSuite))
}

func (this *TestGoenumBuildSuite) Test001_Build() {
	logrus.Info(1)
	FindBeanGoenumBuild().Build("abc")
}
