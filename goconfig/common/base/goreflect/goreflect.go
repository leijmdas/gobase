package goreflect

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"github.com/morrisxyang/xreflect"
	"github.com/sirupsen/logrus"
	"reflect"
)

// cc cv gocloc

func FindNameOfIface(iface any) string {

	// 使用reflect.TypeOf获取接口的动态类型
	ifaceType := reflect.TypeOf(iface)
	// 检查是否是指针类型
	if ifaceType.Kind() == reflect.Ptr {
		// 取指针指向的类型
		ifaceType = ifaceType.Elem()
	}
	// 获取类型名称
	return ifaceType.Name()

}
func FindPkgNameOfIface(iface any) string {

	// 使用reflect.TypeOf获取接口的动态类型
	ifaceType := reflect.TypeOf(iface)
	// 检查是否是指针类型
	if ifaceType.Kind() == reflect.Ptr {
		// 取指针指向的类型
		ifaceType = ifaceType.Elem()
	}
	// 获取类型名称
	return ifaceType.PkgPath() + "." + ifaceType.Name()

}
func IfProxy(some any) bool {
	if some == nil {
		return false
	}
	if i, ok := some.(baseiface.IbaseProxy); ok {
		i.InitProxy(i)
		return ok
	}
	return false

}
func NewOfPtrTypeProxy[T baseiface.IbaseProxy]() T {

	return NewOfPtrType[T]()

}

// T is *type
func NewOfPtrType[T baseiface.IpoolObj]() T {
	var baseptoxy T
	var typeOf = reflect.TypeOf(baseptoxy)
	if typeOf.Kind() == reflect.Ptr {
		typeOf = typeOf.Elem()
	}
	var value = reflect.New(typeOf)
	baseptoxy = value.Interface().(T)
	IfProxy(baseptoxy)
	baseptoxy.Init()
	return baseptoxy

}

func LoadBeanProxy[T baseiface.IbaseProxy]() T {
	var baseproxy T

	var value = reflect.New(reflect.TypeOf(baseproxy).Elem())
	baseproxy = value.Interface().(T)
	baseproxy.InitProxy(baseproxy)
	baseproxy.Init()

	return baseproxy

}

// no used
func NewOfType[T baseiface.IpoolObj]() T {

	return NewOfPtrType[T]()
}

func IfPtrNil(v any) bool {
	if v == nil {
		return true
	}
	if ptrValue := reflect.ValueOf(v); ptrValue.Kind() == reflect.Ptr {

		return ptrValue.IsNil()
	}

	return false

}

func IfIface[T any](some any) bool {

	_, ok := some.(T)
	return ok

}
func IfInterface[T any](some any) (T, bool) {

	v, ok := some.(T)
	return v, ok

}
func NewStru[S any]() *S {
	var s = new(S)
	IfProxy(s)
	return s
}
func NewStruProxy[S baseiface.IbaseProxy]() S {

	return NewOfPtrTypeProxy[S]()
}
func IfStru(value any) bool {

	var v = reflect.ValueOf(value)
	if v.Kind() == reflect.Struct {
		return true
	}
	if v.Kind() == reflect.Ptr && v.Elem().Kind() == reflect.Struct {
		return true
	}
	return false

}
func IfValueStru(v reflect.Value) bool {

	if v.Kind() == reflect.Struct {
		return true
	}
	if v.Kind() == reflect.Ptr && v.Elem().Kind() == reflect.Struct {
		return true
	}
	return false

}
func IfTypeStru(v reflect.Type) bool {

	if v.Kind() == reflect.Struct {
		return true
	}
	if v.Kind() == reflect.Ptr && v.Elem().Kind() == reflect.Struct {
		return true
	}
	return false

}
func FindStructFields(v any) []reflect.StructField {
	var fields = make([]reflect.StructField, 0)
	if !IfTypeStru(reflect.TypeOf(v)) {
		logrus.Info("v is not stru")
		return fields
	}

	var t = reflect.TypeOf(v)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	for i := 0; i < t.NumField(); i++ {
		if t.Field(i).Type.Kind() == reflect.Ptr &&
			t.Field(i).Type.Elem().Kind() == reflect.Struct {

			fields = append(fields, t.Field(i))
			continue
		}
		if t.Field(i).Type.Kind() == reflect.Struct {

			fields = append(fields, t.Field(i))
			continue
		}
	}

	return fields

}

func NewProxyType(t reflect.Type) (baseiface.IbaseProxy, bool) {

	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	var proxy = reflect.New(t)
	if iproxy, ok := proxy.Interface().(baseiface.IbaseProxy); ok {
		IfProxy(iproxy)
		iproxy.Init()
		return iproxy, true
	}

	return nil, false
}

func Autowired(some any) error {

	var fields = FindStructFields(some)
	for _, field := range fields {
		var t = field.Type
		if t.Kind() == reflect.Ptr {
			var baseproxy, ok = NewProxyType(t)
			if ok {
				err := xreflect.SetField(some, field.Name, baseproxy)
				if err != nil {
					logrus.Error(err)
					continue
				}
			}
		}
	}

	return nil
}

func ContainsStru[T any](someStru interface{}) bool {
	value := reflect.ValueOf(someStru)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}

	if value.Kind() != reflect.Struct {
		return false
	}
	var childStru T
	var tt = reflect.TypeOf(childStru)
	if tt.Kind() == reflect.Ptr {
		tt = tt.Elem()
	}
	for i := 0; i < value.NumField(); i++ {
		if value.Field(i).Type().Name() == tt.Name() {
			return true
		}
	}

	return false
}

//	func ContainsGometa(some interface{}) bool {
//		return ContainsStru[basedto.Gometa](some)
//
// }
func ContainsStruPtr(someStru, childStru any) bool {
	someValue := reflect.ValueOf(someStru)
	if someValue.Kind() == reflect.Ptr {
		someValue = someValue.Elem()
	}

	if someValue.Kind() != reflect.Struct {
		return false
	}

	var tt = reflect.TypeOf(childStru)
	if tt.Kind() == reflect.Ptr {
		tt = tt.Elem()
	}
	for i := 0; i < someValue.NumField(); i++ {
		if someValue.Field(i).Type().Name() == tt.Name() {
			return true
		}
	}

	return false
}
func InitStruNilPtrFields(Stru any) {

	for _, field := range FindStruNilPtrFields(Stru) {

		switch field.Type.Elem().Kind() {
		case reflect.Int:
			xreflect.SetField(Stru, field.Name, new(int))
		case reflect.Int32:
			xreflect.SetField(Stru, field.Name, new(int32))
		case reflect.Int64:
			xreflect.SetField(Stru, field.Name, new(int64))
		case reflect.Uint:
			xreflect.SetField(Stru, field.Name, new(uint))
		case reflect.Uint32:
			xreflect.SetField(Stru, field.Name, new(uint32))
		case reflect.Uint64:
			xreflect.SetField(Stru, field.Name, new(uint64))
		case reflect.String:
			xreflect.SetField(Stru, field.Name, new(string))
		case reflect.Bool:
			xreflect.SetField(Stru, field.Name, new(bool))
		}
	}

}
func FindStruNilPtrFields(Stru any) []reflect.StructField {

	var fields = make([]reflect.StructField, 0)
	if Stru == nil {
		return fields
	}
	if !IfTypeStru(reflect.TypeOf(Stru)) {
		return fields
	}
	value := reflect.ValueOf(Stru)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}
	var struType = reflect.TypeOf(Stru)
	if struType.Kind() == reflect.Ptr {
		struType = struType.Elem()
	}

	for i := 0; i < struType.NumField(); i++ {
		var field = struType.Field(i)
		if field.Type.Kind() == reflect.Ptr && value.FieldByName(field.Name).IsNil() {
			fields = append(fields, field)
		}
	}

	return fields

}

func IfNilPtrChild(Stru any, feild string) bool {

	if Stru == nil {
		return false
	}
	if !IfTypeStru(reflect.TypeOf(Stru)) {
		return false
	}
	value := reflect.ValueOf(Stru)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}
	var struType = reflect.TypeOf(Stru)
	if struType.Kind() == reflect.Ptr {
		struType = struType.Elem()
	}

	for i := 0; i < struType.NumField(); i++ {
		var field = struType.Field(i)
		if field.Name == feild && field.Type.Kind() == reflect.Ptr && value.FieldByName(field.Name).IsNil() {
			return true
		}
	}

	return false

}
