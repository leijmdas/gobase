package goreflect

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/sirupsen/logrus"
	"testing"
)

type Dpt struct {
	basedto.BaseEntity
	Id   int
	Name string
}

func NewDpt() *Dpt {
	return &Dpt{}
}

func Test001_if(t *testing.T) {
	logrus.Info(1)
	var d = IfProxy(NewDpt())
	logrus.Info(d)

}
func Test002_ifPtrNil(t *testing.T) {
	logrus.Info(1)
	var d = IfPtrNil(NewDpt())
	logrus.Info(d)

}
func NeBaseProxy[S baseiface.IbaseProxy]() S {
	var s = NewOfPtrTypeProxy[S]()
	s.InitProxy(s)
	return s
}
func Test003_newStru(t *testing.T) {
	var a = NewStru[int]()

	logrus.Info(*a)

}
func Test004_newStru(t *testing.T) {

	var a = NewStru[ichubconfig.IchubConfig]()
	logrus.Info(a.Read())

}
func Test004_newStruSSS(t *testing.T) {

	var a = NewStru[ichubconfig.IchubConfig]()
	logrus.Info(IfIface[baseiface.IbaseProxy](a.Read()))

}
func Test004_newStruSSS1(t *testing.T) {

	var a = NewStruProxy[*ichubconfig.IchubConfig]()
	logrus.Info(IfIface[baseiface.IbaseProxy](a.Read()))

}
