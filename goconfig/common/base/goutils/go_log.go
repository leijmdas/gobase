package goutils

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/glog"
	"github.com/sirupsen/logrus"
)

/*
 * @Description:
 * @FilePath: \goconfig\common\configdto\baseutils\golog\go_log.go
 */

const (
	RotateSize     = "100M" //1024 * 1024 * 20
	logfile_rule   = "gosys_rule.log"
	logfile_stat   = "gosys_stat.log"
	logfile_cli    = "gosys_cli.log"
	logfile_server = "gosys.log"
	logfile_err    = "gosys_err.log"
)

// https://blog.csdn.net/weixin_51261234/article/details/124504638
type GoLog struct {
	basedto.BaseEntitySingle

	Name   string
	logdir string
	*glog.Logger
}

func NewGoLog() *GoLog {
	var log = &GoLog{
		Logger: glog.New(),
	}

	return log.init()
}
func (l *GoLog) init() *GoLog {
	l.Logger.SetConfigWithMap(g.Map{
		"path":                 fileutils.FindBeanGofileUtil().FindRootDir() + "/logs",
		"level":                "all",
		"file":                 logfile_server,
		"stdout":               true,
		"StStatus":             0,
		"RotateSize":           RotateSize,
		"RotateBackupLimit":    10,
		"RotateBackupExpire":   "7d",
		"RotateBackupCompress": 9,
	})
	l.Logger.SetWriterColorEnable(true)

	return l

}

func (l *GoLog) SetLevelErr() *GoLog {

	l.Logger.SetLevel(glog.LEVEL_ERRO)
	return l

}

func (l *GoLog) SetLevel(level int) *GoLog {

	l.Logger.SetLevel(level)
	return l

}
func IfNil(v ...any) {
	for i := 0; i < len(v); i++ {
		if baseutils.IfPtrNil(v[i]) {

			v[i] = "nil"
		}
	}
}
func Info(v ...interface{}) {
	IfNil(v...)
	FindBeanGoLog().Info(context.Background(), v...)

}
func Debug(v ...interface{}) {
	IfNil(v...)
	FindBeanGoLog().Debug(context.Background(), v...)
	logrus.Info(v...)
}
