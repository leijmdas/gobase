package goutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title  文件名称: go_log_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
	@Update 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
*/

const singleNameGoLog = "goutils.GoLog"

// init register load
func init() {
	registerBeanGoLog()
}

// register GoLog
func registerBeanGoLog() {
	basedi.RegisterLoadBean(singleNameGoLog, LoadGoLog)
}

func FindBeanGoLog() *GoLog {
	bean, ok := basedi.FindBean(singleNameGoLog).(*GoLog)
	if !ok {
		logrus.Errorf("FindBeanGoLog: failed to cast bean to *GoLog")
		return nil
	}
	return bean

}

func LoadGoLog() baseiface.ISingleton {
	var s = NewGoLog()
	InjectGoLog(s)
	return s

}

func InjectGoLog(s *GoLog) {

	//// logrus.Debug("inject")
}
