package goutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestGoLogSuite struct {
	suite.Suite
	inst *GoLog

	rootdir string
}

func (this *TestGoLogSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanGoLog()

	this.rootdir = fileutils.FindRootDir()

}

func TestGoLogSuites(t *testing.T) {
	suite.Run(t, new(TestGoLogSuite))
}

func (this *TestGoLogSuite) Test001_init() {
	logrus.Info(1)

}

func (this *TestGoLogSuite) Test002_SetLevelErr() {
	logrus.Info(1)

}

func (this *TestGoLogSuite) Test003_SetLevel() {
	logrus.Info(1)

}
