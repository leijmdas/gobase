package goutils

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type GoLogcli struct {
	basedto.BaseEntitySingle
	GoLog
}

func NewGoLogcli() *GoLogcli {
	var cli = &GoLogcli{
		GoLog: *NewGoLog(),
	}
	cli.SetFile(logfile_cli)
	return cli

}
