package goutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title  文件名称: go_logcli_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
	@Update 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
*/

const singleNameGoLogcli = "goutils.GoLogcli"

// init register load
func init() {
	registerBeanGoLogcli()
}

// register GoLogcli
func registerBeanGoLogcli() {
	basedi.RegisterLoadBean(singleNameGoLogcli, LoadGoLogcli)
}

func FindBeanGoLogcli() *GoLogcli {
	bean, ok := basedi.FindBean(singleNameGoLogcli).(*GoLogcli)
	if !ok {
		logrus.Errorf("FindBeanGoLogcli: failed to cast bean to *GoLogcli")
		return nil
	}
	return bean

}

func LoadGoLogcli() baseiface.ISingleton {
	var s = NewGoLogcli()
	InjectGoLogcli(s)
	return s

}

func InjectGoLogcli(s *GoLogcli) {

	//// logrus.Debug("inject")
}
