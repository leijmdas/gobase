package goutils

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/v2/os/glog"
	"github.com/sirupsen/logrus"
)

type GoLogerr struct {
	basedto.BaseEntitySingle
	*GoLog
}

func NewGoLogServerErr() *GoLogerr {
	var g = &GoLogerr{
		GoLog: NewGoLog(),
	}
	g.Logger.SetFile(logfile_err)
	g.Logger.SetLevel(glog.LEVEL_ERRO)
	return g
}
func Error(v ...interface{}) {
	IfNil(v...)
	FindBeanGoLogerr().Error(context.Background(), v...)
	logrus.Error(v...)
}
func Warning(v ...interface{}) {
	IfNil(v...)
	FindBeanGoLogerr().Warning(context.Background(), v...)

}
