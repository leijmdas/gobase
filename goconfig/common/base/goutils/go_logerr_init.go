package goutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title  文件名称: go_logerr_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
	@Update 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
*/

const singleNameGoLogerr = "goutils.GoLogerr"

// init register load
func init() {
	registerBeanGoLogerr()
}

// register GoLogerr
func registerBeanGoLogerr() {
	basedi.RegisterLoadBean(singleNameGoLogerr, LoadGoLogerr)
}

func FindBeanGoLogerr() *GoLogerr {
	bean, ok := basedi.FindBean(singleNameGoLogerr).(*GoLogerr)
	if !ok {
		logrus.Errorf("FindBeanGoLogerr: failed to cast bean to *GoLogerr")
		return nil
	}
	return bean

}

func LoadGoLogerr() baseiface.ISingleton {
	var s = NewGoLogServerErr()
	InjectGoLogerr(s)
	return s

}

func InjectGoLogerr(s *GoLogerr) {

	//// logrus.Debug("inject")
}
