package goutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/os/glog"
)

type GoLogrule struct {
	basedto.BaseEntitySingle
	*GoLog
}

func NewGoLogrule() *GoLogrule {
	var g = &GoLogrule{
		GoLog: NewGoLog(),
	}
	g.Logger.SetFile(logfile_rule)
	g.Logger.SetStdoutPrint(false)
	g.Logger.SetLevel(glog.LEVEL_INFO)
	return g
}
