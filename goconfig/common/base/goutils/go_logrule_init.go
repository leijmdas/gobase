package goutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: go_logrule_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-09-12 09:56:30
	@Update  作者:    leijmdas@163.com  时间: 2024-09-12 09:56:30
*/

var singleNameGoLogrule = "*golog.GoLogrule-2d392491-d52e-42cf-b73c-b489ff16828f"

// init register load
func init() {
	registerBeanGoLogrule()
}

// register GoLogrule
func registerBeanGoLogrule() {
	basedi.RegisterLoadBean(singleNameGoLogrule, LoadGoLogrule)
}

// FindBeanGoLogrule
func FindBeanGoLogrule() *GoLogrule {

	if bean, ok := basedi.FindBeanOk(singleNameGoLogrule); ok {
		return bean.(*GoLogrule)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadGoLogrule() baseiface.ISingleton {
	var inst = NewGoLogrule()
	InjectGoLogrule(inst)
	return inst

}

func InjectGoLogrule(s *GoLogrule) {

	//logrus.Info("inject")
}
