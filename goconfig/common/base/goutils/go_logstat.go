package goutils

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/v2/os/glog"
)

type GoLogstat struct {
	basedto.BaseEntitySingle
	*GoLog
}

func NewGoLogStat() *GoLogstat {
	var g = &GoLogstat{
		GoLog: NewGoLog(),
	}
	g.Logger.SetFile(logfile_stat)
	g.Logger.SetStdoutPrint(false)
	g.Logger.SetLevel(glog.LEVEL_INFO)
	return g
}

func Stat(v ...interface{}) {
	IfNil(v...)
	FindBeanGoLogstat().Info(context.Background(), v...)

}
