package goutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title  文件名称: go_logstat_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
	@Update 作者: leijmdas@163.com 时间(2024-05-03 22:14:33)
*/

const singleNameGoLogstat = "goutils.GoLogstat"

// init register load
func init() {
	registerBeanGoLogstat()
}

// register GoLogstat
func registerBeanGoLogstat() {
	basedi.RegisterLoadBean(singleNameGoLogstat, LoadGoLogstat)
}

func FindBeanGoLogstat() *GoLogstat {
	bean, ok := basedi.FindBean(singleNameGoLogstat).(*GoLogstat)
	if !ok {
		logrus.Errorf("FindBeanGoLogstat: failed to cast bean to *GoLogstat")
		return nil
	}
	return bean

}

func LoadGoLogstat() baseiface.ISingleton {
	var s = NewGoLogStat()
	InjectGoLogstat(s)
	return s

}

func InjectGoLogstat(s *GoLogstat) {

	//// logrus.Debug("inject")
}
