package jsonutils

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/gogf/gf/v2/util/gconv"
	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"
)

type JsonUtils struct {
}

func ToJson(v any) (string, error) {

	result, err := ToJsonBytes(v)
	return string(result), err
}

func ToJsonBytes(v any) ([]byte, error) {
	result, err := jsoniter.MarshalIndent(v, "", "  ")
	if err != nil {
		logrus.Println(err)
		return []byte("{}"), err
	}
	return result, nil
}

func FromJson(str string, obj any) error {

	d := jsoniter.NewDecoder(bytes.NewBufferString(str))
	d.UseNumber()
	err := d.Decode(obj)
	if err != nil {
		logrus.Error(err)
	}
	return err
}
func FromJsonByte(inbyte []byte, obj any) error {
	d := jsoniter.NewDecoder(bytes.NewBuffer(inbyte))
	d.UseNumber()
	err := d.Decode(obj)
	if err != nil {
		logrus.Error(err)
	}
	return err
}
func ToJsonStr(v any) string {
	result, err := jsoniter.Marshal(v)
	if err != nil {
		logrus.Error(err)
		return "{}"
	}
	return string(result)
}

func ToJsonPretty(v any) string {
	result, err := jsoniter.MarshalIndent(v, "", "     ")
	if err != nil {
		logrus.Error(err)
		return "{}"
	}
	return string(result)
}

func FromJsonStr(param []byte) any {
	var ret any
	_ = jsoniter.Unmarshal(param, &ret)
	return ret
}

func MapFromJson(param string) (m *map[string]any, err error) {
	m = &map[string]any{}
	err = FromJson(param, m)
	return m, err
}

func Str2JsonPretty(txt string) string {
	v := FromJsonStr([]byte(txt))
	return ToJsonPretty(v)
}

func Byte2JsonPretty(b []byte) string {
	v := FromJsonStr(b)
	return ToJsonPretty(v)
}
func JsonFromStr(v string) string {
	var params = make(map[string]any, 0)
	FromJson(v, &params)
	return ToJsonPretty(params)
}
func Parse2Map(src string) (map[string]any, error) {
	if src == "" {
		return nil, errors.New("src is empty")
	}
	var res = make(map[string]any)

	var err = FromJson(src, &res)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	logrus.Info(res)
	return res, nil

}
func ParseArray2Map(stru []any) (any, error) {
	var m = make([]map[string]any, 0)
	FromJson(ToJsonPretty(stru), &m)
	return m, nil

}
func Stru2Map(stru any) map[string]any {

	var m = make(map[string]any, 0)
	FromJson(ToJsonPretty(stru), &m)
	return m
}
func Stru2MapExclude(stru any, excludeFields ...string) map[string]any {

	var m = Stru2Map(stru)
	for _, key := range excludeFields {
		delete(m, key)
	}
	return m
}

func Decode2Stru(some, out any) error {
	if _, ok := some.(string); ok {
		var rets = map[string]any{}
		err := FromJson(some.(string), &rets)
		if err != nil {
			return err
		}
		return gconv.Struct(rets, out)
	}
	return gconv.Struct(some, out)
}
func Decode2StruList(some, out any) error {
	if _, ok := some.([]map[string]any); ok {
		return gconv.Structs(some, out)
	}
	var rets = []map[string]any{}
	if _, ok := some.(string); ok {
		err := FromJson(some.(string), &rets)
		if err != nil {
			return err
		}
	} else {
		err := FromJson(ToJsonPretty(some), &rets)
		if err != nil {
			return err
		}
	}
	return gconv.Structs(rets, out)

}

func Check(self any, key string, exp any) bool {
	fmt.Println(" check  key = exp ", key, " : ", exp)
	var data = Stru2Map(self)
	if data[key] == nil {
		return false
	}
	return gconv.String(data[key]) == gconv.String(exp)
}
