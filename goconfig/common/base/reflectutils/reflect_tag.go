package reflectutils

import "strings"

// Db int32 `env:"${web.server.name}"
// gorm:"column:id;type:int(11);PRIMARY_KEY;comment:”"
// json:"id"`
type ReflectTag struct {
	Tag      string //env gorm json
	Field    string //id
	TagName  string //${web.server.name}
	TagValue string //web.server.com
}

func NewReflectTag() *ReflectTag {
	return &ReflectTag{}

}

func (this *ReflectTag) ParseTag(tagstr string) *ReflectTag {

	tags := strings.Split(tagstr, ":")
	if len(tags) > 1 {
		this.Tag = tags[1]
	}
	return this
}

func (this *ReflectTag) ParseEnv(tagstr string) *ReflectTag {

	tags := strings.Split(tagstr, ":")
	if len(tags) > 1 {
		this.Tag = tags[1]
	}
	return this
}
