package reflectutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/sirupsen/logrus"
	"testing"
)

type Department struct {
	Id int32 `env:"${web.server.name}" gorm:"column:id;type:int(11);PRIMARY_KEY;comment:''" json:"id"`
	/*  编码  */
	Code string `env:"${web.server.port}" gorm:"column:code;type:varchar(32);comment:'code';default:'none'" json:"code"`
}

func NewDepartment() *Department {
	return &Department{}
}

func Test001_tagGorm(t *testing.T) {
	clientDto := NewDepartment()
	reflectTags := NewReflectTags()
	reflectTags.ParseGorm(clientDto)
	reflectTags.Log()

}

func Test002_tagEnv(t *testing.T) {
	clientDto := NewDepartment()
	reflectTags := NewReflectTags()
	reflectTags.ParseEnv(clientDto)
	//reflectTags.Log()
	reflectTags.TagMap().Each(func(key interface{}, value interface{}) {
		logrus.Info(key, "=", value)

	})

}
func Test003_CfgNilSoimegEnv(t *testing.T) {
	var cfg = ichubconfig.FindBeanIchubConfig()
	cfg.InitProxy(nil)
	golog.Info(cfg)

}
