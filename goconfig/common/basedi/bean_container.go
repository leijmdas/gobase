package basedi

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"github.com/sirupsen/logrus"
	"reflect"
	"sync"
)

func RegisterLoadBean(name string, loadbean baseiface.LoadBean) error {

	container.RegisterLoadBean(name, loadbean)
	return nil
}

func RegisterBean(name string, bean baseiface.ISingleton) {
	container.RegisterBean(name, bean)
}

func FindBean(name string) baseiface.ISingleton {
	var s, _ = FindBeanOk(name)
	return s

}
func FindBeanOk(name string) (baseiface.ISingleton, bool) {
	var beanInfo, ok = container.FindBean(name)
	if beanInfo == nil {
		return nil, ok
	}
	if beanInfo.bean.Single() {
		return beanInfo.Bean(), ok
	}
	return beanInfo.CreateBean(), ok
}

func FindBeanInst[T any]() T {
	var t T
	var instType = reflect.TypeOf(t)
	var intance, ok = FindBeanOk(instType.String()[1:])
	if !ok {
		intance, ok = FindBeanOk(instType.String())
	}
	return intance.(T)
}
func FindBeanInstOk[T any]() (T, bool) {
	var t T
	var instType = reflect.TypeOf(t)
	var intance, ok = FindBeanOk(instType.String()[1:])
	if !ok {
		intance, ok = FindBeanOk(instType.String())
	}
	return intance.(T), ok
}

func FindBeanProxy(name string) baseiface.IbaseProxy {
	//return  FindBean(name).(baseiface.IbaseProxy)
	proxy, ok := FindBean(name).(baseiface.IbaseProxy)
	if !ok {
		logrus.Error("Type assertion to ibaseProxy failed for ", name)
		return nil
	}
	return proxy
}

type beanContainer struct {
	container map[string]*BeanInfo

	lock sync.RWMutex
}

var container = newBeanContainer()

func Container() *beanContainer {
	return container
}
func newBeanContainer() *beanContainer {
	return &beanContainer{
		container: make(map[string]*BeanInfo),
		lock:      sync.RWMutex{},
	}
}

func (self *beanContainer) FindBean(name string) (*BeanInfo, bool) {
	self.lock.RLock()
	defer self.lock.RUnlock()

	var beanInfo, ok = self.container[name]
	if !ok {
		//logrus.Error(name, " bean found?", ok)
		return nil, ok
	}
	if beanInfo.bean == nil {
		beanInfo.SetBean(beanInfo.CreateBean())
		self.autoInjectConfigItem(beanInfo)
	}

	return beanInfo, ok
}

func (self *beanContainer) autoInjectConfigItem(beanInfo *BeanInfo) {
	if ibcfg, ok := beanInfo.bean.(baseiface.IbaseConfig); ok {
		ibcfg.InjectConfigItem(ibcfg)
	}
}
func (self *beanContainer) RegisterLoadBean(name string, loadbean baseiface.LoadBean) error {
	if _, ok := self.FindBean(name); ok {
		//logrus.Error(errors.New(name + " bean already exists!"))
		return nil
	}
	self.lock.Lock()
	defer self.lock.Unlock()

	var beanInfo = NewBeanInfo()
	beanInfo.SetBeanName(name)
	beanInfo.SetLoadBean(loadbean)
	self.container[name] = beanInfo
	return nil
}
func (self *beanContainer) RegisterBean(name string, bean baseiface.ISingleton) {
	self.lock.Lock()
	defer self.lock.Unlock()

	var beanInfo = NewBeanInfo()
	beanInfo.SetBeanName(name)
	beanInfo.SetBean(bean)
	self.container[name] = beanInfo
}

func (self *beanContainer) CreateBean(beanInfo *BeanInfo) baseiface.ISingleton {

	return beanInfo.CreateBean()
}
