package basedi

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"github.com/morrisxyang/xreflect"
	"github.com/sirupsen/logrus"
	"reflect"
)

func FindBeanGeneral[T baseiface.IbaseProxy]() T {
	factory := NewBeanFactroy[T]()
	bean := factory.FindBeanGeneral()

	//factory.IfAutowire(bean)
	return bean
}

type BeanFactroy[T baseiface.IbaseProxy] struct {
}

func NewBeanFactroy[T baseiface.IbaseProxy]() *BeanFactroy[T] {
	return &BeanFactroy[T]{}
}
func (self *BeanFactroy[T]) FindBeanGeneral() T {
	var beanName, err = self.RegisterLoadBean()
	if err != nil {
		logrus.Error(err)
	}
	return FindBean(beanName).(T)
}

func (self *BeanFactroy[T]) RegisterLoadBean() (string, error) {
	var beanName = self.FindTypePkgName()
	var err = RegisterLoadBean(beanName, self.LoadBeanGeneral)
	if err != nil {
		logrus.Error(err)
	}
	return beanName, err
}
func (self *BeanFactroy[T]) LoadBeanGeneral() baseiface.ISingleton {
	return self.LoadBeanProxy()
}

func (self *BeanFactroy[T]) IfProxy(some any) bool {
	if some == nil {
		return false
	}
	if ibaseProxy, ok := some.(baseiface.IbaseProxy); ok {
		ibaseProxy.InitProxy(ibaseProxy)
		return ok
	}
	return false

}
func (self *BeanFactroy[T]) LoadBeanProxy() T {

	var baseproxy T
	var value = reflect.New(reflect.TypeOf(baseproxy).Elem())
	baseproxy = value.Interface().(T)
	baseproxy.InitProxy(baseproxy)
	baseproxy.Init()

	return baseproxy

}

func (self *BeanFactroy[T]) NewOfPtrTypeProxy() T {

	var proxy T
	var typeOf = reflect.TypeOf(proxy)
	if typeOf.Kind() != reflect.Ptr {
		proxy = self.NewOfType()
		baseutils.IfProxy(proxy)
		proxy.Init()
		return proxy
	}
	var value = reflect.New(typeOf.Elem())
	proxy = value.Interface().(T)
	baseutils.IfProxy(proxy)
	proxy.Init()

	return proxy

}

// no used
func (self *BeanFactroy[T]) NewOfType() T {
	var proxy T
	baseutils.IfProxy(&proxy)
	proxy.Init()
	return proxy
}
func (self *BeanFactroy[T]) FindTypePkgName() string {
	var value T
	// 使用reflect.TypeOf获取接口的动态类型
	itype := self.FindTypeStru(value)
	return itype.Name() + "::" + itype.PkgPath() + "." + itype.Name()

}
func (self *BeanFactroy[T]) FindTypeName() string {

	var value T
	return self.FindTypeStru(value).Name()

}
func (self *BeanFactroy[T]) FindTypeStru(value any) reflect.Type {

	// 使用reflect.TypeOf获取接口的动态类型
	itype := reflect.TypeOf(value)
	// 检查是否是指针类型
	if itype.Kind() == reflect.Ptr {
		// 取指针指向的类型
		itype = itype.Elem()
	}
	// 获取类型名称
	return itype
}

func (self *BeanFactroy[T]) SetFieldValue(some any, key string, value any) {

	xreflect.SetField(some, key, value)

}
func (self *BeanFactroy[T]) Autowired(some any) error {

	var fields = self.FindStructFields(some)
	for _, field := range fields {
		var t = field.Type
		if t.Kind() == reflect.Ptr {
			var baseproxy, ok = self.NewProxyTypeOf(t)
			if ok && baseutils.IfNilPtrChild(some, field.Name) {
				var err = xreflect.SetField(some, field.Name, baseproxy)
				if err != nil {
					logrus.Error(err)
				}
			}

		}
	}
	return nil
}
func (self *BeanFactroy[T]) IfTypeStru(v reflect.Type) bool {

	if v.Kind() == reflect.Struct {
		return true
	}
	if v.Kind() == reflect.Ptr && v.Elem().Kind() == reflect.Struct {
		return true
	}
	return false

}
func (self *BeanFactroy[T]) FindStructFields(v any) []reflect.StructField {
	var fields = make([]reflect.StructField, 0)
	if !self.IfTypeStru(reflect.TypeOf(v)) {
		logrus.Info("v is not stru")
		return fields
	}

	var t = reflect.TypeOf(v)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	for i := 0; i < t.NumField(); i++ {
		if t.Field(i).Type.Kind() == reflect.Ptr &&
			t.Field(i).Type.Elem().Kind() == reflect.Struct {

			fields = append(fields, t.Field(i))
			continue
		}
		if t.Field(i).Type.Kind() == reflect.Struct {

			fields = append(fields, t.Field(i))
			continue
		}
	}

	return fields

}
func (self *BeanFactroy[T]) NewProxyTypeOf(t reflect.Type) (baseiface.IbaseProxy, bool) {

	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	var proxy = reflect.New(t)
	if iproxy, ok := proxy.Interface().(baseiface.IbaseProxy); ok {
		self.IfProxy(iproxy)
		iproxy.Init()
		return iproxy, true
	}

	return nil, false
}
