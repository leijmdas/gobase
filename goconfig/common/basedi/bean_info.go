package basedi

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
)

type BeanInfo struct {
	beanName string
	loadBean baseiface.LoadBean

	bean baseiface.ISingleton
}

func NewBeanInfo() *BeanInfo {
	return &BeanInfo{}
}

func (this *BeanInfo) CreateBean() baseiface.ISingleton {

	var single = this.loadBean()
	single.InitProxy(single)
	if single.IfAutowire() {
		single.Autowire()
	}
	return single
}

func (b *BeanInfo) BeanName() string {
	return b.beanName
}

func (b *BeanInfo) SetBeanName(name string) {
	b.beanName = name
}

func (b *BeanInfo) LoadBean() baseiface.LoadBean {
	return b.loadBean
}

func (b *BeanInfo) SetLoadBean(load baseiface.LoadBean) {
	b.loadBean = load
}

func (b *BeanInfo) Bean() baseiface.ISingleton {
	return b.bean
}

func (b *BeanInfo) SetBean(bean baseiface.ISingleton) {
	b.bean = bean
}
