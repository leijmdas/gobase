package database

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameIchubDbClient = "IchubDbClient"

// init register load
func init() {
	registerBeanIchubDbClient()
}

// register IchubDbClient
func registerBeanIchubDbClient() {
	basedi.RegisterLoadBean(singleNameIchubDbClient, LoadIchubDbClient)
}

func FindBeanIchubDbClient() *IchubDbClient {
	return basedi.FindBean(singleNameIchubDbClient).(*IchubDbClient)
}

func LoadIchubDbClient() baseiface.ISingleton {
	return NewIchubDbClient()
}
