package database

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestIchubDbClientSuite struct {
	suite.Suite
	inst *IchubDbClient

	rootdir string
}

func (this *TestIchubDbClientSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanIchubDbClient()

	this.rootdir = fileutils.FindRootDir()

}

func TestIchubDbClientSuites(t *testing.T) {
	suite.Run(t, new(TestIchubDbClientSuite))
}

func (this *TestIchubDbClientSuite) Test001_DbDebug() {
	logrus.Info(1)
	var db, _ = GetDB()
	logrus.Info(db.DB().Ping())
}

func (this *TestIchubDbClientSuite) Test002_ValueOf() {
	var db, _ = GetDB()
	logrus.Info(db.DB().Ping())
	var r, e = db.DB().Exec("select * from sys_dept")
	logrus.Info(r, e)
}

func (this *TestIchubDbClientSuite) Test003_MakeDbUrl() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test004_MakeDbUrlSsl() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test005_MakeDbUrlMysql() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test006_MakeDbUrlPostgres() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test007_MakeDbUrlCockRoach() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test008_Log() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test009_InitDbMysql() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test010_InitDbPostgres() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test011_GetDb() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test012_Valueof() {
	logrus.Info(1)

}

func (this *TestIchubDbClientSuite) Test013_IniDb() {
	logrus.Info(1)

}
