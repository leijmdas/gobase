package dbcontent

import (
	"gitee.com/leijmdas/gobase/goconfig/common/dbcontent/database"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

/*
	@Title    文件名称: dbcontent.go
	@Description  描述: dbclient客户端

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

func GetDB() (*gorm.DB, error) {

	return database.GetDB()

}
