package gocache

import "time"
import "github.com/patrickmn/go-cache"

var FileCache = NewFileCache()

type FuncFileCache struct {
	gocache    *cache.Cache `json:"-"`
	Pkey       string
	ExpireTime time.Duration
}

func NewFileCache() *FuncFileCache {

	return NewFunFileCache("FileCache:")
}
func NewFunFileCache(pkey string) *FuncFileCache {
	var filecache = &FuncFileCache{
		Pkey:       pkey,
		ExpireTime: Cache_expire_time,
		// 默认过期时间10s；清理间隔30s，即每30s钟会自动清理过期的键值对
		gocache: cache.New(Cache_expire_time, cache_clean_time),
	}

	return filecache
}

func (this *FuncFileCache) Set(k string, x interface{}) {
	this.gocache.Set(this.Pkey+k, x, cache.DefaultExpiration)

}

func (this *FuncFileCache) Get(k string) (interface{}, bool) {
	return this.gocache.Get(this.Pkey + k)
}
