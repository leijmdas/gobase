package gobigcache

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/sirupsen/logrus"
	"time"

	bigcache "github.com/allegro/bigcache/v3"
)

func Get(key string) ([]byte, error) {
	return FindBeanBigcacheConfig().BigCache.Get(key)
}
func Set(key string, entry []byte) error {
	return FindBeanBigcacheConfig().BigCache.Set(key, entry)
}

type BigcacheConfig struct {
	basedto.BaseEntitySingle

	BigCache *bigcache.BigCache `json:"-"`
}

func NewBigcacheConfig() *BigcacheConfig {
	var c = &BigcacheConfig{}
	var cache, err = bigcache.New(context.Background(), bigcache.DefaultConfig(10*time.Minute))
	c.BigCache = cache
	if err != nil {
		logrus.Error(err)
	}
	return c
}
