package gobigcache

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: bigcache_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-29 13:51:24)
	@Update 作者: leijmdas@163.com 时间(2025-01-29 13:51:24)

* *********************************************************/

const singleNameBigcacheConfig = "*gobigcache.BigcacheConfig-5cba5e5e8e6f4537b146f8f8b761282d"

// init register load
func init() {
	registerBeanBigcacheConfig()
}

// register BigcacheConfig
func registerBeanBigcacheConfig() {
	basedi.RegisterLoadBean(singleNameBigcacheConfig, LoadBigcacheConfig)
}

func FindBeanBigcacheConfig() *BigcacheConfig {
	bean, ok := basedi.FindBean(singleNameBigcacheConfig).(*BigcacheConfig)
	if !ok {
		logrus.Errorf("FindBeanBigcacheConfig: failed to cast bean to *BigcacheConfig")
		return nil
	}
	return bean

}

func LoadBigcacheConfig() baseiface.ISingleton {
	var s = NewBigcacheConfig()
	InjectBigcacheConfig(s)
	return s

}

func InjectBigcacheConfig(s *BigcacheConfig) {

	//golog.Debug("inject")
}
