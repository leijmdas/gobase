package gobigcache

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestBigcacheConfigSuite struct {
	suite.Suite
	inst *BigcacheConfig

	rootdir string
}

func (this *TestBigcacheConfigSuite) SetupTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanBigcacheConfig()

	this.rootdir = fileutils.FindRootDir()

}

func TestBigcacheConfigSuites(t *testing.T) {
	suite.Run(t, new(TestBigcacheConfigSuite))
}
func (this *TestBigcacheConfigSuite) Test001_bc() {

	// 设置一个唯一的键和对应的值
	const myUniqueKey = "my-unique-key"
	var cache = this.inst.BigCache
	cache.Set(myUniqueKey, []byte("some value"))

	// 从缓存中获取值
	entry, err := cache.Get(myUniqueKey)
	if err != nil {
		fmt.Println("Error while fetching from cache:", err)
		return
	}
	fmt.Println("Value from cache:", string(entry))

}
func (this *TestBigcacheConfigSuite) Test002_bc() {

	// 设置一个唯一的键和对应的值
	const myUniqueKey = "my-unique-key"

	Set(myUniqueKey, []byte("some value"))

	// 从缓存中获取值
	entry, err := Get(myUniqueKey)
	if err != nil {
		fmt.Println("Error while fetching from cache:", err)
		return
	}
	fmt.Println("Value from cache:", string(entry))

}
func (this *TestBigcacheConfigSuite) Test003_bc() {
	var b baseconfig.GatewayDto
	var bb = baseutils.ContainsTypeOfSub(b, "BaseEntity")
	logrus.Info(bb)

}
func (this *TestBigcacheConfigSuite) Test004_baseentity() {

	logrus.Info("\r\nbaseeintity is ", baseutils.IfBaseEntity(baseconfig.GatewayDto{}))
	var s = gstr.Trim("dd ddd ", " ")
	logrus.Info(len(s))

}
