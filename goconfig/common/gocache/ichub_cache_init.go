package gocache

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: ichub_cache_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-29 13:51:24)
	@Update 作者: leijmdas@163.com 时间(2025-01-29 13:51:24)

* *********************************************************/

const singleNameIchubCache = "*gocache.IchubCache-4bf0e027f3b54f8aa4fa59f50067bbc5"

// init register load
func init() {
	registerBeanIchubCache()
}

// register IchubCache
func registerBeanIchubCache() {
	basedi.RegisterLoadBean(singleNameIchubCache, LoadIchubCache)
}

func FindBeanIchubCache() *IchubCache {
	bean, ok := basedi.FindBean(singleNameIchubCache).(*IchubCache)
	if !ok {
		logrus.Errorf("FindBeanIchubCache: failed to cast bean to *IchubCache")
		return nil
	}
	return bean

}

func LoadIchubCache() baseiface.ISingleton {
	var s = NewCache()
	InjectIchubCache(s)
	return s

}

func InjectIchubCache(s *IchubCache) {

	//golog.Debug("inject")
}
