package gocache

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"github.com/patrickmn/go-cache"
	"testing"
	"time"
)

var gocache = NewCache()

func Test0001_gocacheString(t *testing.T) {

	// 设置一个键值对，过期时间是 3s
	gocache.Set("a", "testa", 3*time.Second)

	// 设置一个键值对，采用 New() 时的默认过期时间，即 10s
	gocache.Set("foo", "bar", cache.DefaultExpiration)

	// 设置一个键值对，没有过期时间，不会自动过期，需要手动调用 Delete() 才能删除
	gocache.Set("baz", 42, cache.NoExpiration)

	v, found := gocache.Get("a")
	fmt.Println(v, found) // testa,true

	//<-time.After(5 * time.Second) // 延时5s

	v, found = gocache.Get("a") // nil,false
	fmt.Println(v, found)

	//<-time.After(6 * time.Second)
	v, found = gocache.Get("foo") // nil,false
	fmt.Println(v, found)

	v, found = gocache.Get("baz") // 42,true
	fmt.Println(v, found)
}

type User struct {
	Id   int64
	Name string
}

func Test0002_gocacheStruct(t *testing.T) {
	user := &User{
		Id:   1,
		Name: "leijmdas",
	}
	gocache.Set("user", *user, 3*time.Second)
	u, f := gocache.Get("user")
	if f {
		fmt.Println(jsonutils.ToJsonStr(u.(User)))
	}
	//gocache := cache.New(10*time.Second, 30*time.Second) // 默认过期时间10s；清理间隔30s，即每30s钟会自动清理过期的键值对
}

func Test0003_gp(t *testing.T) {
	var a interface{}
	a = 1
	fmt.Println(a)
}
func Test00042_gocacheStruct(t *testing.T) {

}
