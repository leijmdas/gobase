package gocache

import "time"

const Cache_expire_time = 30 * time.Minute
const cache_clean_time = 5 * time.Minute
