package gocache

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: meta_cache_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-29 13:51:24)
	@Update 作者: leijmdas@163.com 时间(2025-01-29 13:51:24)

* *********************************************************/

const singleNameMetaCache = "*gocache.MetaCache-c483d713985142d88cdab206e9d2faa8"

// init register load
func init() {
	registerBeanMetaCache()
}

// register MetaCache
func registerBeanMetaCache() {
	basedi.RegisterLoadBean(singleNameMetaCache, LoadMetaCache)
}

func FindBeanMetaCache() *MetaCache {
	bean, ok := basedi.FindBean(singleNameMetaCache).(*MetaCache)
	if !ok {
		logrus.Errorf("FindBeanMetaCache: failed to cast bean to *MetaCache")
		return nil
	}
	return bean

}

func LoadMetaCache() baseiface.ISingleton {
	var s = NewMetaCache()
	InjectMetaCache(s)
	return s

}

func InjectMetaCache(s *MetaCache) {

	//golog.Debug("inject")
}
