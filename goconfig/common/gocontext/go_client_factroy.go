package gocontext

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig/configdomain"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/dbcontent/database"
	"gitee.com/leijmdas/gobase/goconfig/common/gocache"
	ichubelastic "gitee.com/leijmdas/gobase/goconfig/common/goelastic"
	"gitee.com/leijmdas/gobase/goconfig/common/goredis/ichubredis"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/os/gsession"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

//var IchubClient = FindBeanGoClientFactroy()

type GoClientFactroy struct {
	basedto.BaseEntitySingle

	ichubConfig *ichubconfig.IchubConfig
	dbDto       *baseconfig.DbClientDto
	dbClient    *database.IchubDbClient

	esDto       *baseconfig.ElasticClientDto
	esclient    *ichubelastic.ElasticClient
	redisDto    *baseconfig.RedisClientDto
	redisClient *ichubredis.IchubRedisClient
	gredis      *gredis.Redis

	mysqlDto    *baseconfig.DbClientDto
	mysqlClient *database.IchubDbClient
	dsDto       *baseconfig.DbClientDto
	dssqlClient *database.IchubDbClient
	natsDto     *baseconfig.NatsClientDto

	SessionManager *gsession.Manager
}

func (this *GoClientFactroy) Gredis() *gredis.Redis {
	return this.gredis
}

func (this *GoClientFactroy) SetGredis(gredis *gredis.Redis) {
	this.gredis = gredis
}

func NewIchubClientFactroy() *GoClientFactroy {
	var gocliFactroy = &GoClientFactroy{
		ichubConfig: ichubconfig.FindBeanIchubConfig(),
	}
	gocliFactroy.InitProxy(gocliFactroy)
	return gocliFactroy
}
func (this *GoClientFactroy) IchubConfig() *ichubconfig.IchubConfig {
	return this.ichubConfig
}

func (this *GoClientFactroy) SetIchubConfig(ichubConfig *ichubconfig.IchubConfig) {
	this.ichubConfig = ichubConfig
}

func (this *GoClientFactroy) DbClient() *database.IchubDbClient {
	return this.dbClient
}

// v	suite.dbinst = gocontext.IchubClient.IniDbClient().DbClient().Db
func (this *GoClientFactroy) GetDB() *gorm.DB {
	return this.IniDbClient().dbClient.DbDebug()
}

func (this *GoClientFactroy) SetDbClient(dbClient *database.IchubDbClient) {
	this.dbClient = dbClient
}

func (this *GoClientFactroy) RedisClient() *ichubredis.IchubRedisClient {
	return this.redisClient
}

func (this *GoClientFactroy) SetRedisClient(redisClient *ichubredis.IchubRedisClient) {
	this.redisClient = redisClient
}

func (this *GoClientFactroy) Esclient() *ichubelastic.ElasticClient {
	return this.esclient
}

func (this *GoClientFactroy) SetEsclient(esclient *ichubelastic.ElasticClient) {
	this.esclient = esclient
}

func (this *GoClientFactroy) IniGredisClient() *GoClientFactroy {
	if this.gredis == nil {
		this.redisDto = this.ichubConfig.ReadIchubRedis()
		var redisConfig = &gredis.Config{
			Address:     this.redisDto.Addr,
			Db:          this.redisDto.DB,
			Pass:        this.redisDto.Password,
			MinIdle:     10,  //			否	0	允许闲置的最小连接数
			MaxIdle:     100, //	否	10	允许闲置的最大连接数(0表示不限制)
			IdleTimeout: 90,  //
			MaxActive:   200, //	否	100
		}
		gredis.SetConfig(redisConfig, "cache")
		var err error
		this.gredis, err = gredis.New(redisConfig)
		if err != nil {
			logrus.Error(err)
			panic(err)
		}

	}
	return this
}
func (this *GoClientFactroy) IniRedisClient() *GoClientFactroy {
	if this.redisClient == nil {

		this.redisDto = this.ichubConfig.ReadIchubRedis()
		this.redisClient = ichubredis.NewIchubRedisClient()
		this.redisClient.SetRedisClientDto(this.redisDto)
		this.redisClient.Open()
	}
	return this
}
func (this *GoClientFactroy) IniEsClient() *GoClientFactroy {
	if this.esDto == nil {

		this.esDto = this.ichubConfig.ReadIchubEs()
		this.esclient = ichubelastic.New(this.esDto)
		this.esclient.Open()
	}
	return this
}

func (this *GoClientFactroy) Ini() *GoClientFactroy {
	this.ichubConfig.Read()
	this.IniRedisClient()
	this.IniDbClient()
	return this.IniEsClient()
}
func (this *GoClientFactroy) IniMysqlClient() *GoClientFactroy {
	if this.dbDto == nil {
		this.mysqlDto = this.ichubConfig.ReadIchubMysql()
		this.mysqlClient = database.FindDbClient(this.mysqlDto)

	}
	return this
}
func (this *GoClientFactroy) IniDbClient() *GoClientFactroy {
	if this.dbDto == nil {

		this.dbDto = this.ichubConfig.ReadIchubDb()
		this.dbClient = database.FindDbClient(this.dbDto)

	}
	return this
}
func (this *GoClientFactroy) IniDsClient() *GoClientFactroy {
	if this.dsDto == nil {
		this.dsDto = this.ichubConfig.ReadIchubDatasource()
		this.dbClient = database.FindDbClient(this.dsDto)
	}
	return this
}
func (this *GoClientFactroy) GetIchubDbClient(dbtype string) *database.IchubDbClient {
	if dbtype == baseconsts.DB_TYPE_MYSQL {
		this.IniMysqlClient()
		return this.mysqlClient
	}
	if dbtype == "cockroach" {
		this.IniDsClient()
		return this.dssqlClient
	}

	this.IniDbClient()
	return this.dbClient
}

func (this *GoClientFactroy) GetDbClientDto(dbtype string) *baseconfig.DbClientDto {
	if dbtype == baseconsts.DB_TYPE_MYSQL {
		return this.mysqlDto
	}
	if dbtype == "cockroach" {

		return this.dsDto
	}
	return this.dbDto
}
func (this *GoClientFactroy) RegisterEncDec(encdec baseiface.IEncDec) *GoClientFactroy {
	if this.ichubConfig != nil {
		this.ichubConfig.RegisterEncDec(encdec)
	}
	return this
}
func (this *GoClientFactroy) GetNatsDto() *baseconfig.NatsClientDto {

	return this.ichubConfig.ReadNats()
}
func (this *GoClientFactroy) IniSessionManager() *GoClientFactroy {
	if this.SessionManager == nil {
		this.IniGredisClient()
		storage := gsession.NewStorageRedis(this.gredis)
		this.SessionManager = gsession.New(24*time.Hour, storage)

	}
	return this
}
func (this *GoClientFactroy) MakeConfigAgg() *configdomain.ConfigAgg {
	var agg = configdomain.NewConfigAgg()
	//agg.Bypass=this.config.ReadGateway().ByPass
	agg.Datasource = this.ichubConfig.ReadIchubDatasource()
	agg.DbClient = this.ichubConfig.ReadIchubDb()
	agg.WebClient = this.ichubConfig.ReadIchubWebClient()
	agg.WebServer = this.ichubConfig.ReadIchubWebServer()
	agg.NatsClient = this.ichubConfig.ReadNats()
	agg.RedisClient = this.ichubConfig.ReadIchubRedis()
	agg.ElasticClient = this.ichubConfig.ReadIchubEs()

	return agg
}
func (this *GoClientFactroy) FindConfig(env string) *ichubconfig.IchubConfig {
	if c, ok := this.Gocache(env); ok {
		return c
	}
	var cfg *ichubconfig.IchubConfig
	switch env {
	case baseconsts.ENV_DEV:
		cfg = ichubconfig.NewIchubConfig().ReadDev()
	case baseconsts.ENV_TEST:
		cfg = ichubconfig.NewIchubConfig().ReadTest()
	case baseconsts.ENV_MASTER:
		cfg = ichubconfig.NewIchubConfig().ReadMaster()
	case baseconsts.ENV_RELEASE:
		cfg = ichubconfig.NewIchubConfig().ReadRelease()

	default:
		cfg = ichubconfig.NewIchubConfig().ReadDefault()
	}
	gocache.FindBeanIchubCache().Set(env, cfg, time.Minute*60)
	return cfg
}
func (this *GoClientFactroy) Gocache(env string) (*ichubconfig.IchubConfig, bool) {
	if c, ok := gocache.FindBeanIchubCache().Get(env); c != nil && ok {
		return c.(*ichubconfig.IchubConfig), ok
	}

	//var c, ok = gocache.Get(baseconsts.ENV_DEFAULT)
	return nil, false
}
