package gocontext

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_client_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-10-13 10:35:21)
	@Update 作者: leijmdas@163.com 时间(2024-10-13 10:35:21)

* *********************************************************/

const singleNameGoClientFactroy = "*gocontext.GoClientFactroy-2c42f9acb0184d0c9d3465c1ae6c68eb"

// init register load
func init() {
	registerBeanGoClientFactroy()
}

// register GoClientFactroy
func registerBeanGoClientFactroy() {
	basedi.RegisterLoadBean(singleNameGoClientFactroy, LoadGoClientFactroy)
}

func FindBeanGoClientFactroy() *GoClientFactroy {
	bean, ok := basedi.FindBean(singleNameGoClientFactroy).(*GoClientFactroy)
	if !ok {
		logrus.Errorf("FindBeanGoClientFactroy: failed to cast bean to *GoClientFactroy")
		return nil
	}
	return bean

}

func LoadGoClientFactroy() baseiface.ISingleton {
	var s = NewIchubClientFactroy()
	InjectGoClientFactroy(s)
	return s

}

func InjectGoClientFactroy(s *GoClientFactroy) {

	//// goutils.Debug("inject")
}
