package gocontext

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/encrypt"
	"github.com/sirupsen/logrus"
	"testing"
)

func Test001_cfg(t *testing.T) {

	FindBeanGoClientFactroy().RegisterEncDec(encrypt.FindBeanEncDec())

	var swag = FindBeanGoClientFactroy().IchubConfig().ReadWebSwagger()
	logrus.Info(swag)
}

func Test002_Gredis(t *testing.T) {
	var gredis = FindBeanGoClientFactroy().IniGredisClient().Gredis()
	gredis.Set(context.Background(), "key", "222")
	logrus.Info(gredis.Get(context.Background(), "key"))
}
func Test003_FindCfg(t *testing.T) {
	var cfg = FindBeanGoClientFactroy().FindConfig(baseconsts.ENV_DEV)
	var cfg1 = FindBeanGoClientFactroy().FindConfig(baseconsts.ENV_DEV)
	var cfg2 = FindBeanGoClientFactroy().FindConfig(baseconsts.ENV_TEST)
	logrus.Info(cfg2, cfg1 == cfg)
}
func Test0034FindCfg(t *testing.T) {
	var cfg = FindBeanGoClientFactroy().FindConfig(baseconsts.ENV_DEV)
	var cfg1 = FindBeanGoClientFactroy().FindConfig(baseconsts.ENV_DEV)
	var cfg2 = FindBeanGoClientFactroy().FindConfig(baseconsts.ENV_TEST)
	logrus.Info(cfg2, cfg1 == cfg)
}
