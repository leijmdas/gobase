package gocontext

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig/goinicfg"
	"github.com/duke-git/lancet/fileutil"
	"github.com/sirupsen/logrus"
)

/*
	@Title    文件名称: go_rulefile_ctx.go
	@Description  描述:  全局上下文

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

const (
	DEFINE_RULE_PATH = "/config/rule_define/"
	DEFILE_RULE_FILE = "rule_define.ini"
	DAO_RULE_PATH    = "/data/output/dao/"

	//used by data-file
	INPUT_RULE_PATH  = "/data/input/rule/"
	OUTPUT_RULE_PATH = "/data/output/rule/"
)

type GoRulefileCtx struct {
	basedto.BaseEntitySingle `json:"-"`
	//Container *dig.Container `json:"-"`

	BasePath       string `json:"base_path"`
	DefineRulePath string `json:"define_rule_path"`
	DefineRuleFile string `json:"define_rule_file"`

	outRulePath string `json:"out_rule_path"`
	inRulePath  string `json:"in_rule_path"`
	DaoFilePath string `json:"dao_file_path"`

	IchubConfigIni *goinicfg.GoiniConfig `json:"="`
	FuncDefineMap  map[string]string     `json:"func_define_map"`

	ApiAll map[string]any `json:"-"`
}

func newGoRulefileCtx() *GoRulefileCtx {
	var ctxt = &GoRulefileCtx{}
	ctxt.init()
	ctxt.InitProxy(ctxt)
	ichublog.InitLogrus()
	return ctxt
}

func (self *GoRulefileCtx) init() {

	self.BasePath = fileutils.FindRootDir()

	self.DefineRulePath = self.BasePath + DEFINE_RULE_PATH
	self.DefineRuleFile = self.DefineRulePath + DEFILE_RULE_FILE
	self.inRulePath = self.BasePath + INPUT_RULE_PATH
	self.outRulePath = self.BasePath + OUTPUT_RULE_PATH
	self.DaoFilePath = self.BasePath + DAO_RULE_PATH

	self.LoadRuleDefine()
}

func (self *GoRulefileCtx) OutRulePath() string {
	return self.outRulePath
}

func (self *GoRulefileCtx) SetOutRulePath(outRulePath string) {
	self.outRulePath = outRulePath
}

func (self *GoRulefileCtx) InRulePath() string {
	return self.inRulePath
}

func (self *GoRulefileCtx) SetInRulePath(inRulePath string) {
	self.inRulePath = inRulePath
}

func (self *GoRulefileCtx) CheckConfigFileExist() bool {
	return fileutils.CheckFileExist(self.BasePath + baseconsts.DEFINE_ENV_PATHFILE)
}

func (self *GoRulefileCtx) LoadRuleDefine() error {
	self.IchubConfigIni = goinicfg.FindBeanGoiniConfig()
	err := self.IchubConfigIni.Load(self.DefineRuleFile)
	if err != nil {
		logrus.Error(err.Error())
		return err
	}

	self.FuncDefineMap = self.IchubConfigIni.Find2Map()
	return err
}
func (self *GoRulefileCtx) LoadRule() (map[string]string, error) {

	err := self.LoadRuleDefine()

	return self.FuncDefineMap, err
}

func (self *GoRulefileCtx) WriteDaoFile(daofile string, content string) error {
	fileutil.RemoveFile(self.DaoFilePath + daofile)
	err := fileutil.WriteStringToFile(self.DaoFilePath+daofile, content, false)
	if err != nil {
		logrus.Error(err)
	}
	return err
}
