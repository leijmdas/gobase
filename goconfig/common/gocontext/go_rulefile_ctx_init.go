package gocontext

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_rulefile_ctx_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-31 17:13:30)
	@Update 作者: leijmdas@163.com 时间(2025-01-31 17:13:30)

* *********************************************************/

const singleNameGoRulefileCtx = "*gocontext.GoRulefileCtx-b6485ca81df1407d9cda8a6c338b05e1"

// init register load
func init() {
	registerBeanGoRulefileCtx()
}

// register GoRulefileCtx
func registerBeanGoRulefileCtx() {
	basedi.RegisterLoadBean(singleNameGoRulefileCtx, LoadGoRulefileCtx)
}

func FindBeanGoRulefileCtx() *GoRulefileCtx {
	bean, ok := basedi.FindBean(singleNameGoRulefileCtx).(*GoRulefileCtx)
	if !ok {
		logrus.Errorf("FindBeanGoRulefileCtx: failed to cast bean to *GoRulefileCtx")
		return nil
	}
	return bean

}

func LoadGoRulefileCtx() baseiface.ISingleton {
	var s = newGoRulefileCtx()
	InjectGoRulefileCtx(s)
	return s

}

func InjectGoRulefileCtx(s *GoRulefileCtx) {

	//golog.Debug("inject")
}
