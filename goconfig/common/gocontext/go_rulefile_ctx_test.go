package gocontext

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig/goinicfg"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func Test0001_cfgIni(t *testing.T) {

	err := FindBeanGoRulefileCtx().LoadRuleDefine()
	if err != nil {
		panic(err.Error())
	}
	var cfgIni = FindBeanGoRulefileCtx().IchubConfigIni
	var keys = cfgIni.FindGeneral().Keys()
	logrus.Debug(jsonutils.ToJsonPretty(keys))
	cfgIni.Log()
	for _, v := range keys {
		logrus.Println(v)
	}
	logrus.Debug(cfgIni.Find2Map())

}

var config = ichubconfig.NewIchubConfig()

func Test0002_readSwaagerVarNo(t *testing.T) {

	var client = config.ReadWebSwagger()
	fmt.Println(client.ToPrettyString())

}
func Test0003_readSwaagerVarNo(t *testing.T) {
	os.Setenv("SWAGGER_HOST", "192.168.14.58:88")

	var client = config.ReadWebSwagger()
	fmt.Println(client.ToPrettyString())
	assert.Equal(t, "192.168.14.58:88", client.Host)

}
func Test0004_cfgIni(t *testing.T) {

	err := goinicfg.FindBeanGoiniConfig().Load(FindBeanGoRulefileCtx().DefineRuleFile)
	if err != nil {
		panic(err.Error())
	}
	var cfgIni = goinicfg.FindBeanGoiniConfig()
	var keys = cfgIni.FindGeneral().Keys()
	fmt.Println(jsonutils.ToJsonPretty(keys))
	cfgIni.Log()
	for _, v := range keys {
		logrus.Println(v)
	}
	cfgIni.Find2Map()
}
func Test0005_cfgIni(t *testing.T) {

	var config = ichubconfig.Default()
	config.Read()
	logrus.Info(config)

}
