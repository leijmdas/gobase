package ichubelastic

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

/*
	@Title    文件名称: elastic_search.go
	@Description  描述:  配置文件

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

type ElasticClient struct {
	basedto.BaseEntitySingle

	ClientDto *baseconfig.ElasticClientDto
	client    *elastic.Client `json:"-"`
}

func (this *ElasticClient) Client() *elastic.Client {
	return this.client
}

func (this *ElasticClient) SetClient(client *elastic.Client) {
	this.client = client
}

func NewElasticClient() *ElasticClient {
	return &ElasticClient{}
}
func New(dto *baseconfig.ElasticClientDto) *ElasticClient {
	return &ElasticClient{
		ClientDto: dto,
	}
}

func (this *ElasticClient) Open() (*elastic.Client, error) {
	var urls = strings.Split(this.ClientDto.URL, ",")

	// 创建ES client用于后续操作ES

	client, err := elastic.NewClient( // 设置ES服务地址，支持多个地址
		elastic.SetURL(urls...),
		// 设置基于http configdto auth验证的账号和密码
		elastic.SetBasicAuth(this.ClientDto.Username, this.ClientDto.Password),
		// 启用gzip压缩
		//elastic.SetGzip(this.ClientDto.Gzip),
		elastic.SetHttpClient(&http.Client{Timeout: 60 * time.Second}),
		// 设置监控检查时间间隔
		elastic.SetHealthcheckInterval(10*time.Second),

		elastic.SetSniff(true),
		// 设置请求失败最大重试次数
		elastic.SetMaxRetries(5),
		elastic.SetSniff(true),
		// 设置错误日志输出
		elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)),
		// 设置info日志输出
		elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)))
	if err != nil {
		// Handle error
		logrus.Errorf("连接失败: %v\n", err)
	} else {
		logrus.Info("连接成功")
	}
	logrus.Info(client)
	this.client = client
	//client.Ping()
	return client, err
}
func (this *ElasticClient) Close() {
	this.client.Stop()
}
