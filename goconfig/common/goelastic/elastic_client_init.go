package ichubelastic

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameElasticClient = "ElasticClient"

// init
// register
// load
func init() {
	registerBeanElasticClient()
}

// register ElasticClient
func registerBeanElasticClient() {
	basedi.RegisterLoadBean(singleNameElasticClient, LoadElasticClient)
}

func FindBeanElasticClient() *ElasticClient {
	return basedi.FindBean(singleNameElasticClient).(*ElasticClient)
}

func LoadElasticClient() baseiface.ISingleton {
	return NewElasticClient()
}
