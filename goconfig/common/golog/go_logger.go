package golog

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"github.com/sirupsen/logrus"
)

func Info(v ...interface{}) {

	goutils.FindBeanGoLog().Info(context.Background(), v...)
	logrus.Info(v...)
}

func Println(v ...interface{}) {

	goutils.FindBeanGoLog().Info(context.Background(), v...)
	logrus.Info(v...)
}
func Debug(v ...interface{}) {

	goutils.Debug(v...)
}

func Warn(v ...interface{}) {

	goutils.FindBeanGoLog().Warning(context.Background(), v...)
	logrus.Info(v...)
}

func Error(v ...interface{}) {

	goutils.Error(v...)
}
func Stat(v ...interface{}) {

	goutils.Stat(v...)

}
func StatInfo(v ...interface{}) {

	goutils.Stat(v...)
	logrus.Info(v)
}
func InitLogrus() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetReportCaller(true)

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:               true,
		EnvironmentOverrideColors: true,
		TimestampFormat:           "2006-01-12 15:04:05", //时间格式
		FullTimestamp:             true,
		DisableLevelTruncation:    true,
	})

}
