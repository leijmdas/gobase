package ichublog

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
)

func init() {
	InitLogrus()
}
func Log(ps ...interface{}) {
	goutils.FindBeanGoLogrule().Debug(context.Background(), ps...)
}
func Debug(ps ...interface{}) {
	goutils.FindBeanGoLogrule().Debug(context.Background(), ps...)
}

func Info(ps ...interface{}) {
	goutils.FindBeanGoLogrule().Info(context.Background(), ps...)

}

func Error(ps ...interface{}) {
	goutils.FindBeanGoLogrule().Error(context.Background(), ps...)

}

func InitLogrus() {
	golog.InitLogrus()

}
