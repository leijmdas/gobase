package gopool

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type IchubResultPool struct {
	basedto.BaseEntitySingle
	*GeneralObjectPool[*basedto.IchubResult]
}

func NewIchubResultPool() *IchubResultPool {
	return &IchubResultPool{
		GeneralObjectPool: NewGeneralObjectPool[*basedto.IchubResult](),
	}
}
