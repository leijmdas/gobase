package gopool

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: ichub_result_pool_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-08-26 14:39:35
	@Update  作者:    leijmdas@163.com  时间: 2024-08-26 14:39:35
*/

var singleNameIchubResultPool = "*gopool.IchubResultPool-5b62f84f-9430-4ad5-9762-6681fc43c704"

// init register load
func init() {
	registerBeanIchubResultPool()
}

// register IchubResultPool
func registerBeanIchubResultPool() {
	basedi.RegisterLoadBean(singleNameIchubResultPool, LoadIchubResultPool)
}

// FindBeanIchubResultPool
func FindBeanIchubResultPool() *IchubResultPool {

	if bean, ok := basedi.FindBeanOk(singleNameIchubResultPool); ok {
		return bean.(*IchubResultPool)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadIchubResultPool() baseiface.ISingleton {
	var inst = NewIchubResultPool()
	InjectIchubResultPool(inst)
	return inst

}

func InjectIchubResultPool(s *IchubResultPool) {

	//logrus.Info("inject")
}
