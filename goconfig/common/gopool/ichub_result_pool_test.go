package gopool

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"

	// "git.ichub.com/general/webcli120/goconfig/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestIchubResultPoolSuite struct {
	suite.Suite

	inst    *IchubResultPool
	rootdir string

	dbtype string
	cfg    *ichubconfig.IchubConfig
}

func TestIchubResultPoolSuites(t *testing.T) {
	suite.Run(t, new(TestIchubResultPoolSuite))
}

func (suite *TestIchubResultPoolSuite) SetupSuite() {
	logrus.Info(" setup suite")
	//ichublog.InitLogrus()
	suite.inst = FindBeanIchubResultPool()
	suite.rootdir = fileutils.FindRootDir()
	suite.cfg = ichubconfig.FindBeanIchubConfig()
	suite.dbtype = suite.cfg.ReadIchubDb().Dbtype
	suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_COCKROACH

}

func (suite *TestIchubResultPoolSuite) TearDownSuite() {
	logrus.Info(" teardown suite")
	suite.cfg.Gorm.DbType = suite.dbtype
	//	var dto = suite.cfg.ReadIchubDb()

}
func (suite *TestIchubResultPoolSuite) SetupTest() {
	logrus.Info(" setup test")
}

func (suite *TestIchubResultPoolSuite) TearDownTest() {
	logrus.Info(" teardown test")
}

func (suite *TestIchubResultPoolSuite) Test001_borrow() {
	var pool = FindBeanIchubResultPool()
	var r, err = pool.BorrowObject(context.Background())
	logrus.Info("\r\n", r, err)
	err = pool.ReturnObject(context.Background(), r)
	logrus.Error(err)
}

func (suite *TestIchubResultPoolSuite) Test002_return() {
	var pool = FindBeanIchubResultPool()
	var r, err = pool.BorrowObject(context.Background())
	logrus.Info("\r\n", r, err)

	err = pool.ReturnObject(context.Background(), r)
	if err != nil {
		goutils.Error(err)

	}
	if rr, err := pool.BorrowObject(context.Background()); err != nil {
		logrus.Error(err)
	} else {
		logrus.Info("\r\n", rr, err)

	}
}
