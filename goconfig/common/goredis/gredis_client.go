package goredis

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/sirupsen/logrus"
)

type GredisClient struct {
	basedto.BaseEntitySingle

	config gredis.Config
	Group  string
	ctx    gctx.Ctx
	Cfg    *ichubconfig.IchubConfig
	*gredis.Redis
}

func NewGredisClient() *GredisClient {
	var cfg = ichubconfig.FindBeanIchubConfig()
	var dto = cfg.ReadIchubRedis()
	var config = gredis.Config{

		Address:     dto.Addr,     // "huawei.akunlong.top:6379",
		Db:          dto.DB,       //1,
		Pass:        dto.Password, //"123456",
		MaxIdle:     10,
		MaxActive:   100,
		IdleTimeout: 60,
		//	DialTimeout: 30,
		//	ReadTimeout: 30,
	}
	var group = "default"
	gredis.SetConfig(&config, group)

	return &GredisClient{
		config: config,
		Group:  group,
		Cfg:    cfg,
		ctx:    gctx.New(),
		Redis:  g.Redis(group),
	}
}

func (gredis *GredisClient) Get(key string) (*gvar.Var, error) {
	value, err := gredis.Redis.Get(gredis.ctx, key)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	return value, err
}
func (gredis *GredisClient) Set(key string, value any) error {
	value, err := gredis.Redis.Set(gredis.ctx, key, value)

	if err != nil {
		logrus.Error(err)
		return err
	}

	return err
}
