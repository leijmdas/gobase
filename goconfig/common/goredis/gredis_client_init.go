package goredis

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: gredis_client_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-08-02 23:20:11
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-08-02 23:20:11
*/

const singleNameGredisClient = "goredis.GredisClient"

// init register load
func init() {
	registerBeanGredisClient()
}

// register GredisClient
func registerBeanGredisClient() {
	basedi.RegisterLoadBean(singleNameGredisClient, LoadGredisClient)
}

// FindBeanGredisClient
func FindBeanGredisClient() *GredisClient {

	if bean, ok := basedi.FindBeanOk(singleNameGredisClient); ok {
		return bean.(*GredisClient)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadGredisClient() baseiface.ISingleton {
	var inst = NewGredisClient()
	InjectGredisClient(inst)
	return inst

}

func InjectGredisClient(s *GredisClient) {

	//logrus.Info("inject")
}
