package goredis

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestGredisClientSuite struct {
	suite.Suite
	inst    *GredisClient
	rootdir string
}

func (this *TestGredisClientSuite) SetupTest() {
	this.rootdir = fileutils.FindRootDir()
	ichublog.InitLogrus()
	this.inst = FindBeanGredisClient()

}
func (suite *TestGredisClientSuite) TearDownTest() {
	logrus.Println(" teardown")
}
func (suite *TestGredisClientSuite) TearDownSuite() {
	logrus.Println(" teardown")
}

func TestGredisClientSuites(t *testing.T) {
	suite.Run(t, new(TestGredisClientSuite))
}

func (suite *TestGredisClientSuite) Test001_gredis() {

	//var gredis =  FindBeanGredisClient()
	suite.inst.Set("key", suite.inst.Cfg.ReadIchubRedis())
	value, err := suite.inst.Get("key")
	logrus.Info(err, "\r\n", value.String())

}

func (this *TestGredisClientSuite) Test002_Set() {
	logrus.Info(2)

}
func (suite *TestGredisClientSuite) Test003_Incr() {
	var i, err = suite.inst.Incr(gctx.New(), "myid")
	logrus.Info(i, err)
	fmt.Println(i)
}
