package ichubredis

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: ichub_redis_client_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-21 13:16:29)
	@Update 作者: leijmdas@163.com 时间(2024-09-21 13:16:29)

* *********************************************************/

const singleNameIchubRedisClient = "*ichubredis.IchubRedisClient-1c74c52c3cd049e29e23bbacb3b77b51"

// init register load
func init() {
	registerBeanIchubRedisClient()
}

// register IchubRedisClient
func registerBeanIchubRedisClient() {
	basedi.RegisterLoadBean(singleNameIchubRedisClient, LoadIchubRedisClient)
}

func FindBeanIchubRedisClient() *IchubRedisClient {
	bean, ok := basedi.FindBean(singleNameIchubRedisClient).(*IchubRedisClient)
	if !ok {
		logrus.Errorf("FindBeanIchubRedisClient: failed to cast bean to *IchubRedisClient")
		return nil
	}
	return bean

}

func LoadIchubRedisClient() baseiface.ISingleton {
	var s = NewIchubRedisClient()
	InjectIchubRedisClient(s)
	return s

}

func InjectIchubRedisClient(s *IchubRedisClient) {

	//// goutils.Debug("inject")
}
