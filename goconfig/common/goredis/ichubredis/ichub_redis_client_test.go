package ichubredis

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"log"
	"testing"

	_ "github.com/go-sql-driver/mysql"
)

// hset(key, field, value)：向名称为key的hash中添加元素field
// hget(key, field)：返回名称为key的hash中field对应的value
// hmget(key, (fields))：返回名称为key的hash中field i对应的value
// hmset(key, (fields))：向名称为key的hash中添加元素field
// hincrby(key, field, integer)：将名称为key的hash中field的value增加integer
// hexists(key, field)：名称为key的hash中是否存在键为field的域
// hdel(key, field)：删除名称为key的hash中键为field的域
// hlen(key)：返回名称为key的hash中元素个数
// hkeys(key)：返回名称为key的hash中所有键
// hvals(key)：返回名称为key的hash中所有键对应的value
// hgetall(key)：返回名称为key的hash中所有的键（field）及其对应的value
type TestRedisSuite struct {
	suite.Suite
	Redis *IchubRedisClient
}

func (suite *TestRedisSuite) SetupTest() {
	suite.Redis = FindBeanIchubRedisClient()
	suite.OpenRedis()
	logrus.Info("openRedis...")
}

var RedisCli *redis.Client

const SESSION_PREFIX = "session:mgr:"

func (suite *TestRedisSuite) OpenRedis() *redis.Client {
	suite.Redis.Client = redis.NewClient(&redis.Options{
		Addr:     "huawei.akunlong.top:6379",
		Password: "123456",
		DB:       0,
	})

	return suite.Redis.Client
}

func HKeysMgr() *redis.StringSliceCmd {
	return RedisCli.HKeys("mgr")
}

func HGetAll(token string) *redis.StringStringMapCmd {
	logrus.Println(SESSION_PREFIX + token)
	return RedisCli.HGetAll(SESSION_PREFIX + token)
}

func TestTestSuite(t *testing.T) {
	suite.Run(t, new(TestRedisSuite))
}

func (suite *TestRedisSuite) Test0001_Redis() {
	client := suite.Redis.Client
	ping, err := client.Ping().Result()
	fmt.Println(client)
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}
	fmt.Println("connected result: ", ping)

	cmd := client.Set("user", "leijmdas@163.com", 1000000)
	fmt.Println(*cmd)
	str := client.Get("user")
	fmt.Println(str)
	client.Del("user")
}

type User struct {
	basedto.BaseEntity

	Id   int64
	Name string
}

func NewUser() *User {
	var u = &User{}
	u.InitProxy(u)
	return u
}

var cfg = ichubconfig.NewIchubConfig()

func (suite *TestRedisSuite) queryUser() *User {
	var db *sqlx.DB

	ini := func() (err error) {

		dsn := "root:leijmdas@163.comL@tcp(huawei.akunlong.top:13306)/hrms?charset=utf8mb4&parseTime=True"
		// 也可以使用MustConnect连接不成功就panic
		db, err = sqlx.Connect("mysql", dsn)
		if err != nil {
			fmt.Printf("connect DB failed, err:%v\n", err)
			return
		}
		db.SetMaxOpenConns(20)
		db.SetMaxIdleConns(10)
		return
	}
	ini()
	query := func() (u User) {
		sqlStr := "select id, name  from employee where id=?"

		err := db.Get(&u, sqlStr, 1)
		if err != nil {
			fmt.Printf("get failed, err:%v\n", err)
			return
		}
		fmt.Printf("id:%d name:%s  \n", u.Id, u.Name)
		return
	}
	u := query()
	fmt.Println(u)
	return &u
}

func (suite *TestRedisSuite) Test002_redis() {
	u := suite.queryUser()
	client := suite.Redis.Client
	var statcmd = client.Set("user", jsonutils.ToJsonStr(*u), 0)
	user := client.Get("user")
	fmt.Println("user=", statcmd, user)
}

func (suite *TestRedisSuite) Test004_redisKeyInt64() {
	u := suite.queryUser()
	suite.Redis.SetKey(baseutils.Any2Str(u.Id), *u)
	user := suite.Redis.GetKey(baseutils.Any2Str(u.Id))
	fmt.Println("user=", user)
}

func (suite *TestRedisSuite) Test005_redisKeyString() {

	suite.Redis.SetKey("kkk", "vvv")
	result := suite.Redis.GetKey("kkk")
	logrus.Println("result=", result)
	suite.Equal("vvv", result)
}

func (suite *TestRedisSuite) Test006_redisKeyOut() {
	var u = &User{
		Id:   1,
		Name: "leijmdas@163.com",
	}
	u.InitProxy(u)
	suite.Redis.Set("u", u)
	var uu = NewUser()
	suite.Redis.Get("u", uu)
	logrus.Info(uu.ToString())
}
