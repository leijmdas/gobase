package baseconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: base_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 18:37:37)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 18:37:37)

* *********************************************************/

const singleNameBaseConfig = "*ichubconfig.BaseConfig-e4908e2c5d4644739cd1de9ae55d6444"

// init register load
func init() {
	registerBeanBaseConfig()
}

// register BaseConfig
func registerBeanBaseConfig() {
	basedi.RegisterLoadBean(singleNameBaseConfig, LoadBaseConfig)
}

func FindBeanBaseConfig() *BaseConfig {
	bean, ok := basedi.FindBean(singleNameBaseConfig).(*BaseConfig)
	if !ok {
		logrus.Errorf("FindBeanBaseConfig: failed to cast bean to *BaseConfig")
		return nil
	}
	return bean

}

func LoadBaseConfig() baseiface.ISingleton {
	var s = NewBaseConfig()
	InjectBaseConfig(s)
	return s

}

func InjectBaseConfig(s *BaseConfig) {

}
