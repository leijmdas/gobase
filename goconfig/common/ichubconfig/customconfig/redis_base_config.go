package customconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig/baseconfig"
)

//  @Value("${api.url.dbagent:http://127.0.0.1:9701}")

type RedisBaseConfig struct {
	baseconfig.BaseConfig
	Db       int    `gocfg:"${redis.db:10}"`
	Addr     string `gocfg:"${redis.addr:192.168.4.111:16379}"`
	Password int    `gocfg:"${redis.password:112KiZ9dJBSk1cju}"`
}

func NewRedisBaseConfig() *RedisBaseConfig {
	var rbc = &RedisBaseConfig{}
	rbc.InitProxy(rbc)
	return rbc
}
