package customconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: redis_base_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 18:38:25)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 18:38:25)

* *********************************************************/

const singleNameRedisBaseConfig = "*goapolloconfig.RedisBaseConfig-e60c0d18ff434618ac3cf28656cfb7dc"

// init register load
func init() {
	registerBeanRedisBaseConfig()
}

// register RedisBaseConfig
func registerBeanRedisBaseConfig() {
	basedi.RegisterLoadBean(singleNameRedisBaseConfig, LoadRedisBaseConfig)
}

func FindBeanRedisBaseConfig() *RedisBaseConfig {
	bean, ok := basedi.FindBean(singleNameRedisBaseConfig).(*RedisBaseConfig)
	if !ok {
		logrus.Errorf("FindBeanRedisBaseConfig: failed to cast bean to *RedisBaseConfig")
		return nil
	}
	return bean

}

func LoadRedisBaseConfig() baseiface.ISingleton {
	var s = NewRedisBaseConfig()
	InjectRedisBaseConfig(s)
	return s

}

func InjectRedisBaseConfig(s *RedisBaseConfig) {

}
