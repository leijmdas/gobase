package customconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestRedisBaseConfigSuite struct {
	suite.Suite
	inst *RedisBaseConfig

	rootdir string
}

func (this *TestRedisBaseConfigSuite) SetupTest() {
	fileutils.FindRootDir()
	ichublog.InitLogrus()
	this.inst = FindBeanRedisBaseConfig()

	this.rootdir = fileutils.FindRootDir()

}
func (this *TestRedisBaseConfigSuite) TearDownTest() {

}
func (this *TestRedisBaseConfigSuite) SetupSuite() {

}
func (this *TestRedisBaseConfigSuite) TearDownSuite() {

}
func TestRedisBaseConfigSuites(t *testing.T) {
	suite.Run(t, new(TestRedisBaseConfigSuite))
}
func (this *TestRedisBaseConfigSuite) Test001() {
	logrus.Info(FindBeanRedisBaseConfig())
}
