package goapolloconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"github.com/apolloconfig/agollo/v4/env/config"
)

const (
	apollo_cache_dir = "/data/apollo-config-cache"

	default_apollo_host = "http://huawei.akunlong.top:8080"
)

func GetDefaultApolloConfig() *ApolloConfig {
	return apolloConfig
}

var apolloConfig = &ApolloConfig{
	AppID:            "dxp-manager",
	Cluster:          "dev",
	IP:               default_apollo_host,
	NamespaceName:    "TEST1.ichub.yml",
	IsBackupConfig:   true,
	BackupConfigPath: fileutils.FindRootDir() + apollo_cache_dir,
	Secret:           "6ce3ff7e96a24335a9634fe9abca6d51",
}
var WebsiteConfig = &ApolloConfig{
	AppID:            "monitor",
	Cluster:          "dev",
	IP:               default_apollo_host,
	NamespaceName:    "public.yml",
	IsBackupConfig:   true,
	BackupConfigPath: fileutils.FindRootDir() + apollo_cache_dir,
	Secret:           "6ce3ff7e96a24335a9634fe9abca6d51",
}

type ApolloConfig struct {
	basedto.BaseEntity
	cachedir         string
	IP               string
	IsBackupConfig   bool
	AppID            string
	Cluster          string
	NamespaceName    string
	BackupConfigPath string // fileutils.FindRootDir() + "/data/apollocche",
	Secret           string
}

func NewApolloConfig() *ApolloConfig {
	var ac = &ApolloConfig{}
	ac.InitProxy(ac)
	return ac
}
func (this *ApolloConfig) ToAppConfig() *config.AppConfig {

	var c = &config.AppConfig{}
	jsonutils.FromJson(jsonutils.ToJsonPretty(this), c)
	return c
}
