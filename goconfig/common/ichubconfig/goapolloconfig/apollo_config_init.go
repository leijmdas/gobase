package goapolloconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: apollo_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 18:37:37)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 18:37:37)

* *********************************************************/

const singleNameApolloConfig = "*goapolloconfig.ApolloConfig-64df8e1502ed4a4dae8f9cdecea92b18"

// init register load
func init() {
	registerBeanApolloConfig()
}

// register ApolloConfig
func registerBeanApolloConfig() {
	basedi.RegisterLoadBean(singleNameApolloConfig, LoadApolloConfig)
}

func FindBeanApolloConfig() *ApolloConfig {
	bean, ok := basedi.FindBean(singleNameApolloConfig).(*ApolloConfig)
	if !ok {
		logrus.Errorf("FindBeanApolloConfig: failed to cast bean to *ApolloConfig")
		return nil
	}
	return bean

}

func LoadApolloConfig() baseiface.ISingleton {
	var s = GetDefaultApolloConfig()
	InjectApolloConfig(s)
	return s

}

func InjectApolloConfig(s *ApolloConfig) {

	// goutils.Debug("inject")
}
