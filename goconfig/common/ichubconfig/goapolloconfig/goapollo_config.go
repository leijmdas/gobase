package goapolloconfig

//https://gitee.com/apolloconfig/agollo

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	goutils2 "gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"github.com/apolloconfig/agollo/v4"
	"github.com/apolloconfig/agollo/v4/agcache"
	"github.com/apolloconfig/agollo/v4/env/config"
	"github.com/apolloconfig/agollo/v4/storage"
	"github.com/gogf/gf/os/gfile"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"strings"
)

const (
	VAR_FLAG_SINMPLE = "$"
	VAR_FLAG_MEDIUM  = "%%%"
)

type GoapolloConfig struct {
	basedto.BaseEntitySingle
	OutPath string
}

func NewGoapolloConfig() *GoapolloConfig {
	return &GoapolloConfig{
		OutPath: fileutils.FindRootDir() + "\\logs",
	}
}

func (this *GoapolloConfig) GetConfig(ac *ApolloConfig) *storage.Config {
	var c = ac.ToAppConfig()
	client, err := agollo.StartWithConfig(func() (*config.AppConfig, error) {
		return c, nil
	})
	if err != nil {
		panic(err)
	}
	goutils2.Info("初始化Apollo配置成功")
	cache := client.GetConfig(c.NamespaceName)
	return cache
}
func (this *GoapolloConfig) GetConfigCache(ac *ApolloConfig) agcache.CacheInterface {
	var c = ac.ToAppConfig()
	client, err := agollo.StartWithConfig(func() (*config.AppConfig, error) {
		return c, nil
	})
	if err != nil {
		panic(err)
	}
	goutils2.Info("初始化Apollo配置成功")
	cache := client.GetConfigCache(c.NamespaceName)
	return cache
}
func (this *GoapolloConfig) GetContentDefault() string {
	return this.GetContent(apolloConfig)
}

func (this *GoapolloConfig) GetContent(ac *ApolloConfig) string {

	return this.GetConfig(ac).GetContent()
}

/*
* contest :配置信息
* out : 输出配置信息
 */
func (this *GoapolloConfig) Prop2YamlStr(content string) string {
	var outpathfile = this.Prop2Yaml(content)
	defer func() {
		var err = gfile.Remove(outpathfile)
		if err != nil {
			logrus.Error(err.Error())
		}
	}()
	var res, err = fileutils.ReadFileToString(outpathfile)
	if err != nil {
		goutils2.Error(err.Error())
	}
	content = strings.ReplaceAll(res, VAR_FLAG_MEDIUM, VAR_FLAG_SINMPLE)
	goutils2.Info("\r\n", content)
	return content
}

/*
* contest :配置信息
* out : 输出文件名称
 */
func (this *GoapolloConfig) Prop2Yaml(content string) string {
	var infilename = baseutils.Uuid() + "1.properties"
	var inpathfile = this.OutPath + "\\" + infilename

	defer func() {
		var err = gfile.Remove(inpathfile)
		if err != nil {
			goutils2.Error(err)
		}
	}()

	content = strings.ReplaceAll(content, VAR_FLAG_SINMPLE, VAR_FLAG_MEDIUM)
	err := fileutils.WriteBytesFile(inpathfile, []byte(content))
	// 初始化Viper
	v := viper.New()
	// 设置配置文件名（不需要带后缀）
	v.SetConfigName(infilename)
	// 设置配置文件类型
	v.SetConfigType("properties")
	// 添加配置文件所在的路径
	v.AddConfigPath(this.OutPath)
	// 读取配置文件
	if err := v.ReadInConfig(); err != nil {
		logrus.Fatalf("Error reading config infilename, %s", err)
	}
	var outfile = baseutils.Uuid() + "config.yaml"
	var outPathfile = this.OutPath + "/" + outfile
	err = v.WriteConfigAs(outPathfile)
	if err != nil {
		goutils2.Error("Error writing YAML config" + err.Error())
	}

	return outPathfile
}

//https://blog.csdn.net/leijmdas/article/details/141744347
