package goapolloconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: goapollo_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-31 11:35:30)
	@Update 作者: leijmdas@163.com 时间(2024-08-31 11:35:30)

* *********************************************************/

const singleNameGoapolloConfig = "*goapolloconfig.GoapolloConfig-6f0f86b050c34f42903f05d8d767ad3d"

// init register load
func init() {
	registerBeanGoapolloConfig()
}

// register GoapolloConfig
func registerBeanGoapolloConfig() {
	basedi.RegisterLoadBean(singleNameGoapolloConfig, LoadGoapolloConfig)
}

func FindBeanGoapolloConfig() *GoapolloConfig {
	bean, ok := basedi.FindBean(singleNameGoapolloConfig).(*GoapolloConfig)
	if !ok {
		logrus.Errorf("FindBeanGoapolloConfig: failed to cast bean to *GoapolloConfig")
		return nil
	}
	return bean

}

func LoadGoapolloConfig() baseiface.ISingleton {
	var s = NewGoapolloConfig()
	InjectGoapolloConfig(s)
	return s

}

func InjectGoapolloConfig(s *GoapolloConfig) {

	// goutils.Debug("inject")
}
