package goapolloconfig

//https://gitee.com/apolloconfig/agollo

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/reflectutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig/customconfig"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/samber/lo"
	"github.com/sirupsen/logrus"
	"reflect"
	"strings"
	"testing"
)

func Test001_application(t *testing.T) {

	c := GetDefaultApolloConfig()
	c.NamespaceName = "application.properties"
	cache := FindBeanGoapolloConfig().GetConfigCache(c) //client.GetConfig(c.NamespaceName)
	value, err := cache.Get("server.port")

	fmt.Println(value, err)
}

// https://blog.csdn.net/leijmdas/article/details/141744347
func Test002_application(t *testing.T) {

	cache := FindBeanGoapolloConfig().GetConfig(GetDefaultApolloConfig()) //client.GetConfig(c.NamespaceName)
	value := cache.GetValue("software.env")

	fmt.Println(value)

}

// 原文链接：https://blog.csdn.net/leijmdas/article/details/141749048

func Test003_prop2yaml(t *testing.T) {
	var gc = FindBeanGoapolloConfig()
	var out = gc.Prop2Yaml(gc.GetContentDefault())
	logrus.Info(out)

}
func Test004_prop2yamlStr(t *testing.T) {
	var gc = FindBeanGoapolloConfig()
	gc.Prop2YamlStr(gc.GetContentDefault())

}
func Test005_prop2yamlStrApp(t *testing.T) {
	var gc = FindBeanGoapolloConfig()
	var ac = GetDefaultApolloConfig()
	ac.NamespaceName = "application.properties"
	gc.Prop2YamlStr(gc.GetContent(ac))

}
func Test006_WebSiteProp2yamlStrApp(t *testing.T) {
	var gc = FindBeanGoapolloConfig()
	gc.Prop2YamlStr(gc.GetContent(WebsiteConfig))

}
func Test006_WebSiteWebProp2yamlStrApp(t *testing.T) {
	var gc = FindBeanGoapolloConfig()
	gc.Prop2YamlStr(gc.GetContent(WebsiteConfig))

}
func Test007_appwebsite(t *testing.T) {

	cache := FindBeanGoapolloConfig().GetConfigCache(WebsiteConfig) //client.GetConfig(c.NamespaceName)
	value, err := cache.Get("mysql.dbtype")

	fmt.Println(value, err)
}

func Test008_appwebsite(t *testing.T) {

	cache := FindBeanGoapolloConfig().GetConfigCache(WebsiteConfig) //client.GetConfig(c.NamespaceName)
	value, err := cache.Get("mysql.dbtype")

	fmt.Println(value, err)
}

var cfg = ichubconfig.FindBeanIchubConfig().Read()

/*
some is ptr
*/
func SetValue(some any, key string, value any) {
	var svalue = strings.ReplaceAll(gconv.String(value), "${", "")
	svalue = strings.ReplaceAll(svalue, "}", "")

	var values = strings.Split(svalue, ":")
	var rv = cfg.ReadVar(gstr.Trim(values[0]))
	var ifnil bool = rv == ""
	if ifnil && len(values) < 2 {
		return
	}
	if ifnil {
		rv = reflect.ValueOf(gstr.Trim(values[1]))
	}

	var rt = reflect.ValueOf(some).Elem()
	var fieldvalue = rt.FieldByName(key)
	switch fieldvalue.Kind() {
	case reflect.Int:
		fieldvalue.SetInt(gconv.Int64(rv))
	case reflect.String:
		fieldvalue.SetString(gconv.String(rv))
	case reflect.Bool:
		fieldvalue.SetBool(gconv.Bool(rv))
	case reflect.Float64:
		fieldvalue.SetFloat(gconv.Float64(rv))
	case reflect.Float32:
		fieldvalue.SetFloat(gconv.Float64(rv))
	case reflect.Uint:
		fieldvalue.SetUint(gconv.Uint64(rv))
	}

}
func ParseSetValue(some any) {

	var linkmap = reflectutils.ParseGocfgTag(some)

	for _, key := range linkmap.Keys() {
		value, found := linkmap.Get(key)

		if found {
			SetValue(some, key.(string), value)

		}
	}
	logrus.Info(some)
}

func Test009_gocfg(t *testing.T) {

	var bc = customconfig.NewRedisBaseConfig()

	ParseSetValue(bc)
}

//	ParseSetValue(rbc)
//
// rbc.InjectConfigItem(rbc)
func Test012_FindBeanRedisBaseConfig(t *testing.T) {

	customconfig.FindBeanRedisBaseConfig()
	customconfig.FindBeanRedisBaseConfig()

}
func outerFun() func() int {
	count := 0
	return func() int {
		count++
		return count
	}
}
func Test013_FuncProgram(t *testing.T) {

	var f = outerFun()
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	int64s := []int64{1, 2, 3, 4}
	stringstr := lo.Map[int64, string](int64s, func(x int64, _ int) string {
		return fmt.Sprintf("%d", x)
	})
	logrus.Println(stringstr) // 输出：["1" "2" "3" "4"]
}

// numbers := []int{1, 2, 3, 4, 5}
//
//    // 将每个元素乘以 2
//    doubled := lo.Map(numbers, func(n int, _ int) int {
//        return n * 2
//    })
