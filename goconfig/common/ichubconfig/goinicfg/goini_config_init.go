package goinicfg

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: goini_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-09-21 11:10:57)
	@Update 作者: leijmdas@163.com 时间(2024-09-21 11:10:57)

* *********************************************************/

const singleNameGoiniConfig = "*goinicfg.GoiniConfig-466d53254fc248a393e047a74667309b"

// init register load
func init() {
	registerBeanGoiniConfig()
}

// register GoiniConfig
func registerBeanGoiniConfig() {
	basedi.RegisterLoadBean(singleNameGoiniConfig, LoadGoiniConfig)
}

func FindBeanGoiniConfig() *GoiniConfig {
	bean, ok := basedi.FindBean(singleNameGoiniConfig).(*GoiniConfig)
	if !ok {
		logrus.Errorf("FindBeanGoiniConfig: failed to cast bean to *GoiniConfig")
		return nil
	}
	return bean

}

func LoadGoiniConfig() baseiface.ISingleton {
	var s = NewGoiniConfig()
	InjectGoiniConfig(s)
	return s

}

func InjectGoiniConfig(s *GoiniConfig) {

	//// goutils.Debug("inject")
}
