package ichubconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

const singleNameIchubConfig = "IchubConfig"

// init
// register
// load
func init() {
	registerBeanIchubConfig()
}

// register IchubConfig
func registerBeanIchubConfig() {
	basedi.RegisterLoadBean(singleNameIchubConfig, LoadIchubConfig)
}

func FindBeanIchubConfig() *IchubConfig {
	return basedi.FindBean(singleNameIchubConfig).(*IchubConfig)
}

func LoadIchubConfig() baseiface.ISingleton {
	return NewIchubConfig()
}
