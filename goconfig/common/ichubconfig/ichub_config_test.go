package ichubconfig

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/encrypt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/goconfig/common/gopool"
	"github.com/gogf/gf/util/gutil"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	orderedmap "github.com/wk8/go-ordered-map/v2"
	"os"
	"testing"
)

type TestIchubConfigSuite struct {
	basedto.BaseEntity
	suite.Suite
	I int
}

func TestIchubConfigSuites(t *testing.T) {
	suite.Run(t, new(TestIchubConfigSuite))
}

func (testIchubConfigSuite *TestIchubConfigSuite) SetupTest() {

	logrus.Info("SetupTest")

}

func (testIchubConfigSuite *TestIchubConfigSuite) TearDownTest() {

	logrus.Info("TearDownTest")
}

func (testIchubConfigSuite *TestIchubConfigSuite) SetupSuite() {
	testIchubConfigSuite.init()
	logrus.Println("Suite setup: 领域驱动设计，测试驱动开发!")
	ichublog.InitLogrus()

}

func (testIchubConfigSuite *TestIchubConfigSuite) TearDownSuite() {
	logrus.Info("Suite teardown")
}

func (testIchubConfigSuite *TestIchubConfigSuite) init() {
	ichublog.InitLogrus()
	fileutils.FindRootDir()

}
func (testIchubConfigSuite *TestIchubConfigSuite) Test001_LogString() {
	var ichubConfig = NewIchubConfig()
	ichubConfig.Read()
	ichublog.Log(os.Getenv(baseconsts.IchubBasePath))
}

func (testIchubConfigSuite *TestIchubConfigSuite) Test002_ReadIchubGorm(t *testing.T) {
	var ichubConfig = NewIchubConfig()
	var dto = ichubConfig.ReadIchubGorm()

	ichublog.Log(ichubConfig, dto)
}

func (testIchubConfigSuite *TestIchubConfigSuite) Test003_TestParseEnvvar() {
	os.Setenv("WEB_SERVER_HOST", "huawei.web.test")
	logrus.Info(os.Getenv("WEB_SERVER_HOST"))
	var ichubConfig = NewIchubConfig().Read()
	testIchubConfigSuite.Equal(os.Getenv("WEB_SERVER_HOST"), ichubConfig.Web.Server.Name)
	var dto = ichubConfig.ReadIchubMysql()

	testIchubConfigSuite.Equal("leijmdas@163.comL", dto.Password)
	ichublog.Log(ichubConfig)

}
func (testIchubConfigSuite *TestIchubConfigSuite) Test004_TestParseReadMap() {

	var ichubConfig = NewIchubConfig().Read()

	m := ichubConfig.ReadMap("web")
	logrus.Info(jsonutils.ToJsonPretty(m))
}

func (testIchubConfigSuite *TestIchubConfigSuite) Test005_TestReadStruct() {

	var clientDto = baseconfig.NewSwaggerClientDto()
	NewIchubConfig().RegisterEncDec(encrypt.FindBeanEncDec()).ReadStruct("web.swagger", clientDto)
	logrus.Info(clientDto)

}
func (testIchubConfigSuite *TestIchubConfigSuite) Test006_TestReadGateway() {

	var cfg = NewIchubConfig()
	var g = cfg.ReadGateway()
	var name = g.FindServiceName("/deptemp")

	ichublog.Log(name)
	var n = cfg.ReadNats()
	logrus.Info(n.ToPrettyString())
	var webcli = cfg.ReadIchubWebClient()
	goutils.Info(webcli)

	var w = cfg.ReadIchubWebServer()
	goutils.Info(w)

}

type Employee struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

func NewEmployee() *Employee {
	return &Employee{}
}
func (this *TestIchubConfigSuite) Test007_() {

	var f = gconv.Float32("2222")
	logrus.Info(f)
	var ee = NewEmployee()
	ee.Code = "112"
	var ees = []*Employee{ee}
	var e = NewEmployee()
	var es = []*Employee{}
	gconv.Struct(ee, e)
	gconv.Structs(ees, &es)

	goutils.Info("es=", jsonutils.ToJsonPretty(es))
}
func (this *TestIchubConfigSuite) Test008_gocenter() {
	//	github.com/gogf/gf/util/gconv
	var cfg = FindBeanIchubConfig()
	var g = cfg.ReadGocenter()
	goutils.Info(g)
}
func (this *TestIchubConfigSuite) Test009_gocenter() {
	//	github.com/gogf/gf/util/gconv
	var cfg1 = FindBeanIchubConfig()
	var cfg = FindBeanIchubConfig()
	var g = cfg.ReadIchubMysql()
	goutils.Info(g, cfg1)
}
func (this *TestIchubConfigSuite) Test009_cfgsingle() {
	//	github.com/gogf/gf/util/gconv
	var cfg = FindBeanIchubConfig()
	var cfg1 = FindBeanIchubConfig()
	var g = cfg.ReadIchubMysql()
	goutils.Info(g, cfg1)
}
func (this *TestIchubConfigSuite) Test010_readConfigFile() {
	//	github.com/gogf/gf/util/gconv
	var cfg = FindBeanIchubConfig()
	goutils.Info(cfg.ReadConfigFile())
}
func (this *TestIchubConfigSuite) Test011_readConfigDev() {

	var cfg = NewIchubConfig()
	goutils.Info(cfg.ReadDev())
}
func (this *TestIchubConfigSuite) Test012_readConfigTest() {

	var cfg = NewIchubConfig()
	goutils.Info(cfg.ReadTest())
}
func (this *TestIchubConfigSuite) Test013_NewOfPtrType() {

	var obj = baseutils.NewOfPtrType[*basedto.IchubResult]()
	logrus.Info(obj)
}
func (this TestIchubConfigSuite) Init() {

}
func (this TestIchubConfigSuite) Shutdown() {

}

func (this *TestIchubConfigSuite) Test014_NewOfType() {

	var obj = baseutils.NewOfPtrTypeProxy[*TestIchubConfigSuite]()
	logrus.Info(obj.ToPrettyString())
}

type TestPool struct {
	basedto.BaseEntitySingle
	*gopool.GeneralObjectPool[*TestIchubConfigSuite]
}

func NewTestPool() *TestPool {
	return &TestPool{
		GeneralObjectPool: gopool.NewGeneralObjectPool[*TestIchubConfigSuite](),
	}
}

func (this *TestIchubConfigSuite) Test015_NewOfType() {

	var obj = baseutils.NewOfPtrType[TestIchubConfigSuite]()
	logrus.Info(obj.ToPrettyString())
}
func (this *TestIchubConfigSuite) Test016_borrow() {

	var pool = NewTestPool()
	var o1, _ = pool.BorrowObject(context.Background())
	var o2, _ = pool.BorrowObject(context.Background())

	logrus.Info(o1, o2)

}

func (this *TestIchubConfigSuite) Test018_ordermapType() {
	// 创建一个有序的map
	ordermap := orderedmap.New[string, string]()

	// 添加键值对
	ordermap.Set("key1", "value1")
	ordermap.Set("key2", "value2")
	ordermap.Set("key3", "value3")
	for pair := ordermap.Oldest(); pair != nil; pair = pair.Next() {
		logrus.Info(pair.Value, pair.Key)
	}

	var lmap = gmap.NewListMap()
	lmap.Set("ee", "1")
	logrus.Info(jsonutils.ToJsonPretty(lmap))
	var llmap = gmap.NewTreeMap(gutil.ComparatorString)
	llmap.Set("ee", "12")
	llmap.Set("e11e", "12")
	llmap.Set("222ee", "12")
	logrus.Info(jsonutils.ToJsonPretty(llmap))

}

func (this *TestIchubConfigSuite) Test019_gmap() {
	// 创建一个有序的map

	var lmap = gmap.NewListMap()
	lmap.Set("ee", "1")
	logrus.Info(jsonutils.ToJsonPretty(lmap))
	var llmap = gmap.NewTreeMap(gutil.ComparatorString)
	llmap.Set("ee", "12")
	llmap.Set("e11e", "12")
	llmap.Set("222ee", "12")
	logrus.Info(jsonutils.ToJsonPretty(llmap))

}
func (this *TestIchubConfigSuite) Test020_ghashmap() {
	// 创建一个有序的map

	var llmap = gmap.NewHashMap()

	llmap.Set("name", "12")
	llmap.Set("id", "12")
	llmap.Set("age", "12")
	logrus.Info(jsonutils.ToJsonPretty(llmap))
	var keys = llmap.Keys()
	logrus.Info(jsonutils.ToJsonPretty(keys))
}
func (this *TestIchubConfigSuite) Test021_ghashmap() {

	var soft = FindBeanIchubConfig().Read().Software
	soft.InitProxy(soft)
	golog.Info(soft)
	golog.Info(FindBeanIchubConfig())
}
func (this *TestIchubConfigSuite) Test022_lognil() {

	golog.Info(nil)
}
func (this *TestIchubConfigSuite) Test030_FindBeanFactroy() {
	var c1 = basedi.FindBeanGeneral[*IchubConfig]()
	var c2 = basedi.FindBeanGeneral[*IchubConfig]()
	c1.Read()
	golog.Info(c1, c2, c1 == c2, basedto.BaseEntity{})

}
