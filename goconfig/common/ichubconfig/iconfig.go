package ichubconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
)

type IConfig interface {
	Read() *IchubConfig

	ReadMap(pathkey string) map[string]interface{}
	ReadVar(pathkey string) interface{}
	ReadInt(pathkey string) int
	ReadString(pathkey string) string
	ReadStruct(pathkey string, object interface{})

	RegisterEncDec(encdec baseiface.IEncDec) *IchubConfig

	ReadIchubDb() *baseconfig.DbClientDto
	ReadIchubDatasource() *baseconfig.DbClientDto
	ReadIchubMysql() *baseconfig.DbClientDto
	ReadIchubRpc() *baseconfig.RpcServerDto
	ReadIchubEs() *baseconfig.ElasticClientDto
	ReadIchubWebServer() (serverDto *baseconfig.WebServerDto)
	ReadIchubWebClient() (clientDto *baseconfig.WebClientDto)
	ReadWebSwagger() *baseconfig.SwaggerClientDto
}
