package ichubconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestIConfigSuite struct {
	suite.Suite
}

func TestIConfigSuites(t *testing.T) {
	suite.Run(t, new(TestIConfigSuite))
}

func (testIConfigSuite *TestIConfigSuite) SetupTest() {

	logrus.Info("SetupTest")

}

func (testIConfigSuite *TestIConfigSuite) TearDownTest() {

	logrus.Info("TearDownTest")
}

func (testIConfigSuite *TestIConfigSuite) SetupSuite() {
	logrus.Println("SetupSuite 领域驱动设计，测试驱动开发!")
	ichublog.InitLogrus()
	fileutils.FindRootDir()

}

func (testIConfigSuite *TestIConfigSuite) TearDownSuite() {
	logrus.Println("Suite teardown")
}

func (testIConfigSuite *TestIConfigSuite) Test0001_urlEnv() {
	var clientDto = baseconfig.NewSwaggerClientDto()
	NewIchubConfig().ReadStruct("web.swagger", clientDto)
	logrus.Info(clientDto)
}

func (testIConfigSuite *TestIConfigSuite) Test0002_gateway() {

	var cfg = NewIchubConfig()
	var m = cfg.ReadVar("gateway.routes")

	for _, v := range m.([]any) {
		logrus.Info(jsonutils.ToJsonPretty(v.(map[string]any)))
	}
	var g = cfg.ReadGateway()
	logrus.Info(jsonutils.ToJsonPretty(g))
}
func (testIConfigSuite *TestIConfigSuite) Test0003_gateway() {

	var cfg = FindBeanIchubConfig()
	var g = cfg.ReadGateway()
	logrus.Info(jsonutils.ToJsonPretty(g))
}
