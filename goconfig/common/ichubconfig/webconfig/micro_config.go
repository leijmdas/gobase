package webconfig

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/micro/go-micro/v2/config"
	"github.com/micro/go-micro/v2/config/reader"
	"github.com/micro/go-micro/v2/config/source/file"
)

/*
@Title    文件名称: MicroConfig.go
@Description  描述: MicroConfig

@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/
type MicroConfig struct {
	Conf config.Config
}

func (cfg *MicroConfig) Load() (config.Config, error) {
	conf, err := config.NewConfig()
	if err != nil {
		fmt.Println(conf)
		return conf, err
	}

	home := fileutils.FindRootDir() //.Getwd()
	fileSource := file.NewSource(
		file.WithPath(home + ichubconfig.ConfigfileApp),
	)

	// Load file source
	err = conf.Load(fileSource)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	cfg.Conf = conf

	return conf, nil
}

func (cfg *MicroConfig) Get(paths ...string) reader.Value {
	return cfg.Conf.Get(paths...)
}
