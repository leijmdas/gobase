package webconfig

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
)

type WebConfig struct {
	basedto.BaseEntitySingle
	RootDir string
	CfgFile string
}

func NewWebConfig() *WebConfig {
	return &WebConfig{
		CfgFile: "/config/app-%s.yml",
	}
}

func (this *WebConfig) BuildFileName(env string) string {

	this.RootDir = fileutils.FindRootDir()
	var filename = this.RootDir + fmt.Sprintf(this.CfgFile, env)

	return filename
}
