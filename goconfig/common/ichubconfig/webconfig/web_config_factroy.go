package webconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
)

type WebConfigFactroy struct {
	basedto.BaseEntitySingle
	WebConfig *WebConfig

	Dev     *WebConfig
	Test    *WebConfig
	Release *WebConfig
	Master  *WebConfig
}

func NewWebConfigFactroy() *WebConfigFactroy {
	return &WebConfigFactroy{
		Dev:       NewWebConfig(),
		Test:      NewWebConfig(),
		Release:   NewWebConfig(),
		Master:    NewWebConfig(),
		WebConfig: NewWebConfig(),
	}
}

func (this *WebConfigFactroy) Load() *WebConfigFactroy {

	return this
}
