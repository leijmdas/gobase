package webconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: web_config_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-19 16:47:39)
	@Update 作者: leijmdas@163.com 时间(2024-08-19 16:47:39)

* *********************************************************/

const singleNameWebConfigFactroy = "*gocontext.WebConfigFactroy-a6509b2e1dbe4c71ab3936ee399f0a8b"

// init register load
func init() {
	registerBeanWebConfigFactroy()
}

// register WebConfigFactroy
func registerBeanWebConfigFactroy() {
	basedi.RegisterLoadBean(singleNameWebConfigFactroy, LoadWebConfigFactroy)
}

func FindBeanWebConfigFactroy() *WebConfigFactroy {
	bean, ok := basedi.FindBean(singleNameWebConfigFactroy).(*WebConfigFactroy)
	if !ok {
		logrus.Errorf("FindBeanWebConfigFactroy: failed to cast bean to *WebConfigFactroy")
		return nil
	}
	return bean

}

func LoadWebConfigFactroy() baseiface.ISingleton {
	var s = NewWebConfigFactroy()
	InjectWebConfigFactroy(s)
	return s

}

func InjectWebConfigFactroy(s *WebConfigFactroy) {

	// goutils.Debug("inject")
}
