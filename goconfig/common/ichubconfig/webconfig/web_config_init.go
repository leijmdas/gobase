package webconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: web_config_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-19 16:33:16)
	@Update 作者: leijmdas@163.com 时间(2024-08-19 16:33:16)

* *********************************************************/

const singleNameWebConfig = "*ichubconfig.WebConfig-e6a1ddde2f334f42994e966c5ae988fd"

// init register load
func init() {
	registerBeanWebConfig()
}

// register WebConfig
func registerBeanWebConfig() {
	basedi.RegisterLoadBean(singleNameWebConfig, LoadWebConfig)
}

func FindBeanWebConfig() *WebConfig {
	bean, ok := basedi.FindBean(singleNameWebConfig).(*WebConfig)
	if !ok {
		logrus.Errorf("FindBeanWebConfig: failed to cast bean to *WebConfig")
		return nil
	}
	return bean

}

func LoadWebConfig() baseiface.ISingleton {
	var s = NewWebConfig()
	InjectWebConfig(s)
	return s

}

func InjectWebConfig(s *WebConfig) {

	// goutils.Debug("inject")
}
