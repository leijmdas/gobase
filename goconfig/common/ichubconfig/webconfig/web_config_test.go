package webconfig

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestWebConfigSuite struct {
	suite.Suite
	inst *WebConfig

	rootdir string
}

func (this *TestWebConfigSuite) SetupTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanWebConfig()

	this.rootdir = fileutils.FindRootDir()

}

func TestWebConfigSuites(t *testing.T) {
	suite.Run(t, new(TestWebConfigSuite))
}

func (this *TestWebConfigSuite) Test001_BuildFileName() {
	logrus.Info("\r\n", this.inst.BuildFileName(baseconsts.ENV_DEV))

}
