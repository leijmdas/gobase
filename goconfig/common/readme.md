# base基础公共结构对象

## basedi 依赖注入容器
## cmd 加密解密工具代码

## dbcontent 数据库连接 GetDB()
## gocache公共缓存
###    gobigcache暂未启用
###    goredis redis客户端，不建议使用
###    gocache  服务内缓存， 已经启用

### gorpc rpc公共代码 client/server 示例

## ichubconfig
###  "github.com/spf13/viper"
    单例 ichubconfig.FindBeanIchubConfig()
    config目录下配置处理

## gocontext
    IchubClientFactroy 对config配置的client产生单实例

## goelastic
    esclient客户端代码，配置了合适的连接池

## ichubetcd ETCD客户端
    web gorpc esserver/web gorpc client 等使用
    webclient客户端代码，配置了合适的连接池

## ichublog 统一日志
    ruleserver规则引擎使用，采用单独的日志文件
###  golog
####  golog.Error Info Stat不同文件

## goredis redis客户端
    集成gredis,容易使用
