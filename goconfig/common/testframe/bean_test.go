package testframe

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/gometa"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/morrisxyang/xreflect"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"reflect"
	"testing"
)

type TestBeanSuite struct {
	suite.Suite
}

func (this *TestBeanSuite) SetupSuite() {
	golog.Info(" setup suite")
	ichublog.InitLogrus()
}

func (suite *TestBeanSuite) SetupTest() {
	logrus.Println(" setup website-admin")
}

func (suite *TestBeanSuite) TearDownTest() {
	logrus.Println(" teardown website-admin")
}
func (suite *TestBeanSuite) TearDownSuite() {
	logrus.Println(" teardown suite")
}

func TestBeanSuites(t *testing.T) {
	suite.Run(t, new(TestBeanSuite))
}

type Stru struct {
	basedto.BaseEntity
	S1  *Stru
	S2  *Stru
	Dto baseconfig.DbClientDto
	I   int
	*baseconfig.DbClientDto
	gometa.Gometa
}

func NewStru() *Stru {
	var s = &Stru{}
	s.InitProxy(s)

	return s
}

func (r *TestBeanSuite) FindStruFields(v any) []reflect.StructField {
	var fields = make([]reflect.StructField, 0)
	if !baseutils.IfTypeStru(reflect.TypeOf(v)) {
		logrus.Info("v is not stru")
		return fields
	}

	var t = reflect.TypeOf(v)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	for i := 0; i < t.NumField(); i++ {
		if t.Field(i).Type.Kind() == reflect.Ptr &&
			t.Field(i).Type.Elem().Kind() == reflect.Struct {

			fields = append(fields, t.Field(i))
			continue
		}
		if t.Field(i).Type.Kind() == reflect.Struct {

			fields = append(fields, t.Field(i))
			continue
		}
	}

	return fields

}
func (self *TestBeanSuite) Autowired(some any) error {

	var fields = baseutils.FindStructFields(some)
	for _, field := range fields {
		var t = field.Type
		if t.Kind() == reflect.Ptr {
			var baseproxy, ok = baseutils.NewProxyTypeOf(t)
			if ok {
				err := xreflect.SetField(some, field.Name, baseproxy)
				if err != nil {
					logrus.Error(err)
					continue
				}
			}
		}
	}

	return nil
}

func (this *TestBeanSuite) Test001_FindStructFields() {
	var some = NewStru()

	var fields = baseutils.FindStructFields(some)
	for _, field := range fields {
		var t = field.Type

		if t.Kind() == reflect.Ptr {
			var v, ok = baseutils.NewProxyTypeOf(t)
			fmt.Println("v =", v, ok)
			if ok {
				err := xreflect.SetField(some, field.Name, v)
				logrus.Error(err)
			}
			t = t.Elem()
		}
		fmt.Println(field.Name, " is stru ", t.Name())
	}
	some.S1.I = 13

	logrus.Info(some)
}
func SetFieldValue(some any, key string, value any) error {
	v := reflect.ValueOf(some).Elem()

	k := v.FieldByName(key)
	if k.CanSet() {
		return xreflect.SetField(some, key, value)
	}
	return nil

}
func SetFieldValues(some any, key string, value any) error {
	Values, _ := xreflect.Fields(some)
	for k, _ := range Values {
		logrus.Info("jkey k=", k, " ", key)
		if k == key {
			err := xreflect.SetField(some, k, value)
			return err
		}
	}
	return errors.New("key not found!")

}

func (this *TestBeanSuite) Test002_Autowired() {
	var s = basedi.NewBeanFactroy[*Stru]()
	var stru = NewStru()
	stru.S1 = NewStru()
	stru.S1.I = 29291
	s.Autowired(stru)
	stru.S2.I = 2222224
	stru.DbClientDto.Username = "leijmdas"
	golog.Info(stru)

}
func (this *TestBeanSuite) Test003_FindStructFields() {
	var stru = NewStru()
	baseutils.Autowired(stru)
	stru.S1.I = 2333322
	stru.DbClientDto.Username = "leijmdas"
	golog.Info(stru)
}
func (this *TestBeanSuite) Test004_FindStructFields() {
	var stru = basedi.FindBeanGeneral[*Stru]()
	stru.Autowire()
	stru.S1.I = 2333322
	stru.DbClientDto.Username = "leijmdas"
	golog.Info(stru)
	golog.Info("existStru Gometa = ", stru.ContainsStru(gometa.Gometa{}))

}

type Stu struct {
	basedto.BaseEntity
	Id  *int
	Id1 *int32
	Id2 *int64
	B   *bool
	S   *string
}

func NewStu() *Stu {
	var s = &Stu{}
	s.InitProxy(s)
	return s
}

func (this *TestBeanSuite) Test010_IniStru() {
	var i = 2
	var stu = NewStu()
	stu.Id = &i
	baseutils.InitStruNilPtrFields(stu)
	golog.Info(stu)
}

type Student struct {
	basedto.BaseEntity
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func NewStudent() *Student {
	var s = &Student{}
	s.InitProxy(s)
	return s
}

func (this *TestBeanSuite) Test011_Stru() {

	var stu = NewStudent()
	stu.Id = 1
	stu.Name = "songly"

	golog.Info(stu)
}
