
rule=func_dbmetadata
rule "func-dbmetadata-rule" "func-dbmetadata-rule" salience 0
begin

    IchubLog(In)

    id=@id
    Out.SetParam("FuncId","func-dbmetadata-rule")
    DbResult =  RuleFunc.DbMetadataQuery(In)

    Out.SetReturn(200,"计算成功")
    Out.SetParam("DbResult",DbResult)

end
param=
{
     "page_size": 0,
     "current": 0,
     "order_by": null,
     "fields": null,
     "table_name": "",
     "fields_name": "",
     "time_to_int64": false
}
result=
{
     "DbResult": {
          "code": 500,
          "msg": "表不存在！"
     },
     "FuncId": "func-dbmetadata-rule",
     "ReturnMsg": "计算成功",
     "ReturnValue": 200
}