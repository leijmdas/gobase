#!/bin/sh

export GO111MODULE=on
export GOPROXY=https://goproxy.io

docker-compose down
docker rmi ichub-dbagent
docker-compose build
docker-compose up -d
docker logs ichub-dbagent