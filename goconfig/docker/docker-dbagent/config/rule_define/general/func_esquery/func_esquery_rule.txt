rule "func-esquery-rule" "func-esquery-rule" salience 0
begin
    IchubLog(In)

    id=@id
    Out.SetParam("FuncId","func-esquery-rule")
    EsResult = RuleFunc.EsQuery(In)

    Out.SetReturn(200,"计算成功")
    Out.SetParam("EsResult",EsResult)

end