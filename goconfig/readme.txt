module gitee.com/ichub/goconfig

go 1.20

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0 // indirect
