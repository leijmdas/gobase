
# godi
 ![difactroy.png](doc%2Fdifactroy.png)
![img.png](img.png)
![agg.png](agg.png)
## 自动注入配置

type SingleEntity struct {
	basedto.BaseEntitySingle

	// 以下是依赖注入的示例
	// 注入的对象可以是单例、绑定、新建、无注入
	//auto  通过LoadBean()方法注入
	MultiAuto   *multi.MultiEntity `godi:"auto"`
	//bind  通过BindUp()方法注入
	MultiBind   *multi.MultiEntity `godi:"bind"`
	//new   通过New()方法注入
	MultiNew    *multi.MultiEntity `godi:"new"`
	//none  不注入
	MultiNone   *multi.MultiEntity `godi:"none"`
	//single  单例; 注入的对象为简单NEW
	MultiSingle *multi.MultiEntity `godi:"single"`

	Id   int
	Name string
}

// 注册一个成员加载函数
func (this *SingleEntity) Bind() {

	this.BindUp("MultiBind", multi.LoadMultiEntity)
}


## 使用步骤

* 1 配置config/di/template模板
* 2 定义结构体继承BaseEntity(Single) / 组合
* 3 执行：
func (this *TestDiFactoySuite) Test001_MakeDiOne() {

	var fileinfo = this.DiFactroy.Parse("./difactroy/DiFactroy.go")
	fileinfo.StructInfos[0].ForceBuild = true
	this.DiFactroy.MakeBatch(fileinfo.StructInfos[0])
}

* // 生成所有继承baseentity的结果
func (this *TestDiFactoySuite) Test002_MakeDiAll() {

	//this.DiFactroy.MakeDiAll()
	this.DiFactroy.MakeDiAllForce(false)

}

// 生成所有继承baseentity的结果
func (this *TestDiFactoySuite) Test003_MakeDiAllForce() {

	//this.DiFactroy.MakeDiAll()
	this.DiFactroy.MakeDiAllForce(true)

}

## 自动代码生成
/*
@Title  文件名称: single_entity_init.go
@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-04-23 13:31:36)
	@Update 作者: leijmdas@163.com 时间(2024-04-23 13:31:36)
*/

const single_nameSingleEntity = "SingleEntity"

// init register load
func init() {
registerBeanSingleEntity()
}

// register SingleEntity
func registerBeanSingleEntity() {
basedi.RegisterLoadBean(single_nameSingleEntity, LoadSingleEntity)
}

func FindBeanSingleEntity() *SingleEntity {
return basedi.FindBean(single_nameSingleEntity).(*SingleEntity)
}

func LoadSingleEntity() baseiface.ISingleton {
var s = NewSingleEntity()
InjectSingleEntity(s)
return s

}

func InjectSingleEntity(s *SingleEntity)    {

	// 自动实例化
	s.MultiAuto = multi.FindBeanMultiEntity()

	// 实例注册函数
    s.Bind()

	// 实例查找函数
	var find = s.FindBinding("MultiBind")
	if find != nil {
		logrus.Debug("find binded LoadBeanMultiEntity!")
		// 类型断言执行load
		s.MultiBind  = find().(*multi.MultiEntity)
	} else {
		// 未找到绑定，实例化
		s.MultiBind  = &multi.MultiEntity{}
		logrus.Error("no binding LoadBeanMultiEntity!")
	}

	// 实例化默认New
	s.MultiNew = multi.NewMultiEntity()

	// 简单实例化
	s.MultiSingle = &multi.MultiEntity{}
    // logrus.Debug("inject")
}





* 功能简介
** 支持环境变量

** 支持不同配置文件

* 配置文件

** 环境根文件： /config/app-env.yml

** 环境文件：  /config/app-dev.yml

* go环境变量

go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct
go env -w GOPRIVATE=git.ichub.com,gitee.com
go env -w GOINSECURE=git.ichub.com

![diframe.png](doc%2Fdiframe.png)

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

7. ![FamilyBucket .png](doc%2FFamilyBucket%20.png)
![config.png](doc%2Fconfig.png)