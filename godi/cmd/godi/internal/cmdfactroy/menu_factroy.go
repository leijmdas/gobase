package cmdfactroy

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils/goconfig"
	"github.com/spf13/cobra"
)

var version = "1.0.0"

const BASE_PKG = "gitee.com/leijmdas/gobase/godi/di/codefactroy"

type MenuFactroy struct {
	basedto.BaseEntitySingle

	version    string
	cmdName    string
	basePkg    string
	RootCmd    *cobra.Command
	versionCmd *cobra.Command
}

func (self *MenuFactroy) BasePkg() string {
	return self.basePkg
}

func (self *MenuFactroy) SetBasePkg(basePkg string) {
	self.basePkg = basePkg
}

func NewMenuFactroy() *MenuFactroy {
	var f = &MenuFactroy{
		cmdName: "godi",
		version: version,
		basePkg: BASE_PKG,
	}

	return f
}

// goconfig.FindBeanGominiFilecfg().InitPkg(BASE_PKG)
func (self *MenuFactroy) Init() *MenuFactroy {
	goconfig.FindBeanGominiFilecfg().InitPkg(self.basePkg)

	self.rootCmds()
	self.versionCmds()
	self.AddCommand(self.versionCmd)
	return self
}
func (self *MenuFactroy) AddCommand(cmd *cobra.Command) *MenuFactroy {

	self.RootCmd.AddCommand(cmd)
	return self
}
func (self *MenuFactroy) AddCommands(cmds ...*cobra.Command) *MenuFactroy {
	for _, cmd := range cmds {
		self.RootCmd.AddCommand(cmd)
	}
	return self
}
func (self *MenuFactroy) AddSubCmds(parentTitle string, cmds ...*cobra.Command) *MenuFactroy {
	var pcmd = &cobra.Command{
		Use:   parentTitle,
		Short: parentTitle,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(parentTitle)
		},
	}
	for _, cmd := range cmds {
		pcmd.AddCommand(cmd)
	}
	self.RootCmd.AddCommand(pcmd)
	return self
}

func (self *MenuFactroy) Execute() error {

	return self.RootCmd.Execute()
}

func (self *MenuFactroy) rootCmds() {

	self.RootCmd = &cobra.Command{
		Use:   self.cmdName,
		Short: "A godi : a tool of godimenu",
		Long:  `This is a godi application using Cobra`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Hello, godi!")
		},
	}
}
func (self *MenuFactroy) versionCmds() {
	self.versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Print the version number of godi",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("godi version %s\n", self.version)
		},
	}
}

func (self *MenuFactroy) CmdName() string {
	return self.cmdName
}

func (self *MenuFactroy) SetCmdName(cmdName string) {
	self.cmdName = cmdName
}

func (self *MenuFactroy) Version() string {
	return self.version
}

func (self *MenuFactroy) SetVersion(version string) {
	self.version = version
}
