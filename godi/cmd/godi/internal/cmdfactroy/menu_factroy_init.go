package cmdfactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: menu_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameMenuFactroy = "*cmdfactroy.MenuFactroy-9ce241506ed8466599354932fc7662b3"

// init register load
func init() {
	registerBeanMenuFactroy()
}

// register MenuFactroy
func registerBeanMenuFactroy() {
	basedi.RegisterLoadBean(singleNameMenuFactroy, LoadMenuFactroy)
}

func FindBeanMenuFactroy() *MenuFactroy {
	var inst, ok = basedi.FindBeanOk(singleNameMenuFactroy)
	if !ok {
		logrus.Error("not found MenuFactroy!")
		return nil
	}
	bean, ok := inst.(*MenuFactroy)
	if !ok {
		logrus.Errorf("FindBeanMenuFactroy: failed to cast bean to *MenuFactroy")
		return nil
	}
	return bean

}
func FindBeanMenuFactroyOk() (*MenuFactroy, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameMenuFactroy)
	if !ok {
		logrus.Error("not found MenuFactroy!")
		return nil, ok
	}
	bean, ok := inst.(*MenuFactroy)
	if !ok {
		logrus.Errorf("FindBeanMenuFactroy: failed to cast bean to *MenuFactroy")
		return nil, ok
	}
	return bean, ok

}

func LoadMenuFactroy() baseiface.ISingleton {
	var s = NewMenuFactroy()
	InjectMenuFactroy(s)
	return s

}

func InjectMenuFactroy(s *MenuFactroy) {

	goutils.Debug("inject")
}
