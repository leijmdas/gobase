package cmdfactroy

type MenuFactroyOption func(entity *MenuFactroy)

func (self *MenuFactroy) MenuFactroyOption(opts ...MenuFactroyOption) {
	for _, opt := range opts {
		opt(self)
	}
}

//{{.optionFuncs}}

/*
func (self *MenuFactroy) WithId(v int) MenuFactroyOption {
	return func(u *MenuFactroy) {
		u.Id = v
	}
}

func (self * WithName(name string) MenuFactroyOption {
	return func(u *MenuFactroy) {
		u.Name = name
	}
}*/
