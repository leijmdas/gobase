package cmdfactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestMenuFactroySuite struct {
	suite.Suite
	inst *MenuFactroy

	rootdir string
}

func (this *TestMenuFactroySuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanMenuFactroy()

	this.rootdir = fileutils.FindRootDir()

}

func TestMenuFactroySuites(t *testing.T) {
	suite.Run(t, new(TestMenuFactroySuite))
}

func (this *TestMenuFactroySuite) Test001_BasePkg() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test002_SetBasePkg() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test003_Init() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test004_AddCommand() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test005_AddCommands() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test006_AddSubCmds() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test007_Execute() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test008_rootCmds() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test009_versionCmds() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test010_CmdName() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test011_SetCmdName() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test012_Version() {
	logrus.Info(1)

}

func (this *TestMenuFactroySuite) Test013_SetVersion() {
	logrus.Info(1)

}
