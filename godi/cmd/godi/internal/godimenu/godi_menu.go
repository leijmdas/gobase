package godimenu

import (
	"fmt"
	"gitee.com/leijmdas/gobase/godi/cmd/godi/internal/cmdfactroy"
	"gitee.com/leijmdas/gobase/godi/di/difactroy"

	"github.com/spf13/cobra"
)

var DiCmds = []*cobra.Command{
	ForceCmd,
	AllCmd,
	FileCmd,
	StruCmd,
	ListStruCmd,
}

var ForceCmd = &cobra.Command{
	Use:   "force",
	Short: "force MakeDi all struct",
	Run: func(cmd *cobra.Command, args []string) {
		difactroy.FindBeanDiFactroy().MakeDiAllForce(true)
	},
}
var AllCmd = &cobra.Command{
	Use:   "all",
	Short: "MakeDi all struct",
	Run: func(cmd *cobra.Command, args []string) {
		difactroy.FindBeanDiFactroy().MakeDiAll()
	},
}
var FileCmd = &cobra.Command{
	Use:   "file [flag]",
	Short: "MakeDi one file flag",
	Run: func(cmd *cobra.Command, args []string) {

		flag := cmd.Flags().Arg(0)
		difactroy.FindBeanDiFactroy().MakeDiFile(flag)
	},
}
var StruCmd = &cobra.Command{
	Use:   "stru [flag]",
	Short: "MakeDi one struct flag",
	Run: func(cmd *cobra.Command, args []string) {
		flag := cmd.Flags().Arg(0)
		difactroy.FindBeanDiFactroy().MakeDiStru(flag)
	},
}
var ListStruCmd = &cobra.Command{
	Use:   "list ",
	Short: "MakeDi list all struct!",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("list all struct")
		difactroy.FindBeanDiFactroy().ListDiAll()
	},
}

func iniMenu() {
	var factroy = cmdfactroy.FindBeanMenuFactroy()

	// i all
	factroy.AddSubCmds("godi", DiCmds...)
	factroy.AddSubCmds("suite", SuiteCmds...)
}
func Execute() {

	cmdfactroy.FindBeanMenuFactroy().Init()
	iniMenu()
	cmdfactroy.FindBeanMenuFactroy().Execute()
}
