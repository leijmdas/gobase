package godimenu

import (
	"gitee.com/leijmdas/gobase/godi/di/difactroy"
	"github.com/spf13/cobra"
)

var OptCmd = &cobra.Command{
	Use:   "opt [flag]",
	Short: "Make opt of one struct flag",
	Run: func(cmd *cobra.Command, args []string) {
		//flag := cmd.Flags().Arg(0)
		difactroy.FindBeanDiFactroy().MakeOption()
	},
}
