package godimenu

import (
	"fmt"
	"gitee.com/leijmdas/gobase/godi/di/difactroy"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var SuiteCmds = []*cobra.Command{
	SuiteCmdAll,
	SuiteCmdForce,
	SuiteOneCmd,
}

// gocfg i ini
var SuiteOneCmd = &cobra.Command{
	Use:   "one [flag]",
	Short: "Make suite of one struct flag",
	Run: func(cmd *cobra.Command, args []string) {
		if len(cmd.Flags().Args()) > 0 {
			flag := cmd.Flags().Arg(0)
			fmt.Println(flag)
			difactroy.FindBeanDiFactroy().MakeSuite(flag)
		} else {
			logrus.Error(" godi suite XXXX!")
		}
	},
}
var SuiteCmdAll = &cobra.Command{
	Use:   "all ",
	Short: "Make all suites!",
	Run: func(cmd *cobra.Command, args []string) {

		difactroy.FindBeanDiFactroy().MakeSuite()

	},
}
var SuiteCmdForce = &cobra.Command{
	Use:   "force ",
	Short: "Make force one suite!",
	Run: func(cmd *cobra.Command, args []string) {

		flag := cmd.Flags().Arg(0)
		difactroy.FindBeanDiFactroy().MakeSuite(flag)
	},
}
