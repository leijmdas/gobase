package main

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/godi/cmd/godi/internal/godimenu"
)

const ThisPkgName = "gitee.com/leijmdas/gobaseconfigweb/goconfig/server/service/config"

func main() {
	//var pkgroot = gominifactroy.FindPkgRootDirOf(ThisPkgName)
	//os.Setenv("BasePath", pkgroot)

	golog.Info(fileutils.FindRootDir())
	godimenu.Execute()

}
