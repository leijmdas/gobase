package template

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type TemplateEntity struct {
	basedto.BaseEntity
}

func NewTemplateEntity() *TemplateEntity {
	var entity = &TemplateEntity{}
	entity.InitProxy(entity)
	return entity

}
