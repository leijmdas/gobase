package template

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: template_entity_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameTemplateEntity = "*template.TemplateEntity-c88620c30ce04330a99b3dd4d5f14fca"

// init register load
func init() {
	registerBeanTemplateEntity()
}

// register TemplateEntity
func registerBeanTemplateEntity() {
	basedi.RegisterLoadBean(singleNameTemplateEntity, LoadTemplateEntity)
}

func FindBeanTemplateEntity() *TemplateEntity {
	var inst, ok = basedi.FindBeanOk(singleNameTemplateEntity)
	if !ok {
		logrus.Error("not found TemplateEntity!")
		return nil
	}
	bean, ok := inst.(*TemplateEntity)
	if !ok {
		logrus.Errorf("FindBeanTemplateEntity: failed to cast bean to *TemplateEntity")
		return nil
	}
	return bean

}
func FindBeanTemplateEntityOk() (*TemplateEntity, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameTemplateEntity)
	if !ok {
		logrus.Error("not found TemplateEntity!")
		return nil, ok
	}
	bean, ok := inst.(*TemplateEntity)
	if !ok {
		logrus.Errorf("FindBeanTemplateEntity: failed to cast bean to *TemplateEntity")
		return nil, ok
	}
	return bean, ok

}

func LoadTemplateEntity() baseiface.ISingleton {
	var s = NewTemplateEntity()
	InjectTemplateEntity(s)
	return s

}

func InjectTemplateEntity(s *TemplateEntity) {

	golog.Debug("inject")
}
