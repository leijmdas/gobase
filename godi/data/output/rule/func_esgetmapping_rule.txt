
rule=func_esgetmapping
rule "func-esgetmapping-rule" "func-esgetmapping-rule" salience 0
begin
    IchubLog(In)

    id=@id
    Out.SetParam("FuncId","func-esquery-rule")
    Result = RuleFunc.EsGetMapping(In)

    Out.SetReturn(200,"计算成功")
    // Out.SetEsPageResult(Result)
    Out.SetParam("EsResult",Result)

end
param=
{
     "page_size": 20,
     "current": 1,
     "order_by": null,
     "fields": null,
     "index_name": "ichub_sys_dept"
}
result=
{
     "EsResult": {
          "ichub_sys_dept": {
               "mappings": {
                    "properties": {
                         "ancestors": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "create_by": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "create_time": {
                              "type": "date"
                         },
                         "created": {
                              "type": "date"
                         },
                         "del_flag": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "dept_id": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "dept_name": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "email": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "image": {
                              "type": "keyword"
                         },
                         "leader": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "location": {
                              "type": "geo_point"
                         },
                         "message": {
                              "type": "text"
                         },
                         "order_nm": {
                              "type": "long"
                         },
                         "parent_id": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "phone": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "status": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "suggest_field": {
                              "analyzer": "simple",
                              "max_input_length": 50,
                              "preserve_position_increments": true,
                              "preserve_separators": true,
                              "type": "completion"
                         },
                         "tags": {
                              "type": "keyword"
                         },
                         "update_by": {
                              "fields": {
                                   "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                   }
                              },
                              "type": "text"
                         },
                         "user": {
                              "type": "keyword"
                         }
                    }
               }
          }
     },
     "FuncId": "func-esquery-rule",
     "ReturnMsg": "计算成功",
     "ReturnValue": 200
}