package di

import (
	"errors"
	fmt "fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/godi/di/difactroy"
	single "gitee.com/leijmdas/gobase/godi/di/dimodel/simple"
	"gitee.com/leijmdas/gobase/godi/di/internal/codefactroy"
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"reflect"
	"runtime"
	"testing"
)

type TestDiFactoySuite struct {
	suite.Suite
	rootdir string

	DiFactroy *difactroy.DiFactroy
}

func (this *TestDiFactoySuite) SetupTest() {

	ichublog.InitLogrus()
	this.rootdir = fileutils.FindRootDir()
	this.DiFactroy = difactroy.NewDiFactroy()

}

func TestDiFactoySuites(t *testing.T) {
	suite.Run(t, new(TestDiFactoySuite))
}
func CurrentFile() string {
	_, file, _, ok := runtime.Caller(1)
	if !ok {
		panic(errors.New("Can not get current file info"))
	}
	return file
}

// 指定一个文件生成
func (this *TestDiFactoySuite) Test001_MakeDiFile() {

	this.DiFactroy.MakeDiFile("./dimodel/multi/multi_entity.go")
	logrus.Info(CurrentFile())

}

// 指定结构体structName生成注册与注入代码
func (this *TestDiFactoySuite) Test002_MakeDiStru() {

	this.DiFactroy.Rootdir = fileutils.FindRootDirGoMod()
	this.DiFactroy.MakeDiStru("RedisBaseConfig")

}

// 生成所有, 继承baseentity的结果: 已经有不再生成！
func (this *TestDiFactoySuite) Test003_MakeDiAll() {

	this.DiFactroy.MakeDiAll()

}

// 生成所有 继承baseentity的结果，强制执行
func (this *TestDiFactoySuite) Test004_MakeDiForce() {

	this.DiFactroy.MakeDiAllForce(true)

}

// 生成选项模式
func (this *TestDiFactoySuite) Test005_MakeOption() {

	this.DiFactroy.MakeOption()

}

// 生成测试套
func (this *TestDiFactoySuite) Test006_MakeSuite() {

	this.DiFactroy.MakeSuite()
}

// 生成一个结构体的注册文件
func (this *TestDiFactoySuite) Test007_MakeDiDto() {
	var dto = didto.FindBeanDiDto()
	dto.PkgName = "didto"
	dto.StructName = "DiDto"
	codefactroy.FindBeanCodeFactroy().Make(dto)
}

// 查找所有GO文件
func (this *TestDiFactoySuite) Test008_FindAllFiles() {
	this.DiFactroy.FindGoFiles()

	logrus.Info("basedi=", jsonutils.ToJsonPretty(this.DiFactroy))
}

// AST分析所有文件
func (this *TestDiFactoySuite) Test009_ParseAll() {

	this.DiFactroy.ParseAll()
	var cf, found = this.DiFactroy.FindSome("SingleEntity")
	if found {
		golog.Info(cf.ToString())
	} else {
		golog.Error("\r\n!found ", found)
	}

}

func (this *TestDiFactoySuite) Test010_findPkgDir() {
	var p, err = fileutils.GetPkgPath("gitee.com/leijmdas/gobase/godi/di")
	if err != nil {
		goutils.Error(err)
	}
	golog.Info(p)
	var t = Find[*single.SimpleEntity]()
	var _ = basedi.FindBean("single.SimpleEntity")
	logrus.Info(t)
}
func Find[T any]() T {

	var inst T
	var instType = reflect.TypeOf(inst)
	var intance = basedi.FindBean(instType.String()[1:])

	return intance.(T)
}
func (this *TestDiFactoySuite) Test011_findBean() {

	var bean = basedi.FindBean("*single.SimpleEntity")
	golog.Info(bean)
}
func Add(a, b int) int {
	return a + b
}
func (this *TestDiFactoySuite) Test012_refFunc() {

	// 获取函数的反射对象
	funcValue := reflect.ValueOf(Add)

	// 准备参数（转换为[]reflect.Value）
	args := []reflect.Value{
		reflect.ValueOf(3),
		reflect.ValueOf(4),
	}

	// 调用函数并获取结果
	results := funcValue.Call(args)

	// 处理返回值
	sum := results[0].Int()
	fmt.Println("3 + 4 =", sum) // 输出：3 + 4 = 7
}

type Calculator struct{}

func (c Calculator) Multiply(x, y int) int {
	return x * y
}
func (this *TestDiFactoySuite) Test013_refStruFunc() {
	calc := Calculator{}

	// 获取结构体的反射对象，并找到方法
	methodValue := reflect.ValueOf(calc).MethodByName("Multiply")

	// 准备参数
	args := []reflect.Value{
		reflect.ValueOf(5),
		reflect.ValueOf(6),
	}

	// 调用方法
	results := methodValue.Call(args)

	// 处理结果
	product := results[0].Int()
	fmt.Println("5 * 6 =", product) // 输出：5 * 6 = 30
}
