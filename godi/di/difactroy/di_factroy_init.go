package difactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"

	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: di_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameDiFactroy = "*difactroy.DiFactroy-e8b3b144eff54c31a63b19a0631ae7e8"

// init register load
func init() {
	registerBeanDiFactroy()
}

// register DiFactroy
func registerBeanDiFactroy() {
	basedi.RegisterLoadBean(singleNameDiFactroy, LoadDiFactroy)
}

func FindBeanDiFactroy() *DiFactroy {
	var inst, ok = basedi.FindBeanOk(singleNameDiFactroy)
	if !ok {
		logrus.Error("not found DiFactroy!")
		return nil
	}
	bean, ok := inst.(*DiFactroy)
	if !ok {
		logrus.Errorf("FindBeanDiFactroy: failed to cast bean to *DiFactroy")
		return nil
	}
	return bean

}
func FindBeanDiFactroyOk() (*DiFactroy, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameDiFactroy)
	if !ok {
		logrus.Error("not found DiFactroy!")
		return nil, ok
	}
	bean, ok := inst.(*DiFactroy)
	if !ok {
		logrus.Errorf("FindBeanDiFactroy: failed to cast bean to *DiFactroy")
		return nil, ok
	}
	return bean, ok

}

func LoadDiFactroy() baseiface.ISingleton {
	var s = NewDiFactroy()
	InjectDiFactroy(s)
	return s

}

func InjectDiFactroy(s *DiFactroy) {

	// 自动实例化
	s.Config = ichubconfig.FindBeanIchubConfig()
	goutils.Debug("inject")
}
