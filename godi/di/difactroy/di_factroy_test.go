package difactroy

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"reflect"
	"testing"
)

type TestDiFactroySuite struct {
	suite.Suite
	inst *DiFactroy

	rootdir string
}

func (this *TestDiFactroySuite) SetupTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanDiFactroy()

	this.rootdir = fileutils.FindRootDir()

}

func TestDiFactroySuites(t *testing.T) {
	suite.Run(t, new(TestDiFactroySuite))
}

func (this *TestDiFactroySuite) Test001_init() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test002_FindBasePkg() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test003_FindFile() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test004_FindGoFiles() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test005_ParseDir() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test006_MakeDiStru() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test007_MakeDiAll() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test008_ListDiAll() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test009_MakeDiStruInfos() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test010_MakeSuite() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test011_MakeOption() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test012_MakeDiAllForce() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test013_MakeDiFile() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test014_FindSome() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test015_ParseAll() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test016_Save2JSONFile() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test017_SaveFile2JSONFile() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test018_ParseFuncs() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test019_ParseImports() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test020_ParseTags() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test021_ParseStruct() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test022_ParseMethods() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test023_ExistNewMethod() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test024_findNewFunc() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test025_Parse() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test026_SetImport2StructInfos() {
	logrus.Info(1)

}

func (this *TestDiFactroySuite) Test027() {
	var pkgPath string
	pkgPath = "fmt" // 你可以根据条件动态设置这个路径

	pkg, err := importPackage1(pkgPath)
	if err != nil {
		fmt.Println("Error importing package:", err)
		return
	}

	fmt.Println("Package imported successfully:", pkg)
}

func importPackage1(path string) (interface{}, error) {
	i := reflect.ValueOf(path).Elem()
	pkg := i.Call([]reflect.Value{reflect.ValueOf(path)})

	return pkg[0].Interface().(*reflect.Value).Interface(), nil
}
