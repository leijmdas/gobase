package difactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"github.com/sirupsen/logrus"
	"go/ast"
)

type ParseFields struct {
}

func NewParseFields() *ParseFields {
	return &ParseFields{}
}

func (this *ParseFields) Parse(field *ast.Field) *didto.DiInject {

	var di = didto.NewDiInject()
	if len(field.Names) > 0 {
		this.ParseName(field, di)
	} else {
		this.ParseNoName(field, di)
	}
	return di
}

func (this *ParseFields) ParseNoName(field *ast.Field, di *didto.DiInject) {
	this.ParseSelectorExpr(field, di)

}
func (this *ParseFields) ParseSelectorExpr(field *ast.Field, di *didto.DiInject) bool {

	if expr, ok := field.Type.(*ast.SelectorExpr); ok {
		var name = expr.Sel.Name
		di.MemberType = name
		di.GoType = "struct"
		di.MemberName = name
		di.PkgName = expr.X.(*ast.Ident).Name
		return true
	}
	return false
}

// godi struct
func (this *ParseFields) ParseStarExpr(field *ast.Field, di *didto.DiInject) bool {
	if fieldType, ok := field.Type.(*ast.StarExpr); ok {

		di.GoType = "struct"
		if filedTypeX, ok := fieldType.X.(*ast.SelectorExpr); ok {
			di.PkgName = filedTypeX.X.(*ast.Ident).Name
			di.MemberType = filedTypeX.Sel.Name
			return true
		}
		if starExpr, ok := fieldType.X.(*ast.Ident); ok {
			di.MemberType = starExpr.Name
			return true
		}
	}
	return false

}

func (this *ParseFields) ParseIdent(field *ast.Field, di *didto.DiInject) bool {
	if ident, ok := field.Type.(*ast.Ident); ok {
		di.GoType = ident.Name
		di.MemberType = ident.Name
		return true
	}
	return false

}

func (this *ParseFields) ParseMapType(field *ast.Field, di *didto.DiInject) bool {
	maptype, ok := field.Type.(*ast.MapType)
	if !ok {
		return false
	}

	di.GoType = "map"
	if _, ok := maptype.Key.(*ast.Ident); !ok {
		return ok
	}
	di.MemberType = "map[" + maptype.Key.(*ast.Ident).Name + "]"

	if id, ok := maptype.Value.(*ast.Ident); ok {
		di.MemberType += id.Name
	} else {
		if star, ok := maptype.Value.(*ast.StarExpr); ok {
			if ident, ok := star.X.(*ast.Ident); ok {
				di.MemberType += ident.Name
			} else {
				goutils.Warning(ok)
				return false
			}
		} else {
			logrus.Error("godi maptype err...")
		}
	}
	return ok

}

func (this *ParseFields) ParseArrayType(field *ast.Field, di *didto.DiInject) bool {
	var arraytype, ok = field.Type.(*ast.ArrayType)
	if !ok {
		return false
	}

	di.GoType = "array"
	di.MemberType = "array"
	di.MemberName = field.Names[0].Name
	if elt, ok := arraytype.Elt.(*ast.Ident); ok {
		di.MemberType = "[]" + elt.String()
		di.MemberName = field.Names[0].Name
	} else {
		if star, ok := arraytype.Elt.(*ast.StarExpr); ok {
			if v, ok := star.X.(*ast.Ident); ok {
				di.MemberType = "[]" + v.Name
			} else {
				goutils.Warning("godi arraytype err...")
			}
		} else {
			logrus.Warning("godi arraytype err...")
		}
	}

	return ok
}

func (this *ParseFields) ParseName(field *ast.Field, di *didto.DiInject) bool {

	di.MemberName = field.Names[0].Name
	if this.ParseStarExpr(field, di) || this.ParseIdent(field, di) ||
		this.ParseMapType(field, di) || this.ParseArrayType(field, di) {
		return true
	}
	goutils.Error("godi field error", di.MemberName, field.Type)
	return false
}
