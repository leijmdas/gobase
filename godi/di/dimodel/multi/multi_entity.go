package multi

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
)

type MultiEntity struct {
	basedto.BaseEntity

	Id       int
	Name     string
	MyEn     MyEntity
	myentity *MyEntity `godi:"auto"`
}

func NewMultiEntity() *MultiEntity {
	var s = &MultiEntity{
		Name: "multi",
	}
	s.InitProxy(s)
	return s
}

func (*MultiEntity) Autoload() bool {
	return true
}
func (*MultiEntity) AutoInject() bool {
	return true
}

func (*MultiEntity) Check() {
	ichublog.InitLogrus()

	var proxy baseiface.IbaseProxy = basedto.NewIchubResult()
	var single, oksingle = proxy.(baseiface.ISingleton)

	ichublog.Log("single=", single, oksingle, single.Single())

}
