package multi

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: multi_entity_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-28 09:41:34)
	@Update 作者: leijmdas@163.com 时间(2025-01-28 09:41:34)

* *********************************************************/

const singleNameMultiEntity = "*multi.MultiEntity-364295491199495b887521fc0fbf2316"

// init register load
func init() {
	registerBeanMultiEntity()
}

// register MultiEntity
func registerBeanMultiEntity() {
	basedi.RegisterLoadBean(singleNameMultiEntity, LoadMultiEntity)
}

func FindBeanMultiEntity() *MultiEntity {
	var inst, ok = basedi.FindBeanOk(singleNameMultiEntity)
	if !ok {
		logrus.Error("not found MultiEntity!")
		return nil
	}
	bean, ok := inst.(*MultiEntity)
	if !ok {
		logrus.Errorf("FindBeanMultiEntity: failed to cast bean to *MultiEntity")
		return nil
	}
	return bean

}
func FindBeanMultiEntityOk() (*MultiEntity, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameMultiEntity)
	if !ok {
		logrus.Error("not found MultiEntity!")
		return nil, ok
	}
	bean, ok := inst.(*MultiEntity)
	if !ok {
		logrus.Errorf("FindBeanMultiEntity: failed to cast bean to *MultiEntity")
		return nil, ok
	}
	return bean, ok

}

func LoadMultiEntity() baseiface.ISingleton {
	var s = NewMultiEntity()
	InjectMultiEntity(s)
	return s

}

func InjectMultiEntity(s *MultiEntity) {

	// 自动实例化
	s.myentity = FindBeanMyEntity()
	// golog.Debug("inject")
}
