package multi

import (
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"github.com/gookit/goutil/testutil/assert"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"
)

// find is not support multi entity
func Test001_FindMulti(t *testing.T) { //var bean1 = FindBeanMultiEntity()

	var bean2 = FindBeanMultiEntity()
	var bean3 = FindBeanMultiEntity()
	bean2.Name = "1111222"
	logrus.Info(bean2, bean3)
	assert.Equal(t, bean3.Name != bean2.Name, true)

}
func Test002_makedi(t *testing.T) {

	var dto = didto.NewDiDto()

	logrus.Info(ContainsType(dto, "BaseEntity"))
}
func ContainsType(s interface{}, name string) bool {
	value := reflect.ValueOf(s)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}

	if value.Kind() != reflect.Struct {
		return false
	}
	for i := 0; i < value.NumField(); i++ {
		logrus.Info(value.Field(i).Kind(), value.Field(i).Type().Name())
		if value.Field(i).Type().Name() == name {

		}
	}
	for i := 0; i < value.NumField(); i++ {
		logrus.Info(value.Field(i).Interface())
		logrus.Info(value.Field(i).Type().Name())
		if value.Field(i).Type().Name() == name {
			return true
		}
	}

	//if struct
	return false
}
func Test003_makediContains(t *testing.T) {

	var dto = didto.NewDiDto()

	logrus.Info(dto.ContainsType("BaseEntity"))
}
