package multi

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type MyEntity struct {
	basedto.BaseEntity
	price int
}

func newMy() *MyEntity {
	return &MyEntity{}
}
