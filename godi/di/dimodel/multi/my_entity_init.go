package multi

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: my_entity_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-27 11:49:04)
	@Update 作者: leijmdas@163.com 时间(2025-01-27 11:49:04)

* *********************************************************/

const singleNameMyEntity = "*multi.MyEntity-fda5b4bd1ac64cc2a2a575a4711500b4"

// init register load
func init() {
	registerBeanMyEntity()
}

// register MyEntity
func registerBeanMyEntity() {
	basedi.RegisterLoadBean(singleNameMyEntity, LoadMyEntity)
}

func FindBeanMyEntity() *MyEntity {
	var inst, ok = basedi.FindBeanOk(singleNameMyEntity)
	if !ok {
		logrus.Error("not found MyEntity!")
		return nil
	}
	bean, ok := inst.(*MyEntity)
	if !ok {
		logrus.Errorf("FindBeanMyEntity: failed to cast bean to *MyEntity")
		return nil
	}
	return bean

}
func FindBeanMyEntityOk() (*MyEntity, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameMyEntity)
	if !ok {
		logrus.Error("not found MyEntity!")
		return nil, ok
	}
	bean, ok := inst.(*MyEntity)
	if !ok {
		logrus.Errorf("FindBeanMyEntity: failed to cast bean to *MyEntity")
		return nil, ok
	}
	return bean, ok

}

func LoadMyEntity() baseiface.ISingleton {
	var s = newMy()
	InjectMyEntity(s)
	return s

}

func InjectMyEntity(s *MyEntity) {

	golog.Debug("inject")
}
