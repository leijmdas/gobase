package multi

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"go/ast"
	"go/parser"
	"go/token"

	"github.com/stretchr/testify/suite"
	"testing"
)

type TestMyEntitySuite struct {
	suite.Suite
	inst *MyEntity

	rootdir string
}

func (this *TestMyEntitySuite) SetupTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanMyEntity()

	this.rootdir = fileutils.FindRootDir()

}

func TestMyEntitySuites(t *testing.T) {
	suite.Run(t, new(TestMyEntitySuite))
}
func TestNestStru_001(t *testing.T) {
	// 读取源文件
	fset := token.NewFileSet()
	file, err := parser.ParseFile(fset, "multi_entity.go", nil, parser.ParseComments)
	if err != nil {
		fmt.Println("Error parsing file:", err)
		return
	}

	// 遍历AST节点
	ast.Inspect(file, func(n ast.Node) bool {
		switch x := n.(type) {
		case *ast.GenDecl:
			// 查找类型定义
			for _, spec := range x.Specs {
				typeSpec, ok := spec.(*ast.TypeSpec)

				if !ok {
					break
				}

				// 查找结构体类型
				structType, ok := typeSpec.Type.(*ast.StructType)
				if !ok {
					break
				}

				fmt.Printf("Type: %s\n", typeSpec.Name.Name)
				for _, field := range structType.Fields.List {
					// 检查字段类型是否为结构体
					if structType, ok := field.Type.(*ast.StructType); ok {
						printStructFields(structType)
					}
					if se, ok := field.Type.(*ast.StarExpr); ok {
						//printStructFields(structType)
						fmt.Println("type:", se.X.(*ast.Ident))
						underlyingType := se.X
						if structType, ok := underlyingType.(*ast.StructType); ok {
							printStructFields(structType)
						}
					}
					logrus.Info(1)

				}
			}
		}
		return true
	})
}

func printStructFields(structType *ast.StructType) {
	fmt.Println("  Fields:")
	for _, field := range structType.Fields.List {
		if len(field.Names) > 0 {
			fmt.Printf("    %s %s\n", field.Names[0].Name, field.Type)
		} else {
			fmt.Printf("    %s\n", field.Type)
		}
	}
}
