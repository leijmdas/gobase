package single

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/godi/di/dimodel/multi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: simple_entity_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-27 11:47:37)
	@Update 作者: leijmdas@163.com 时间(2025-01-27 11:47:37)

* *********************************************************/

const singleNameSimpleEntity = "*single.SimpleEntity-de989ab9b2ac440ea3d15874c5a9eeee"

// init register load
func init() {
	registerBeanSimpleEntity()
}

// register SimpleEntity
func registerBeanSimpleEntity() {
	basedi.RegisterLoadBean(singleNameSimpleEntity, LoadSimpleEntity)
}

func FindBeanSimpleEntity() *SimpleEntity {
	var inst, ok = basedi.FindBeanOk(singleNameSimpleEntity)
	if !ok {
		logrus.Error("not found SimpleEntity!")
		return nil
	}
	bean, ok := inst.(*SimpleEntity)
	if !ok {
		logrus.Errorf("FindBeanSimpleEntity: failed to cast bean to *SimpleEntity")
		return nil
	}
	return bean

}
func FindBeanSimpleEntityOk() (*SimpleEntity, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameSimpleEntity)
	if !ok {
		logrus.Error("not found SimpleEntity!")
		return nil, ok
	}
	bean, ok := inst.(*SimpleEntity)
	if !ok {
		logrus.Errorf("FindBeanSimpleEntity: failed to cast bean to *SimpleEntity")
		return nil, ok
	}
	return bean, ok

}

func LoadSimpleEntity() baseiface.ISingleton {
	var s = &SimpleEntity{}
	InjectSimpleEntity(s)
	return s

}

func InjectSimpleEntity(s *SimpleEntity) {

	// 实例注册函数
	s.Bind()
	// 实例查找函数
	if find := s.FindBinding("MultiBind"); find != nil {
		golog.Debug("find binded LoadBeanMultiEntity!")
		// 类型断言执行load
		s.MultiBind = find().(*multi.MultiEntity)
	} else {
		// 未找到绑定，实例化
		s.MultiBind = &multi.MultiEntity{}
		golog.Warn("no binding LoadBeanMultiEntity!")
	}

	// 简单实例化
	s.MultiSingle = &multi.MultiEntity{}

	// 自动实例化
	s.MultiAuto = multi.FindBeanMultiEntity()

	// 实例化默认New
	s.MultiNew = multi.NewMultiEntity()

	// 实例注册函数
	s.Bind()
	// 实例查找函数
	if find := s.FindBinding("MultiBindNew"); find != nil {
		golog.Debug("find binded LoadBeanMultiEntity!")
		// 类型断言执行load
		s.MultiBindNew = find().(*multi.MultiEntity)
	} else {
		// 未找到绑定，实例化
		s.MultiBindNew = &multi.MultiEntity{}
		golog.Warn("no binding LoadBeanMultiEntity!")
	}
	golog.Debug("inject")
}
