package single

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestSimpleEntitySuite struct {
	suite.Suite
	inst *SimpleEntity

	rootdir string
}

func (this *TestSimpleEntitySuite) SetupTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanSimpleEntity()

	this.rootdir = fileutils.FindRootDir()

}

func TestSimpleEntitySuites(t *testing.T) {
	suite.Run(t, new(TestSimpleEntitySuite))
}

func (this *TestSimpleEntitySuite) Test001_Bind() {
	logrus.Info(1)

}

func (this *TestSimpleEntitySuite) Test002_Check() {
	logrus.Info(1)

}

func (this *TestSimpleEntitySuite) Test003_findBeanFactroyr() {

	var bean1, _ = basedi.FindBeanInstOk[*SimpleEntity]()
	var bean2, _ = basedi.FindBeanInstOk[*SimpleEntity]()

	logrus.Info(bean1, bean2)
}
