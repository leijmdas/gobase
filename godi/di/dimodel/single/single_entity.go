package single

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/godi/di/dimodel/multi"
	"github.com/sirupsen/logrus"
)

type SingleEntity struct {
	basedto.BaseEntitySingle //单例

	// 以下是依赖注入的示例
	// 注入的对象可以是单例、绑定、新建、无注入
	//auto  通过LoadBean()方法注入
	MultiAuto *multi.MultiEntity `godi:"auto"`
	//bind  通过BindUp()方法注入
	MultiBind *multi.MultiEntity `godi:"bind"`
	//new   通过New()方法注入
	MultiNew *multi.MultiEntity `godi:"new"`
	//none  不注入
	MultiNone *multi.MultiEntity `godi:"none"`
	//single  单例; 注入的对象为简单NEW
	MultiSingle  *multi.MultiEntity `godi:"single"`
	MultiBindNew *multi.MultiEntity `godi:"bind"`

	Id   int
	Name string
}

// 注册一个成员加载函数
func (this *SingleEntity) Bind() {

	this.BindUp("MultiBind", multi.LoadMultiEntity)
}

func NewSingleEntity() *SingleEntity {
	var s = &SingleEntity{
		Name: "single",
	}
	s.InitProxy(s)
	return s
}

func (*SingleEntity) Autoload() bool {
	return true
}
func (*SingleEntity) AutoInject() bool {
	return true
}
func (*SingleEntity) IfAutowire() bool {
	return true
}

func (*SingleEntity) Check() {
	ichublog.InitLogrus()

	var proxy baseiface.IbaseProxy = basedto.NewIchubResult()
	var single, oksingle = proxy.(baseiface.ISingleton)

	logrus.Info("single=", single, oksingle)
	logrus.Info(single.Single())
}
