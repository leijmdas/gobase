package single

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"github.com/sirupsen/logrus"
	//"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/godi/di/dimodel/multi"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: single_entity_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-28 09:41:34)
	@Update 作者: leijmdas@163.com 时间(2025-01-28 09:41:34)

* *********************************************************/

const singleNameSingleEntity = "*single.SingleEntity-9249aa385a2b467d9c8603d019fb0a53"

// init register load
func init() {
	registerBeanSingleEntity()
}

// register SingleEntity
func registerBeanSingleEntity() {
	basedi.RegisterLoadBean(singleNameSingleEntity, LoadSingleEntity)
}

func FindBeanSingleEntity() *SingleEntity {
	var inst, ok = basedi.FindBeanOk(singleNameSingleEntity)
	if !ok {
		logrus.Error("not found SingleEntity!")
		return nil
	}
	bean, ok := inst.(*SingleEntity)
	if !ok {
		logrus.Errorf("FindBeanSingleEntity: failed to cast bean to *SingleEntity")
		return nil
	}
	return bean

}
func FindBeanSingleEntityOk() (*SingleEntity, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameSingleEntity)
	if !ok {
		logrus.Error("not found SingleEntity!")
		return nil, ok
	}
	bean, ok := inst.(*SingleEntity)
	if !ok {
		logrus.Errorf("FindBeanSingleEntity: failed to cast bean to *SingleEntity")
		return nil, ok
	}
	return bean, ok

}

func LoadSingleEntity() baseiface.ISingleton {
	var s = NewSingleEntity()
	InjectSingleEntity(s)
	return s

}

func InjectSingleEntity(s *SingleEntity) {

	// 自动实例化
	s.MultiAuto = multi.FindBeanMultiEntity()

	// 简单实例化
	s.MultiSingle = &multi.MultiEntity{}

	// 实例注册函数
	s.Bind()
	// 实例查找函数
	if find := s.FindBinding("MultiBindNew"); find != nil {
		golog.Debug("find binded LoadBeanMultiEntity!")
		// 类型断言执行load
		s.MultiBindNew = find().(*multi.MultiEntity)
	} else {
		// 未找到绑定，实例化
		s.MultiBindNew = &multi.MultiEntity{}
		golog.Warn("no binding LoadBeanMultiEntity!")
	}

	// 实例注册函数
	s.Bind()
	// 实例查找函数
	if find := s.FindBinding("MultiBind"); find != nil {
		golog.Debug("find binded LoadBeanMultiEntity!")
		// 类型断言执行load
		s.MultiBind = find().(*multi.MultiEntity)
	} else {
		// 未找到绑定，实例化
		s.MultiBind = &multi.MultiEntity{}
		golog.Warn("no binding LoadBeanMultiEntity!")
	}

	// 实例化默认New
	s.MultiNew = multi.NewMultiEntity()
	// golog.Debug("inject")
}
