package single

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/godi/di/dimodel/multi"
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"reflect"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
)

// 注入的三种方式： auto/ new / bind
func Test001_FindSingleAutoInject(t *testing.T) {

	var singleEntity = FindBeanSingleEntity()
	logrus.Info(singleEntity)

}

// find single is same
func Test002_FindSingle(t *testing.T) {

	var singleEntity = FindBeanSingleEntity()

	singleEntity.Name = "aaa"

	logrus.Info(singleEntity.ToPrettyString())
	var sa = FindBeanSingleEntity()
	var sb = FindBeanSingleEntity()
	sb.Name = "bbb"
	logrus.Info(singleEntity, sa, sb)
	assert.Equal(t, sa.Name == singleEntity.Name, true)
	assert.Equal(t, sb.Name == singleEntity.Name, true)

}

func Test003_create(t *testing.T) {
	var single = NewSingleEntity()
	logrus.Info(single.ToPrettyString())
	single.Check()
	var s1 baseiface.ISingleton = single
	s1.(*SingleEntity).Id = 111
	logrus.Info("is=:", s1, s1.Single())
	logrus.Info("s=", single, single.Single())

}

// find single is same
func Test005_FindSingle(t *testing.T) {

	var bean1 = FindBeanSingleEntity()
	bean1.Name = "111"
	logrus.Info("proxy1=", bean1, bean1.Single())

	var bean2 = FindBeanSingleEntity() //basedi.FindBean(singleNameSingleEntity)
	var bean3 = FindBeanSingleEntity() //basedi.FindBean(singleNameSingleEntity)
	bean2.Name = "1111222"
	logrus.Info("proxy1=", bean1, bean1.Single())
	logrus.Info("proxy2=", bean2, bean2.Single(), bean3)

	var tp = reflect.TypeOf(bean3)
	logrus.Info("pp=", tp.PkgPath())

}

func Test006_makedi(t *testing.T) {

	var dto = didto.FindBeanDiDto() //NewDiDto()
	dto.PkgName = "single"
	dto.StructName = "SingleEntity"
	dto.ForceBuild = true
	//DiFactroy.MakeDi(dto)
}

func Test007_SingelCase(t *testing.T) {
	var s1 = FindBeanSingleEntity()
	var s2 = FindBeanSingleEntity()
	golog.Info(s1 == s2)

}
func Test008_NotSingelCase(t *testing.T) {
	var s1 = multi.FindBeanMultiEntity()
	var s2 = multi.FindBeanMultiEntity()
	golog.Info(s1 == s2)

}
func Test009_AutoCase(t *testing.T) {
	var s1 = basedi.FindBeanGeneral[*SingleEntity]()
	var s2 = basedi.FindBeanGeneral[*SingleEntity]()
	golog.Info(s1 == s2)

}
