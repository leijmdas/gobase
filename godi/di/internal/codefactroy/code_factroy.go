package codefactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"github.com/duke-git/lancet/fileutil"
	"github.com/sirupsen/logrus"
	"path/filepath"
	"strings"
	"time"
)

/*
@Title    文件名称: {{.FileName}}.go
@Description  描述: 代码工厂服务

@Author  作者: {{.Author}}  时间({{.Now}})
@Update  作者: {{.Author}}  时间({{.Now}})
*/
const (
	VAR_KEY_FileName = "FileName"
	VAR_KEY_Author   = "Author"
	VAR_KEY_Corp     = "Corp"
	VAR_KEY_Now      = "Now"
)
const tEMPLATE_CODE_FILE = "config/di/template/struct_init.template"

func MakeDi(dto *didto.DiDto) error {
	return FindBeanCodeFactroy().Make(dto)

}

type CodeFactroy struct {
	basedto.BaseEntity
	config *ichubconfig.IchubConfig `json:"-"`

	*didto.DiDto
	ExistNewStruct bool
	Params         map[string]string

	Templatefile string
	Rootdir      string
}

func NewCodeFactroy() *CodeFactroy {
	var cf = &CodeFactroy{
		Rootdir:        fileutils.FindRootDir(),
		Params:         make(map[string]string),
		DiDto:          didto.NewDiDto(),
		ExistNewStruct: true,
		config:         ichubconfig.NewIchubConfig(),
		Templatefile:   tEMPLATE_CODE_FILE,
	}
	cf.OutPath = "./"
	cf.InitProxy(cf)
	return cf
}

func (cf *CodeFactroy) Path() string {
	return cf.OutPath
}

func (cf *CodeFactroy) SetPath(outpath string) {
	cf.OutPath = outpath
}

func (cf *CodeFactroy) MakeToParam() {
	if len(cf.NewFuncName) == 0 {
		cf.NewFuncName = "New" + cf.StructName
	}
	cf.Params["SinglePkgStruct"] = "*" + cf.PkgName + "." + cf.StructName + "-" + baseutils.Uuid()
	cf.Params["SinglePKG"] = cf.PkgName
	cf.Params["SingleStruct"] = cf.StructName
	cf.Params["NewSingle"] = "&" + cf.StructName + "{}"
	if cf.ExistNewFunc {
		cf.Params["NewSingle"] = cf.NewFuncName + "()"
	}
	cf.Params["ToInjectCode"] = cf.ToInjectCode()
	cf.Params["ImportLibs"] = cf.ToInjectImportLib()

	cf.Params[VAR_KEY_FileName] = stringutils.Camel2Case(cf.StructName) + "_init"

	/*
	  software: kunlong@sz.com 	/ author: leijmdas@163.com
	  corp: huawei.akunlong.top
	*/
	cf.config.Read()
	if cf.config.ReadVar("software.author") == nil {
		cf.Params[VAR_KEY_Author] = "LEIJMDAS@163.COM"
		cf.Params[VAR_KEY_Corp] = "kunlong@sz.com"
	} else {
		cf.Params[VAR_KEY_Author] = cf.config.ReadVar("software.author").(string)
		cf.Params[VAR_KEY_Corp] = cf.config.ReadVar("software.corp").(string)
	}
	cf.Params[VAR_KEY_Now] = time.Now().Format(baseconsts.FormatDateTime)

}

func (cf *CodeFactroy) DecideTemlateFile() string {

	var file = cf.Rootdir + "/" + cf.Templatefile
	if fileutils.TryFileExist(file) {
		return file
	}
	file = fileutils.FindRootPkg() + "/" + cf.Templatefile
	if !fileutils.TryFileExist(file) {
		golog.Error("fiel not exists!", file)
		panic("fiel not exists!" + file)
	}
	return file

}
func (cf *CodeFactroy) ReadTemplate() (string, error) {

	return fileutil.ReadFileToString(cf.DecideTemlateFile())

}

func (cf *CodeFactroy) ParseParams() (string, error) {

	var content, err = cf.ReadTemplate()
	if err != nil {
		logrus.Error(err)
		return "", err
	}
	for k, v := range cf.Params {
		content = strings.ReplaceAll(content, "{{."+k+"}}", v)
	}
	return content, err

}

func (cf *CodeFactroy) WriteFile(filename string) error {

	var params, _ = cf.ParseParams()
	path, _ := filepath.Abs(cf.OutPath)
	cf.OutFile = path + "/" + filename
	if !cf.ForceBuild && fileutils.CheckFileExist(cf.OutFile) {
		return nil
	}
	cf.ForceBuild = true
	//golog.Info(cf.ToPrettyString())
	return fileutil.WriteBytesToFile(cf.OutFile, []byte(params))

}

func (cf *CodeFactroy) StructFileName() string {
	return stringutils.Camel2Case(cf.StructName) + "_init.go"
}

func (cf *CodeFactroy) WriteStructFile() error {

	return cf.WriteFile(cf.StructFileName())

}

func (cf *CodeFactroy) Make(dto *didto.DiDto) error {
	cf.DiDto = dto
	cf.MakeToParam()
	return cf.WriteStructFile()
}
func (cf *CodeFactroy) MakeDiOne(structInfo *didto.StructInfo) bool {

	if !structInfo.IsBaseEntity {
		return false
	}
	var dto = structInfo.DiDto
	dto.OutPath = filepath.Dir(structInfo.PathFile)
	cf.Make(dto)
	return dto.ForceBuild
}
