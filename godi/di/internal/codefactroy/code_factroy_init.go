package codefactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: code_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2025-01-27 11:40:47)
	@Update 作者: leijmdas@163.com 时间(2025-01-27 11:40:47)

* *********************************************************/

const singleNameCodeFactroy = "*codefactroy.CodeFactroy-18a40a9ce2224b26b50c5eef357d5661"

// init register load
func init() {
	registerBeanCodeFactroy()
}

// register CodeFactroy
func registerBeanCodeFactroy() {
	basedi.RegisterLoadBean(singleNameCodeFactroy, LoadCodeFactroy)
}

func FindBeanCodeFactroy() *CodeFactroy {
	var inst, ok = basedi.FindBeanOk(singleNameCodeFactroy)
	if !ok {
		logrus.Error("not found CodeFactroy!")
		return nil
	}
	bean, ok := inst.(*CodeFactroy)
	if !ok {
		logrus.Errorf("FindBeanCodeFactroy: failed to cast bean to *CodeFactroy")
		return nil
	}
	return bean

}
func FindBeanCodeFactroyOk() (*CodeFactroy, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameCodeFactroy)
	if !ok {
		logrus.Error("not found CodeFactroy!")
		return nil, ok
	}
	bean, ok := inst.(*CodeFactroy)
	if !ok {
		logrus.Errorf("FindBeanCodeFactroy: failed to cast bean to *CodeFactroy")
		return nil, ok
	}
	return bean, ok

}

func LoadCodeFactroy() baseiface.ISingleton {
	var s = NewCodeFactroy()
	InjectCodeFactroy(s)
	return s

}

func InjectCodeFactroy(s *CodeFactroy) {

	golog.Debug("inject")
}
