package codefactroy

type CodeFactroyOption func(entity *CodeFactroy)

func (self *CodeFactroy) CodeFactroyOption(opts ...CodeFactroyOption) {
	for _, opt := range opts {
		opt(self)
	}
}

//{{.optionFuncs}}

/*
func (self *CodeFactroy) WithId(v int) CodeFactroyOption {
	return func(u *CodeFactroy) {
		u.Id = v
	}
}

func (self * WithName(name string) CodeFactroyOption {
	return func(u *CodeFactroy) {
		u.Name = name
	}
}*/
