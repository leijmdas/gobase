package codefactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"reflect"
	"testing"
)

type TestCodeFactroySuite struct {
	suite.Suite
	rootdir string
}

func (this *TestCodeFactroySuite) SetupTest() {

	ichublog.InitLogrus()
	this.rootdir = fileutils.FindRootDir()

}

func TestCodeFactroySuites(t *testing.T) {
	suite.Run(t, new(TestCodeFactroySuite))
}

// 指定一个文件生成
func (this *TestCodeFactroySuite) Test001_func() {
	logrus.Info(1)

}

func TestCodeFactroy_DecideTemlateFile(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			if got := cf.DecideTemlateFile(); got != tt.want {
				t.Errorf("DecideTemlateFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCodeFactroy_Make(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	type args struct {
		dto *didto.DiDto
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			if err := cf.Make(tt.args.dto); (err != nil) != tt.wantErr {
				t.Errorf("Make() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCodeFactroy_MakeToParam(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			cf.MakeToParam()
		})
	}
}

func TestCodeFactroy_ParseParams(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			got, err := cf.ParseParams()
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseParams() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ParseParams() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCodeFactroy_Path(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			if got := cf.Path(); got != tt.want {
				t.Errorf("Path() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCodeFactroy_ReadTemplate(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			got, err := cf.ReadTemplate()
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadTemplate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ReadTemplate() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCodeFactroy_SetPath(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	type args struct {
		outpath string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			cf.SetPath(tt.args.outpath)
		})
	}
}

func TestCodeFactroy_StructFileName(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			if got := cf.StructFileName(); got != tt.want {
				t.Errorf("StructFileName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCodeFactroy_WriteFile(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	type args struct {
		filename string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			if err := cf.WriteFile(tt.args.filename); (err != nil) != tt.wantErr {
				t.Errorf("WriteFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestCodeFactroy_WriteStructFile(t *testing.T) {
	type fields struct {
		BaseEntity     basedto.BaseEntity
		config         *ichubconfig.IchubConfig
		DiDto          *didto.DiDto
		ExistNewStruct bool
		params         map[string]string
		templatefile   string
		rootdir        string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cf := &CodeFactroy{
				BaseEntity:     tt.fields.BaseEntity,
				config:         tt.fields.config,
				DiDto:          tt.fields.DiDto,
				ExistNewStruct: tt.fields.ExistNewStruct,
				Params:         tt.fields.params,
				Templatefile:   tt.fields.templatefile,
				Rootdir:        tt.fields.rootdir,
			}
			if err := cf.WriteStructFile(); (err != nil) != tt.wantErr {
				t.Errorf("WriteStructFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMakeDi(t *testing.T) {
	type args struct {
		dto *didto.DiDto
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := MakeDi(tt.args.dto); (err != nil) != tt.wantErr {
				t.Errorf("MakeDi() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewCodeFactroy(t *testing.T) {
	tests := []struct {
		name string
		want *CodeFactroy
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewCodeFactroy(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCodeFactroy() = %v, want %v", got, tt.want)
			}
		})
	}
}
