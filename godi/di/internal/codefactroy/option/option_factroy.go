package option

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/godi/di/internal/codefactroy"
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"path/filepath"
)

const templateOptionFile = "config/di/template/struct_option.template"
const templateOptionWithFile = "config/di/template/struct_option_with.template"

type OptionFactroy struct {
	basedto.BaseEntity
	*codefactroy.CodeFactroy
}

func NewOptionFactroy() *OptionFactroy {
	var factroy = &OptionFactroy{
		CodeFactroy: codefactroy.NewCodeFactroy(),
	}
	factroy.Templatefile = templateOptionFile
	return factroy
}

func (this *OptionFactroy) WithRootDir(rootdir string) *OptionFactroy {
	this.Rootdir = rootdir
	return this
}
func (this *OptionFactroy) MakeDi() error {
	var cf = this.CodeFactroy

	cf.MakeToParam()
	return this.WriteStructFile()

}
func (cf *OptionFactroy) StructFileName() string {
	return stringutils.Camel2Case(cf.StructName) + "_option.go"
}

func (self *OptionFactroy) WriteStructFile() error {

	return self.WriteFile(self.StructFileName())

}
func (self *OptionFactroy) MakeToParam() {
	self.CodeFactroy.MakeToParam()

}

func (self *OptionFactroy) Make(dto *didto.DiDto) error {
	self.DiDto = dto
	self.MakeToParam()
	return self.WriteStructFile()
}

func (this *OptionFactroy) MakeOptionOne(structInfo *didto.StructInfo) bool {

	if !structInfo.IsBaseEntity {
		return false
	}
	var dto = structInfo.DiDto
	dto.OutPath = filepath.Dir(structInfo.PathFile)
	this.Make(dto)
	return dto.ForceBuild
}
