package option

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: option_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameOptionFactroy = "*option.OptionFactroy-0eb99312f06444ea9300bcf58bcbdae3"

// init register load
func init() {
	registerBeanOptionFactroy()
}

// register OptionFactroy
func registerBeanOptionFactroy() {
	basedi.RegisterLoadBean(singleNameOptionFactroy, LoadOptionFactroy)
}

func FindBeanOptionFactroy() *OptionFactroy {
	var inst, ok = basedi.FindBeanOk(singleNameOptionFactroy)
	if !ok {
		logrus.Error("not found OptionFactroy!")
		return nil
	}
	bean, ok := inst.(*OptionFactroy)
	if !ok {
		logrus.Errorf("FindBeanOptionFactroy: failed to cast bean to *OptionFactroy")
		return nil
	}
	return bean

}
func FindBeanOptionFactroyOk() (*OptionFactroy, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameOptionFactroy)
	if !ok {
		logrus.Error("not found OptionFactroy!")
		return nil, ok
	}
	bean, ok := inst.(*OptionFactroy)
	if !ok {
		logrus.Errorf("FindBeanOptionFactroy: failed to cast bean to *OptionFactroy")
		return nil, ok
	}
	return bean, ok

}

func LoadOptionFactroy() baseiface.ISingleton {
	var s = NewOptionFactroy()
	InjectOptionFactroy(s)
	return s

}

func InjectOptionFactroy(s *OptionFactroy) {

	goutils.Debug("inject")
}
