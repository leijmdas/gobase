package suite

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/godi/di/internal/codefactroy"
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
	"path/filepath"
)

const templateSuiteFile = "config/di/template/suite_test.template"

type SuiteFactroy struct {
	basedto.BaseEntity
	*codefactroy.CodeFactroy
}

func NewSuiteFactroy() *SuiteFactroy {
	var factroy = &SuiteFactroy{
		CodeFactroy: codefactroy.NewCodeFactroy(),
	}
	factroy.Templatefile = templateSuiteFile
	return factroy

}

func (cf *SuiteFactroy) StructFileName() string {
	return stringutils.Camel2Case(cf.StructName) + "_test.go"
}

func (self *SuiteFactroy) WriteStructFile() error {

	return self.WriteFile(self.StructFileName())

}
func (self *SuiteFactroy) MakeToParam() {
	self.CodeFactroy.MakeToParam()

}

func (self *SuiteFactroy) encodeFuncs(dto *didto.DiDto) string {
	var f = FindBeanSuiteMethod()
	f.clear()
	var i = 1
	for _, v := range dto.FuncDefines {
		f.BuildMethod(i, v.Name, self.StructName)
		i++
	}
	return f.Encode()
}
func (self *SuiteFactroy) Make(dto *didto.DiDto) error {
	self.DiDto = dto
	self.CodeFactroy.MakeToParam()

	self.Params["TestFuncs"] = self.encodeFuncs(dto)

	return self.WriteStructFile()
}

func (this *SuiteFactroy) MakeSuiteOne(structInfo *didto.StructInfo) bool {

	if !structInfo.IsBaseEntity {
		return false
	}
	var dto = structInfo.DiDto
	dto.OutPath = filepath.Dir(structInfo.PathFile)
	this.Make(dto)
	return dto.ForceBuild
}
