package suite

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: suite_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameSuiteFactroy = "*suite.SuiteFactroy-dee683dce7d3486bb84895b24dddb03b"

// init register load
func init() {
	registerBeanSuiteFactroy()
}

// register SuiteFactroy
func registerBeanSuiteFactroy() {
	basedi.RegisterLoadBean(singleNameSuiteFactroy, LoadSuiteFactroy)
}

func FindBeanSuiteFactroy() *SuiteFactroy {
	var inst, ok = basedi.FindBeanOk(singleNameSuiteFactroy)
	if !ok {
		logrus.Error("not found SuiteFactroy!")
		return nil
	}
	bean, ok := inst.(*SuiteFactroy)
	if !ok {
		logrus.Errorf("FindBeanSuiteFactroy: failed to cast bean to *SuiteFactroy")
		return nil
	}
	return bean

}
func FindBeanSuiteFactroyOk() (*SuiteFactroy, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameSuiteFactroy)
	if !ok {
		logrus.Error("not found SuiteFactroy!")
		return nil, ok
	}
	bean, ok := inst.(*SuiteFactroy)
	if !ok {
		logrus.Errorf("FindBeanSuiteFactroy: failed to cast bean to *SuiteFactroy")
		return nil, ok
	}
	return bean, ok

}

func LoadSuiteFactroy() baseiface.ISingleton {
	var s = NewSuiteFactroy()
	InjectSuiteFactroy(s)
	return s

}

func InjectSuiteFactroy(s *SuiteFactroy) {

	goutils.Debug("inject")
}
