package suite

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestSuiteFactroySuite struct {
	suite.Suite
	inst *SuiteFactroy

	rootdir string
}

func (this *TestSuiteFactroySuite) SetupTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanSuiteFactroy()

	this.rootdir = fileutils.FindRootDir()

}

func TestSuiteFactroySuites(t *testing.T) {
	suite.Run(t, new(TestSuiteFactroySuite))
}

func (this *TestSuiteFactroySuite) Test001_StructFileName() {
	logrus.Info(1)

}

func (this *TestSuiteFactroySuite) Test002_WriteStructFile() {
	logrus.Info(1)

}

func (this *TestSuiteFactroySuite) Test003_MakeToParam() {
	logrus.Info(1)

}

func (this *TestSuiteFactroySuite) Test004_encodeFuncs() {
	logrus.Info(1)

}

func (this *TestSuiteFactroySuite) Test005_Make() {
	logrus.Info(1)

}

func (this *TestSuiteFactroySuite) Test006_MakeSuiteOne() {
	logrus.Info(1)

}
