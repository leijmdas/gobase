package suite

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/v2/container/gmap"
	"sort"

	"strings"
)

const FuncDefine = `
func (this *Test{{.SingleStruct}}Suite) {{.TestFuncName}}() {
    logrus.Info(1)

}
`

type SuiteMethod struct {
	basedto.BaseEntity
	Methods *gmap.StrStrMap
}

func NewSuiteMethod() *SuiteMethod {
	return &SuiteMethod{
		Methods: gmap.NewStrStrMap(true),
	}
}
func (this *SuiteMethod) clear() {

}

func (this *SuiteMethod) BuildMethod(i int, m string, stru string) {
	var fn = fmt.Sprintf("Test%03d_%s", i, m)
	var ss = strings.ReplaceAll(FuncDefine, "{{.TestFuncName}}", fn)
	ss = strings.ReplaceAll(ss, "{{.SingleStruct}}", stru)
	this.Methods.Set(m, ss)

}

func (this *SuiteMethod) Encode() string {

	var values = this.Methods.Values()
	sort.Strings(values)
	return strings.Join(values, "\n")
}
