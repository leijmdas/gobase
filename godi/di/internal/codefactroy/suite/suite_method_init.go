package suite

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: suite_method_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameSuiteMethod = "*suite.SuiteMethod-98040716087f41de95b882b9e14cfed7"

// init register load
func init() {
	registerBeanSuiteMethod()
}

// register SuiteMethod
func registerBeanSuiteMethod() {
	basedi.RegisterLoadBean(singleNameSuiteMethod, LoadSuiteMethod)
}

func FindBeanSuiteMethod() *SuiteMethod {
	var inst, ok = basedi.FindBeanOk(singleNameSuiteMethod)
	if !ok {
		logrus.Error("not found SuiteMethod!")
		return nil
	}
	bean, ok := inst.(*SuiteMethod)
	if !ok {
		logrus.Errorf("FindBeanSuiteMethod: failed to cast bean to *SuiteMethod")
		return nil
	}
	return bean

}
func FindBeanSuiteMethodOk() (*SuiteMethod, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameSuiteMethod)
	if !ok {
		logrus.Error("not found SuiteMethod!")
		return nil, ok
	}
	bean, ok := inst.(*SuiteMethod)
	if !ok {
		logrus.Errorf("FindBeanSuiteMethod: failed to cast bean to *SuiteMethod")
		return nil, ok
	}
	return bean, ok

}

func LoadSuiteMethod() baseiface.ISingleton {
	var s = NewSuiteMethod()
	InjectSuiteMethod(s)
	return s

}

func InjectSuiteMethod(s *SuiteMethod) {

	goutils.Debug("inject")
}
