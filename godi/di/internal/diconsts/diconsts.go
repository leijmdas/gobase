package diconsts

const (
	BaseEntity       = "BaseEntity"
	BaseEntitySingle = "BaseEntitySingle"
	BaseConfig       = "BaseConfig"
	IchubClientDto   = "IchubClientDto"
)
const (
	INJECT_ENTITY_NONE = iota
	INJECT_ENTITY_SINGLE
	INJECT_ENTITY_AUTO
	INJECT_ENTITY_NEW
	INJECT_ENTITY_BIND
)
