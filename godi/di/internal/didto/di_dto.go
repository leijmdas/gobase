package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"strings"
)

type DiDto struct {
	basedto.BaseEntity
	//包名
	FullPkg string `json:"fullPkg"`
	//NEW exist
	ExistNewFunc bool   `json:"ExistNewFunc"`
	NewFuncName  string `json:"NewFuncName"`

	//in
	PkgName    string `json:"pkg_name"`
	StructName string `json:"struct_name"`
	PathFile   string `json:"pathFile"`
	//output
	OutPath string `json:"out_path"`
	OutFile string `json:"out_file"`

	FuncDefines  []*FuncDefine
	ImportedLibs []string             `json:"importedLibs,omitempty"`
	DiInjects    map[string]*DiInject `json:"diInjects,omitempty"`
	//可重新生成！
	ForceBuild bool `json:"force_build"`
}

func NewDiDto() *DiDto {
	var dto = &DiDto{
		DiInjects:    make(map[string]*DiInject, 0),
		ImportedLibs: make([]string, 0),
		OutPath:      "./",
		ExistNewFunc: true,
		FuncDefines:  make([]*FuncDefine, 0),
	}
	dto.InitProxy(dto)
	return dto
}
func (this *DiDto) FindImportLib(inject *DiInject) {
	for _, lib := range this.ImportedLibs {
		if strings.HasSuffix(lib, inject.PkgName) {
			inject.ImportLib = lib
		}
	}
}

func (this *DiDto) ToInjectCode() string {
	var codes = make([]string, 0)

	for _, diInject := range this.DiInjects {
		var code = diInject.ToInjectCode()
		if code != "" {
			this.FindImportLib(diInject)
			codes = append(codes, code)
		}
	}

	return strings.Join(codes, "\r\n")

}
func (this *DiDto) ToInjectImportLib() string {
	var codes = make(map[string]string)

	for _, diInject := range this.DiInjects {
		if diInject.FilterInject() {
			continue
		}
		var code = diInject.ImportLib
		if code != "" {
			codes[code] = `"` + code + `"`
		}
	}
	var codearray = make([]string, 0)
	for _, v := range codes {
		codearray = append(codearray, v)
	}
	return strings.Join(codearray, "\r\n")

}
