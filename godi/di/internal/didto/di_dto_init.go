package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: di_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameDiDto = "*didto.DiDto-eb5570a6bfed4071ac24e3e3969dcca8"

// init register load
func init() {
	registerBeanDiDto()
}

// register DiDto
func registerBeanDiDto() {
	basedi.RegisterLoadBean(singleNameDiDto, LoadDiDto)
}

func FindBeanDiDto() *DiDto {
	var inst, ok = basedi.FindBeanOk(singleNameDiDto)
	if !ok {
		logrus.Error("not found DiDto!")
		return nil
	}
	bean, ok := inst.(*DiDto)
	if !ok {
		logrus.Errorf("FindBeanDiDto: failed to cast bean to *DiDto")
		return nil
	}
	return bean

}
func FindBeanDiDtoOk() (*DiDto, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameDiDto)
	if !ok {
		logrus.Error("not found DiDto!")
		return nil, ok
	}
	bean, ok := inst.(*DiDto)
	if !ok {
		logrus.Errorf("FindBeanDiDto: failed to cast bean to *DiDto")
		return nil, ok
	}
	return bean, ok

}

func LoadDiDto() baseiface.ISingleton {
	var s = NewDiDto()
	InjectDiDto(s)
	return s

}

func InjectDiDto(s *DiDto) {

	goutils.Debug("inject")
}
