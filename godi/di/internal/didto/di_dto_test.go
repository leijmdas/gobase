package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/godi/di/internal/codefactroy"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestDiDtoSuite struct {
	suite.Suite
}

func TestDiDtoSuites(t *testing.T) {
	suite.Run(t, new(TestDiDtoSuite))
}

func (this *TestDiDtoSuite) SetupTest() {

	ichublog.InitLogrus()
}

func (this *TestDiDtoSuite) Test001_MakeDi() {
	var dto = NewDiDto()
	dto.PkgName = "didto"
	dto.StructName = "DiDto"
	var err = codefactroy.MakeDi(dto)
	if err != nil {
		ichublog.Error(err)
	}

}
func (this *TestDiDtoSuite) Test002_FindDi() {
	var dto = FindBeanDiDto()
	dto.PkgName = "didto"
	dto.StructName = "DiDto"
	logrus.Error(codefactroy.MakeDi(dto))

}
