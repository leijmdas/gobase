package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: di_inject_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameDiInject = "*didto.DiInject-fc806624a34047dfbcd1f6d5ba0e7633"

// init register load
func init() {
	registerBeanDiInject()
}

// register DiInject
func registerBeanDiInject() {
	basedi.RegisterLoadBean(singleNameDiInject, LoadDiInject)
}

func FindBeanDiInject() *DiInject {
	var inst, ok = basedi.FindBeanOk(singleNameDiInject)
	if !ok {
		logrus.Error("not found DiInject!")
		return nil
	}
	bean, ok := inst.(*DiInject)
	if !ok {
		logrus.Errorf("FindBeanDiInject: failed to cast bean to *DiInject")
		return nil
	}
	return bean

}
func FindBeanDiInjectOk() (*DiInject, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameDiInject)
	if !ok {
		logrus.Error("not found DiInject!")
		return nil, ok
	}
	bean, ok := inst.(*DiInject)
	if !ok {
		logrus.Errorf("FindBeanDiInject: failed to cast bean to *DiInject")
		return nil, ok
	}
	return bean, ok

}

func LoadDiInject() baseiface.ISingleton {
	var s = NewDiInject()
	InjectDiInject(s)
	return s

}

func InjectDiInject(s *DiInject) {

	//	goutils.Debug("inject")
}
