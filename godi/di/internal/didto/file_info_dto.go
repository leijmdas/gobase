package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"go/ast"
)

func NewFuncDefine(name string) *FuncDefine {
	return &FuncDefine{
		Name:    name,
		Params:  make([]*ast.Field, 0),
		Results: make([]*ast.Field, 0),
	}
}

type FileInfoDto struct {
	basedto.BaseEntity

	PathFile    string `json:"path_file,omitempty"`
	FileContent string `json:"content,omitempty"`

	StructInfoMap map[string]*StructInfo `json:"struct_info_map,omitempty"`
	StructInfos   []*StructInfo
	FuncDefines   []*FuncDefine `json:"func_defines"`
	ImportedLibs  []string      `json:"importedLibs,omitempty"`

	Node *ast.File `json:"-"`
}

func NewFileInfoDto() *FileInfoDto {
	var fi = &FileInfoDto{
		StructInfoMap: make(map[string]*StructInfo),
		FuncDefines:   make([]*FuncDefine, 0),
		StructInfos:   make([]*StructInfo, 0),
		ImportedLibs:  make([]string, 0),
	}
	fi.InitProxy(fi)
	return fi
}
func (this *FileInfoDto) ParseNewFunc(Stru *StructInfo) {

	Stru.NewFuncName = ""
	for _, funcDefine := range this.FuncDefines {
		if funcDefine.ParamsCount > 0 {
			continue
		}
		if funcDefine.Results == nil || len(funcDefine.Results) != 1 {
			continue
		}

		funcType := funcDefine.Results[0].Type
		var se, ok = funcType.(*ast.StarExpr)
		if ok {

			if id, ok := se.X.(*ast.Ident); ok {

				if id.Name == Stru.StructName {
					Stru.NewFuncName = funcDefine.Name
					break
				}
			} else {
				if id, ok := se.X.(*ast.SelectorExpr); ok {
					Stru.NewFuncName = id.Sel.Name
				} else {
					goutils.Error("FileInfoDto.ParseNewFunc.id.Name")
				}
			}
		}
	}

	Stru.ExistNewFunc = Stru.NewFuncName != ""
	if Stru.NewFuncName == "" {
		Stru.NewFuncName = Stru.NewFuncNameDefault()
	}
}
