package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: file_info_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameFileInfoDto = "*didto.FileInfoDto-d9774d1dd1f547d6a38651fa3f9e565b"

// init register load
func init() {
	registerBeanFileInfoDto()
}

// register FileInfoDto
func registerBeanFileInfoDto() {
	basedi.RegisterLoadBean(singleNameFileInfoDto, LoadFileInfoDto)
}

func FindBeanFileInfoDto() *FileInfoDto {
	var inst, ok = basedi.FindBeanOk(singleNameFileInfoDto)
	if !ok {
		logrus.Error("not found FileInfoDto!")
		return nil
	}
	bean, ok := inst.(*FileInfoDto)
	if !ok {
		golog.Error("FindBeanFileInfoDto: failed to cast bean to *FileInfoDto")
		return nil
	}
	return bean

}
func FindBeanFileInfoDtoOk() (*FileInfoDto, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameFileInfoDto)
	if !ok {
		logrus.Error("not found FileInfoDto!")
		return nil, ok
	}
	bean, ok := inst.(*FileInfoDto)
	if !ok {
		logrus.Errorf("FindBeanFileInfoDto: failed to cast bean to *FileInfoDto")
		return nil, ok
	}
	return bean, ok

}

func LoadFileInfoDto() baseiface.ISingleton {
	var s = NewFileInfoDto()
	InjectFileInfoDto(s)
	return s

}

func InjectFileInfoDto(s *FileInfoDto) {

	goutils.Debug("inject")
}
