package didto

import "go/ast"

type FuncDefine struct {
	StruName      string
	Name          string
	Params        []*ast.Field `json:"-"`
	Results       []*ast.Field `json:"-"`
	ParamsCount   int
	ResultType    string
	ParamNameType string
}
