package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/godi/di/internal/diconsts"
	"gitee.com/leijmdas/gobase/godi/di/internal/diutils"
	"strings"
)

type StructInfo struct {
	basedto.BaseEntity

	IsBaseEntity bool `json:"isBaseEntity"`
	*DiDto

	MethodNames []string `json:"methodNames,omitempty"`
	Fields      []string `json:"fields"`
}

func NewStructInfo() *StructInfo {
	var si = &StructInfo{
		DiDto:       NewDiDto(),
		MethodNames: make([]string, 0),
	}
	si.InitProxy(si)
	return si
}

func (this *StructInfo) NewFuncNameDefault() string {
	return "New" + this.StructName
}

func (this *StructInfo) CheckBaseEntity() bool {

	for _, v := range this.Fields {
		this.IsBaseEntity = v == diconsts.BaseEntity || v == diconsts.BaseEntitySingle || v == "Gometa"
		if this.IsBaseEntity {
			return true
		}

		if v == diconsts.BaseConfig || v == diconsts.IchubClientDto {
			this.IsBaseEntity = true
			return true
		}
	}
	this.IsBaseEntity = diutils.FindKey(this.StructName)
	return this.IsBaseEntity
}
func (this *StructInfo) IfBaseEntity() bool {

	for _, v := range this.Fields {
		this.IsBaseEntity = v == diconsts.BaseEntity || v == diconsts.BaseEntitySingle

		if this.IsBaseEntity {
			return true
		}
		if v == diconsts.BaseConfig || v == diconsts.IchubClientDto {
			this.IsBaseEntity = true
			return true
		}
	}
	this.IsBaseEntity = diutils.FindKey(this.StructName)
	return this.IsBaseEntity
}

func (s *StructInfo) ParsePkgName(rootdir, basepkg string) {

	s.FullPkg = strings.Replace(s.PathFile, rootdir, basepkg, -1)
	s.FullPkg = strings.Replace(s.FullPkg, "\\", "/", -1)
	var paths = strings.Split(s.FullPkg, "/")
	s.FullPkg = strings.Join(paths[0:len(paths)-2], "/") + "/" + s.PkgName

}

func (s *StructInfo) AddMethodName(methodName string) {
	s.MethodNames = append(s.MethodNames, methodName)
}

func (s *StructInfo) AddField(fieldName string) {
	s.Fields = append(s.Fields, fieldName)
}
