package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: struct_info_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameStructInfo = "*didto.StructInfo-f61ddfaa5c434a4ab4a1498bd072f5a0"

// init register load
func init() {
	registerBeanStructInfo()
}

// register StructInfo
func registerBeanStructInfo() {
	basedi.RegisterLoadBean(singleNameStructInfo, LoadStructInfo)
}

func FindBeanStructInfo() *StructInfo {
	var inst, ok = basedi.FindBeanOk(singleNameStructInfo)
	if !ok {
		logrus.Error("not found StructInfo!")
		return nil
	}
	bean, ok := inst.(*StructInfo)
	if !ok {
		logrus.Errorf("FindBeanStructInfo: failed to cast bean to *StructInfo")
		return nil
	}
	return bean

}
func FindBeanStructInfoOk() (*StructInfo, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameStructInfo)
	if !ok {
		logrus.Error("not found StructInfo!")
		return nil, ok
	}
	bean, ok := inst.(*StructInfo)
	if !ok {
		logrus.Errorf("FindBeanStructInfo: failed to cast bean to *StructInfo")
		return nil, ok
	}
	return bean, ok

}

func LoadStructInfo() baseiface.ISingleton {
	var s = NewStructInfo()
	InjectStructInfo(s)
	return s

}

func InjectStructInfo(s *StructInfo) {

	goutils.Debug("inject")
}
