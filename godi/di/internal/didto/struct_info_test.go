package didto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestStructInfoSuite struct {
	suite.Suite
	inst *StructInfo

	rootdir string
}

func (this *TestStructInfoSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanStructInfo()

	this.rootdir = fileutils.FindRootDir()

}

func TestStructInfoSuites(t *testing.T) {
	suite.Run(t, new(TestStructInfoSuite))
}

func (this *TestStructInfoSuite) Test001_NewFuncNameDefault() {
	logrus.Info(1)

}

func (this *TestStructInfoSuite) Test002_CheckBaseEntity() {
	logrus.Info(1)

}

func (this *TestStructInfoSuite) Test003_ParsePkgName() {
	logrus.Info(1)

}

func (this *TestStructInfoSuite) Test004_AddMethodName() {
	logrus.Info(1)

}

func (this *TestStructInfoSuite) Test005_AddField() {
	logrus.Info(1)

}
