package diface

import "gitee.com/leijmdas/gobase/godi/di/internal/didto"

type CodeFace interface {
	MakeDi(dto *didto.DiDto) error
}
