package diface

import (
	"gitee.com/leijmdas/gobase/godi/di/internal/didto"
)

type DiFactroy interface {
	CodeFace
	FindGoFiles() error
	FindBasePkg() string

	Parse(file string) *didto.FileInfoDto
	ParseAll()

	MakeDiOne(structInfo *didto.StructInfo) bool
	MakeDiAll() int
}
