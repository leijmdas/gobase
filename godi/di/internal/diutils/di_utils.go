package diutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig/goinicfg"
)

func FindKey(key string) bool {
	return FindBeanDiUtils().FindKey(key)
}

const ini_di_key = "di"
const ini_di_gen = "generate"
const di_define_file = "/config/di/di_define.ini"

type DiUtils struct {
	basedto.BaseEntitySingle
	rootdir string
	ini     *goinicfg.GoiniConfig `json:"-"`
	//key struct ->bool
	Items map[string]string
	Rerun map[string]string
}

func NewDiUtils() *DiUtils {
	var u = &DiUtils{
		rootdir: fileutils.FindRootDir(),
		ini:     goinicfg.FindBeanGoiniConfig(),
	}
	u.init()
	return u
}

func (u *DiUtils) init() {
	u.LoadConfig()
}

func (this *DiUtils) FindKey(key string) bool {
	b, ok := this.Items[key]
	return b == "true" || ok
}

/*
#auto init generate
[generate]
rerun = true
#重新生成

[di]
IchubConfig = true
SingleEntity = true
*/
func (this *DiUtils) LoadConfig() {
	var filename = this.rootdir + "/" + di_define_file
	if !fileutils.TryFileExist(filename) {
		filename = fileutils.FindRootPkg() + "/" + di_define_file
	}
	this.ini.Load(filename)
	this.Items = this.ini.FindMap(ini_di_key)
	this.Rerun = this.ini.FindMap(ini_di_gen)
}
