package diutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: di_utils_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)
	@Update 作者: leijmdas@163.com 时间(2024-08-08 16:17:33)

* *********************************************************/

const singleNameDiUtils = "*diutils.DiUtils-939f3151a3c6478abf288ac275bdb8e5"

// init register load
func init() {
	registerBeanDiUtils()
}

// register DiUtils
func registerBeanDiUtils() {
	basedi.RegisterLoadBean(singleNameDiUtils, LoadDiUtils)
}

func FindBeanDiUtils() *DiUtils {
	var inst, ok = basedi.FindBeanOk(singleNameDiUtils)
	if !ok {
		logrus.Error("not found DiUtils!")
		return nil
	}
	bean, ok := inst.(*DiUtils)
	if !ok {
		logrus.Errorf("FindBeanDiUtils: failed to cast bean to *DiUtils")
		return nil
	}
	return bean

}
func FindBeanDiUtilsOk() (*DiUtils, bool) {

	var inst, ok = basedi.FindBeanOk(singleNameDiUtils)
	if !ok {
		logrus.Error("not found DiUtils!")
		return nil, ok
	}
	bean, ok := inst.(*DiUtils)
	if !ok {
		logrus.Errorf("FindBeanDiUtils: failed to cast bean to *DiUtils")
		return nil, ok
	}
	return bean, ok

}

func LoadDiUtils() baseiface.ISingleton {
	var s = NewDiUtils()
	InjectDiUtils(s)
	return s

}

func InjectDiUtils(s *DiUtils) {

	goutils.Debug("inject")
}
