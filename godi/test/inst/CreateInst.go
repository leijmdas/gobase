package inst

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"go/ast"
	"go/parser"
	"go/token"
	"reflect"
)

// 假设我们有一个结构体，需要为其成员实例化并注入依赖
type ExampleStruct struct {
	Member1 string
	Member2 int
}

//type RedisBaseConfig struct {
//	ichubconfig.BaseConfig
//	Db       int    `gocfg:"${redis.db:10}"`
//	Addr     string `gocfg:"${redis.addr:192.168.4.111:16379}"`
//	Password int    `gocfg:"${redis.password:112KiZ9dJBSk1cju}"`
//}
//
//func NewRedisBaseConfig() *RedisBaseConfig {
//	var rbc = &RedisBaseConfig{}
//	rbc.InitProxy(rbc)
//	return rbc
//}

// 解析源代码并获取成员的函数
func getMembers(fileContent string) ([]ast.Expr, error) {
	// 解析源代码
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "", fileContent, 0)
	if err != nil {
		return nil, err
	}

	// 假设我们只关注全局变量的声明
	var members []ast.Expr
	for _, decl := range f.Decls {
		if genDecl, ok := decl.(*ast.GenDecl); ok {
			for _, spec := range genDecl.Specs {
				if valueSpec, ok := spec.(*ast.ValueSpec); ok {
					members = append(members, valueSpec.Values...)
				}
			}
		}
	}

	return members, nil
}
func getStru(fileContent string) ([]ast.Expr, error) {
	// 解析源代码
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "", fileContent, 0)
	if err != nil {
		return nil, err
	}

	// 假设我们只关注全局变量的声明
	var members []ast.Expr
	for _, decl := range f.Decls {
		if genDecl, ok := decl.(*ast.GenDecl); ok {
			for _, spec := range genDecl.Specs {
				if valueSpec, ok := spec.(*ast.ValueSpec); ok {
					members = append(members, valueSpec.Values...)
				}
			}
		}
	}

	return members, nil
}

// 创建实例并注入依赖
func createInstance(t reflect.Type) reflect.Value {
	// 根据类型创建实例
	inst := reflect.New(t).Elem()

	// 例如，为ExampleStruct的成员赋值
	if t.Name() == "ExampleStruct" {
		inst.FieldByName("Member1").SetString("Dependency11 Injected String")
		inst.FieldByName("Member2").SetInt(4112)
	}
	fmt.Println(jsonutils.ToJsonPretty(t))

	return inst
}

func main() {
	// 示例结构体代码
	_ = `package main
 
var ExampleStructVar = ExampleStruct{
    Member1: "Hello, World!",
    Member2: 42,
}`

	var ExampleStructVar = ExampleStruct{
		Member1: "Hello,11111 World!",
		Member2: 42111,
	}
	// 使用反射来处理表达式
	//types := reflect.TypeOf(ExampleStructVar)
	v := reflect.ValueOf(ExampleStructVar)

	// 创建实例并注入依赖
	instance := createInstance(v.Type())
	fmt.Println("v=", jsonutils.ToJsonPretty(instance))

}
