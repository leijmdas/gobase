package inst

type User struct {
	Id   int
	Name string
}

type option func(*User)

func (u *User) Option(opts ...option) {
	for _, opt := range opts {
		opt(u)
	}
}

func (u *User) WithId(id int) option {
	return func(u *User) {
		u.Id = id
	}
}

func (u *User) WithName(name string) option {
	return func(u *User) {
		u.Name = name
	}
}
