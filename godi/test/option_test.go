package test

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/godi/test/inst"
	"github.com/samber/lo"
	"sort"
	"testing"
)

func Test002_opt(t *testing.T) {
	myMap := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
	}

	// 使用 lo.Keys 提取 map 的键
	keys := lo.Keys(myMap)
	golog.Info(keys[0])
}
func Test003_opt(t *testing.T) {
	numbers := []int{1, 2, 3, 4, 5}

	// 将每个元素乘以 2
	doubled := lo.Map(numbers, func(n int, _ int) int {
		return n * 2
	})

	golog.Info(doubled)
}

func Test004_Mapopt(t *testing.T) {
	numbers := []*inst.User{&inst.User{Id: 2}, &inst.User{Id: 1}}

	// 将每个元素乘以 2
	doubled := lo.Map(numbers, func(n *inst.User, _ int) int {
		return n.Id * 2
	})

	golog.Info(doubled)
}
func Test006_Filteropt(t *testing.T) {
	numbers := []*inst.User{&inst.User{Id: 2}, &inst.User{Id: 1}}

	// 将每个元素乘以 2
	doubled := lo.Filter(numbers, func(n *inst.User, _ int) bool {
		return n.Id > 1
	})

	golog.Info(doubled)
}

func Test007_Filteropt(t *testing.T) {
	numbers := []int{1, 2, 2, 3, 2, 4, 3, 3}

	// 按元素值分组
	grouped := lo.GroupBy(numbers, func(n int) int {
		return n
	})

	// 计算每组的最大值（这里每组只有一个值，仅作示例）
	maxValues := lo.MapValues(grouped, func(nums []int, _ int) int {
		return lo.Max(nums)
	})

	fmt.Println(maxValues)
}
func Test008_Filteropt(t *testing.T) {

	numbers := []*inst.User{&inst.User{Id: 2}, &inst.User{Id: 331}}

	// 使用 Reduce 求和
	sum := lo.Reduce(numbers, func(acc *inst.User, n *inst.User, _ int) *inst.User {
		acc.Id = acc.Id + n.Id
		return acc
	}, &inst.User{})
	fmt.Println(sum) // 输出: 15
}
func Test009_Filteropt(t *testing.T) {

	numbers := []*inst.User{&inst.User{Id: 2}, &inst.User{Id: 331}}

	// 使用 Reduce 求和
	lo.ForEach[*inst.User](numbers, func(acc *inst.User, i int) {
		golog.Info(i, acc)

	})

}

func Test0109_Filteropt(t *testing.T) {
	numbers := []int{3, 1, 4, 1, 5, 9}

	sort.Slice(numbers, func(i, j int) bool {
		return numbers[i] < numbers[j]
	})
	// 升序排序
	sorted := lo.IsSortedByKey[int, int](numbers, func(a int) int {
		return a
	})
	fmt.Println(sorted, numbers) // 输出: [1 1 3 4 5 9]

}
func Test0119_Filteropt(t *testing.T) {
	numbers := []int{3, 1, 4, 1, 5, 9}

	sort.Slice(numbers, func(i, j int) bool {
		return numbers[i] < numbers[j]
	})
	// 升序排序
	u := lo.Uniq(numbers)
	fmt.Println(u, numbers) // 输出: [1 1 3 4 5 9]

}
func Test200_Filteropt(t *testing.T) {
	numbers := []*inst.User{&inst.User{Id: 2}, &inst.User{Id: 2}, &inst.User{Id: 331}}

	// 升序排序
	u := lo.Uniq(numbers)
	fmt.Println(u) // 输出: [1 1 3 4 5 9]
	var tt = lo.Ternary(1 > 2, 1, 2)
	golog.Info(tt)
}
func Find(users []*inst.User, id int) (*inst.User, bool) {

	// 查找名字为 "Bob" 的人
	result, found := lo.Find(users, func(p *inst.User) bool {
		return p.Id == id
	})
	return result, found
}
func Test201_Filteropt(t *testing.T) {
	numbers := []*inst.User{&inst.User{Id: 2}, &inst.User{Id: 2}, &inst.User{Id: 331}}

	var ii = 331
	// 查找名字为 "Bob" 的人
	result, found := lo.Find(numbers, func(p *inst.User) bool {
		return p.Id == ii
	})
	fmt.Println(result, found) // 输出: {Bob 30} true
}
func Test20_Find(t *testing.T) {
	numbers := []*inst.User{&inst.User{Id: 2}, &inst.User{Id: 2}, &inst.User{Id: 331}}

	// 查找名字为 "Bob" 的人
	result, found := Find(numbers, 331)
	fmt.Println(result, found) // 输出: {Bob 30} true

	for i := range numbers {
		golog.Info(i, numbers[i])
	}

}
