package main

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"github.com/sirupsen/logrus"
	"go/build"
	"os"
	"path/filepath"
	"strings"
)

func getPkgPath(pkgPath string) (string, error) {
	pkg, err := build.Default.Import(pkgPath, "", build.FindOnly)
	return pkg.Dir, err
}

// 获取第三方包下所有文件的相对路径
func getAllFilesPath(pkgPath string) ([]string, error) {
	pkg, err := build.Default.Import(pkgPath, "", build.FindOnly)

	if err != nil {
		return nil, err
	}

	var paths []string
	err = filepath.Walk(pkg.Dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			relPath, err := filepath.Rel(pkg.Dir, path)
			if err != nil {
				return err
			}
			paths = append(paths, relPath)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return paths, nil
}

func main() {

	var p, _ = fileutils.GetPkgPath("github.com/gin-gonic/gin") // gitee.com/leijmdas/gobase/goconfig@v1.0.91
	var pp = strings.ReplaceAll(p, "github.com\\gin-gonic\\gin@v1.9.1", "gitee.com/leijmdas/gobase/goconfig@v1.0.91")

	var p1, err = build.Default.Import("gitee.com/leijmdas/gobase/goconfig", "", build.FindOnly)
	logrus.Info(p, fileutils.CheckFileExist(pp))
	logrus.Info(err, p1)
}
