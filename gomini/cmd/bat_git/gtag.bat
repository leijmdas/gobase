CD .\cmd\bat_git

IF  "%1"=="" ECHO "Usage: gtag.bat [a|d|l] [tag]"

IF  "%1"=="a" gaddtag %2

IF "%1"=="d" gdeltag %2

IF "%1"=="l" glisttag

