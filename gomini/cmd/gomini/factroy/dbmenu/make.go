package dbmenu

import (
	"fmt"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/goast"
	"github.com/spf13/cobra"
)

var DbCmd = &cobra.Command{
	Use:   "dbdict [flag]",
	Short: "dbdict table ",
	Run: func(cmd *cobra.Command, args []string) {
		flag := cmd.Flags().Arg(0)

		db.FindMetaTable(flag)
	},
}
var DbListCmd = &cobra.Command{
	Use:   "dblist [flag]",
	Short: "dblist tables",
	Run: func(cmd *cobra.Command, args []string) {

		var tables = db.FindTables()
		for _, table := range tables {
			fmt.Println(table.TableName)
		}
	},
}
var GoastCmd = &cobra.Command{
	Use:   "goast [flag]",
	Short: "goast flag stru ",
	Run: func(cmd *cobra.Command, args []string) {
		flag := cmd.Flags().Arg(0)

		inf, ok := goast.FindBeanDimetaFactroy().GoastOne(flag)
		fmt.Println(inf, ok)
	},
}
