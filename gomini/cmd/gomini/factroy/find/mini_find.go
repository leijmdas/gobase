package find

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/gomini/mini/file/minifactroy"

	"github.com/spf13/cobra"
)

type MiniFind struct {
	basedto.BaseEntitySingle
}

func NewMiniFind() *MiniFind {
	return &MiniFind{}
}

var MiniPathCmd = &cobra.Command{
	Use:   "minipath",
	Short: "minipath : get all rootDir",
	Run: func(cmd *cobra.Command, args []string) {

		var m = minifactroy.FindBeanMinifactroy().FindMiniPath()
		fmt.Println("all RootDir =", jsonutils.ToJsonPretty(m))
	},
}
