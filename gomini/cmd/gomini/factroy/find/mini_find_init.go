package find

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: mini_find_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMiniFind = "find.MiniFind"

// init register load
func init() {
	registerBeanMiniFind()
}

// register MiniFind
func registerBeanMiniFind() {
	basedi.RegisterLoadBean(singleNameMiniFind, LoadMiniFind)
}

func FindBeanMiniFind() *MiniFind {
	bean, ok := basedi.FindBean(singleNameMiniFind).(*MiniFind)
	if !ok {
		logrus.Errorf("FindBeanMiniFind: failed to cast bean to *MiniFind")
		return nil
	}
	return bean

}

func LoadMiniFind() baseiface.ISingleton {
	var s = NewMiniFind()
	InjectMiniFind(s)
	return s

}

func InjectMiniFind(s *MiniFind) {

	// logrus.Debug("inject")
}
