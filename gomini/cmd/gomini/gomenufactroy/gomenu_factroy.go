package gomenufactroy

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils/goconfig"
	"github.com/spf13/cobra"
)

var version = "1.0"

const BASE_PKG = "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type GomenuFactroy struct {
	basedto.BaseEntitySingle

	version    string
	cmdName    string
	basePkg    string
	RootCmd    *cobra.Command
	versionCmd *cobra.Command
}

func (self *GomenuFactroy) BasePkg() string {
	return self.basePkg
}

func (self *GomenuFactroy) SetBasePkg(basePkg string) {
	self.basePkg = basePkg
}

func NewGomenuFactroy() *GomenuFactroy {
	var f = &GomenuFactroy{
		cmdName: "godi",
		version: version,
		basePkg: BASE_PKG,
	}

	return f
}

// goconfig.FindBeanGominicfg().InitPkg(BASE_PKG)
func (self *GomenuFactroy) Init() *GomenuFactroy {
	goconfig.FindBeanGominiFilecfg().InitPkg(self.basePkg)

	self.rootCmds()
	self.versionCmds()
	self.AddCommand(self.versionCmd)
	return self
}
func (self *GomenuFactroy) AddCommand(cmd *cobra.Command) *GomenuFactroy {

	self.RootCmd.AddCommand(cmd)
	return self
}
func (self *GomenuFactroy) AddCommands(cmds ...*cobra.Command) *GomenuFactroy {
	for _, cmd := range cmds {
		self.RootCmd.AddCommand(cmd)
	}
	return self
}
func (self *GomenuFactroy) AddSubCmds(parentTitle string, cmds ...*cobra.Command) *GomenuFactroy {
	var pcmd = &cobra.Command{
		Use:   parentTitle,
		Short: parentTitle,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(parentTitle)
		},
	}
	for _, cmd := range cmds {
		pcmd.AddCommand(cmd)
	}
	self.RootCmd.AddCommand(pcmd)
	return self
}
func (self *GomenuFactroy) AddSub(parentTitle, short string, cmds ...*cobra.Command) *GomenuFactroy {
	var pcmd = &cobra.Command{
		Use:   parentTitle,
		Short: short,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(parentTitle)
		},
	}
	for _, cmd := range cmds {
		pcmd.AddCommand(cmd)
	}
	self.RootCmd.AddCommand(pcmd)
	return self
}
func (self *GomenuFactroy) Execute() error {

	return self.RootCmd.Execute()
}

func (self *GomenuFactroy) rootCmds() {

	self.RootCmd = &cobra.Command{
		Use:   self.cmdName,
		Short: "A godi : a tool of godimenu",
		Long:  `This is a godi application using Cobra`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Hello, " + self.cmdName + "!")
		},
	}
}
func (self *GomenuFactroy) versionCmds() {
	self.versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Print the version number of " + self.cmdName,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("godi version %s\n", self.version)
		},
	}
}

func (self *GomenuFactroy) CmdName() string {
	return self.cmdName
}

func (self *GomenuFactroy) SetCmdName(cmdName string) {
	self.cmdName = cmdName
}

func (self *GomenuFactroy) Version() string {
	return self.version
}

func (self *GomenuFactroy) SetVersion(version string) {
	self.version = version
}
