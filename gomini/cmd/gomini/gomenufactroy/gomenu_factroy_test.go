package gomenufactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/stretchr/testify/suite"
	"testing"

	"github.com/sirupsen/logrus"
)

type TestGomenuFactroySuite struct {
	suite.Suite
	inst *GomenuFactroy

	rootdir string
}

func (this *TestGomenuFactroySuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanGomenuFactroy()

	this.rootdir = fileutils.FindRootDir()

}

func TestGomenuFactroySuites(t *testing.T) {
	suite.Run(t, new(TestGomenuFactroySuite))
}

func (this *TestGomenuFactroySuite) Test001_BasePkg() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test002_SetBasePkg() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test003_Init() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test004_AddCommand() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test005_AddCommands() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test006_AddSubCmds() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test007_Execute() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test008_rootCmds() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test009_versionCmds() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test010_CmdName() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test011_SetCmdName() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test012_Version() {
	logrus.Info(1)

}

func (this *TestGomenuFactroySuite) Test013_SetVersion() {
	logrus.Info(1)

}
