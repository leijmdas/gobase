package menu

import (
	"fmt"
	"gitee.com/leijmdas/gobase/gomini/cmd/gomini/factroy/dbmenu"
	"gitee.com/leijmdas/gobase/gomini/cmd/gomini/factroy/find"
	"gitee.com/leijmdas/gobase/gomini/cmd/gomini/gomenufactroy"
	"gitee.com/leijmdas/gobase/gomini/mini/cyclocomplex"
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/gqlserver"
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/service"

	"github.com/gogf/gf/v2/util/gconv"

	//	"gitee.com/leijmdas/gobase/gomini/websample/server/startweb"
	"github.com/spf13/cobra"
)

var version = "1.0.0"

var gqlServerCmd = &cobra.Command{
	Use:   "gqlserver",
	Short: "start gqlServer",
	Run: func(cmd *cobra.Command, args []string) {
		gqlserver.FindBeanMinigqlServer().Register(service.MyQuery, service.MySimpleQuery)

		gqlserver.FindBeanMinigqlServer().StartServer()

	},
}
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "start server",
	Run: func(cmd *cobra.Command, args []string) {
		//webstart.StartWeb()

	},
}

const pkgname = "gitee.com/leijmdas/gobase/gomini/cmd/gomini/factroy/find"

var ccCheckAllCmd = &cobra.Command{
	Use:   "ccall",
	Short: "ccall : a tool of gomini menu",
	Long:  `This is a gomini cc check using Cobra`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(cmd.Flags().Args()) > 0 {
			flag := cmd.Flags().Arg(0)
			cyclocomplex.FindBeanCycloComplex().CcCheckScan(gconv.Int(flag))
			return
		}

		cyclocomplex.FindBeanCycloComplex().CcCheckAllScan()
	},
}
var ccCheckCmd = &cobra.Command{
	Use:   "cc",
	Short: "cc : a tool of gomini menu",
	Long:  `This is a gomini cc check using Cobra`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(cmd.Flags().Args()) > 0 {
			flag := cmd.Flags().Arg(0)
			cyclocomplex.FindBeanCycloComplex().CcCheckScan(gconv.Int(flag))
			return
		}

		cyclocomplex.FindBeanCycloComplex().CcCheckScan()
	},
}
var ccCheckFileCmd = &cobra.Command{
	Use:   "ccfile",
	Short: "ccfile filename : a tool of gomini menu",
	Long:  `This is a gomini cc check using Cobra`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(cmd.Flags().Args()) == 0 {
			fmt.Println("please enter file ")
			return
		}
		file := cmd.Flags().Arg(0)
		if len(cmd.Flags().Args()) > 1 {
			flag := cmd.Flags().Arg(1)
			var err = cyclocomplex.FindBeanCycloComplex().CcCheckFileScan(file, gconv.Int(flag))
			if err != nil {
				fmt.Println(err)
			}
			return
		}

		cyclocomplex.FindBeanCycloComplex().CcCheckFileScan(file)
	},
}

func init() {

	var mf = gomenufactroy.FindBeanGomenuFactroy()
	mf.SetVersion("1.0")
	mf.SetCmdName("gomini")
	mf.SetBasePkg(pkgname)
	mf.Init()
	mf.AddCommands(find.MiniPathCmd, dbmenu.DbListCmd, dbmenu.DbCmd, dbmenu.GoastCmd)
	mf.AddCommands(ccCheckCmd, ccCheckAllCmd, ccCheckFileCmd, gqlServerCmd, serverCmd)

}

func startGql() {
	gqlserver.FindBeanMinigqlServer().Register(service.GqlQuery)
	gqlserver.FindBeanMinigqlServer().Register(service.MyQuery, service.MySimpleQuery)

	gqlserver.FindBeanMinigqlServer().StartServer()
}

func Execute() {

	gomenufactroy.FindBeanGomenuFactroy().Execute()
}
