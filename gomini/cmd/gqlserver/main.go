package main

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/gqlserver"
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/service"
)

func main() {
	gqlserver.FindBeanMinigqlServer().Register(service.GqlQuery)
	gqlserver.FindBeanMinigqlServer().Register(service.MyQuery, service.MySimpleQuery)
	golog.Info("gqlserver")
	gqlserver.FindBeanMinigqlServer().StartServer()
}
