rule "func-dbmetadata-rule" "func-dbmetadata-rule" salience 0
begin

    IchubLog(In)

    id=@id
    Out.SetParam("FuncId","func-dbmetadata-rule")
    DbResult =  RuleFunc.DbMetadataQuery(In)

    Out.SetReturn(200,"计算成功")
    Out.SetParam("DbResult",DbResult)

end