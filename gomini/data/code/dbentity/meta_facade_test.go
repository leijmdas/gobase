package dbentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestMetaFacadeSuite struct {
	suite.Suite

	inst    *metafacade.MetaFacade
	rootdir string
	dbtype  string
	cfg     *ichubconfig.IchubConfig
}

func TestMetaFacadeSuites(t *testing.T) {
	suite.Run(t, new(TestMetaFacadeSuite))
}

func (suite *TestMetaFacadeSuite) SetupSuite() {
	logrus.Info(" setup suite")

	suite.inst = metafacade.FindBeanMetaFacade()
	suite.rootdir = fileutils.FindRootDir()
	suite.cfg = ichubconfig.FindBeanIchubConfig()
	suite.dbtype = suite.cfg.ReadIchubDb().Dbtype
	suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_MYSQL
	//suite.cfg.Gorm.DbType = baseconsts.DB_TYPE_COCKROACH

}

func (suite *TestMetaFacadeSuite) TearDownSuite() {
	logrus.Info(" teardown suite")
	suite.cfg.Gorm.DbType = suite.dbtype
	//var dto = suite.cfg.ReadIchubDb()
	//golog.Info(dto)

}
func (suite *TestMetaFacadeSuite) SetupTest() {
	logrus.Info(" setup test")
}

func (suite *TestMetaFacadeSuite) TearDownTest() {
	logrus.Info(" teardown test")
}

func (this *TestMetaFacadeSuite) Test001_MetaEs() {
	golog.Info(1)
	this.inst.MetaEs("ichub_sys_dept")
}

func (this *TestMetaFacadeSuite) Test002_MetaEsSortType() {
	golog.Info(2)
	this.inst.MetaEsSortType("ichub_sys_dept")
}

func (this *TestMetaFacadeSuite) Test003_MetaDb() {

	this.inst.MetaDb("general_rule")

}
func (this *TestMetaFacadeSuite) Test004_QueryDb() {

	//var dao = NewDaoSysUser()
	//dao.SetPageSize(2).DbBetween("id", 3, 8)
	//ret := dao.Query()
	//
	//golog.Info(ret.Code)
	//if ret.IsFailed() {
	//	golog.Error(ret)
	//}

}
