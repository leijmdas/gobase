package esentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/goweb/common/webclient/eswebclient/webfacade"
	"github.com/gogf/gf/v2/util/gconv"
	jsoniter "github.com/json-iterator/go"
	"github.com/olivere/elastic/v7"
	"time"
)



/*
@Title    文件名称: ichub_sys_dept_es.go
@Description  描述: 统一返回结构

@Author  作者: raymond@163.com  时间(2025-02-28 23:02:38)
@Update  作者: raymond@163.com  时间(2025-02-28 23:02:38)
*/

type  IchubSysDeptEs struct {
	 basedto.BaseEntity 

	 ParentId string `json:"parent_id"`
	 CreateBy string `json:"create_by"`
	 DeptId string `json:"dept_id"`
	 DeptName string `json:"dept_name"`
	 Leader string `json:"leader"`
	 UpdateBy string `json:"update_by"`
	 DelFlag string `json:"del_flag"`
	 Email string `json:"email"`
	 OrderNm int64 `json:"order_nm,string"`
	 Phone string `json:"phone"`
	 CreatedBy string `json:"created_by"`
	 CreateTime time.Time `json:"create_time"`
	 Status string `json:"status"`
	 Ancestors string `json:"ancestors"` 

}

type  IchubSysDeptEsDto struct {
	 basedto.BaseEntity 

	 ParentId string `json:"parent_id"`
	 CreateBy string `json:"create_by"`
	 DeptId string `json:"dept_id"`
	 DeptName string `json:"dept_name"`
	 Leader string `json:"leader"`
	 UpdateBy string `json:"update_by"`
	 DelFlag string `json:"del_flag"`
	 Email string `json:"email"`
	 OrderNm int64 `json:"order_nm,string"`
	 Phone string `json:"phone"`
	 CreatedBy string `json:"created_by"`
	 CreateTime time.Time `json:"create_time"`
	 Status string `json:"status"`
	 Ancestors string `json:"ancestors"` 

}


func NewIchubSysDeptEs() *IchubSysDeptEs {
	return &IchubSysDeptEs{}
}
  
func (self *IchubSysDeptEs) PkeyName() string {
	return "id"
}
func (self *IchubSysDeptEs) PkeyValue() string {
	return gconv.String(self.Id)
}
func (self *IchubSysDeptEs) TableName() string {
	return "ichub_sys_dept"
}
func (self *IchubSysDeptEs) IndexName() string {
	return self.TableName()
}

func (self *IchubSysDeptEs) GetMapping() string {
	return "{}"
}
func (self *IchubSysDeptEs) Mapping() string {
	return self.GetMapping()
}
func (self *IchubSysDeptEs) Unmarshal(data []byte) error {
	return jsoniter.Unmarshal(data, self)
}

func (self IchubSysDeptEs) IndexID() string {
	return self.PkeyName()
}

func (self IchubSysDeptEs) IndexAliasName() string {
	return self.IndexName() + "_alias"
}
 

func (self *IchubSysDeptEs) EsFill(p any) error {
	var err = gconv.Struct(p, self)
	if err != nil {
		golog.Error("IchubSysDeptEs err:", err)
	}
	return err
}

func (self *IchubSysDeptEs) Init() {
}
func (self *IchubSysDeptEs) Shutdown() {

}

func NewWebFacadeIchubSysDeptEsOf (q elastic.Query) *webfacade.WebFacade[*IchubSysDeptEs] {
	var facade = webfacade.DefaultOf[*IchubSysDeptEs](q)
	facade.Query(q)
	return facade
}

func NewWebFacadeIchubSysDeptEs () *webfacade.WebFacade[*IchubSysDeptEs] {
	return  webfacade.Default[*IchubSysDeptEs]() 
 
}

func (self *IchubSysDeptEs) DefaultOf(q elastic.Query) *webfacade.WebFacade[*IchubSysDeptEs] {
	return NewWebFacadeIchubSysDeptEsOf(q)

}
func (self *IchubSysDeptEs) Default() *webfacade.WebFacade[*IchubSysDeptEs] {
	return NewWebFacadeIchubSysDeptEs()
}

 
