/*
Navicat MySQL Data Transfer

Source Server         : huawei.akunlong.top
Source Server Version : 50733
Source Host           : huawei.akunlong.top:13306
Source Database       : wso2am

Target Server Type    : MYSQL
Target Server Version : 50733
 Encoding         : 65001

Date: 2024-05-17 08:25:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for am_alert_emaillist
-- ----------------------------
DROP TABLE IF EXISTS `am_alert_emaillist`;
CREATE TABLE `am_alert_emaillist` (
  `EMAIL_LIST_ID` int(11) NOT NULL COMMENT '电子邮件列表ID',
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  `STAKE_HOLDER` varchar(100) NOT NULL,
  PRIMARY KEY (`EMAIL_LIST_ID`,`USER_NAME`,`STAKE_HOLDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am提醒电子邮件列表';

-- ----------------------------
-- Table structure for am_alert_emaillist_details
-- ----------------------------
DROP TABLE IF EXISTS `am_alert_emaillist_details`;
CREATE TABLE `am_alert_emaillist_details` (
  `EMAIL_LIST_ID` int(11) NOT NULL COMMENT '电子邮件列表ID',
  `EMAIL` varchar(255) NOT NULL COMMENT '电子邮件',
  PRIMARY KEY (`EMAIL_LIST_ID`,`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='警报电子邮件列表详细信息';

-- ----------------------------
-- Table structure for am_alert_types
-- ----------------------------
DROP TABLE IF EXISTS `am_alert_types`;
CREATE TABLE `am_alert_types` (
  `ALERT_TYPE_ID` int(11) NOT NULL COMMENT '警报类型ID',
  `ALERT_TYPE_NAME` varchar(255) DEFAULT NULL COMMENT '警报类型名称',
  `STAKE_HOLDER` varchar(100) DEFAULT NULL COMMENT 'STAKE HOLDER',
  PRIMARY KEY (`ALERT_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am警报类型';

-- ----------------------------
-- Table structure for am_alert_types_values
-- ----------------------------
DROP TABLE IF EXISTS `am_alert_types_values`;
CREATE TABLE `am_alert_types_values` (
  `ALERT_TYPE_ID` int(11) NOT NULL COMMENT '警报类型ID',
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  `STAKE_HOLDER` varchar(100) NOT NULL COMMENT 'STAKE HOLDER',
  PRIMARY KEY (`ALERT_TYPE_ID`,`USER_NAME`,`STAKE_HOLDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am警报类型值';

-- ----------------------------
-- Table structure for am_api
-- ----------------------------
DROP TABLE IF EXISTS `am_api`;
CREATE TABLE `am_api` (
  `API_ID` int(11) NOT NULL AUTO_INCREMENT,
  `API_UUID` varchar(256) DEFAULT NULL COMMENT 'neneneba api UUID',
  `API_PROVIDER` varchar(200) DEFAULT NULL COMMENT 'api提供商',
  `API_NAME` varchar(200) DEFAULT NULL COMMENT 'api名称',
  `API_VERSION` varchar(30) DEFAULT NULL COMMENT 'api版本',
  `CONTEXT` varchar(256) DEFAULT NULL COMMENT '上下文',
  `CONTEXT_TEMPLATE` varchar(256) DEFAULT NULL COMMENT '上下文模板',
  `API_TIER` varchar(256) DEFAULT NULL COMMENT 'api等级',
  `API_TYPE` varchar(10) DEFAULT NULL COMMENT 'api类型',
  `ORGANIZATION` varchar(100) DEFAULT NULL,
  `GATEWAY_VENDOR` varchar(100) DEFAULT NULL COMMENT '网关供应商',
  `CREATED_BY` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `UPDATED_BY` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '更新的时间',
  `STATUS` varchar(30) DEFAULT NULL COMMENT '状态',
  `LOG_LEVEL` varchar(255) DEFAULT NULL COMMENT '日志级别',
  `REVISIONS_CREATED` int(11) DEFAULT NULL COMMENT '已创建修订',
  `VERSION_COMPARABLE` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`API_ID`),
  UNIQUE KEY `API_PROVIDER` (`API_PROVIDER`,`API_NAME`,`API_VERSION`,`ORGANIZATION`),
  UNIQUE KEY `API_UUID` (`API_UUID`),
  KEY `IDX_AAI_CTX` (`CONTEXT`),
  KEY `IDX_AAI_ORG` (`ORGANIZATION`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COMMENT='am接口';

-- ----------------------------
-- Table structure for am_api_categories
-- ----------------------------
DROP TABLE IF EXISTS `am_api_categories`;
CREATE TABLE `am_api_categories` (
  `UUID` varchar(50) NOT NULL COMMENT 'UUID',
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL,
  `ORGANIZATION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UUID`),
  UNIQUE KEY `NAME` (`NAME`,`ORGANIZATION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api类别';

-- ----------------------------
-- Table structure for am_api_client_certificate
-- ----------------------------
DROP TABLE IF EXISTS `am_api_client_certificate`;
CREATE TABLE `am_api_client_certificate` (
  `TENANT_ID` int(11) NOT NULL,
  `ALIAS` varchar(45) NOT NULL COMMENT 'ALIAS',
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba api ID',
  `CERTIFICATE` blob COMMENT '证书',
  `REMOVED` tinyint(1) NOT NULL COMMENT '已删除',
  `TIER_NAME` varchar(512) DEFAULT NULL,
  `REVISION_UUID` varchar(255) NOT NULL COMMENT '修订UUID',
  PRIMARY KEY (`ALIAS`,`TENANT_ID`,`REMOVED`,`REVISION_UUID`),
  KEY `API_ID` (`API_ID`),
  CONSTRAINT `am_api_client_certificate_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for am_api_comments
-- ----------------------------
DROP TABLE IF EXISTS `am_api_comments`;
CREATE TABLE `am_api_comments` (
  `COMMENT_ID` varchar(64) NOT NULL COMMENT '评论ID',
  `COMMENT_TEXT` varchar(512) DEFAULT NULL COMMENT '注释文本',
  `CREATED_BY` varchar(255) DEFAULT NULL COMMENT '创建者',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '更新的时间',
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba api ID',
  `PARENT_COMMENT_ID` varchar(64) DEFAULT NULL COMMENT '家长评论ID',
  `ENTRY_POINT` varchar(20) DEFAULT NULL COMMENT '入口点',
  `CATEGORY` varchar(20) DEFAULT NULL COMMENT '类别',
  PRIMARY KEY (`COMMENT_ID`),
  KEY `API_ID` (`API_ID`),
  KEY `PARENT_COMMENT_ID` (`PARENT_COMMENT_ID`),
  CONSTRAINT `am_api_comments_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE,
  CONSTRAINT `am_api_comments_ibfk_2` FOREIGN KEY (`PARENT_COMMENT_ID`) REFERENCES `am_api_comments` (`COMMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api注释';

-- ----------------------------
-- Table structure for am_api_default_version
-- ----------------------------
DROP TABLE IF EXISTS `am_api_default_version`;
CREATE TABLE `am_api_default_version` (
  `DEFAULT_VERSION_ID` int(11) NOT NULL COMMENT '默认版本ID',
  `API_NAME` varchar(256) DEFAULT NULL COMMENT 'api名称',
  `API_PROVIDER` varchar(256) DEFAULT NULL COMMENT 'api提供商',
  `DEFAULT_API_VERSION` varchar(30) DEFAULT NULL COMMENT '默认api版本',
  `PUBLISHED_DEFAULT_API_VERSION` varchar(30) DEFAULT NULL,
  `ORGANIZATION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`DEFAULT_VERSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api默认版本';

-- ----------------------------
-- Table structure for am_api_environment_keys
-- ----------------------------
DROP TABLE IF EXISTS `am_api_environment_keys`;
CREATE TABLE `am_api_environment_keys` (
  `UUID` varchar(45) NOT NULL COMMENT 'UUID',
  `ENVIRONMENT_ID` varchar(45) DEFAULT NULL COMMENT '环境ID',
  `API_UUID` varchar(256) DEFAULT NULL COMMENT 'neneneba api UUID',
  `PROPERTY_CONFIG` blob COMMENT '属性配置',
  PRIMARY KEY (`UUID`),
  UNIQUE KEY `ENVIRONMENT_ID` (`ENVIRONMENT_ID`,`API_UUID`),
  KEY `API_UUID` (`API_UUID`),
  CONSTRAINT `am_api_environment_keys_ibfk_1` FOREIGN KEY (`API_UUID`) REFERENCES `am_api` (`API_UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am-api环境密钥';

-- ----------------------------
-- Table structure for am_api_lc_event
-- ----------------------------
DROP TABLE IF EXISTS `am_api_lc_event`;
CREATE TABLE `am_api_lc_event` (
  `EVENT_ID` int(11) NOT NULL COMMENT '事件ID',
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba api ID',
  `PREVIOUS_STATE` varchar(50) DEFAULT NULL COMMENT 'PREVIOUS STATE',
  `NEW_STATE` varchar(50) DEFAULT NULL COMMENT '新状态',
  `USER_ID` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `TENANT_ID` int(11) NOT NULL,
  `EVENT_DATE` timestamp NULL DEFAULT NULL COMMENT '事件日期',
  PRIMARY KEY (`EVENT_ID`),
  KEY `API_ID` (`API_ID`),
  CONSTRAINT `am_api_lc_event_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api lc事件';

-- ----------------------------
-- Table structure for am_api_lc_publish_events
-- ----------------------------
DROP TABLE IF EXISTS `am_api_lc_publish_events`;
CREATE TABLE `am_api_lc_publish_events` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_DOMAIN` varchar(500) DEFAULT NULL COMMENT '租户域',
  `API_ID` varchar(500) DEFAULT NULL COMMENT 'neneneba api ID',
  `EVENT_TIME` timestamp NULL DEFAULT NULL COMMENT '事件时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api lc发布事件';

-- ----------------------------
-- Table structure for am_api_operation_policy
-- ----------------------------
DROP TABLE IF EXISTS `am_api_operation_policy`;
CREATE TABLE `am_api_operation_policy` (
  `API_SPECIFIC_POLICY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `POLICY_UUID` varchar(45) NOT NULL,
  `API_UUID` varchar(45) DEFAULT NULL COMMENT 'neneneba api UUID',
  `REVISION_UUID` varchar(45) DEFAULT NULL COMMENT '修订UUID',
  `CLONED_POLICY_UUID` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`API_SPECIFIC_POLICY_ID`),
  KEY `POLICY_UUID` (`POLICY_UUID`),
  CONSTRAINT `am_api_operation_policy_ibfk_1` FOREIGN KEY (`POLICY_UUID`) REFERENCES `am_operation_policy` (`POLICY_UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api操作策略';

-- ----------------------------
-- Table structure for am_api_operation_policy_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_api_operation_policy_mapping`;
CREATE TABLE `am_api_operation_policy_mapping` (
  `OPERATION_POLICY_MAPPING_ID` int(11) NOT NULL COMMENT '操作策略映射ID',
  `URL_MAPPING_ID` int(11) DEFAULT NULL COMMENT 'URL映射ID',
  `POLICY_UUID` varchar(45) NOT NULL,
  `POLICY_ORDER` int(11) DEFAULT NULL COMMENT '政策订单',
  `DIRECTION` varchar(10) NOT NULL,
  `PARAMETERS` varchar(1024) DEFAULT NULL COMMENT '参数',
  PRIMARY KEY (`OPERATION_POLICY_MAPPING_ID`),
  KEY `URL_MAPPING_ID` (`URL_MAPPING_ID`),
  KEY `POLICY_UUID` (`POLICY_UUID`),
  CONSTRAINT `am_api_operation_policy_mapping_ibfk_1` FOREIGN KEY (`URL_MAPPING_ID`) REFERENCES `am_api_url_mapping` (`URL_MAPPING_ID`) ON DELETE CASCADE,
  CONSTRAINT `am_api_operation_policy_mapping_ibfk_2` FOREIGN KEY (`POLICY_UUID`) REFERENCES `am_operation_policy` (`POLICY_UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am-api操作策略映射';

-- ----------------------------
-- Table structure for am_api_product_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_api_product_mapping`;
CREATE TABLE `am_api_product_mapping` (
  `API_PRODUCT_MAPPING_ID` int(11) NOT NULL COMMENT 'neneneba api产品映射ID',
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba api ID',
  `URL_MAPPING_ID` int(11) DEFAULT NULL COMMENT 'URL映射ID',
  `REVISION_UUID` varchar(255) DEFAULT NULL COMMENT '修订UUID',
  PRIMARY KEY (`API_PRODUCT_MAPPING_ID`),
  KEY `URL_MAPPING_ID` (`URL_MAPPING_ID`),
  KEY `IDX_AAPM_AI` (`API_ID`),
  CONSTRAINT `am_api_product_mapping_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE,
  CONSTRAINT `am_api_product_mapping_ibfk_2` FOREIGN KEY (`URL_MAPPING_ID`) REFERENCES `am_api_url_mapping` (`URL_MAPPING_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am-api产品映射';

-- ----------------------------
-- Table structure for am_api_ratings
-- ----------------------------
DROP TABLE IF EXISTS `am_api_ratings`;
CREATE TABLE `am_api_ratings` (
  `RATING_ID` varchar(255) NOT NULL,
  `API_ID` int(11) DEFAULT NULL COMMENT 'api ID',
  `RATING` int(11) DEFAULT NULL,
  `SUBSCRIBER_ID` int(11) DEFAULT NULL COMMENT '订阅ID',
  PRIMARY KEY (`RATING_ID`),
  KEY `API_ID` (`API_ID`),
  KEY `SUBSCRIBER_ID` (`SUBSCRIBER_ID`),
  CONSTRAINT `am_api_ratings_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `am_api_ratings_ibfk_2` FOREIGN KEY (`SUBSCRIBER_ID`) REFERENCES `am_subscriber` (`SUBSCRIBER_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api评级';

-- ----------------------------
-- Table structure for am_api_resource_scope_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_api_resource_scope_mapping`;
CREATE TABLE `am_api_resource_scope_mapping` (
  `SCOPE_NAME` varchar(255) NOT NULL,
  `URL_MAPPING_ID` int(11) NOT NULL,
  `TENANT_ID` int(11) NOT NULL,
  PRIMARY KEY (`SCOPE_NAME`,`URL_MAPPING_ID`),
  KEY `URL_MAPPING_ID` (`URL_MAPPING_ID`),
  CONSTRAINT `am_api_resource_scope_mapping_ibfk_1` FOREIGN KEY (`URL_MAPPING_ID`) REFERENCES `am_api_url_mapping` (`URL_MAPPING_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am-api资源范围映射';

-- ----------------------------
-- Table structure for am_api_revision_metadata
-- ----------------------------
DROP TABLE IF EXISTS `am_api_revision_metadata`;
CREATE TABLE `am_api_revision_metadata` (
  `API_UUID` varchar(64) DEFAULT NULL COMMENT 'neneneba api UUID',
  `REVISION_UUID` varchar(255) DEFAULT NULL COMMENT '修订UUID',
  `API_TIER` varchar(128) DEFAULT NULL COMMENT 'neneneba api等级',
  UNIQUE KEY `API_UUID` (`API_UUID`,`REVISION_UUID`),
  KEY `REVISION_UUID` (`REVISION_UUID`),
  CONSTRAINT `am_api_revision_metadata_ibfk_1` FOREIGN KEY (`REVISION_UUID`) REFERENCES `am_revision` (`REVISION_UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am api修订元数据';

-- ----------------------------
-- Table structure for am_api_service_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_api_service_mapping`;
CREATE TABLE `am_api_service_mapping` (
  `API_ID` int(11) NOT NULL COMMENT 'neneneba api ID',
  `SERVICE_KEY` varchar(256) NOT NULL COMMENT '服务密钥',
  `MD5` varchar(100) DEFAULT NULL COMMENT 'MD5',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`API_ID`,`SERVICE_KEY`),
  CONSTRAINT `am_api_service_mapping_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am-api服务映射';

-- ----------------------------
-- Table structure for am_api_throttle_policy
-- ----------------------------
DROP TABLE IF EXISTS `am_api_throttle_policy`;
CREATE TABLE `am_api_throttle_policy` (
  `POLICY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(512) DEFAULT NULL COMMENT '名称',
  `DISPLAY_NAME` varchar(512) DEFAULT NULL COMMENT '显示名称',
  `TENANT_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '描述',
  `DEFAULT_QUOTA_TYPE` varchar(25) DEFAULT NULL COMMENT '默认报价类型',
  `DEFAULT_QUOTA` int(11) DEFAULT NULL COMMENT '默认报价',
  `DEFAULT_QUOTA_UNIT` varchar(10) DEFAULT NULL COMMENT '默认报价单位',
  `DEFAULT_UNIT_TIME` int(11) DEFAULT NULL COMMENT '默认单位时间',
  `DEFAULT_TIME_UNIT` varchar(25) DEFAULT NULL COMMENT '默认时间单位',
  `APPLICABLE_LEVEL` varchar(25) DEFAULT NULL COMMENT '适用级别',
  `IS_DEPLOYED` tinyint(1) DEFAULT NULL COMMENT '已部署',
  `UUID` varchar(256) DEFAULT NULL COMMENT 'UUID',
  PRIMARY KEY (`POLICY_ID`),
  UNIQUE KEY `API_NAME_TENANT` (`NAME`,`TENANT_ID`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `IDX_AATP_DQT` (`DEFAULT_QUOTA_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='am api节流策略';

-- ----------------------------
-- Table structure for am_api_url_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_api_url_mapping`;
CREATE TABLE `am_api_url_mapping` (
  `URL_MAPPING_ID` int(11) NOT NULL AUTO_INCREMENT,
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba api ID',
  `HTTP_METHOD` varchar(20) DEFAULT NULL COMMENT 'HTTP方法',
  `AUTH_SCHEME` varchar(50) DEFAULT NULL COMMENT '验证方案',
  `URL_PATTERN` varchar(512) DEFAULT NULL,
  `THROTTLING_TIER` varchar(512) DEFAULT NULL,
  `MEDIATION_SCRIPT` blob COMMENT '调解脚本',
  `REVISION_UUID` varchar(255) DEFAULT NULL COMMENT '修订UUID',
  PRIMARY KEY (`URL_MAPPING_ID`),
  KEY `IDX_AAUM_AI` (`API_ID`),
  KEY `IDX_AAUM_TT` (`THROTTLING_TIER`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=latin1 COMMENT='am-api url映射';

-- ----------------------------
-- Table structure for am_app_key_domain_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_app_key_domain_mapping`;
CREATE TABLE `am_app_key_domain_mapping` (
  `CONSUMER_KEY` varchar(255) NOT NULL,
  `AUTHZ_DOMAIN` varchar(255) NOT NULL COMMENT 'AUTHZ域',
  PRIMARY KEY (`CONSUMER_KEY`,`AUTHZ_DOMAIN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am应用程序密钥域映射';

-- ----------------------------
-- Table structure for am_application
-- ----------------------------
DROP TABLE IF EXISTS `am_application`;
CREATE TABLE `am_application` (
  `APPLICATION_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '应用标识',
  `NAME` varchar(100) DEFAULT NULL COMMENT '名称',
  `SUBSCRIBER_ID` int(11) DEFAULT NULL COMMENT '订阅程序ID',
  `APPLICATION_TIER` varchar(50) DEFAULT NULL COMMENT '应用层',
  `CALLBACK_URL` varchar(512) DEFAULT NULL COMMENT 'CALLBACK URL',
  `DESCRIPTION` varchar(512) DEFAULT NULL,
  `APPLICATION_STATUS` varchar(50) DEFAULT NULL COMMENT '应用程序状态',
  `GROUP_ID` varchar(100) DEFAULT NULL COMMENT '组ID',
  `CREATED_BY` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `UPDATED_BY` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '更新的时间',
  `UUID` varchar(256) DEFAULT NULL COMMENT 'UUID',
  `TOKEN_TYPE` varchar(10) DEFAULT NULL COMMENT '代币类型',
  `ORGANIZATION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`APPLICATION_ID`),
  UNIQUE KEY `NAME` (`NAME`,`SUBSCRIBER_ID`,`ORGANIZATION`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `SUBSCRIBER_ID` (`SUBSCRIBER_ID`),
  KEY `IDX_AA_AT_CB` (`APPLICATION_TIER`,`CREATED_BY`),
  CONSTRAINT `am_application_ibfk_1` FOREIGN KEY (`SUBSCRIBER_ID`) REFERENCES `am_subscriber` (`SUBSCRIBER_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='am应用程序';

-- ----------------------------
-- Table structure for am_application_attributes
-- ----------------------------
DROP TABLE IF EXISTS `am_application_attributes`;
CREATE TABLE `am_application_attributes` (
  `APPLICATION_ID` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL COMMENT '名称',
  `APP_ATTRIBUTE` varchar(1024) DEFAULT NULL COMMENT '应用程序属性',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`APPLICATION_ID`,`NAME`),
  CONSTRAINT `am_application_attributes_ibfk_1` FOREIGN KEY (`APPLICATION_ID`) REFERENCES `am_application` (`APPLICATION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am应用程序属性';

-- ----------------------------
-- Table structure for am_application_group_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_application_group_mapping`;
CREATE TABLE `am_application_group_mapping` (
  `APPLICATION_ID` int(11) NOT NULL,
  `GROUP_ID` varchar(512) NOT NULL COMMENT '组ID',
  `TENANT` varchar(255) NOT NULL,
  PRIMARY KEY (`APPLICATION_ID`,`GROUP_ID`,`TENANT`),
  CONSTRAINT `am_application_group_mapping_ibfk_1` FOREIGN KEY (`APPLICATION_ID`) REFERENCES `am_application` (`APPLICATION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am应用程序组映射';

-- ----------------------------
-- Table structure for am_application_key_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_application_key_mapping`;
CREATE TABLE `am_application_key_mapping` (
  `UUID` varchar(100) DEFAULT NULL COMMENT 'UUID',
  `APPLICATION_ID` int(11) NOT NULL,
  `CONSUMER_KEY` varchar(255) DEFAULT NULL,
  `KEY_TYPE` varchar(512) NOT NULL COMMENT '密钥类型',
  `STATE` varchar(30) DEFAULT NULL COMMENT 'STATE',
  `CREATE_MODE` varchar(30) DEFAULT NULL COMMENT '创建模式',
  `KEY_MANAGER` varchar(100) NOT NULL COMMENT '密钥管理器',
  `APP_INFO` blob COMMENT '应用程序信息',
  PRIMARY KEY (`APPLICATION_ID`,`KEY_TYPE`,`KEY_MANAGER`),
  KEY `IDX_AAKM_CK` (`CONSUMER_KEY`),
  CONSTRAINT `am_application_key_mapping_ibfk_1` FOREIGN KEY (`APPLICATION_ID`) REFERENCES `am_application` (`APPLICATION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am应用程序密钥映射';

-- ----------------------------
-- Table structure for am_application_registration
-- ----------------------------
DROP TABLE IF EXISTS `am_application_registration`;
CREATE TABLE `am_application_registration` (
  `REG_ID` int(11) NOT NULL COMMENT 'REG ID',
  `SUBSCRIBER_ID` int(11) DEFAULT NULL COMMENT '订阅程序ID',
  `WF_REF` varchar(255) DEFAULT NULL COMMENT 'WF REF',
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  `TOKEN_TYPE` varchar(30) DEFAULT NULL COMMENT '代币类型',
  `TOKEN_SCOPE` varchar(1500) DEFAULT NULL COMMENT '代币范围',
  `INPUTS` varchar(1000) DEFAULT NULL COMMENT 'INPUTS',
  `ALLOWED_DOMAINS` varchar(256) DEFAULT NULL COMMENT '允许的域',
  `VALIDITY_PERIOD` bigint(20) DEFAULT NULL COMMENT '有效期',
  `KEY_MANAGER` varchar(255) DEFAULT NULL COMMENT '密钥管理器',
  PRIMARY KEY (`REG_ID`),
  UNIQUE KEY `SUBSCRIBER_ID` (`SUBSCRIBER_ID`,`APP_ID`,`TOKEN_TYPE`,`KEY_MANAGER`),
  KEY `APP_ID` (`APP_ID`),
  CONSTRAINT `am_application_registration_ibfk_1` FOREIGN KEY (`SUBSCRIBER_ID`) REFERENCES `am_subscriber` (`SUBSCRIBER_ID`) ON UPDATE CASCADE,
  CONSTRAINT `am_application_registration_ibfk_2` FOREIGN KEY (`APP_ID`) REFERENCES `am_application` (`APPLICATION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am应用程序注册';

-- ----------------------------
-- Table structure for am_block_conditions
-- ----------------------------
DROP TABLE IF EXISTS `am_block_conditions`;
CREATE TABLE `am_block_conditions` (
  `CONDITION_ID` int(11) NOT NULL COMMENT '条件ID',
  `TYPE` varchar(45) DEFAULT NULL COMMENT 'TYPE',
  `BLOCK_CONDITION` varchar(512) DEFAULT NULL COMMENT '块条件',
  `ENABLED` varchar(45) DEFAULT NULL COMMENT '已启用',
  `DOMAIN` varchar(45) DEFAULT NULL COMMENT '域',
  `UUID` varchar(256) DEFAULT NULL COMMENT 'UUID',
  PRIMARY KEY (`CONDITION_ID`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am块条件';

-- ----------------------------
-- Table structure for am_certificate_metadata
-- ----------------------------
DROP TABLE IF EXISTS `am_certificate_metadata`;
CREATE TABLE `am_certificate_metadata` (
  `TENANT_ID` int(11) NOT NULL,
  `ALIAS` varchar(255) NOT NULL COMMENT 'ALIAS',
  `END_POINT` varchar(255) DEFAULT NULL COMMENT '终点',
  `CERTIFICATE` blob COMMENT '证书',
  PRIMARY KEY (`ALIAS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am证书元数据';

-- ----------------------------
-- Table structure for am_common_operation_policy
-- ----------------------------
DROP TABLE IF EXISTS `am_common_operation_policy`;
CREATE TABLE `am_common_operation_policy` (
  `COMMON_POLICY_ID` int(11) NOT NULL COMMENT '公共策略ID',
  `POLICY_UUID` varchar(45) NOT NULL,
  PRIMARY KEY (`COMMON_POLICY_ID`),
  KEY `POLICY_UUID` (`POLICY_UUID`),
  CONSTRAINT `am_common_operation_policy_ibfk_1` FOREIGN KEY (`POLICY_UUID`) REFERENCES `am_operation_policy` (`POLICY_UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='常用操作策略';

-- ----------------------------
-- Table structure for am_condition_group
-- ----------------------------
DROP TABLE IF EXISTS `am_condition_group`;
CREATE TABLE `am_condition_group` (
  `CONDITION_GROUP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `POLICY_ID` int(11) DEFAULT NULL COMMENT '策略ID',
  `QUOTA_TYPE` varchar(25) DEFAULT NULL COMMENT 'QUOTA-TYPE',
  `QUOTA` int(11) DEFAULT NULL COMMENT 'QUOTA',
  `QUOTA_UNIT` varchar(10) DEFAULT NULL COMMENT '报价单位',
  `UNIT_TIME` int(11) DEFAULT NULL COMMENT '单位时间',
  `TIME_UNIT` varchar(25) DEFAULT NULL COMMENT '时间单位',
  `DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`CONDITION_GROUP_ID`),
  KEY `POLICY_ID` (`POLICY_ID`),
  KEY `IDX_ACG_QT` (`QUOTA_TYPE`),
  CONSTRAINT `am_condition_group_ibfk_1` FOREIGN KEY (`POLICY_ID`) REFERENCES `am_api_throttle_policy` (`POLICY_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am条件组';

-- ----------------------------
-- Table structure for am_correlation_configs
-- ----------------------------
DROP TABLE IF EXISTS `am_correlation_configs`;
CREATE TABLE `am_correlation_configs` (
  `COMPONENT_NAME` varchar(45) NOT NULL COMMENT '组件名称',
  `ENABLED` varchar(45) DEFAULT NULL COMMENT '已启用',
  PRIMARY KEY (`COMPONENT_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am相关性配置';

-- ----------------------------
-- Table structure for am_correlation_properties
-- ----------------------------
DROP TABLE IF EXISTS `am_correlation_properties`;
CREATE TABLE `am_correlation_properties` (
  `PROPERTY_NAME` varchar(45) NOT NULL,
  `COMPONENT_NAME` varchar(45) NOT NULL,
  `PROPERTY_VALUE` varchar(1023) DEFAULT NULL COMMENT '属性值',
  PRIMARY KEY (`PROPERTY_NAME`,`COMPONENT_NAME`),
  KEY `COMPONENT_NAME` (`COMPONENT_NAME`),
  CONSTRAINT `am_correlation_properties_ibfk_1` FOREIGN KEY (`COMPONENT_NAME`) REFERENCES `am_correlation_configs` (`COMPONENT_NAME`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am相关性属性';

-- ----------------------------
-- Table structure for am_deployed_revision
-- ----------------------------
DROP TABLE IF EXISTS `am_deployed_revision`;
CREATE TABLE `am_deployed_revision` (
  `NAME` varchar(255) NOT NULL,
  `VHOST` varchar(255) DEFAULT NULL COMMENT 'VHOST',
  `REVISION_UUID` varchar(255) NOT NULL,
  `DEPLOYED_TIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NAME`,`REVISION_UUID`),
  KEY `REVISION_UUID` (`REVISION_UUID`),
  CONSTRAINT `am_deployed_revision_ibfk_1` FOREIGN KEY (`REVISION_UUID`) REFERENCES `am_revision` (`REVISION_UUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am部署的修订';

-- ----------------------------
-- Table structure for am_deployment_revision_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_deployment_revision_mapping`;
CREATE TABLE `am_deployment_revision_mapping` (
  `NAME` varchar(255) NOT NULL,
  `VHOST` varchar(255) DEFAULT NULL,
  `REVISION_UUID` varchar(255) NOT NULL,
  `DISPLAY_ON_DEVPORTAL` tinyint(1) DEFAULT '0',
  `DEPLOYED_TIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NAME`,`REVISION_UUID`),
  KEY `REVISION_UUID` (`REVISION_UUID`),
  CONSTRAINT `am_deployment_revision_mapping_ibfk_1` FOREIGN KEY (`REVISION_UUID`) REFERENCES `am_revision` (`REVISION_UUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for am_external_stores
-- ----------------------------
DROP TABLE IF EXISTS `am_external_stores`;
CREATE TABLE `am_external_stores` (
  `APISTORE_ID` int(11) NOT NULL COMMENT 'APISTORE ID',
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba API ID',
  `STORE_ID` varchar(255) DEFAULT NULL COMMENT 'STORE ID',
  `STORE_DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '存储显示名称',
  `STORE_ENDPOINT` varchar(255) DEFAULT NULL COMMENT '存储终点',
  `STORE_TYPE` varchar(255) DEFAULT NULL COMMENT '存储类型',
  `LAST_UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '上次更新时间',
  PRIMARY KEY (`APISTORE_ID`),
  KEY `API_ID` (`API_ID`),
  CONSTRAINT `am_external_stores_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am外部存储';

-- ----------------------------
-- Table structure for am_gateway_environment
-- ----------------------------
DROP TABLE IF EXISTS `am_gateway_environment`;
CREATE TABLE `am_gateway_environment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UUID` varchar(45) DEFAULT NULL COMMENT 'UUID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `DESCRIPTION` varchar(1023) DEFAULT NULL,
  `PROVIDER` varchar(255) DEFAULT NULL COMMENT '提供者',
  `ORGANIZATION` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`,`ORGANIZATION`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am网关环境';

-- ----------------------------
-- Table structure for am_graphql_complexity
-- ----------------------------
DROP TABLE IF EXISTS `am_graphql_complexity`;
CREATE TABLE `am_graphql_complexity` (
  `UUID` varchar(256) NOT NULL COMMENT 'UUID',
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba API ID',
  `TYPE` varchar(256) DEFAULT NULL COMMENT 'TYPE',
  `FIELD` varchar(256) DEFAULT NULL COMMENT 'FIELD',
  `COMPLEXITY_VALUE` int(11) DEFAULT NULL COMMENT '复杂性值',
  `REVISION_UUID` varchar(255) DEFAULT NULL COMMENT 'REVISION UUID',
  PRIMARY KEY (`UUID`),
  KEY `API_ID` (`API_ID`),
  CONSTRAINT `am_graphql_complexity_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am graphql复杂性';

-- ----------------------------
-- Table structure for am_gw_api_artifacts
-- ----------------------------
DROP TABLE IF EXISTS `am_gw_api_artifacts`;
CREATE TABLE `am_gw_api_artifacts` (
  `API_ID` varchar(255) NOT NULL COMMENT 'neneneba api ID',
  `REVISION_ID` varchar(255) NOT NULL COMMENT '修订ID',
  `ARTIFACT` mediumblob COMMENT '艺术',
  `TIME_STAMP` timestamp NULL DEFAULT NULL COMMENT '时间戳',
  PRIMARY KEY (`REVISION_ID`,`API_ID`),
  KEY `API_ID` (`API_ID`),
  CONSTRAINT `am_gw_api_artifacts_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_gw_published_api_details` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am gw api工件';

-- ----------------------------
-- Table structure for am_gw_api_deployments
-- ----------------------------
DROP TABLE IF EXISTS `am_gw_api_deployments`;
CREATE TABLE `am_gw_api_deployments` (
  `API_ID` varchar(255) NOT NULL COMMENT 'neneneba api ID',
  `REVISION_ID` varchar(255) NOT NULL COMMENT '修订ID',
  `LABEL` varchar(255) NOT NULL COMMENT '标签',
  `VHOST` varchar(255) DEFAULT NULL COMMENT 'VHOST',
  PRIMARY KEY (`REVISION_ID`,`API_ID`,`LABEL`),
  KEY `API_ID` (`API_ID`),
  CONSTRAINT `am_gw_api_deployments_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_gw_published_api_details` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am gw api部署';

-- ----------------------------
-- Table structure for am_gw_published_api_details
-- ----------------------------
DROP TABLE IF EXISTS `am_gw_published_api_details`;
CREATE TABLE `am_gw_published_api_details` (
  `API_ID` varchar(255) NOT NULL COMMENT 'neneneba api ID',
  `TENANT_DOMAIN` varchar(255) DEFAULT NULL COMMENT '张量域',
  `API_PROVIDER` varchar(255) DEFAULT NULL COMMENT 'api提供商',
  `API_NAME` varchar(255) DEFAULT NULL COMMENT 'api名称',
  `API_VERSION` varchar(255) DEFAULT NULL COMMENT 'api版本',
  `API_TYPE` varchar(50) DEFAULT NULL COMMENT 'api类型',
  PRIMARY KEY (`API_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am-gw已发布的api详细信息';

-- ----------------------------
-- Table structure for am_gw_vhost
-- ----------------------------
DROP TABLE IF EXISTS `am_gw_vhost`;
CREATE TABLE `am_gw_vhost` (
  `GATEWAY_ENV_ID` int(11) NOT NULL COMMENT 'GATEWAY ENV ID',
  `HOST` varchar(255) NOT NULL COMMENT 'HOST',
  `HTTP_CONTEXT` varchar(255) DEFAULT NULL COMMENT 'HTTP上下文',
  `HTTP_PORT` varchar(5) DEFAULT NULL COMMENT 'HTTP端口',
  `HTTPS_PORT` varchar(5) DEFAULT NULL COMMENT 'HTTPS端口',
  `WS_PORT` varchar(5) DEFAULT NULL COMMENT 'WS端口',
  `WSS_PORT` varchar(5) DEFAULT NULL COMMENT 'WSS端口',
  PRIMARY KEY (`GATEWAY_ENV_ID`,`HOST`),
  CONSTRAINT `am_gw_vhost_ibfk_1` FOREIGN KEY (`GATEWAY_ENV_ID`) REFERENCES `am_gateway_environment` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am gw vhost';

-- ----------------------------
-- Table structure for am_header_field_condition
-- ----------------------------
DROP TABLE IF EXISTS `am_header_field_condition`;
CREATE TABLE `am_header_field_condition` (
  `HEADER_FIELD_ID` int(11) NOT NULL COMMENT '标题字段ID',
  `CONDITION_GROUP_ID` int(11) DEFAULT NULL COMMENT '条件组ID',
  `HEADER_FIELD_NAME` varchar(255) DEFAULT NULL COMMENT '标题字段名称',
  `HEADER_FIELD_VALUE` varchar(255) DEFAULT NULL COMMENT '标题字段值',
  `IS_HEADER_FIELD_MAPPING` tinyint(1) DEFAULT NULL COMMENT 'IS标题字段映射',
  PRIMARY KEY (`HEADER_FIELD_ID`),
  KEY `CONDITION_GROUP_ID` (`CONDITION_GROUP_ID`),
  CONSTRAINT `am_header_field_condition_ibfk_1` FOREIGN KEY (`CONDITION_GROUP_ID`) REFERENCES `am_condition_group` (`CONDITION_GROUP_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am标头字段条件';

-- ----------------------------
-- Table structure for am_ip_condition
-- ----------------------------
DROP TABLE IF EXISTS `am_ip_condition`;
CREATE TABLE `am_ip_condition` (
  `AM_IP_CONDITION_ID` int(11) NOT NULL COMMENT 'am ip条件ID',
  `STARTING_IP` varchar(45) DEFAULT NULL COMMENT '启动ip',
  `ENDING_IP` varchar(45) DEFAULT NULL COMMENT 'ENDING ip',
  `SPECIFIC_IP` varchar(45) DEFAULT NULL COMMENT '特定ip',
  `WITHIN_IP_RANGE` tinyint(1) DEFAULT NULL COMMENT '在ip范围内',
  `CONDITION_GROUP_ID` int(11) DEFAULT NULL COMMENT '条件组ID',
  PRIMARY KEY (`AM_IP_CONDITION_ID`),
  KEY `fk_AM_IP_CONDITION_1_idx` (`CONDITION_GROUP_ID`),
  CONSTRAINT `fk_AM_IP_CONDITION_1` FOREIGN KEY (`CONDITION_GROUP_ID`) REFERENCES `am_condition_group` (`CONDITION_GROUP_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am ip条件';

-- ----------------------------
-- Table structure for am_jwt_claim_condition
-- ----------------------------
DROP TABLE IF EXISTS `am_jwt_claim_condition`;
CREATE TABLE `am_jwt_claim_condition` (
  `JWT_CLAIM_ID` int(11) NOT NULL COMMENT 'jwt CLAIMID',
  `CONDITION_GROUP_ID` int(11) DEFAULT NULL COMMENT '条件组ID',
  `CLAIM_URI` varchar(512) DEFAULT NULL,
  `CLAIM_ATTRIB` varchar(1024) DEFAULT NULL,
  `IS_CLAIM_MAPPING` tinyint(1) DEFAULT NULL COMMENT 'IS声明映射',
  PRIMARY KEY (`JWT_CLAIM_ID`),
  KEY `CONDITION_GROUP_ID` (`CONDITION_GROUP_ID`),
  CONSTRAINT `am_jwt_claim_condition_ibfk_1` FOREIGN KEY (`CONDITION_GROUP_ID`) REFERENCES `am_condition_group` (`CONDITION_GROUP_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am jwt索赔条件';

-- ----------------------------
-- Table structure for am_key_manager
-- ----------------------------
DROP TABLE IF EXISTS `am_key_manager`;
CREATE TABLE `am_key_manager` (
  `UUID` varchar(50) NOT NULL COMMENT 'UUID',
  `NAME` varchar(100) DEFAULT NULL COMMENT '名称',
  `DISPLAY_NAME` varchar(100) DEFAULT NULL COMMENT '显示名称',
  `DESCRIPTION` varchar(256) DEFAULT NULL,
  `TYPE` varchar(45) DEFAULT NULL COMMENT 'TYPE',
  `CONFIGURATION` blob COMMENT '配置',
  `ENABLED` tinyint(1) DEFAULT NULL COMMENT '已启用',
  `ORGANIZATION` varchar(100) DEFAULT NULL,
  `TOKEN_TYPE` varchar(45) DEFAULT NULL COMMENT '代币类型',
  `EXTERNAL_REFERENCE_ID` varchar(100) DEFAULT NULL COMMENT '外部参考ID',
  PRIMARY KEY (`UUID`),
  UNIQUE KEY `NAME` (`NAME`,`ORGANIZATION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am密钥管理器';

-- ----------------------------
-- Table structure for am_monetization
-- ----------------------------
DROP TABLE IF EXISTS `am_monetization`;
CREATE TABLE `am_monetization` (
  `API_ID` int(11) NOT NULL,
  `TIER_NAME` varchar(512) DEFAULT NULL,
  `STRIPE_PRODUCT_ID` varchar(512) DEFAULT NULL,
  `STRIPE_PLAN_ID` varchar(512) DEFAULT NULL,
  KEY `API_ID` (`API_ID`),
  CONSTRAINT `am_monetization_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for am_monetization_platform_customers
-- ----------------------------
DROP TABLE IF EXISTS `am_monetization_platform_customers`;
CREATE TABLE `am_monetization_platform_customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SUBSCRIBER_ID` int(11) NOT NULL,
  `TENANT_ID` int(11) NOT NULL,
  `CUSTOMER_ID` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `SUBSCRIBER_ID` (`SUBSCRIBER_ID`),
  CONSTRAINT `am_monetization_platform_customers_ibfk_1` FOREIGN KEY (`SUBSCRIBER_ID`) REFERENCES `am_subscriber` (`SUBSCRIBER_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am货币化平台客户';

-- ----------------------------
-- Table structure for am_monetization_shared_customers
-- ----------------------------
DROP TABLE IF EXISTS `am_monetization_shared_customers`;
CREATE TABLE `am_monetization_shared_customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `APPLICATION_ID` int(11) NOT NULL,
  `API_PROVIDER` varchar(256) NOT NULL,
  `TENANT_ID` int(11) NOT NULL,
  `SHARED_CUSTOMER_ID` varchar(256) NOT NULL,
  `PARENT_CUSTOMER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `APPLICATION_ID` (`APPLICATION_ID`),
  KEY `PARENT_CUSTOMER_ID` (`PARENT_CUSTOMER_ID`),
  CONSTRAINT `am_monetization_shared_customers_ibfk_1` FOREIGN KEY (`APPLICATION_ID`) REFERENCES `am_application` (`APPLICATION_ID`) ON DELETE CASCADE,
  CONSTRAINT `am_monetization_shared_customers_ibfk_2` FOREIGN KEY (`PARENT_CUSTOMER_ID`) REFERENCES `am_monetization_platform_customers` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am货币化共享客户';

-- ----------------------------
-- Table structure for am_monetization_subscriptions
-- ----------------------------
DROP TABLE IF EXISTS `am_monetization_subscriptions`;
CREATE TABLE `am_monetization_subscriptions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SUBSCRIBED_APPLICATION_ID` int(11) NOT NULL,
  `SUBSCRIBED_API_ID` int(11) NOT NULL,
  `TENANT_ID` int(11) NOT NULL,
  `SUBSCRIPTION_ID` varchar(256) NOT NULL,
  `SHARED_CUSTOMER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `SUBSCRIBED_APPLICATION_ID` (`SUBSCRIBED_APPLICATION_ID`),
  KEY `SUBSCRIBED_API_ID` (`SUBSCRIBED_API_ID`),
  KEY `SHARED_CUSTOMER_ID` (`SHARED_CUSTOMER_ID`),
  CONSTRAINT `am_monetization_subscriptions_ibfk_1` FOREIGN KEY (`SUBSCRIBED_APPLICATION_ID`) REFERENCES `am_application` (`APPLICATION_ID`) ON DELETE CASCADE,
  CONSTRAINT `am_monetization_subscriptions_ibfk_2` FOREIGN KEY (`SUBSCRIBED_API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE,
  CONSTRAINT `am_monetization_subscriptions_ibfk_3` FOREIGN KEY (`SHARED_CUSTOMER_ID`) REFERENCES `am_monetization_shared_customers` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am货币化订阅';

-- ----------------------------
-- Table structure for am_monetization_usage
-- ----------------------------
DROP TABLE IF EXISTS `am_monetization_usage`;
CREATE TABLE `am_monetization_usage` (
  `ID` varchar(100) NOT NULL,
  `STATE` varchar(50) NOT NULL,
  `STATUS` varchar(50) NOT NULL,
  `STARTED_TIME` varchar(50) NOT NULL,
  `PUBLISHED_TIME` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am货币化使用';

-- ----------------------------
-- Table structure for am_notification_subscriber
-- ----------------------------
DROP TABLE IF EXISTS `am_notification_subscriber`;
CREATE TABLE `am_notification_subscriber` (
  `UUID` varchar(255) NOT NULL COMMENT 'UUID',
  `CATEGORY` varchar(255) DEFAULT NULL COMMENT 'CATEGORY',
  `NOTIFICATION_METHOD` varchar(255) DEFAULT NULL COMMENT '通知方法',
  `SUBSCRIBER_ADDRESS` varchar(255) NOT NULL COMMENT '订阅地址',
  PRIMARY KEY (`UUID`,`SUBSCRIBER_ADDRESS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am通知订阅者';

-- ----------------------------
-- Table structure for am_operation_policy
-- ----------------------------
DROP TABLE IF EXISTS `am_operation_policy`;
CREATE TABLE `am_operation_policy` (
  `POLICY_UUID` varchar(45) NOT NULL,
  `POLICY_NAME` varchar(300) DEFAULT NULL COMMENT '策略名称',
  `POLICY_VERSION` varchar(45) DEFAULT NULL COMMENT '策略版本',
  `DISPLAY_NAME` varchar(300) DEFAULT NULL COMMENT '显示名称',
  `POLICY_DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '策略描述',
  `APPLICABLE_FLOWS` varchar(45) DEFAULT NULL COMMENT '适用流量',
  `GATEWAY_TYPES` varchar(45) DEFAULT NULL COMMENT '网关类型',
  `API_TYPES` varchar(45) DEFAULT NULL COMMENT 'neneneba API类型',
  `POLICY_PARAMETERS` blob COMMENT '策略参数',
  `ORGANIZATION` varchar(100) DEFAULT NULL COMMENT '组织',
  `POLICY_CATEGORY` varchar(45) DEFAULT NULL COMMENT '策略类别',
  `POLICY_MD5` varchar(45) DEFAULT NULL COMMENT '策略MD5',
  PRIMARY KEY (`POLICY_UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am操作策略';

-- ----------------------------
-- Table structure for am_operation_policy_definition
-- ----------------------------
DROP TABLE IF EXISTS `am_operation_policy_definition`;
CREATE TABLE `am_operation_policy_definition` (
  `DEFINITION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `POLICY_UUID` varchar(45) NOT NULL,
  `POLICY_DEFINITION` blob COMMENT '策略定义',
  `GATEWAY_TYPE` varchar(20) DEFAULT NULL COMMENT '网关类型',
  `DEFINITION_MD5` varchar(45) NOT NULL,
  PRIMARY KEY (`DEFINITION_ID`),
  UNIQUE KEY `POLICY_UUID` (`POLICY_UUID`,`GATEWAY_TYPE`),
  CONSTRAINT `am_operation_policy_definition_ibfk_1` FOREIGN KEY (`POLICY_UUID`) REFERENCES `am_operation_policy` (`POLICY_UUID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 COMMENT='am操作策略定义';

-- ----------------------------
-- Table structure for am_policy_application
-- ----------------------------
DROP TABLE IF EXISTS `am_policy_application`;
CREATE TABLE `am_policy_application` (
  `POLICY_ID` int(11) NOT NULL COMMENT '策略ID',
  `NAME` varchar(512) DEFAULT NULL COMMENT '名称',
  `DISPLAY_NAME` varchar(512) DEFAULT NULL COMMENT '显示名称',
  `TENANT_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL,
  `QUOTA_TYPE` varchar(25) DEFAULT NULL COMMENT 'QUOTA-TYPE',
  `QUOTA` int(11) DEFAULT NULL COMMENT 'QUOTA',
  `QUOTA_UNIT` varchar(10) DEFAULT NULL COMMENT '报价单位',
  `UNIT_TIME` int(11) DEFAULT NULL COMMENT '单位时间',
  `TIME_UNIT` varchar(25) DEFAULT NULL COMMENT '时间单位',
  `IS_DEPLOYED` tinyint(1) DEFAULT NULL COMMENT '已部署',
  `CUSTOM_ATTRIBUTES` blob COMMENT '自定义属性',
  `UUID` varchar(256) DEFAULT NULL COMMENT 'UUID',
  PRIMARY KEY (`POLICY_ID`),
  UNIQUE KEY `APP_NAME_TENANT` (`NAME`,`TENANT_ID`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `IDX_APA_QT` (`QUOTA_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am策略应用程序';

-- ----------------------------
-- Table structure for am_policy_global
-- ----------------------------
DROP TABLE IF EXISTS `am_policy_global`;
CREATE TABLE `am_policy_global` (
  `POLICY_ID` int(11) NOT NULL COMMENT '策略ID',
  `NAME` varchar(512) DEFAULT NULL COMMENT '名称',
  `KEY_TEMPLATE` varchar(512) DEFAULT NULL COMMENT '密钥模板',
  `TENANT_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '描述',
  `SIDDHI_QUERY` blob,
  `IS_DEPLOYED` tinyint(1) DEFAULT NULL COMMENT '已部署',
  `UUID` varchar(256) DEFAULT NULL COMMENT 'UUID',
  PRIMARY KEY (`POLICY_ID`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am策略全局';

-- ----------------------------
-- Table structure for am_policy_hard_throttling
-- ----------------------------
DROP TABLE IF EXISTS `am_policy_hard_throttling`;
CREATE TABLE `am_policy_hard_throttling` (
  `POLICY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(512) NOT NULL,
  `TENANT_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL,
  `QUOTA_TYPE` varchar(25) NOT NULL,
  `QUOTA` int(11) NOT NULL,
  `QUOTA_UNIT` varchar(10) DEFAULT NULL,
  `UNIT_TIME` int(11) NOT NULL,
  `TIME_UNIT` varchar(25) NOT NULL,
  `IS_DEPLOYED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`POLICY_ID`),
  UNIQUE KEY `POLICY_HARD_NAME_TENANT` (`NAME`,`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am策略硬节流';

-- ----------------------------
-- Table structure for am_policy_plan_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_policy_plan_mapping`;
CREATE TABLE `am_policy_plan_mapping` (
  `POLICY_UUID` varchar(256) DEFAULT NULL,
  `PRODUCT_ID` varchar(512) DEFAULT NULL,
  `PLAN_ID` varchar(512) DEFAULT NULL COMMENT '计划ID',
  KEY `POLICY_UUID` (`POLICY_UUID`),
  CONSTRAINT `am_policy_plan_mapping_ibfk_1` FOREIGN KEY (`POLICY_UUID`) REFERENCES `am_policy_subscription` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am策略计划映射';

-- ----------------------------
-- Table structure for am_policy_subscription
-- ----------------------------
DROP TABLE IF EXISTS `am_policy_subscription`;
CREATE TABLE `am_policy_subscription` (
  `POLICY_ID` int(11) NOT NULL COMMENT '策略ID',
  `NAME` varchar(512) DEFAULT NULL COMMENT '名称',
  `DISPLAY_NAME` varchar(512) DEFAULT NULL COMMENT '显示名称',
  `TENANT_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL,
  `QUOTA_TYPE` varchar(25) DEFAULT NULL COMMENT '报价类型',
  `QUOTA` int(11) DEFAULT NULL COMMENT 'QUOTA',
  `QUOTA_UNIT` varchar(10) DEFAULT NULL COMMENT '报价单位',
  `UNIT_TIME` int(11) DEFAULT NULL COMMENT '单位时间',
  `TIME_UNIT` varchar(25) DEFAULT NULL COMMENT '时间单位',
  `RATE_LIMIT_COUNT` int(11) DEFAULT NULL COMMENT '速率限制计数',
  `RATE_LIMIT_TIME_UNIT` varchar(25) DEFAULT NULL COMMENT 'RATE LIMIT时间单位',
  `IS_DEPLOYED` tinyint(1) DEFAULT NULL COMMENT '已部署',
  `CUSTOM_ATTRIBUTES` blob COMMENT '自定义属性',
  `STOP_ON_QUOTA_REACH` tinyint(1) NOT NULL DEFAULT '0',
  `BILLING_PLAN` varchar(20) DEFAULT NULL COMMENT '计费计划',
  `UUID` varchar(256) DEFAULT NULL COMMENT 'UUID',
  `MONETIZATION_PLAN` varchar(25) DEFAULT NULL,
  `FIXED_RATE` varchar(15) DEFAULT NULL COMMENT '固定费率',
  `BILLING_CYCLE` varchar(15) DEFAULT NULL COMMENT '计费周期',
  `PRICE_PER_REQUEST` varchar(15) DEFAULT NULL COMMENT '每个请求的价格',
  `CURRENCY` varchar(15) DEFAULT NULL COMMENT '货币',
  `MAX_COMPLEXITY` int(11) DEFAULT NULL COMMENT '最大复杂性',
  `MAX_DEPTH` int(11) DEFAULT NULL COMMENT '最大深度',
  `CONNECTIONS_COUNT` int(11) DEFAULT NULL COMMENT '连接计数',
  PRIMARY KEY (`POLICY_ID`),
  UNIQUE KEY `AM_POLICY_SUBSCRIPTION_NAME_TENANT` (`NAME`,`TENANT_ID`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `IDX_APS_QT` (`QUOTA_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am策略订阅';

-- ----------------------------
-- Table structure for am_query_parameter_condition
-- ----------------------------
DROP TABLE IF EXISTS `am_query_parameter_condition`;
CREATE TABLE `am_query_parameter_condition` (
  `QUERY_PARAMETER_ID` int(11) NOT NULL COMMENT '查询参数ID',
  `CONDITION_GROUP_ID` int(11) DEFAULT NULL COMMENT '条件组ID',
  `PARAMETER_NAME` varchar(255) DEFAULT NULL COMMENT '参数名称',
  `PARAMETER_VALUE` varchar(255) DEFAULT NULL COMMENT '参数值',
  `IS_PARAM_MAPPING` tinyint(1) DEFAULT NULL COMMENT 'IS PARAM映射',
  PRIMARY KEY (`QUERY_PARAMETER_ID`),
  KEY `CONDITION_GROUP_ID` (`CONDITION_GROUP_ID`),
  CONSTRAINT `am_query_parameter_condition_ibfk_1` FOREIGN KEY (`CONDITION_GROUP_ID`) REFERENCES `am_condition_group` (`CONDITION_GROUP_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am查询参数条件';

-- ----------------------------
-- Table structure for am_revision
-- ----------------------------
DROP TABLE IF EXISTS `am_revision`;
CREATE TABLE `am_revision` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `API_UUID` varchar(256) NOT NULL COMMENT 'neneneba API UUID',
  `REVISION_UUID` varchar(255) DEFAULT NULL COMMENT '修订UUID',
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `CREATED_BY` varchar(255) DEFAULT NULL COMMENT '创建者',
  PRIMARY KEY (`ID`,`API_UUID`),
  UNIQUE KEY `REVISION_UUID` (`REVISION_UUID`),
  KEY `API_UUID` (`API_UUID`),
  CONSTRAINT `am_revision_ibfk_1` FOREIGN KEY (`API_UUID`) REFERENCES `am_api` (`API_UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am修订';

-- ----------------------------
-- Table structure for am_revoked_jwt
-- ----------------------------
DROP TABLE IF EXISTS `am_revoked_jwt`;
CREATE TABLE `am_revoked_jwt` (
  `UUID` varchar(255) NOT NULL COMMENT 'UUID',
  `SIGNATURE` varchar(2048) NOT NULL,
  `EXPIRY_TIMESTAMP` bigint(20) DEFAULT NULL COMMENT 'EXPIRY TIMESTAMP',
  `TENANT_ID` int(11) DEFAULT '-1',
  `TOKEN_TYPE` varchar(15) DEFAULT NULL COMMENT '代币类型',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  PRIMARY KEY (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am已撤销jwt';

-- ----------------------------
-- Table structure for am_scope
-- ----------------------------
DROP TABLE IF EXISTS `am_scope`;
CREATE TABLE `am_scope` (
  `SCOPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `DESCRIPTION` varchar(512) DEFAULT NULL COMMENT '描述',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `SCOPE_TYPE` varchar(255) DEFAULT NULL COMMENT '范围类型',
  PRIMARY KEY (`SCOPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='am范围';

-- ----------------------------
-- Table structure for am_scope_binding
-- ----------------------------
DROP TABLE IF EXISTS `am_scope_binding`;
CREATE TABLE `am_scope_binding` (
  `SCOPE_ID` int(11) DEFAULT NULL COMMENT '范围ID',
  `SCOPE_BINDING` varchar(255) DEFAULT NULL COMMENT '范围绑定',
  `BINDING_TYPE` varchar(255) DEFAULT NULL COMMENT '绑定类型',
  KEY `SCOPE_ID` (`SCOPE_ID`),
  CONSTRAINT `am_scope_binding_ibfk_1` FOREIGN KEY (`SCOPE_ID`) REFERENCES `am_scope` (`SCOPE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am范围绑定';

-- ----------------------------
-- Table structure for am_security_audit_uuid_mapping
-- ----------------------------
DROP TABLE IF EXISTS `am_security_audit_uuid_mapping`;
CREATE TABLE `am_security_audit_uuid_mapping` (
  `API_ID` int(11) NOT NULL,
  `AUDIT_UUID` varchar(255) NOT NULL,
  PRIMARY KEY (`API_ID`),
  CONSTRAINT `am_security_audit_uuid_mapping_ibfk_1` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for am_service_catalog
-- ----------------------------
DROP TABLE IF EXISTS `am_service_catalog`;
CREATE TABLE `am_service_catalog` (
  `UUID` varchar(36) NOT NULL COMMENT 'UUID',
  `SERVICE_KEY` varchar(512) DEFAULT NULL COMMENT '服务密钥',
  `MD5` varchar(100) DEFAULT NULL COMMENT 'MD5',
  `SERVICE_NAME` varchar(255) DEFAULT NULL COMMENT '服务名称',
  `SERVICE_VERSION` varchar(30) DEFAULT NULL COMMENT '服务版本',
  `TENANT_ID` int(11) NOT NULL,
  `SERVICE_URL` varchar(2048) DEFAULT NULL COMMENT '服务URL',
  `DEFINITION_TYPE` varchar(20) DEFAULT NULL,
  `DEFINITION_URL` varchar(2048) DEFAULT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '描述',
  `SECURITY_TYPE` varchar(50) DEFAULT NULL COMMENT '安全类型',
  `MUTUAL_SSL_ENABLED` tinyint(1) DEFAULT '0',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `LAST_UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '上次更新时间',
  `CREATED_BY` varchar(255) DEFAULT NULL COMMENT '创建者',
  `UPDATED_BY` varchar(255) DEFAULT NULL COMMENT '更新者',
  `SERVICE_DEFINITION` blob COMMENT '服务定义',
  PRIMARY KEY (`UUID`),
  UNIQUE KEY `SERVICE_NAME` (`SERVICE_NAME`,`SERVICE_VERSION`,`TENANT_ID`),
  UNIQUE KEY `SERVICE_KEY` (`SERVICE_KEY`,`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am服务目录';

-- ----------------------------
-- Table structure for am_shared_scope
-- ----------------------------
DROP TABLE IF EXISTS `am_shared_scope`;
CREATE TABLE `am_shared_scope` (
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `UUID` varchar(256) NOT NULL COMMENT 'UUID',
  `TENANT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am共享作用域';

-- ----------------------------
-- Table structure for am_subscriber
-- ----------------------------
DROP TABLE IF EXISTS `am_subscriber`;
CREATE TABLE `am_subscriber` (
  `SUBSCRIBER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `EMAIL_ADDRESS` varchar(256) DEFAULT NULL COMMENT '电子邮件地址',
  `DATE_SUBSCRIBED` timestamp NULL DEFAULT NULL COMMENT '订阅日期',
  `CREATED_BY` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `UPDATED_BY` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '更新的时间',
  PRIMARY KEY (`SUBSCRIBER_ID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='am订阅者';

-- ----------------------------
-- Table structure for am_subscription
-- ----------------------------
DROP TABLE IF EXISTS `am_subscription`;
CREATE TABLE `am_subscription` (
  `SUBSCRIPTION_ID` int(11) NOT NULL COMMENT '订阅ID',
  `TIER_ID` varchar(50) DEFAULT NULL COMMENT 'TIER ID',
  `TIER_ID_PENDING` varchar(50) DEFAULT NULL COMMENT 'TIER ID PENDING',
  `API_ID` int(11) DEFAULT NULL COMMENT 'neneneba API ID',
  `LAST_ACCESSED` timestamp NULL DEFAULT NULL COMMENT '上次访问',
  `APPLICATION_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  `SUB_STATUS` varchar(50) DEFAULT NULL COMMENT '子状态',
  `SUBS_CREATE_STATE` varchar(50) DEFAULT 'SUBSCRIBE',
  `CREATED_BY` varchar(100) DEFAULT NULL COMMENT '创建者',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `UPDATED_BY` varchar(100) DEFAULT NULL COMMENT '更新者',
  `UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '更新的时间',
  `UUID` varchar(256) DEFAULT NULL COMMENT 'UUID',
  PRIMARY KEY (`SUBSCRIPTION_ID`),
  UNIQUE KEY `UUID` (`UUID`),
  KEY `IDX_SUB_APP_ID` (`APPLICATION_ID`,`SUBSCRIPTION_ID`),
  KEY `IDX_AS_AITIAI` (`API_ID`,`TIER_ID`,`APPLICATION_ID`),
  CONSTRAINT `am_subscription_ibfk_1` FOREIGN KEY (`APPLICATION_ID`) REFERENCES `am_application` (`APPLICATION_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `am_subscription_ibfk_2` FOREIGN KEY (`API_ID`) REFERENCES `am_api` (`API_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am订阅';

-- ----------------------------
-- Table structure for am_system_apps
-- ----------------------------
DROP TABLE IF EXISTS `am_system_apps`;
CREATE TABLE `am_system_apps` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `CONSUMER_KEY` varchar(512) DEFAULT NULL COMMENT '消费者密钥',
  `CONSUMER_SECRET` varchar(512) DEFAULT NULL COMMENT '消费者机密',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `TENANT_DOMAIN` varchar(255) DEFAULT NULL COMMENT '租户域',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CONSUMER_KEY` (`CONSUMER_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am系统应用程序';

-- ----------------------------
-- Table structure for am_system_configs
-- ----------------------------
DROP TABLE IF EXISTS `am_system_configs`;
CREATE TABLE `am_system_configs` (
  `ORGANIZATION` varchar(100) NOT NULL COMMENT '组织',
  `CONFIG_TYPE` varchar(100) NOT NULL COMMENT '配置类型',
  `CONFIGURATION` blob COMMENT '配置',
  PRIMARY KEY (`ORGANIZATION`,`CONFIG_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am系统配置';

-- ----------------------------
-- Table structure for am_tenant_themes
-- ----------------------------
DROP TABLE IF EXISTS `am_tenant_themes`;
CREATE TABLE `am_tenant_themes` (
  `TENANT_ID` int(11) NOT NULL,
  `THEME` mediumblob COMMENT '主题',
  PRIMARY KEY (`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am租户主题';

-- ----------------------------
-- Table structure for am_throttle_tier_permissions
-- ----------------------------
DROP TABLE IF EXISTS `am_throttle_tier_permissions`;
CREATE TABLE `am_throttle_tier_permissions` (
  `THROTTLE_TIER_PERMISSIONS_ID` int(11) NOT NULL COMMENT 'throttle tier permissions ID',
  `TIER` varchar(50) DEFAULT NULL COMMENT 'tier',
  `PERMISSIONS_TYPE` varchar(50) DEFAULT NULL COMMENT '许可类型',
  `ROLES` varchar(512) DEFAULT NULL COMMENT '角色',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`THROTTLE_TIER_PERMISSIONS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='限制层权限';

-- ----------------------------
-- Table structure for am_tier_permissions
-- ----------------------------
DROP TABLE IF EXISTS `am_tier_permissions`;
CREATE TABLE `am_tier_permissions` (
  `TIER_PERMISSIONS_ID` int(11) NOT NULL COMMENT 'tier permissions ID',
  `TIER` varchar(50) DEFAULT NULL COMMENT 'tier',
  `PERMISSIONS_TYPE` varchar(50) DEFAULT NULL COMMENT '许可类型',
  `ROLES` varchar(512) DEFAULT NULL COMMENT '角色',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`TIER_PERMISSIONS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am层权限';

-- ----------------------------
-- Table structure for am_usage_uploaded_s
-- ----------------------------
DROP TABLE IF EXISTS `am_usage_uploaded_s`;
CREATE TABLE `am_usage_uploaded_s` (
  `TENANT_DOMAIN` varchar(255) NOT NULL,
  `_NAME` varchar(255) NOT NULL COMMENT '文件名',
  `_TIMESTAMP` timestamp NOT NULL COMMENT '文件时间戳',
  `_PROCESSED` tinyint(1) DEFAULT NULL COMMENT '文件已处理',
  `_CONTENT` mediumblob COMMENT '文件内容',
  PRIMARY KEY (`TENANT_DOMAIN`,`_NAME`,`_TIMESTAMP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am使用上传的文件';

-- ----------------------------
-- Table structure for am_user
-- ----------------------------
DROP TABLE IF EXISTS `am_user`;
CREATE TABLE `am_user` (
  `USER_ID` varchar(255) NOT NULL COMMENT '用户ID',
  `USER_NAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am用户';

-- ----------------------------
-- Table structure for am_webhooks_subscription
-- ----------------------------
DROP TABLE IF EXISTS `am_webhooks_subscription`;
CREATE TABLE `am_webhooks_subscription` (
  `WH_SUBSCRIPTION_ID` int(11) NOT NULL COMMENT 'WH subscription ID',
  `API_UUID` varchar(255) DEFAULT NULL COMMENT 'neneneba API UUID',
  `APPLICATION_ID` varchar(20) NOT NULL,
  `TENANT_DOMAIN` varchar(255) NOT NULL,
  `HUB_CALLBACK_URL` varchar(1024) DEFAULT NULL COMMENT '集线器回拨URL',
  `HUB_TOPIC` varchar(255) DEFAULT NULL COMMENT '集线器主题',
  `HUB_SECRET` varchar(2048) DEFAULT NULL COMMENT '集线器机密',
  `HUB_LEASE_SECONDS` int(11) DEFAULT NULL COMMENT 'HUB最短秒',
  `UPDATED_AT` timestamp NULL DEFAULT NULL COMMENT '更新的AT',
  `EXPIRY_AT` bigint(20) DEFAULT NULL COMMENT 'EXPIRY AT',
  `DELIVERED_AT` timestamp NULL DEFAULT NULL,
  `DELIVERY_STATE` int(11) DEFAULT NULL COMMENT '交货状态',
  PRIMARY KEY (`WH_SUBSCRIPTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am webhooks订阅';

-- ----------------------------
-- Table structure for am_webhooks_unsubscription
-- ----------------------------
DROP TABLE IF EXISTS `am_webhooks_unsubscription`;
CREATE TABLE `am_webhooks_unsubscription` (
  `API_UUID` varchar(255) NOT NULL,
  `APPLICATION_ID` varchar(20) NOT NULL,
  `TENANT_DOMAIN` varchar(255) NOT NULL,
  `HUB_CALLBACK_URL` varchar(1024) NOT NULL,
  `HUB_TOPIC` varchar(255) NOT NULL,
  `HUB_SECRET` varchar(2048) DEFAULT NULL,
  `HUB_LEASE_SECONDS` int(11) DEFAULT NULL,
  `ADDED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for am_workflows
-- ----------------------------
DROP TABLE IF EXISTS `am_workflows`;
CREATE TABLE `am_workflows` (
  `WF_ID` int(11) NOT NULL COMMENT 'WF ID',
  `WF_REFERENCE` varchar(255) DEFAULT NULL COMMENT 'WF REFERENCE',
  `WF_TYPE` varchar(255) DEFAULT NULL COMMENT 'WF TYPE',
  `WF_STATUS` varchar(255) DEFAULT NULL COMMENT 'WF状态',
  `WF_CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'WF创建时间',
  `WF_UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'WF更新时间',
  `WF_STATUS_DESC` varchar(1000) DEFAULT NULL COMMENT 'WF状态DESC',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `TENANT_DOMAIN` varchar(255) DEFAULT NULL COMMENT '张量域',
  `WF_EXTERNAL_REFERENCE` varchar(255) DEFAULT NULL COMMENT 'WF外部参考',
  `WF_METADATA` blob COMMENT 'WF METADATA',
  `WF_PROPERTIES` blob COMMENT 'WF属性',
  PRIMARY KEY (`WF_ID`),
  UNIQUE KEY `WF_EXTERNAL_REFERENCE` (`WF_EXTERNAL_REFERENCE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='am工作流';

-- ----------------------------
-- Table structure for cm_consent_receipt_property
-- ----------------------------
DROP TABLE IF EXISTS `cm_consent_receipt_property`;
CREATE TABLE `cm_consent_receipt_property` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `CONSENT_RECEIPT_ID` varchar(255) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `VALUE` varchar(1023) DEFAULT NULL COMMENT 'VALUE',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CONSENT_RECEIPT_ID` (`CONSENT_RECEIPT_ID`,`NAME`),
  CONSTRAINT `CM_CONSENT_RECEIPT_PRT_fk0` FOREIGN KEY (`CONSENT_RECEIPT_ID`) REFERENCES `cm_receipt` (`CONSENT_RECEIPT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='cm同意接收属性';

-- ----------------------------
-- Table structure for cm_pii_category
-- ----------------------------
DROP TABLE IF EXISTS `cm_pii_category`;
CREATE TABLE `cm_pii_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `DESCRIPTION` varchar(1023) DEFAULT NULL COMMENT '描述',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `IS_SENSITIVE` int(11) DEFAULT NULL COMMENT '敏感',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`,`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='cm pii类别';

-- ----------------------------
-- Table structure for cm_purpose
-- ----------------------------
DROP TABLE IF EXISTS `cm_purpose`;
CREATE TABLE `cm_purpose` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `DESCRIPTION` varchar(1023) DEFAULT NULL,
  `PURPOSE_GROUP` varchar(255) DEFAULT NULL COMMENT '目的组',
  `GROUP_TYPE` varchar(255) DEFAULT NULL COMMENT '组类型',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`,`TENANT_ID`,`PURPOSE_GROUP`,`GROUP_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='cm用途';

-- ----------------------------
-- Table structure for cm_purpose_category
-- ----------------------------
DROP TABLE IF EXISTS `cm_purpose_category`;
CREATE TABLE `cm_purpose_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `DESCRIPTION` varchar(1023) DEFAULT NULL COMMENT '描述',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`,`TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='cm目的类别';

-- ----------------------------
-- Table structure for cm_purpose_pii_cat_assoc
-- ----------------------------
DROP TABLE IF EXISTS `cm_purpose_pii_cat_assoc`;
CREATE TABLE `cm_purpose_pii_cat_assoc` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `PURPOSE_ID` int(11) DEFAULT NULL COMMENT '目的ID',
  `CM_PII_CATEGORY_ID` int(11) DEFAULT NULL COMMENT 'cm pii类别ID',
  `IS_MANDATORY` int(11) DEFAULT NULL COMMENT '强制',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PURPOSE_ID` (`PURPOSE_ID`,`CM_PII_CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='cm目的pii猫assoc';

-- ----------------------------
-- Table structure for cm_receipt
-- ----------------------------
DROP TABLE IF EXISTS `cm_receipt`;
CREATE TABLE `cm_receipt` (
  `CONSENT_RECEIPT_ID` varchar(255) NOT NULL,
  `VERSION` varchar(255) DEFAULT NULL COMMENT '版本',
  `JURISDICTION` varchar(255) DEFAULT NULL COMMENT '管辖权',
  `CONSENT_TIMESTAMP` timestamp NOT NULL,
  `COLLECTION_METHOD` varchar(255) DEFAULT NULL COMMENT '收集方法',
  `LANGUAGE` varchar(255) DEFAULT NULL COMMENT '语言',
  `PII_PRINCIPAL_ID` varchar(255) NOT NULL,
  `PRINCIPAL_TENANT_ID` int(11) DEFAULT '-1234',
  `POLICY_URL` varchar(255) DEFAULT NULL COMMENT '策略URL',
  `STATE` varchar(255) DEFAULT NULL COMMENT 'STATE',
  `PII_CONTROLLER` varchar(2048) NOT NULL,
  PRIMARY KEY (`CONSENT_RECEIPT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='cm收据';

-- ----------------------------
-- Table structure for cm_receipt_sp_assoc
-- ----------------------------
DROP TABLE IF EXISTS `cm_receipt_sp_assoc`;
CREATE TABLE `cm_receipt_sp_assoc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONSENT_RECEIPT_ID` varchar(255) NOT NULL,
  `SP_NAME` varchar(255) DEFAULT NULL COMMENT 'sp名称',
  `SP_DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT 'sp显示名称',
  `SP_DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT 'sp描述',
  `SP_TENANT_ID` int(11) DEFAULT '-1234',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CONSENT_RECEIPT_ID` (`CONSENT_RECEIPT_ID`,`SP_NAME`,`SP_TENANT_ID`),
  CONSTRAINT `CM_RECEIPT_SP_ASSOC_fk0` FOREIGN KEY (`CONSENT_RECEIPT_ID`) REFERENCES `cm_receipt` (`CONSENT_RECEIPT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='cm收据sp assoc';

-- ----------------------------
-- Table structure for cm_sp_purpose_assoc
-- ----------------------------
DROP TABLE IF EXISTS `cm_sp_purpose_assoc`;
CREATE TABLE `cm_sp_purpose_assoc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECEIPT_SP_ASSOC` int(11) NOT NULL,
  `PURPOSE_ID` int(11) NOT NULL,
  `CONSENT_TYPE` varchar(255) NOT NULL,
  `IS_PRIMARY_PURPOSE` int(11) NOT NULL,
  `TERMINATION` varchar(255) NOT NULL,
  `THIRD_PARTY_DISCLOSURE` int(11) NOT NULL,
  `THIRD_PARTY_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RECEIPT_SP_ASSOC` (`RECEIPT_SP_ASSOC`,`PURPOSE_ID`),
  KEY `CM_SP_PURPOSE_ASSOC_fk1` (`PURPOSE_ID`),
  CONSTRAINT `CM_SP_PURPOSE_ASSOC_fk0` FOREIGN KEY (`RECEIPT_SP_ASSOC`) REFERENCES `cm_receipt_sp_assoc` (`ID`),
  CONSTRAINT `CM_SP_PURPOSE_ASSOC_fk1` FOREIGN KEY (`PURPOSE_ID`) REFERENCES `cm_purpose` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for cm_sp_purpose_pii_cat_assoc
-- ----------------------------
DROP TABLE IF EXISTS `cm_sp_purpose_pii_cat_assoc`;
CREATE TABLE `cm_sp_purpose_pii_cat_assoc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SP_PURPOSE_ASSOC_ID` int(11) DEFAULT NULL COMMENT 'sp目的关联ID',
  `PII_CATEGORY_ID` int(11) NOT NULL,
  `VALIDITY` varchar(1023) DEFAULT NULL,
  `IS_CONSENTED` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SP_PURPOSE_ASSOC_ID` (`SP_PURPOSE_ASSOC_ID`,`PII_CATEGORY_ID`),
  KEY `CM_SP_P_PII_CAT_ASSOC_fk1` (`PII_CATEGORY_ID`),
  CONSTRAINT `CM_SP_P_PII_CAT_ASSOC_fk0` FOREIGN KEY (`SP_PURPOSE_ASSOC_ID`) REFERENCES `cm_sp_purpose_assoc` (`ID`),
  CONSTRAINT `CM_SP_P_PII_CAT_ASSOC_fk1` FOREIGN KEY (`PII_CATEGORY_ID`) REFERENCES `cm_pii_category` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='cm sp目的pii猫assoc';

-- ----------------------------
-- Table structure for cm_sp_purpose_purpose_cat_assc
-- ----------------------------
DROP TABLE IF EXISTS `cm_sp_purpose_purpose_cat_assc`;
CREATE TABLE `cm_sp_purpose_purpose_cat_assc` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SP_PURPOSE_ASSOC_ID` int(11) NOT NULL,
  `PURPOSE_CATEGORY_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SP_PURPOSE_ASSOC_ID` (`SP_PURPOSE_ASSOC_ID`,`PURPOSE_CATEGORY_ID`),
  KEY `CM_SP_P_P_CAT_ASSOC_fk1` (`PURPOSE_CATEGORY_ID`),
  CONSTRAINT `CM_SP_P_P_CAT_ASSOC_fk0` FOREIGN KEY (`SP_PURPOSE_ASSOC_ID`) REFERENCES `cm_sp_purpose_assoc` (`ID`),
  CONSTRAINT `CM_SP_P_P_CAT_ASSOC_fk1` FOREIGN KEY (`PURPOSE_CATEGORY_ID`) REFERENCES `cm_purpose_category` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for fido_device_store
-- ----------------------------
DROP TABLE IF EXISTS `fido_device_store`;
CREATE TABLE `fido_device_store` (
  `TENANT_ID` int(11) NOT NULL,
  `DOMAIN_NAME` varchar(255) NOT NULL COMMENT '域名',
  `USER_NAME` varchar(45) NOT NULL COMMENT '用户名',
  `TIME_REGISTERED` timestamp NULL DEFAULT NULL COMMENT '注册时间',
  `KEY_HANDLE` varchar(200) NOT NULL COMMENT '钥匙手柄',
  `DEVICE_DATA` varchar(2048) NOT NULL,
  PRIMARY KEY (`TENANT_ID`,`DOMAIN_NAME`,`USER_NAME`,`KEY_HANDLE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='fido设备存储';

-- ----------------------------
-- Table structure for fido2_device_store
-- ----------------------------
DROP TABLE IF EXISTS `fido2_device_store`;
CREATE TABLE `fido2_device_store` (
  `TENANT_ID` int(11) DEFAULT NULL,
  `DOMAIN_NAME` varchar(255) DEFAULT NULL COMMENT '域名',
  `USER_NAME` varchar(45) DEFAULT NULL COMMENT '用户名',
  `TIME_REGISTERED` timestamp NULL DEFAULT NULL,
  `USER_HANDLE` varchar(64) NOT NULL COMMENT '用户句柄',
  `CREDENTIAL_ID` varchar(200) NOT NULL,
  `PUBLIC_KEY_COSE` varchar(1024) NOT NULL,
  `SIGNATURE_COUNT` bigint(20) DEFAULT NULL COMMENT '签名计数',
  `USER_IDENTITY` varchar(512) DEFAULT NULL COMMENT '用户标识',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `IS_USERNAMELESS_SUPPORTED` char(1) DEFAULT NULL COMMENT '是否支持USERNAMELESS',
  PRIMARY KEY (`CREDENTIAL_ID`,`USER_HANDLE`),
  KEY `IDX_FIDO2_STR` (`USER_NAME`,`TENANT_ID`,`DOMAIN_NAME`,`CREDENTIAL_ID`,`USER_HANDLE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='fido2设备存储';

-- ----------------------------
-- Table structure for idn_associated_id
-- ----------------------------
DROP TABLE IF EXISTS `idn_associated_id`;
CREATE TABLE `idn_associated_id` (
  `ID` int(11) NOT NULL COMMENT 'id',
  `IDP_USER_ID` varchar(255) DEFAULT NULL COMMENT 'IDP用户id',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户id',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'IDP id',
  `DOMAIN_NAME` varchar(255) DEFAULT NULL COMMENT '域名',
  `USER_NAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  `ASSOCIATION_ID` char(36) DEFAULT NULL COMMENT '关联id',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDP_USER_ID` (`IDP_USER_ID`,`TENANT_ID`,`IDP_ID`),
  KEY `IDP_ID` (`IDP_ID`),
  KEY `IDX_AI_DN_UN_AI` (`DOMAIN_NAME`,`USER_NAME`,`ASSOCIATION_ID`),
  CONSTRAINT `idn_associated_id_ibfk_1` FOREIGN KEY (`IDP_ID`) REFERENCES `idp` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn关联id';

-- ----------------------------
-- Table structure for idn_auth_session_app_info
-- ----------------------------
DROP TABLE IF EXISTS `idn_auth_session_app_info`;
CREATE TABLE `idn_auth_session_app_info` (
  `SESSION_ID` varchar(100) NOT NULL,
  `SUBJECT` varchar(100) NOT NULL COMMENT '主题',
  `APP_ID` int(11) NOT NULL COMMENT '应用程序ID',
  `INBOUND_AUTH_TYPE` varchar(255) NOT NULL COMMENT 'INBOUND auth TYPE',
  PRIMARY KEY (`SESSION_ID`,`SUBJECT`,`APP_ID`,`INBOUND_AUTH_TYPE`),
  KEY `IDX_AUTH_SAI_UN_AID_SID` (`APP_ID`,`SUBJECT`,`SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-auth会话应用程序信息';

-- ----------------------------
-- Table structure for idn_auth_session_meta_data
-- ----------------------------
DROP TABLE IF EXISTS `idn_auth_session_meta_data`;
CREATE TABLE `idn_auth_session_meta_data` (
  `SESSION_ID` varchar(100) NOT NULL,
  `PROPERTY_TYPE` varchar(100) NOT NULL COMMENT '属性类型',
  `VALUE` varchar(255) NOT NULL COMMENT 'VALUE',
  PRIMARY KEY (`SESSION_ID`,`PROPERTY_TYPE`,`VALUE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-auth会话元数据';

-- ----------------------------
-- Table structure for idn_auth_session_store
-- ----------------------------
DROP TABLE IF EXISTS `idn_auth_session_store`;
CREATE TABLE `idn_auth_session_store` (
  `SESSION_ID` varchar(100) NOT NULL,
  `SESSION_TYPE` varchar(100) NOT NULL,
  `OPERATION` varchar(10) NOT NULL COMMENT '操作',
  `SESSION_OBJECT` blob,
  `TIME_CREATED` bigint(20) NOT NULL COMMENT '创建的时间',
  `TENANT_ID` int(11) DEFAULT '-1',
  `EXPIRY_TIME` bigint(20) DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`SESSION_ID`,`SESSION_TYPE`,`TIME_CREATED`,`OPERATION`),
  KEY `IDX_IDN_AUTH_SESSION_TIME` (`TIME_CREATED`),
  KEY `IDX_IDN_AUTH_SSTR_ST_OP_ID_TM` (`OPERATION`,`SESSION_TYPE`,`SESSION_ID`,`TIME_CREATED`),
  KEY `IDX_IDN_AUTH_SSTR_ET_ID` (`EXPIRY_TIME`,`SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn身份验证会话存储';

-- ----------------------------
-- Table structure for idn_auth_temp_session_store
-- ----------------------------
DROP TABLE IF EXISTS `idn_auth_temp_session_store`;
CREATE TABLE `idn_auth_temp_session_store` (
  `SESSION_ID` varchar(100) NOT NULL,
  `SESSION_TYPE` varchar(100) NOT NULL,
  `OPERATION` varchar(10) NOT NULL COMMENT '操作',
  `SESSION_OBJECT` blob,
  `TIME_CREATED` bigint(20) NOT NULL COMMENT '创建的时间',
  `TENANT_ID` int(11) DEFAULT '-1',
  `EXPIRY_TIME` bigint(20) DEFAULT NULL COMMENT 'EXPIRY TIME',
  PRIMARY KEY (`SESSION_ID`,`SESSION_TYPE`,`TIME_CREATED`,`OPERATION`),
  KEY `IDX_IDN_AUTH_TMP_SESSION_TIME` (`TIME_CREATED`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-auth临时会话存储';

-- ----------------------------
-- Table structure for idn_auth_user
-- ----------------------------
DROP TABLE IF EXISTS `idn_auth_user`;
CREATE TABLE `idn_auth_user` (
  `USER_ID` varchar(255) NOT NULL COMMENT '用户ID',
  `USER_NAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  `TENANT_ID` int(11) NOT NULL,
  `DOMAIN_NAME` varchar(255) DEFAULT NULL COMMENT '域名',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'IDP ID',
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `USER_STORE_CONSTRAINT` (`USER_NAME`,`TENANT_ID`,`DOMAIN_NAME`,`IDP_ID`),
  KEY `IDX_AUTH_USER_UN_TID_DN` (`USER_NAME`,`TENANT_ID`,`DOMAIN_NAME`),
  KEY `IDX_AUTH_USER_DN_TOD` (`DOMAIN_NAME`,`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-auth-user';

-- ----------------------------
-- Table structure for idn_auth_user_session_mapping
-- ----------------------------
DROP TABLE IF EXISTS `idn_auth_user_session_mapping`;
CREATE TABLE `idn_auth_user_session_mapping` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` varchar(255) NOT NULL,
  `SESSION_ID` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USER_SESSION_STORE_CONSTRAINT` (`USER_ID`,`SESSION_ID`),
  KEY `IDX_USER_ID` (`USER_ID`),
  KEY `IDX_SESSION_ID` (`SESSION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for idn_auth_wait_status
-- ----------------------------
DROP TABLE IF EXISTS `idn_auth_wait_status`;
CREATE TABLE `idn_auth_wait_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) NOT NULL,
  `LONG_WAIT_KEY` varchar(255) DEFAULT NULL COMMENT '长等待密钥',
  `WAIT_STATUS` char(1) DEFAULT NULL COMMENT '等待状态',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `EXPIRE_TIME` timestamp NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDN_AUTH_WAIT_STATUS_KEY` (`LONG_WAIT_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn身份验证等待状态';

-- ----------------------------
-- Table structure for idn_base_table
-- ----------------------------
DROP TABLE IF EXISTS `idn_base_table`;
CREATE TABLE `idn_base_table` (
  `PRODUCT_NAME` varchar(20) NOT NULL COMMENT '产品名称',
  PRIMARY KEY (`PRODUCT_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn基表';

-- ----------------------------
-- Table structure for idn_certificate
-- ----------------------------
DROP TABLE IF EXISTS `idn_certificate`;
CREATE TABLE `idn_certificate` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `NAME` varchar(100) DEFAULT NULL COMMENT '名称',
  `CERTIFICATE_IN_PEM` blob COMMENT 'PEM证书',
  `TENANT_ID` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CERTIFICATE_UNIQUE_KEY` (`NAME`,`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn证书';

-- ----------------------------
-- Table structure for idn_claim
-- ----------------------------
DROP TABLE IF EXISTS `idn_claim`;
CREATE TABLE `idn_claim` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DIALECT_ID` int(11) NOT NULL,
  `CLAIM_URI` varchar(255) NOT NULL,
  `TENANT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CLAIM_URI_CONSTRAINT` (`DIALECT_ID`,`CLAIM_URI`,`TENANT_ID`),
  CONSTRAINT `idn_claim_ibfk_1` FOREIGN KEY (`DIALECT_ID`) REFERENCES `idn_claim_dialect` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=330 DEFAULT CHARSET=latin1 COMMENT='idn索赔';

-- ----------------------------
-- Table structure for idn_claim_dialect
-- ----------------------------
DROP TABLE IF EXISTS `idn_claim_dialect`;
CREATE TABLE `idn_claim_dialect` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DIALECT_URI` varchar(255) DEFAULT NULL COMMENT 'dialect URI',
  `TENANT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DIALECT_URI_CONSTRAINT` (`DIALECT_URI`,`TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='idn索赔方言';

-- ----------------------------
-- Table structure for idn_claim_mapped_attribute
-- ----------------------------
DROP TABLE IF EXISTS `idn_claim_mapped_attribute`;
CREATE TABLE `idn_claim_mapped_attribute` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOCAL_CLAIM_ID` int(11) DEFAULT NULL,
  `USER_STORE_DOMAIN_NAME` varchar(255) DEFAULT NULL COMMENT '用户存储域名',
  `ATTRIBUTE_NAME` varchar(255) DEFAULT NULL COMMENT '属性名称',
  `TENANT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USER_STORE_DOMAIN_CONSTRAINT` (`LOCAL_CLAIM_ID`,`USER_STORE_DOMAIN_NAME`,`TENANT_ID`),
  CONSTRAINT `idn_claim_mapped_attribute_ibfk_1` FOREIGN KEY (`LOCAL_CLAIM_ID`) REFERENCES `idn_claim` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1 COMMENT='idn索赔映射的属性';

-- ----------------------------
-- Table structure for idn_claim_mapping
-- ----------------------------
DROP TABLE IF EXISTS `idn_claim_mapping`;
CREATE TABLE `idn_claim_mapping` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `EXT_CLAIM_ID` int(11) DEFAULT NULL COMMENT 'EXT索赔ID',
  `MAPPED_LOCAL_CLAIM_ID` int(11) DEFAULT NULL COMMENT '映射的本地索赔ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EXT_TO_LOC_MAPPING_CONSTRN` (`EXT_CLAIM_ID`,`TENANT_ID`),
  KEY `MAPPED_LOCAL_CLAIM_ID` (`MAPPED_LOCAL_CLAIM_ID`),
  CONSTRAINT `idn_claim_mapping_ibfk_1` FOREIGN KEY (`EXT_CLAIM_ID`) REFERENCES `idn_claim` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `idn_claim_mapping_ibfk_2` FOREIGN KEY (`MAPPED_LOCAL_CLAIM_ID`) REFERENCES `idn_claim` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn索赔映射';

-- ----------------------------
-- Table structure for idn_claim_property
-- ----------------------------
DROP TABLE IF EXISTS `idn_claim_property`;
CREATE TABLE `idn_claim_property` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `LOCAL_CLAIM_ID` int(11) DEFAULT NULL,
  `PROPERTY_NAME` varchar(255) DEFAULT NULL COMMENT '财产名称',
  `PROPERTY_VALUE` varchar(255) DEFAULT NULL COMMENT '财产价值',
  `TENANT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PROPERTY_NAME_CONSTRAINT` (`LOCAL_CLAIM_ID`,`PROPERTY_NAME`,`TENANT_ID`),
  CONSTRAINT `idn_claim_property_ibfk_1` FOREIGN KEY (`LOCAL_CLAIM_ID`) REFERENCES `idn_claim` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn索赔属性';

-- ----------------------------
-- Table structure for idn_config_attribute
-- ----------------------------
DROP TABLE IF EXISTS `idn_config_attribute`;
CREATE TABLE `idn_config_attribute` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `RESOURCE_ID` varchar(255) DEFAULT NULL COMMENT 'RESOURCE ID',
  `ATTR_KEY` varchar(255) DEFAULT NULL COMMENT 'ATTR KEY',
  `ATTR_VALUE` varchar(1023) DEFAULT NULL COMMENT '属性值',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RESOURCE_KEY_VAL_CONSTRAINT` (`RESOURCE_ID`(64),`ATTR_KEY`),
  KEY `RESOURCE_ID_ATTRIBUTE_FOREIGN_CONSTRAINT` (`RESOURCE_ID`),
  CONSTRAINT `RESOURCE_ID_ATTRIBUTE_FOREIGN_CONSTRAINT` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `idn_config_resource` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-config属性';

-- ----------------------------
-- Table structure for idn_config_
-- ----------------------------
DROP TABLE IF EXISTS `idn_config_`;
CREATE TABLE `idn_config_` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `VALUE` blob COMMENT 'VALUE',
  `RESOURCE_ID` varchar(255) DEFAULT NULL COMMENT '资源ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`ID`),
  KEY `RESOURCE_ID__FOREIGN_CONSTRAINT` (`RESOURCE_ID`),
  CONSTRAINT `RESOURCE_ID__FOREIGN_CONSTRAINT` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `idn_config_resource` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn配置文件';

-- ----------------------------
-- Table structure for idn_config_resource
-- ----------------------------
DROP TABLE IF EXISTS `idn_config_resource`;
CREATE TABLE `idn_config_resource` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `LAST_MODIFIED` timestamp NULL DEFAULT NULL COMMENT '上次修改',
  `HAS_` tinyint(1) DEFAULT NULL COMMENT 'HAS文件',
  `HAS_ATTRIBUTE` tinyint(1) DEFAULT NULL COMMENT 'HAS属性',
  `TYPE_ID` varchar(255) DEFAULT NULL COMMENT '类型ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME_TENANT_TYPE_CONSTRAINT` (`NAME`,`TENANT_ID`,`TYPE_ID`),
  KEY `TYPE_ID_FOREIGN_CONSTRAINT` (`TYPE_ID`),
  CONSTRAINT `TYPE_ID_FOREIGN_CONSTRAINT` FOREIGN KEY (`TYPE_ID`) REFERENCES `idn_config_type` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn配置资源';

-- ----------------------------
-- Table structure for idn_config_type
-- ----------------------------
DROP TABLE IF EXISTS `idn_config_type`;
CREATE TABLE `idn_config_type` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `DESCRIPTION` varchar(1023) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TYPE_NAME_CONSTRAINT` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn配置类型';

-- ----------------------------
-- Table structure for idn_cors_association
-- ----------------------------
DROP TABLE IF EXISTS `idn_cors_association`;
CREATE TABLE `idn_cors_association` (
  `IDN_CORS_ORIGIN_ID` int(11) NOT NULL COMMENT 'idn cors原始ID',
  `SP_APP_ID` int(11) NOT NULL COMMENT 'SP应用程序ID',
  PRIMARY KEY (`IDN_CORS_ORIGIN_ID`,`SP_APP_ID`),
  KEY `IDX_CORS_SP_APP_ID` (`SP_APP_ID`),
  KEY `IDX_CORS_ORIGIN_ID` (`IDN_CORS_ORIGIN_ID`),
  CONSTRAINT `idn_cors_association_ibfk_1` FOREIGN KEY (`IDN_CORS_ORIGIN_ID`) REFERENCES `idn_cors_origin` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `idn_cors_association_ibfk_2` FOREIGN KEY (`SP_APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-cors关联';

-- ----------------------------
-- Table structure for idn_cors_origin
-- ----------------------------
DROP TABLE IF EXISTS `idn_cors_origin`;
CREATE TABLE `idn_cors_origin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) NOT NULL,
  `ORIGIN` varchar(2048) DEFAULT NULL COMMENT '起源',
  `UUID` char(36) DEFAULT NULL COMMENT 'UUID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UUID` (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-cors原点';

-- ----------------------------
-- Table structure for idn_fed_auth_session_mapping
-- ----------------------------
DROP TABLE IF EXISTS `idn_fed_auth_session_mapping`;
CREATE TABLE `idn_fed_auth_session_mapping` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDP_SESSION_ID` varchar(255) NOT NULL,
  `SESSION_ID` varchar(255) NOT NULL,
  `IDP_NAME` varchar(255) NOT NULL,
  `AUTHENTICATOR_ID` varchar(255) DEFAULT NULL,
  `PROTOCOL_TYPE` varchar(255) DEFAULT NULL,
  `TIME_CREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TENANT_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDP_SESSION_ID` (`IDP_SESSION_ID`,`TENANT_ID`),
  KEY `IDX_FEDERATED_AUTH_SESSION_ID` (`SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for idn_function_library
-- ----------------------------
DROP TABLE IF EXISTS `idn_function_library`;
CREATE TABLE `idn_function_library` (
  `NAME` varchar(255) NOT NULL COMMENT 'NAME',
  `DESCRIPTION` varchar(1023) DEFAULT NULL COMMENT '描述',
  `TYPE` varchar(255) DEFAULT NULL COMMENT 'TYPE',
  `TENANT_ID` int(11) NOT NULL,
  `DATA` blob COMMENT '数据',
  PRIMARY KEY (`TENANT_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn函数库';

-- ----------------------------
-- Table structure for idn_identity_meta_data
-- ----------------------------
DROP TABLE IF EXISTS `idn_identity_meta_data`;
CREATE TABLE `idn_identity_meta_data` (
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  `TENANT_ID` int(11) NOT NULL COMMENT '租户ID',
  `METADATA_TYPE` varchar(255) NOT NULL COMMENT '元数据类型',
  `METADATA` varchar(255) NOT NULL COMMENT '元数据',
  `VALID` varchar(255) DEFAULT NULL COMMENT '有效',
  PRIMARY KEY (`TENANT_ID`,`USER_NAME`,`METADATA_TYPE`,`METADATA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn标识元数据';

-- ----------------------------
-- Table structure for idn_identity_user_data
-- ----------------------------
DROP TABLE IF EXISTS `idn_identity_user_data`;
CREATE TABLE `idn_identity_user_data` (
  `TENANT_ID` int(11) NOT NULL COMMENT '租户ID',
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  `DATA_KEY` varchar(255) NOT NULL COMMENT '数据密钥',
  `DATA_VALUE` varchar(2048) DEFAULT NULL COMMENT '数据值',
  PRIMARY KEY (`TENANT_ID`,`USER_NAME`,`DATA_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn标识用户数据';

-- ----------------------------
-- Table structure for idn_oauth_consumer_apps
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth_consumer_apps`;
CREATE TABLE `idn_oauth_consumer_apps` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONSUMER_KEY` varchar(255) DEFAULT NULL COMMENT '消费者密钥',
  `CONSUMER_SECRET` varchar(2048) DEFAULT NULL COMMENT '消费者机密',
  `USERNAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `USER_DOMAIN` varchar(50) DEFAULT NULL COMMENT '用户域',
  `APP_NAME` varchar(255) DEFAULT NULL COMMENT '应用程序名称',
  `OAUTH_VERSION` varchar(128) DEFAULT NULL COMMENT 'oauth VERSION',
  `CALLBACK_URL` varchar(2048) DEFAULT NULL COMMENT 'CALLBACK URL',
  `GRANT_TYPES` varchar(1024) DEFAULT NULL COMMENT 'GRANT TYPES',
  `PKCE_MANDATORY` char(1) DEFAULT NULL COMMENT 'PKCE强制',
  `PKCE_SUPPORT_PLAIN` char(1) DEFAULT NULL COMMENT 'PKCE SUPPORT PLAIN',
  `APP_STATE` varchar(25) DEFAULT NULL COMMENT '应用程序状态',
  `USER_ACCESS_TOKEN_EXPIRE_TIME` bigint(20) DEFAULT NULL COMMENT '用户访问令牌到期时间',
  `APP_ACCESS_TOKEN_EXPIRE_TIME` bigint(20) DEFAULT NULL COMMENT '应用程序访问令牌到期时间',
  `REFRESH_TOKEN_EXPIRE_TIME` bigint(20) DEFAULT NULL COMMENT '刷新代币到期时间',
  `ID_TOKEN_EXPIRE_TIME` bigint(20) DEFAULT NULL COMMENT 'ID代币到期时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CONSUMER_KEY_CONSTRAINT` (`CONSUMER_KEY`),
  KEY `IDX_OCA_UM_TID_UD_APN` (`USERNAME`,`TENANT_ID`,`USER_DOMAIN`,`APP_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='idn-oauth消费者应用';

-- ----------------------------
-- Table structure for idn_oauth1a_access_token
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth1a_access_token`;
CREATE TABLE `idn_oauth1a_access_token` (
  `ACCESS_TOKEN` varchar(255) NOT NULL COMMENT '访问令牌',
  `ACCESS_TOKEN_SECRET` varchar(512) DEFAULT NULL COMMENT '访问令牌机密',
  `CONSUMER_KEY_ID` int(11) DEFAULT NULL,
  `SCOPE` varchar(2048) DEFAULT NULL COMMENT '范围',
  `AUTHZ_USER` varchar(512) DEFAULT NULL COMMENT 'AUTHZ用户',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`ACCESS_TOKEN`),
  KEY `CONSUMER_KEY_ID` (`CONSUMER_KEY_ID`),
  CONSTRAINT `idn_oauth1a_access_token_ibfk_1` FOREIGN KEY (`CONSUMER_KEY_ID`) REFERENCES `idn_oauth_consumer_apps` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth1a访问令牌';

-- ----------------------------
-- Table structure for idn_oauth1a_request_token
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth1a_request_token`;
CREATE TABLE `idn_oauth1a_request_token` (
  `REQUEST_TOKEN` varchar(255) NOT NULL COMMENT '请求令牌',
  `REQUEST_TOKEN_SECRET` varchar(512) DEFAULT NULL COMMENT '请求令牌机密',
  `CONSUMER_KEY_ID` int(11) DEFAULT NULL,
  `CALLBACK_URL` varchar(2048) DEFAULT NULL COMMENT '回拨URL',
  `SCOPE` varchar(2048) DEFAULT NULL COMMENT '范围',
  `AUTHORIZED` varchar(128) DEFAULT NULL COMMENT '授权',
  `OAUTH_VERIFIER` varchar(512) DEFAULT NULL COMMENT 'OAUTH VERIFIER',
  `AUTHZ_USER` varchar(512) DEFAULT NULL COMMENT 'AUTHZ用户',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`REQUEST_TOKEN`),
  KEY `CONSUMER_KEY_ID` (`CONSUMER_KEY_ID`),
  CONSTRAINT `idn_oauth1a_request_token_ibfk_1` FOREIGN KEY (`CONSUMER_KEY_ID`) REFERENCES `idn_oauth_consumer_apps` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth1a请求令牌';

-- ----------------------------
-- Table structure for idn_oauth2_access_token
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_access_token`;
CREATE TABLE `idn_oauth2_access_token` (
  `TOKEN_ID` varchar(255) NOT NULL COMMENT '代币ID',
  `ACCESS_TOKEN` varchar(2048) DEFAULT NULL COMMENT '访问令牌',
  `REFRESH_TOKEN` varchar(2048) DEFAULT NULL COMMENT '刷新令牌',
  `CONSUMER_KEY_ID` int(11) DEFAULT NULL,
  `AUTHZ_USER` varchar(100) DEFAULT NULL COMMENT 'AUTHZ USER',
  `TENANT_ID` int(11) DEFAULT NULL,
  `USER_DOMAIN` varchar(50) DEFAULT NULL COMMENT '用户域',
  `USER_TYPE` varchar(25) DEFAULT NULL COMMENT '用户类型',
  `GRANT_TYPE` varchar(50) DEFAULT NULL COMMENT 'GRANT TYPE',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `REFRESH_TOKEN_TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的REFRESH token时间',
  `VALIDITY_PERIOD` bigint(20) DEFAULT NULL COMMENT '有效期',
  `REFRESH_TOKEN_VALIDITY_PERIOD` bigint(20) DEFAULT NULL COMMENT '刷新令牌有效期',
  `TOKEN_SCOPE_HASH` varchar(32) DEFAULT NULL COMMENT '代币范围HASH',
  `TOKEN_STATE` varchar(25) DEFAULT NULL COMMENT '令牌状态',
  `TOKEN_STATE_ID` varchar(128) DEFAULT NULL COMMENT '令牌状态ID',
  `SUBJECT_IDENTIFIER` varchar(255) DEFAULT NULL COMMENT '主题标识符',
  `ACCESS_TOKEN_HASH` varchar(512) DEFAULT NULL COMMENT '访问令牌哈希',
  `REFRESH_TOKEN_HASH` varchar(512) DEFAULT NULL COMMENT 'REFRESH token HASH',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'IDP ID',
  `TOKEN_BINDING_REF` varchar(32) DEFAULT NULL COMMENT '代币绑定参考',
  `CONSENTED_TOKEN` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`TOKEN_ID`),
  UNIQUE KEY `CON_APP_KEY` (`CONSUMER_KEY_ID`,`AUTHZ_USER`,`TENANT_ID`,`USER_DOMAIN`,`USER_TYPE`,`TOKEN_SCOPE_HASH`,`TOKEN_STATE`,`TOKEN_STATE_ID`,`IDP_ID`,`TOKEN_BINDING_REF`),
  KEY `IDX_TC` (`TIME_CREATED`),
  KEY `IDX_ATH` (`ACCESS_TOKEN_HASH`),
  KEY `IDX_AT_TI_UD` (`AUTHZ_USER`,`TENANT_ID`,`TOKEN_STATE`,`USER_DOMAIN`),
  KEY `IDX_AT_RTH` (`REFRESH_TOKEN_HASH`),
  KEY `IDX_AT_CKID_AU_TID_UD_TSH_TS` (`CONSUMER_KEY_ID`,`AUTHZ_USER`,`TENANT_ID`,`USER_DOMAIN`,`TOKEN_SCOPE_HASH`,`TOKEN_STATE`),
  KEY `IDX_IOAT_UT` (`USER_TYPE`),
  CONSTRAINT `idn_oauth2_access_token_ibfk_1` FOREIGN KEY (`CONSUMER_KEY_ID`) REFERENCES `idn_oauth_consumer_apps` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2访问令牌';

-- ----------------------------
-- Table structure for idn_oauth2_access_token_audit
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_access_token_audit`;
CREATE TABLE `idn_oauth2_access_token_audit` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TOKEN_ID` varchar(255) DEFAULT NULL COMMENT 'token ID',
  `ACCESS_TOKEN` varchar(2048) DEFAULT NULL COMMENT '访问令牌',
  `REFRESH_TOKEN` varchar(2048) DEFAULT NULL COMMENT '刷新令牌',
  `CONSUMER_KEY_ID` int(11) DEFAULT NULL,
  `AUTHZ_USER` varchar(100) DEFAULT NULL COMMENT 'AUTHZ USER',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `USER_DOMAIN` varchar(50) DEFAULT NULL COMMENT '用户域',
  `USER_TYPE` varchar(25) DEFAULT NULL COMMENT '用户类型',
  `GRANT_TYPE` varchar(50) DEFAULT NULL COMMENT 'GRANT TYPE',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `REFRESH_TOKEN_TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的REFRESH token时间',
  `VALIDITY_PERIOD` bigint(20) DEFAULT NULL,
  `REFRESH_TOKEN_VALIDITY_PERIOD` bigint(20) DEFAULT NULL COMMENT '刷新令牌有效期',
  `TOKEN_SCOPE_HASH` varchar(32) DEFAULT NULL COMMENT 'token范围HASH',
  `TOKEN_STATE` varchar(25) DEFAULT NULL COMMENT '令牌状态',
  `TOKEN_STATE_ID` varchar(128) DEFAULT NULL COMMENT '令牌状态ID',
  `SUBJECT_IDENTIFIER` varchar(255) DEFAULT NULL COMMENT '主题标识符',
  `ACCESS_TOKEN_HASH` varchar(512) DEFAULT NULL COMMENT '访问令牌哈希',
  `REFRESH_TOKEN_HASH` varchar(512) DEFAULT NULL COMMENT 'REFRESH token HASH',
  `INVALIDATED_TIME` timestamp NULL DEFAULT NULL COMMENT '无效时间',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'IDP ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2访问令牌审核';

-- ----------------------------
-- Table structure for idn_oauth2_access_token_scope
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_access_token_scope`;
CREATE TABLE `idn_oauth2_access_token_scope` (
  `TOKEN_ID` varchar(255) NOT NULL COMMENT '代币ID',
  `TOKEN_SCOPE` varchar(100) NOT NULL COMMENT '令牌范围',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`TOKEN_ID`,`TOKEN_SCOPE`),
  KEY `IDX_ATS_TID` (`TOKEN_ID`),
  CONSTRAINT `idn_oauth2_access_token_scope_ibfk_1` FOREIGN KEY (`TOKEN_ID`) REFERENCES `idn_oauth2_access_token` (`TOKEN_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2访问令牌作用域';

-- ----------------------------
-- Table structure for idn_oauth2_authorization_code
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_authorization_code`;
CREATE TABLE `idn_oauth2_authorization_code` (
  `CODE_ID` varchar(255) NOT NULL COMMENT '代码ID',
  `AUTHORIZATION_CODE` varchar(2048) DEFAULT NULL COMMENT '授权代码',
  `CONSUMER_KEY_ID` int(11) DEFAULT NULL,
  `CALLBACK_URL` varchar(2048) DEFAULT NULL COMMENT 'CALLBACK URL',
  `SCOPE` varchar(2048) DEFAULT NULL COMMENT '范围',
  `AUTHZ_USER` varchar(100) DEFAULT NULL COMMENT 'AUTHZ USER',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `USER_DOMAIN` varchar(50) DEFAULT NULL COMMENT '用户域',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `VALIDITY_PERIOD` bigint(20) DEFAULT NULL COMMENT '有效期',
  `STATE` varchar(25) DEFAULT NULL COMMENT 'STATE',
  `TOKEN_ID` varchar(255) DEFAULT NULL COMMENT 'TOKEN ID',
  `SUBJECT_IDENTIFIER` varchar(255) DEFAULT NULL COMMENT '主题标识符',
  `PKCE_CODE_CHALLENGE` varchar(255) DEFAULT NULL COMMENT 'PKCE代码挑战',
  `PKCE_CODE_CHALLENGE_METHOD` varchar(128) DEFAULT NULL COMMENT 'PKCE代码挑战方法',
  `AUTHORIZATION_CODE_HASH` varchar(512) DEFAULT NULL COMMENT '授权代码HASH',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'IDP ID',
  PRIMARY KEY (`CODE_ID`),
  KEY `IDX_AUTHORIZATION_CODE_HASH` (`AUTHORIZATION_CODE_HASH`,`CONSUMER_KEY_ID`),
  KEY `IDX_AUTHORIZATION_CODE_AU_TI` (`AUTHZ_USER`,`TENANT_ID`,`USER_DOMAIN`,`STATE`),
  KEY `IDX_AC_CKID` (`CONSUMER_KEY_ID`),
  KEY `IDX_AC_TID` (`TOKEN_ID`),
  CONSTRAINT `idn_oauth2_authorization_code_ibfk_1` FOREIGN KEY (`CONSUMER_KEY_ID`) REFERENCES `idn_oauth_consumer_apps` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2授权代码';

-- ----------------------------
-- Table structure for idn_oauth2_authz_code_scope
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_authz_code_scope`;
CREATE TABLE `idn_oauth2_authz_code_scope` (
  `CODE_ID` varchar(255) NOT NULL COMMENT '代码ID',
  `SCOPE` varchar(60) NOT NULL,
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`CODE_ID`,`SCOPE`),
  CONSTRAINT `idn_oauth2_authz_code_scope_ibfk_1` FOREIGN KEY (`CODE_ID`) REFERENCES `idn_oauth2_authorization_code` (`CODE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2 authz代码范围';

-- ----------------------------
-- Table structure for idn_oauth2_ciba_auth_code
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_ciba_auth_code`;
CREATE TABLE `idn_oauth2_ciba_auth_code` (
  `AUTH_CODE_KEY` char(36) NOT NULL COMMENT '验证码密钥',
  `AUTH_REQ_ID` char(36) DEFAULT NULL COMMENT 'auth REQ ID',
  `ISSUED_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CONSUMER_KEY` varchar(255) DEFAULT NULL,
  `LAST_POLLED_TIME` timestamp NULL DEFAULT NULL COMMENT '上次轮询时间',
  `POLLING_INTERVAL` int(11) DEFAULT NULL COMMENT '轮询间隔',
  `EXPIRES_IN` int(11) DEFAULT NULL COMMENT 'EXPIRES IN',
  `AUTHENTICATED_USER_NAME` varchar(255) DEFAULT NULL COMMENT '已验证用户名',
  `USER_STORE_DOMAIN` varchar(100) DEFAULT NULL COMMENT '用户存储域',
  `TENANT_ID` int(11) DEFAULT NULL,
  `AUTH_REQ_STATUS` varchar(100) DEFAULT NULL COMMENT 'auth REQ STATUS',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'IDP ID',
  PRIMARY KEY (`AUTH_CODE_KEY`),
  UNIQUE KEY `AUTH_REQ_ID` (`AUTH_REQ_ID`),
  KEY `CONSUMER_KEY` (`CONSUMER_KEY`),
  CONSTRAINT `idn_oauth2_ciba_auth_code_ibfk_1` FOREIGN KEY (`CONSUMER_KEY`) REFERENCES `idn_oauth_consumer_apps` (`CONSUMER_KEY`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2-ciba身份验证代码';

-- ----------------------------
-- Table structure for idn_oauth2_ciba_request_scopes
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_ciba_request_scopes`;
CREATE TABLE `idn_oauth2_ciba_request_scopes` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `AUTH_CODE_KEY` char(36) DEFAULT NULL COMMENT '验证代码密钥',
  `SCOPE` varchar(255) DEFAULT NULL COMMENT '范围',
  PRIMARY KEY (`ID`),
  KEY `AUTH_CODE_KEY` (`AUTH_CODE_KEY`),
  CONSTRAINT `idn_oauth2_ciba_request_scopes_ibfk_1` FOREIGN KEY (`AUTH_CODE_KEY`) REFERENCES `idn_oauth2_ciba_auth_code` (`AUTH_CODE_KEY`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for idn_oauth2_device_flow
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_device_flow`;
CREATE TABLE `idn_oauth2_device_flow` (
  `CODE_ID` varchar(255) DEFAULT NULL COMMENT '代码ID',
  `DEVICE_CODE` varchar(255) NOT NULL,
  `USER_CODE` varchar(25) DEFAULT NULL COMMENT '用户代码',
  `QUANTIFIER` int(11) DEFAULT NULL COMMENT 'QUANTIFIER',
  `CONSUMER_KEY_ID` int(11) DEFAULT NULL,
  `LAST_POLL_TIME` timestamp NULL DEFAULT NULL COMMENT '上次轮询时间',
  `EXPIRY_TIME` timestamp NULL DEFAULT NULL COMMENT 'EXPIRY TIME',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `POLL_TIME` bigint(20) DEFAULT NULL COMMENT '轮询时间',
  `STATUS` varchar(25) DEFAULT 'PENDING',
  `AUTHZ_USER` varchar(100) DEFAULT NULL COMMENT 'AUTHZ USER',
  `TENANT_ID` int(11) DEFAULT NULL,
  `USER_DOMAIN` varchar(50) DEFAULT NULL COMMENT '用户域',
  `IDP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DEVICE_CODE`),
  UNIQUE KEY `CODE_ID` (`CODE_ID`),
  UNIQUE KEY `USRCDE_QNTFR_CONSTRAINT` (`USER_CODE`,`QUANTIFIER`),
  KEY `CONSUMER_KEY_ID` (`CONSUMER_KEY_ID`),
  CONSTRAINT `idn_oauth2_device_flow_ibfk_1` FOREIGN KEY (`CONSUMER_KEY_ID`) REFERENCES `idn_oauth_consumer_apps` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2设备流';

-- ----------------------------
-- Table structure for idn_oauth2_device_flow_scopes
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_device_flow_scopes`;
CREATE TABLE `idn_oauth2_device_flow_scopes` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `SCOPE_ID` varchar(255) DEFAULT NULL COMMENT '范围ID',
  `SCOPE` varchar(255) DEFAULT NULL COMMENT '范围',
  PRIMARY KEY (`ID`),
  KEY `SCOPE_ID` (`SCOPE_ID`),
  CONSTRAINT `idn_oauth2_device_flow_scopes_ibfk_1` FOREIGN KEY (`SCOPE_ID`) REFERENCES `idn_oauth2_device_flow` (`CODE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for idn_oauth2_resource_scope
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_resource_scope`;
CREATE TABLE `idn_oauth2_resource_scope` (
  `RESOURCE_PATH` varchar(255) NOT NULL COMMENT '资源路径',
  `SCOPE_ID` int(11) DEFAULT NULL COMMENT '范围ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`RESOURCE_PATH`),
  KEY `SCOPE_ID` (`SCOPE_ID`),
  CONSTRAINT `idn_oauth2_resource_scope_ibfk_1` FOREIGN KEY (`SCOPE_ID`) REFERENCES `idn_oauth2_scope` (`SCOPE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2资源范围';

-- ----------------------------
-- Table structure for idn_oauth2_scope
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_scope`;
CREATE TABLE `idn_oauth2_scope` (
  `SCOPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `DESCRIPTION` varchar(512) DEFAULT NULL,
  `TENANT_ID` int(11) NOT NULL DEFAULT '-1',
  `SCOPE_TYPE` varchar(255) DEFAULT NULL COMMENT '范围类型',
  PRIMARY KEY (`SCOPE_ID`),
  UNIQUE KEY `NAME` (`NAME`,`TENANT_ID`),
  KEY `IDX_SC_TID` (`TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='idn-oauth2 scope';

-- ----------------------------
-- Table structure for idn_oauth2_scope_binding
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_scope_binding`;
CREATE TABLE `idn_oauth2_scope_binding` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `SCOPE_ID` int(11) DEFAULT NULL COMMENT '范围ID',
  `SCOPE_BINDING` varchar(255) DEFAULT NULL COMMENT '范围绑定',
  `BINDING_TYPE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SCOPE_ID` (`SCOPE_ID`,`SCOPE_BINDING`,`BINDING_TYPE`),
  KEY `IDX_SB_SCPID` (`SCOPE_ID`),
  CONSTRAINT `idn_oauth2_scope_binding_ibfk_1` FOREIGN KEY (`SCOPE_ID`) REFERENCES `idn_oauth2_scope` (`SCOPE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2范围绑定';

-- ----------------------------
-- Table structure for idn_oauth2_scope_validators
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_scope_validators`;
CREATE TABLE `idn_oauth2_scope_validators` (
  `APP_ID` int(11) NOT NULL COMMENT '应用程序ID',
  `SCOPE_VALIDATOR` varchar(128) NOT NULL COMMENT '范围验证器',
  PRIMARY KEY (`APP_ID`,`SCOPE_VALIDATOR`),
  CONSTRAINT `idn_oauth2_scope_validators_ibfk_1` FOREIGN KEY (`APP_ID`) REFERENCES `idn_oauth_consumer_apps` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2范围验证器';

-- ----------------------------
-- Table structure for idn_oauth2_token_binding
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_token_binding`;
CREATE TABLE `idn_oauth2_token_binding` (
  `TOKEN_ID` varchar(255) DEFAULT NULL COMMENT '代币ID',
  `TOKEN_BINDING_TYPE` varchar(32) DEFAULT NULL COMMENT '令牌绑定类型',
  `TOKEN_BINDING_REF` varchar(32) DEFAULT NULL COMMENT '代币绑定参考',
  `TOKEN_BINDING_VALUE` varchar(1024) DEFAULT NULL COMMENT '令牌绑定值',
  `TENANT_ID` int(11) DEFAULT '-1',
  UNIQUE KEY `TOKEN_ID` (`TOKEN_ID`,`TOKEN_BINDING_TYPE`,`TOKEN_BINDING_VALUE`),
  KEY `IDX_IDN_AUTH_BIND` (`TOKEN_BINDING_REF`),
  KEY `IDX_TK_VALUE_TYPE` (`TOKEN_BINDING_VALUE`,`TOKEN_BINDING_TYPE`),
  CONSTRAINT `idn_oauth2_token_binding_ibfk_1` FOREIGN KEY (`TOKEN_ID`) REFERENCES `idn_oauth2_access_token` (`TOKEN_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2令牌绑定';

-- ----------------------------
-- Table structure for idn_oauth2_user_consent
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_user_consent`;
CREATE TABLE `idn_oauth2_user_consent` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `USER_ID` varchar(255) NOT NULL,
  `APP_ID` char(36) NOT NULL,
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `CONSENT_ID` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CONSENT_ID` (`CONSENT_ID`),
  UNIQUE KEY `USER_ID` (`USER_ID`,`APP_ID`,`TENANT_ID`),
  KEY `APP_ID` (`APP_ID`),
  CONSTRAINT `idn_oauth2_user_consent_ibfk_1` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for idn_oauth2_user_consented_scopes
-- ----------------------------
DROP TABLE IF EXISTS `idn_oauth2_user_consented_scopes`;
CREATE TABLE `idn_oauth2_user_consented_scopes` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `CONSENT_ID` varchar(255) NOT NULL,
  `TENANT_ID` int(11) NOT NULL DEFAULT '-1',
  `SCOPE` varchar(255) DEFAULT NULL COMMENT '范围',
  `CONSENT` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CONSENT_ID` (`CONSENT_ID`,`SCOPE`),
  CONSTRAINT `idn_oauth2_user_consented_scopes_ibfk_1` FOREIGN KEY (`CONSENT_ID`) REFERENCES `idn_oauth2_user_consent` (`CONSENT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oauth2用户同意的范围';

-- ----------------------------
-- Table structure for idn_oidc_jti
-- ----------------------------
DROP TABLE IF EXISTS `idn_oidc_jti`;
CREATE TABLE `idn_oidc_jti` (
  `JWT_ID` varchar(255) NOT NULL COMMENT 'JWT ID',
  `EXP_TIME` timestamp NULL DEFAULT NULL COMMENT 'EXP TIME',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  PRIMARY KEY (`JWT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oidc-jti';

-- ----------------------------
-- Table structure for idn_oidc_property
-- ----------------------------
DROP TABLE IF EXISTS `idn_oidc_property`;
CREATE TABLE `idn_oidc_property` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) DEFAULT NULL,
  `CONSUMER_KEY` varchar(255) DEFAULT NULL,
  `PROPERTY_KEY` varchar(255) DEFAULT NULL COMMENT '属性密钥',
  `PROPERTY_VALUE` varchar(2047) DEFAULT NULL COMMENT '属性值',
  PRIMARY KEY (`ID`),
  KEY `IDX_IOP_CK` (`CONSUMER_KEY`),
  CONSTRAINT `idn_oidc_property_ibfk_1` FOREIGN KEY (`CONSUMER_KEY`) REFERENCES `idn_oauth_consumer_apps` (`CONSUMER_KEY`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oidc属性';

-- ----------------------------
-- Table structure for idn_oidc_req_obj_claim_values
-- ----------------------------
DROP TABLE IF EXISTS `idn_oidc_req_obj_claim_values`;
CREATE TABLE `idn_oidc_req_obj_claim_values` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REQ_OBJECT_CLAIMS_ID` int(11) DEFAULT NULL COMMENT 'req对象声明ID',
  `CLAIM_VALUES` varchar(255) DEFAULT NULL COMMENT '索赔值',
  PRIMARY KEY (`ID`),
  KEY `REQ_OBJECT_CLAIMS_ID` (`REQ_OBJECT_CLAIMS_ID`),
  CONSTRAINT `idn_oidc_req_obj_claim_values_ibfk_1` FOREIGN KEY (`REQ_OBJECT_CLAIMS_ID`) REFERENCES `idn_oidc_req_object_claims` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oidc req-obj声明值';

-- ----------------------------
-- Table structure for idn_oidc_req_object_claims
-- ----------------------------
DROP TABLE IF EXISTS `idn_oidc_req_object_claims`;
CREATE TABLE `idn_oidc_req_object_claims` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REQ_OBJECT_ID` int(11) DEFAULT NULL COMMENT 'req对象ID',
  `CLAIM_ATTRIBUTE` varchar(255) DEFAULT NULL,
  `ESSENTIAL` char(1) DEFAULT NULL COMMENT 'ESSENTIAL',
  `VALUE` varchar(255) DEFAULT NULL COMMENT '值',
  `IS_USERINFO` char(1) DEFAULT NULL COMMENT 'IS用户信息',
  PRIMARY KEY (`ID`),
  KEY `REQ_OBJECT_ID` (`REQ_OBJECT_ID`),
  CONSTRAINT `idn_oidc_req_object_claims_ibfk_1` FOREIGN KEY (`REQ_OBJECT_ID`) REFERENCES `idn_oidc_req_object_reference` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oidc-req对象声明';

-- ----------------------------
-- Table structure for idn_oidc_req_object_reference
-- ----------------------------
DROP TABLE IF EXISTS `idn_oidc_req_object_reference`;
CREATE TABLE `idn_oidc_req_object_reference` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONSUMER_KEY_ID` int(11) DEFAULT NULL,
  `CODE_ID` varchar(255) DEFAULT NULL COMMENT '代码ID',
  `TOKEN_ID` varchar(255) DEFAULT NULL COMMENT 'TOKEN ID',
  `SESSION_DATA_KEY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CONSUMER_KEY_ID` (`CONSUMER_KEY_ID`),
  KEY `CODE_ID` (`CODE_ID`),
  KEY `IDX_OROR_TID` (`TOKEN_ID`),
  CONSTRAINT `idn_oidc_req_object_reference_ibfk_1` FOREIGN KEY (`CONSUMER_KEY_ID`) REFERENCES `idn_oauth_consumer_apps` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `idn_oidc_req_object_reference_ibfk_2` FOREIGN KEY (`TOKEN_ID`) REFERENCES `idn_oauth2_access_token` (`TOKEN_ID`) ON DELETE CASCADE,
  CONSTRAINT `idn_oidc_req_object_reference_ibfk_3` FOREIGN KEY (`CODE_ID`) REFERENCES `idn_oauth2_authorization_code` (`CODE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oidc-req对象引用';

-- ----------------------------
-- Table structure for idn_oidc_scope_claim_mapping
-- ----------------------------
DROP TABLE IF EXISTS `idn_oidc_scope_claim_mapping`;
CREATE TABLE `idn_oidc_scope_claim_mapping` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `SCOPE_ID` int(11) DEFAULT NULL COMMENT '范围ID',
  `EXTERNAL_CLAIM_ID` int(11) DEFAULT NULL COMMENT '外部索赔ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SCOPE_ID` (`SCOPE_ID`,`EXTERNAL_CLAIM_ID`),
  KEY `EXTERNAL_CLAIM_ID` (`EXTERNAL_CLAIM_ID`),
  KEY `IDX_AT_SI_ECI` (`SCOPE_ID`,`EXTERNAL_CLAIM_ID`),
  CONSTRAINT `idn_oidc_scope_claim_mapping_ibfk_1` FOREIGN KEY (`SCOPE_ID`) REFERENCES `idn_oauth2_scope` (`SCOPE_ID`) ON DELETE CASCADE,
  CONSTRAINT `idn_oidc_scope_claim_mapping_ibfk_2` FOREIGN KEY (`EXTERNAL_CLAIM_ID`) REFERENCES `idn_claim` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-oidc范围声明映射';

-- ----------------------------
-- Table structure for idn_openid_associations
-- ----------------------------
DROP TABLE IF EXISTS `idn_openid_associations`;
CREATE TABLE `idn_openid_associations` (
  `HANDLE` varchar(255) NOT NULL COMMENT 'HANDLE',
  `ASSOC_TYPE` varchar(255) DEFAULT NULL COMMENT '关联类型',
  `EXPIRE_IN` timestamp NULL DEFAULT NULL COMMENT 'EXPIRE IN',
  `MAC_KEY` varchar(255) DEFAULT NULL COMMENT 'MAC密钥',
  `ASSOC_STORE` varchar(128) DEFAULT 'SHARED',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`HANDLE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-openid关联';

-- ----------------------------
-- Table structure for idn_openid_remember_me
-- ----------------------------
DROP TABLE IF EXISTS `idn_openid_remember_me`;
CREATE TABLE `idn_openid_remember_me` (
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  `TENANT_ID` int(11) NOT NULL COMMENT '租户ID',
  `COOKIE_VALUE` varchar(1024) DEFAULT NULL COMMENT 'COOKIE VALUE',
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`USER_NAME`,`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-openid记住我';

-- ----------------------------
-- Table structure for idn_openid_user_rps
-- ----------------------------
DROP TABLE IF EXISTS `idn_openid_user_rps`;
CREATE TABLE `idn_openid_user_rps` (
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  `TENANT_ID` int(11) NOT NULL COMMENT '租户ID',
  `RP_URL` varchar(255) NOT NULL COMMENT 'RP URL',
  `TRUSTED_ALWAYS` varchar(128) DEFAULT NULL COMMENT '始终受信任',
  `LAST_VISIT` date DEFAULT NULL COMMENT '上次访问',
  `VISIT_COUNT` int(11) DEFAULT NULL COMMENT '访问计数',
  `DEFAULT_PRO_NAME` varchar(255) DEFAULT NULL COMMENT '默认档案名称',
  PRIMARY KEY (`USER_NAME`,`TENANT_ID`,`RP_URL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-openid用户rps';

-- ----------------------------
-- Table structure for idn_password_history_data
-- ----------------------------
DROP TABLE IF EXISTS `idn_password_history_data`;
CREATE TABLE `idn_password_history_data` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `USER_NAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  `USER_DOMAIN` varchar(127) DEFAULT NULL COMMENT '用户域',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `SALT_VALUE` varchar(255) DEFAULT NULL COMMENT 'SALT VALUE',
  `HASH` varchar(255) DEFAULT NULL COMMENT 'HASH',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn密码历史数据';

-- ----------------------------
-- Table structure for idn_recovery_data
-- ----------------------------
DROP TABLE IF EXISTS `idn_recovery_data`;
CREATE TABLE `idn_recovery_data` (
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  `USER_DOMAIN` varchar(127) NOT NULL COMMENT '用户域',
  `TENANT_ID` int(11) NOT NULL COMMENT '租户ID',
  `CODE` varchar(255) DEFAULT NULL COMMENT '代码',
  `SCENARIO` varchar(255) NOT NULL COMMENT '场景',
  `STEP` varchar(127) NOT NULL COMMENT 'STEP',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `REMAINING_SETS` varchar(2500) DEFAULT NULL COMMENT '剩余集',
  PRIMARY KEY (`USER_NAME`,`USER_DOMAIN`,`TENANT_ID`,`SCENARIO`,`STEP`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn恢复数据';

-- ----------------------------
-- Table structure for idn_remote_fetch_config
-- ----------------------------
DROP TABLE IF EXISTS `idn_remote_fetch_config`;
CREATE TABLE `idn_remote_fetch_config` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `IS_ENABLED` char(1) DEFAULT NULL COMMENT '已启用',
  `REPO_MANAGER_TYPE` varchar(255) DEFAULT NULL COMMENT 'REPO MANAGER类型',
  `ACTION_LISTENER_TYPE` varchar(255) NOT NULL,
  `CONFIG_DEPLOYER_TYPE` varchar(255) DEFAULT NULL COMMENT '配置展开类型',
  `REMOTE_FETCH_NAME` varchar(255) DEFAULT NULL COMMENT '远程fetch名称',
  `REMOTE_RESOURCE_URI` varchar(255) DEFAULT NULL COMMENT '远程资源URI',
  `ATTRIBUTES_JSON` mediumtext COMMENT '属性JSON',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_REMOTE_RESOURCE_TYPE` (`TENANT_ID`,`CONFIG_DEPLOYER_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn远程获取配置';

-- ----------------------------
-- Table structure for idn_remote_fetch_revisions
-- ----------------------------
DROP TABLE IF EXISTS `idn_remote_fetch_revisions`;
CREATE TABLE `idn_remote_fetch_revisions` (
  `ID` varchar(255) NOT NULL,
  `CONFIG_ID` varchar(255) NOT NULL,
  `_PATH` varchar(255) NOT NULL,
  `_HASH` varchar(255) DEFAULT NULL,
  `DEPLOYED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_SYNC_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DEPLOYMENT_STATUS` varchar(255) DEFAULT NULL,
  `ITEM_NAME` varchar(255) DEFAULT NULL,
  `DEPLOY_ERR_LOG` mediumtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_REVISIONS` (`CONFIG_ID`,`ITEM_NAME`),
  KEY `IDX_REMOTE_FETCH_REVISION_CONFIG_ID` (`CONFIG_ID`),
  CONSTRAINT `idn_remote_fetch_revisions_ibfk_1` FOREIGN KEY (`CONFIG_ID`) REFERENCES `idn_remote_fetch_config` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for idn_saml2_artifact_store
-- ----------------------------
DROP TABLE IF EXISTS `idn_saml2_artifact_store`;
CREATE TABLE `idn_saml2_artifact_store` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SOURCE_ID` varchar(255) DEFAULT NULL COMMENT '源ID',
  `MESSAGE_HANDLER` varchar(255) NOT NULL,
  `AUTHN_REQ_DTO` blob NOT NULL,
  `SESSION_ID` varchar(255) NOT NULL,
  `EXP_TIMESTAMP` timestamp NOT NULL,
  `INIT_TIMESTAMP` timestamp NULL DEFAULT NULL COMMENT 'INIT TIMESTAMP',
  `ASSERTION_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-saml2工件存储';

-- ----------------------------
-- Table structure for idn_saml2_assertion_store
-- ----------------------------
DROP TABLE IF EXISTS `idn_saml2_assertion_store`;
CREATE TABLE `idn_saml2_assertion_store` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SAML2_ID` varchar(255) DEFAULT NULL,
  `SAML2_ISSUER` varchar(255) DEFAULT NULL COMMENT 'saml2 ISSUER',
  `SAML2_SUBJECT` varchar(255) DEFAULT NULL COMMENT 'saml2主题',
  `SAML2_SESSION_INDEX` varchar(255) DEFAULT NULL COMMENT 'saml2会话索引',
  `SAML2_AUTHN_CONTEXT_CLASS_REF` varchar(255) DEFAULT NULL,
  `SAML2_ASSERTION` varchar(4096) DEFAULT NULL COMMENT 'saml2 assertion',
  `ASSERTION` blob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-saml2断言存储';

-- ----------------------------
-- Table structure for idn_scim_group
-- ----------------------------
DROP TABLE IF EXISTS `idn_scim_group`;
CREATE TABLE `idn_scim_group` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `ROLE_NAME` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `ATTR_NAME` varchar(1024) DEFAULT NULL COMMENT 'ATTR NAME',
  `ATTR_VALUE` varchar(1024) DEFAULT NULL COMMENT 'ATTR VALUE',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`ROLE_NAME`,`ATTR_NAME`),
  KEY `IDX_IDN_SCIM_GROUP_TI_RN` (`TENANT_ID`,`ROLE_NAME`),
  KEY `IDX_IDN_SCIM_GROUP_TI_RN_AN` (`TENANT_ID`,`ROLE_NAME`,`ATTR_NAME`(500))
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-scim组';

-- ----------------------------
-- Table structure for idn_secret
-- ----------------------------
DROP TABLE IF EXISTS `idn_secret`;
CREATE TABLE `idn_secret` (
  `ID` varchar(255) NOT NULL,
  `TENANT_ID` int(11) NOT NULL,
  `SECRET_NAME` varchar(255) NOT NULL,
  `SECRET_VALUE` varchar(8000) NOT NULL,
  `CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `LAST_MODIFIED` timestamp NULL DEFAULT NULL COMMENT '上次修改',
  `TYPE_ID` varchar(255) DEFAULT NULL COMMENT '类型ID',
  `DESCRIPTION` varchar(1023) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SECRET_NAME` (`SECRET_NAME`,`TENANT_ID`,`TYPE_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `idn_secret_ibfk_1` FOREIGN KEY (`TYPE_ID`) REFERENCES `idn_secret_type` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn机密';

-- ----------------------------
-- Table structure for idn_secret_type
-- ----------------------------
DROP TABLE IF EXISTS `idn_secret_type`;
CREATE TABLE `idn_secret_type` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '姓名',
  `DESCRIPTION` varchar(1023) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SECRET_TYPE_NAME_CONSTRAINT` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn机密类型';

-- ----------------------------
-- Table structure for idn_sts_store
-- ----------------------------
DROP TABLE IF EXISTS `idn_sts_store`;
CREATE TABLE `idn_sts_store` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TOKEN_ID` varchar(255) DEFAULT NULL COMMENT '代币ID',
  `TOKEN_CONTENT` blob COMMENT '代币内容',
  `CREATE_DATE` timestamp NULL DEFAULT NULL COMMENT '创建日期',
  `EXPIRE_DATE` timestamp NULL DEFAULT NULL COMMENT '到期日期',
  `STATE` int(11) DEFAULT NULL COMMENT 'STATE',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-sts-store';

-- ----------------------------
-- Table structure for idn_thrift_session
-- ----------------------------
DROP TABLE IF EXISTS `idn_thrift_session`;
CREATE TABLE `idn_thrift_session` (
  `SESSION_ID` varchar(255) NOT NULL,
  `USER_NAME` varchar(255) NOT NULL,
  `CREATED_TIME` varchar(255) NOT NULL,
  `LAST_MODIFIED_TIME` varchar(255) NOT NULL,
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`SESSION_ID`),
  KEY `IDX_ITS_LMT` (`LAST_MODIFIED_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for idn_uma_permission_ticket
-- ----------------------------
DROP TABLE IF EXISTS `idn_uma_permission_ticket`;
CREATE TABLE `idn_uma_permission_ticket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PT` varchar(255) DEFAULT NULL COMMENT 'PT',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `EXPIRY_TIME` timestamp NULL DEFAULT NULL COMMENT '到期时间',
  `TICKET_STATE` varchar(25) DEFAULT NULL COMMENT '票证状态',
  `TENANT_ID` int(11) DEFAULT '-1234',
  `TOKEN_ID` varchar(255) DEFAULT NULL COMMENT '代币ID',
  PRIMARY KEY (`ID`),
  KEY `IDX_PT` (`PT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn_uma权限票证';

-- ----------------------------
-- Table structure for idn_uma_pt_resource
-- ----------------------------
DROP TABLE IF EXISTS `idn_uma_pt_resource`;
CREATE TABLE `idn_uma_pt_resource` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PT_RESOURCE_ID` int(11) DEFAULT NULL COMMENT 'pt资源ID',
  `PT_ID` int(11) DEFAULT NULL COMMENT 'pt ID',
  PRIMARY KEY (`ID`),
  KEY `PT_ID` (`PT_ID`),
  KEY `PT_RESOURCE_ID` (`PT_RESOURCE_ID`),
  CONSTRAINT `idn_uma_pt_resource_ibfk_1` FOREIGN KEY (`PT_ID`) REFERENCES `idn_uma_permission_ticket` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `idn_uma_pt_resource_ibfk_2` FOREIGN KEY (`PT_RESOURCE_ID`) REFERENCES `idn_uma_resource` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-uma-pt资源';

-- ----------------------------
-- Table structure for idn_uma_pt_resource_scope
-- ----------------------------
DROP TABLE IF EXISTS `idn_uma_pt_resource_scope`;
CREATE TABLE `idn_uma_pt_resource_scope` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PT_RESOURCE_ID` int(11) DEFAULT NULL COMMENT 'pt资源ID',
  `PT_SCOPE_ID` int(11) DEFAULT NULL COMMENT 'pt范围ID',
  PRIMARY KEY (`ID`),
  KEY `PT_RESOURCE_ID` (`PT_RESOURCE_ID`),
  KEY `PT_SCOPE_ID` (`PT_SCOPE_ID`),
  CONSTRAINT `idn_uma_pt_resource_scope_ibfk_1` FOREIGN KEY (`PT_RESOURCE_ID`) REFERENCES `idn_uma_pt_resource` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `idn_uma_pt_resource_scope_ibfk_2` FOREIGN KEY (`PT_SCOPE_ID`) REFERENCES `idn_uma_resource_scope` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-uma-pt资源范围';

-- ----------------------------
-- Table structure for idn_uma_resource
-- ----------------------------
DROP TABLE IF EXISTS `idn_uma_resource`;
CREATE TABLE `idn_uma_resource` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RESOURCE_ID` varchar(255) DEFAULT NULL COMMENT '资源ID',
  `RESOURCE_NAME` varchar(255) DEFAULT NULL COMMENT '资源名称',
  `TIME_CREATED` timestamp NULL DEFAULT NULL COMMENT '创建的时间',
  `RESOURCE_OWNER_NAME` varchar(255) DEFAULT NULL COMMENT '资源所有者名称',
  `CLIENT_ID` varchar(255) DEFAULT NULL COMMENT '客户端ID',
  `TENANT_ID` int(11) DEFAULT '-1234',
  `USER_DOMAIN` varchar(50) DEFAULT NULL COMMENT '用户域',
  PRIMARY KEY (`ID`),
  KEY `IDX_RID` (`RESOURCE_ID`),
  KEY `IDX_USER` (`RESOURCE_OWNER_NAME`,`USER_DOMAIN`),
  KEY `IDX_USER_RID` (`RESOURCE_ID`,`RESOURCE_OWNER_NAME`(32),`USER_DOMAIN`,`CLIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn-uma资源';

-- ----------------------------
-- Table structure for idn_uma_resource_meta_data
-- ----------------------------
DROP TABLE IF EXISTS `idn_uma_resource_meta_data`;
CREATE TABLE `idn_uma_resource_meta_data` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `RESOURCE_IDENTITY` int(11) DEFAULT NULL COMMENT '资源标识',
  `PROPERTY_KEY` varchar(40) DEFAULT NULL COMMENT '属性密钥',
  `PROPERTY_VALUE` varchar(255) DEFAULT NULL COMMENT '属性值',
  PRIMARY KEY (`ID`),
  KEY `RESOURCE_IDENTITY` (`RESOURCE_IDENTITY`),
  CONSTRAINT `idn_uma_resource_meta_data_ibfk_1` FOREIGN KEY (`RESOURCE_IDENTITY`) REFERENCES `idn_uma_resource` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn_uma资源元数据';

-- ----------------------------
-- Table structure for idn_uma_resource_scope
-- ----------------------------
DROP TABLE IF EXISTS `idn_uma_resource_scope`;
CREATE TABLE `idn_uma_resource_scope` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RESOURCE_IDENTITY` int(11) DEFAULT NULL COMMENT '资源标识',
  `SCOPE_NAME` varchar(255) DEFAULT NULL COMMENT '范围名称',
  PRIMARY KEY (`ID`),
  KEY `RESOURCE_IDENTITY` (`RESOURCE_IDENTITY`),
  KEY `IDX_RS` (`SCOPE_NAME`),
  CONSTRAINT `idn_uma_resource_scope_ibfk_1` FOREIGN KEY (`RESOURCE_IDENTITY`) REFERENCES `idn_uma_resource` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn_uma资源范围';

-- ----------------------------
-- Table structure for idn_user_account_association
-- ----------------------------
DROP TABLE IF EXISTS `idn_user_account_association`;
CREATE TABLE `idn_user_account_association` (
  `ASSOCIATION_KEY` varchar(255) DEFAULT NULL COMMENT '关联密钥',
  `TENANT_ID` int(11) NOT NULL,
  `DOMAIN_NAME` varchar(255) NOT NULL COMMENT '域名',
  `USER_NAME` varchar(255) NOT NULL COMMENT '用户名',
  PRIMARY KEY (`TENANT_ID`,`DOMAIN_NAME`,`USER_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn用户帐户关联';

-- ----------------------------
-- Table structure for idn_user_functionality_mapping
-- ----------------------------
DROP TABLE IF EXISTS `idn_user_functionality_mapping`;
CREATE TABLE `idn_user_functionality_mapping` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `USER_ID` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `FUNCTIONALITY_ID` varchar(255) DEFAULT NULL COMMENT '功能ID',
  `IS_FUNCTIONALITY_LOCKED` tinyint(1) DEFAULT NULL COMMENT 'IS functionality LOCKED',
  `FUNCTIONALITY_UNLOCK_TIME` bigint(20) DEFAULT NULL COMMENT '功能解锁时间',
  `FUNCTIONALITY_LOCK_REASON` varchar(1023) DEFAULT NULL COMMENT '功能锁定原因',
  `FUNCTIONALITY_LOCK_REASON_CODE` varchar(255) DEFAULT NULL COMMENT '功能锁定原因代码',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDN_USER_FUNCTIONALITY_MAPPING_CONSTRAINT` (`USER_ID`,`TENANT_ID`,`FUNCTIONALITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn用户功能映射';

-- ----------------------------
-- Table structure for idn_user_functionality_property
-- ----------------------------
DROP TABLE IF EXISTS `idn_user_functionality_property`;
CREATE TABLE `idn_user_functionality_property` (
  `ID` varchar(255) NOT NULL COMMENT 'ID',
  `USER_ID` varchar(255) DEFAULT NULL COMMENT '用户ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `FUNCTIONALITY_ID` varchar(255) DEFAULT NULL COMMENT '功能ID',
  `PROPERTY_NAME` varchar(255) DEFAULT NULL COMMENT '属性名称',
  `PROPERTY_VALUE` varchar(255) DEFAULT NULL COMMENT '属性值',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDN_USER_FUNCTIONALITY_PROPERTY_CONSTRAINT` (`USER_ID`,`TENANT_ID`,`FUNCTIONALITY_ID`,`PROPERTY_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idn用户功能属性';

-- ----------------------------
-- Table structure for idp
-- ----------------------------
DROP TABLE IF EXISTS `idp`;
CREATE TABLE `idp` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `NAME` varchar(254) DEFAULT NULL COMMENT '名称',
  `IS_ENABLED` char(1) DEFAULT NULL COMMENT '已启用',
  `IS_PRIMARY` char(1) DEFAULT NULL COMMENT '为主',
  `HOME_REALM_ID` varchar(254) DEFAULT NULL,
  `IMAGE` mediumblob COMMENT '图像',
  `CERTIFICATE` blob COMMENT '证书',
  `ALIAS` varchar(254) DEFAULT NULL COMMENT 'ALIAS',
  `INBOUND_PROV_ENABLED` char(1) DEFAULT NULL COMMENT 'INBOUND PROV ENABLED',
  `INBOUND_PROV_USER_STORE_ID` varchar(254) DEFAULT NULL COMMENT 'INBOUND PROV用户存储ID',
  `USER_CLAIM_URI` varchar(254) DEFAULT NULL COMMENT '用户声明URI',
  `ROLE_CLAIM_URI` varchar(254) DEFAULT NULL COMMENT '角色声明URI',
  `DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '描述',
  `DEFAULT_AUTHENTICATOR_NAME` varchar(254) DEFAULT NULL COMMENT '默认验证器名称',
  `DEFAULT_PRO_CONNECTOR_NAME` varchar(254) DEFAULT NULL COMMENT '默认专业连接器名称',
  `PROVISIONING_ROLE` varchar(128) DEFAULT NULL COMMENT '提供角色',
  `IS_FEDERATION_HUB` char(1) DEFAULT NULL COMMENT 'IS联邦集线器',
  `IS_LOCAL_CLAIM_DIALECT` char(1) DEFAULT NULL COMMENT '是本地索赔DIALECT',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `IMAGE_URL` varchar(1024) DEFAULT NULL COMMENT '图像URL',
  `UUID` char(36) DEFAULT NULL COMMENT 'UUID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UUID` (`UUID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='idp';

-- ----------------------------
-- Table structure for idp_authenticator
-- ----------------------------
DROP TABLE IF EXISTS `idp_authenticator`;
CREATE TABLE `idp_authenticator` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) DEFAULT NULL,
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'idp ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `IS_ENABLED` char(1) DEFAULT NULL COMMENT '已启用',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`IDP_ID`,`NAME`),
  KEY `IDP_ID` (`IDP_ID`),
  CONSTRAINT `idp_authenticator_ibfk_1` FOREIGN KEY (`IDP_ID`) REFERENCES `idp` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='idp验证器';

-- ----------------------------
-- Table structure for idp_authenticator_property
-- ----------------------------
DROP TABLE IF EXISTS `idp_authenticator_property`;
CREATE TABLE `idp_authenticator_property` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `AUTHENTICATOR_ID` int(11) DEFAULT NULL COMMENT '身份验证程序ID',
  `PROPERTY_KEY` varchar(255) DEFAULT NULL COMMENT '属性密钥',
  `PROPERTY_VALUE` varchar(2047) DEFAULT NULL COMMENT '属性值',
  `IS_SECRET` char(1) DEFAULT NULL COMMENT 'IS SECRET',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`AUTHENTICATOR_ID`,`PROPERTY_KEY`),
  KEY `AUTHENTICATOR_ID` (`AUTHENTICATOR_ID`),
  CONSTRAINT `idp_authenticator_property_ibfk_1` FOREIGN KEY (`AUTHENTICATOR_ID`) REFERENCES `idp_authenticator` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp authenticator属性';

-- ----------------------------
-- Table structure for idp_claim
-- ----------------------------
DROP TABLE IF EXISTS `idp_claim`;
CREATE TABLE `idp_claim` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'idp ID',
  `TENANT_ID` int(11) DEFAULT NULL,
  `CLAIM` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDP_ID` (`IDP_ID`,`CLAIM`),
  CONSTRAINT `idp_claim_ibfk_1` FOREIGN KEY (`IDP_ID`) REFERENCES `idp` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp索赔';

-- ----------------------------
-- Table structure for idp_claim_mapping
-- ----------------------------
DROP TABLE IF EXISTS `idp_claim_mapping`;
CREATE TABLE `idp_claim_mapping` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `IDP_CLAIM_ID` int(11) DEFAULT NULL,
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `LOCAL_CLAIM` varchar(253) DEFAULT NULL COMMENT '本地索赔',
  `DEFAULT_VALUE` varchar(255) DEFAULT NULL COMMENT '默认值',
  `IS_REQUESTED` varchar(128) DEFAULT NULL COMMENT '请求',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDP_CLAIM_ID` (`IDP_CLAIM_ID`,`TENANT_ID`,`LOCAL_CLAIM`),
  CONSTRAINT `idp_claim_mapping_ibfk_1` FOREIGN KEY (`IDP_CLAIM_ID`) REFERENCES `idp_claim` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp索赔映射';

-- ----------------------------
-- Table structure for idp_local_claim
-- ----------------------------
DROP TABLE IF EXISTS `idp_local_claim`;
CREATE TABLE `idp_local_claim` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'idp ID',
  `CLAIM_URI` varchar(255) DEFAULT NULL COMMENT '声明URI',
  `DEFAULT_VALUE` varchar(255) DEFAULT NULL COMMENT '默认值',
  `IS_REQUESTED` varchar(128) DEFAULT NULL COMMENT '请求',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`IDP_ID`,`CLAIM_URI`),
  KEY `IDP_ID` (`IDP_ID`),
  CONSTRAINT `idp_local_claim_ibfk_1` FOREIGN KEY (`IDP_ID`) REFERENCES `idp` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp本地声明';

-- ----------------------------
-- Table structure for idp_metadata
-- ----------------------------
DROP TABLE IF EXISTS `idp_metadata`;
CREATE TABLE `idp_metadata` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `IDP_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `VALUE` varchar(255) DEFAULT NULL COMMENT 'VALUE',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDP_METADATA_CONSTRAINT` (`IDP_ID`,`NAME`),
  CONSTRAINT `idp_metadata_ibfk_1` FOREIGN KEY (`IDP_ID`) REFERENCES `idp` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp元数据';

-- ----------------------------
-- Table structure for idp_prov_config_property
-- ----------------------------
DROP TABLE IF EXISTS `idp_prov_config_property`;
CREATE TABLE `idp_prov_config_property` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) DEFAULT NULL,
  `PROVISIONING_CONFIG_ID` int(11) DEFAULT NULL COMMENT 'PROVISIONING config ID',
  `PROPERTY_KEY` varchar(255) DEFAULT NULL COMMENT '属性密钥',
  `PROPERTY_VALUE` varchar(2048) DEFAULT NULL COMMENT '属性值',
  `PROPERTY_BLOB_VALUE` blob COMMENT '属性BLOB值',
  `PROPERTY_TYPE` char(32) DEFAULT NULL COMMENT '属性类型',
  `IS_SECRET` char(1) DEFAULT NULL COMMENT 'IS SECRET',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`PROVISIONING_CONFIG_ID`,`PROPERTY_KEY`),
  KEY `PROVISIONING_CONFIG_ID` (`PROVISIONING_CONFIG_ID`),
  CONSTRAINT `idp_prov_config_property_ibfk_1` FOREIGN KEY (`PROVISIONING_CONFIG_ID`) REFERENCES `idp_provisioning_config` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp-prov配置属性';

-- ----------------------------
-- Table structure for idp_provisioning_config
-- ----------------------------
DROP TABLE IF EXISTS `idp_provisioning_config`;
CREATE TABLE `idp_provisioning_config` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'idp ID',
  `PROVISIONING_CONNECTOR_TYPE` varchar(255) NOT NULL,
  `IS_ENABLED` char(1) DEFAULT NULL COMMENT '已启用',
  `IS_BLOCKING` char(1) DEFAULT NULL COMMENT '正在阻塞',
  `IS_RULES_ENABLED` char(1) DEFAULT NULL COMMENT 'IS规则已启用',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TENANT_ID` (`TENANT_ID`,`IDP_ID`,`PROVISIONING_CONNECTOR_TYPE`),
  KEY `IDP_ID` (`IDP_ID`),
  CONSTRAINT `idp_provisioning_config_ibfk_1` FOREIGN KEY (`IDP_ID`) REFERENCES `idp` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp配置配置';

-- ----------------------------
-- Table structure for idp_provisioning_entity
-- ----------------------------
DROP TABLE IF EXISTS `idp_provisioning_entity`;
CREATE TABLE `idp_provisioning_entity` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `PROVISIONING_CONFIG_ID` int(11) DEFAULT NULL,
  `ENTITY_TYPE` varchar(255) DEFAULT NULL COMMENT '实体类型',
  `ENTITY_LOCAL_USERSTORE` varchar(255) DEFAULT NULL COMMENT '实体本地用户存储',
  `ENTITY_NAME` varchar(255) DEFAULT NULL COMMENT '实体名称',
  `ENTITY_VALUE` varchar(255) DEFAULT NULL COMMENT '实体值',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `ENTITY_LOCAL_ID` varchar(255) DEFAULT NULL COMMENT '实体本地ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ENTITY_TYPE` (`ENTITY_TYPE`,`TENANT_ID`,`ENTITY_LOCAL_USERSTORE`,`ENTITY_NAME`,`PROVISIONING_CONFIG_ID`),
  UNIQUE KEY `PROVISIONING_CONFIG_ID` (`PROVISIONING_CONFIG_ID`,`ENTITY_TYPE`,`ENTITY_VALUE`),
  CONSTRAINT `idp_provisioning_entity_ibfk_1` FOREIGN KEY (`PROVISIONING_CONFIG_ID`) REFERENCES `idp_provisioning_config` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp供应实体';

-- ----------------------------
-- Table structure for idp_role
-- ----------------------------
DROP TABLE IF EXISTS `idp_role`;
CREATE TABLE `idp_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDP_ID` int(11) DEFAULT NULL COMMENT 'idp ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `ROLE` varchar(254) DEFAULT NULL COMMENT '角色',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDP_ID` (`IDP_ID`,`ROLE`),
  CONSTRAINT `idp_role_ibfk_1` FOREIGN KEY (`IDP_ID`) REFERENCES `idp` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp角色';

-- ----------------------------
-- Table structure for idp_role_mapping
-- ----------------------------
DROP TABLE IF EXISTS `idp_role_mapping`;
CREATE TABLE `idp_role_mapping` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDP_ROLE_ID` int(11) DEFAULT NULL,
  `TENANT_ID` int(11) DEFAULT NULL,
  `USER_STORE_ID` varchar(253) DEFAULT NULL,
  `LOCAL_ROLE` varchar(253) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDP_ROLE_ID` (`IDP_ROLE_ID`,`TENANT_ID`,`USER_STORE_ID`,`LOCAL_ROLE`),
  CONSTRAINT `idp_role_mapping_ibfk_1` FOREIGN KEY (`IDP_ROLE_ID`) REFERENCES `idp_role` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='idp角色映射';

-- ----------------------------
-- Table structure for sp_app
-- ----------------------------
DROP TABLE IF EXISTS `sp_app`;
CREATE TABLE `sp_app` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) NOT NULL,
  `APP_NAME` varchar(255) DEFAULT NULL COMMENT '应用程序名称',
  `USER_STORE` varchar(255) DEFAULT NULL COMMENT '用户存储',
  `USERNAME` varchar(255) DEFAULT NULL COMMENT 'USERNAME',
  `DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT '描述',
  `ROLE_CLAIM` varchar(512) DEFAULT NULL COMMENT '角色声明',
  `AUTH_TYPE` varchar(255) DEFAULT NULL COMMENT '身份类型',
  `PROVISIONING_USERSTORE_DOMAIN` varchar(512) DEFAULT NULL COMMENT '正在设置USERSTORE域',
  `IS_LOCAL_CLAIM_DIALECT` char(1) DEFAULT NULL COMMENT 'IS本地声明DIALECT',
  `IS_SEND_LOCAL_SUBJECT_ID` char(1) DEFAULT NULL COMMENT '发送本地主题ID',
  `IS_SEND_AUTH_LIST_OF_IDPS` char(1) DEFAULT NULL COMMENT '发送IDPS的AUTH列表',
  `IS_USE_TENANT_DOMAIN_SUBJECT` char(1) DEFAULT NULL COMMENT '正在使用租户域主题',
  `IS_USE_USER_DOMAIN_SUBJECT` char(1) DEFAULT NULL COMMENT 'IS使用用户域主题',
  `ENABLE_AUTHORIZATION` char(1) DEFAULT NULL COMMENT '启用授权',
  `SUBJECT_CLAIM_URI` varchar(512) DEFAULT NULL COMMENT '主题声明URI',
  `IS_SAAS_APP` char(1) DEFAULT NULL COMMENT 'IS SAAS应用程序',
  `IS_DUMB_MODE` char(1) DEFAULT NULL COMMENT 'IS转储模式',
  `UUID` char(36) DEFAULT NULL COMMENT 'UUID',
  `IMAGE_URL` varchar(1024) DEFAULT NULL COMMENT '图像URL',
  `ACCESS_URL` varchar(1024) DEFAULT NULL COMMENT '访问URL',
  `IS_DISCOVERABLE` char(1) DEFAULT NULL COMMENT '可发现',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `APPLICATION_NAME_CONSTRAINT` (`APP_NAME`,`TENANT_ID`),
  UNIQUE KEY `APPLICATION_UUID_CONSTRAINT` (`UUID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='sp应用';

-- ----------------------------
-- Table structure for sp_auth_script
-- ----------------------------
DROP TABLE IF EXISTS `sp_auth_script`;
CREATE TABLE `sp_auth_script` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  `TYPE` varchar(255) DEFAULT NULL COMMENT 'TYPE',
  `CONTENT` blob COMMENT '内容',
  `IS_ENABLED` char(1) DEFAULT NULL COMMENT '已启用',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp身份验证脚本';

-- ----------------------------
-- Table structure for sp_auth_step
-- ----------------------------
DROP TABLE IF EXISTS `sp_auth_step`;
CREATE TABLE `sp_auth_step` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) NOT NULL,
  `STEP_ORDER` int(11) DEFAULT '1',
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  `IS_SUBJECT_STEP` char(1) DEFAULT NULL COMMENT 'IS主题步骤',
  `IS_ATTRIBUTE_STEP` char(1) DEFAULT NULL COMMENT 'IS属性步骤',
  PRIMARY KEY (`ID`),
  KEY `APPLICATION_ID_CONSTRAINT_STEP` (`APP_ID`),
  CONSTRAINT `APPLICATION_ID_CONSTRAINT_STEP` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp身份验证步骤';

-- ----------------------------
-- Table structure for sp_claim_dialect
-- ----------------------------
DROP TABLE IF EXISTS `sp_claim_dialect`;
CREATE TABLE `sp_claim_dialect` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `SP_DIALECT` varchar(512) DEFAULT NULL COMMENT 'sp dialect',
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  PRIMARY KEY (`ID`),
  KEY `DIALECTID_APPID_CONSTRAINT` (`APP_ID`),
  CONSTRAINT `DIALECTID_APPID_CONSTRAINT` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp声明方言';

-- ----------------------------
-- Table structure for sp_claim_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sp_claim_mapping`;
CREATE TABLE `sp_claim_mapping` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `IDP_CLAIM` varchar(512) DEFAULT NULL COMMENT 'IDP索赔',
  `SP_CLAIM` varchar(512) DEFAULT NULL COMMENT 'sp索赔',
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  `IS_REQUESTED` varchar(128) DEFAULT NULL COMMENT '请求',
  `IS_MANDATORY` varchar(128) DEFAULT NULL COMMENT '强制',
  `DEFAULT_VALUE` varchar(255) DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`ID`),
  KEY `CLAIMID_APPID_CONSTRAINT` (`APP_ID`),
  CONSTRAINT `CLAIMID_APPID_CONSTRAINT` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp声明映射';

-- ----------------------------
-- Table structure for sp_federated_idp
-- ----------------------------
DROP TABLE IF EXISTS `sp_federated_idp`;
CREATE TABLE `sp_federated_idp` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) DEFAULT NULL COMMENT '租户ID',
  `AUTHENTICATOR_ID` int(11) NOT NULL COMMENT '认证者ID',
  PRIMARY KEY (`ID`,`AUTHENTICATOR_ID`),
  CONSTRAINT `STEP_ID_CONSTRAINT` FOREIGN KEY (`ID`) REFERENCES `sp_auth_step` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp联合idp';

-- ----------------------------
-- Table structure for sp_inbound_auth
-- ----------------------------
DROP TABLE IF EXISTS `sp_inbound_auth`;
CREATE TABLE `sp_inbound_auth` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `INBOUND_AUTH_KEY` varchar(255) DEFAULT NULL COMMENT 'inbound auth KEY',
  `INBOUND_AUTH_TYPE` varchar(255) DEFAULT NULL COMMENT 'inbound auth类型',
  `INBOUND_CONFIG_TYPE` varchar(255) DEFAULT NULL COMMENT 'inbound CONFIG类型',
  `PROP_NAME` varchar(255) DEFAULT NULL COMMENT 'PROP名称',
  `PROP_VALUE` varchar(1024) DEFAULT NULL COMMENT 'PROP VALUE',
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  PRIMARY KEY (`ID`),
  KEY `IDX_SPI_APP` (`APP_ID`),
  CONSTRAINT `APPLICATION_ID_CONSTRAINT` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp入站身份验证';

-- ----------------------------
-- Table structure for sp_metadata
-- ----------------------------
DROP TABLE IF EXISTS `sp_metadata`;
CREATE TABLE `sp_metadata` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `SP_ID` int(11) DEFAULT NULL COMMENT 'sp ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT 'NAME',
  `VALUE` varchar(255) DEFAULT NULL COMMENT 'VALUE',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL COMMENT '显示名称',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SP_METADATA_CONSTRAINT` (`SP_ID`,`NAME`),
  CONSTRAINT `sp_metadata_ibfk_1` FOREIGN KEY (`SP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp元数据';

-- ----------------------------
-- Table structure for sp_provisioning_connector
-- ----------------------------
DROP TABLE IF EXISTS `sp_provisioning_connector`;
CREATE TABLE `sp_provisioning_connector` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `IDP_NAME` varchar(255) DEFAULT NULL COMMENT 'IDP名称',
  `CONNECTOR_NAME` varchar(255) DEFAULT NULL COMMENT '连接器名称',
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  `IS_JIT_ENABLED` char(1) DEFAULT NULL COMMENT '是否JIT已启用',
  `BLOCKING` char(1) DEFAULT NULL COMMENT '阻止',
  `RULE_ENABLED` char(1) DEFAULT NULL COMMENT '已启用规则',
  PRIMARY KEY (`ID`),
  KEY `PRO_CONNECTOR_APPID_CONSTRAINT` (`APP_ID`),
  CONSTRAINT `PRO_CONNECTOR_APPID_CONSTRAINT` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp配置连接器';

-- ----------------------------
-- Table structure for sp_req_path_authenticator
-- ----------------------------
DROP TABLE IF EXISTS `sp_req_path_authenticator`;
CREATE TABLE `sp_req_path_authenticator` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `AUTHENTICATOR_NAME` varchar(255) DEFAULT NULL COMMENT '身份验证程序名称',
  `APP_ID` int(11) DEFAULT NULL COMMENT '应用程序ID',
  PRIMARY KEY (`ID`),
  KEY `REQ_AUTH_APPID_CONSTRAINT` (`APP_ID`),
  CONSTRAINT `REQ_AUTH_APPID_CONSTRAINT` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp req路径验证器';

-- ----------------------------
-- Table structure for sp_role_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sp_role_mapping`;
CREATE TABLE `sp_role_mapping` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TENANT_ID` int(11) NOT NULL,
  `IDP_ROLE` varchar(255) NOT NULL,
  `SP_ROLE` varchar(255) NOT NULL,
  `APP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ROLEID_APPID_CONSTRAINT` (`APP_ID`),
  CONSTRAINT `ROLEID_APPID_CONSTRAINT` FOREIGN KEY (`APP_ID`) REFERENCES `sp_app` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp角色映射';

-- ----------------------------
-- Table structure for sp_shared_app
-- ----------------------------
DROP TABLE IF EXISTS `sp_shared_app`;
CREATE TABLE `sp_shared_app` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `MAIN_APP_ID` char(36) DEFAULT NULL COMMENT '主应用程序ID',
  `OWNER_ORG_ID` char(36) DEFAULT NULL COMMENT '所有者ORG ID',
  `SHARED_APP_ID` char(36) DEFAULT NULL COMMENT '共享的应用程序ID',
  `SHARED_ORG_ID` char(36) DEFAULT NULL COMMENT '共享的组织ID',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SHARED_APP_ID` (`SHARED_APP_ID`),
  UNIQUE KEY `MAIN_APP_ID` (`MAIN_APP_ID`,`OWNER_ORG_ID`,`SHARED_ORG_ID`),
  CONSTRAINT `sp_shared_app_ibfk_1` FOREIGN KEY (`MAIN_APP_ID`) REFERENCES `sp_app` (`UUID`) ON DELETE CASCADE,
  CONSTRAINT `sp_shared_app_ibfk_2` FOREIGN KEY (`SHARED_APP_ID`) REFERENCES `sp_app` (`UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp共享应用';

-- ----------------------------
-- Table structure for sp_template
-- ----------------------------
DROP TABLE IF EXISTS `sp_template`;
CREATE TABLE `sp_template` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `TENANT_ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL COMMENT 'NAME',
  `DESCRIPTION` varchar(1023) DEFAULT NULL COMMENT '描述',
  `CONTENT` blob COMMENT '内容',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SP_TEMPLATE_CONSTRAINT` (`TENANT_ID`,`NAME`),
  KEY `IDX_SP_TEMPLATE` (`TENANT_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='sp模板';

-- ----------------------------
-- Table structure for wf_bps_pro
-- ----------------------------
DROP TABLE IF EXISTS `wf_bps_pro`;
CREATE TABLE `wf_bps_pro` (
  `PRO_NAME` varchar(45) NOT NULL COMMENT '档案名称',
  `HOST_URL_MANAGER` varchar(255) DEFAULT NULL COMMENT '主机URL管理器',
  `HOST_URL_WORKER` varchar(255) DEFAULT NULL COMMENT 'HOST URL WORKER',
  `USERNAME` varchar(100) DEFAULT NULL COMMENT 'USERNAME',
  `PASSWORD` varchar(1023) DEFAULT NULL COMMENT '密码',
  `CALLBACK_HOST` varchar(45) DEFAULT NULL COMMENT 'CALLBACK HOST',
  `CALLBACK_USERNAME` varchar(100) DEFAULT NULL COMMENT 'CALLBACK用户名',
  `CALLBACK_PASSWORD` varchar(255) DEFAULT NULL COMMENT '回拨密码',
  `TENANT_ID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`PRO_NAME`,`TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='wf bps配置文件';

-- ----------------------------
-- Table structure for wf_request
-- ----------------------------
DROP TABLE IF EXISTS `wf_request`;
CREATE TABLE `wf_request` (
  `UUID` varchar(45) NOT NULL COMMENT 'UUID',
  `CREATED_BY` varchar(255) DEFAULT NULL COMMENT '创建者',
  `TENANT_ID` int(11) DEFAULT '-1',
  `OPERATION_TYPE` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `CREATED_AT` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `UPDATED_AT` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `STATUS` varchar(30) DEFAULT NULL COMMENT '状态',
  `REQUEST` blob COMMENT '请求',
  PRIMARY KEY (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='wf请求';

-- ----------------------------
-- Table structure for wf_request_entity_relationship
-- ----------------------------
DROP TABLE IF EXISTS `wf_request_entity_relationship`;
CREATE TABLE `wf_request_entity_relationship` (
  `REQUEST_ID` varchar(45) NOT NULL COMMENT '请求ID',
  `ENTITY_NAME` varchar(255) NOT NULL COMMENT '实体名称',
  `ENTITY_TYPE` varchar(50) NOT NULL COMMENT '实体类型',
  `TENANT_ID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`REQUEST_ID`,`ENTITY_NAME`,`ENTITY_TYPE`,`TENANT_ID`),
  CONSTRAINT `wf_request_entity_relationship_ibfk_1` FOREIGN KEY (`REQUEST_ID`) REFERENCES `wf_request` (`UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='wf请求实体关系';

-- ----------------------------
-- Table structure for wf_workflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflow`;
CREATE TABLE `wf_workflow` (
  `ID` varchar(45) NOT NULL COMMENT 'ID',
  `WF_NAME` varchar(45) DEFAULT NULL COMMENT 'wf名称',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '描述',
  `TEMPLATE_ID` varchar(45) DEFAULT NULL COMMENT '模板ID',
  `IMPL_ID` varchar(45) DEFAULT NULL,
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='wf工作流';

-- ----------------------------
-- Table structure for wf_workflow_association
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflow_association`;
CREATE TABLE `wf_workflow_association` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `ASSOC_NAME` varchar(45) DEFAULT NULL COMMENT '协会名称',
  `EVENT_ID` varchar(45) DEFAULT NULL COMMENT '事件ID',
  `ASSOC_CONDITION` varchar(2000) DEFAULT NULL COMMENT '关联条件',
  `WORKFLOW_ID` varchar(45) DEFAULT NULL COMMENT '工作流ID',
  `IS_ENABLED` char(1) DEFAULT NULL COMMENT '已启用',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`ID`),
  KEY `WORKFLOW_ID` (`WORKFLOW_ID`),
  CONSTRAINT `wf_workflow_association_ibfk_1` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `wf_workflow` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='wf工作流关联';

-- ----------------------------
-- Table structure for wf_workflow_config_param
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflow_config_param`;
CREATE TABLE `wf_workflow_config_param` (
  `WORKFLOW_ID` varchar(45) NOT NULL COMMENT '工作流ID',
  `PARAM_NAME` varchar(45) NOT NULL COMMENT 'param NAME',
  `PARAM_VALUE` varchar(1000) DEFAULT NULL COMMENT 'param VALUE',
  `PARAM_QNAME` varchar(45) NOT NULL COMMENT 'param QNAME',
  `PARAM_HOLDER` varchar(45) NOT NULL COMMENT 'param-HOLDER',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`WORKFLOW_ID`,`PARAM_NAME`,`PARAM_QNAME`,`PARAM_HOLDER`),
  CONSTRAINT `wf_workflow_config_param_ibfk_1` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `wf_workflow` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='wf工作流配置参数';

-- ----------------------------
-- Table structure for wf_workflow_request_relation
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflow_request_relation`;
CREATE TABLE `wf_workflow_request_relation` (
  `RELATIONSHIP_ID` varchar(45) NOT NULL COMMENT '关系ID',
  `WORKFLOW_ID` varchar(45) DEFAULT NULL COMMENT '工作流ID',
  `REQUEST_ID` varchar(45) DEFAULT NULL COMMENT '请求ID',
  `UPDATED_AT` timestamp NULL DEFAULT NULL COMMENT '更新AT',
  `STATUS` varchar(30) DEFAULT NULL COMMENT '状态',
  `TENANT_ID` int(11) DEFAULT '-1',
  PRIMARY KEY (`RELATIONSHIP_ID`),
  KEY `WORKFLOW_ID` (`WORKFLOW_ID`),
  KEY `REQUEST_ID` (`REQUEST_ID`),
  CONSTRAINT `wf_workflow_request_relation_ibfk_1` FOREIGN KEY (`WORKFLOW_ID`) REFERENCES `wf_workflow` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `wf_workflow_request_relation_ibfk_2` FOREIGN KEY (`REQUEST_ID`) REFERENCES `wf_request` (`UUID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='wf工作流请求关系';
