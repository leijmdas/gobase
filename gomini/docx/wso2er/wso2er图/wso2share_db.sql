/*
Navicat MySQL Data Transfer

Source Server         : huawei.akunlong.top
Source Server Version : 50733
Source Host           : huawei.akunlong.top:13306
Source Database       : wso2share_db

Target Server Type    : MYSQL
Target Server Version : 50733
 Encoding         : 65001

Date: 2024-05-17 08:26:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for reg_association
-- ----------------------------
DROP TABLE IF EXISTS `reg_association`;
CREATE TABLE `reg_association` (
  `REG_ASSOCIATION_ID` int(11) NOT NULL COMMENT 'reg关联ID',
  `REG_SOURCEPATH` varchar(750) DEFAULT NULL COMMENT 'reg来源路径',
  `REG_TARGETPATH` varchar(750) DEFAULT NULL COMMENT 'reg目标路径',
  `REG_ASSOCIATION_TYPE` varchar(2000) DEFAULT NULL COMMENT 'reg关联类型',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_ASSOCIATION_ID`,`REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册关联';

-- ----------------------------
-- Table structure for reg_cluster_lock
-- ----------------------------
DROP TABLE IF EXISTS `reg_cluster_lock`;
CREATE TABLE `reg_cluster_lock` (
  `REG_LOCK_NAME` varchar(20) NOT NULL COMMENT 'reg lock NAME',
  `REG_LOCK_STATUS` varchar(20) DEFAULT NULL COMMENT 'reg锁定状态',
  `REG_LOCKED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg锁定时间',
  `REG_TENANT_ID` int(11) DEFAULT NULL COMMENT 'reg租户ID',
  PRIMARY KEY (`REG_LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册群集锁定';

-- ----------------------------
-- Table structure for reg_comment
-- ----------------------------
DROP TABLE IF EXISTS `reg_comment`;
CREATE TABLE `reg_comment` (
  `REG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REG_COMMENT_TEXT` varchar(500) DEFAULT NULL COMMENT 'reg注释文本',
  `REG_USER_ID` varchar(255) DEFAULT NULL COMMENT 'reg用户ID',
  `REG_COMMENTED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg注释时间',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_ID`,`REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册注释';

-- ----------------------------
-- Table structure for reg_content
-- ----------------------------
DROP TABLE IF EXISTS `reg_content`;
CREATE TABLE `reg_content` (
  `REG_CONTENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REG_CONTENT_DATA` longblob COMMENT 'reg内容数据',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_CONTENT_ID`,`REG_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=latin1 COMMENT='reg内容';

-- ----------------------------
-- Table structure for reg_content_history
-- ----------------------------
DROP TABLE IF EXISTS `reg_content_history`;
CREATE TABLE `reg_content_history` (
  `REG_CONTENT_ID` int(11) NOT NULL COMMENT 'reg内容ID',
  `REG_CONTENT_DATA` longblob COMMENT 'reg内容数据',
  `REG_DELETED` smallint(6) DEFAULT NULL COMMENT 'reg已删除',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT 'reg租户ID',
  PRIMARY KEY (`REG_CONTENT_ID`,`REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册内容历史记录';

-- ----------------------------
-- Table structure for reg_log
-- ----------------------------
DROP TABLE IF EXISTS `reg_log`;
CREATE TABLE `reg_log` (
  `REG_LOG_ID` int(11) NOT NULL COMMENT 'reg日志ID',
  `REG_PATH` varchar(750) DEFAULT NULL COMMENT 'reg路径',
  `REG_USER_ID` varchar(255) DEFAULT NULL COMMENT 'reg用户ID',
  `REG_LOGGED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg记录的时间',
  `REG_ACTION` int(11) DEFAULT NULL COMMENT 'reg ACTION',
  `REG_ACTION_DATA` varchar(500) DEFAULT NULL COMMENT 'reg动作数据',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_LOG_ID`,`REG_TENANT_ID`),
  KEY `REG_LOG_IND_BY_REGLOG` (`REG_LOGGED_TIME`,`REG_TENANT_ID`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册日志';

-- ----------------------------
-- Table structure for reg_path
-- ----------------------------
DROP TABLE IF EXISTS `reg_path`;
CREATE TABLE `reg_path` (
  `REG_PATH_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REG_PATH_VALUE` varchar(750) DEFAULT NULL COMMENT 'reg路径值',
  `REG_PATH_PARENT_ID` int(11) DEFAULT NULL COMMENT 'reg路径父ID',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_PATH_ID`,`REG_TENANT_ID`),
  UNIQUE KEY `UNIQUE_REG_PATH_TENANT_ID` (`REG_PATH_VALUE`,`REG_TENANT_ID`),
  KEY `REG_PATH_IND_BY_PATH_PARENT_ID` (`REG_PATH_PARENT_ID`,`REG_TENANT_ID`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=728 DEFAULT CHARSET=latin1 COMMENT='注册路径';

-- ----------------------------
-- Table structure for reg_property
-- ----------------------------
DROP TABLE IF EXISTS `reg_property`;
CREATE TABLE `reg_property` (
  `REG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REG_NAME` varchar(100) DEFAULT NULL COMMENT 'reg名称',
  `REG_VALUE` varchar(1000) DEFAULT NULL COMMENT 'reg值',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_ID`,`REG_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3002 DEFAULT CHARSET=latin1 COMMENT='reg属性';

-- ----------------------------
-- Table structure for reg_rating
-- ----------------------------
DROP TABLE IF EXISTS `reg_rating`;
CREATE TABLE `reg_rating` (
  `REG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REG_RATING` int(11) NOT NULL,
  `REG_USER_ID` varchar(255) DEFAULT NULL COMMENT 'reg用户ID',
  `REG_RATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg额定时间',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_ID`,`REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册评级';

-- ----------------------------
-- Table structure for reg_resource
-- ----------------------------
DROP TABLE IF EXISTS `reg_resource`;
CREATE TABLE `reg_resource` (
  `REG_PATH_ID` int(11) DEFAULT NULL COMMENT 'reg路径ID',
  `REG_NAME` varchar(256) DEFAULT NULL COMMENT 'reg名称',
  `REG_VERSION` int(11) NOT NULL COMMENT 'reg版本',
  `REG_MEDIA_TYPE` varchar(500) DEFAULT NULL COMMENT 'reg媒体类型',
  `REG_CREATOR` varchar(255) DEFAULT NULL COMMENT 'reg创建者',
  `REG_CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg创建时间',
  `REG_LAST_UPDATOR` varchar(255) DEFAULT NULL COMMENT 'reg最后一个更新程序',
  `REG_LAST_UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg上次更新时间',
  `REG_DESCRIPTION` varchar(1000) DEFAULT NULL COMMENT 'reg描述',
  `REG_CONTENT_ID` int(11) DEFAULT NULL COMMENT 'reg内容ID',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  `REG_UUID` varchar(100) DEFAULT NULL COMMENT 'reg UUID',
  PRIMARY KEY (`REG_VERSION`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_FK_BY_PATH_ID` (`REG_PATH_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_FK_BY_CONTENT_ID` (`REG_CONTENT_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_IND_BY_NAME` (`REG_NAME`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_IND_BY_PATH_ID_NAME` (`REG_PATH_ID`,`REG_NAME`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_IND_BY_UUID` (`REG_UUID`) USING HASH,
  KEY `REG_RESOURCE_IND_BY_TENAN` (`REG_TENANT_ID`,`REG_UUID`) USING HASH,
  KEY `REG_RESOURCE_IND_BY_TYPE` (`REG_TENANT_ID`,`REG_MEDIA_TYPE`) USING HASH,
  KEY `REG_RESOURCE_HISTORY_IND_BY_PATH_ID_NAME` (`REG_PATH_ID`,`REG_NAME`,`REG_TENANT_ID`) USING HASH,
  CONSTRAINT `REG_RESOURCE_FK_BY_CONTENT_ID` FOREIGN KEY (`REG_CONTENT_ID`, `REG_TENANT_ID`) REFERENCES `reg_content` (`REG_CONTENT_ID`, `REG_TENANT_ID`),
  CONSTRAINT `REG_RESOURCE_FK_BY_PATH_ID` FOREIGN KEY (`REG_PATH_ID`, `REG_TENANT_ID`) REFERENCES `reg_path` (`REG_PATH_ID`, `REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册资源';

-- ----------------------------
-- Table structure for reg_resource_comment
-- ----------------------------
DROP TABLE IF EXISTS `reg_resource_comment`;
CREATE TABLE `reg_resource_comment` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `REG_COMMENT_ID` int(11) DEFAULT NULL COMMENT 'reg comment ID',
  `REG_VERSION` int(11) DEFAULT NULL COMMENT 'reg版本',
  `REG_PATH_ID` int(11) DEFAULT NULL COMMENT 'reg路径ID',
  `REG_RESOURCE_NAME` varchar(256) DEFAULT NULL COMMENT 'reg资源名称',
  `REG_TENANT_ID` int(11) DEFAULT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`ID`),
  KEY `REG_RESOURCE_COMMENT_FK_BY_PATH_ID` (`REG_PATH_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_COMMENT_FK_BY_COMMENT_ID` (`REG_COMMENT_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_COMMENT_IND_BY_PATH_ID_AND_RESOURCE_NAME` (`REG_PATH_ID`,`REG_RESOURCE_NAME`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_COMMENT_IND_BY_VERSION` (`REG_VERSION`,`REG_TENANT_ID`) USING HASH,
  CONSTRAINT `REG_RESOURCE_COMMENT_FK_BY_COMMENT_ID` FOREIGN KEY (`REG_COMMENT_ID`, `REG_TENANT_ID`) REFERENCES `reg_comment` (`REG_ID`, `REG_TENANT_ID`),
  CONSTRAINT `REG_RESOURCE_COMMENT_FK_BY_PATH_ID` FOREIGN KEY (`REG_PATH_ID`, `REG_TENANT_ID`) REFERENCES `reg_path` (`REG_PATH_ID`, `REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册资源注释';

-- ----------------------------
-- Table structure for reg_resource_history
-- ----------------------------
DROP TABLE IF EXISTS `reg_resource_history`;
CREATE TABLE `reg_resource_history` (
  `REG_PATH_ID` int(11) DEFAULT NULL COMMENT 'reg路径ID',
  `REG_NAME` varchar(256) DEFAULT NULL COMMENT 'reg名称',
  `REG_VERSION` int(11) NOT NULL COMMENT 'reg版本',
  `REG_MEDIA_TYPE` varchar(500) DEFAULT NULL COMMENT 'reg媒体类型',
  `REG_CREATOR` varchar(255) DEFAULT NULL COMMENT 'reg创建者',
  `REG_CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg创建时间',
  `REG_LAST_UPDATOR` varchar(255) DEFAULT NULL COMMENT 'reg最后一个更新程序',
  `REG_LAST_UPDATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg上次更新时间',
  `REG_DESCRIPTION` varchar(1000) DEFAULT NULL COMMENT 'reg描述',
  `REG_CONTENT_ID` int(11) DEFAULT NULL COMMENT 'reg内容ID',
  `REG_DELETED` smallint(6) DEFAULT NULL COMMENT 'reg已删除',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT 'reg租户ID',
  `REG_UUID` varchar(100) DEFAULT NULL COMMENT 'reg UUID',
  PRIMARY KEY (`REG_VERSION`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_HIST_FK_BY_PATHID` (`REG_PATH_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_HIST_FK_BY_CONTENT_ID` (`REG_CONTENT_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_HISTORY_IND_BY_NAME` (`REG_NAME`,`REG_TENANT_ID`) USING HASH,
  CONSTRAINT `REG_RESOURCE_HIST_FK_BY_CONTENT_ID` FOREIGN KEY (`REG_CONTENT_ID`, `REG_TENANT_ID`) REFERENCES `reg_content_history` (`REG_CONTENT_ID`, `REG_TENANT_ID`),
  CONSTRAINT `REG_RESOURCE_HIST_FK_BY_PATHID` FOREIGN KEY (`REG_PATH_ID`, `REG_TENANT_ID`) REFERENCES `reg_path` (`REG_PATH_ID`, `REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册资源历史记录';

-- ----------------------------
-- Table structure for reg_resource_property
-- ----------------------------
DROP TABLE IF EXISTS `reg_resource_property`;
CREATE TABLE `reg_resource_property` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `REG_PROPERTY_ID` int(11) DEFAULT NULL COMMENT 'reg属性ID',
  `REG_VERSION` int(11) DEFAULT NULL COMMENT 'reg版本',
  `REG_PATH_ID` int(11) DEFAULT NULL COMMENT 'reg路径ID',
  `REG_RESOURCE_NAME` varchar(256) DEFAULT NULL COMMENT 'reg资源名称',
  `REG_TENANT_ID` int(11) DEFAULT NULL COMMENT 'reg租户ID',
  PRIMARY KEY (`ID`),
  KEY `REG_RESOURCE_PROPERTY_FK_BY_PATH_ID` (`REG_PATH_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_PROPERTY_FK_BY_TAG_ID` (`REG_PROPERTY_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_PROPERTY_IND_BY_PATH_ID_AND_RESOURCE_NAME` (`REG_PATH_ID`,`REG_RESOURCE_NAME`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_PROPERTY_IND_BY_VERSION` (`REG_VERSION`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_PROPERTY_IND_BY_REG_PROP_ID` (`REG_TENANT_ID`,`REG_PROPERTY_ID`),
  CONSTRAINT `REG_RESOURCE_PROPERTY_FK_BY_PATH_ID` FOREIGN KEY (`REG_PATH_ID`, `REG_TENANT_ID`) REFERENCES `reg_path` (`REG_PATH_ID`, `REG_TENANT_ID`),
  CONSTRAINT `REG_RESOURCE_PROPERTY_FK_BY_TAG_ID` FOREIGN KEY (`REG_PROPERTY_ID`, `REG_TENANT_ID`) REFERENCES `reg_property` (`REG_ID`, `REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册资源属性';

-- ----------------------------
-- Table structure for reg_resource_rating
-- ----------------------------
DROP TABLE IF EXISTS `reg_resource_rating`;
CREATE TABLE `reg_resource_rating` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `REG_RATING_ID` int(11) DEFAULT NULL COMMENT 'reg rating ID',
  `REG_VERSION` int(11) DEFAULT NULL COMMENT 'reg版本',
  `REG_PATH_ID` int(11) DEFAULT NULL COMMENT 'reg路径ID',
  `REG_RESOURCE_NAME` varchar(256) DEFAULT NULL COMMENT 'reg资源名称',
  `REG_TENANT_ID` int(11) DEFAULT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`ID`),
  KEY `REG_RESOURCE_RATING_FK_BY_PATH_ID` (`REG_PATH_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_RATING_FK_BY_RATING_ID` (`REG_RATING_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_RATING_IND_BY_PATH_ID_AND_RESOURCE_NAME` (`REG_PATH_ID`,`REG_RESOURCE_NAME`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_RATING_IND_BY_VERSION` (`REG_VERSION`,`REG_TENANT_ID`) USING HASH,
  CONSTRAINT `REG_RESOURCE_RATING_FK_BY_PATH_ID` FOREIGN KEY (`REG_PATH_ID`, `REG_TENANT_ID`) REFERENCES `reg_path` (`REG_PATH_ID`, `REG_TENANT_ID`),
  CONSTRAINT `REG_RESOURCE_RATING_FK_BY_RATING_ID` FOREIGN KEY (`REG_RATING_ID`, `REG_TENANT_ID`) REFERENCES `reg_rating` (`REG_ID`, `REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册资源评级';

-- ----------------------------
-- Table structure for reg_resource_tag
-- ----------------------------
DROP TABLE IF EXISTS `reg_resource_tag`;
CREATE TABLE `reg_resource_tag` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `REG_TAG_ID` int(11) DEFAULT NULL COMMENT 'reg标签ID',
  `REG_VERSION` int(11) DEFAULT NULL COMMENT 'reg版本',
  `REG_PATH_ID` int(11) DEFAULT NULL COMMENT 'reg路径ID',
  `REG_RESOURCE_NAME` varchar(256) DEFAULT NULL COMMENT 'reg资源名称',
  `REG_TENANT_ID` int(11) DEFAULT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`ID`),
  KEY `REG_RESOURCE_TAG_FK_BY_PATH_ID` (`REG_PATH_ID`,`REG_TENANT_ID`),
  KEY `REG_RESOURCE_TAG_IND_BY_PATH_ID_AND_RESOURCE_NAME` (`REG_PATH_ID`,`REG_RESOURCE_NAME`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_TAG_IND_BY_VERSION` (`REG_VERSION`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_RESOURCE_TAG_IND_BY_REG_TAG_ID` (`REG_TAG_ID`,`REG_TENANT_ID`) USING HASH,
  CONSTRAINT `REG_RESOURCE_TAG_FK_BY_PATH_ID` FOREIGN KEY (`REG_PATH_ID`, `REG_TENANT_ID`) REFERENCES `reg_path` (`REG_PATH_ID`, `REG_TENANT_ID`),
  CONSTRAINT `REG_RESOURCE_TAG_FK_BY_TAG_ID` FOREIGN KEY (`REG_TAG_ID`, `REG_TENANT_ID`) REFERENCES `reg_tag` (`REG_ID`, `REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册资源标记';

-- ----------------------------
-- Table structure for reg_snapshot
-- ----------------------------
DROP TABLE IF EXISTS `reg_snapshot`;
CREATE TABLE `reg_snapshot` (
  `REG_SNAPSHOT_ID` int(11) NOT NULL COMMENT 'reg快照ID',
  `REG_PATH_ID` int(11) DEFAULT NULL COMMENT 'reg路径ID',
  `REG_RESOURCE_NAME` varchar(255) DEFAULT NULL COMMENT 'reg资源名称',
  `REG_RESOURCE_VIDS` longblob COMMENT 'reg资源VIDS',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_SNAPSHOT_ID`,`REG_TENANT_ID`),
  KEY `REG_SNAPSHOT_IND_BY_PATH_ID_AND_RESOURCE_NAME` (`REG_PATH_ID`,`REG_RESOURCE_NAME`,`REG_TENANT_ID`) USING HASH,
  KEY `REG_SNAPSHOT_FK_BY_PATH_ID` (`REG_PATH_ID`,`REG_TENANT_ID`),
  CONSTRAINT `REG_SNAPSHOT_FK_BY_PATH_ID` FOREIGN KEY (`REG_PATH_ID`, `REG_TENANT_ID`) REFERENCES `reg_path` (`REG_PATH_ID`, `REG_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='注册快照';

-- ----------------------------
-- Table structure for reg_tag
-- ----------------------------
DROP TABLE IF EXISTS `reg_tag`;
CREATE TABLE `reg_tag` (
  `REG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REG_TAG_NAME` varchar(500) DEFAULT NULL COMMENT 'reg标签名称',
  `REG_USER_ID` varchar(255) DEFAULT NULL COMMENT 'reg用户ID',
  `REG_TAGGED_TIME` timestamp NULL DEFAULT NULL COMMENT 'reg标记的时间',
  `REG_TENANT_ID` int(11) NOT NULL COMMENT '注册租户ID',
  PRIMARY KEY (`REG_ID`,`REG_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='注册标签';

-- ----------------------------
-- Table structure for um_account_mapping
-- ----------------------------
DROP TABLE IF EXISTS `um_account_mapping`;
CREATE TABLE `um_account_mapping` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_USER_NAME` varchar(255) DEFAULT NULL COMMENT 'um用户名',
  `UM_TENANT_ID` int(11) DEFAULT NULL COMMENT 'um租户ID',
  `UM_USER_STORE_DOMAIN` varchar(100) DEFAULT NULL COMMENT 'um用户存储域',
  `UM_ACC_LINK_ID` int(11) DEFAULT NULL COMMENT 'um ACC链接ID',
  PRIMARY KEY (`UM_ID`),
  UNIQUE KEY `UM_USER_NAME` (`UM_USER_NAME`,`UM_TENANT_ID`,`UM_USER_STORE_DOMAIN`,`UM_ACC_LINK_ID`),
  KEY `UM_TENANT_ID` (`UM_TENANT_ID`),
  CONSTRAINT `um_account_mapping_ibfk_1` FOREIGN KEY (`UM_TENANT_ID`) REFERENCES `um_tenant` (`UM_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um帐户映射';

-- ----------------------------
-- Table structure for um_claim
-- ----------------------------
DROP TABLE IF EXISTS `um_claim`;
CREATE TABLE `um_claim` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_DIALECT_ID` int(11) DEFAULT NULL COMMENT 'um DIALECT ID',
  `UM_CLAIM_URI` varchar(255) DEFAULT NULL COMMENT 'um声明URI',
  `UM_DISPLAY_TAG` varchar(255) DEFAULT NULL COMMENT 'um显示标签',
  `UM_DESCRIPTION` varchar(255) DEFAULT NULL COMMENT 'um描述',
  `UM_MAPPED_ATTRIBUTE_DOMAIN` varchar(255) DEFAULT NULL COMMENT 'um映射的属性域',
  `UM_MAPPED_ATTRIBUTE` varchar(255) DEFAULT NULL COMMENT 'um映射的属性',
  `UM_REG_EX` varchar(255) DEFAULT NULL COMMENT 'um REG EX',
  `UM_SUPPORTED` smallint(6) DEFAULT NULL COMMENT 'um支持',
  `UM_REQUIRED` smallint(6) DEFAULT NULL COMMENT 'um必需',
  `UM_DISPLAY_ORDER` int(11) DEFAULT NULL COMMENT 'um显示顺序',
  `UM_CHECKED_ATTRIBUTE` smallint(6) DEFAULT NULL COMMENT 'um已检查的属性',
  `UM_READ_ONLY` smallint(6) DEFAULT NULL COMMENT 'um只读',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_DIALECT_ID` (`UM_DIALECT_ID`,`UM_CLAIM_URI`,`UM_TENANT_ID`,`UM_MAPPED_ATTRIBUTE_DOMAIN`),
  KEY `UM_DIALECT_ID_2` (`UM_DIALECT_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_claim_ibfk_1` FOREIGN KEY (`UM_DIALECT_ID`, `UM_TENANT_ID`) REFERENCES `um_dialect` (`UM_ID`, `UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um索赔';

-- ----------------------------
-- Table structure for um_claim_behavior
-- ----------------------------
DROP TABLE IF EXISTS `um_claim_behavior`;
CREATE TABLE `um_claim_behavior` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_PRO_ID` int(11) DEFAULT NULL COMMENT 'um档案ID',
  `UM_CLAIM_ID` int(11) DEFAULT NULL COMMENT 'um索赔ID',
  `UM_BEHAVIOUR` smallint(6) DEFAULT NULL COMMENT 'um行为',
  `UM_TENANT_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  KEY `UM_PRO_ID` (`UM_PRO_ID`,`UM_TENANT_ID`),
  KEY `UM_CLAIM_ID` (`UM_CLAIM_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_claim_behavior_ibfk_1` FOREIGN KEY (`UM_PRO_ID`, `UM_TENANT_ID`) REFERENCES `um_pro_config` (`UM_ID`, `UM_TENANT_ID`),
  CONSTRAINT `um_claim_behavior_ibfk_2` FOREIGN KEY (`UM_CLAIM_ID`, `UM_TENANT_ID`) REFERENCES `um_claim` (`UM_ID`, `UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for um_dialect
-- ----------------------------
DROP TABLE IF EXISTS `um_dialect`;
CREATE TABLE `um_dialect` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_DIALECT_URI` varchar(255) DEFAULT NULL COMMENT 'um dialect URI',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_DIALECT_URI` (`UM_DIALECT_URI`,`UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um方言';

-- ----------------------------
-- Table structure for um_domain
-- ----------------------------
DROP TABLE IF EXISTS `um_domain`;
CREATE TABLE `um_domain` (
  `UM_DOMAIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_DOMAIN_NAME` varchar(255) DEFAULT NULL COMMENT 'um域名',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_DOMAIN_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_DOMAIN_NAME` (`UM_DOMAIN_NAME`,`UM_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='um域';

-- ----------------------------
-- Table structure for um_group_uuid_domain_mapper
-- ----------------------------
DROP TABLE IF EXISTS `um_group_uuid_domain_mapper`;
CREATE TABLE `um_group_uuid_domain_mapper` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_GROUP_ID` varchar(255) DEFAULT NULL COMMENT 'um组ID',
  `UM_DOMAIN_ID` int(11) NOT NULL,
  `UM_TENANT_ID` int(11) DEFAULT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`),
  UNIQUE KEY `UM_GROUP_ID` (`UM_GROUP_ID`),
  KEY `UM_DOMAIN_ID` (`UM_DOMAIN_ID`,`UM_TENANT_ID`),
  KEY `GRP_UUID_DM_GRP_ID_TID` (`UM_GROUP_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_group_uuid_domain_mapper_ibfk_1` FOREIGN KEY (`UM_DOMAIN_ID`, `UM_TENANT_ID`) REFERENCES `um_domain` (`UM_DOMAIN_ID`, `UM_TENANT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for um_hybrid_group_role
-- ----------------------------
DROP TABLE IF EXISTS `um_hybrid_group_role`;
CREATE TABLE `um_hybrid_group_role` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_GROUP_NAME` varchar(255) DEFAULT NULL COMMENT 'um组名称',
  `UM_ROLE_ID` int(11) DEFAULT NULL COMMENT 'um角色ID',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  `UM_DOMAIN_ID` int(11) DEFAULT NULL COMMENT 'um域ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_GROUP_NAME` (`UM_GROUP_NAME`,`UM_ROLE_ID`,`UM_TENANT_ID`,`UM_DOMAIN_ID`),
  KEY `UM_ROLE_ID` (`UM_ROLE_ID`,`UM_TENANT_ID`),
  KEY `UM_DOMAIN_ID` (`UM_DOMAIN_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_hybrid_group_role_ibfk_1` FOREIGN KEY (`UM_ROLE_ID`, `UM_TENANT_ID`) REFERENCES `um_hybrid_role` (`UM_ID`, `UM_TENANT_ID`) ON DELETE CASCADE,
  CONSTRAINT `um_hybrid_group_role_ibfk_2` FOREIGN KEY (`UM_DOMAIN_ID`, `UM_TENANT_ID`) REFERENCES `um_domain` (`UM_DOMAIN_ID`, `UM_TENANT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um混合组角色';

-- ----------------------------
-- Table structure for um_hybrid_remember_me
-- ----------------------------
DROP TABLE IF EXISTS `um_hybrid_remember_me`;
CREATE TABLE `um_hybrid_remember_me` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_USER_NAME` varchar(255) DEFAULT NULL COMMENT 'um用户名',
  `UM_COOKIE_VALUE` varchar(1024) DEFAULT NULL COMMENT 'um COOKIE VALUE',
  `UM_CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'um创建时间',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um混血儿记住我';

-- ----------------------------
-- Table structure for um_hybrid_role
-- ----------------------------
DROP TABLE IF EXISTS `um_hybrid_role`;
CREATE TABLE `um_hybrid_role` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_ROLE_NAME` varchar(255) DEFAULT NULL COMMENT 'um角色名称',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_ROLE_NAME` (`UM_ROLE_NAME`,`UM_TENANT_ID`),
  KEY `UM_ROLE_NAME_IND` (`UM_ROLE_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='um混合角色';

-- ----------------------------
-- Table structure for um_hybrid_user_role
-- ----------------------------
DROP TABLE IF EXISTS `um_hybrid_user_role`;
CREATE TABLE `um_hybrid_user_role` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_USER_NAME` varchar(255) DEFAULT NULL COMMENT 'um用户名',
  `UM_ROLE_ID` int(11) DEFAULT NULL COMMENT 'um角色ID',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  `UM_DOMAIN_ID` int(11) DEFAULT NULL COMMENT 'um域ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_USER_NAME` (`UM_USER_NAME`,`UM_ROLE_ID`,`UM_TENANT_ID`,`UM_DOMAIN_ID`),
  KEY `UM_ROLE_ID` (`UM_ROLE_ID`,`UM_TENANT_ID`),
  KEY `UM_DOMAIN_ID` (`UM_DOMAIN_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_hybrid_user_role_ibfk_1` FOREIGN KEY (`UM_ROLE_ID`, `UM_TENANT_ID`) REFERENCES `um_hybrid_role` (`UM_ID`, `UM_TENANT_ID`) ON DELETE CASCADE,
  CONSTRAINT `um_hybrid_user_role_ibfk_2` FOREIGN KEY (`UM_DOMAIN_ID`, `UM_TENANT_ID`) REFERENCES `um_domain` (`UM_DOMAIN_ID`, `UM_TENANT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um混合用户角色';

-- ----------------------------
-- Table structure for um_module
-- ----------------------------
DROP TABLE IF EXISTS `um_module`;
CREATE TABLE `um_module` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_MODULE_NAME` varchar(100) DEFAULT NULL COMMENT 'um模块名称',
  PRIMARY KEY (`UM_ID`),
  UNIQUE KEY `UM_MODULE_NAME` (`UM_MODULE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um模块';

-- ----------------------------
-- Table structure for um_module_actions
-- ----------------------------
DROP TABLE IF EXISTS `um_module_actions`;
CREATE TABLE `um_module_actions` (
  `UM_ACTION` varchar(255) NOT NULL COMMENT 'um ACTION',
  `UM_MODULE_ID` int(11) NOT NULL COMMENT 'um模块ID',
  PRIMARY KEY (`UM_ACTION`,`UM_MODULE_ID`),
  KEY `UM_MODULE_ID` (`UM_MODULE_ID`),
  CONSTRAINT `um_module_actions_ibfk_1` FOREIGN KEY (`UM_MODULE_ID`) REFERENCES `um_module` (`UM_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um模块操作';

-- ----------------------------
-- Table structure for um_org
-- ----------------------------
DROP TABLE IF EXISTS `um_org`;
CREATE TABLE `um_org` (
  `UM_ID` varchar(36) NOT NULL COMMENT 'um ID',
  `UM_ORG_NAME` varchar(255) DEFAULT NULL COMMENT 'um组织名称',
  `UM_ORG_DESCRIPTION` varchar(1024) DEFAULT NULL COMMENT 'um组织描述',
  `UM_CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT 'um创建时间',
  `UM_LAST_MODIFIED` timestamp NULL DEFAULT NULL COMMENT 'um上次修改',
  `UM_STATUS` varchar(255) DEFAULT NULL COMMENT 'um状态',
  `UM_PARENT_ID` varchar(36) DEFAULT NULL COMMENT 'um家长ID',
  `UM_ORG_TYPE` varchar(100) DEFAULT NULL COMMENT 'um组织类型',
  PRIMARY KEY (`UM_ID`),
  KEY `UM_PARENT_ID` (`UM_PARENT_ID`),
  CONSTRAINT `um_org_ibfk_1` FOREIGN KEY (`UM_PARENT_ID`) REFERENCES `um_org` (`UM_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织';

-- ----------------------------
-- Table structure for um_org_attribute
-- ----------------------------
DROP TABLE IF EXISTS `um_org_attribute`;
CREATE TABLE `um_org_attribute` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_ORG_ID` varchar(36) DEFAULT NULL COMMENT 'um org ID',
  `UM_ATTRIBUTE_KEY` varchar(255) DEFAULT NULL COMMENT 'um属性键',
  `UM_ATTRIBUTE_VALUE` varchar(512) DEFAULT NULL COMMENT 'um属性值',
  PRIMARY KEY (`UM_ID`),
  UNIQUE KEY `UM_ORG_ID` (`UM_ORG_ID`,`UM_ATTRIBUTE_KEY`),
  CONSTRAINT `um_org_attribute_ibfk_1` FOREIGN KEY (`UM_ORG_ID`) REFERENCES `um_org` (`UM_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织属性';

-- ----------------------------
-- Table structure for um_org_hierarchy
-- ----------------------------
DROP TABLE IF EXISTS `um_org_hierarchy`;
CREATE TABLE `um_org_hierarchy` (
  `UM_PARENT_ID` varchar(36) NOT NULL COMMENT 'um家长ID',
  `UM_ID` varchar(36) NOT NULL COMMENT 'um ID',
  `DEPTH` int(11) DEFAULT NULL,
  PRIMARY KEY (`UM_PARENT_ID`,`UM_ID`),
  KEY `UM_ID` (`UM_ID`),
  CONSTRAINT `um_org_hierarchy_ibfk_1` FOREIGN KEY (`UM_PARENT_ID`) REFERENCES `um_org` (`UM_ID`) ON DELETE CASCADE,
  CONSTRAINT `um_org_hierarchy_ibfk_2` FOREIGN KEY (`UM_ID`) REFERENCES `um_org` (`UM_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织层次结构';

-- ----------------------------
-- Table structure for um_org_permission
-- ----------------------------
DROP TABLE IF EXISTS `um_org_permission`;
CREATE TABLE `um_org_permission` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_RESOURCE_ID` varchar(255) DEFAULT NULL COMMENT 'um资源ID',
  `UM_ACTION` varchar(255) DEFAULT NULL COMMENT 'um ACTION',
  `UM_TENANT_ID` int(11) DEFAULT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织权限';

-- ----------------------------
-- Table structure for um_org_role
-- ----------------------------
DROP TABLE IF EXISTS `um_org_role`;
CREATE TABLE `um_org_role` (
  `UM_ROLE_ID` varchar(255) NOT NULL COMMENT 'um角色ID',
  `UM_ROLE_NAME` varchar(255) DEFAULT NULL COMMENT 'um角色名称',
  `UM_ORG_ID` varchar(36) DEFAULT NULL COMMENT 'um org ID',
  PRIMARY KEY (`UM_ROLE_ID`),
  KEY `FK_UM_ORG_ROLE_UM_ORG` (`UM_ORG_ID`),
  CONSTRAINT `FK_UM_ORG_ROLE_UM_ORG` FOREIGN KEY (`UM_ORG_ID`) REFERENCES `um_org` (`UM_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织角色';

-- ----------------------------
-- Table structure for um_org_role_group
-- ----------------------------
DROP TABLE IF EXISTS `um_org_role_group`;
CREATE TABLE `um_org_role_group` (
  `UM_GROUP_ID` varchar(255) DEFAULT NULL COMMENT 'um组ID',
  `UM_ROLE_ID` varchar(255) DEFAULT NULL COMMENT 'um角色ID',
  KEY `FK_UM_ORG_ROLE_GROUP_UM_ORG_ROLE` (`UM_ROLE_ID`),
  CONSTRAINT `FK_UM_ORG_ROLE_GROUP_UM_ORG_ROLE` FOREIGN KEY (`UM_ROLE_ID`) REFERENCES `um_org_role` (`UM_ROLE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织角色组';

-- ----------------------------
-- Table structure for um_org_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `um_org_role_permission`;
CREATE TABLE `um_org_role_permission` (
  `UM_PERMISSION_ID` int(11) DEFAULT NULL COMMENT 'um权限ID',
  `UM_ROLE_ID` varchar(255) DEFAULT NULL COMMENT 'um角色ID',
  KEY `FK_UM_ORG_ROLE_PERMISSION_UM_ORG_ROLE` (`UM_ROLE_ID`),
  KEY `FK_UM_ORG_ROLE_PERMISSION_UM_ORG_PERMISSION` (`UM_PERMISSION_ID`),
  CONSTRAINT `FK_UM_ORG_ROLE_PERMISSION_UM_ORG_PERMISSION` FOREIGN KEY (`UM_PERMISSION_ID`) REFERENCES `um_org_permission` (`UM_ID`) ON DELETE CASCADE,
  CONSTRAINT `FK_UM_ORG_ROLE_PERMISSION_UM_ORG_ROLE` FOREIGN KEY (`UM_ROLE_ID`) REFERENCES `um_org_role` (`UM_ROLE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织角色权限';

-- ----------------------------
-- Table structure for um_org_role_user
-- ----------------------------
DROP TABLE IF EXISTS `um_org_role_user`;
CREATE TABLE `um_org_role_user` (
  `UM_USER_ID` varchar(255) DEFAULT NULL COMMENT 'um用户ID',
  `UM_ROLE_ID` varchar(255) DEFAULT NULL COMMENT 'um角色ID',
  KEY `FK_UM_ORG_ROLE_USER_UM_ORG_ROLE` (`UM_ROLE_ID`),
  CONSTRAINT `FK_UM_ORG_ROLE_USER_UM_ORG_ROLE` FOREIGN KEY (`UM_ROLE_ID`) REFERENCES `um_org_role` (`UM_ROLE_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um组织角色用户';

-- ----------------------------
-- Table structure for um_permission
-- ----------------------------
DROP TABLE IF EXISTS `um_permission`;
CREATE TABLE `um_permission` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_RESOURCE_ID` varchar(255) DEFAULT NULL COMMENT 'um资源ID',
  `UM_ACTION` varchar(255) DEFAULT NULL COMMENT 'um ACTION',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  `UM_MODULE_ID` int(11) DEFAULT NULL COMMENT 'um模块ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_RESOURCE_ID` (`UM_RESOURCE_ID`,`UM_ACTION`,`UM_TENANT_ID`),
  KEY `INDEX_UM_PERMISSION_UM_RESOURCE_ID_UM_ACTION` (`UM_RESOURCE_ID`,`UM_ACTION`,`UM_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1 COMMENT='um权限';

-- ----------------------------
-- Table structure for um_pro_config
-- ----------------------------
DROP TABLE IF EXISTS `um_pro_config`;
CREATE TABLE `um_pro_config` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_DIALECT_ID` int(11) DEFAULT NULL COMMENT 'um DIALECT ID',
  `UM_PRO_NAME` varchar(255) DEFAULT NULL COMMENT 'um档案名称',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  KEY `UM_DIALECT_ID` (`UM_DIALECT_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_pro_config_ibfk_1` FOREIGN KEY (`UM_DIALECT_ID`, `UM_TENANT_ID`) REFERENCES `um_dialect` (`UM_ID`, `UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um配置文件配置';

-- ----------------------------
-- Table structure for um_role
-- ----------------------------
DROP TABLE IF EXISTS `um_role`;
CREATE TABLE `um_role` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_ROLE_NAME` varchar(255) DEFAULT NULL COMMENT 'um角色名称',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  `UM_SHARED_ROLE` tinyint(1) DEFAULT NULL COMMENT 'um共享角色',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_ROLE_NAME` (`UM_ROLE_NAME`,`UM_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='um角色';

-- ----------------------------
-- Table structure for um_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `um_role_permission`;
CREATE TABLE `um_role_permission` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_PERMISSION_ID` int(11) DEFAULT NULL COMMENT 'um权限ID',
  `UM_ROLE_NAME` varchar(255) DEFAULT NULL COMMENT 'um角色名称',
  `UM_IS_ALLOWED` smallint(6) DEFAULT NULL COMMENT 'um被允许',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  `UM_DOMAIN_ID` int(11) DEFAULT NULL COMMENT 'um域ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_PERMISSION_ID` (`UM_PERMISSION_ID`,`UM_ROLE_NAME`,`UM_TENANT_ID`,`UM_DOMAIN_ID`),
  KEY `UM_PERMISSION_ID_2` (`UM_PERMISSION_ID`,`UM_TENANT_ID`),
  KEY `UM_DOMAIN_ID` (`UM_DOMAIN_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_role_permission_ibfk_1` FOREIGN KEY (`UM_PERMISSION_ID`, `UM_TENANT_ID`) REFERENCES `um_permission` (`UM_ID`, `UM_TENANT_ID`) ON DELETE CASCADE,
  CONSTRAINT `um_role_permission_ibfk_2` FOREIGN KEY (`UM_DOMAIN_ID`, `UM_TENANT_ID`) REFERENCES `um_domain` (`UM_DOMAIN_ID`, `UM_TENANT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um角色权限';

-- ----------------------------
-- Table structure for um_shared_user_role
-- ----------------------------
DROP TABLE IF EXISTS `um_shared_user_role`;
CREATE TABLE `um_shared_user_role` (
  `ID` int(11) NOT NULL COMMENT 'ID',
  `UM_ROLE_ID` int(11) DEFAULT NULL COMMENT 'um角色ID',
  `UM_USER_ID` int(11) DEFAULT NULL COMMENT 'um用户ID',
  `UM_USER_TENANT_ID` int(11) DEFAULT NULL COMMENT 'um用户租户ID',
  `UM_ROLE_TENANT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UM_USER_ID` (`UM_USER_ID`,`UM_ROLE_ID`,`UM_USER_TENANT_ID`,`UM_ROLE_TENANT_ID`),
  KEY `UM_ROLE_ID` (`UM_ROLE_ID`,`UM_ROLE_TENANT_ID`),
  KEY `UM_USER_ID_2` (`UM_USER_ID`,`UM_USER_TENANT_ID`),
  CONSTRAINT `um_shared_user_role_ibfk_1` FOREIGN KEY (`UM_ROLE_ID`, `UM_ROLE_TENANT_ID`) REFERENCES `um_role` (`UM_ID`, `UM_TENANT_ID`) ON DELETE CASCADE,
  CONSTRAINT `um_shared_user_role_ibfk_2` FOREIGN KEY (`UM_USER_ID`, `UM_USER_TENANT_ID`) REFERENCES `um_user` (`UM_ID`, `UM_TENANT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um个共享用户角色';

-- ----------------------------
-- Table structure for um_system_role
-- ----------------------------
DROP TABLE IF EXISTS `um_system_role`;
CREATE TABLE `um_system_role` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_ROLE_NAME` varchar(255) DEFAULT NULL COMMENT 'um角色名称',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_ROLE_NAME` (`UM_ROLE_NAME`,`UM_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='um系统角色';

-- ----------------------------
-- Table structure for um_system_user
-- ----------------------------
DROP TABLE IF EXISTS `um_system_user`;
CREATE TABLE `um_system_user` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_USER_NAME` varchar(255) DEFAULT NULL COMMENT 'um用户名',
  `UM_USER_PASSWORD` varchar(255) DEFAULT NULL COMMENT 'um用户密码',
  `UM_SALT_VALUE` varchar(31) DEFAULT NULL COMMENT 'um盐值',
  `UM_REQUIRE_CHANGE` tinyint(1) DEFAULT NULL COMMENT 'um需要更改',
  `UM_CHANGED_TIME` timestamp NULL DEFAULT NULL COMMENT 'um更改时间',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_USER_NAME` (`UM_USER_NAME`,`UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um系统用户';

-- ----------------------------
-- Table structure for um_system_user_role
-- ----------------------------
DROP TABLE IF EXISTS `um_system_user_role`;
CREATE TABLE `um_system_user_role` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_USER_NAME` varchar(255) DEFAULT NULL COMMENT 'um用户名',
  `UM_ROLE_ID` int(11) DEFAULT NULL COMMENT 'um角色ID',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_USER_NAME` (`UM_USER_NAME`,`UM_ROLE_ID`,`UM_TENANT_ID`),
  KEY `UM_ROLE_ID` (`UM_ROLE_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_system_user_role_ibfk_1` FOREIGN KEY (`UM_ROLE_ID`, `UM_TENANT_ID`) REFERENCES `um_system_role` (`UM_ID`, `UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um系统用户角色';

-- ----------------------------
-- Table structure for um_tenant
-- ----------------------------
DROP TABLE IF EXISTS `um_tenant`;
CREATE TABLE `um_tenant` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_TENANT_UUID` varchar(36) DEFAULT NULL COMMENT 'um租户UUID',
  `UM_DOMAIN_NAME` varchar(255) DEFAULT NULL COMMENT 'um域名',
  `UM_EMAIL` varchar(255) DEFAULT NULL COMMENT 'um电子邮件',
  `UM_ACTIVE` tinyint(1) DEFAULT NULL COMMENT 'um活动',
  `UM_CREATED_DATE` timestamp NULL DEFAULT NULL COMMENT 'um创建日期',
  `UM_USER_CONFIG` longblob COMMENT 'um用户配置',
  `UM_ORG_UUID` varchar(36) DEFAULT NULL COMMENT 'um ORG UUID',
  PRIMARY KEY (`UM_ID`),
  UNIQUE KEY `UM_DOMAIN_NAME` (`UM_DOMAIN_NAME`),
  UNIQUE KEY `INDEX_UM_TENANT_UM_DOMAIN_NAME` (`UM_DOMAIN_NAME`),
  UNIQUE KEY `UM_TENANT_UUID` (`UM_TENANT_UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um租户';

-- ----------------------------
-- Table structure for um_user
-- ----------------------------
DROP TABLE IF EXISTS `um_user`;
CREATE TABLE `um_user` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_USER_ID` varchar(255) DEFAULT NULL COMMENT 'um用户ID',
  `UM_USER_NAME` varchar(255) DEFAULT NULL COMMENT 'um用户名',
  `UM_USER_PASSWORD` varchar(255) DEFAULT NULL COMMENT 'um用户密码',
  `UM_SALT_VALUE` varchar(31) DEFAULT NULL COMMENT 'um盐值',
  `UM_REQUIRE_CHANGE` tinyint(1) DEFAULT NULL COMMENT 'um需要更改',
  `UM_CHANGED_TIME` timestamp NULL DEFAULT NULL COMMENT 'um更改时间',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_USER_NAME` (`UM_USER_NAME`,`UM_TENANT_ID`),
  UNIQUE KEY `INDEX_UM_USERNAME_UM_TENANT_ID` (`UM_USER_NAME`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_USER_ID` (`UM_USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='um用户';

-- ----------------------------
-- Table structure for um_user_attribute
-- ----------------------------
DROP TABLE IF EXISTS `um_user_attribute`;
CREATE TABLE `um_user_attribute` (
  `UM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UM_ATTR_NAME` varchar(255) NOT NULL,
  `UM_ATTR_VALUE` varchar(1024) DEFAULT NULL,
  `UM_PRO_ID` varchar(255) DEFAULT NULL,
  `UM_USER_ID` int(11) DEFAULT NULL,
  `UM_TENANT_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  KEY `UM_USER_ID` (`UM_USER_ID`,`UM_TENANT_ID`),
  KEY `UM_USER_ID_INDEX` (`UM_USER_ID`),
  KEY `UM_ATTR_NAME_VALUE_INDEX` (`UM_ATTR_NAME`,`UM_ATTR_VALUE`(512)),
  CONSTRAINT `um_user_attribute_ibfk_1` FOREIGN KEY (`UM_USER_ID`, `UM_TENANT_ID`) REFERENCES `um_user` (`UM_ID`, `UM_TENANT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='um用户属性';

-- ----------------------------
-- Table structure for um_user_permission
-- ----------------------------
DROP TABLE IF EXISTS `um_user_permission`;
CREATE TABLE `um_user_permission` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_PERMISSION_ID` int(11) DEFAULT NULL COMMENT 'um权限ID',
  `UM_USER_NAME` varchar(255) DEFAULT NULL COMMENT 'um用户名',
  `UM_IS_ALLOWED` smallint(6) DEFAULT NULL COMMENT 'um被允许',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  KEY `UM_PERMISSION_ID` (`UM_PERMISSION_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_user_permission_ibfk_1` FOREIGN KEY (`UM_PERMISSION_ID`, `UM_TENANT_ID`) REFERENCES `um_permission` (`UM_ID`, `UM_TENANT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um用户权限';

-- ----------------------------
-- Table structure for um_user_role
-- ----------------------------
DROP TABLE IF EXISTS `um_user_role`;
CREATE TABLE `um_user_role` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_ROLE_ID` int(11) DEFAULT NULL COMMENT 'um角色ID',
  `UM_USER_ID` int(11) DEFAULT NULL COMMENT 'um用户ID',
  `UM_TENANT_ID` int(11) NOT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`,`UM_TENANT_ID`),
  UNIQUE KEY `UM_USER_ID` (`UM_USER_ID`,`UM_ROLE_ID`,`UM_TENANT_ID`),
  KEY `UM_ROLE_ID` (`UM_ROLE_ID`,`UM_TENANT_ID`),
  KEY `UM_USER_ID_2` (`UM_USER_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_user_role_ibfk_1` FOREIGN KEY (`UM_ROLE_ID`, `UM_TENANT_ID`) REFERENCES `um_role` (`UM_ID`, `UM_TENANT_ID`),
  CONSTRAINT `um_user_role_ibfk_2` FOREIGN KEY (`UM_USER_ID`, `UM_TENANT_ID`) REFERENCES `um_user` (`UM_ID`, `UM_TENANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um用户角色';

-- ----------------------------
-- Table structure for um_uuid_domain_mapper
-- ----------------------------
DROP TABLE IF EXISTS `um_uuid_domain_mapper`;
CREATE TABLE `um_uuid_domain_mapper` (
  `UM_ID` int(11) NOT NULL COMMENT 'um ID',
  `UM_USER_ID` varchar(255) DEFAULT NULL COMMENT 'um用户ID',
  `UM_DOMAIN_ID` int(11) DEFAULT NULL COMMENT 'um域ID',
  `UM_TENANT_ID` int(11) DEFAULT NULL COMMENT 'um租户ID',
  PRIMARY KEY (`UM_ID`),
  UNIQUE KEY `UM_USER_ID` (`UM_USER_ID`),
  KEY `UM_DOMAIN_ID` (`UM_DOMAIN_ID`,`UM_TENANT_ID`),
  KEY `UUID_DM_UID_TID` (`UM_USER_ID`,`UM_TENANT_ID`),
  CONSTRAINT `um_uuid_domain_mapper_ibfk_1` FOREIGN KEY (`UM_DOMAIN_ID`, `UM_TENANT_ID`) REFERENCES `um_domain` (`UM_DOMAIN_ID`, `UM_TENANT_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='um uuid域映射器';
