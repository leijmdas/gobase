package cyclocomplex

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/gomini/mini/cyclocomplex/internal"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"strings"
)

const (
	ScanMode_Cc = 1 << iota
	ScanMode_ALL
	ScanMode_File
)

// go install github.com/fzipp/gocyclo/cmd/gocyclo@latest
// rem gocy
const (
	ccPath        = "/data/cc/"
	ccPathCmdFile = "/data/cc/cmd.bat"
	ccReportFile  = "-cc-report.txt"
	cmd           = "gocyclo -top 5 ./core"
	topNum        = 100
)

type CycloComplex struct {
	basedto.BaseEntitySingle
	Dirs             []string
	Report           []string
	Rootdir, DestDir string
	Cmd              string
	Cmds             []string
	topNum           int
	ScanMode         int
	ccFilename       string
}

func NewCycloComplex() *CycloComplex {
	var cc = &CycloComplex{
		ScanMode: ScanMode_Cc,
		Rootdir:  fileutils.FindRootDir(),
		Report:   make([]string, 0),
		DestDir:  fileutils.FindRootDir() + ccPath,
		Cmds:     make([]string, 0),
		Cmd:      fileutils.FindRootDir() + ccPathCmdFile,
		topNum:   topNum,
	}
	cc.InitProxy(cc)
	return cc
}

func (cc *CycloComplex) FindDirs(rootdir string) error {
	var err error
	cc.Dirs, err = fileutils.FindDirs(rootdir)
	cc.Rootdir = rootdir
	return err
}
func (this *CycloComplex) ExistGoFiles(dirPath string) (bool, error) {
	var exist = false
	err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			goutils.Error(err)
			return err
		}
		// 如果是文件，可以进行额外操作，比如读取文件内容
		if !info.IsDir() {
			if strings.HasSuffix(path, ".go") {

				exist = true
				return nil
			}
		}
		// 返回nil继续遍历
		return nil
	})

	if err != nil {
		logrus.Error("Error walking the path:", err, exist)
		return exist, err
	}
	return exist, err
}
func (cc *CycloComplex) procTopNum(topNums ...int) {
	if len(topNums) > 0 {
		cc.topNum = topNums[0]
		if cc.topNum == -1 {
			cc.topNum = 10000
		}
	}
}

func (cc *CycloComplex) CcCheckFileScan(file string, topNums ...int) error {
	cc.ScanMode = ScanMode_File
	cc.procTopNum(topNum)
	if len(cc.Dirs) == 0 {
		cc.FindDirs(fileutils.FindRootDir())
	}
	cc.Ccrmdir()
	cc.ScanFile(file)
	cc.ExecScanCmd()
	//fmt.Println("report =", strings.Join(cc.Report, "\r\n"))
	var ccReport, err = fileutils.ReadFileToString(cc.ccFilename)
	logrus.Info(ccReport)
	return err
}

func (cc *CycloComplex) CcCheckScan(topNums ...int) {
	cc.ScanMode = ScanMode_Cc
	cc.procTopNum(topNum)
	if len(cc.Dirs) == 0 {
		cc.FindDirs(fileutils.FindRootDir())
	}
	cc.Ccrmdir()
	cc.Scan()
	cc.ExecScanCmd()
}
func (cc *CycloComplex) CcCheckAllScan(topNums ...int) {
	cc.ScanMode = ScanMode_ALL
	cc.procTopNum(topNum)
	if len(cc.Dirs) == 0 {
		cc.FindDirs(fileutils.FindRootDir())

	}
	cc.Ccrmdir()
	cc.ScanAll()
	cc.ExecScanCmd()
}
func (cc *CycloComplex) Ccrmdir() {
	os.Remove(cc.DestDir)
}
func (cc *CycloComplex) CcMkdir() {
	os.MkdirAll(cc.DestDir, os.ModeDir)
}
func (cc *CycloComplex) Scan() ([]string, error) {
	cc.CcMkdir()
	os.Chdir(cc.Rootdir)
	for _, dir := range cc.Dirs {
		if strings.HasPrefix(dir, ".") {
			continue
		}
		var godir = cc.Rootdir + "/" + dir
		var cmd = "gocyclo -top " + gconv.String(cc.topNum) + " " + godir + " > " + cc.DestDir + dir + ccReportFile

		if exist, _ := cc.ExistGoFiles(godir); !exist {
			continue
		}
		var res, err = internal.FindBeanOsCmd().ScanCmd(cmd)
		if err != nil {
			cc.Cmds = append(cc.Cmds, cmd)
		}
		cc.Report = append(cc.Report, res)
	}
	fileutils.WriteBytesFile(cc.Rootdir+ccPathCmdFile, []byte(strings.Join(cc.Cmds, "\r\n")))
	return cc.Report, nil
}
func (cc *CycloComplex) ScanAll() ([]string, error) {
	cc.CcMkdir()
	os.Chdir(cc.Rootdir)

	var godir = cc.Rootdir + "/"
	var cmd = "gocyclo -top " + gconv.String(cc.topNum) + " " + godir + " > " + cc.DestDir + "all" + ccReportFile

	var res, err = internal.FindBeanOsCmd().ScanCmd(cmd)
	if err != nil {
		cc.Cmds = append(cc.Cmds, cmd)
	}
	cc.Report = append(cc.Report, res)
	fileutils.WriteBytesFile(cc.Rootdir+ccPathCmdFile, []byte(strings.Join(cc.Cmds, "\r\n")))
	return cc.Report, nil
}

func (cc *CycloComplex) ScanFile(file string) ([]string, error) {
	if !fileutils.TryFileExist(file) {
		logrus.Error("file not exist", file)
		panic("file not exist")
		return cc.Report, nil
	}
	cc.CcMkdir()

	cc.ccFilename = cc.DestDir + gfile.Basename(file)

	var cmd = "gocyclo -top " + gconv.String(cc.topNum) + " " + file + " > " + cc.ccFilename + ccReportFile
	var res, err = internal.FindBeanOsCmd().ScanCmd(cmd)
	if err != nil {
		cc.Cmds = append(cc.Cmds, cmd)
	}
	cc.Report = append(cc.Report, res)
	fileutils.WriteBytesFile(cc.Rootdir+ccPathCmdFile, []byte(strings.Join(cc.Cmds, "\r\n")))
	return cc.Report, nil
}
func (cc *CycloComplex) ExecScanCmd() {
	internal.FindBeanOsCmd().ScanCmd(cc.Cmd)
}
