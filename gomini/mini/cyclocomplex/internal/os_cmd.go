package internal

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"github.com/sirupsen/logrus"
	"os/exec"
	"strings"
)

//var ProtoPath = "D:/go-ichub/go/gowebcode/code/mysql/db/gorpc/proto"

type OsCmd struct {
	basedto.BaseEntitySingle
	Cmd     string
	rootdir string
	CmdPath string
}

func NewOsCmd() *OsCmd {
	var osCMd = &OsCmd{
		rootdir: fileutils.FindRootDir(),
	}
	osCMd.CmdPath = osCMd.rootdir + "/code/"
	osCMd.Cmd = "cd " + osCMd.CmdPath + ";./protoc --plugin=protoc-gen-go=protoc-gen-go.exe --plugin=protoc-gen-micro=protoc-gen-micro.exe --micro_out={{.ProtoPath}}/ --go_out= {{.ProtoPath}}/  {{.ProtoPath}}/*.proto"

	return osCMd
}
func (self *OsCmd) ScanCmd(cmd string) (string, error) {
	logrus.Info(cmd)
	cmdResult := exec.Command(cmd)
	//cmdResult.Start()
	//cmdResult.Wait()
	stdout, err := cmdResult.Output()
	if err != nil {
		if strings.Contains(err.Error(), "file does not") {
			return "", err
		}
		goutils.Error(err.Error())
		return "", err
	}

	return string(stdout), nil
}

func (self *OsCmd) ExecCmd(cmd string) error {

	goutils.Info(cmd)
	cmdResult := exec.Command(cmd)
	out, err := cmdResult.Output()
	if err != nil {
		goutils.Error(err.Error())
		return err
	}
	goutils.Info(string(out))
	return nil

}

//	var cmd = "protoc --plugin=protoc-gen-go=protoc-gen-go.exe  --plugin=protoc-gen-micro=protoc-gen-micro.exe --micro_out={{.ProtoPath}}/ --go_out=proto/  {{.ProtoPath}}/*.proto"
//	cmd = strings.Replace(cmd, "{{.ProtoPath}}", protopath, -1)
