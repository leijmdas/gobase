package di

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/godi/di/difactroy"
	"gitee.com/leijmdas/gobase/gomini/mini/file/minilog"
	"go/build"
	"go/importer"

	"gitee.com/leijmdas/gobase/gomini/mini/gometa/goast"
	"github.com/gogf/gf/v2/crypto/gmd5"
	"github.com/gogf/gf/v2/encoding/gbase64"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"sync"
	"testing"
	"time"
)

type TestDiFactoySuite struct {
	suite.Suite
	DiFactroy *difactroy.DiFactroy
	rootdir   string
	*goast.DimetaFactroy
}

func (this *TestDiFactoySuite) SetupTest() {

	ichublog.InitLogrus()
	logrus.Info("DimetaFactroy...", fileutils.FindRootDir())
	this.DiFactroy = difactroy.FindBeanDiFactroy()
	this.DimetaFactroy = goast.FindBeanDimetaFactroy()

}

func TestDiFactoySuites(t *testing.T) {
	suite.Run(t, new(TestDiFactoySuite))
}

// 指定一个文件生成
func (this *TestDiFactoySuite) Test001_MakeDi() {

	//this.DiFactroy.MakeDi("./gometa/dbmenu/metadb/metadata_records.go")

}

// 指定结构体structName生成注册与注入代码
func (this *TestDiFactoySuite) Test002_MakeDiStru() {

	this.DiFactroy.Rootdir = fileutils.FindRootDirGoMod()
	this.DiFactroy.MakeDiStru("EventFrame")

}

// 生成所有, 继承baseentity的结果: 已经有不再生成！
func (this *TestDiFactoySuite) Test003_MakeDiAll() {

	this.DiFactroy.MakeDiAll()

}

// 生成所有 继承baseentity的结果，强制执行
func (this *TestDiFactoySuite) Test004_MakeDiForce() {

	this.DiFactroy.MakeDiAllForce(true)

}

// 查找所有GO文件
func (this *TestDiFactoySuite) Test007_FindAlls() {
	//this.DiFactroy.FindGos()

	logrus.Info("basedi=", jsonutils.ToJsonPretty(this.DiFactroy))
}

// AST分析所有文件
func (this *TestDiFactoySuite) Test008_ParseAll() {

	this.DiFactroy.ParseAll()
	var cf, found = this.DiFactroy.FindSome("SingleEntity")
	if found {
		logrus.Info(cf.ToString())
	} else {
		logrus.Error("\r\n!found ", found)
	}
}
func worker(id int, wg *sync.WaitGroup) {
	defer wg.Done() // 通知主goroutine这个协程已经完成

	fmt.Printf("Worker %d starting\n", id)
	time.Sleep(time.Second) // 模拟工作
	fmt.Printf("Worker %d done\n", id)
}

func (this *TestDiFactoySuite) Test009_mul() {

	var wg sync.WaitGroup
	for i := 1; i <= 5; i++ {
		wg.Add(1) // 增加WaitGroup的计数
		go worker(i, &wg)
	}

	wg.Wait() // 等待所有协程完成
	fmt.Println("All workers completed")
}
func (this *TestDiFactoySuite) Test010_GoastOneStru() {

	var fi, found = goast.FindBeanDimetaFactroy().GoastOne("MetaFactroy")
	logrus.Info("fi=", fi, found)
	var r, _ = gmd5.Encrypt("123")
	logrus.Info("md5=", r)

}
func (this *TestDiFactoySuite) Test011_Gbase64() {

	var r, _ = gmd5.Encrypt("123")
	logrus.Info("md5=", r)

	logrus.Info("base64 =", gbase64.Encode([]byte("22")))

	pkg, err := build.Default.Import("gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils", "", build.FindOnly)
	logrus.Info(pkg.Dir, err)

}
func (this *TestDiFactoySuite) Test012_Gbase64() {

	minilog.Info("base64 =", gbase64.Encode([]byte("22")))

	targetPackage := "gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	targetPackage = "gitee.com/leijmdas/gobase/gomini/mini/file/minilog"
	// 导入包并检查错误
	importedPackage, err := importer.Default().Import(targetPackage)
	if err != nil {
		logrus.Errorf("Failed to import package %s: %v", targetPackage, err)
		return // 早期返回以避免进一步处理错误情况
	}

	// 使用合适的日志级别
	logrus.Infof("Successfully imported package: %v", importedPackage)
}

//RotateBackupLimit:     2
//RotateBackupExpire:    "7d"
//RotateBackupCompress:  9
