package dto

import "github.com/gqlengine/gqlengine"

type GqlRequest struct {
	gqlengine.IsGraphQLArguments
	Name string `json:"name" gqlDesc:"human name" gqlRequired:"true"`
	Unit int    `json:"unit" gqlDesc:"height unit" gqlDefault:"1" gqlRequired:"true"`
}
