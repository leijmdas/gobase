package dto

import "github.com/gqlengine/gqlengine"

// GqlResult 定义了业务的数据结构
type GqlResult struct {
	gqlengine.IsGraphQLObject `gqlDesc:"gqlresult"` // gqlDesc用于生成描述信息
	Code                      int
	Msg                       string
	Total                     int

	Unit          int
	Name          string
	MyStringField string // 定义一个字段，gqlengine会根据golang的基本类型自动匹配到graphql类型
	MyIntField    int    `gqlRequired:"true"` // gqlRequired用于标记该字段是必备非空字段

	Records string
}

func NewMyGqlResult() *GqlResult {
	return &GqlResult{
		Unit: 1,

		Name: "test",
	}
}
