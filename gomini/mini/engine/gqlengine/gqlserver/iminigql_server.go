package gqlserver

type IMinigqlServer interface {
	Register(resolve ...interface{})
	Init() error
	Start()
	StartServer()
}
