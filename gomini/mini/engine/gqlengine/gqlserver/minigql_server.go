package gqlserver

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/gorilla/mux"
	"github.com/gqlengine/gqlengine"
	"github.com/gqlengine/playground"

	"net/http"
)

type MinigqlServer struct {
	basedto.BaseEntitySingle
	Port int
	Url  string

	*gqlengine.Engine
}

func NewMinigqlServer() *MinigqlServer {
	return &MinigqlServer{
		Port: 9696,
		Url:  "/api/graphql",
		Engine: gqlengine.NewEngine(gqlengine.Options{
			Tracing: true, // 使能GraphQL调用链路分析功能
		}),
	}

}

// register
func (m *MinigqlServer) Register(resolve ...interface{}) {
	//register
	for _, v := range resolve {
		m.Engine.NewQuery(v) // 务必注册你的接口！！！
	}
}
func (m *MinigqlServer) Init() error {
	if err := m.Engine.Init(); err != nil {
		panic(err)
	}
	return nil
}
func (m *MinigqlServer) StartServer() {
	m.Init()
	m.Start()
}

func (m *MinigqlServer) Start() {

	// 初始化engine

	// init your gql gqlengine
	//http.HandleFunc(m.Url, m.Engine.ServeHTTP)

	// 为playground配置GraphQL端点（起码让playground知道该去哪里获得graphql数据吧;)）
	playground.SetEndpoints("/api/graphql", "/api/graphql/subscriptions")

	// recommends to use 'gorilla/mux' to serve the playground web assets
	r := mux.NewRouter()
	r.HandleFunc("/api/graphql", m.Engine.ServeHTTP)
	r.HandleFunc("/api/graphql/subscriptions", m.Engine.ServeWebsocket)
	r.PathPrefix("/api/graphql/playground").
		Handler(http.StripPrefix("/api/graphql/playground",
			http.FileServer(playground.WebBundle)))

	println("open playground http://localhost:" + gconv.String(m.Port) + "/api/graphql/playground/")
	if err := http.ListenAndServe(":"+gconv.String(m.Port), r); err != nil {
		panic(err)
	}

}

func StartGql(resolve ...interface{}) {
	engine := gqlengine.NewEngine(gqlengine.Options{
		Tracing: true, // 使能GraphQL调用链路分析功能
	})

	for _, v := range resolve {
		engine.NewQuery(v) // 务必注册你的接口！！！
	}

	// 初始化engine
	if err := engine.Init(); err != nil {
		panic(err)
	}
	// init your gql gqlengine
	http.HandleFunc("/api/graphql", engine.ServeHTTP)

	// 为playground配置GraphQL端点（起码让playground知道该去哪里获得graphql数据吧;)）
	playground.SetEndpoints("/api/graphql", "/api/graphql/subscriptions")

	// recommends to use 'gorilla/mux' to serve the playground web assets
	r := mux.NewRouter()
	r.HandleFunc("/api/graphql", engine.ServeHTTP)
	r.HandleFunc("/api/graphql/subscriptions", engine.ServeWebsocket)
	r.PathPrefix("/api/graphql/playground").
		Handler(http.StripPrefix("/api/graphql/playground",
			http.FileServer(playground.WebBundle)))

	println("open playground http://localhost:9996/api/graphql/playground/")
	if err := http.ListenAndServe(":9996", r); err != nil {
		panic(err)
	}

}
