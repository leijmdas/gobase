package gqlserver

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: minigql_server_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-19 09:27:39)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-19 09:27:39)

* *********************************************************/

const singleNameMinigqlServer = "gqlserver.MinigqlServer"

// init register load
func init() {
	registerBeanMinigqlServer()
}

// register MinigqlServer
func registerBeanMinigqlServer() {
	basedi.RegisterLoadBean(singleNameMinigqlServer, LoadMinigqlServer)
}

func FindBeanMinigqlServer() *MinigqlServer {
	bean, ok := basedi.FindBean(singleNameMinigqlServer).(*MinigqlServer)
	if !ok {
		logrus.Errorf("FindBeanMinigqlServer: failed to cast bean to *MinigqlServer")
		return nil
	}
	return bean

}

func LoadMinigqlServer() baseiface.ISingleton {
	var s = NewMinigqlServer()
	InjectMinigqlServer(s)
	return s

}

func InjectMinigqlServer(s *MinigqlServer) {

	// logrus.Debug("inject")
}
