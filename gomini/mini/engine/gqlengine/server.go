package main

import (
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/gqlserver"
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/service"
)

func main() {
	gqlserver.FindBeanMinigqlServer().Register(service.MyQuery, service.MySimpleQuery, service.GqlQuery)

	gqlserver.FindBeanMinigqlServer().StartServer()

}
