package service

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/dto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/pagedb"
	"github.com/jinzhu/copier"
)

//type GqlResult struct {
//	//*page.PageResult
//	gqlengine.IsGraphQLObject
//	Code int    `json:"code"`
//	Msg  string `json:"msg"`
//
//	PageSize    int `json:"page_size,omitempty"`
//	PageCurrent int `json:"current,omitempty"`
//
//	Total int `json:"total,omitempty"`
//	//Data  interface{} `json:"data"`
//	Data string
//}

func GqlQuery() *dto.GqlResult {
	var dbRequest = pagedb.NewPagedbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "employee"
	dbRequest.TimeToInt = true
	dbRequest.Ge("department_id", 1)

	var result = dbRequest.GeneralQuery()
	goutils.Info(result)
	var gr = dto.NewMyGqlResult()
	copier.Copy(gr, result)

	gr.Records = jsonutils.ToJsonPretty(result.Data)
	return gr
}
