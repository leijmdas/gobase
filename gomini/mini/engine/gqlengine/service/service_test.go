package service

//https://gitee.com/gqlengine/gqlengine

import (
	"gitee.com/leijmdas/gobase/gomini/mini/engine/gqlengine/gqlserver"
	"github.com/gorilla/mux"
	"github.com/gqlengine/playground"
	"net/http"
	"testing"

	"github.com/gqlengine/gqlengine"
)

func Test001_GqlEngine(t *testing.T) {
	engine := gqlengine.NewEngine(gqlengine.Options{
		Tracing: true, // 使能GraphQL调用链路分析功能
	})

	engine.NewQuery(MySimpleQuery) // 务必注册你的接口！！！

	// 初始化engine
	if err := engine.Init(); err != nil {
		panic(err)
	}

	println("open playground http://localhost:8000/api/graphql/")
	// serve for HTTP
	http.HandleFunc("/api/graphql", engine.ServeHTTP)
	if err := http.ListenAndServe(":8000", nil); err != nil {
		panic(err)
	}
}

func Test002_Gql(t *testing.T) {
	engine := gqlengine.NewEngine(gqlengine.Options{
		Tracing: true, // 使能GraphQL调用链路分析功能
	})

	engine.NewQuery(MySimpleQuery) // 务必注册你的接口！！！
	engine.NewQuery(MyQuery)       // 务必注册你的接口！！！
	engine.NewQuery(GqlQuery)

	// 初始化engine
	if err := engine.Init(); err != nil {
		panic(err)
	}
	// init your gql gqlengine
	http.HandleFunc("/api/graphql", engine.ServeHTTP)

	// 为playground配置GraphQL端点（起码让playground知道该去哪里获得graphql数据吧;)）
	playground.SetEndpoints("/api/graphql", "/api/graphql/subscriptions")

	// recommends to use 'gorilla/mux' to serve the playground web assets
	r := mux.NewRouter()
	r.HandleFunc("/api/graphql", engine.ServeHTTP)
	r.HandleFunc("/api/graphql/subscriptions", engine.ServeWebsocket)
	r.PathPrefix("/api/graphql/playground").
		Handler(http.StripPrefix("/api/graphql/playground",
			http.Server(playground.WebBundle)))

	println("open playground http://localhost:9996/api/graphql/playground/")
	if err := http.ListenAndServe(":9996", r); err != nil {
		panic(err)
	}
}
func Test003_Gql(t *testing.T) {

	gqlserver.StartGql(GqlQuery, MyQuery, MySimpleQuery)

}
func Test004_Gql(t *testing.T) {
	var gql = gqlserver.FindBeanMinigqlServer()
	gql.Register(GqlQuery, MyQuery, MySimpleQuery)
	gql.Start()

}
