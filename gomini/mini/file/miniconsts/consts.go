package miniconsts

const (
	MiniPath_cmd         = "cmd"
	MiniPath_bat         = "cmd/bat"
	MiniPath_config      = "config"
	MiniPath_data        = "data"
	MiniPath_data_output = "data/output"
	MiniPath_data_input  = "data/input"
	MiniPath_websample   = "websample"
	MiniPath_test        = "test"
)
