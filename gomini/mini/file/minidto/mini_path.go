package minidto

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type MiniPath struct {
	basedto.BaseEntity

	RootDir      string `json:"rootDir"`
	GomodRootDir string `json:"gomodRootDir"`
	PkgRootDir   string `json:"pkgRootDir"`
	PkgName      string `json:"pkgName"`
}

func NewMiniPath() *MiniPath {
	var mp = &MiniPath{}
	mp.InitProxy(mp)
	return mp
}
