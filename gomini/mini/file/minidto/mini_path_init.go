package minidto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: mini_path_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-07-04 17:54:21)
	@Update 作者: leijmdas@163.com 时间(2024-07-04 17:54:21)

* *********************************************************/

const singleNameMiniPath = "minidto.MiniPath"

// init register load
func init() {
	registerBeanMiniPath()
}

// register MiniPath
func registerBeanMiniPath() {
	basedi.RegisterLoadBean(singleNameMiniPath, LoadMiniPath)
}

func FindBeanMiniPath() *MiniPath {
	bean, ok := basedi.FindBean(singleNameMiniPath).(*MiniPath)
	if !ok {
		logrus.Errorf("FindBeanMiniPath: failed to cast bean to *MiniPath")
		return nil
	}
	return bean

}

func LoadMiniPath() baseiface.ISingleton {
	var s = NewMiniPath()
	InjectMiniPath(s)
	return s

}

func InjectMiniPath(s *MiniPath) {

	// logrus.Debug("inject")
}
