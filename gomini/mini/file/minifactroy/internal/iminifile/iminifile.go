package mini

type Mini interface {
	FindRootDir() string
	CheckExist(name string) bool
}
