package mini

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/gomini/mini/metafile"
	"github.com/duke-git/lancet/system"
	"github.com/sirupsen/logrus"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

const cfg_ = "/config/app.yml"
const cfg_env = "/config/app-env.yml"

type Mini struct {
	basedto.BaseEntitySingle `json:"-"`
	metafile.Meta

	RootDir    string `json:"rootDir,omitempty"`
	configPath string `json:"configPath,omitempty"`
	config     string `json:"config,omitempty"`
	cfgEnv     string `json:"cfgEnv,omitempty"`
}

func (this *Mini) ConfigPath() string {
	return this.configPath
}

func (this *Mini) SetConfigPath(configPath string) {
	this.configPath = configPath
}

func (this *Mini) Config() string {
	return this.config
}

func (this *Mini) SetConfig(config string) {
	this.config = config
}

func (this *Mini) CfgEnv() string {
	return this.cfgEnv
}

func (this *Mini) SetCfgEnv(cfgEnv string) {
	this.cfgEnv = cfgEnv
}

func NewMini() *Mini {
	var mf = &Mini{
		config: cfg_,
		cfgEnv: cfg_env,
	}
	mf.configPath = mf.GetCurPath() + "/" + baseconsts.DEFINE_ENV_PATHFILE
	mf.cfgEnv = mf.GetCurPath() + "/" + baseconsts.ConfigfileAppEnv
	return mf
}

func (this *Mini) CheckConfigExist(root, f string) bool {
	return this.TryExist(root + f)
}

func (this *Mini) FindRootDir() string {
	return this.FindRoot(this.GetCurPath(), this.cfgEnv)

}

// find project rootpkg
func (this *Mini) FindPkgName() string {
	var pkgname string
	var content, _ = fileutils.ReadFileToString(fileutils.FindRootDirGoMod() + "/go.mod")
	var lines = strings.Split(content, "\n")
	for _, line := range lines {
		if strings.Contains(line, "module") {
			var lineArr = strings.Split(line, "module")
			pkgname = strings.Trim(lineArr[1], " ")
			return strings.Trim(pkgname, "\r")

		}
	}
	return ""
}

// go.mod
func (this *Mini) FindRoot(curpath, name string) string {

	var rootdir = os.Getenv(baseconsts.IchubBasePath)
	if len(rootdir) > 0 {
		return rootdir
	}
	rootdir = this.findWorkRoot(curpath, name)
	os.Setenv(baseconsts.IchubBasePath, rootdir)
	this.RootDir = rootdir
	return rootdir
}

func (this *Mini) findWorkRoot(curPath, f string) string {
	var split = "\\"
	if system.IsLinux() {
		split = "/"
	}
	var dirs = strings.Split(curPath, split)
	var workRoot = strings.Join(dirs, split)
	var i = len(dirs)
	for i > 0 {
		if this.CheckConfigExist(workRoot, f) {
			os.Setenv(baseconsts.IchubBasePath, workRoot)
			return workRoot
		}
		i--
		workRoot = strings.Join(dirs[0:i], split)
	}
	return workRoot
}

func (this *Mini) GetCurPath() string {

	c, _ := os.Getwd()
	return c
}

func (this *Mini) CheckExist(name string) bool {

	if fileutils.CheckFileExist(name) {
		logrus.Debug("find  exist! ", name)
		return true
	}

	logrus.Warnf("find not exists !%s\r\n", name)
	return false

}
func (this *Mini) TryExist(name string) bool {

	return fileutils.CheckFileExist(name)

}

func (this *Mini) Finds(rootdir string, suffix string) ([]string, error) {
	var s []string = make([]string, 0)
	// 指定需要遍历的目录
	// 使用path.Walk遍历目录
	err := filepath.Walk(rootdir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			logrus.Error(err)
			return err
		}
		// 如果是文件，可以进行额外操作，比如读取文件内容
		if !info.IsDir() && strings.HasSuffix(path, suffix) {
			s = append(s, path)

		}
		// 返回nil继续遍历
		return nil
	})

	return s, err
}
