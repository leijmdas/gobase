package minipkg

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	mini "gitee.com/leijmdas/gobase/gomini/mini/file/minifactroy/internal/minifile"
	"go/build"
	"io/fs"
	"os"
	"path/filepath"
)

const pkgname = "gitee.com/leijmdas/gobase/gomini/mini/find"

type Minipkg struct {
	basedto.BaseEntitySingle
	*mini.Mini
	PkgName string
}

func NewMinipkg() *Minipkg {
	return &Minipkg{
		Mini:    mini.NewMini(),
		PkgName: pkgname,
	}
}
func (this *Minipkg) FindRootDir() string {
	var pkg, err = this.GetPkg(this.PkgName)
	if err != nil {
		goutils.Error(err)
		this.RootDir = ""
		return ""
	}

	this.RootDir = pkg.Root
	this.SetPkgEnv(this.RootDir)
	return this.RootDir

}

func (this *Minipkg) GetPkg(pkgPath string) (*build.Package, error) {
	pkg, err := build.Default.Import(pkgPath, "", build.FindOnly)
	return pkg, err
}
func (this *Minipkg) SetPkgEnv(path string) {
	os.Setenv(baseconsts.IchubBasePathPkg, path)
}

func (m *Minipkg) GetPkgPath(pkgPath string) (string, error) {
	pkg, err := m.FindPkg(pkgPath)
	return pkg.Dir, err
}
func (self *Minipkg) FindPkg(pkgPathName string) (*build.Package, error) {
	return build.Default.Import(pkgPathName, "", build.FindOnly)

}

// 获取第三方包下所有文件的相对路径
func (m *Minipkg) GetPkgAllsPath(pkgPath string) ([]string, error) {
	pkg, err := build.Default.Import(pkgPath, "", build.FindOnly)
	goutils.Info(pkg.Dir)

	if err != nil {
		return nil, err
	}

	var paths []string
	err = filepath.Walk(pkg.Dir, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			relPath, err := filepath.Rel(pkg.Dir, path)
			if err != nil {
				return err
			}
			paths = append(paths, relPath)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return paths, nil
}
