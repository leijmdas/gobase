package minifactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: mini_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-07-05 06:45:55)
	@Update 作者: leijmdas@163.com 时间(2024-07-05 06:45:55)

* *********************************************************/

const singleNameMinifactroy = "gominifactroy.MiniFactroy"

// init register load
func init() {
	registerBeanMinifactroy()
}

// register MiniFactroy
func registerBeanMinifactroy() {
	basedi.RegisterLoadBean(singleNameMinifactroy, LoadMinifactroy)
}

func FindBeanMinifactroy() *MiniFactroy {
	bean, ok := basedi.FindBean(singleNameMinifactroy).(*MiniFactroy)
	if !ok {
		logrus.Errorf("FindBeanMinifactroy: failed to cast bean to *MiniFactroy")
		return nil
	}
	return bean

}

func LoadMinifactroy() baseiface.ISingleton {
	var s = NewMiniFactroy()
	InjectMinifactroy(s)
	return s

}

func InjectMinifactroy(s *MiniFactroy) {

	// logrus.Debug("inject")
}
