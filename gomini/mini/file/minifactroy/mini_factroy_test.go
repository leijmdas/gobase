package minifactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/gomini/mini/file/minilog"

	"github.com/sirupsen/logrus"
	"strings"
	"testing"
)

// FindProjectPkgName
func Test001_findPrjPkg(t *testing.T) {

	var root = FindBeanMinifactroy().FindProjectPkgName()
	logrus.Info("\r\nroot =", root)
}

func Test010_findPkgRootDirOf(t *testing.T) {
	var BasePkgName = "gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	var root = FindBeanMinifactroy().FindPkgRootDirOf(BasePkgName)
	logrus.Info("\r\nroot =", root)
}

func Test011_findPkgRootDir(t *testing.T) {

	var root = FindBeanMinifactroy().FindPkgRootDir()
	logrus.Info(root)
}
func Test012_findRootDir(t *testing.T) {

	var root = FindBeanMinifactroy().FindRootDir()
	logrus.Info(root)
}

func Test013_findGomodRootDir(t *testing.T) {

	var root = FindBeanMinifactroy().FindGomodRootDir()
	logrus.Info(root)
}
func Test014_minilogPkg(t *testing.T) {

	minilog.Info("root=", FindBeanMinifactroy().FindPkgRootDir())
}
func Test015_minilogFind(t *testing.T) {

	var fs, err = FindBeanMinifactroy().Finds(FindBeanMinifactroy().FindRootDir(), ".go")
	if err != nil {
		goutils.Error(err)
	}
	goutils.Info(strings.Join(fs, "\r\n"))
}
func Test016_minilogFind(t *testing.T) {

	var dto = FindBeanMinifactroy().FindMiniPath()
	logrus.Info(dto)
}
