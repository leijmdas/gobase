package internal

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type GoLogcli struct {
	basedto.BaseEntitySingle
	*GoMiniLog
}

func NewGoLogcli() *GoLogcli {
	var cli = &GoLogcli{
		GoMiniLog: NewGoLog(),
	}
	cli.SetFile(log_cli)
	return cli

}
