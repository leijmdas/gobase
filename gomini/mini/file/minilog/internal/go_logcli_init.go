package internal

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_logcli_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameGoLogcli = "internal.GoLogcli"

// init register load
func init() {
	registerBeanGoLogcli()
}

// register GoLogcli
func registerBeanGoLogcli() {
	basedi.RegisterLoadBean(singleNameGoLogcli, LoadGoLogcli)
}

func FindBeanGoLogcli() *GoLogcli {
	bean, ok := basedi.FindBean(singleNameGoLogcli).(*GoLogcli)
	if !ok {
		logrus.Errorf("FindBeanGoLogcli: failed to cast bean to *GoLogcli")
		return nil
	}
	return bean

}

func LoadGoLogcli() baseiface.ISingleton {
	var s = NewGoLogcli()
	InjectGoLogcli(s)
	return s

}

func InjectGoLogcli(s *GoLogcli) {

	// logrus.Debug("inject")
}
