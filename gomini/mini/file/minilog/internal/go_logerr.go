package internal

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/v2/os/glog"
)

type GoLogerr struct {
	basedto.BaseEntitySingle
	*GoMiniLog
}

func NewGoLogServerErr() *GoLogerr {
	var g = &GoLogerr{
		GoMiniLog: NewGoLog(),
	}
	g.Logger.SetFile(log_err)
	g.Logger.SetLevel(glog.LEVEL_ERRO)
	return g
}
func Error(v ...interface{}) {
	FindBeanGoLogerr().Error(context.Background(), v...)

}
