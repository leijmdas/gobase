package internal

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_logerr_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameGoLogerr = "internal.GoLogerr"

// init register load
func init() {
	registerBeanGoLogerr()
}

// register GoLogerr
func registerBeanGoLogerr() {
	basedi.RegisterLoadBean(singleNameGoLogerr, LoadGoLogerr)
}

func FindBeanGoLogerr() *GoLogerr {
	bean, ok := basedi.FindBean(singleNameGoLogerr).(*GoLogerr)
	if !ok {
		logrus.Errorf("FindBeanGoLogerr: failed to cast bean to *GoLogerr")
		return nil
	}
	return bean

}

func LoadGoLogerr() baseiface.ISingleton {
	var s = NewGoLogServerErr()
	InjectGoLogerr(s)
	return s

}

func InjectGoLogerr(s *GoLogerr) {

	// logrus.Debug("inject")
}
