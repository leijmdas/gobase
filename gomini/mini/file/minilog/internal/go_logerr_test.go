package internal

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestGoLogerrSuite struct {
	suite.Suite
	inst *GoLogerr

	rootdir string
}

func (this *TestGoLogerrSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanGoLogerr()

	this.rootdir = fileutils.FindRootDir()

}
func (this *TestGoLogerrSuite) TearDownTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanGoLogerr()

	this.rootdir = fileutils.FindRootDir()

}

func TestGoLogerrSuites(t *testing.T) {
	suite.Run(t, new(TestGoLogerrSuite))
}
