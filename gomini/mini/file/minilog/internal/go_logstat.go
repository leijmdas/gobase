package internal

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/os/glog"
)

type GoLogstat struct {
	basedto.BaseEntitySingle
	*GoMiniLog
}

func NewGoLogStat() *GoLogstat {
	var g = &GoLogstat{
		GoMiniLog: NewGoLog(),
	}
	g.Logger.SetFile(log_stat)
	g.Logger.SetStdoutPrint(false)
	g.Logger.SetLevel(glog.LEVEL_INFO)
	return g
}

func Stat(v ...interface{}) {
	FindBeanGoLogstat().Info(context.Background(), v...)

}
