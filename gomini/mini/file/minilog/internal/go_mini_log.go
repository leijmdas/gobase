package internal

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/glog"
)

/*
 * @Description:
 * @PathFile: \goconfig\common\base\baseutils\golog\go_mini_log.go
 */

var logdir = fileutils.FindRootDir() + "/logs"

const (
	RotateSize = "100M" //1024 * 1024 * 20

	log_stat = "mini_stat.log"
	log_cli  = "mini_cli.log"

	log_server = "mini.log"
	log_err    = "mini_err.log"
)

// https://blog.csdn.net/weixin_51261234/article/details/124504638
type GoMiniLog struct {
	basedto.BaseEntitySingle

	Name string
	*glog.Logger
}

func NewGoLog() *GoMiniLog {
	var log = &GoMiniLog{Logger: glog.New()}

	return log.init()
}
func (l *GoMiniLog) init() *GoMiniLog {
	l.Logger.SetConfigWithMap(g.Map{
		"path":                 logdir,
		"level":                "all",
		"":                     log_server,
		"stdout":               true,
		"StStatus":             0,
		"RotateSize":           RotateSize,
		"RotateBackupLimit":    10,
		"RotateBackupExpire":   "7d",
		"RotateBackupCompress": 9,
	})

	l.Logger.SetWriterColorEnable(true)

	return l
}

func Info(v ...interface{}) {
	FindBeanGoMiniLog().Info(context.Background(), v...)

}
func Debug(v ...interface{}) {
	FindBeanGoMiniLog().Debug(context.Background(), v...)

}
