package internal

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_mini_log_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameGoMiniLog = "internal.GoMiniLog"

// init register load
func init() {
	registerBeanGoMiniLog()
}

// register GoMiniLog
func registerBeanGoMiniLog() {
	basedi.RegisterLoadBean(singleNameGoMiniLog, LoadGoMiniLog)
}

func FindBeanGoMiniLog() *GoMiniLog {
	bean, ok := basedi.FindBean(singleNameGoMiniLog).(*GoMiniLog)
	if !ok {
		logrus.Errorf("FindBeanGoMiniLog: failed to cast bean to *GoMiniLog")
		return nil
	}
	return bean

}

func LoadGoMiniLog() baseiface.ISingleton {
	var s = NewGoLog()
	InjectGoMiniLog(s)
	return s

}

func InjectGoMiniLog(s *GoMiniLog) {

	// logrus.Debug("inject")
}
