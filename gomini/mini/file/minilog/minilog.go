package minilog

import (
	"context"
	"gitee.com/leijmdas/gobase/gomini/mini/file/minilog/internal"
)

func Info(v ...interface{}) {
	internal.FindBeanGoMiniLog().Info(context.Background(), v...)

}
func Debug(v ...interface{}) {
	internal.FindBeanGoMiniLog().Debug(context.Background(), v...)

}

func Error(v ...interface{}) {
	internal.FindBeanGoLogerr().Error(context.Background(), v...)

}
func Stat(v ...interface{}) {
	internal.FindBeanGoLogstat().Info(context.Background(), v...)

}
