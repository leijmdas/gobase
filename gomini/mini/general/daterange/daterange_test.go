package report_test

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/gomini/mini/general/daterange/report_base/daterange"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"

	"testing"
	"time"
)

type TestGeneralDrSuite struct {
	suite.Suite

	rootdir string
	dbtype  string
	cfg     *ichubconfig.IchubConfig
}

func TestGeneralDrSuites(t *testing.T) {

	suite.Run(t, new(TestGeneralDrSuite))
}

func (self *TestGeneralDrSuite) SetupSuite() {

	self.rootdir = fileutils.FindRootDir()
	self.cfg = ichubconfig.FindBeanIchubConfig()

}

func (self *TestGeneralDrSuite) TearDownSuite() {
	logrus.Info(" teardown self")

}
func (self *TestGeneralDrSuite) SetupTest() {
	logrus.Info(" setup test")
}

func (self *TestGeneralDrSuite) TearDownTest() {
	logrus.Info(" teardown test")
}

func (self *TestGeneralDrSuite) Test005_FindAll() {
	var rangeAll = daterange.FindBeanDateUtcRange()

	golog.Info(rangeAll)
}
func (self *TestGeneralDrSuite) Test005_FindAll2Int() {

	var rangeAll = daterange.FindBeanDateUtcRange()
	golog.Info(rangeAll.ToDateRangeInt())
	var dr = daterange.FindBeanDateRange()
	dr.UseZoneShanghai().FromOfInt(1731576239)
	dr.UseZoneShanghai().FromOfInt(1730802563)
	dr.UseZoneShanghai().FromOfInt(1731576239)
	golog.Info(dr)
	dr.UseZoneUtc().FromOfInt(1731576239)
	golog.Info(dr)
}
func (self *TestGeneralDrSuite) Test006_FindDaterangeOFAll() {

	var rangeAll = daterange.FindBeanDateUtcRange()
	golog.Info(rangeAll.DateRange) // rangeAll.FromOf("2024-12-08")
	//rangeAll.UseZoneUtc().FromOfInt(1731576239)
	rangeAll.UseZoneShanghai().FromOfInt(1731576239)
	golog.Info(rangeAll.DateRange)
	n := time.Now().In(daterange.FindBeanDateRange().ZoneShanghai)
	golog.Info(n)
}
func (self *TestGeneralDrSuite) Test007_FindDaterangeOFAll() {

	var rangeAll = daterange.FindBeanDateUtcRange()
	var a = rangeAll.FromOf("2024-12-11")
	golog.Info(a.ToDateRangeInt())
	var b = rangeAll.FromOf("2024-12-08")
	golog.Info(b.ToDateRangeInt()) //1733529600000 1733788800000
}

// 2024-12-09 14:08:22.502883+00
func (self *TestGeneralDrSuite) Test008_FindDaterangeOFAll() {

	var rangeAll = daterange.FindBeanDateUtcRange()

	var dr = rangeAll.UseZoneShanghai().FromOfInt(1733702400)
	golog.Info(dr)
}
