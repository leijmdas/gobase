package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/daterange/report_base/reportkey"
	"github.com/gogf/gf/v2/os/gtime"
)

type DateGtime struct {
	basedto.BaseEntity
	Start *gtime.Time
	End   *gtime.Time
	End2  *gtime.Time
	*reportkey.ReportKey
}

func NewDateGtime() *DateGtime {
	return &DateGtime{
		ReportKey: reportkey.FindBeanReportKey(),
	}
}

func (self DateGtime) ToEsDateRange() *DateStr {
	var startTime = self.Start.Layout("2006-01-02") + "T00:00:00+08:00"
	var endTime = self.End.Layout("2006-01-02") + "T23:59:59+08:00"
	var str = DefaultStrOf(startTime, endTime)
	str.ZoneType = self.ZoneType
	return str
}
