package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: date_range_gtime_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-11-30 22:01:24
	@Update  作者:    leijmdas@163.com  时间: 2024-11-30 22:01:24
*/

var singleNameDateRangeGtime = "*report_dto.DateGtime-da1efb38-6d69-4ff4-b2e3-77a236e2fa1b"

// init register load
func init() {
	registerBeanDateRangeGtime()
}

// register DateGtime
func registerBeanDateRangeGtime() {
	err := basedi.RegisterLoadBean(singleNameDateRangeGtime, LoadDateRangeGtime)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanDateRangeGtime
func FindBeanDateRangeGtime() *DateGtime {

	if bean, ok := basedi.FindBeanOk(singleNameDateRangeGtime); ok {
		return bean.(*DateGtime)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadDateRangeGtime() baseiface.ISingleton {
	var inst = NewDateGtime()
	InjectDateRangeGtime(inst)
	return inst

}

func InjectDateRangeGtime(s *DateGtime) {

}
