package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/daterange/report_base/reportkey"
)

type DateInt struct {
	basedto.BaseEntity
	Start int64 `json:"start"`
	End   int64 `json:"end"`
	End2  int64 `json:"end2"`

	*reportkey.ReportKey
}

func NewDateRangeInt() *DateInt {
	return &DateInt{
		ReportKey: reportkey.FindBeanReportKey(),
	}
}
