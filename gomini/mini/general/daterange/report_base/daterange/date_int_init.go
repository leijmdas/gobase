package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"git.ichub.com/general/webcli120/goconfig/ichublog/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: date_range_int_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-12-09 10:02:26
	@Update  作者:    leijmdas@163.com  时间: 2024-12-09 10:02:26
*/

var singleNameDateRangeInt = "*daterange.DateInt-186618ee-8d22-47bb-9668-e848771879e4"

// init register load
func init() {
	registerBeanDateRangeInt()
}

// register DateInt
func registerBeanDateRangeInt() {
	err := basedi.RegisterLoadBean(singleNameDateRangeInt, LoadDateRangeInt)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanDateRangeInt
func FindBeanDateRangeInt() *DateInt {

	if bean, ok := basedi.FindBeanOk(singleNameDateRangeInt); ok {
		return bean.(*DateInt)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadDateRangeInt() baseiface.ISingleton {
	var inst = NewDateRangeInt()
	InjectDateRangeInt(inst)
	return inst

}

func InjectDateRangeInt(s *DateInt) {

}
