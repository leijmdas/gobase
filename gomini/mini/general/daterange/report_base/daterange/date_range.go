package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/daterange/report_base/reportkey"
	"github.com/gogf/gf/v2/os/gtime"

	"time"
)

type DateRange struct {
	basedto.BaseEntity
	*reportkey.ReportKey
	Start time.Time //2021-01-01 00:00:00
	End   time.Time //2021-01-01 23:59:59
	End2  time.Time ///2021-01-02 00:00:00  第二天

}

func NewDateRange() *DateRange {
	var dr = &DateRange{
		ReportKey: reportkey.FindBeanReportKey(),
	}
	dr.InitProxy(dr)
	dr.Init()
	return dr
}
func (self *DateRange) Init() *DateRange {
	self.From(time.Now())
	return self
}
func (self *DateRange) From(d time.Time) *DateRange {
	d = d.In(self.ZoneType)
	self.Start = d
	self.End = d
	self.End2 = d.Add(24 * time.Hour)
	self.DateRangeOnly()
	self.End = self.End2.Add(-1 * time.Nanosecond)

	return self
}
func (self *DateRange) Froms(s, e time.Time) *DateRange {
	self.From(s)
	self.End = e.In(self.ZoneType)
	return self
}
func (self *DateRange) FromOf(d string) *DateRange {

	var start, _ = gtime.StrToTimeLayout(d, "2006-01-02")
	self.From(start.Time.In(self.ZoneType))
	return self
}
func (self *DateRange) FromsOf(s, e string) *DateRange {

	var start, _ = gtime.StrToTimeLayout(s, "2006-01-02")
	var end, _ = gtime.StrToTimeLayout(e, "2006-01-02")
	self.From(start.Time.In(self.ZoneType))
	self.End = end.Time.In(self.ZoneType)
	self.DateRangeOnly()
	return self
}
func (self *DateRange) FromsOfInt(s, e int64) *DateRange {
	var d = time.Unix(s, 0).In(self.ZoneType)
	self.From(d)
	self.End = time.Unix(e, 0).In(self.ZoneType)
	self.DateRangeOnly()
	return self
}
func (self *DateRange) FromOfInt(dd int64) *DateRange {
	var d = time.Unix(dd, 0).In(self.ZoneType)
	return self.From(d)
}
func (self *DateRange) Yesterday() DateRange {
	return DateRange{}
}
func (self *DateRange) DayBefore() DateRange {
	return DateRange{}
}

func (self *DateRange) ToDateRangeStr() *DateStr {
	var strday = FindBeanDateRangeStr()
	strday.Start = gtime.NewFromTime(self.Start).Layout("2006-01-02")
	strday.End = gtime.NewFromTime(self.End).Layout("2006-01-02")
	strday.End2 = gtime.NewFromTime(self.End2).Layout("2006-01-02")

	return strday
}
func (self *DateRange) ToId(memberId int64) string {
	return self.Start.Format("2006-01-02") + "|" + utils.FormatInt(memberId) + "|0|0"

}
func (self *DateRange) Clone() *DateRange {
	var c = *self
	return &c
}
func (self DateRange) BeforeDay() *DateRange {
	var c = self.Clone()
	c.Start = c.Start.Add(-48 * time.Hour)
	c.End = c.End.Add(-48 * time.Hour)
	c.End2 = c.End2.Add(-48 * time.Hour)
	return c
}
func (self DateRange) YesterDay() *DateRange {
	var c = self.Clone()
	c.Start = c.Start.Add(-24 * time.Hour)
	c.End = c.End.Add(-24 * time.Hour)
	c.End2 = c.End2.Add(-24 * time.Hour)
	return c
}
func (self DateRange) Tommorow() *DateRange {
	var c = self.Clone()
	c.Start = c.Start.Add(24 * time.Hour)
	c.End = c.End.Add(24 * time.Hour)
	c.End2 = c.End2.Add(24 * time.Hour)
	return c
}

func (self DateRange) ToUtcTime() *DateRange {
	var c = self.Clone()
	c.Start = c.Start.In(self.ZoneUtc)
	c.End = c.End.In(self.ZoneUtc)
	c.End2 = c.End2.In(self.ZoneUtc)

	c.ZoneType = self.ZoneUtc
	return c
}
func (self DateRange) ToShanghaiTime() *DateRange {
	var c = self.Clone()
	c.Start = c.Start.In(self.ZoneShanghai)
	c.End = c.End.In(self.ZoneShanghai)
	c.End2 = c.End2.In(self.ZoneShanghai)
	c.ZoneType = self.ZoneShanghai
	return c
}

func (self DateRange) ToDateOnly() *DateRange {
	var c = self.Clone()
	c.Start = time.Date(c.Start.Year(), c.Start.Month(), c.Start.Day(), 0, 0, 0, 0, self.ZoneType)
	c.End = time.Date(c.End.Year(), c.End.Month(), c.End.Day(), 23, 59, 59, 0, self.ZoneType)
	c.End2 = c.Start.Add(time.Hour * 24)
	c.End = c.End2.Add(-1 * time.Nanosecond)
	return c
}

// shanghai datestr used by es
func (self DateRange) ToEsDateStr() *DateStr {

	var dayStr = NewDateStr()
	dayStr.Start = gtime.NewFromTime(self.Start).Layout("2006-01-02") + "T00:00:00+08:00"
	dayStr.End = gtime.NewFromTime(self.End).Layout("2006-01-02") + "T23:59:59+08:00"
	dayStr.End2 = gtime.NewFromTime(self.End2).Layout("2006-01-02") + "T00:00:00+08:00"
	return dayStr
}

func (self DateRange) ToDateRangeInt() *DateInt {
	var dr = FindBeanDateRangeInt()
	dr.Start = self.Time2Int(self.Start)
	dr.End = self.Time2Int(self.End)
	dr.End2 = self.Time2Int(self.End2)
	return dr
}
func (self *DateRange) DateRangeOnly(atends ...bool) *DateRange {
	self.Start = self.TimeFrom(self.Start)
	if len(atends) > 0 && atends[0] {
		self.End = self.TimeFrom(self.End)
	}
	self.End2 = self.TimeFrom(self.End2)

	return self
}

func (self *DateRange) UseZoneUtc() *DateRange {
	self.ZoneType = self.ZoneUtc
	return self
}
func (self *DateRange) UseZoneShanghai() *DateRange {
	self.ZoneType = self.ZoneShanghai
	return self
}
