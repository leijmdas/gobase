package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"

	//	"git.ichub.com/general/webcli120/goconfig/base/baseiface"
	//	"git.ichub.com/general/webcli120/goconfig/basedi"
	//"git.ichub.com/general/webcli120/goconfig/ichublog/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: date_range_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-11-30 22:01:24
	@Update  作者:    leijmdas@163.com  时间: 2024-11-30 22:01:24
*/

var singleNameDateRange = "*report_dto.DateRange-84b844d8-d710-448f-8ba3-9f4940356690"

// init register load
func init() {
	registerBeanDateRange()
}

// register DateRange
func registerBeanDateRange() {
	err := basedi.RegisterLoadBean(singleNameDateRange, LoadDateRange)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanDateRange
func FindBeanDateRange() *DateRange {

	if bean, ok := basedi.FindBeanOk(singleNameDateRange); ok {
		return bean.(*DateRange)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadDateRange() baseiface.ISingleton {
	var inst = NewDateRange()
	InjectDateRange(inst)
	return inst

}

func InjectDateRange(s *DateRange) {

}
