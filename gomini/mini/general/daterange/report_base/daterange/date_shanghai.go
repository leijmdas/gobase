package daterange

import (
	//"git.ichub.com/general/webcli120/goconfig/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"time"
)

type DateShanghai struct {
	basedto.BaseEntitySingle
	*DateUtcRange
}

func NewDateShanghai() *DateShanghai {
	var r = &DateShanghai{
		DateUtcRange: FindBeanDateUtcRange(),
	}
	r.InitProxy(r)
	r.UseZoneShanghai()
	r.Init(time.Now())
	return r
}
