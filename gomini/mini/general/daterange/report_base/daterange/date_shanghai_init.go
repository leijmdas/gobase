package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: date_range_shanghai_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-12-10 09:46:41
	@Update  作者:    leijmdas@163.com  时间: 2024-12-10 09:46:41
*/

var singleNameDateRangeShanghai = "*daterange.DateShanghai-fa723877-816a-49e4-84b6-1609a8102e88"

// init register load
func init() {
	registerBeanDateRangeShanghai()
}

// register DateShanghai
func registerBeanDateRangeShanghai() {
	err := basedi.RegisterLoadBean(singleNameDateRangeShanghai, LoadDateRangeShanghai)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanDateShanghai
func FindBeanDateShanghai() *DateShanghai {

	if bean, ok := basedi.FindBeanOk(singleNameDateRangeShanghai); ok {
		return bean.(*DateShanghai)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadDateRangeShanghai() baseiface.ISingleton {
	var inst = NewDateShanghai()
	InjectDateRangeShanghai(inst)
	return inst

}

func InjectDateRangeShanghai(s *DateShanghai) {

}
