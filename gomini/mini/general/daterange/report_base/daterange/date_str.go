package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/daterange/report_base/reportkey"
	"github.com/gogf/gf/v2/os/gtime"
	"time"
)

type DateStr struct {
	basedto.BaseEntity
	Start string `json:"start"`
	End   string `json:"end"`
	End2  string `json:"end2"`
	*reportkey.ReportKey
}

func NewDateStr() *DateStr {
	return &DateStr{
		ReportKey: reportkey.FindBeanReportKey(),
	}
}

func DefaultStrOf(start, end string) *DateStr {
	var dr = NewDateStr()
	dr.Start = start
	dr.End = end
	return dr
}

// time.ParseInLocation("2006-01-02", "2024-12-03", time.Local), time.ParseInLocation("2006-01-02", "2024-12-03", time.Local)
func (self *DateStr) ToGtime() (*DateGtime, error) {
	var gt = FindBeanDateRangeGtime()
	var start, err = gtime.StrToTimeLayout(self.Start, "2006-01-02")
	if err != nil {
		return gt, err
	}
	gt.Start = start
	gt.End, err = gtime.StrToTimeLayout(self.End, "2006-01-02")
	gt.End2 = gt.Start.Add(24 * time.Hour)
	return gt, err
}
func (self *DateStr) ToDateRange() (*DateRange, error) {
	var dr = FindBeanDateRange()

	var gdr, err = self.ToGtime()
	if err != nil {
		return dr, err
	}
	dr.Start = gdr.Start.Time
	dr.End = gdr.End.Time
	dr.End2 = dr.Start.Add(24 * time.Hour)
	dr.DateRangeOnly()
	return dr, nil
}
