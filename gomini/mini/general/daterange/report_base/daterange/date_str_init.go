package daterange

import (
	//	"git.ichub.com/general/webcli120/goconfig/base/baseiface"

	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: date_range_str_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-11-30 21:59:26
	@Update  作者:    leijmdas@163.com  时间: 2024-11-30 21:59:26
*/

var singleNameDateRangeStr = "*report_dto.DateStr-53f2c4c7-a1ce-44cd-a170-274368cd73b2"

// init register load
func init() {
	registerBeanDateRangeStr()
}

// register DateStr
func registerBeanDateRangeStr() {
	err := basedi.RegisterLoadBean(singleNameDateRangeStr, LoadDateRangeStr)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanDateRangeStr
func FindBeanDateRangeStr() *DateStr {

	if bean, ok := basedi.FindBeanOk(singleNameDateRangeStr); ok {
		return bean.(*DateStr)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadDateRangeStr() baseiface.ISingleton {
	var inst = NewDateStr()
	InjectDateRangeStr(inst)
	return inst

}

func InjectDateRangeStr(s *DateStr) {

}
