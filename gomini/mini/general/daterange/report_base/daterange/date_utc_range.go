package daterange

import (
	//	"git.ichub.com/general/webcli120/goconfig/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"time"
)

// cron exe hour
type DateUtcRange struct {
	basedto.BaseEntity

	DateRangeInt   *DateInt   `json:"date_range_int,omitempty"`
	DateRangeStr   *DateStr   `json:"date_range_str,omitempty"`
	dateRangeGtime *DateGtime `json:"date_range_gtime,omitempty"`
	*DateRange     `json:"date_range,omitempty"`
	Utc            *DateRange `json:"utc"`
	Local          *DateRange `json:"local"`
	Shanghai       *DateRange `json:"shanghai"`
}

func NewDateUtcRange() *DateUtcRange {
	var dr = &DateUtcRange{
		DateRange:      FindBeanDateRange(),
		DateRangeStr:   FindBeanDateRangeStr(),
		dateRangeGtime: FindBeanDateRangeGtime(),
		DateRangeInt:   FindBeanDateRangeInt(),

		Utc:      FindBeanDateRange(),
		Local:    FindBeanDateRange(),
		Shanghai: FindBeanDateRange(),
	}

	dr.InitProxy(dr)
	dr.UseZoneUtc()
	dr.Init(time.Now())
	return dr
}
func (dr *DateUtcRange) Init(now time.Time) {

	dr.Utc.From(now)
	dr.Local.From(now)
	dr.Shanghai.From(now)

}

func DefaultAll(start, end string) *DateUtcRange {
	var dr = NewDateUtcRange()
	dr.DateRangeStr = DefaultStrOf(start, end)
	dr.DateRange, _ = dr.DateRangeStr.ToDateRange()
	dr.dateRangeGtime, _ = dr.DateRangeStr.ToGtime()
	return dr
}
func Default(start, end time.Time) *DateUtcRange {
	var dr = NewDateUtcRange()
	dr.DateRange = NewDateRange()
	dr.DateRange.Start = start
	dr.DateRange.End = end
	dr.DateRangeStr = dr.DateRange.ToDateRangeStr()
	return dr
}

func (self DateUtcRange) ToEsDateRange() *DateStr {
	return self.dateRangeGtime.ToEsDateRange()
}
