package daterange

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: date_range_all_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-12-02 23:05:30
	@Update  作者:    leijmdas@163.com  时间: 2024-12-02 23:05:30
*/

var singleNameDateRangeAll = "*daterange.DateUtcRange-47830b68-bc6b-475c-bb6f-2af911311c10"

// init register load
func init() {
	registerBeanDateRangeAll()
}

// register DateUtcRange
func registerBeanDateRangeAll() {
	err := basedi.RegisterLoadBean(singleNameDateRangeAll, LoadDateRangeAll)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanDateUtcRange
func FindBeanDateUtcRange() *DateUtcRange {

	if bean, ok := basedi.FindBeanOk(singleNameDateRangeAll); ok {
		return bean.(*DateUtcRange)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadDateRangeAll() baseiface.ISingleton {
	var inst = NewDateUtcRange()
	InjectDateRangeAll(inst)
	return inst

}

func InjectDateRangeAll(s *DateUtcRange) {

}
