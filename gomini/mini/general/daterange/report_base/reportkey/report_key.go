package reportkey

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/general/daterange/report_const"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/util/gconv"
	"time"
	_ "time/tzdata"
)

// var ZoneShanghai = NewReportKey().ZoneShanghai
var ZoneShanghai = time.FixedZone("CST", 8*3600)

// employee buyer department account
type ReportKey struct {
	basedto.BaseEntitySingle
	ZoneLocation `json:"-"`
	EventType    string `json:"event_type"`
	KeyType      string `json:"key_type"` // object_type : 员工， 账户， 部门， 下单人
	ShopMemberId int64  `json:"shop_member_id"`
	//EmployeeId   int64  `json:"employee_id"`
	AccountId int64 `json:"account_id"`
	BuyerId   int64 `json:"buyer_id"`
	DeptId    int64 `json:"dept_id"`

	Id        string    `json:"id"`
	CreatedAt time.Time `json:"created_at"`
}

func NewReportKey() *ReportKey {
	var reportKey = &ReportKey{
		CreatedAt: time.Now(),
		KeyType:   report_const.OBJECT_EMP,
		ZoneLocation: ZoneLocation{
			ZoneType:  time.UTC,
			ZoneLocal: time.Local,
			ZoneUtc:   time.UTC,
		},
	}
	reportKey.InitProxy(reportKey)
	reportKey.initShanghai()
	if reportKey.ZoneShanghai != nil {
		reportKey.CreatedAt = reportKey.CreatedAt.In(reportKey.ZoneShanghai)
	}
	return reportKey
}
func (self *ReportKey) initShanghai() *ReportKey {
	var err error
	self.ZoneShanghai, err = time.LoadLocation(report_const.LOCATION_SHANGHAI)
	if err != nil {
		self.ZoneShanghai = time.FixedZone("CST", 8*3600)
		golog.Error("NewReportKey err=", err, self.ZoneShanghai)
	}
	if self.ZoneShanghai == nil {
		self.ZoneShanghai = time.FixedZone("CST", 8*3600)
	}

	return self
}
func (self *ReportKey) CheckZone() *ReportKey {

	self.CreatedAt = self.CreatedAt.In(self.ZoneShanghai)
	return self
}
func (self *ReportKey) IfEmp() bool {

	return self.KeyType == report_const.OBJECT_EMP
}
func (self *ReportKey) IfDept() bool {

	return self.KeyType == report_const.OBJECT_DEPT
}
func (self *ReportKey) IfAccount() bool {

	return self.KeyType == report_const.OBJECT_AC
}
func (self *ReportKey) IfBuyer() bool {

	return self.KeyType == report_const.OBJECT_BUYER
}
func (self *ReportKey) Pkey() string {
	if self.IfEmp() {
		return self.PkeyEmployee()
	}
	if self.IfDept() {
		return self.PkeyEmployee()
	}
	if self.IfBuyer() {
		return self.PkeyEmployee()
	}
	if self.IfAccount() {
		return self.PkeyEmployee()
	}
	return self.PkeyEmployee()
}
func (self *ReportKey) PkeyEmployee() string {
	self.CheckZone()
	self.Id = self.CreatedAt.Format("2006-01-02") +
		"|" + gconv.String(self.ShopMemberId) + "|0|0"

	return self.Id
}
func (self *ReportKey) DateOnly() *ReportKey {

	self.CreatedAt = self.TimeFrom(self.CreatedAt)
	return self
}
func (self *ReportKey) TimeFrom(d time.Time) time.Time {

	var t = time.Date(d.Year(), d.Month(), d.Day(), 0, 0, 0, 0, d.Location())
	return t.In(self.ZoneType)

}
func (self *ReportKey) TimeFromOf(d string) time.Time {

	var start, _ = gtime.StrToTimeLayout(d, "2006-01-02")
	return self.TimeFrom(start.Time)
}

func (self *ReportKey) TimeFromOfInt(d int64) time.Time {
	return self.TimeFrom(time.Unix(d, 0))

}
func (self *ReportKey) TimeFromOfIntMilli(d int64) time.Time {
	return self.TimeFrom(time.Unix(d/1e3, 0))

}

func (self *ReportKey) Beforeday(time2 time.Time) time.Time {
	return time2.Add(-48 * time.Hour)
}
func (self *ReportKey) Yesterday(time2 time.Time) time.Time {
	return time2.Add(-24 * time.Hour)
}

//today

func (self *ReportKey) UseZoneUtc() *ReportKey {
	self.ZoneType = self.ZoneUtc
	return self
}
func (self *ReportKey) UseZoneShanghai() *ReportKey {
	self.ZoneType = self.ZoneShanghai
	return self
}

func (self *ReportKey) Time2Int(t time.Time) int64 {
	return t.UnixNano() / 1e6
}
