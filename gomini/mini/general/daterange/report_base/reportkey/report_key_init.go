package reportkey

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"git.ichub.com/general/webcli120/goconfig/ichublog/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: report_key_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2024-12-08 15:10:56
	@Update  作者:    leijmdas@163.com  时间: 2024-12-08 15:10:56
*/

var singleNameReportKey = "*reportkey.ReportKey-51240ac27-8bcf-42c9-b3fa-1adec6592e1d"

// init register load
func init() {
	registerBeanReportKey()
}

// register ReportKey
func registerBeanReportKey() {
	err := basedi.RegisterLoadBean(singleNameReportKey, LoadReportKey)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanReportKey
func FindBeanReportKey() *ReportKey {

	if bean, ok := basedi.FindBeanOk(singleNameReportKey); ok {
		return bean.(*ReportKey)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadReportKey() baseiface.ISingleton {
	var inst = NewReportKey()
	InjectReportKey(inst)
	return inst

}

func InjectReportKey(s *ReportKey) {

}
