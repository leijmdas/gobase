package reportkey

import "time"

type ZoneLocation struct {
	ZoneType     *time.Location `json:"zone_type"`
	ZoneUtc      *time.Location `json:"zone_utc"`
	ZoneLocal    *time.Location `json:"zone_local"`
	ZoneShanghai *time.Location `json:"zone_shanghai"`
}
