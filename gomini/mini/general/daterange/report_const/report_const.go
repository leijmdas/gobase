package report_const

const (
	Query_MAX_RECORDS = 10000

	TYPE_DAY_TODAY     = "today"
	TYPE_DAY_YESTERDAY = "yesterday"
	TYPE_DAY_BEFOREDAY = "beforeday"

	LOCATION_SHANGHAI = "Asia/Shanghai"
	LOCATION_UTC      = "UTC"
)
const (
	OBJECT_EMP   = "employee"
	OBJECT_DEPT  = "department"
	OBJECT_AC    = "account"
	OBJECT_BUYER = "buyer"
)

// 业务订单类型
const (
	MatchTypeS = "S" //销售类型
	MatchTypeP = "P" //采购类型

	TypePlatform     = "platform"      //平台
	TypeSelfOperated = "self-operated" //自营/内联
	TypeOffline      = "offline"       //外联
	TypeService      = "service"
)

// 管理指标类型
const (
	//beforeday 聊天事件
	Event_Type_Session = "Event_Type_Session"
	//yesterday 定价
	Event_Type_Pricing = "Event_Type_Pricing"
	//yesterday 议价
	Event_Type_Bargaining = "Event_Type_Bargaining"
)

// 统计步骤
const (
	//0.从盘面取每天汇总询盘数，成员、下单人、库存项维度
	STEP_METRICTS_TAKEOUT = iota + 100
	//1.从盘面取 供盘数，成员维度，每天汇总昨天的数据。昨天新增+更新（激活状态）- 更新（归档）
	STEP_SUPPLY_YESTERDAY

	//3.从财务域取财务指标：成员、账户维度
	STEP_FINANCE_METRICS

	//4.从物流域取物流指标：成员维度
	STEP_LOGISTICS_METRICS

	//5.从订单域取订单指标，成员、下单人、库存项维度
	STEP_ORDER__METRICS

	//6.取财务核销额、核销毛利、费用额，成员，商户维度
	STEP_FINANCE_MONEY
	//最终数据集合
	STEP_MERGE

	//2.从业务问答事件表取问询数、24小时回复数,成员维度。因为统计前天的数据，所有特殊处理，直接更新ES
	STEP_CHAT_COUNT
	//此操作必须放置最后。
)

//日志
