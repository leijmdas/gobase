package coroutines

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/dto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/iface"
	"github.com/emirpasic/gods/lists/arraylist"
	"sync"
	"time"
)

type CoRoutines struct {
	basedto.BaseEntitySingle
}

func NewCoRoutines() *CoRoutines {
	return &CoRoutines{}
}

func (self *CoRoutines) Execute(iface iface.CoRunnerIface) *basedto.IchubResult {
	if iface.GetData() == nil {
		return basedto.ResultFailMsg("Data is nil")
	}
	var wg sync.WaitGroup
	done := make(chan *basedto.IchubResult, iface.GetData().GroupTasks.Size())
	for i := 0; i < iface.GetData().GroupNumber; i++ {

		var task, ok = iface.GetData().GroupTasks.Get(i)
		if !ok {
			continue
		}
		var data = dto.NewCoData()
		data.GroupOrder = i + 1
		data.Tasks = task.(*arraylist.List)
		wg.Add(1)
		go func() {
			defer wg.Done()
			result := iface.Execute(data)
			if result == nil {
				result = basedto.ResultFailMsg("Task execution resulted in nil result")
			}
			done <- result
		}()

	}
	resultList := arraylist.New()
	// 恢复并优化超时机制
	timeout := time.After(iface.GetData().TimeoutSeconds)
	for i := 0; i < iface.GetData().GroupNumber; i++ {
		select {
		case <-timeout:
			fmt.Println("操作超时，退出。")
			wg.Wait()
			close(done)
			return self.Merge(resultList)
		case result := <-done:

			resultList.Add(result)
		}
	}

	wg.Wait()
	close(done)
	return self.Merge(resultList)

}

func (self *CoRoutines) Merge(resultList *arraylist.List) *basedto.IchubResult {

	if resultList.Size() == 0 {
		return basedto.ResultFailMsg("merge resultList is empty!")
	}
	var result = basedto.NewIchubResult()
	result.Data = resultList
	return result
}
