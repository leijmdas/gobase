package coroutines

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: co_routines_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameCoRoutines = "coroutines.CoRoutines"

// init register load
func init() {
	registerBeanCoRoutines()
}

// register CoRoutines
func registerBeanCoRoutines() {
	basedi.RegisterLoadBean(singleNameCoRoutines, LoadCoRoutines)
}

func FindBeanCoRoutines() *CoRoutines {
	bean, ok := basedi.FindBean(singleNameCoRoutines).(*CoRoutines)
	if !ok {
		logrus.Errorf("FindBeanCoRoutines: failed to cast bean to *CoRoutines")
		return nil
	}
	return bean

}

func LoadCoRoutines() baseiface.ISingleton {
	var s = NewCoRoutines()
	InjectCoRoutines(s)
	return s

}

func InjectCoRoutines(s *CoRoutines) {

	// logrus.Debug("inject")
}
