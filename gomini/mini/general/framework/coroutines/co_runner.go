package coroutines

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/dto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/iface"
)

// CoRunner 是一个负责并发执行任务的协程运行器
// 它继承了基于DTO的基础实体和协程运行器接口，
// 用于简化并发任务的管理和执行。
type CoRunner struct {
	basedto.BaseEntity // BaseEntity 提供基础实体属性和方法。
	*dto.CoData        `di:"auto"`
	iface.CoRunnerIface
}

// NewCoRunner 创建并返回一个新的CoRunner实例。
// 这是获取CoRunner实例的推荐方式。
func NewCoRunner() *CoRunner {
	return &CoRunner{
		CoData: dto.NewCoData(),
	}
}

func (g *CoRunner) Execute(data *dto.CoData) *basedto.IchubResult {

	return basedto.NewIchubResult().SuccessData(data.Tasks)
}

func (g *CoRunner) GetData() *dto.CoData {
	g.CoData.DivideGroup()
	return g.CoData
}
