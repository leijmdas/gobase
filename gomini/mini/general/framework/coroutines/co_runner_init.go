package coroutines

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: co_runner_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameCoRunner = "coroutines.CoRunner"

// init register load
func init() {
	registerBeanCoRunner()
}

// register CoRunner
func registerBeanCoRunner() {
	basedi.RegisterLoadBean(singleNameCoRunner, LoadCoRunner)
}

func FindBeanCoRunner() *CoRunner {
	bean, ok := basedi.FindBean(singleNameCoRunner).(*CoRunner)
	if !ok {
		logrus.Errorf("FindBeanCoRunner: failed to cast bean to *CoRunner")
		return nil
	}
	return bean

}

func LoadCoRunner() baseiface.ISingleton {
	var s = NewCoRunner()
	InjectCoRunner(s)
	return s

}

func InjectCoRunner(s *CoRunner) {

	// logrus.Debug("inject")
}
