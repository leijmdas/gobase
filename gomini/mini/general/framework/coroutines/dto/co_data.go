package dto

import (
	"context"
	"github.com/emirpasic/gods/lists/arraylist"
	"time"
)

const DATA_WAIT_TIMEOUT = 5 * 60 * time.Second
const DATA_COROUTINES_NUMBER = 2

type CoData struct {
	Ctx context.Context
	//协程数
	GroupNumber int `json:"group_number"`
	//超时时间
	TimeoutSeconds time.Duration `json:"timeout_seconds"`

	GroupTasks *arraylist.List `json:"group_tasks"`
	Tasks      *arraylist.List `json:"tasks"`

	GroupOrder int `json:"group_order"`
}

func NewCoData() *CoData {
	return &CoData{
		///Ctx:            context.WithTimeout(context.Background(), 2*time.Second),
		GroupNumber:    DATA_COROUTINES_NUMBER,
		TimeoutSeconds: DATA_WAIT_TIMEOUT,
	}
}

func (this *CoData) DivideGroup() *CoData {
	if this.GroupTasks != nil {
		return this
	}

	this.GroupTasks = arraylist.New()
	if this.Tasks.Size() == 0 {
		return this
	} else if this.Tasks.Size() <= this.GroupNumber {
		this.GroupTasks.Add(this.Tasks)
	} else {
		this.Split2Group()
	}
	return this
}
func (this *CoData) GroupSize() int {

	var groupSize = this.Tasks.Size() / this.GroupNumber
	if this.Tasks.Size()%this.GroupNumber > 0 {
		groupSize += 1
	}
	//logrus.Info(groupSize, this.Tasks)
	return groupSize
}

func (this *CoData) Split2Group() {

	var groupSize = this.GroupSize()
	var j = 0
	var group = arraylist.New()
	for i := 0; i < this.Tasks.Size(); i++ {
		j++
		var task, ok = this.Tasks.Get(i)
		if ok {
			group.Add(task)
		}
		if j%groupSize == 0 || i == this.Tasks.Size()-1 {
			this.GroupTasks.Add(group)
			j = 0
			group = arraylist.New()
		}

	}

}
