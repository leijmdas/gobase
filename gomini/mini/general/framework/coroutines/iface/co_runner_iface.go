package iface

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/dto"
)

type CoRunnerIface interface {
	Execute(data *dto.CoData) *basedto.IchubResult

	GetData() *dto.CoData
}
