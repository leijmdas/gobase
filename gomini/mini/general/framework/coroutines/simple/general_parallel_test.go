package simple

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func worker(id int, wg *sync.WaitGroup) {
	defer wg.Done() // 通知主goroutine这个协程已经完成

	fmt.Printf("Worker %d starting\n", id)
	time.Sleep(time.Second) // 模拟工作
	fmt.Printf("Worker %d done\n", id)
}

func Test001(t *testing.T) {
	var wg sync.WaitGroup
	for i := 1; i <= 5; i++ {
		wg.Add(1) // 增加WaitGroup的计数
		go worker(i, &wg)
	}

	wg.Wait() // 等待所有协程完成
	fmt.Println("All workers completed")
}
