package gonats

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/gogf/gf/v2/util/gconv"
	"sync"
)

const chanNumber = 10
const chanSize = 100

type NatsEventFramework struct {
	basedto.BaseEntitySingle

	channels []chan string
	wg       sync.WaitGroup
}

func NewNatsEventFramework() *NatsEventFramework {
	var e = &NatsEventFramework{
		channels: make([]chan string, chanNumber),
	}
	e.init()
	e.InitProxy(e)
	return e
}

func (this *NatsEventFramework) init() {
	for i := 0; i < chanNumber; i++ {
		this.channels[i] = make(chan string, chanSize)
	}
}

func (this *NatsEventFramework) Send() {
	for i := 0; i < 10; i++ {
		this.wg.Add(1)
		go this.sendOne(this.channels[i], i)
	}
	this.wg.Wait()
}
func (this *NatsEventFramework) Recv() {

	for i := 0; i < 10; i++ {
		this.wg.Add(1)
		go this.recOne(this.channels[i])
	}
	this.wg.Wait()
}
func (this *NatsEventFramework) sendOne(ch chan string, i int) {
	defer this.wg.Done()
	var s = gconv.String(10000 + i)
	fmt.Println("send=", s)
	ch <- s
}

func (this *NatsEventFramework) recOne(ch chan string) {
	defer this.wg.Done()
	var s string
	s = <-ch
	fmt.Println("recv=", s)
}
