package gonats

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: nats_event_framework_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameNatsEventFramework = "gonats.NatsEventFramework"

// init register load
func init() {
	registerBeanNatsEventFramework()
}

// register NatsEventFramework
func registerBeanNatsEventFramework() {
	basedi.RegisterLoadBean(singleNameNatsEventFramework, LoadNatsEventFramework)
}

func FindBeanNatsEventFramework() *NatsEventFramework {
	bean, ok := basedi.FindBean(singleNameNatsEventFramework).(*NatsEventFramework)
	if !ok {
		logrus.Errorf("FindBeanNatsEventFramework: failed to cast bean to *NatsEventFramework")
		return nil
	}
	return bean

}

func LoadNatsEventFramework() baseiface.ISingleton {
	var s = NewNatsEventFramework()
	InjectNatsEventFramework(s)
	return s

}

func InjectNatsEventFramework(s *NatsEventFramework) {

	// logrus.Debug("inject")
}
