package gonats

import (
	"fmt"
	"sync"
	"testing"
)

func Test001_chn(t *testing.T) {
	FindBeanNatsEventFramework().Send()

	FindBeanNatsEventFramework().Recv()

}

func Test002_chn(t *testing.T) {

	// 创建10个channel
	channels := make([]chan int, 10)

	// 初始化channel数组
	for i := range channels {
		channels[i] = make(chan int)
	}

	// 用于等待所有goroutine完成
	var wg sync.WaitGroup

	// 启动写入数据的goroutine
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(ch chan int, val int) {
			defer wg.Done()
			ch <- val
		}(channels[i], i)
	}

	// 启动读取数据的goroutine
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(ch chan int) {
			defer wg.Done()
			val := <-ch
			fmt.Printf("Received value: %d\n", val)
		}(channels[i])
	}

	// 等待所有goroutine完成
	wg.Wait()
}
