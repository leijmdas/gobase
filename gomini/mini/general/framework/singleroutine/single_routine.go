package singleroutine

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/dto"
	"time"
)

type SingleRoutine struct {
	basedto.BaseEntitySingle
}

func NewSingleRoutine() *SingleRoutine {
	return &SingleRoutine{}
}

func (self *SingleRoutine) Execute(data *dto.CoData) *basedto.IchubResult {
	//timeout := time.After(time.Duration(iface.GetData().TimeoutSeconds))

	timeout := data.Ctx.Done()
	select {
	case <-timeout:
		// 超时或取消，退出协程
		goutils.Info("job is cancelled/timeout\n", data)
		fmt.Println(":timeout")
		return basedto.ResultSuccessData(data)
	default:
		// 模拟工作负载
		time.Sleep(30 * time.Millisecond)
		fmt.Println("working on \n", data)
	}
	return basedto.ResultSuccessData("ok")

}

func (self *SingleRoutine) GetData() *dto.CoData {
	//TODO implement me
	panic("implement me")

}
