package singleroutine

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: single_routine_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-06-04 10:08:54)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-06-04 10:08:54)

* *********************************************************/

const singleNameSingleRoutine = "singleroutine.SingleRoutine"

// init register load
func init() {
	registerBeanSingleRoutine()
}

// register SingleRoutine
func registerBeanSingleRoutine() {
	basedi.RegisterLoadBean(singleNameSingleRoutine, LoadSingleRoutine)
}

func FindBeanSingleRoutine() *SingleRoutine {
	bean, ok := basedi.FindBean(singleNameSingleRoutine).(*SingleRoutine)
	if !ok {
		logrus.Errorf("FindBeanSingleRoutine: failed to cast bean to *SingleRoutine")
		return nil
	}
	return bean

}

func LoadSingleRoutine() baseiface.ISingleton {
	var s = NewSingleRoutine()
	InjectSingleRoutine(s)
	return s

}

func InjectSingleRoutine(s *SingleRoutine) {

	// logrus.Debug("inject")
}
