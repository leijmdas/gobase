package singleroutine

import (
	"context"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/dto"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
	"time"
)

type TestSingleRoutineSuite struct {
	suite.Suite
	inst *SingleRoutine

	rootdir string
}

func (this *TestSingleRoutineSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanSingleRoutine()

	this.rootdir = utils.FindRootDir()

}

func TestSingleRoutineSuites(t *testing.T) {
	suite.Run(t, new(TestSingleRoutineSuite))
}
func work(ctx context.Context, job string) {
	// 模拟工作，使用一个无限循环来代表长时间运行的任务
	// 这里使用select来监听两个channel：ctx.Done()和另一个模拟工作的channel
	// ctx.Done()会在超时或取消时关闭
	for {
		select {
		case <-ctx.Done():
			// 超时或取消，退出协程
			fmt.Printf("job %s is cancelled/timeout\n", job)
			return
		default:
			// 模拟工作负载
			time.Sleep(100 * time.Millisecond)
			fmt.Printf("working on %s\n", job)
		}
	}
}
func (this *TestSingleRoutineSuite) Test001_Execute() {
	logrus.Info(1)
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	var data = dto.NewCoData()
	data.Ctx = ctx
	// 启动协程
	go FindBeanSingleRoutine().Execute(data)

	// 主协程等待超时或协程完成
	//	<-ctx.Done()

	fmt.Println("main goroutine continues...")
}

func (this *TestSingleRoutineSuite) Test002_GetData() {
	// 创建一个超时为2秒的context
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	// 启动协程
	work(ctx, "job1")

	// 主协程等待超时或协程完成

	fmt.Println("main goroutine continues...")

	//cancel()
}
