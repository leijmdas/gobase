package test

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines/dto"
	"gitee.com/leijmdas/gobase/gomini/mini/internal/model"
	"github.com/sirupsen/logrus"
	"reflect"
)

type RulesRunner struct {
	basedto.BaseEntity
	*coroutines.CoRunner `di:"auto"`
}

func NewRulesRunner() *RulesRunner {
	return &RulesRunner{
		CoRunner: coroutines.NewCoRunner(),
	}
}

func (g *RulesRunner) Execute(data *dto.CoData) *basedto.IchubResult {
	for i := 0; i < data.Tasks.Size(); i++ {
		var task, ok = data.Tasks.Get(i)
		if ok {
			rule, ok := task.(*model.Rules)
			if !ok {
				logrus.Error("任务类型不匹配", "taskType", reflect.TypeOf(task).String())
				continue
			}
			rule.Name = "NAME" + baseutils.Any2Str(i+10000*data.GroupOrder)
		} else {
			return basedto.NewIchubResult().FailMessage("处理类型不是Rules!")
		}
	}

	return basedto.NewIchubResult().SuccessData(data.Tasks)
}
