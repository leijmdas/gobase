package test

import (
	"fmt"
	"gitee.com/leijmdas/gobase/gomini/mini/general/framework/coroutines"
	"gitee.com/leijmdas/gobase/gomini/mini/internal/model"
	"github.com/emirpasic/gods/lists/arraylist"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func Test001_runner(t *testing.T) {
	//Execute()
	var rulesRunner = FindBeanRulesRunner()
	rulesRunner.GroupNumber = 2
	rulesRunner.Tasks = arraylist.New()

	rulesRunner.Tasks.Add(model.NewRules())
	rulesRunner.Tasks.Add(model.NewRules())
	rulesRunner.Tasks.Add(model.NewRules())

	var result = coroutines.FindBeanCoRoutines().Execute(rulesRunner)
	logrus.Error(result)

}

func Test002_coroutines(t *testing.T) {
	//Execute()
	var coRunner = coroutines.FindBeanCoRunner()
	coRunner.GroupNumber = 2
	coRunner.Tasks = arraylist.New()
	var rules = model.NewRules()
	coRunner.Tasks.Add(rules)
	coRunner.Tasks.Add(rules)
	coRunner.Tasks.Add(rules)
	coRunner.Tasks.Add(rules)
	coRunner.Tasks.Add(rules)
	coRunner.Tasks.Add(rules)
	coRunner.Tasks.Add(rules)
	coRunner.Tasks.Add(rules)

	var ret = coroutines.FindBeanCoRoutines().Execute(coRunner)
	logrus.Info(ret)
	assert.Equal(t, 200, ret.Code)

}

func sender(ch chan string) {
	ch <- "hello"
	ch <- "this"
	ch <- "is"
	ch <- "alice"
}

// recver 循环读取chan里面的数据，直到channel关闭
func recver(ch chan string) {
	for v := range ch {
		fmt.Println(v)
	}
}

func Test003_channel(t *testing.T) {
	ch := make(chan string)
	go sender(ch) // sender goroutine
	go recver(ch) // recver goroutine

	// 这个可以使用同步组 waitGroup来进行同步等待
	time.Sleep(1 * time.Second)
}
