package test

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: rules_runner_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameRulesRunner = "test.RulesRunner"

// init register load
func init() {
	registerBeanRulesRunner()
}

// register RulesRunner
func registerBeanRulesRunner() {
	basedi.RegisterLoadBean(singleNameRulesRunner, LoadRulesRunner)
}

func FindBeanRulesRunner() *RulesRunner {
	bean, ok := basedi.FindBean(singleNameRulesRunner).(*RulesRunner)
	if !ok {
		logrus.Errorf("FindBeanRulesRunner: failed to cast bean to *RulesRunner")
		return nil
	}
	return bean

}

func LoadRulesRunner() baseiface.ISingleton {
	var s = NewRulesRunner()
	InjectRulesRunner(s)
	return s

}

func InjectRulesRunner(s *RulesRunner) {

	// logrus.Debug("inject")
}
