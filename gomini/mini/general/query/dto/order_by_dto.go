package dto

import "github.com/gookit/goutil/strutil"

/*
@Title    文件名称: order_by_dto.go
@Description  描述:  OrderByDto

	@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
// 排序
type OrderByDto struct {
	//字段名 camlecase or case
	Field string `json:"field"`
	//排序：asc or desc
	Sort string `json:"sort"` //desc  asc

}

func (this *OrderByDto) keySnake(key string) string {
	return strutil.SnakeCase(key)
}
func NewOrderByDto() *OrderByDto {
	return &OrderByDto{}
}

func (this *OrderByDto) ToOrderBy() string {
	return this.keySnake(this.Field) + " " + this.Sort
}

func OrderBy(field, sort string) *OrderByDto {
	return &OrderByDto{Field: field, Sort: sort}
}

func (this *OrderByDto) SetSort(sort string) {
	this.Sort = sort
}

func (this *OrderByDto) SetField(field string) {
	this.Field = field
}
