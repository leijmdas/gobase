package dto

/*
@Title    文件名称: pageconsts.go
@Description  描述:  PageConsts

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
const (
	Eq = iota
	Ne
	Ge
	Gt
	Le
	Lt
	In
	Like
	Between
	NotIn
	NotLike
	NotBetween
	//	es
	EsFuzzy
	EsTerm
	EsTerms
	EsId
	EsIds
	EsMatch
	EsMatchAll
)

type FieldSign string

var OpSign = map[int]FieldSign{
	Eq: "=",
	Ne: "!=",
	Ge: ">=",
	Gt: ">",
	Le: "<=",
	Lt: "<",

	In:         "in",
	Like:       "like",
	Between:    "between",
	NotIn:      "not in",
	NotLike:    "not like",
	NotBetween: "not between",

	EsFuzzy:    "fuzzy",
	EsTerm:     "term",
	EsTerms:    "terms",
	EsId:       "id",
	EsIds:      "ids",
	EsMatch:    "match",
	EsMatchAll: "match-all",
}
