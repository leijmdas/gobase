package dto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
)

/*
@Title    文件名称: QueryField.go
@Description  描述:  QueryField

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

// 通用查询条件
type QueryField struct {
	basedto.BaseEntity
	// 字段名 小写
	Field string `json:"field"`
	//比较符 = != > < >= <= between notbetween in notin like notlike
	OpType FieldSign `json:"op_type"`
	//比较值：一个或多个值
	Values []interface{} `json:"values,omitempty"`
}

func NewQueryFields() *QueryField {
	var bfields = &QueryField{}
	bfields.InitProxy(bfields)
	return bfields
}

func NewFields(field string, opType int, opValues []any) *QueryField {

	var queryFields = &QueryField{
		Field:  field,
		OpType: OpSign[opType],
		Values: opValues,
	}
	queryFields.InitProxy(queryFields)
	return queryFields
}
func (this *QueryField) CheckType() string {
	return baseutils.CheckType(this.Values[0])
}

func (this *QueryField) SetField(field string) {
	this.Field = field
}

func (this *QueryField) Values2InStr() string {
	var s = ""
	for _, v := range this.Values {
		if len(s) > 0 {
			s += ","
		}
		if this.CheckType() == "string" {
			s += `"` + baseutils.Any2Str(v) + `"`
		} else {
			s += baseutils.Any2Str(v)
		}

	}
	return s

}
func (this *QueryField) Field2Keyword() string {
	return this.Field + ".keyword"
}
