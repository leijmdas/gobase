package dto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: query_field_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameQueryField = "dto.QueryField"

// init register load
func init() {
	registerBeanQueryField()
}

// register QueryField
func registerBeanQueryField() {
	basedi.RegisterLoadBean(singleNameQueryField, LoadQueryField)
}

func FindBeanQueryField() *QueryField {
	bean, ok := basedi.FindBean(singleNameQueryField).(*QueryField)
	if !ok {
		logrus.Errorf("FindBeanQueryField: failed to cast bean to *QueryField")
		return nil
	}
	return bean

}

func LoadQueryField() baseiface.ISingleton {
	var s = NewQueryFields()
	InjectQueryField(s)
	return s

}

func InjectQueryField(s *QueryField) {

	// logrus.Debug("inject")
}
