package model

/*
   @Title    文件名称: Department.gomodel
   @Description  描述: 实体Department

   @Author  作者: leijianming@163.com  时间(2024-03-12 13:04:48)
   @Update  作者: leijianming@163.com  时间(2024-03-12 13:04:48)

*/

import (
	"encoding/json"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basemodel"
	"github.com/jinzhu/gorm"
	jsoniter "github.com/json-iterator/go"
	_ "time"
)

/* 指定扩展结结构，单独存文件。生成时不会覆盖: */
//type DepartmentBase struct {ModelBase}

/*
 */
type Department struct {
	//	DepartmentBase
	//	ModelBase

	/*    */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:''" json:"id"`
	/*  编码  */
	Code string `gorm:"column:code;type:varchar(32);comment:'编码';default:'none'" json:"code"`
	/*  部门名称  */
	Name string `gorm:"column:name;type:varchar(32);comment:'部门名称'" json:"name"`
	/*    */
	ParentId int32 `gorm:"column:parent_id;type:int(11);comment:''" json:"parent_id"`
	/*    */
	DepPath string `gorm:"column:dep_path;type:varchar(255);comment:''" json:"dep_path"`

	Enabled uint8 `gorm:"column:enabled;type:bit(1);comment:''" json:"enabled"`

	IsParent int8 `gorm:"column:is_parent;type:tinyint(1);comment:'';default:0" json:"is_parent"`
	/*  负责人  */
	MngrId int32 `gorm:"column:mngr_id;type:int(11);comment:'负责人';default:0" json:"mngr_id"`
	/*  0-部门 1-班组  */
	Type int32 `gorm:"column:type;type:int(11);comment:'0-部门 1-班组';default:0" json:"type"`
}

func NewDepartment() *Department {
	return &Department{}
}

type DepartmentParams struct {

	/*    */
	Id *int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:''" json:"id"`
	/*  编码  */
	Code *string `gorm:"column:code;type:varchar(32);comment:'编码';default:\'none\'" json:"code"`
	/*  部门名称  */
	Name *string `gorm:"column:name;type:varchar(32);comment:'部门名称'" json:"name"`
	/*    */
	ParentId *int32 `gorm:"column:parent_id;type:int(11);comment:''" json:"parent_id"`
	/*    */
	DepPath *string `gorm:"column:dep_path;type:varchar(255);comment:''" json:"dep_path"`
	/*    */
	Enabled *basemodel.BitField `gorm:"column:enabled;type:bit(1);comment:''" json:"enabled"`
	/*    */
	IsParent *int8 `gorm:"column:is_parent;type:tinyint(1);comment:'';default:0" json:"is_parent"`
	/*  负责人  */
	MngrId *int32 `gorm:"column:mngr_id;type:int(11);comment:'负责人';default:0" json:"mngr_id"`
	/*  0-部门 1-班组  */
	Type *int32 `gorm:"column:type;type:int(11);comment:'0-部门 1-班组';default:0" json:"type"`
}
type DepartmentDto struct {

	/*    */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:''" json:"id"`
	/*  编码  */
	Code string `gorm:"column:code;type:varchar(32);comment:'编码';default:\'none\'" json:"code"`
	/*  部门名称  */
	Name string `gorm:"column:name;type:varchar(32);comment:'部门名称'" json:"name"`
	/*    */
	ParentId int32 `gorm:"column:parent_id;type:int(11);comment:''" json:"parent_id"`
	/*    */
	DepPath string `gorm:"column:dep_path;type:varchar(255);comment:''" json:"dep_path"`
	/*    */
	Enabled basemodel.BitField `gorm:"column:enabled;type:bit(1);comment:''" json:"enabled"`
	/*    */
	IsParent int8 `gorm:"column:is_parent;type:tinyint(1);comment:'';default:0" json:"is_parent"`
	/*  负责人  */
	MngrId int32 `gorm:"column:mngr_id;type:int(11);comment:'负责人';default:0" json:"mngr_id"`
	/*  0-部门 1-班组  */
	Type int32 `gorm:"column:type;type:int(11);comment:'0-部门 1-班组';default:0" json:"type"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *Department) TableName() string {

	return "department"
}

/*
迁移
*/
func (entity *Department) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(dbmenu)

	return err
}

/*
exec pg comment sql
*/
func (entity *Department) execComment(db *gorm.DB) {

	sql := "comment on table department is '';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *Department) String() string {
	s, err := jsoniter.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Department) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Department) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *Department) UnmarshalBy(body []byte) error {
	return json.Unmarshal(body, entity)

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *Department) Ini(iniPk bool) *Department {
	if iniPk {
		entity.Id = 0 //new(int32)
	}

	//entity.Code = new(string)

	//entity.Name = new(string)

	//entity.ParentId = new(int32)

	//entity.DepPath = new(string)

	//entity.Enabled = new(basemodel.BitField)

	//entity.IsParent = new(int8)

	//entity.MngrId = new(int32)

	//entity.Type = new(int32)

	return entity

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *Department) IniNil(iniPk bool) *Department {
	if iniPk {

		entity.Id = 0 //new(int32)

	}

	//if entity.Code == nil {
	//entity.Code = new(string)
	//}

	//if entity.Name == nil {
	//entity.Name = new(string)
	//}

	//if entity.ParentId == nil {
	//entity.ParentId = new(int32)
	//}

	//if entity.DepPath == nil {
	//entity.DepPath = new(string)
	//}

	//if entity.Enabled == nil {
	//entity.Enabled = new(basemodel.BitField)
	//}

	//if entity.IsParent == nil {
	//entity.IsParent = new(int8)
	//}

	//if entity.MngrId == nil {
	//entity.MngrId = new(int32)
	//}

	//if entity.Type == nil {
	//entity.Type = new(int32)
	//}

	return entity

}

func (entity *Department) copy(iniPk bool, cpentity *Department) *Department {

	entity.Ini(iniPk)
	cpentity.IniNil(iniPk)

	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID =  *cpentity.ID

	entity.Code = cpentity.Code
	entity.Name = cpentity.Name
	entity.ParentId = cpentity.ParentId
	entity.DepPath = cpentity.DepPath
	entity.IsParent = cpentity.IsParent
	entity.MngrId = cpentity.MngrId
	entity.Type = cpentity.Type
	return entity
}

func (entity *Department) copyNotNil(iniPk bool, cpentity *Department) *Department {
	entity.IniNil(iniPk)
	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID = *cpentity.ID

	entity.Code = cpentity.Code
	entity.Name = cpentity.Name
	entity.ParentId = cpentity.ParentId
	entity.DepPath = cpentity.DepPath
	entity.IsParent = cpentity.IsParent
	entity.MngrId = cpentity.MngrId
	entity.Type = cpentity.Type
	return entity
}

/*
func (entity *Department ) Model2PbMsg (pbentity *proto.DepartmentProto) * proto.DepartmentProto{

    entity.IniNil(true)


		pbentity.Id =  entity.GetId()
		pbentity.Code =  entity.GetCode()
		pbentity.Name =  entity.GetName()
		pbentity.ParentId =  entity.GetParentId()
		pbentity.DepPath =  entity.GetDepPath()
		pbentity.Enabled =  entity.GetEnabled()
		pbentity.IsParent =  entity.GetIsParent()
		pbentity.MngrId =  entity.GetMngrId()
		pbentity.Type =  entity.GetType()

    return pbentity
}

func (entity *Department ) PbMsg2Model (pbentity *proto.DepartmentProto) *Department{
    entity.IniNil(true)


		 * entity.Id =  pbentity.GetId()
		 * entity.Code =  pbentity.GetCode()
		 * entity.Name =  pbentity.GetName()
		 * entity.ParentId =  pbentity.GetParentId()
		 * entity.DepPath =  pbentity.GetDepPath()
		 entity.Enabled =  utils.Str2BoolPtr( pbentity.GetEnabled() )
		 * entity.IsParent =  pbentity.GetIsParent()
		 * entity.MngrId =  pbentity.GetMngrId()
		 * entity.Type =  pbentity.GetType()
    return entity
}


func (entity *Department) IniPbMsg() (pbentity *proto.<no value>Proto) {

	pbentity = &proto.<no value>Proto{}


		pbentity.Id =  0
		pbentity.Code =  ""
		pbentity.Name =  ""
		pbentity.ParentId =  0
		pbentity.DepPath =  ""
		pbentity.Enabled =  "false"
		pbentity.IsParent =  0
		pbentity.MngrId =  0
		pbentity.Type =  0
	return
}

*/

func (entity *Department) GetId() int32 {

	return entity.Id

}

func (entity *Department) SetId(Id int32) {
	entity.Id = Id
}

func (entity *Department) GetCode() string {

	return entity.Code

}

func (entity *Department) SetCode(Code string) {
	entity.Code = Code
}

func (entity *Department) GetName() string {

	return entity.Name

}

func (entity *Department) SetName(Name string) {
	entity.Name = Name
}

func (entity *Department) GetParentId() int32 {

	return entity.ParentId

}

func (entity *Department) SetParentId(ParentId int32) {
	entity.ParentId = ParentId
}

func (entity *Department) GetDepPath() string {

	return entity.DepPath

}

func (entity *Department) SetDepPath(DepPath string) {
	entity.DepPath = DepPath
}

func (entity *Department) GetIsParent() int8 {

	return entity.IsParent

}

func (entity *Department) SetIsParent(IsParent int8) {
	entity.IsParent = IsParent
}

func (entity *Department) GetMngrId() int32 {

	return entity.MngrId

}

func (entity *Department) SetMngrId(MngrId int32) {
	entity.MngrId = MngrId
}

func (entity *Department) GetType() int32 {

	return entity.Type

}

func (entity *Department) SetType(Type int32) {
	entity.Type = Type
}
