package model

/*
   @Title    文件名称: Employee.gomodel
   @Description  描述: 实体Employee

   @Author  作者: leijianming@163.com  时间(2024-03-14 09:43:07)
   @Update  作者: leijianming@163.com  时间(2024-03-14 09:43:07)

*/

import (
	"encoding/json"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/jinzhu/gorm"
	"time"
)

/* 指定扩展结结构，单独存文件。生成时不会覆盖: */
//type EmployeeBase struct {ModelBase}

/*
 */
type Employee struct {
	//	EmployeeBase
	basedto.BaseEntity

	/*  员工编号  */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:'员工编号'" json:"id"`
	/*  所属部门  */
	DepartmentId int32 `gorm:"column:department_id;type:int(11);comment:'所属部门';default:0" json:"department_id"`
	/*  员工姓名  */
	Name string `gorm:"column:name;type:varchar(16);comment:'员工姓名'" json:"name"`
	/*  性别  */
	Gender string `gorm:"column:gender;type:char(4);comment:'性别'" json:"gender"`
	/*  出生日期  */
	Birthday time.Time `gorm:"column:birthday;type:date;comment:'出生日期'" json:"birthday"`
	/*  身份证号  */
	IdCard string `gorm:"column:id_card;type:char(18);comment:'身份证号'" json:"id_card"`
	/*  婚姻状况  */
	Wedlock string `gorm:"column:wedlock;type:varchar(8);comment:'婚姻状况'" json:"wedlock"`
	/*  民族  */
	NationId int32 `gorm:"column:nation_id;type:int(8);comment:'民族'" json:"nation_id"`
	/*  籍贯  */
	NativePlace string `gorm:"column:native_place;type:varchar(20);comment:'籍贯'" json:"native_place"`
	/*  政治面貌  */
	PoliticId int32 `gorm:"column:politic_id;type:int(8);comment:'政治面貌'" json:"politic_id"`
	/*  邮箱  */
	Email string `gorm:"column:email;type:varchar(20);comment:'邮箱'" json:"email"`
	/*  电话号码  */
	Phone string `gorm:"column:phone;type:varchar(11);comment:'电话号码'" json:"phone"`
	/*  联系地址  */
	Address string `gorm:"column:address;type:varchar(64);comment:'联系地址'" json:"address"`
	/*  职称ID  */
	JobLevelId int32 `gorm:"column:job_level_id;type:int(11);comment:'职称ID'" json:"job_level_id"`
	/*  职位ID  */
	PosId int32 `gorm:"column:pos_id;type:int(11);comment:'职位ID'" json:"pos_id"`
	/*  聘用形式  */
	EngageForm string `gorm:"column:engage_form;type:varchar(8);comment:'聘用形式'" json:"engage_form"`
	/*  最高学历  */
	TiptopDegree string `gorm:"column:tiptop_degree;type:varchar(8);comment:'最高学历'" json:"tiptop_degree"`
	/*  所属专业  */
	Specialty string `gorm:"column:specialty;type:varchar(32);comment:'所属专业'" json:"specialty"`
	/*  毕业院校  */
	School string `gorm:"column:school;type:varchar(32);comment:'毕业院校'" json:"school"`
	/*  入职日期  */
	BeginDate time.Time `gorm:"column:begin_date;type:date;comment:'入职日期'" json:"begin_date"`
	/*  在职状态  */
	WorkState string `gorm:"column:work_state;type:varchar(8);comment:'在职状态';default:\'在职\'" json:"work_state"`
	/*  工号  */
	Code string `gorm:"column:code;type:varchar(8);comment:'工号'" json:"code"`
	/*  合同期限  */
	ContractTerm float64 `gorm:"column:contract_term;type:double;comment:'合同期限'" json:"contract_term"`
	/*  转正日期  */
	ConversionTime time.Time `gorm:"column:conversion_time;type:date;comment:'转正日期'" json:"conversion_time"`
	/*  离职日期  */
	NotWorkDate time.Time `gorm:"column:not_work_date;type:date;comment:'离职日期'" json:"not_work_date"`
	/*  合同起始日期  */
	BeginContract time.Time `gorm:"column:begin_contract;type:date;comment:'合同起始日期'" json:"begin_contract"`
	/*  合同终止日期  */
	EndContract time.Time `gorm:"column:end_contract;type:date;comment:'合同终止日期'" json:"end_contract"`
	/*  工龄  */
	WorkAge int32 `gorm:"column:work_age;type:int(11);comment:'工龄'" json:"work_age"`
	/*    */
	WorkId string `gorm:"column:work_id;type:varchar(16);comment:''" json:"work_id"`
}

func NewEmployee() *Employee {
	var m = &Employee{}
	m.InitProxy(m)
	return m
}

type EmployeeParams struct {

	/*  员工编号  */
	Id *int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:'员工编号'" json:"id"`
	/*  所属部门  */
	DepartmentId *int32 `gorm:"column:department_id;type:int(11);comment:'所属部门';default:0" json:"department_id"`
	/*  员工姓名  */
	Name *string `gorm:"column:name;type:varchar(16);comment:'员工姓名'" json:"name"`
	/*  性别  */
	Gender *string `gorm:"column:gender;type:char(4);comment:'性别'" json:"gender"`
	/*  出生日期  */
	Birthday *time.Time `gorm:"column:birthday;type:date;comment:'出生日期'" json:"birthday"`
	/*  身份证号  */
	IdCard *string `gorm:"column:id_card;type:char(18);comment:'身份证号'" json:"id_card"`
	/*  婚姻状况  */
	Wedlock *string `gorm:"column:wedlock;type:varchar(8);comment:'婚姻状况'" json:"wedlock"`
	/*  民族  */
	NationId *int32 `gorm:"column:nation_id;type:int(8);comment:'民族'" json:"nation_id"`
	/*  籍贯  */
	NativePlace *string `gorm:"column:native_place;type:varchar(20);comment:'籍贯'" json:"native_place"`
	/*  政治面貌  */
	PoliticId *int32 `gorm:"column:politic_id;type:int(8);comment:'政治面貌'" json:"politic_id"`
	/*  邮箱  */
	Email *string `gorm:"column:email;type:varchar(20);comment:'邮箱'" json:"email"`
	/*  电话号码  */
	Phone *string `gorm:"column:phone;type:varchar(11);comment:'电话号码'" json:"phone"`
	/*  联系地址  */
	Address *string `gorm:"column:address;type:varchar(64);comment:'联系地址'" json:"address"`
	/*  职称ID  */
	JobLevelId *int32 `gorm:"column:job_level_id;type:int(11);comment:'职称ID'" json:"job_level_id"`
	/*  职位ID  */
	PosId *int32 `gorm:"column:pos_id;type:int(11);comment:'职位ID'" json:"pos_id"`
	/*  聘用形式  */
	EngageForm *string `gorm:"column:engage_form;type:varchar(8);comment:'聘用形式'" json:"engage_form"`
	/*  最高学历  */
	TiptopDegree *string `gorm:"column:tiptop_degree;type:varchar(8);comment:'最高学历'" json:"tiptop_degree"`
	/*  所属专业  */
	Specialty *string `gorm:"column:specialty;type:varchar(32);comment:'所属专业'" json:"specialty"`
	/*  毕业院校  */
	School *string `gorm:"column:school;type:varchar(32);comment:'毕业院校'" json:"school"`
	/*  入职日期  */
	BeginDate *time.Time `gorm:"column:begin_date;type:date;comment:'入职日期'" json:"begin_date"`
	/*  在职状态  */
	WorkState *string `gorm:"column:work_state;type:varchar(8);comment:'在职状态';default:\'在职\'" json:"work_state"`
	/*  工号  */
	Code *string `gorm:"column:code;type:varchar(8);comment:'工号'" json:"code"`
	/*  合同期限  */
	ContractTerm *float64 `gorm:"column:contract_term;type:double;comment:'合同期限'" json:"contract_term"`
	/*  转正日期  */
	ConversionTime *time.Time `gorm:"column:conversion_time;type:date;comment:'转正日期'" json:"conversion_time"`
	/*  离职日期  */
	NotWorkDate *time.Time `gorm:"column:not_work_date;type:date;comment:'离职日期'" json:"not_work_date"`
	/*  合同起始日期  */
	BeginContract *time.Time `gorm:"column:begin_contract;type:date;comment:'合同起始日期'" json:"begin_contract"`
	/*  合同终止日期  */
	EndContract *time.Time `gorm:"column:end_contract;type:date;comment:'合同终止日期'" json:"end_contract"`
	/*  工龄  */
	WorkAge *int32 `gorm:"column:work_age;type:int(11);comment:'工龄'" json:"work_age"`
	/*    */
	WorkId *string `gorm:"column:work_id;type:varchar(16);comment:''" json:"work_id"`
}
type EmployeeDto struct {

	/*  员工编号  */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:'员工编号'" json:"id"`
	/*  所属部门  */
	DepartmentId int32 `gorm:"column:department_id;type:int(11);comment:'所属部门';default:0" json:"department_id"`
	/*  员工姓名  */
	Name string `gorm:"column:name;type:varchar(16);comment:'员工姓名'" json:"name"`
	/*  性别  */
	Gender string `gorm:"column:gender;type:char(4);comment:'性别'" json:"gender"`
	/*  出生日期  */
	Birthday int64 `gorm:"column:birthday;type:date;comment:'出生日期'" json:"birthday"`
	/*  身份证号  */
	IdCard string `gorm:"column:id_card;type:char(18);comment:'身份证号'" json:"id_card"`
	/*  婚姻状况  */
	Wedlock string `gorm:"column:wedlock;type:varchar(8);comment:'婚姻状况'" json:"wedlock"`
	/*  民族  */
	NationId int32 `gorm:"column:nation_id;type:int(8);comment:'民族'" json:"nation_id"`
	/*  籍贯  */
	NativePlace string `gorm:"column:native_place;type:varchar(20);comment:'籍贯'" json:"native_place"`
	/*  政治面貌  */
	PoliticId int32 `gorm:"column:politic_id;type:int(8);comment:'政治面貌'" json:"politic_id"`
	/*  邮箱  */
	Email string `gorm:"column:email;type:varchar(20);comment:'邮箱'" json:"email"`
	/*  电话号码  */
	Phone string `gorm:"column:phone;type:varchar(11);comment:'电话号码'" json:"phone"`
	/*  联系地址  */
	Address string `gorm:"column:address;type:varchar(64);comment:'联系地址'" json:"address"`
	/*  职称ID  */
	JobLevelId int32 `gorm:"column:job_level_id;type:int(11);comment:'职称ID'" json:"job_level_id"`
	/*  职位ID  */
	PosId int32 `gorm:"column:pos_id;type:int(11);comment:'职位ID'" json:"pos_id"`
	/*  聘用形式  */
	EngageForm string `gorm:"column:engage_form;type:varchar(8);comment:'聘用形式'" json:"engage_form"`
	/*  最高学历  */
	TiptopDegree string `gorm:"column:tiptop_degree;type:varchar(8);comment:'最高学历'" json:"tiptop_degree"`
	/*  所属专业  */
	Specialty string `gorm:"column:specialty;type:varchar(32);comment:'所属专业'" json:"specialty"`
	/*  毕业院校  */
	School string `gorm:"column:school;type:varchar(32);comment:'毕业院校'" json:"school"`
	/*  入职日期  */
	BeginDate int64 `gorm:"column:begin_date;type:date;comment:'入职日期'" json:"begin_date"`
	/*  在职状态  */
	WorkState string `gorm:"column:work_state;type:varchar(8);comment:'在职状态';default:\'在职\'" json:"work_state"`
	/*  工号  */
	Code string `gorm:"column:code;type:varchar(8);comment:'工号'" json:"code"`
	/*  合同期限  */
	ContractTerm float64 `gorm:"column:contract_term;type:double;comment:'合同期限'" json:"contract_term"`
	/*  转正日期  */
	ConversionTime int64 `gorm:"column:conversion_time;type:date;comment:'转正日期'" json:"conversion_time"`
	/*  离职日期  */
	NotWorkDate int64 `gorm:"column:not_work_date;type:date;comment:'离职日期'" json:"not_work_date"`
	/*  合同起始日期  */
	BeginContract int64 `gorm:"column:begin_contract;type:date;comment:'合同起始日期'" json:"begin_contract"`
	/*  合同终止日期  */
	EndContract int64 `gorm:"column:end_contract;type:date;comment:'合同终止日期'" json:"end_contract"`
	/*  工龄  */
	WorkAge int32 `gorm:"column:work_age;type:int(11);comment:'工龄'" json:"work_age"`
	/*    */
	WorkId string `gorm:"column:work_id;type:varchar(16);comment:''" json:"work_id"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *Employee) TableName() string {

	return "employee"
}

/*
迁移
*/
func (entity *Employee) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(dbmenu)

	return err
}

/*
exec pg comment sql
*/
func (entity *Employee) execComment(db *gorm.DB) {

	sql := "comment on table employee is '';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *Employee) String() string {
	s, err := json.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Employee) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Employee) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *Employee) UnmarshalBy(body []byte) error {
	return json.Unmarshal(body, entity)

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *Employee) Ini(iniPk bool) *Employee {
	if iniPk {
		entity.Id = 0 //new(int32)
	}

	//entity.DepartmentId = new(int32)

	//entity.Name = new(string)

	//entity.Gender = new(string)

	//entity.Birthday = new(time.Time)

	//entity.IdCard = new(string)

	//entity.Wedlock = new(string)

	//entity.NationId = new(int32)

	//entity.NativePlace = new(string)

	//entity.PoliticId = new(int32)

	//entity.Email = new(string)

	//entity.Phone = new(string)

	//entity.Address = new(string)

	//entity.JobLevelId = new(int32)

	//entity.PosId = new(int32)

	//entity.EngageForm = new(string)

	//entity.TiptopDegree = new(string)

	//entity.Specialty = new(string)

	//entity.School = new(string)

	//entity.BeginDate = new(time.Time)

	//entity.WorkState = new(string)

	//entity.Code = new(string)

	//entity.ContractTerm = new(float64)

	//entity.ConversionTime = new(time.Time)

	//entity.NotWorkDate = new(time.Time)

	//entity.BeginContract = new(time.Time)

	//entity.EndContract = new(time.Time)

	//entity.WorkAge = new(int32)

	//entity.WorkId = new(string)

	return entity

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *Employee) IniNil(iniPk bool) *Employee {
	if iniPk {

		entity.Id = 0 //new(int32)

	}

	//if entity.DepartmentId == nil {
	//entity.DepartmentId = new(int32)
	//}

	//if entity.Name == nil {
	//entity.Name = new(string)
	//}

	//if entity.Gender == nil {
	//entity.Gender = new(string)
	//}

	//if entity.Birthday == nil {
	//entity.Birthday = new(time.Time)
	//}

	//if entity.IdCard == nil {
	//entity.IdCard = new(string)
	//}

	//if entity.Wedlock == nil {
	//entity.Wedlock = new(string)
	//}

	//if entity.NationId == nil {
	//entity.NationId = new(int32)
	//}

	//if entity.NativePlace == nil {
	//entity.NativePlace = new(string)
	//}

	//if entity.PoliticId == nil {
	//entity.PoliticId = new(int32)
	//}

	//if entity.Email == nil {
	//entity.Email = new(string)
	//}

	//if entity.Phone == nil {
	//entity.Phone = new(string)
	//}

	//if entity.Address == nil {
	//entity.Address = new(string)
	//}

	//if entity.JobLevelId == nil {
	//entity.JobLevelId = new(int32)
	//}

	//if entity.PosId == nil {
	//entity.PosId = new(int32)
	//}

	//if entity.EngageForm == nil {
	//entity.EngageForm = new(string)
	//}

	//if entity.TiptopDegree == nil {
	//entity.TiptopDegree = new(string)
	//}

	//if entity.Specialty == nil {
	//entity.Specialty = new(string)
	//}

	//if entity.School == nil {
	//entity.School = new(string)
	//}

	//if entity.BeginDate == nil {
	//entity.BeginDate = new(time.Time)
	//}

	//if entity.WorkState == nil {
	//entity.WorkState = new(string)
	//}

	//if entity.Code == nil {
	//entity.Code = new(string)
	//}

	//if entity.ContractTerm == nil {
	//entity.ContractTerm = new(float64)
	//}

	//if entity.ConversionTime == nil {
	//entity.ConversionTime = new(time.Time)
	//}

	//if entity.NotWorkDate == nil {
	//entity.NotWorkDate = new(time.Time)
	//}

	//if entity.BeginContract == nil {
	//entity.BeginContract = new(time.Time)
	//}

	//if entity.EndContract == nil {
	//entity.EndContract = new(time.Time)
	//}

	//if entity.WorkAge == nil {
	//entity.WorkAge = new(int32)
	//}

	//if entity.WorkId == nil {
	//entity.WorkId = new(string)
	//}

	return entity

}

func (entity *Employee) copy(iniPk bool, cpentity *Employee) *Employee {

	entity.Ini(iniPk)
	cpentity.IniNil(iniPk)

	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID =  *cpentity.ID

	entity.DepartmentId = cpentity.DepartmentId
	entity.Name = cpentity.Name
	entity.Gender = cpentity.Gender
	entity.Birthday = cpentity.Birthday
	entity.IdCard = cpentity.IdCard
	entity.Wedlock = cpentity.Wedlock
	entity.NationId = cpentity.NationId
	entity.NativePlace = cpentity.NativePlace
	entity.PoliticId = cpentity.PoliticId
	entity.Email = cpentity.Email
	entity.Phone = cpentity.Phone
	entity.Address = cpentity.Address
	entity.JobLevelId = cpentity.JobLevelId
	entity.PosId = cpentity.PosId
	entity.EngageForm = cpentity.EngageForm
	entity.TiptopDegree = cpentity.TiptopDegree
	entity.Specialty = cpentity.Specialty
	entity.School = cpentity.School
	entity.BeginDate = cpentity.BeginDate
	entity.WorkState = cpentity.WorkState
	entity.Code = cpentity.Code
	entity.ContractTerm = cpentity.ContractTerm
	entity.ConversionTime = cpentity.ConversionTime
	entity.NotWorkDate = cpentity.NotWorkDate
	entity.BeginContract = cpentity.BeginContract
	entity.EndContract = cpentity.EndContract
	entity.WorkAge = cpentity.WorkAge
	entity.WorkId = cpentity.WorkId
	return entity
}

func (entity *Employee) copyNotNil(iniPk bool, cpentity *Employee) *Employee {
	entity.IniNil(iniPk)
	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID = *cpentity.ID

	entity.DepartmentId = cpentity.DepartmentId
	entity.Name = cpentity.Name
	entity.Gender = cpentity.Gender
	entity.Birthday = cpentity.Birthday
	entity.IdCard = cpentity.IdCard
	entity.Wedlock = cpentity.Wedlock
	entity.NationId = cpentity.NationId
	entity.NativePlace = cpentity.NativePlace
	entity.PoliticId = cpentity.PoliticId
	entity.Email = cpentity.Email
	entity.Phone = cpentity.Phone
	entity.Address = cpentity.Address
	entity.JobLevelId = cpentity.JobLevelId
	entity.PosId = cpentity.PosId
	entity.EngageForm = cpentity.EngageForm
	entity.TiptopDegree = cpentity.TiptopDegree
	entity.Specialty = cpentity.Specialty
	entity.School = cpentity.School
	entity.BeginDate = cpentity.BeginDate
	entity.WorkState = cpentity.WorkState
	entity.Code = cpentity.Code
	entity.ContractTerm = cpentity.ContractTerm
	entity.ConversionTime = cpentity.ConversionTime
	entity.NotWorkDate = cpentity.NotWorkDate
	entity.BeginContract = cpentity.BeginContract
	entity.EndContract = cpentity.EndContract
	entity.WorkAge = cpentity.WorkAge
	entity.WorkId = cpentity.WorkId
	return entity
}

/*
func (entity *Employee ) Model2PbMsg (pbentity *proto.EmployeeProto) * proto.EmployeeProto{

    entity.IniNil(true)


		pbentity.Id =  entity.GetId()
		pbentity.DepartmentId =  entity.GetDepartmentId()
		pbentity.Name =  entity.GetName()
		pbentity.Gender =  entity.GetGender()
		pbentity.Birthday =  entity.GetBirthday()
		pbentity.IdCard =  entity.GetIdCard()
		pbentity.Wedlock =  entity.GetWedlock()
		pbentity.NationId =  entity.GetNationId()
		pbentity.NativePlace =  entity.GetNativePlace()
		pbentity.PoliticId =  entity.GetPoliticId()
		pbentity.Email =  entity.GetEmail()
		pbentity.Phone =  entity.GetPhone()
		pbentity.Address =  entity.GetAddress()
		pbentity.JobLevelId =  entity.GetJobLevelId()
		pbentity.PosId =  entity.GetPosId()
		pbentity.EngageForm =  entity.GetEngageForm()
		pbentity.TiptopDegree =  entity.GetTiptopDegree()
		pbentity.Specialty =  entity.GetSpecialty()
		pbentity.School =  entity.GetSchool()
		pbentity.BeginDate =  entity.GetBeginDate()
		pbentity.WorkState =  entity.GetWorkState()
		pbentity.Code =  entity.GetCode()
		pbentity.ContractTerm =  entity.GetContractTerm()
		pbentity.ConversionTime =  entity.GetConversionTime()
		pbentity.NotWorkDate =  entity.GetNotWorkDate()
		pbentity.BeginContract =  entity.GetBeginContract()
		pbentity.EndContract =  entity.GetEndContract()
		pbentity.WorkAge =  entity.GetWorkAge()
		pbentity.WorkId =  entity.GetWorkId()

    return pbentity
}

func (entity *Employee ) PbMsg2Model (pbentity *proto.EmployeeProto) *Employee{
    entity.IniNil(true)


		 * entity.Id =  pbentity.GetId()
		 * entity.DepartmentId =  pbentity.GetDepartmentId()
		 * entity.Name =  pbentity.GetName()
		 * entity.Gender =  pbentity.GetGender()
		 * entity.Birthday =  pbentity.GetBirthday()
		 * entity.IdCard =  pbentity.GetIdCard()
		 * entity.Wedlock =  pbentity.GetWedlock()
		 * entity.NationId =  pbentity.GetNationId()
		 * entity.NativePlace =  pbentity.GetNativePlace()
		 * entity.PoliticId =  pbentity.GetPoliticId()
		 * entity.Email =  pbentity.GetEmail()
		 * entity.Phone =  pbentity.GetPhone()
		 * entity.Address =  pbentity.GetAddress()
		 * entity.JobLevelId =  pbentity.GetJobLevelId()
		 * entity.PosId =  pbentity.GetPosId()
		 * entity.EngageForm =  pbentity.GetEngageForm()
		 * entity.TiptopDegree =  pbentity.GetTiptopDegree()
		 * entity.Specialty =  pbentity.GetSpecialty()
		 * entity.School =  pbentity.GetSchool()
		 * entity.BeginDate =  pbentity.GetBeginDate()
		 * entity.WorkState =  pbentity.GetWorkState()
		 * entity.Code =  pbentity.GetCode()
		 * entity.ContractTerm =  pbentity.GetContractTerm()
		 * entity.ConversionTime =  pbentity.GetConversionTime()
		 * entity.NotWorkDate =  pbentity.GetNotWorkDate()
		 * entity.BeginContract =  pbentity.GetBeginContract()
		 * entity.EndContract =  pbentity.GetEndContract()
		 * entity.WorkAge =  pbentity.GetWorkAge()
		 * entity.WorkId =  pbentity.GetWorkId()
    return entity
}


func (entity *Employee) IniPbMsg() (pbentity *proto.<no value>Proto) {

	pbentity = &proto.<no value>Proto{}


		pbentity.Id =  0
		pbentity.DepartmentId =  0
		pbentity.Name =  ""
		pbentity.Gender =  ""
		pbentity.Birthday =  <no value>
		pbentity.IdCard =  ""
		pbentity.Wedlock =  ""
		pbentity.NationId =  0
		pbentity.NativePlace =  ""
		pbentity.PoliticId =  0
		pbentity.Email =  ""
		pbentity.Phone =  ""
		pbentity.Address =  ""
		pbentity.JobLevelId =  0
		pbentity.PosId =  0
		pbentity.EngageForm =  ""
		pbentity.TiptopDegree =  ""
		pbentity.Specialty =  ""
		pbentity.School =  ""
		pbentity.BeginDate =  <no value>
		pbentity.WorkState =  ""
		pbentity.Code =  ""
		pbentity.ContractTerm =  0
		pbentity.ConversionTime =  <no value>
		pbentity.NotWorkDate =  <no value>
		pbentity.BeginContract =  <no value>
		pbentity.EndContract =  <no value>
		pbentity.WorkAge =  0
		pbentity.WorkId =  ""
	return
}

*/

func (entity *Employee) GetId() int32 {

	return entity.Id

}

func (entity *Employee) SetId(Id int32) {
	entity.Id = Id
}

func (entity *Employee) GetDepartmentId() int32 {

	return entity.DepartmentId

}

func (entity *Employee) SetDepartmentId(DepartmentId int32) {
	entity.DepartmentId = DepartmentId
}

func (entity *Employee) GetName() string {

	return entity.Name

}

func (entity *Employee) SetName(Name string) {
	entity.Name = Name
}

func (entity *Employee) GetGender() string {

	return entity.Gender

}

func (entity *Employee) SetGender(Gender string) {
	entity.Gender = Gender
}

func (entity *Employee) GetBirthday() time.Time {

	return entity.Birthday

}

func (entity *Employee) SetBirthday(Birthday time.Time) {
	entity.Birthday = Birthday
}

func (entity *Employee) GetIdCard() string {

	return entity.IdCard

}

func (entity *Employee) SetIdCard(IdCard string) {
	entity.IdCard = IdCard
}

func (entity *Employee) GetWedlock() string {

	return entity.Wedlock

}

func (entity *Employee) SetWedlock(Wedlock string) {
	entity.Wedlock = Wedlock
}

func (entity *Employee) GetNationId() int32 {

	return entity.NationId

}

func (entity *Employee) SetNationId(NationId int32) {
	entity.NationId = NationId
}

func (entity *Employee) GetNativePlace() string {

	return entity.NativePlace

}

func (entity *Employee) SetNativePlace(NativePlace string) {
	entity.NativePlace = NativePlace
}

func (entity *Employee) GetPoliticId() int32 {

	return entity.PoliticId

}

func (entity *Employee) SetPoliticId(PoliticId int32) {
	entity.PoliticId = PoliticId
}

func (entity *Employee) GetEmail() string {

	return entity.Email

}

func (entity *Employee) SetEmail(Email string) {
	entity.Email = Email
}

func (entity *Employee) GetPhone() string {

	return entity.Phone

}

func (entity *Employee) SetPhone(Phone string) {
	entity.Phone = Phone
}

func (entity *Employee) GetAddress() string {

	return entity.Address

}

func (entity *Employee) SetAddress(Address string) {
	entity.Address = Address
}

func (entity *Employee) GetJobLevelId() int32 {

	return entity.JobLevelId

}

func (entity *Employee) SetJobLevelId(JobLevelId int32) {
	entity.JobLevelId = JobLevelId
}

func (entity *Employee) GetPosId() int32 {

	return entity.PosId

}

func (entity *Employee) SetPosId(PosId int32) {
	entity.PosId = PosId
}

func (entity *Employee) GetEngageForm() string {

	return entity.EngageForm

}

func (entity *Employee) SetEngageForm(EngageForm string) {
	entity.EngageForm = EngageForm
}

func (entity *Employee) GetTiptopDegree() string {

	return entity.TiptopDegree

}

func (entity *Employee) SetTiptopDegree(TiptopDegree string) {
	entity.TiptopDegree = TiptopDegree
}

func (entity *Employee) GetSpecialty() string {

	return entity.Specialty

}

func (entity *Employee) SetSpecialty(Specialty string) {
	entity.Specialty = Specialty
}

func (entity *Employee) GetSchool() string {

	return entity.School

}

func (entity *Employee) SetSchool(School string) {
	entity.School = School
}

func (entity *Employee) GetBeginDate() time.Time {

	return entity.BeginDate

}

func (entity *Employee) SetBeginDate(BeginDate time.Time) {
	entity.BeginDate = BeginDate
}

func (entity *Employee) GetWorkState() string {

	return entity.WorkState

}

func (entity *Employee) SetWorkState(WorkState string) {
	entity.WorkState = WorkState
}

func (entity *Employee) GetCode() string {

	return entity.Code

}

func (entity *Employee) SetCode(Code string) {
	entity.Code = Code
}

func (entity *Employee) GetContractTerm() float64 {

	return entity.ContractTerm

}

func (entity *Employee) SetContractTerm(ContractTerm float64) {
	entity.ContractTerm = ContractTerm
}

func (entity *Employee) GetConversionTime() time.Time {

	return entity.ConversionTime

}

func (entity *Employee) SetConversionTime(ConversionTime time.Time) {
	entity.ConversionTime = ConversionTime
}

func (entity *Employee) GetNotWorkDate() time.Time {

	return entity.NotWorkDate

}

func (entity *Employee) SetNotWorkDate(NotWorkDate time.Time) {
	entity.NotWorkDate = NotWorkDate
}

func (entity *Employee) GetBeginContract() time.Time {

	return entity.BeginContract

}

func (entity *Employee) SetBeginContract(BeginContract time.Time) {
	entity.BeginContract = BeginContract
}

func (entity *Employee) GetEndContract() time.Time {

	return entity.EndContract

}

func (entity *Employee) SetEndContract(EndContract time.Time) {
	entity.EndContract = EndContract
}

func (entity *Employee) GetWorkAge() int32 {

	return entity.WorkAge

}

func (entity *Employee) SetWorkAge(WorkAge int32) {
	entity.WorkAge = WorkAge
}

func (entity *Employee) GetWorkId() string {

	return entity.WorkId

}

func (entity *Employee) SetWorkId(WorkId string) {
	entity.WorkId = WorkId
}
