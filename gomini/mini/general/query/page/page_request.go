package page

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/dbcontent"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	pagedto2 "gitee.com/leijmdas/gobase/gomini/mini/general/query/dto"
	"github.com/duke-git/lancet/strutil"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

/*
@Title    文件名称: page_request.go
@Description  描述:  PageRequest

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
type PageRequest struct {
	basedto.BaseEntity `json:"-"`

	//每页记录数
	PageSize int `json:"page_size"`
	//当前页码
	PageCurrent int `json:"current"`
	//排序字段数组
	OrderBys []*pagedto2.OrderByDto `json:"order_by"`
	//查询字段条件
	Fields []*pagedto2.QueryField `json:"fields"`

	//Param interface{} `json:"param,omitempty"`

}

func NewPageRequest(pageSize int, pageCurrent int) *PageRequest {
	var pageRequest = &PageRequest{
		PageSize:    pageSize,
		PageCurrent: pageCurrent,

		OrderBys: []*pagedto2.OrderByDto{},
		Fields:   make([]*pagedto2.QueryField, 0),
	}

	pageRequest.InitProxy(pageRequest)
	return pageRequest
}

func NewIchubPageRequest() *PageRequest {

	return NewPageRequest(
		baseconsts.PAGE_SIZE_DEFAULT,
		baseconsts.PAGE_CURRENT)
}

func (this *PageRequest) InitPage() {

	if this.PageSize <= baseconsts.PAGE_SIZE_ZERO {

		this.PageSize = baseconsts.PAGE_SIZE_DEFAULT

	} else if this.PageSize > baseconsts.PAGE_SIZE_MAX {

		this.PageSize = baseconsts.PAGE_SIZE_MAX

	}
	if this.PageCurrent <= 0 {
		this.PageCurrent = baseconsts.PAGE_CURRENT
	}

}

func (this *PageRequest) GetDB() *gorm.DB {

	var db, _ = dbcontent.GetDB()
	return db
}

func (this *PageRequest) Clear() {
	this.PageSize = baseconsts.PAGE_SIZE_DEFAULT
	this.PageCurrent = baseconsts.PAGE_CURRENT

	this.OrderBys = []*pagedto2.OrderByDto{}
	this.Fields = make([]*pagedto2.QueryField, 0)
}

func (this *PageRequest) FindFieldSign(op int) pagedto2.FieldSign {
	return pagedto2.OpSign[op]

}

func (this *PageRequest) Start() int {
	return (this.PageCurrent - 1) * this.PageSize
}
func (this *PageRequest) Limit() int {
	return this.PageSize
}

func (this *PageRequest) keySnake(key string) string {
	return strutil.SnakeCase(key)
}
func (this *PageRequest) CheckTyope(field string, value interface{}) {
	baseutils.CheckType(value)
}

func (this *PageRequest) QueryFields(f string, opType int, opValues []interface{}) *PageRequest {

	var field = pagedto2.NewFields(this.keySnake(f), opType, opValues)
	this.Fields = append(this.Fields, field)

	return this
}

func (this *PageRequest) Eq(field string, opValues any) *PageRequest {

	return this.QueryFields(field, pagedto2.Eq, []any{opValues})
}
func (this *PageRequest) Ge(field string, opValue any) *PageRequest {

	return this.QueryFields(field, pagedto2.Ge, []any{opValue})
}
func (this *PageRequest) Le(field string, opValue any) *PageRequest {

	return this.QueryFields(field, pagedto2.Le, []any{opValue})
}
func (this *PageRequest) Lt(field string, opValue any) *PageRequest {

	return this.QueryFields(field, pagedto2.Lt, []any{opValue})
}
func (this *PageRequest) Gt(field string, opValue any) *PageRequest {

	return this.QueryFields(field, pagedto2.Gt, []any{opValue})
}
func (this *PageRequest) In(field string, opValues []any) *PageRequest {

	return this.QueryFields(field, pagedto2.In, opValues)
}

func (this *PageRequest) NotIn(field string, opValues []any) *PageRequest {

	return this.QueryFields(field, pagedto2.NotIn, opValues)
}
func (this *PageRequest) Like(field string, opValue any) *PageRequest {

	return this.QueryFields(field, pagedto2.Like, []any{opValue})
}

func (this *PageRequest) NotLike(field string, opValue any) *PageRequest {

	return this.QueryFields(field, pagedto2.NotLike, []any{opValue})
}
func (this *PageRequest) Between(field string, opValues []any) *PageRequest {

	return this.QueryFields(field, pagedto2.Between, opValues)
}
func (this *PageRequest) NotBetween(field string, opValues []any) *PageRequest {

	return this.QueryFields(field, pagedto2.NotBetween, opValues)
}
func (this *PageRequest) OrderBy(field string, sort string) *PageRequest {

	var orderby = pagedto2.OrderBy(field, sort)
	this.OrderBys = append(this.OrderBys, orderby)
	return this
}

func (this *PageRequest) OrderByDesc(field string) *PageRequest {

	return this.OrderBy(field, "desc")
}
func (this *PageRequest) OrderByAsc(field string) *PageRequest {

	return this.OrderBy(field, "asc")
}

func (this *PageRequest) ToMap() (*map[string]interface{}, error) {
	var str = jsonutils.ToJsonPretty(this)
	m, err := jsonutils.MapFromJson(str)
	if err != nil {
		logrus.Error(err)
	}
	return m, err

}

//update

func (this *PageRequest) FindByTable(table string, result any) error {

	dbc := this.GetDB().Table(table)
	dbc = this.BuildWhere(dbc)
	dbc = this.Order(dbc)
	dbc = this.SetLimit(dbc)
	dbc = dbc.Find(result)

	return dbc.Error
}

func (this *PageRequest) Insert(model any) (any, error) {

	err := this.GetDB().Create(model).Error
	if err != nil {
		logrus.Error(err.Error())

	}

	return model, err
}
func (this *PageRequest) Update(model any, pkey int64) (any, error) {

	err := this.GetDB().Model(model).Where("rule_id=?", pkey).Updates(model).Error
	if err != nil {
		logrus.Error(err.Error())
		return -1, err
	}
	return model, err

}

func (this *PageRequest) FindBy(model any, result any) error {

	dbc := this.GetDB().Model(model)
	dbc = this.BuildWhere(dbc)
	dbc = this.Order(dbc)
	dbc = this.SetLimit(dbc)
	dbc = dbc.Find(result)

	return dbc.Error
}
func (this *PageRequest) TransOpType(field *pagedto2.QueryField) {
	if field.OpType == "notin" {
		field.OpType = "not in"
	}
	if field.OpType == "notlike" {
		field.OpType = "not like"
	}
	if field.OpType == "notbetween" {
		field.OpType = "not between"
	}
}
func (this *PageRequest) BuildWhere(db *gorm.DB) *gorm.DB {

	this.InitPage()
	if this.Fields == nil {
		return db
	}
	for _, field := range this.Fields {
		this.TransOpType(field)
		//if notbetween noin notlike
		if field.OpType == pagedto2.OpSign[pagedto2.Between] {
			db = db.Where(fmt.Sprintf("%s BETWEEN ? and ?", field.Field),
				field.Values[0], field.Values[1])
		}
		if field.OpType == pagedto2.OpSign[pagedto2.NotBetween] {
			db = db.Where(fmt.Sprintf("%s Not BETWEEN ? and ?", field.Field),
				field.Values[0], field.Values[1])
		}

		if field.OpType == pagedto2.OpSign[pagedto2.Ge] {
			db = db.Where(fmt.Sprintf("%s >= ?", field.Field), field.Values[0])
		}
		if field.OpType == pagedto2.OpSign[pagedto2.Gt] {
			db = db.Where(fmt.Sprintf("%s > ?", field.Field), field.Values[0])
		}
		if field.OpType == pagedto2.OpSign[pagedto2.Le] {
			db = db.Where(fmt.Sprintf("%s <= ?", field.Field), field.Values[0])
		}
		if field.OpType == pagedto2.OpSign[pagedto2.Lt] {
			db = db.Where(fmt.Sprintf("%s < ?", field.Field), field.Values[0])
		}
		if field.OpType == pagedto2.OpSign[pagedto2.Eq] {
			db = db.Where(fmt.Sprintf("%s = ?", field.Field), field.Values[0])
		}
		if field.OpType == pagedto2.OpSign[pagedto2.Ne] {
			db = db.Where(fmt.Sprintf("%s != ?", field.Field), field.Values[0])
		}
		if field.OpType == pagedto2.OpSign[pagedto2.In] {

			db = db.Where(fmt.Sprintf("%s in (%s)", field.Field, field.Values2InStr()))
		}
		if field.OpType == pagedto2.OpSign[pagedto2.NotIn] {
			db = db.Where(fmt.Sprintf("%s not in (%s)", field.Field, field.Values2InStr()))
		}
		if field.OpType == pagedto2.OpSign[pagedto2.Like] {
			var sval = baseutils.Any2Str(field.Values[0])
			db = db.Where(fmt.Sprintf("%s like ?", field.Field), "'%"+sval+"%'")
		}
		if field.OpType == pagedto2.OpSign[pagedto2.NotLike] {
			var sval = baseutils.Any2Str(field.Values[0])
			db = db.Where(fmt.Sprintf("%s not like ?", field.Field), "'%"+sval+"%'")
		}
	}

	return db
}

func (this *PageRequest) SetLimit(dbc *gorm.DB) *gorm.DB {
	this.InitPage()
	return dbc.Offset(this.Start()).Limit(this.Limit())

}
func (this *PageRequest) Order(dbc *gorm.DB) *gorm.DB {
	if len(this.OrderBys) > 0 {

		for _, orderBy := range this.OrderBys {
			dbc = dbc.Order(orderBy.ToOrderBy())
		}
	}
	return dbc
}

func (this *PageRequest) Count(model any) (int, error) {
	dbc := this.GetDB().Model(model)
	dbc = this.BuildWhere(dbc).Offset(0).Limit(1)
	var count int
	if err := dbc.Count(&count).Error; err != nil {
		logrus.Error(err)
		return 0, err
	}

	logrus.Info("\ncount=", count)
	return count, nil

}
func (this *PageRequest) CountTable(table string) (int, error) {
	dbc := this.GetDB().Table(table)
	dbc = this.BuildWhere(dbc).Offset(0).Limit(1)
	var count int
	if err := dbc.Count(&count).Error; err != nil {
		logrus.Error(err)
		return 0, err
	}

	logrus.Info("\ncount=", count)
	return count, nil

}
func (this *PageRequest) QueryTable(table string, models any) *PageResult {

	Name := table + "_pagerequest.json"
	gocontext.FindBeanGoRulefileCtx().WriteDaoFile(Name, this.ToPrettyString())
	this.InitPage()

	count, err := this.CountTable(table)
	if err != nil {
		logrus.Error(err)
		return NewPageResultError(err.Error())
	}
	if count > 0 {
		err = this.FindByTable(table, models)
		if err != nil {
			logrus.Error(err)
			return NewPageResultError(err.Error())
		}
	}
	var result = PageResultOf(this)
	result.Total = count
	result.Data = models
	Name = table + "_pageresult.json"
	gocontext.FindBeanGoRulefileCtx().WriteDaoFile(Name, result.ToPrettyString())

	ichublog.Log(result)
	return result
}
func (this *PageRequest) Query(model any, models any) *PageResult {

	Name := baseutils.NameOfType(model) + "_pagerequest.json"
	ichublog.Info(this.ToPrettyString())
	gocontext.FindBeanGoRulefileCtx().WriteDaoFile(Name, this.ToPrettyString())
	this.InitPage()

	count, err := this.Count(model)
	if err != nil {
		logrus.Error(err)
		return NewPageResultError(err.Error())
	}
	if count > 0 {
		err = this.FindBy(model, models)
		if err != nil {
			logrus.Error(err)
			return NewPageResultError(err.Error())
		}
	}
	var result = PageResultOf(this)
	result.Total = count
	result.Data = models
	Name = baseutils.NameOfType(model) + "_pageresult.json"
	gocontext.FindBeanGoRulefileCtx().WriteDaoFile(Name, result.ToPrettyString())

	ichublog.Info(result)
	return result
}

func (this *PageRequest) FindById(model any, key int64) (bool, error) {

	db := this.GetDB().First(model, key)
	return db.RecordNotFound(), db.Error
}
