package page

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: page_request_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNamePageRequest = "page.PageRequest"

// init register load
func init() {
	registerBeanPageRequest()
}

// register PageRequest
func registerBeanPageRequest() {
	basedi.RegisterLoadBean(singleNamePageRequest, LoadPageRequest)
}

func FindBeanPageRequest() *PageRequest {
	bean, ok := basedi.FindBean(singleNamePageRequest).(*PageRequest)
	if !ok {
		logrus.Errorf("FindBeanPageRequest: failed to cast bean to *PageRequest")
		return nil
	}
	return bean

}

func LoadPageRequest() baseiface.ISingleton {
	var s = NewIchubPageRequest()
	InjectPageRequest(s)
	return s

}

func InjectPageRequest(s *PageRequest) {

	// logrus.Debug("inject")
}
