package page

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/model"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func init() {
	ichublog.InitLogrus()
}

type User struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}
type PageRequestUser struct {
	PageRequest
	param User
}

func NewPageRequestUser() *PageRequestUser {
	var pr = &PageRequestUser{}
	return pr
}
func Test0002_QueryRuleTable(t *testing.T) {
	var pageRequest = NewIchubPageRequest()

	pageRequest.OrderBy("ruleId", "desc")
	//	pageRequestBase.InFields("ruleId", []string{"1", "2221"})

	r := []map[string]any{
		{"rule_id": 1},
	}
	var pageResult = pageRequest.QueryTable("rules", &r)

	assert.Equal(t, 200, pageResult.Code)

}

func Test0003_QueryEmployee(t *testing.T) {
	var pageRequest = NewPageRequest(3, 1)

	pageRequest.OrderBy("Id", "asc")

	var pageResult = pageRequest.Query(&model.Employee{}, &[]model.Employee{})

	assert.Equal(t, 200, pageResult.Code)
	logrus.Info(pageResult)
}

func Test0004_FindById(t *testing.T) {
	var req = NewIchubPageRequest()

	var rule = &model.Department{}
	var notFound, err = req.FindById(rule, 2)
	if err != nil {
		logrus.Error(err)
		return
	}
	logrus.Info("notFound  = ", notFound)

}

func Test0005_QueryDept(t *testing.T) {
	var pageRequest = NewPageRequest(3, 1)

	pageRequest.OrderBy("DeptId", "asc") //	pageRequestBase.InFields("ruleId", []string{"1", "2221"})
	pageRequest.Ge("DeptId", 1)

	pageRequest.Gt("CreateTime", 0)
	var t1 = stringutils.Strtime2Time("2020-01-01 01:01:01")
	pageRequest.Gt("CreateTime", t1)
	pageRequest.Gt("CreateTime", "2011-01-01 01:01:01")
	pageRequest.Between("CreateTime", []any{"2011-01-01 01:01:01", "2031-01-01 01:01:01"})
	pageRequest.Between("DeptId", []any{100, 102})
	pageRequest.In("DeptId", []any{"100", "102"})
	pageRequest.In("DeptId", []any{100, 102})
	//pageRequestBase.Param2Between()

	var models = []model.SysDept{}
	//	var pageResult = pageRequestBase.Query(&model.SysDept{}, &models)
	var pageResult = pageRequest.QueryTable("sys_dept", &models)
	logrus.Info(pageResult)

	logrus.Info(time.Now(), t1)
}

func Test0007_QueryRuleParams(t *testing.T) {
	var pageRequest = NewPageRequest(3, 1)

	pageRequest.OrderBy("id", "desc")
	var models = []model.Department{}
	var pageResult = pageRequest.Query(&model.Department{}, &models)
	logrus.Info(pageResult)

}

//ReflectTags

func Test0008_ParseFields(t *testing.T) {
	//var refTags = reflectutils.NewReflectTags()
	//refTags.ParseTags(&model.SysDept{})
	//logrus.Info(jsonutils.ToJsonPretty(refTags.TagMap()))
	//var pageRequest = NewPageRequest(3, 1)
	//
	////pageRequestBase.Param = &model.SysDept{DeptId: 1}
	////	pageRequestBase.Param2Fields()
	//var models = []model.SysDept{}
	//var pageResult = pageRequest.Query(&model.SysDept{}, &models)
	//logrus.Info(pageResult)
	//var user = User{}
	//logrus.Info(jsonutils.ToJsonPretty(user))

}
