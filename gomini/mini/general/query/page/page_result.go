package page

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
)

/*
@Title    文件名称: page_result.go
@Description  描述:  PageResult

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
type PageResult struct {
	basedto.BaseEntity `gqlIgnored:"true"`

	Code int    `json:"code"`
	Msg  string `json:"msg"`

	PageSize    int `json:"page_size,omitempty"`
	PageCurrent int `json:"current,omitempty"`

	Total int         `json:"total,omitempty"`
	Data  interface{} `json:"data"`
	// Icode , Imsg string

}

func NewIchubPageResult() *PageResult {
	var resp = &PageResult{
		Code: 200,
		Msg:  "成功",
	}

	resp.InitProxy(resp)
	return resp

}

func NewPageResult(code int, msg string) *PageResult {
	var resp = &PageResult{
		Code: code,
		Msg:  "成功",
	}

	resp.InitProxy(resp)
	return resp
}
func NewPageResultError(msg string) *PageResult {
	var resp = &PageResult{
		Code: 500,
		Msg:  msg,
	}

	resp.InitProxy(resp)
	return resp
}
func PageResultOf(req *PageRequest) *PageResult {
	var this = NewIchubPageResult()
	this.PageSize = req.PageSize
	this.PageCurrent = req.PageCurrent
	return this
}
func (this *PageResult) CodeMsg(Code int, Msg string) *PageResult {
	this.Code = Code
	this.Msg = Msg
	return this
}
func (this *PageResult) Success() *PageResult {
	this.CodeMsg(200, "成功")
	return this
}
func (this *PageResult) OkMsg(Msg string) *PageResult {
	this.CodeMsg(200, Msg)
	return this
}
func (this *PageResult) FailMsg(Msg string) *PageResult {

	this.CodeMsg(500, Msg)
	return this
}

func (this *PageResult) To(out interface{}) {
	var res = jsonutils.ToJsonPretty(this.Data)

	jsonutils.FromJson(res, out)

}
func (this *PageResult) WriteDao(table string) {
	var Name = table + "_pageresult.json"
	gocontext.FindBeanGoRulefileCtx().WriteDaoFile(Name, this.ToPrettyString())
	ichublog.Log(this.ToPrettyString())

}
