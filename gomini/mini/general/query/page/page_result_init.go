package page

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: page_result_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNamePageResult = "page.PageResult"

// init register load
func init() {
	registerBeanPageResult()
}

// register PageResult
func registerBeanPageResult() {
	basedi.RegisterLoadBean(singleNamePageResult, LoadPageResult)
}

func FindBeanPageResult() *PageResult {
	bean, ok := basedi.FindBean(singleNamePageResult).(*PageResult)
	if !ok {
		logrus.Errorf("FindBeanPageResult: failed to cast bean to *PageResult")
		return nil
	}
	return bean

}

func LoadPageResult() baseiface.ISingleton {
	var s = NewIchubPageResult()
	InjectPageResult(s)
	return s

}

func InjectPageResult(s *PageResult) {

	// logrus.Debug("inject")
}
