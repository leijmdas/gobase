package pagedto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

/*
	@Title  文件名称: query_field_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-06 18:24:44)
	@Update 作者: leijmdas@163.com 时间(2024-05-06 18:24:44)
*/

const single_nameQueryField = "QueryField"

// init register load
func init() {
	registerBeanQueryField()
}

// register QueryField
func registerBeanQueryField() {
	basedi.RegisterLoadBean(single_nameQueryField, LoadQueryField)
}

func FindBeanQueryField() *QueryField {
	return basedi.FindBean(single_nameQueryField).(*QueryField)
}

func LoadQueryField() baseiface.ISingleton {
	var s = NewQueryFields()
	InjectQueryField(s)
	return s

}

func InjectQueryField(s *QueryField) {

	// logrus.Debug("inject")
}
