package dbmodel

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

/*
	@Title  文件名称: employee_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-06 18:24:44)
	@Update 作者: leijmdas@163.com 时间(2024-05-06 18:24:44)
*/

const single_nameEmployee = "Employee"

// init register load
func init() {
	registerBeanEmployee()
}

// register Employee
func registerBeanEmployee() {
	basedi.RegisterLoadBean(single_nameEmployee, LoadEmployee)
}

func FindBeanEmployee() *Employee {
	return basedi.FindBean(single_nameEmployee).(*Employee)
}

func LoadEmployee() baseiface.ISingleton {
	var s = NewEmployee()
	InjectEmployee(s)
	return s

}

func InjectEmployee(s *Employee) {

	// logrus.Debug("inject")
}
