package pagedb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/page"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"strings"
)

/*
@Title    文件名称: PagedbRequest.go
@Description  描述:  PagedbRequest

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
const DefaultTable = "rules" //"rules"
var DefaultFields = "rule_id,rule_key"

// 通用表查询请求数据
type PagedbRequest struct {
	page.PageRequest

	//表名
	TableName string `json:"table_name"`
	//字段列表，分隔
	FieldsName string `json:"fields_name"`
	//返回日期转为int64
	TimeToInt bool `json:"time_to_int"`

	SubTable  *SubTableDto `json:"sub_table,omitempty"`
	JoinTable *SubTableDto `json:"join_table,omitempty"`
}

func NewPagedbRequest() *PagedbRequest {
	var r = &PagedbRequest{
		TimeToInt: false,
	}

	r.InitProxy(r)
	return r
}

func NewPageDbRequest(pageSize int) *PagedbRequest {
	var r = NewPagedbRequest()
	r.PageSize = pageSize
	r.PageRequest.PageSize = pageSize
	return r
}

func (this *PagedbRequest) ValueOfPageRequest(that *PagedbRequest) *PagedbRequest {

	this.PageRequest = that.PageRequest
	this.PageRequest.InitProxy(this.PageRequest)
	this.TableName = that.TableName
	this.FieldsName = that.FieldsName
	this.TimeToInt = that.TimeToInt
	return this

}
func (this *PagedbRequest) Clear() *PagedbRequest {
	this.PageRequest.Clear()
	this.FieldsName = ""
	this.TableName = ""
	this.TimeToInt = true
	this.SubTable = nil

	this.JoinTable = nil

	return this

}
func (this *PagedbRequest) NewSubTable() *PagedbRequest {
	this.SubTable = NewSubTableDto()
	return this
}
func (this *PagedbRequest) FindTable() *gorm.DB {

	dbc := this.GetDB().Table(this.TableName)
	dbc = dbc.Select(this.FieldsName)

	dbc = this.BuildWhere(dbc)
	dbc = this.Order(dbc)
	dbc = this.SetLimit(dbc)

	return dbc
}
func (this *PagedbRequest) DefaultTableFields() *PagedbRequest {
	if len(this.TableName) == 0 {
		this.TableName = DefaultTable
		this.FieldsName = DefaultFields

	}
	return this
}
func (this *PagedbRequest) FieldNames2SnakeCase() {
	var fields = strings.Split(this.FieldsName, ",")
	var f = []string{}
	for _, v := range fields {
		v = stringutils.Camel2Case(v)
		f = append(f, strings.TrimSpace(v))

	}
	this.FieldsName = strings.Join(f, ",")

}
func (this *PagedbRequest) InitPage() {
	//ServiceTemplateCombos
	this.TableName = stringutils.Camel2Case(this.TableName)
	this.PageRequest.InitPage()
}
func (this *PagedbRequest) GeneralQueryDto() *page.PageResult {
	this.TimeToInt = true
	return this.GeneralQuery()
}
func (this *PagedbRequest) MetadataQuery() *basedto.IchubResult {
	this.DefaultTableFields()
	var factroy = metafactroy.NewMetaFactroy()
	var metadata = factroy.FindMetadata(this.TableName)
	if !metadata.TableExist {
		return basedto.NewIchubResult().FailMessage("表不存在！")
	}
	return basedto.NewIchubResult().SuccessData(metadata)

}
func (this *PagedbRequest) InitFields() *page.PageResult {
	this.DefaultTableFields()

	this.InitPage()
	var metadataFactroy = metafactroy.NewMetaFactroy()
	var meta = metadataFactroy.FindFields(this.TableName, this.FieldsName)
	if !meta.TableExist {
		return page.NewPageResultError("表不存在！")
	}
	this.FieldsName = meta.FieldsName
	this.FieldNames2SnakeCase()

	return nil
}

func (this *PagedbRequest) GeneralQuery() *page.PageResult {

	if r := this.InitFields(); r != nil {
		return r
	}

	this.WriteDao()
	count, err := this.CountTable(this.TableName)
	if err != nil {

		logrus.Error(err)
		return page.NewPageResultError(err.Error())
	}
	var pageResult = page.PageResultOf(&this.PageRequest)
	pageResult.Total = count
	if count == 0 {
		return pageResult
	}

	var ret = this.FindRecords(pageResult)
	if ret.Code != 200 {
		return ret

	}
	if this.IfSubTable() {
		this.QuerySubTable(pageResult.Data.([]map[string]any))
	}

	pageResult.WriteDao(this.TableName)
	return pageResult

}

func (this *PagedbRequest) FindRecords(result *page.PageResult) *page.PageResult {
	var db = this.FindTable()
	rows, errs := db.Rows()
	if errs != nil {
		logrus.Error(errs)
		return page.NewPageResultError(errs.Error())
	}
	defer func() {
		if err := rows.Close(); err != nil {
			logrus.Error(err)
		}
	}()

	var records = metadb.NewIchubRecords()

	var err = records.TableFields(this.TableName, this.FieldsName).ScanRows(this.TimeToInt, rows)
	if err != nil {
		logrus.Error(err)
		return page.NewPageResultError(err.Error())
	}
	result.Data = records.Records
	return result
}

func (this *PagedbRequest) WriteDao() error {
	logrus.Info("PagedbRequest GeneralQuery this =", this)
	Name := this.TableName + "_pagerequest.json"
	return gocontext.FindBeanGoRulefileCtx().WriteDaoFile(Name, jsonutils.ToJsonPretty(this.PageRequest))

}
func (this *PagedbRequest) IfSubTable() bool {
	return this.SubTable != nil
}
func (this *PagedbRequest) QuerySubTable(records []map[string]interface{}) {
	var req = NewPagedbRequest()
	req.TableName = this.SubTable.TableName
	req.PageSize = this.SubTable.PageSize
	req.FieldsName = this.SubTable.FieldsName
	req.TimeToInt = this.TimeToInt
	var key, subkey string
	for k, v := range this.SubTable.JoinKeys {
		key = k
		subkey = v
	}
	for _, v := range records {
		req.Eq(subkey, v[key])
		var data = req.GeneralQuery()
		v[this.SubTable.TableName] = data.Data
	}

}
