package pagedb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	dbmodel2 "gitee.com/leijmdas/gobase/gomini/mini/general/query/model"
	"github.com/sirupsen/logrus"
	"testing"
	"time"
)

/*
	@Title    文件名称: aggtest.go
	@Description  描述:  聚合根构建

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

//http://www.forenose.com/column/content/427118110.html
//领域事件建模

func init() {
	ichublog.InitLogrus()
	utils.FindRootDir()
}

type DeptDto map[string]any
type EmpDtos []map[string]any

type DeptEmp struct {
	Dept *DeptDto `json:"dept,omitempty"`
	Emp  EmpDtos  `json:"emp,omitempty"`
}

func NewDeptEmp() *DeptEmp {
	return &DeptEmp{}
}

func LoadDpt(id int64) *DeptDto {
	var pageDbRequest = NewPageDbRequest(1)
	pageDbRequest.TableName = "department"
	pageDbRequest.Eq("id", id)
	//	pageDbRequest.Source = "id,name,code"
	var result = pageDbRequest.GeneralQueryDto()
	var dto DeptDto = result.Data.([]map[string]any)[0]
	return &dto
}
func LoadEmp(department_id int64) EmpDtos {

	var pageDbRequest = NewPageDbRequest(10)
	pageDbRequest.TableName = "employee"
	pageDbRequest.Eq("department_id", department_id)
	//pageDbRequest.Source = "begin_date,id,code,department_id,name,gender"
	var result = pageDbRequest.GeneralQueryDto()
	logrus.Info("LoadEmp=", result.ToPrettyString())
	var emps = result.Data.([]map[string]any)
	var res = jsonutils.ToJsonPretty(emps)
	var empdtos EmpDtos
	jsonutils.FromJson(res, &empdtos)
	return empdtos
}
func Build(id int64) *DeptEmp {
	var dept = LoadDpt(id)
	var dptemp = NewDeptEmp()
	if dept != nil {
		dptemp.Dept = dept
		dptemp.Emp = LoadEmp(id)

	}
	return dptemp
}

func Test001_SelDeptEmpMap(t *testing.T) {
	logrus.Info("创建聚合根 starting...")
	var dptemp = Build(1)
	logrus.Info(jsonutils.ToJsonPretty(dptemp))
	logrus.Info("\r\n创建聚合根end")

	ichublog.Log("\r\n部门人员聚合根=\r\n", jsonutils.ToJsonPretty(dptemp))

}

type DptAgg struct {
	Dept dbmodel2.Department    `json:"dept"`
	Emp  []dbmodel2.EmployeeDto `json:"emp"`

	basedto.BaseEntity `json:"-"`
}

func NewDptAgg() *DptAgg {
	var agg = &DptAgg{
		Emp: []dbmodel2.EmployeeDto{},
	}
	agg.InitProxy(agg)
	return agg
}

func Test002_BuildDeptEmp(t *testing.T) {
	logrus.Info("创建聚合根 starting...")
	var dptemp = Build(92)
	logrus.Info("sel=", jsonutils.ToJsonPretty(dptemp))
	var agg = NewDptAgg()
	logrus.Info("\r\n创建聚合根end")
	agg.ValueFrom(dptemp)

	ichublog.Log("\r\n部门人员聚合根agg=\r\n", agg)
	logrus.Info("agg=", agg.ToPrettyString(), time.Now())
	var s = "1989-12-31T00:00:00.000Z"
	//	FormatUTCTime  = "2006-01-02T15:04:05.000Z"
	var _ = stringutils.Strtime2Int(s)

}
func Test003_QueryEmpD2D(t *testing.T) {

	var dbRequest = NewPageDbRequest(2)
	dbRequest.TableName = "employee"
	dbRequest.TimeToInt = true
	//	dbRequest.Source = "department_id"
	var result = dbRequest.GeneralQuery()
	//var emps = result.Data.([]map[string]any)
	//	var res = jsonutils.ToJsonPretty(result.Data)
	var empout []dbmodel2.EmployeeDto
	//	jsonutils.FromJson(res, &emp)
	result.To(&empout)
	ichublog.Log(jsonutils.ToJsonPretty(empout))

}
func Test004_QueryEmpD2E(t *testing.T) {

	var dbRequest = NewPageDbRequest(2)
	dbRequest.TableName = "employee"
	//	dbRequest.Source = "id,birthday,not_work_date"
	dbRequest.TimeToInt = false
	var result = dbRequest.GeneralQuery()
	var emps = result.Data.([]map[string]any)
	var res = jsonutils.ToJsonPretty(emps)
	var emp []dbmodel2.Employee = make([]dbmodel2.Employee, 2)
	jsonutils.FromJson(res, &emp)
	result.To(&emp)
	ichublog.Log("2-====", jsonutils.ToJsonPretty(emp))

}

//func Test005_QueryDeptD2E(t *testing.T) {
//
//	var dbRequest = NewPageDbRequest(2)
//	dbRequest.TableName = "department"
//	dbRequest.TimeToInt = false
//	var result = dbRequest.GeneralQuery()
//	var emps = result.Data.([]map[string]any)
//	var res = jsonutils.ToJsonPretty(emps)
//	var emp []model.Department
//	jsonutils.FromJson(res, &emp)
//	result.To(&emp)
//	ichublog.Log("2-====", jsonutils.ToJsonPretty(emp))
//
//}
