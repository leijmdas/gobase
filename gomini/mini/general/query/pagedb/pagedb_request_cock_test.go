package pagedb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/stretchr/testify/assert"
	"testing"
)

/*
	@Title    文件名称: generalquery.go
	@Description  描述:  通用查询

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

// http://www.forenose.com/column/content/427118110.html
// 领域事件建模
func init() {
	ichublog.InitLogrus()
}
func Test0001_CockCmsSitesGeneralQuery(t *testing.T) {

	var dbreq = NewPageDbRequest(2)
	dbreq.Clear()
	dbreq.TableName = "cms_sites"
	dbreq.TimeToInt = true
	var result = dbreq.GeneralQuery()

	assert.Equal(t, 200, result.Code)
	ichublog.Log(result)

}

func Test0002_CockCmsGeneralQuery(t *testing.T) {

	var pageDbRequest = NewPagedbRequest()
	pageDbRequest.PageSize = 2
	pageDbRequest.TableName = "cms_contents"
	pageDbRequest.TimeToInt = true
	pageDbRequest.Ge("id", 1)

	var result = pageDbRequest.GeneralQuery()
	ichublog.Log(result)

}

func Test0003_CockCmsGeneralQueryTableNotExists(t *testing.T) {

	var pageDbRequest = NewPagedbRequest()
	pageDbRequest.PageSize = 2
	pageDbRequest.TableName = "cms_conten1ts"
	pageDbRequest.Ge("id", 1)

	var result = pageDbRequest.GeneralQuery()
	ichublog.Log(result)

}
