package pagedb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

/*
	@Title    文件名称: generalquery.go
	@Description  描述:  通用查询

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

//http://www.forenose.com/column/content/427118110.html
//领域事件建模

func init() {
	ichublog.InitLogrus()
}
func Test001_SelectDptGeneral(t *testing.T) {

	var dbRequest = NewPageDbRequest(2)
	//	dbRequest.Clear()
	dbRequest.TableName = "department"
	var result = dbRequest.GeneralQuery()

	assert.Equal(t, 200, result.Code)
	ichublog.Log(result.ToPrettyString())

}
func Test002_SelectEmployeeGeneral(t *testing.T) {

	var dbRequest = NewPagedbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "employee"
	dbRequest.TimeToInt = true
	dbRequest.Ge("department_id", 1)

	var result = dbRequest.GeneralQuery()
	goutils.Info(result)

}

// PagedbRequest
func Test003_SelectRuleDbRequest(t *testing.T) {

	var dbRequest = NewPagedbRequest()
	dbRequest.TableName = "rules"
	//	dbRequest.Ge("rule_id", 3)
	dbRequest.OrderByAsc("rule_id")

	var result = dbRequest.GeneralQuery()
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)

}

func Test0004_GeneralQueryEmpRequest(t *testing.T) {

	var dbRequest = NewPagedbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "employee"
	dbRequest.Lt("birthday", time.Now())

	var result = dbRequest.GeneralQuery()
	logrus.Info(result.ToPrettyString())
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)
}

func Test0004_GeneralQueryDtoEmpRequest(t *testing.T) {

	var dbRequest = NewPagedbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "employee"
	dbRequest.Lt("birthday", time.Now())

	var result = dbRequest.GeneralQueryDto()
	logrus.Info(result.ToPrettyString())
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)
}

func Test0006_QueryServicePolicys(t *testing.T) {
	var dbRequest = NewPagedbRequest()
	dbRequest.PageSize = 4
	dbRequest.TableName = "service_policy_instances"
	//	dbRequest.Source = "dept_id,create_time"
	var result = dbRequest.GeneralQuery()
	logrus.Info(result.ToPrettyString())
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)
}

func Test0009_SelectEmp(t *testing.T) {
	//fields
	var dbRequest = NewPageDbRequest(1)
	dbRequest.TableName = "employee"
	dbRequest.TimeToInt = true
	var pageResult = dbRequest.GeneralQuery()

	ichublog.Log(pageResult.ToPrettyString())
}

func Test0010_SelectDptSub(t *testing.T) {

	var dbRequest = NewPageDbRequest(2)
	dbRequest.TableName = "department"
	dbRequest.Eq("id", 1)
	dbRequest.NewSubTable()
	dbRequest.SubTable.TableName = "employee"
	dbRequest.SubTable.JoinKeys["id"] = "department_id"
	dbRequest.SubTable.FieldsName = "id,name"
	var result = dbRequest.GeneralQuery()

	assert.Equal(t, 200, result.Code)
	ichublog.Log(result.ToPrettyString())
	logrus.Info(result.ToPrettyString())

}

func Test0011_QueryMetadata(t *testing.T) {
	var dbRequest = NewPagedbRequest()
	dbRequest.TableName = "service_policy_instances"
	var result = dbRequest.MetadataQuery()
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)
}
