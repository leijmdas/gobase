package pagedb

import "gitee.com/leijmdas/gobase/goconfig/common/base/basedto"

type SubTableDto struct {
	basedto.BaseEntity

	TableName  string            `json:"table_name"`
	FieldsName string            `json:"fields_name"`
	PageSize   int               `json:"page_size"`
	JoinKeys   map[string]string `json:"join_keys"`
}

func NewSubTableDto() *SubTableDto {
	var sub = &SubTableDto{
		PageSize:   1,
		FieldsName: "*",
		JoinKeys:   make(map[string]string),
	}
	sub.InitProxy(sub)
	return sub
}
