package pagedb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: sub_table_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameSubTableDto = "pagedb.SubTableDto"

// init register load
func init() {
	registerBeanSubTableDto()
}

// register SubTableDto
func registerBeanSubTableDto() {
	basedi.RegisterLoadBean(singleNameSubTableDto, LoadSubTableDto)
}

func FindBeanSubTableDto() *SubTableDto {
	bean, ok := basedi.FindBean(singleNameSubTableDto).(*SubTableDto)
	if !ok {
		logrus.Errorf("FindBeanSubTableDto: failed to cast bean to *SubTableDto")
		return nil
	}
	return bean

}

func LoadSubTableDto() baseiface.ISingleton {
	var s = NewSubTableDto()
	InjectSubTableDto(s)
	return s

}

func InjectSubTableDto(s *SubTableDto) {

	// logrus.Debug("inject")
}
