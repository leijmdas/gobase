package keyword

import "github.com/olivere/elastic/v7"

type SplitWordResponse struct {
	Words []SplitWord

	AnalyzeResp *elastic.IndicesAnalyzeResponse `json:"*_indices_analyze_response,omitempty"`
}
