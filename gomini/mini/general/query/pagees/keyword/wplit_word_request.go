package keyword

const (
	IK_SMART    = "ik_smart"
	IK_MAX_WORD = "ik_max_word"
	IK_ICHUB    = "ichub"
	//jieba
)

type SplitWordRequest struct {
	tokenizer string
	text      string
	len       int
	sort      bool
	count     int
	//use stopword
}

func (s *SplitWordRequest) Tokenizer() string {
	return s.tokenizer
}

func (s *SplitWordRequest) SetTokenizer(tokenizer string) {
	s.tokenizer = tokenizer
}

func (s *SplitWordRequest) Text() string {
	return s.text
}

func (s *SplitWordRequest) SetText(text string) {
	s.text = text
}

func (s *SplitWordRequest) Retwordlen() int {
	return s.len
}

func (s *SplitWordRequest) SetRetwordlen(retwordlen int) {
	s.len = retwordlen
}

func (s *SplitWordRequest) Sort() bool {
	return s.sort
}

func (s *SplitWordRequest) SetSort(sort bool) {
	s.sort = sort
}

func (s *SplitWordRequest) Count() int {
	return s.count
}

func (s *SplitWordRequest) SetCount(count int) {
	s.count = count
}

// stop word
func NewSplitWordRequest() *SplitWordRequest {
	return &SplitWordRequest{
		tokenizer: IK_SMART,
		len:       2,
		sort:      false,
		count:     -1,
	}
}
