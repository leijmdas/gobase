package pageesdto

import (
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/dto"
	"github.com/olivere/elastic/v7"
)

/*
@Title    文件名称: PageesQuery.go
@Description  描述: PageesQuery

@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
//https://blog.csdn.net/K346K346/article/details/120906440
//rangeQuery := elastic.NewRangeQuery("age").Gte(18).Lte(35)

type PageesQuery struct {
	pageesRequest *PageesRequest `json:"page_req"`
}

func NewPageesQuery(pageReq *PageesRequest) *PageesQuery {
	return &PageesQuery{pageesRequest: pageReq}
}
func (this *PageesQuery) BuildQuery() elastic.Query {
	var queries = []elastic.Query{}

	queries = append(queries, this.IdQuery()...)
	queries = append(queries, this.IdsQuery()...)

	queries = append(queries, this.FuzzyQuery()...)
	queries = append(queries, this.MatchQuery()...)
	queries = append(queries, this.MatchAllQuery()...)
	queries = append(queries, this.TermQuery()...)
	queries = append(queries, this.TermsQuery()...)
	queries = append(queries, this.RangeQuery()...)

	if len(queries) == 0 {
		return nil
	}
	if len(queries) == 1 { //	searchService.Query(queries[0])
		return queries[0]
	}
	boolQuery := elastic.NewBoolQuery()
	boolQuery.Must(queries...)
	return boolQuery

}

func (this *PageesQuery) IdQuery() []elastic.Query {
	var qs = []elastic.Query{}

	for _, v := range this.pageesRequest.Fields {

		if v.OpType == this.pageesRequest.FindFieldSign(dto.EsId) {
			var svalues = []string{}
			for _, vv := range v.Values {
				svalues = append(svalues, vv.(string))
			}
			qs = append(qs, elastic.NewIdsQuery(svalues...))
			return qs
		}
	}
	return qs

}
func (this *PageesQuery) IdsQuery() []elastic.Query {
	var qs = []elastic.Query{}

	for _, v := range this.pageesRequest.Fields {

		if v.OpType == this.pageesRequest.FindFieldSign(dto.EsIds) {
			var svalues = []string{}
			for _, vv := range v.Values {
				svalues = append(svalues, vv.(string))
			}
			qs = append(qs, elastic.NewIdsQuery().Ids(svalues...))
			return qs
		}
	}
	return qs

}

// match term
// keyword
func (this *PageesQuery) TermQuery() []elastic.Query {
	var qs = []elastic.Query{}

	for _, v := range this.pageesRequest.Fields {
		if v.OpType == this.pageesRequest.FindFieldSign(dto.EsTerm) {
			qs = append(qs, elastic.NewTermQuery(v.Field2Keyword(), v.Values[0].(string)))
		}
	}
	return qs
}
func (this *PageesQuery) TermsQuery() []elastic.Query {
	var qs = []elastic.Query{}
	for _, v := range this.pageesRequest.Fields {
		if v.OpType == this.pageesRequest.FindFieldSign(dto.In) {
			qs = append(qs, elastic.NewTermsQuery(v.Field2Keyword(), v.Values...))
		}
		if v.OpType == this.pageesRequest.FindFieldSign(dto.EsTerm) {
			qs = append(qs, elastic.NewTermsQuery(v.Field2Keyword(), v.Values...))
		}
	}
	return qs

}

func (this *PageesQuery) FuzzyQuery() []elastic.Query {
	var queries = []elastic.Query{}
	for _, v := range this.pageesRequest.Fields {
		if v.OpType == this.pageesRequest.FindFieldSign(dto.EsFuzzy) {
			queries = append(queries, elastic.NewFuzzyQuery(v.Field, v.Values[0].(string)))
		}
		if v.OpType == this.pageesRequest.FindFieldSign(dto.Like) {
			queries = append(queries, elastic.NewFuzzyQuery(v.Field, v.Values[0].(string)))
		}
	}
	return queries

}
func (this *PageesQuery) MatchAllQuery() []elastic.Query {
	var qs = []elastic.Query{}
	for _, v := range this.pageesRequest.Fields {

		if v.OpType == this.pageesRequest.FindFieldSign(dto.EsMatchAll) {
			qs = append(qs, elastic.NewMatchAllQuery())
			return qs
		}
	}

	return qs

}

// text
func (this *PageesQuery) MatchQuery() []elastic.Query {
	var qs = []elastic.Query{}
	for _, v := range this.pageesRequest.Fields {
		if v.OpType == this.pageesRequest.FindFieldSign(dto.Eq) {
			qs = append(qs, elastic.NewMatchQuery(v.Field, v.Values[0].(string)).Operator("and"))
		}
		if v.OpType == this.pageesRequest.FindFieldSign(dto.EsMatch) {
			qs = append(qs, elastic.NewMatchQuery(v.Field, v.Values[0].(string)).Operator("and"))
		}
	}

	return qs

}
func (this *PageesQuery) RangeQuery() []elastic.Query {
	var quries = []elastic.Query{}
	for _, v := range this.pageesRequest.Fields {
		if v.OpType == this.pageesRequest.FindFieldSign(dto.Lt) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Lt(v.Values[0]))
		}
		if v.OpType == this.pageesRequest.FindFieldSign(dto.Le) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Lte(v.Values[0]))
		}
		if v.OpType == this.pageesRequest.FindFieldSign(dto.Gt) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gt(v.Values[0]))
		}
		if v.OpType == this.pageesRequest.FindFieldSign(dto.Ge) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gte(v.Values[0]))
		}

		if v.OpType == this.pageesRequest.FindFieldSign(dto.Between) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gte(v.Values[0]).Lte(v.Values[1]))
		}
		if v.OpType == this.pageesRequest.FindFieldSign(dto.NotBetween) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gt(v.Values[1]))
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gt(v.Values[0]))

		}
	}
	return quries

}
