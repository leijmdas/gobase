package pageesdto

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"gitee.com/leijmdas/gobase/goconfig/common/goelastic"
	pagedto2 "gitee.com/leijmdas/gobase/gomini/mini/general/query/dto"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/page"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/pagees/keyword"
	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"strings"
)

/*
	@Title    文件名称: pageesrequest.go
	@Description  描述:  es请求消息

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

/*
索是 ES 最为复杂精妙的地方，这里只示例项目中较为常用的查询。

	ES 中的查询分为三大类，

一是 Term-level queries（我翻译成字段匹配），
二是 Full-text queries（全文搜索）
，三是不常用的 Specialized queries（专门查询）。各自可细分为如下几种：

	Term-level queries
	exists query 字段是否存在值
	fuzzy query 模糊查询
	ids query ID 查询
	prefix query 前缀查询
	range query 范围查询
	regexp query 正则查询
	term query 精确匹配单个字段
	terms query 精确匹配单个字段，但使用多值进行匹配，类似于 SQL 中的 in 操作
	terms_set query 字段集合查询。文档需包含字段集合中指定的最少数量字段
	wildcard query 通配符查询
	Full-text queries
	match query 单字段搜索（匹配分词结果，不需要全文匹配）
	Specialized queries
	script query 脚本查询

原文链接：https://blog.csdn.net/K346K346/article/details/120906440
*/
const (
	AggSign_COUNT = "count"
	AggSign_MAX   = "max"
	AggSign_MIN   = "min"
	AggSign_AVG   = "avg"
	AggSign_SUM   = "sum"
	AggSign_STATS = "stats"
)

type PageesRequest struct {
	basedto.BaseEntity
	page.PageRequest
	//ES INDEX名称
	IndexName string `json:"index_name"`
	//返回fields
	Source string `json:"Source"` //source

	//must filter
	AggFields *pagedto2.QueryField `json:"agg_fields,omitempty"`

	EsClient   *ichubelastic.ElasticClient `json:"-"`
	attachResp bool
}

func New() *PageesRequest {

	var req = &PageesRequest{}
	req.EsClient = gocontext.IchubClient.IniEsClient().Esclient()
	req.InitPage()
	req.InitProxy(req)

	return req
}

func NewPageesRequest() *PageesRequest {

	return New()
}
func NewEsRequest(pageSize, current int) *PageesRequest {
	var page = New()
	page.PageCurrent = current
	page.PageSize = pageSize

	return page
}
func NewPageEsRequest(indexName string) *PageesRequest {
	var page = New()
	page.IndexName = indexName

	return page
}

func (this *PageesRequest) AttachResp() bool {
	return this.attachResp
}

func (this *PageesRequest) SetAttachResp(attachResp bool) {
	this.attachResp = attachResp
}

func (this *PageesRequest) Clear() {
	this.PageRequest.Clear()
	this.AggFields = nil
}

func (this *PageesRequest) IniDefault() {

	var clientDto = baseconfig.NewElasticClientDto()
	clientDto.URL = "http://192.168.14.58:9200"
	clientDto.Username = "elastic"
	clientDto.Password = "123456"
	this.Ini(clientDto)

}
func (this *PageesRequest) ValueOfPageRequest(that *PageesRequest) *PageesRequest {

	this.PageRequest = that.PageRequest
	this.IndexName = that.IndexName
	this.AggFields = that.AggFields
	this.PageRequest.InitProxy(this.PageRequest)
	return this

}
func (this *PageesRequest) Ini(clientDto *baseconfig.ElasticClientDto) {

	this.EsClient.ClientDto = clientDto

}

func (this *PageesRequest) Open() {
	if this.EsClient.ClientDto == nil {
		this.IniDefault()
	}
	this.EsClient.Open()
}

func (this *PageesRequest) esOrderBy(searchService *elastic.SearchService) *elastic.SearchService {
	for _, v := range this.OrderBys {
		searchService.Sort(v.Field, v.Sort == "asc")

	}
	return searchService

}
func (this *PageesRequest) EsQueryResult() (*PageesResult, error) {

	var result = NewPageesResult()

	searchResult, err := this.EsQuery()

	if err != nil {
		logrus.Error(err)
		return result.FailMsg(err.Error()), err

	}
	result.PageEsResultOf(this, searchResult)
	if !this.attachResp {
		result.SearchResult = nil
	}

	logrus.Info(result)
	return result, err
}
func (this *PageesRequest) EsId(opValue any) *PageesRequest {
	//this.IchubPageRequest.QueryFields("id", dto.EsId, []any{opValues})
	return this.EsIds([]any{opValue})
}
func (this *PageesRequest) EsIds(opValues []any) *PageesRequest {
	this.PageRequest.QueryFields("ids", pagedto2.EsIds, opValues)
	return this
}

func (this *PageesRequest) EsFuzzy(field string, opValue any) *PageesRequest {

	this.PageRequest.QueryFields(field, pagedto2.EsFuzzy, []any{opValue})
	return this
}
func (this *PageesRequest) EsTerm(field string, opValue any) *PageesRequest {

	this.PageRequest.QueryFields(field, pagedto2.EsTerm, []any{opValue})
	return this
}
func (this *PageesRequest) EsMatch(field string, opValue any) *PageesRequest {

	this.PageRequest.QueryFields(field, pagedto2.EsMatch, []any{opValue})
	return this
}
func (this *PageesRequest) EsMatchAll() *PageesRequest {

	this.PageRequest.QueryFields("MatchAll", pagedto2.EsMatchAll, []any{})
	return this
}
func (this *PageesRequest) EsTerms(field string, opValues ...any) *PageesRequest {

	this.PageRequest.QueryFields(field, pagedto2.EsTerms, opValues)
	return this
}

func (this *PageesRequest) Stats(field string) *PageesRequest {
	this.AggFields.Field = field
	this.AggFields.OpType = AggSign_STATS

	return this
}
func (this *PageesRequest) Avg(field string) *PageesRequest {
	this.AggFields.Field = field
	this.AggFields.OpType = AggSign_AVG

	return this
}
func (this *PageesRequest) Max(field string) *PageesRequest {
	this.AggFields.Field = field
	this.AggFields.OpType = AggSign_MAX

	return this
}
func (this *PageesRequest) Min(field string) *PageesRequest {
	this.AggFields.Field = field
	this.AggFields.OpType = AggSign_MIN

	return this
}
func (this *PageesRequest) Sum(field string) *PageesRequest {
	this.AggFields.Field = field
	this.AggFields.OpType = AggSign_SUM

	return this
}
func (this *PageesRequest) Count(field string) *PageesRequest {
	this.AggFields.Field = field
	this.AggFields.OpType = AggSign_COUNT

	return this
}
func (this *PageesRequest) IndexAnalyze(text string) (*elastic.IndicesAnalyzeResponse, error) {
	res, err := this.Client().IndexAnalyze().Tokenizer("ik_smart").Text(text).Do(context.TODO())

	return res, err

}
func (this *PageesRequest) IndexAnalyzeWord(r *keyword.SplitWordRequest) (*elastic.IndicesAnalyzeResponse, error) {
	res, err := this.Client().IndexAnalyze().Tokenizer(r.Tokenizer()).Text(r.Text()).Do(context.TODO())

	return res, err

}
func (this *PageesRequest) IndexAnalyzeToken(token string, text string) (*elastic.IndicesAnalyzeResponse, error) {
	res, err := this.Client().IndexAnalyze().Tokenizer(token).Text(text).Do(context.TODO())

	return res, err

}

// 排序字段是keyword
func (this *PageesRequest) EsCount() (int64, error) {

	logrus.Info(this.ToPrettyString())
	countService := this.Client().Count(this.IndexName)
	var query = NewPageesQuery(this).BuildQuery()
	//	this.esOrderBy(countService)
	if query != nil {
		countService.Query(query)
	}
	count, err := countService.Do(context.Background())

	if err != nil {
		logrus.Error(err)
	}
	logrus.Info(count)
	return count, err
}
func (this *PageesRequest) BuildSource(service *elastic.SearchService) {
	if len(this.Source) > 0 {
		var fsc = elastic.NewFetchSourceContext(true).Include(strings.Split(this.Source, ",")...)
		service.FetchSourceContext(fsc)
	}
}
func (this *PageesRequest) EsQuery() (*elastic.SearchResult, error) {
	if len(this.IndexName) == 0 {
		this.IndexName = "empty"
	}
	logrus.Info(this.ToPrettyString())
	searchService := this.Client().Search().
		Index(this.IndexName). // 设置索引名 设置查询条件  设置排序字段，根据Created字段升序排序，第二个参数false表示逆序
		From(this.Start()).    // 设置分页参数 - 起始偏移量，从第0行记录开始
		Size(this.Limit()).    // 设置分页参数 - 每页大小
		Pretty(true)           // 查询结果返回可读性较好的JSON格式
	var query = NewPageesQuery(this).BuildQuery()
	if query != nil {
		searchService.Query(query)
	}
	this.esOrderBy(searchService)
	this.BuildSource(searchService)
	searchResult, err := searchService.Do(context.Background())

	if err != nil {
		logrus.Error(err)
	}
	logrus.Info(jsonutils.ToJsonPretty(searchResult))
	return searchResult, err
}

func (this *PageesRequest) EsFindId(id string) (*elastic.GetResult, error) {

	result, err := this.EsClient.Client().Get().
		Index(this.IndexName).Id(id).
		Do(context.Background())
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	logrus.Info(jsonutils.ToJsonPretty(result))
	return result, err
}

func (this *PageesRequest) EsFindIds(ids ...string) (*elastic.SearchResult, error) {
	query := elastic.NewIdsQuery().Ids(ids...)

	result, err := this.Client().Search().
		Index(this.IndexName).Query(query).
		Do(context.Background())
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	logrus.Info(jsonutils.ToJsonPretty(result))
	return result, err
}
func (this *PageesRequest) EsFindTerms(key string, values ...any) (*elastic.SearchResult, error) {
	query := elastic.NewTermsQuery(this.Field2Keyword(key), values...)

	result, err := this.EsClient.Client().Search().
		Index(this.IndexName).Query(query).From(this.Start()).Size(this.Limit()).
		Do(context.Background())
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	logrus.Info(jsonutils.ToJsonPretty(result))
	return result, err
}
func (this *PageesRequest) Field2Keyword(field string) string {
	return field + ".keyword"
}
func (this *PageesRequest) EsFindTerm(field string, value any) (*elastic.SearchResult, error) {
	query := elastic.NewTermQuery(this.Field2Keyword(field), value)

	result, err := this.EsClient.Client().Search().
		Index(this.IndexName).Query(query).From(this.Start()).Size(this.Limit()).
		Do(context.Background())
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	logrus.Info(jsonutils.ToJsonPretty(result))
	return result, err
}

func (this *PageesRequest) EsDropIndex(indexName string) {
	this.Client().DeleteIndex(indexName)
}

// EsCreateIndex
// EsDropIndex
func (this *PageesRequest) EsCreateIndex(indexName string, mapping string) (bool, error) {
	this.IndexName = indexName

	client := this.Client()
	// 执行ES请求需要提供一个上下文对象
	ctx := context.Background()
	// 首先检测下weibo索引是否存在
	exists, err := client.IndexExists(indexName).Do(ctx)

	if err != nil {
		logrus.Error(err)
		return false, err
	}
	if !exists {
		// weibo索引不存在，则创建一个
		r, err := client.CreateIndex(indexName).BodyString(mapping).Do(ctx)
		if err != nil {
			logrus.Error(err)
			return false, err
		}
		logrus.Info(r)
	}
	return true, err

}
func (this *PageesRequest) Client() *elastic.Client {
	return this.EsClient.Client()
}

func (this *PageesRequest) EsSaveIndex(id string, model any) (*elastic.IndexResponse, error) {

	client := this.Client()

	// 使用client创建一个新的文档
	indexRep, err := client.Index().
		Index(this.IndexName).   // 设置索引名称
		Id(id).                  // 设置文档id
		BodyJson(model).         // 指定前面声明的微博内容
		Do(context.Background()) // 执行请求，需要传入一个上下文对象
	if err != nil {
		logrus.Error(err)
	}
	logrus.Printf("文档Id %s, 索引名 %s\n", indexRep.Id, indexRep.Index)
	return indexRep, err
}

func (this *PageesRequest) EsDeleteIndex(id string) (*elastic.DeleteResponse, error) {
	var ctx = context.Background()
	r, err := this.Client().Delete().
		Index(this.IndexName).Id(id).Refresh("true").Do(ctx)
	if err != nil {
		logrus.Error(err)
	}
	return r, err
}
func (this *PageesRequest) EsGetMapping(indexName string) (map[string]interface{}, error) {
	return this.Client().GetMapping().Index(indexName).Type().Do(context.Background())

}
