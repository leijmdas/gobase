package pageesdto

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: pagees_request_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNamePageesRequest = "pageesdto.PageesRequest"

// init register load
func init() {
	registerBeanPageesRequest()
}

// register PageesRequest
func registerBeanPageesRequest() {
	basedi.RegisterLoadBean(singleNamePageesRequest, LoadPageesRequest)
}

func FindBeanPageesRequest() *PageesRequest {
	bean, ok := basedi.FindBean(singleNamePageesRequest).(*PageesRequest)
	if !ok {
		logrus.Errorf("FindBeanPageesRequest: failed to cast bean to *PageesRequest")
		return nil
	}
	return bean

}

func LoadPageesRequest() baseiface.ISingleton {
	var s = New()
	InjectPageesRequest(s)
	return s

}

func InjectPageesRequest(s *PageesRequest) {

	// logrus.Debug("inject")
}
