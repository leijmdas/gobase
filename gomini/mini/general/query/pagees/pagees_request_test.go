package pageesdto

import (
	"context"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/page"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/pagees/keyword"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/index"
	"gitee.com/leijmdas/gobase/gomini/mini/internal/model"

	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

//: Elasticsearch实现查询性能的方法包括：
//
//缓存：Elasticsearch可以使用缓存技术，以提高查询性能。这样，在查询时，Elasticsearch可以从缓存中获取数据，以提高查询性能。
//分页：Elasticsearch可以使用分页技术，以减少查询结果的数量，提高查询速度。这样，在查询时，Elasticsearch可以返回满足条件的文档，以提高查询性能。
//排序：Elasticsearch可以使用排序技术，以确保查询结果的有序性，提高查询准确性。这样，在查询时，Elasticsearch可以返回有序的文档，以提高查询性能。
//聚合：Elasticsearch可以使用聚合技术，以对查询结果进行分组和统计，提高查询的可读性和可用性。这样，在查询时，Elasticsearch可以返回聚合后的文档，以提高查询性能。
//

// 原文链接：https://blog.csdn.net/universsky2015/article/details/135783799
var indexName = "ichub_sys_dept"
var esRequest = New()

func init() {

	esRequest.IndexName = indexName

}

func Test0001_Query(t *testing.T) {
	// esRequest.Clear()
	esRequest.IndexName = "ichub_sys_dept"
	// esRequest.Source = "id"
	// esRequest.Eq("email", "sss@163.com")
	r, err := esRequest.EsQueryResult()
	if err != nil {
		logrus.Error(err)
	}
	ichublog.Log(jsonutils.ToJsonPretty(r))
}

func Test0002_FindById(t *testing.T) {
	esRequest.EsFindId("1")

}
func Test0003_FindByIds(t *testing.T) {
	esRequest.EsFindIds("1")

}

func Test0004_EsQuery(t *testing.T) {
	esRequest.IndexName = indexName
	query := elastic.NewRangeQuery("dept_id").Gte(553270041122963456).Lte(553270041122963456)

	var ctx = context.Background()
	res, err := esRequest.EsClient.Client().Search().Index(esRequest.IndexName).Query(query).From(0).Size(2).Pretty(true).Do(ctx)
	//	res, err := esRequest.EsClient.Client().Search().Index(esRequest.IndexName).From(0).Size(3).Pretty(true).Execute(ctx)
	//	esRequest.EsQuery()

	if err != nil {
		logrus.Error(err)
	}
	logrus.Info(jsonutils.ToJsonPretty(res))
}

func Test0005_EsQuery(t *testing.T) {
	esRequest.IndexName = "employee"
	esRequest.OrderBy("id", "asc")
	esRequest.Source = "dept_id,dept_name"
	r, err := esRequest.EsQuery()
	if err != nil {
		logrus.Error(err)
		return
	}
	ichublog.Log(jsonutils.ToJsonPretty(r))
	assert.Equal(t, 0, r.Status)

}

const mapping = `
{
  "mappings": {
    "properties": {
      "user": {
        "type": "keyword"
      },
      "message": {
        "type": "text"
      },
      "image": {
        "type": "keyword"
      },
      "created": {
        "type": "date"
      },
      "tags": {
        "type": "keyword"
      },
      "location": {
        "type": "geo_point"
      },
      "suggest_field": {
        "type": "completion"
      }
    }
  }
}`

func Test0006_CreateIndex(t *testing.T) {
	var mi = index.NewIndexMetadata()
	mi.SetIndexName("weibo2024")
	mi.AddField("id", "text")
	mi.AddField("desc", "text")
	mi.AddField("money", "long")
	logrus.Info(mi.ToMappingStr())

	b, err := esRequest.EsCreateIndex("weibo2024", mi.ToMappingStr())
	if err != nil {
		logrus.Error(err)
	}
	logrus.Info(b, err)
}

type weiBo struct {
	id    int64
	desc  string
	money int64
}

func Test0008_esSaveIndex1(t *testing.T) {
	esRequest.IndexName = "weibo2024"

	var id int64 = time.Now().Unix() //basefuncs.RuleFuncs.SnowflakeNextId()
	model := weiBo{
		id: id, desc: "olivere", money: 1}
	r, err := esRequest.EsSaveIndex(esRequest.Id2Str(id), model)
	if err != nil {
		logrus.Error(err)
	}
	ichublog.IchubLog.Println(r)
}

func Test0007_esSaveRaw(t *testing.T) {

	var id int64 = 1 // basefuncs.RuleFuncs.SnowflakeNextId()
	var sysDept = &model.SysDept{
		DeptId:     id,
		DeptName:   "olivere1111",
		Email:      "sss@163.com",
		CreateTime: time.Now(),
		Status:     "0",
		Leader:     "song",
		UpdateTime: time.Now(),
	}

	// 使用client创建一个新的文档
	indexRep, err := esRequest.Client().Index().
		Index(indexName).          // 设置索引名称
		Id(baseutils.Any2Str(id)). // 设置文档id
		BodyJson(sysDept).         // 指定前面声明的微博内容
		Do(context.Background())   // 执行请求，需要传入一个上下文对象
	if err != nil {
		logrus.Error(err)
	}
	logrus.Printf("文档Id %s, 索引名 %s\n", indexRep.Id, indexRep.Index)

}
func Test0008_esSaveIndex(t *testing.T) {
	esRequest.IndexName = indexName

	var id int64 = 1 // basefuncs.RuleFuncs.SnowflakeNextId()
	model := model.SysDept{
		DeptId: id, DeptName: "olivere", Email: "s@236.com", CreateTime: time.Now()}
	r, err := esRequest.EsSaveIndex(esRequest.Id2Str(id), model)
	if err != nil {
		logrus.Error(err)
	}
	ichublog.IchubLog.Println(r)
}

func Test0009_EsQuerysDept(t *testing.T) {
	esRequest.IndexName = indexName
	esRequest.PageSize = 2
	//esRequest.Eq("dept_id", "553251751520632832")
	//esRequest.OrderBy("dept_id", "desc")
	//	"email": "leijmdas@163.com",
	//	esRequest.Like("email", "s@236")
	//esRequest.Like("email", "s@236.com")
	//	esRequest.In("dept_id", []any{"553270041122963456"})
	//	esRequest.Between("dept_id", []any{"553270041122963456", "553270041122963456"})
	var result, _ = esRequest.EsQuery()
	ichublog.Log(result)

}
func Test0010_EsQuerysDeptResult(t *testing.T) {
	esRequest.IndexName = indexName
	esRequest.PageSize = 1
	//esRequest.Eq("dept_id", "553251751520632832")
	esRequest.OrderBy("create_time", "asc")
	esRequest.OrderBy("order_nm", "desc")
	//	"email": "leijmdas@163.com",
	//esRequest.Like("email", "s@236")
	//	esRequest.Like("email", "leijmdas@163.com")
	//	esRequest.Between("dept_id", []any{"553263647174950912", "553263647174950912"})
	result, err := esRequest.EsQueryResult()
	logrus.Error(err)
	logrus.Info(result.ToPrettyString())
	ichublog.IchubLog.Println(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)
}

//https://blog.csdn.net/K346K346/article/details/120906440

func Test0011_EsCount(t *testing.T) {
	esRequest.IndexName = indexName
	esRequest.PageSize = 2
	//esRequest.Eq("dept_id", "553251751520632832")
	esRequest.OrderBy("create_time", "asc")
	//	"email": "leijmdas@163.com",
	//	esRequest.Like("email", "s@236")
	//  esRequest.Like("email", "leijmdas@163.com")
	esRequest.Between("dept_id", []any{"553263647174950912", "553263647174950912"})
	var c, r = esRequest.EsCount()
	fmt.Println(c, r)
}

// path, _, err := client.GetMapping().Index(test.Indices...).Type(test.Types...).buildURL()
func Test0012_EsGetEmpMappiong(t *testing.T) {
	indexName = "employee"
	var do, _ = esRequest.Client().GetMapping().Index(indexName).Type().Do(context.Background())

	ichublog.Log(jsonutils.ToJsonPretty(do))

}
func Test0013_EsGetMappiong(t *testing.T) {
	var getMapping, _ = esRequest.EsGetMapping("weibo20001")
	ichublog.Log(jsonutils.ToJsonPretty(getMapping))

}

func Test0014_QueryDept(t *testing.T) {
	var pageRequest = page.NewPageRequest(30, 1)

	pageRequest.OrderBy("DeptId", "asc") //	pageRequest.InFields("ruleId", []string{"1", "2221"})
	//	pageRequest.Ge("DeptId", 1)

	var models = []model.SysDept{}
	var pageResult = pageRequest.Query(&model.SysDept{}, &models)
	logrus.Info(pageResult)
	//r, err := esRequest.EsSaveIndex(esRequest.Id2Str(id), model)
	for _, dept := range *pageResult.Data.(*[]model.SysDept) {
		var d = dept
		esRequest.EsSaveIndex(esRequest.Id2Str(d.DeptId), d)
	}
	//	logrus.Info(pageRequest)
}
func Test0015_esSaveRaw(t *testing.T) {

	var id int64 = 1 // basefuncs.RuleFuncs.SnowflakeNextId()
	var sysDept = &model.SysDept{
		DeptId:     id,
		DeptName:   "olivere",
		Email:      "song@163.com",
		CreateTime: time.Now(),
		Status:     "0",
		Leader:     "song",
		UpdateTime: time.Now(),
	}

	// 使用client创建一个新的文档
	indexRep, err := esRequest.Client().Index().
		Index(indexName).          // 设置索引名称
		Id(baseutils.Any2Str(id)). // 设置文档id
		BodyJson(sysDept).         // 指定前面声明的微博内容
		Do(context.Background())   // 执行请求，需要传入一个上下文对象
	if err != nil {
		logrus.Error(err)
	}
	logrus.Printf("文档Id %s, 索引名 %s\n", indexRep.Id, indexRep.Index)

}

func Test0016_EsQueryIds(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept1"
	esRequest.EsIds([]any{"553263647174950912", "1"})
	result, _ := esRequest.EsQueryResult()
	ichublog.Log(result)
	assert.Equal(t, 200, result.Code)

}

func Test0017_EsQueryId(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept"
	esRequest.PageSize = 2
	esRequest.PageCurrent = 1
	esRequest.EsId("553263647174950912")
	result, _ := esRequest.EsQueryResult()
	assert.Equal(t, 200, result.Code)

}
func Test0018_EsQueryTerm(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept"
	esRequest.PageSize = 2
	esRequest.PageCurrent = 1
	result, _ := esRequest.EsFindTerm("email", "leijmdas@163.com")
	//	result, _ := esRequest.EsQueryResult( )
	//assert.Equal(t, 200, result.Code)
	logrus.Info(result)
}
func Test0019_EsQueryTerms(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept"
	esRequest.PageSize = 12
	esRequest.PageCurrent = 1
	//	result, _ := esRequest.EsFindTerms("email", "leijmdas@163.com")
	esRequest.EsTerms("email", "leijmdas@163.com", "song@163.com")
	result, _ := esRequest.EsQueryResult()
	assert.Equal(t, 200, result.Code)
	logrus.Info(result)
}
func Test0020_EsQueryIds(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept"
	esRequest.PageSize = 2
	esRequest.PageCurrent = 1
	esRequest.EsIds([]any{"1"})
	//result, _ := esRequest.EsFindTerms("email", "leijmdas@163.com")
	result, _ := esRequest.EsQueryResult()
	assert.Equal(t, 200, result.Code)
	logrus.Info(result)
}

func Test0021_EsQueryTerm(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept"
	esRequest.PageSize = 2
	esRequest.PageCurrent = 1
	esRequest.EsTerm("email", "leijmdas@163.com")
	//	esRequest.OrderBy("tags", "asc")
	result, _ := esRequest.EsQueryResult()
	assert.Equal(t, 200, result.Code)

}
func Test0022_EsQueryMatchAll(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept"
	esRequest.PageSize = 2
	esRequest.PageCurrent = 1
	esRequest.EsMatchAll()
	//	esRequest.OrderBy("tags", "asc")
	result, _ := esRequest.EsQueryResult()
	assert.Equal(t, 200, result.Code)

}
func Test0023_EsQueryMatch(t *testing.T) {

	esRequest.IndexName = "ichub_sys_dept"
	esRequest.PageSize = 2
	esRequest.PageCurrent = 1
	esRequest.EsMatch("dept_name", "olivere")
	//result, _ := esRequest.EsFindTerms("email", "leijmdas@163.com")
	result, _ := esRequest.EsQueryResult()
	assert.Equal(t, 200, result.Code)
	logrus.Info(result)
}

var DbFactry = metafactroy.MetaFactroy{}

func Test0002_FindMetadataIndex(t *testing.T) {

	var table = DbFactry.FindMetadata("rules")
	logrus.Info(table, table.ToMappingStr())
	var result = NewPageesResult()
	result.FailMsg("Indexname is empty!")

	ichublog.Log(jsonutils.ToJsonPretty(result))
}

func Test0024_CreateIndexByDb(t *testing.T) {

	var table = DbFactry.FindMetadata("employee")
	logrus.Info(table, table.ToMappingStr())

	b, err := esRequest.EsCreateIndex(table.IndexName, table.ToMappingStr())
	logrus.Info(b, err)
}
func Test0025_CreateIndexMappingByDb(t *testing.T) {

	var metadataTable = DbFactry.FindMetadata("rules")
	logrus.Info(metadataTable, metadataTable.ToMappingStr())

	var mi = index.NewIndexMetadata()
	mi.SetIndexName("leijmdas")
	mi.AddField("id", "text")
	mi.AddField("desc", "text")

	//b, err := esRequest.EsCreateIndex(metadataTable.IndexName, metadataTable.ToMapping())
	ichublog.Log(jsonutils.ToJsonPretty(metadataTable.ToMapping()))
}

func Test0026_anaWord(t *testing.T) {
	res, err := esRequest.Client().IndexAnalyze().Tokenizer("ik_smart").Text("hello hi guy").Do(context.TODO())

	res, err = esRequest.IndexAnalyze("中国人民深圳解放军")
	if err != nil {
		logrus.Error(err)
	}
	logrus.Info(jsonutils.ToJsonPretty(res), err)

}

const txt = `缓存的一些好实践动静分离 缓存
对于一个缓存对象，可能分为很多种属性，这些属性中有的是静态的
，有的是动态的。在缓存的时候最好采用动静分离的方式。以免因经常
变动的数据发生更新而要把经常不变的数据也更新至缓存，成本很高。
`

func Test0027_anaWord(t *testing.T) {
	res, err := esRequest.Client().IndexAnalyze().Tokenizer("ik_smart").Text(txt).Do(context.TODO())

	if err != nil {
		logrus.Error(err)
	}

	logrus.Info(jsonutils.ToJsonPretty(res), err)
	var bytes, _ = jsonutils.ToJsonBytes(res)
	fileutils.WriteBytesFile("d:/opt/t.json", bytes)
}

func Test0028_anaWord(t *testing.T) {
	var splitWordRequest = keyword.NewSplitWordRequest()
	splitWordRequest.SetText("中国人民解放军") // txt
	splitWordRequest.SetTokenizer(keyword.IK_SMART)
	res, err := esRequest.IndexAnalyzeWord(splitWordRequest)
	if err != nil {
		logrus.Error(err)
	}
	logrus.Info(jsonutils.ToJsonPretty(res), err)

}
