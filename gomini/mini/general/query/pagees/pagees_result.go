package pageesdto

import (
	"encoding/json"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/page"
	"github.com/olivere/elastic/v7"
)

/*
@Title    文件名称: pageesresponse.go
@Description  描述:  es响应消息

@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
type PageesResult struct {
	page.PageResult
	SearchResult *elastic.SearchResult `json:"search_result,omitempty"`
}

func NewPageesResult() *PageesResult {
	var result = &PageesResult{}
	result.InitProxy(result)
	return result
}
func (this *PageesResult) Hits2Sources(hits []*elastic.SearchHit) *[]json.RawMessage {
	var rawmsg = []json.RawMessage{}
	for _, v := range hits {
		rawmsg = append(rawmsg, v.Source)
	}
	return &rawmsg
}

func (this *PageesResult) PageEsResultOf(that *PageesRequest, SearchResult *elastic.SearchResult) {
	this.PageSize = that.PageSize
	this.PageCurrent = that.PageCurrent
	this.InitPage()
	this.Success()
	this.SearchResult = SearchResult
	this.Data = this.Hits2Sources(SearchResult.Hits.Hits)
	this.Total = int(SearchResult.Hits.TotalHits.Value)
}

func (this *PageesResult) FailMsg(Msg string) *PageesResult {

	this.PageResult.FailMsg(Msg)
	return this
}
func (this *PageesResult) InitPage() {
	if this.PageSize <= baseconsts.PAGE_SIZE_ZERO {

		this.PageSize = baseconsts.PAGE_SIZE_DEFAULT

	} else if this.PageSize > baseconsts.PAGE_SIZE_MAX {

		this.PageSize = baseconsts.PAGE_SIZE_MAX

	}
	if this.PageCurrent <= 0 {
		this.PageCurrent = baseconsts.PAGE_CURRENT
	}

}
