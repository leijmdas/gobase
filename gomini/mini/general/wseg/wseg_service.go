package wseg

import (
	"bufio"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedata"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/plugins/jieba"

	"os"
	"strings"
)

type WsegService struct {
	basedto.BaseEntitySingle
	*basedata.DataPath
	WordDic      map[string]bool `json:"-"`
	dicEn, dicZh string
}

func NewWsegService() *WsegService {
	var s = &WsegService{
		DataPath: basedata.FindBeanDataPath(),
		WordDic:  make(map[string]bool),
		dicEn:    "stopwords_en.dic",
		dicZh:    "stopwords_zh.dic",
	}
	s.InitProxy(s)
	s.LoadDic()
	return s
}

func (self *WsegService) loadStopWords(filePath string) map[string]bool {
	stopWords := make(map[string]bool)
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		word := strings.TrimSpace(scanner.Text())
		stopWords[word] = true
	}
	return stopWords
}

func (self *WsegService) LoadDic() {
	var path = self.RootDir + "/config/dic/"
	self.WordDic = self.loadStopWords(path + self.dicZh)
	var wordsEn = self.loadStopWords(path + self.dicEn)

	for k, v := range wordsEn {
		self.WordDic[k] = v
	}
}

func (self *WsegService) filterWord(w string) bool {

	if _, exist := self.WordDic[w]; exist {
		return true
	}
	return false
}
func (self *WsegService) filterWords(word []string) []string {

	var ret = make([]string, 0)
	for _, i := range word {
		if !self.filterWord(i) {

			if strings.TrimSpace(i) != "" && len(i) > 3 {
				ret = append(ret, i)
			}
		}
	}

	return ret
}

func (self *WsegService) Analyze(text string) ([]string, error) {

	got, err := jieba.Analyze(text, 2)
	if err != nil {
		golog.Error("WsegService [Analyze] err:", err)

	}
	var ret = self.filterWords(got)
	return ret, err
}
