package wseg

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"git.ichub.com/general/webcli120/goconfig/ichublog/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: wseg_service_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2025-02-22 09:00:15
	@Update  作者:    leijmdas@163.com  时间: 2025-02-22 09:00:15
*/

var singleNameWsegService = "*wseg.WsegService-e1aef9e2-4020-4ee1-b525-26eb7c2ab609"

// init register load
func init() {
	registerBeanWsegService()
}

// register WsegService
func registerBeanWsegService() {
	err := basedi.RegisterLoadBean(singleNameWsegService, LoadWsegService)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanWsegService
func FindBeanWsegService() *WsegService {

	if bean, ok := basedi.FindBeanOk(singleNameWsegService); ok {
		return bean.(*WsegService)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadWsegService() baseiface.ISingleton {
	var inst = NewWsegService()
	InjectWsegService(inst)
	return inst

}

func InjectWsegService(s *WsegService) {

}
