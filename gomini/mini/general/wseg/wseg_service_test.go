package wseg

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"testing"
)

func Test001_ik(t *testing.T) {

	var text = "在遥远的山谷里，住着一群快乐的小动物，它们每晚都会聚集在村口的大树下，仰望那轮明亮的习亮，讲述着各自的故事。其中，有一只名叫灵灵的小狐狸，它对月亮有着特别的情感"
	text = `"用户问的是Elasticsearch的IK分词器是否支持停用词。我需要先确认IK分词器的功能，特别是关于停用词的部分。根据搜索结果，看到多个结果提到了IK分词器的停用词配置。比如搜索结果[3]提到可以通过修改IKAnalyzer.cfg.xml 文件来添加停用词词典，而搜索结果[5]和[7]也提到了类似的方法，说明IK确实支持停用词。"`
	got, err := FindBeanWsegService().Analyze(text)
	golog.Error(got, err)
}
func Test002_ik(t *testing.T) {

	var text = "在遥远的山谷里，住着一群快乐的小动物，它们每晚都会聚集在村口的大树下，仰望那轮明亮的习亮，讲述着各自的故事。其中，有一只名叫灵灵的小狐狸，它对月亮有着特别的情感"
	text = `"用户问的是Elasticsearch的IK分词器是否支持停用词。我需要先确认IK分词器的功能，特别是关于停用词的部分。根据搜索结果，看到多个结果提到了IK分词器的停用词配置。比如搜索结果[3]提到可以通过修改IKAnalyzer.cfg.xml 文件来添加停用词词典，而搜索结果[5]和[7]也提到了类似的方法，说明IK确实支持停用词。"`
	text = `I have a very good dream,you can see it.`
	got, err := FindBeanWsegService().Analyze(text)
	golog.Error(got, err)

}

func Test003_ik(t *testing.T) {

	var text = `"用户问的是Elasticsearch的IK分词器是否支持停用词。我需要先确认IK分词器的功能，特别是关于停用词的部分。根据搜索结果，看到多个结果提到了IK分词器的停用词配置。比如搜索结果[3]提到可以通过修改IKAnalyzer.cfg.xml 文件来添加停用词词典，而搜索结果[5]和[7]也提到了类似的方法，说明IK确实支持停用词。"`
	var ret, err = FindBeanWsegService().Analyze(text)
	golog.Info(ret, err)
}
