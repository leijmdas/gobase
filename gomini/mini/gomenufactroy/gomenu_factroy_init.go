package gomenufactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gomenu_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameGomenuFactroy = "gomenufactroy.GomenuFactroy"

// init register load
func init() {
	registerBeanGomenuFactroy()
}

// register GomenuFactroy
func registerBeanGomenuFactroy() {
	basedi.RegisterLoadBean(singleNameGomenuFactroy, LoadGomenuFactroy)
}

func FindBeanGomenuFactroy() *GomenuFactroy {
	bean, ok := basedi.FindBean(singleNameGomenuFactroy).(*GomenuFactroy)
	if !ok {
		logrus.Errorf("FindBeanGomenuFactroy: failed to cast bean to *GomenuFactroy")
		return nil
	}
	return bean

}

func LoadGomenuFactroy() baseiface.ISingleton {
	var s = NewGomenuFactroy()
	InjectGomenuFactroy(s)
	return s

}

func InjectGomenuFactroy(s *GomenuFactroy) {

	// logrus.Debug("inject")
}
