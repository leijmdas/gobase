package db

import (
	"gitee.com/leijmdas/gobase/gomini/mini/file/minilog"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"
)

func FindMetaTable(tablename string) *metadb.MetadataTable {
	var meta = metafactroy.FindBeanMetaFactroy().FindMetadata(tablename)
	minilog.Info(meta)
	return meta
}
func FindTables() []metadb.MetadataTable {
	var meta = metafactroy.FindBeanMetaFactroy().FindTables()
	minilog.Info(meta)
	return meta
}
