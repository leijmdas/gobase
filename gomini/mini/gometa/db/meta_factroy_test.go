package db

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadict"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"
	"github.com/sirupsen/logrus"
	"strings"
	"testing"
)

func init() {
	ichublog.InitLogrus()
	fileutils.FindRootDir()
}

func Test0001_FindMetadata(t *testing.T) {

	var metadataTable = metafactroy.FindBeanMetaFactroy().FindMetadata("employee")
	logrus.Info(metadataTable)
}

func Test0002_FindMetadataIndex(t *testing.T) {

	var table = metafactroy.FindBeanMetaFactroy().FindMetadata("employee")
	logrus.Info(table, table.ToMappingStr())
}

func Test0003_FindMetaMake(t *testing.T) {

	var metadataTable = FindMetaTable("employee")
	logrus.Info(metadataTable)
}
func Test0004_AlterTableComment(t *testing.T) {

	var mf = metafactroy.FindBeanMetaFactroy().IniDb()
	var err = mf.AlterTableComment("am_api", "API接口表")
	logrus.Error(err)
}

func Test0005_AlterTableColComment(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()
	var md = metadict.InstMetaFactroy.FindMetadata("am_api")
	var err = metadict.InstMetaFactroy.AlterMetaTable(md)
	logrus.Error(err)
}
func Test0006_AlterTableColComment(t *testing.T) {

	var md = metadict.InstMetaFactroy.FindMetadata("am_api")
	md.TableComment = "AM接口表1"
	metadict.InstMetaFactroy.AlterMetaTable(md)
	var mc = md.FindMetaColumn("API_NAME")
	mc.ColumnComment = "API接口名称1"
	var err = metadict.InstMetaFactroy.AlterColumnComment(md, mc)
	logrus.Error(err)
}

func Test0007_AlterTableColComment(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var tbls = metadict.InstMetaFactroy.FindTables()
	var s = make([]string, 0)
	for _, t := range tbls {
		s = append(s, t.ToText())
	}
	goutils.Info(strings.Join(s, "\r\n"))

}

func Test0008_AlterTableColComment(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var tbls = metadict.InstMetaFactroy.FindTables()
	var s []string = make([]string, 0)
	for _, t := range tbls {
		s = append(s, t.ToText())
	}
	goutils.Info(strings.Join(s, "\r\n"))

}

func Test014_parsecolshare(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var colparse = metadict.NewMetacolChina()
	var _ = colparse.Parse()
}

func Test015_AlterTableColMakeAll(t *testing.T) {
	metadict.FindBeanMetacolChina().MakeColAll()

}
func Test016_ParseCol6(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var colparse = metadict.NewMetacolChina()
	colparse.ParseSix()
}

func Test016_maketables(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var colparse = metadict.NewMetacolChina()
	colparse.MakeTables()
}

func Test017_writetables(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var colparse = metadict.NewMetacolChina()
	colparse.WriteTables()
}
func Test018_maketablesshare(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var colparse = metadict.FindBeanMetacolChinaShare()
	colparse.MakeTables()
}

func Test019_writetablesshare(t *testing.T) {
	metadict.InstMetaFactroy.IniDb()

	var colparse = metadict.FindBeanMetacolChinaShare()
	colparse.WriteTables()
}
