package metacontext

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
)

/*
@Title    文件名称: metadata_context.go
@Description  描述: 元数据--字典

@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/

type MetadataContext struct {
	basedto.BaseEntitySingle

	DbType       string `json:"db_type"`
	mysqlCtxt    *MetadataMysql
	postgresCtxt *MetadataPostgres
}

func NewMetadataContext() *MetadataContext {
	var ctxt = &MetadataContext{
		mysqlCtxt:    NewMetadataMysql(),
		postgresCtxt: NewMetadataPostgres(),
	}
	return ctxt

}

func (this *MetadataContext) Mysql() bool {
	return this.DbType == baseconsts.DB_TYPE_MYSQL

}

func (this *MetadataContext) FindProtoType(fieldType string) (goType string) {
	if this.Mysql() {
		return this.mysqlCtxt.FindProtoType(fieldType)
	}
	return this.postgresCtxt.FindProtoType(fieldType)
}
func (this *MetadataContext) FindEsType(fieldType string) (goType string) {
	if this.Mysql() {
		return this.mysqlCtxt.FindEsType(fieldType)
	}
	return this.postgresCtxt.FindEsType(fieldType)
}

func (this *MetadataContext) FindGoType(fieldType string) (pbType string) {
	if this.Mysql() {
		return this.mysqlCtxt.FindGoType(fieldType)
	}

	return this.postgresCtxt.FindGoType(fieldType)
}

func (this *MetadataContext) cloneTags(tags map[string]string) map[string]string {
	cloneTags := make(map[string]string)
	for k, v := range tags {
		cloneTags[k] = v
	}
	return cloneTags
}
