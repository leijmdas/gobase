package metacontext

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metadata_context_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMetadataContext = "metacontext.MetadataContext"

// init register load
func init() {
	registerBeanMetadataContext()
}

// register MetadataContext
func registerBeanMetadataContext() {
	basedi.RegisterLoadBean(singleNameMetadataContext, LoadMetadataContext)
}

func FindBeanMetadataContext() *MetadataContext {
	bean, ok := basedi.FindBean(singleNameMetadataContext).(*MetadataContext)
	if !ok {
		logrus.Errorf("FindBeanMetadataContext: failed to cast bean to *MetadataContext")
		return nil
	}
	return bean

}

func LoadMetadataContext() baseiface.ISingleton {
	var s = NewMetadataContext()
	InjectMetadataContext(s)
	return s

}

func InjectMetadataContext(s *MetadataContext) {

	// logrus.Debug("inject")
}
