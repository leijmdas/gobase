package metacontext

/*
	@Title    文件名称: MetadataMysql.go
	@Description  描述: 元数据--字典

	@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/

var mysqlDb2GoMap = map[string]string{
	"char":    "string",
	"varchar": "string",
	"text":    "string",
	"bit":     "byte",
	"tinyint": "int8",

	"smallint":    "int16",
	"mediumint":   "int32",
	"int":         "int32",
	"integer":     "int32",
	"bigint":      "int64",
	"double":      "float64",
	"decimal":     "float64",
	"numeric":     "float64",
	"int32":       "int32",
	"int64":       "int64",
	"string":      "string",
	"time":        "string",
	"date":        "time.Time",
	"datetime":    "time.Time",
	"timestamp":   "time.Time",
	"timestamptz": "time.Time",
	"uuid":        "string",
	"bool":        "bool",
	"boolean":     "bool",
	"serial2":     "int16",
	"serial4":     "int32",
	"serial8":     "int64",
	"money":       "float64",
	"int2":        "int16",
	"int4":        "int32",
	"int8":        "int64",
	"float4":      "float32",
	"float8":      "float64",
}
var mysqlDb2ProtoMap = map[string]string{

	"char":     "string",
	"varchar":  "string",
	"bit":      "string",
	"tinyint":  "int32",
	"smallint": "int32",
	"int":      "int32",
	"bigint":   "string",
	"double":   "double",
	"decimal":  "double",
	"numeric":  "double",

	"time":        "string",
	"date":        "int64",
	"datetime":    "int64",
	"timestamp":   "int64",
	"timestamptz": "int64",
	"uuid":        "string",
	"boolean":     "bool",
	"serial2":     "int32",
	"serial4":     "int32",
	"serial8":     "string",
	"money":       "double",
	"bool":        "string",

	"int2":      "int16",
	"int4":      "int32",
	"int8":      "string",
	"float4":    "float",
	"float8":    "double",
	"time.Time": "string",
}
var mysqlDb2EsMap = map[string]string{
	"char":    "text",
	"varchar": "text",
	"text":    "text",
	"bit":     "byte",
	"tinyint": "short",

	"smallint":    "short",
	"mediumint":   "integer",
	"int":         "integer",
	"integer":     "integer",
	"bigint":      "long",
	"double":      "double",
	"decimal":     "text",
	"numeric":     "text",
	"int32":       "integer",
	"int64":       "long",
	"string":      "text",
	"time":        "text",
	"date":        "date",
	"datetime":    "date",
	"timestamp":   "date",
	"timestamptz": "date",
	"uuid":        "keyword",
	"bool":        "boolean",
	"boolean":     "boolean",
	"serial2":     "short",
	"serial4":     "integer",
	"serial8":     "long",
	"money":       "double",
	"int2":        "short",
	"int4":        "integer",
	"int8":        "long",
	"float4":      "float",
	"float8":      "double",
}

//es字段类型总结（https://www.elastic.co/guide/en/elasticsearch/reference/7.2/mapping-types.html）：
//
//1）核心数据类型：
//string字符串：
//text：文本类型（分词）；
//keyword：关键字类型（不分词）；
//numeric-数值类型：
//long, integer, short, byte, double, float, half_float, scaled_float
//date-日期类型（存储自unix纪元以来的毫秒数）；
//date_nanos：日期纳秒类型（存储自unix纪元以来的纳秒数）；
//boolean-布尔类型；
//binary-二进制类型；
//range-范围类型:
//integer_range, float_range, long_range, double_range, date_range
//2）复杂数据类型
//object：对象类型；存储单个json对象；
//nested：嵌套类型；存储json对象数组；
//3）数组类型（字段数组类型）
//数组不需要专用的数据类型。一个字段默认可以包含0个或多个值。然而，数组中的所有值必须是同一种类型；
//4）多字段类型：
//根据不同目的以不同方式索引同一字段是有帮助的；
//举例，字符串字段可以被映射为 text字段以便全文搜索，也可以映射为keword以便于排序或聚合。
//5）其他数据类型
//如地理位置信息数据类型，ip数据类型等（没有罗列完全）；

// 原文链接：https://blog.csdn.net/PacosonSWJTU/article/details/127200021
type MetadataMysql struct {
	Db2GoMap    map[string]string `json:"db2gomap"`    /*创建集合 */
	Db2ProtoMap map[string]string `json:"db2protomap"` /*创建集合 */
	Db2EsMap    map[string]string `json:"-"`
}

func NewMetadataMysql() *MetadataMysql {
	var ctxt = &MetadataMysql{
		Db2GoMap:    mysqlDb2GoMap,
		Db2ProtoMap: mysqlDb2ProtoMap,
		Db2EsMap:    mysqlDb2EsMap,
	}

	return ctxt

}

func (this *MetadataMysql) FindEsType(fieldType string) (esType string) {

	esType = this.Db2EsMap[fieldType]
	if esType == "" {
		esType = "string"
	}
	return esType
}

func (this *MetadataMysql) FindGoType(fieldType string) (goType string) {

	goType = this.Db2GoMap[fieldType]
	if goType == "" {
		goType = "string"
	}
	return goType
}

func (this *MetadataMysql) FindProtoType(fieldType string) (pbType string) {

	pbType = this.Db2ProtoMap[fieldType]
	if pbType == "" {
		pbType = "string"
	}
	return pbType
}
