package metacontext

/*
	@Title    文件名称: MetadataPostgres.go
	@Description  描述: 元数据--字典

	@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/

type MetadataPostgres struct {
	DbType2GoMap    map[string]string `json:"dbtype_2_gomap"`    /*创建集合 */
	DbType2ProtoMap map[string]string `json:"dbtype_2_protomap"` /*创建集合 */
	Db2EsMap        map[string]string `json:"db_2_esmap"`
}

func NewMetadataPostgres() *MetadataPostgres {
	var ctxt = &MetadataPostgres{
		DbType2GoMap:    postgresDb2GoMap,
		DbType2ProtoMap: postgresDb2ProtoMap,
		Db2EsMap:        postgresDb2EsMap,
	}
	return ctxt

}

func (this *MetadataPostgres) FindGoType(fieldType string) (goType string) {

	goType = this.DbType2GoMap[fieldType]
	if goType == "" {
		goType = "string"
	}
	return goType
}
func (this *MetadataPostgres) FindEsType(fieldType string) (esType string) {

	esType = this.Db2EsMap[fieldType]
	if esType == "" {
		esType = "string"
	}
	return esType
}

func (this *MetadataPostgres) FindProtoType(fieldType string) (pbType string) {

	pbType = this.DbType2ProtoMap[fieldType]
	if pbType == "" {
		pbType = "string"
	}
	return pbType
}

var postgresDb2GoMap = map[string]string{
	"char":    "string",
	"varchar": "string",
	"text":    "string",
	"bit":     "byte",
	"tinyint": "int8",

	"smallint":  "int16",
	"mediumint": "int32",
	"int":       "int32",
	"integer":   "int32",
	"bigint":    "int64",

	"double":      "float64",
	"decimal":     "float64",
	"numeric":     "float64",
	"int32":       "int32",
	"int64":       "int64",
	"string":      "string",
	"time":        "string",
	"date":        "time.Time",
	"datetime":    "time.Time",
	"timestamp":   "time.Time",
	"timestamptz": "time.Time",
	"uuid":        "string",
	"bool":        "bool",
	"boolean":     "bool",
	"serial2":     "int16",
	"serial4":     "int32",
	"serial8":     "int64",
	"money":       "float64",
	"int2":        "int16",
	"int4":        "int32",
	"int8":        "int64",
	"float4":      "float32",
	"float8":      "float64",
}

var postgresDb2ProtoMap = map[string]string{

	"char":     "string",
	"varchar":  "string",
	"bit":      "string",
	"tinyint":  "int32",
	"smallint": "int32",

	"int":     "int32",
	"bigint":  "string",
	"double":  "double",
	"decimal": "double",
	"numeric": "double",

	"time":        "string",
	"date":        "int64",
	"datetime":    "int64",
	"timestamp":   "int64",
	"timestamptz": "int64",

	"uuid":    "string",
	"boolean": "bool",
	"serial2": "int32",
	"serial4": "int32",
	"serial8": "string",
	"money":   "double",
	"bool":    "string",

	"int2":      "int16",
	"int4":      "int32",
	"int8":      "string",
	"float4":    "float",
	"float8":    "double",
	"time.Time": "string",
}

var postgresDb2EsMap = map[string]string{
	"char":    "text",
	"varchar": "text",
	"text":    "text",
	"bit":     "byte",
	"tinyint": "short",

	"smallint":    "short",
	"mediumint":   "integer",
	"int":         "integer",
	"integer":     "integer",
	"bigint":      "long",
	"double":      "double",
	"decimal":     "text",
	"numeric":     "text",
	"int32":       "integer",
	"int64":       "long",
	"string":      "text",
	"time":        "text",
	"date":        "date",
	"datetime":    "date",
	"timestamp":   "date",
	"timestamptz": "date",
	"uuid":        "keyword",
	"bool":        "boolean",
	"boolean":     "boolean",
	"serial2":     "short",
	"serial4":     "integer",
	"serial8":     "long",
	"money":       "double",
	"int2":        "short",
	"int4":        "integer",
	"int8":        "long",
	"float4":      "float",
	"float8":      "double",
}
