package metadb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/gocache"
	"time"
)

var InstMetadataCache = NewMetadataCache()

type MetadataCache struct {
	cache *gocache.IchubCache
}

func NewMetadataCache() *MetadataCache {
	return &MetadataCache{
		cache: gocache.NewIchubCache("RuleEngine:"),
	}
}

func (this *MetadataCache) CacheGet(tableName string) (*MetadataTable, bool) {
	v, found := this.cache.Get("Table:" + tableName)
	if found {
		return v.(*MetadataTable), found
	}
	return nil, false
}

func (this *MetadataCache) CacheSet(tableName string, table *MetadataTable) {
	this.cache.Set("Table:"+tableName, table, 10*time.Minute)

}
