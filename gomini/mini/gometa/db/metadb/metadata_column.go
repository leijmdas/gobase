package metadb

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metacontext"
	jsoniter "github.com/json-iterator/go"
	"strings"
)

/*
@Title    文件名称: coulmns.go
@Description  描述: 元数据--表字段

@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/
type MetadataColumn struct {
	basedto.BaseEntity
	TableName     string `json:"table_name",gorm:"column:table_name"`
	TableSchema   string `json:"table_schema",gorm:"column:table_schema"`
	ColumnName    string `json:"column_name",gorm:"column:column_name"`
	DataType      string `json:"data_type,"gorm:"column:data_type"`
	ColumnType    string `json:"column_type",gorm:"column:column_type"`
	ColumnKey     string `json:"column_key",gorm:"column:column_key"`
	CharMaxLen    string `json:"char_max_len",gorm:"column:char_max_len"`
	ColumnComment string `json:"column_comment",gorm:"column:column_comment"`
	ColumnDefault string `json:"column_default",gorm:"column:column_default"`
}

func NewMetadataColumns() *MetadataColumn {
	var cs = &MetadataColumn{}
	cs.InitProxy(cs)
	return cs
}

func (metaCol *MetadataColumn) String() string {
	s, _ := jsoniter.Marshal(metaCol)
	return string(s)
}
func (metaCol *MetadataColumn) ToString() string {
	s, _ := jsoniter.MarshalIndent(metaCol, "", "    ")
	return string(s)
}
func (metaCol *MetadataColumn) IfString() bool {
	return metaCol.FindGoType(metaCol.DataType) == "string"
}

func (metaCol *MetadataColumn) ToText() string {
	var colvalue = strings.ReplaceAll(metaCol.ColumnName, "_", " ")
	return metaCol.ColumnName + "=" + strings.ToLower(colvalue)

}

func (metaCol *MetadataColumn) ReturnValue() (ReturnValue string) {
	ReturnValue = ""
	if metaCol.IfInt() || metaCol.IfNumeric() {
		ReturnValue = "return 0"
	} else if metaCol.IfBool() || metaCol.IfBitField() {
		ReturnValue = "return false"
	} else if metaCol.IfString() {
		ReturnValue = `return ""`
	} else if metaCol.IfTime() {
		ReturnValue = `return time.Time{}`
	} else if metaCol.IfLocalTimeInt() {
		ReturnValue = `return basemodel.LocalTimeInt{}`
	} else if metaCol.IfLocalTimeUTCInt() {
		ReturnValue = `return basemodel.LocalTimeUTCInt{}`
	} else if metaCol.IfLocalDateInt() {
		ReturnValue = `return basemodel.LocalDateInt{}`
	} else if metaCol.IfLocalTime() {
		ReturnValue = `return basemodel.LocalTime {}`
	} else if metaCol.IfLocalDate() {
		ReturnValue = `return basemodel.LocalDate {}`
	}
	return
}

func (metaCol *MetadataColumn) IfBool() bool {
	return metaCol.FindGoType(metaCol.DataType) == "bool"
}

func (metaCol *MetadataColumn) IfBitField() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "BitField")
}

func (metaCol *MetadataColumn) IfInt() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "int")
}
func (metaCol *MetadataColumn) IfInt64() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "int64")
}
func (metaCol *MetadataColumn) IfNumeric() bool {
	return metaCol.IfInt() || strings.Contains(metaCol.FindGoType(metaCol.DataType), "decimal") || strings.Contains(metaCol.FindGoType(metaCol.DataType), "numeric") || strings.Contains(metaCol.FindGoType(metaCol.DataType), "float")

}

func (metaCol *MetadataColumn) IfTime() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "time.Time")
}

func (metaCol *MetadataColumn) IfLocalTimeInt() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "basemodel.LocalTimeInt")
}
func (metaCol *MetadataColumn) IfLocalTimeUTCInt() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "basemodel.LocalTimeUTCInt")
}

func (metaCol *MetadataColumn) IfLocalDateInt() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "basemodel.LocalDateInt")
}

func (metaCol *MetadataColumn) IfLocalTime() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "basemodel.LocalTime")
}

func (metaCol *MetadataColumn) IfLocalDate() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "basemodel.LocalDate")
}

func (metaCol *MetadataColumn) IfDateTime() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "time")
}

func (metaCol *MetadataColumn) IfDate() bool {
	return strings.Contains(metaCol.FindGoType(metaCol.DataType), "date")
}

func (metaCol *MetadataColumn) FindGoType(fieldType string) (goType string) {

	return metacontext.FindBeanMetadataContext().FindGoType(fieldType)
}
func (metaCol *MetadataColumn) FindColEsType() (goType string) {

	return metacontext.FindBeanMetadataContext().FindEsType(metaCol.DataType)
}
func (metaCol *MetadataColumn) FindColGoType() (goType string) {

	return metacontext.FindBeanMetadataContext().FindGoType(metaCol.DataType)
}

// `gorm:"column:%s"
func (metaCol *MetadataColumn) ToGormTag(pkey string) string {

	colDef := metaCol.ColumnName + ";type:" + metaCol.ColumnType
	if metaCol.ColumnName == pkey {
		colDef = colDef + ";PRIMARY_KEY"
	}
	if metaCol.IfString() {
		if metaCol.ColumnName == pkey {
			colDef = colDef + ";PRIMARY_KEY"
		}
	} else {
		if metaCol.ColumnName == pkey {
			colDef = metaCol.ColumnName + ";AUTO_INCREMENT"
		}
	}
	//if this.IsMysql() {
	colDef = colDef + fmt.Sprintf(";comment:'%s'", metaCol.ColumnComment)
	//}
	if len(metaCol.ColumnDefault) > 0 {
		if metaCol.IfNumeric() {
			colDef = colDef + fmt.Sprintf(";default:%s", metaCol.ColumnDefault)
		}
		if metaCol.IfString() {
			colDef = colDef + fmt.Sprintf(";default:\\'%s\\'", metaCol.ColumnDefault)
		}
	}
	return `gorm:"column:` + colDef + `"`
}
