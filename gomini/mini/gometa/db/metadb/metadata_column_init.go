package metadb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metadata_column_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMetadataColumn = "metadb.MetadataColumn"

// init register load
func init() {
	registerBeanMetadataColumn()
}

// register MetadataColumn
func registerBeanMetadataColumn() {
	basedi.RegisterLoadBean(singleNameMetadataColumn, LoadMetadataColumn)
}

func FindBeanMetadataColumn() *MetadataColumn {
	bean, ok := basedi.FindBean(singleNameMetadataColumn).(*MetadataColumn)
	if !ok {
		logrus.Errorf("FindBeanMetadataColumn: failed to cast bean to *MetadataColumn")
		return nil
	}
	return bean

}

func LoadMetadataColumn() baseiface.ISingleton {
	var s = NewMetadataColumns()
	InjectMetadataColumn(s)
	return s

}

func InjectMetadataColumn(s *MetadataColumn) {

	// logrus.Debug("inject")
}
