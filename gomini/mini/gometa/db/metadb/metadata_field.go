package metadb

import (
	"database/sql"
	"strings"
)

/*
@Title    文件名称: ichubField.go
@Description  描述: 元数据--FIELD

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
type MetadataField struct {
	Field  string      `json:"field"`
	GoType string      `json:"goType"`
	Value  interface{} `json:"value"`
}

func NewIchubField(field string, gotype string, value interface{}) *MetadataField {
	return &MetadataField{Field: field, GoType: gotype, Value: value}
}

func FindGoType(table, field string) string {
	field = strings.TrimSpace(field)

	var metatable, _ = InstMetadataCache.CacheGet(table) //var metatable = FindMapTableGoDict(table)

	var goType = "string"
	if metatable != nil {
		goType = metatable.FindGoType(field)
	}
	return goType
}

func MakeIchubField(table, field string) *MetadataField {
	field = strings.TrimSpace(field)

	var goType = FindGoType(table, field)
	field = strings.TrimSpace(field)

	if goType == "bool" {
		return NewIchubField(field, goType, *new(sql.NullBool))
	}
	if goType == "string" {
		return NewIchubField(field, goType, *new(sql.NullString))
	}
	if goType == "uint8" {
		return NewIchubField(field, goType, *new(sql.NullByte))
	}
	if goType == "[]uint8" {
		return NewIchubField(field, goType, *new([]int8))
	}
	if goType == "uint16" {
		return NewIchubField(field, goType, *new(uint16))
	}
	if goType == "uint32" {
		return NewIchubField(field, goType, *new(uint32))
	}
	if goType == "uint64" {
		return NewIchubField(field, goType, *new(uint64))
	}
	if goType == "int" {
		return NewIchubField(field, goType, *new(sql.NullInt64))
	}
	if goType == "int8" {
		return NewIchubField(field, goType, *new(int8))
	}
	if goType == "int16" {
		return NewIchubField(field, goType, *new(sql.NullInt16))
	}
	if goType == "int32" {
		return NewIchubField(field, goType, *new(sql.NullInt32))
	}
	if goType == "int64" {
		return NewIchubField(field, goType, *new(sql.NullInt64))
	}
	if goType == "float32" {
		return NewIchubField(field, goType, *new(float32))
	}
	if goType == "float64" {
		return NewIchubField(field, goType, *new(sql.NullFloat64))
	}

	if goType == "time.Time" {
		return NewIchubField(field, goType, *new(sql.NullTime))
	}

	return NewIchubField(field, goType, *new(string))
}

func (this *MetadataField) IfFloat() bool {
	return this.GoType == "float32" || this.GoType == "float64"
}
func (this *MetadataField) IfInt() bool {
	return this.GoType == "int32" || this.GoType == "int64" || this.GoType == "int16"
}
func (this *MetadataField) IfUInt() bool {
	return this.GoType == "uint32" || this.GoType == "uint64" || this.GoType == "uint16"
}
