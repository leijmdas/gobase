package metadb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metadata_gofield_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMetadataGofield = "metadb.MetadataGofield"

// init register load
func init() {
	registerBeanMetadataGofield()
}

// register MetadataGofield
func registerBeanMetadataGofield() {
	basedi.RegisterLoadBean(singleNameMetadataGofield, LoadMetadataGofield)
}

func FindBeanMetadataGofield() *MetadataGofield {
	bean, ok := basedi.FindBean(singleNameMetadataGofield).(*MetadataGofield)
	if !ok {
		logrus.Errorf("FindBeanMetadataGofield: failed to cast bean to *MetadataGofield")
		return nil
	}
	return bean

}

func LoadMetadataGofield() baseiface.ISingleton {
	var s = NewMetadatGofield()
	InjectMetadataGofield(s)
	return s

}

func InjectMetadataGofield(s *MetadataGofield) {

	// logrus.Debug("inject")
}
