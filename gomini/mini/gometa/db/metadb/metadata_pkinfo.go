package metadb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	jsoniter "github.com/json-iterator/go"
)

/*
   @Title    文件名称: metadata_pkinfo.go
   @Description  描述: 元数据--主键

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
//for postgres
type MetadataPkinfo struct {
	basedto.BaseEntity

	PkName   string `json:"pk_name" gorm:"column:pkname"`
	ColName  string `json:"col_name" gorm:"column:colname"`
	TypeName string `json:"type_name" gorm:"column:typename"`
}

func NewMetadataPkinfo() *MetadataPkinfo {
	return &MetadataPkinfo{}
}

func (pkInfo *MetadataPkinfo) String() string {
	s, _ := jsoniter.Marshal(pkInfo)
	return string(s)
}

func (pkInfo *MetadataPkinfo) ToString() string {
	s, _ := jsoniter.MarshalIndent(pkInfo, "", "    ")
	return string(s)
}
