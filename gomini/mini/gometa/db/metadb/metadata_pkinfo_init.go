package metadb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metadata_pkinfo_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMetadataPkinfo = "metadb.MetadataPkinfo"

// init register load
func init() {
	registerBeanMetadataPkinfo()
}

// register MetadataPkinfo
func registerBeanMetadataPkinfo() {
	basedi.RegisterLoadBean(singleNameMetadataPkinfo, LoadMetadataPkinfo)
}

func FindBeanMetadataPkinfo() *MetadataPkinfo {
	bean, ok := basedi.FindBean(singleNameMetadataPkinfo).(*MetadataPkinfo)
	if !ok {
		logrus.Errorf("FindBeanMetadataPkinfo: failed to cast bean to *MetadataPkinfo")
		return nil
	}
	return bean

}

func LoadMetadataPkinfo() baseiface.ISingleton {
	var s = NewMetadataPkinfo()
	InjectMetadataPkinfo(s)
	return s

}

func InjectMetadataPkinfo(s *MetadataPkinfo) {

	// logrus.Debug("inject")
}
