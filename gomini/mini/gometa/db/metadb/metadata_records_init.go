package metadb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metadata_records_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMetadataRecords = "metadb.MetadataRecords"

// init register load
func init() {
	registerBeanMetadataRecords()
}

// register MetadataRecords
func registerBeanMetadataRecords() {
	basedi.RegisterLoadBean(singleNameMetadataRecords, LoadMetadataRecords)
}

func FindBeanMetadataRecords() *MetadataRecords {
	bean, ok := basedi.FindBean(singleNameMetadataRecords).(*MetadataRecords)
	if !ok {
		logrus.Errorf("FindBeanMetadataRecords: failed to cast bean to *MetadataRecords")
		return nil
	}
	return bean

}

func LoadMetadataRecords() baseiface.ISingleton {
	var s = NewIchubRecords()
	InjectMetadataRecords(s)
	return s

}

func InjectMetadataRecords(s *MetadataRecords) {

	// logrus.Debug("inject")
}
