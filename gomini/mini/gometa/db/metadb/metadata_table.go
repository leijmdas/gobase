package metadb

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"github.com/jinzhu/gorm"

	jsoniter "github.com/json-iterator/go"
	"strings"
)

/*
@Title    文件名称: metadata_table.go
@Description  描述: 元数据--表信息

@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/
type MetadataTable struct {
	basedto.BaseEntity

	TableSchema  string            `json:"table_schema" gorm:"column:table_schema"`
	TableName    string            `json:"table_name" gorm:"column:table_name"`
	TableComment string            `json:"table_comment" gorm:"column:table_comment"`
	PkInfo       *MetadataPkinfo   `json:"pk_info,omitempty"`
	Columns      []*MetadataColumn `json:"columns"`
	GoFields     []MetadataGofield `json:"go_fields"`

	TableExist bool   `json:"table_exist"`
	FieldsName string `json:"fields_name"`
	IndexName  string `json:"index_name"`
	//IndexMetadata *index.EsindexMetadata `json:"-"`
}

func (this *MetadataTable) Parse2EsIndex() {
	this.IndexName = this.TableName
	//this.IndexMetadata = index.NewIndexMetadata()
	//this.IndexMetadata.SetIndexName(this.IndexName)
	for _, _ = range this.Columns {
		//var estype = c.FindColEsType()
		//this.IndexMetadata.AddField(c.ColumnName, estype)
	}
}
func (this *MetadataTable) ToMapping() map[string]any {
	this.Parse2EsIndex()
	return nil //this.IndexMetadata.ToMapping()

}
func (this *MetadataTable) FindMetaColumn(colname string) *MetadataColumn {
	for _, v := range this.Columns {
		if v.ColumnName == colname {
			return v
		}
	}

	return nil //this.IndexMetadata.ToMapping()

}
func (this *MetadataTable) ToText() string {
	var t = this.TableName + "=" + strings.ReplaceAll(stringutils.Camel2Case(this.TableName), "_", " ")
	return strings.ReplaceAll(t, "-", " ")

}
func (this *MetadataTable) ToColText() string {
	var ss []string = make([]string, 0)
	for _, v := range this.Columns {
		var coltext = this.TableName + ":" + v.ToText()

		ss = append(ss, coltext)
	}
	return strings.Join(ss, "\r\n")

}

func (this *MetadataTable) ToMappingStr() string {
	return "" //this.IndexMetadata.ToMappingStr()

}
func NewMetadataTable() *MetadataTable {
	var meta = &MetadataTable{}
	meta.InitProxy(meta)
	meta.GoFields = []MetadataGofield{}
	//gometa.IndexMetadata = index.NewIndexMetadata()
	return meta
}
func (this *MetadataTable) ToFieldsString() string {
	var sb = strings.Builder{}
	for _, v := range this.Columns {
		if sb.Len() == 0 {
			sb.WriteString(v.ColumnName)
		} else {
			sb.WriteString("," + v.ColumnName)
		}
	}
	return sb.String()

}

//build2GoFields

func (this *MetadataTable) BuildGoFields() {
	for _, v := range this.Columns {
		var gof = NewMetadatGofield()
		gof.DataType = v.DataType
		gof.ColumnName = v.ColumnName
		gof.GoType = v.FindColGoType() //(v.DataType)
		this.GoFields = append(this.GoFields, *gof)
	}
}

func (this *MetadataTable) ToString() string {
	s, _ := jsoniter.Marshal(this)
	return string(s)

}

func (this *MetadataTable) FindGoType(field string) string {
	for _, v := range this.GoFields {
		if v.ColumnName == field {
			return v.GoType
		}
	}
	return "string"
}

var mysql_alterTableComment = "ALTER TABLE %s COMMENT = '%s';"

// var msql_alterColumnComment = `ALTER TABLE %s 	COMMENT ON COLUMN %s IS '%s'`
var mysql_alterColumnComment = `ALTER TABLE %s MODIFY COLUMN %s  %s COMMENT '%s';`

func (this *MetadataTable) AlterColumnComment(db *gorm.DB, column *MetadataColumn) error {
	//var sql = fmt.Sprintf(mysql_alterColumnComment, this.TableName, column.ColumnName, comment)
	var sql = fmt.Sprintf(mysql_alterColumnComment, this.TableName, column.ColumnName, column.ColumnType, column.ColumnComment)
	goutils.Info(sql)
	return db.Exec(sql).Error

}
func (this *MetadataTable) AlterTableComment(db *gorm.DB) error {
	var sql = fmt.Sprintf(mysql_alterTableComment, this.TableName, this.TableComment)
	return db.Exec(sql).Error
}
