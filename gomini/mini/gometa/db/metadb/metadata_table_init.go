package metadb

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metadata_table_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMetadataTable = "metadb.MetadataTable"

// init register load
func init() {
	registerBeanMetadataTable()
}

// register MetadataTable
func registerBeanMetadataTable() {
	basedi.RegisterLoadBean(singleNameMetadataTable, LoadMetadataTable)
}

func FindBeanMetadataTable() *MetadataTable {
	bean, ok := basedi.FindBean(singleNameMetadataTable).(*MetadataTable)
	if !ok {
		logrus.Errorf("FindBeanMetadataTable: failed to cast bean to *MetadataTable")
		return nil
	}
	return bean

}

func LoadMetadataTable() baseiface.ISingleton {
	var s = NewMetadataTable()
	InjectMetadataTable(s)
	return s

}

func InjectMetadataTable(s *MetadataTable) {

	// logrus.Debug("inject")
}
