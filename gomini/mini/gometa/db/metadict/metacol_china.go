package metadict

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/sirupsen/logrus"
	"sort"
	"strings"
)

var input = fileutils.FindRootDir() + "/data/input/dbdict/"
var output = fileutils.FindRootDir() + "/data/output/dbdict/"

type MetacolChina struct {
	basedto.BaseEntity

	incol      string
	outcol     string
	metatables []metadb.MetadataTable

	tablein  string
	tableout string
}

var InstMetaFactroy = metafactroy.FindBeanMetaFactroy()
var outcol = output + "outcol.txt"
var tablein = input + "tablein.txt"
var tableout = outcol + "tableout.txt"
var incol = input + "incol.txt"

func NewMetacolChina() *MetacolChina {
	return &MetacolChina{

		outcol: outcol,
		incol:  incol,

		tablein:  tablein,
		tableout: tableout,
	}
}
func (this *MetacolChina) MakeTables() {
	InstMetaFactroy.IniDb()

	var tbls = InstMetaFactroy.FindTables()
	var lines []string = make([]string, 0)
	for _, t := range tbls {
		lines = append(lines, t.ToText())
	}
	//var text = md.ToText()
	goutils.Info(strings.Join(lines, "\r\n"))
	fileutils.WriteBytesFile(this.tablein, []byte(strings.Join(lines, "\r\n")))

}
func (this *MetacolChina) Parse() []string {
	this.metatables = InstMetaFactroy.FindTables()
	for _, _ = range this.metatables {
		//	t.Columns = InstMetaFactroy.FindMetadata(t.TableName).Columns
	}
	var liness, err = fileutils.ReadFileToString(this.outcol)
	var lines = strings.Split(liness, "\r\n")
	logrus.Info(err, lines)
	this.ParseLines(lines)
	return lines
}
func (this *MetacolChina) ParseLines(lines []string) {
	for _, line := range lines {
		this.parseLine(line)
	}
	logrus.Info(lines)
}

func (this *MetacolChina) parseLine(line string) {
	var colchina = strings.Split(line, "=")
	var tabcols = strings.Split(colchina[0], ":")
	for _, tbl := range this.metatables {
		var t = InstMetaFactroy.FindMetadata(tbl.TableName)
		if t.TableName == tabcols[0] {
			for _, c := range t.Columns {
				if strings.ToUpper(c.ColumnName) == strings.ToUpper(tabcols[1]) {
					if len(colchina) > 1 {
						c.ColumnComment = colchina[1]
						InstMetaFactroy.AlterColumnComment(t, c)
					}
				}
			}
		}
	}
	goutils.Info(colchina, tabcols, "\r\n")
}

func (this *MetacolChina) MakeColAll() {
	InstMetaFactroy.IniDb()

	var tbls = InstMetaFactroy.FindTables()
	var lines []string = make([]string, 0)
	for _, t := range tbls {
		var md = InstMetaFactroy.FindMetadata(t.TableName)
		lines = append(lines, md.ToColText())
	}
	goutils.Info(strings.Join(lines[:128], "\r\n"))
	this.write2Six(lines)
	//	util.WriteBytesTo(this.incol, []byte(strings.Join(lines[:128], "\r\n")))

}
func (this *MetacolChina) write2Six(lines []string) {
	i := len(lines) / 6 //+ len(lines)%6
	for ii := 0; ii < 6; ii++ {

		var start = i * (ii)
		var end = i * (ii + 1)
		if ii == 5 {
			end = len(lines)
		}
		fileutils.WriteBytesFile(this.incol+gconv.String(ii+1), []byte(strings.Join(lines[start:end], "\r\n")))

	}
}
func (this *MetacolChina) ParseSix() {
	this.metatables = InstMetaFactroy.FindTables()

	for i := 1; i <= 6; i++ {
		this.ParseOne(i)
	}
}
func (this *MetacolChina) ParseOne(i int) {
	var name = strings.ReplaceAll(this.outcol, ".txt", gconv.String(i)+".txt")
	var linestr, err = fileutils.ReadFileToString(name)
	var lines = strings.Split(linestr, "\r\n")
	sort.Strings(lines)
	logrus.Info(err, lines)
	if err != nil {
		panic(err)
		return
	}
	this.ParseLines(lines)

}
func (this *MetacolChina) WriteTables() {

	var linestr, err = fileutils.ReadFileToString(this.tableout)
	var lines = strings.Split(linestr, "\n")
	logrus.Info(err)
	for _, line := range lines {
		logrus.Info(line)
		var ss = strings.Split(line, "=")
		if len(ss) == 2 {
			var t = InstMetaFactroy.FindMetadata(ss[0])
			t.TableComment = ss[1]
			InstMetaFactroy.AlterMetaTable(t)
		}
	}

}
