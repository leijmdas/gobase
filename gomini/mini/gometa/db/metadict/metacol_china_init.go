package metadict

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metacol_china_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-17 06:59:10)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-17 06:59:10)

* *********************************************************/

const singleNameMetacolChina = "dbdict.MetacolChina"

// init register load
func init() {
	registerBeanMetacolChina()
}

// register MetacolChina
func registerBeanMetacolChina() {
	basedi.RegisterLoadBean(singleNameMetacolChina, LoadMetacolChina)
}

func FindBeanMetacolChina() *MetacolChina {
	bean, ok := basedi.FindBean(singleNameMetacolChina).(*MetacolChina)
	if !ok {
		logrus.Errorf("FindBeanMetacolChina: failed to cast bean to *MetacolChina")
		return nil
	}
	return bean

}

func LoadMetacolChina() baseiface.ISingleton {
	var s = NewMetacolChina()
	InjectMetacolChina(s)
	return s

}

func InjectMetacolChina(s *MetacolChina) {

	// logrus.Debug("inject")
}
