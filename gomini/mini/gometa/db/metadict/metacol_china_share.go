package metadict

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
)

type MetacolChinaShare struct {
	basedto.BaseEntity
	*MetacolChina
}

var outcolshare = output + "outcolshare.txt"
var tableinshare = input + "tableinshare.txt"
var tableoutshare = output + "tableoutshare.txt"
var incolshare = input + "incolshare.txt"

func NewcolChinaShare() *MetacolChinaShare {
	var mcs = &MetacolChinaShare{
		MetacolChina: NewMetacolChina(),
	}
	mcs.tablein = tableinshare
	mcs.tableout = tableoutshare
	mcs.outcol = outcolshare
	mcs.incol = incolshare
	return mcs
}
