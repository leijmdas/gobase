package metadict

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: metacol_china_share_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-17 10:57:34)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-17 10:57:34)

* *********************************************************/

const singleNameMetacolChinaShare = "dbdict.MetacolChinaShare"

// init register load
func init() {
	registerBeanMetacolChinaShare()
}

// register MetacolChinaShare
func registerBeanMetacolChinaShare() {
	basedi.RegisterLoadBean(singleNameMetacolChinaShare, LoadMetacolChinaShare)
}

func FindBeanMetacolChinaShare() *MetacolChinaShare {
	bean, ok := basedi.FindBean(singleNameMetacolChinaShare).(*MetacolChinaShare)
	if !ok {
		logrus.Errorf("FindBeanMetacolChinaShare: failed to cast bean to *MetacolChinaShare")
		return nil
	}
	return bean

}

func LoadMetacolChinaShare() baseiface.ISingleton {
	var s = NewcolChinaShare()
	InjectMetacolChinaShare(s)
	return s

}

func InjectMetacolChinaShare(s *MetacolChinaShare) {

	// logrus.Debug("inject")
}
