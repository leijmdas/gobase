package metafactroy

import (
	"container/list"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
)

//var Cfg = ichubconfig.Default()

type DefineFactors struct {
	basedto.BaseEntity

	Columns *[]*metadb.MetadataColumn
	Models  *list.List
	//mappings
}

func NewDefineFactors() *DefineFactors {
	return &DefineFactors{
		Columns: new([]*metadb.MetadataColumn),
		Models:  new(list.List),
	}
}
