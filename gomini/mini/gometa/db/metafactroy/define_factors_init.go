package metafactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: define_factors_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameDefineFactors = "factroy.DefineFactors"

// init register load
func init() {
	registerBeanDefineFactors()
}

// register DefineFactors
func registerBeanDefineFactors() {
	basedi.RegisterLoadBean(singleNameDefineFactors, LoadDefineFactors)
}

func FindBeanDefineFactors() *DefineFactors {
	bean, ok := basedi.FindBean(singleNameDefineFactors).(*DefineFactors)
	if !ok {
		logrus.Errorf("FindBeanDefineFactors: failed to cast bean to *DefineFactors")
		return nil
	}
	return bean

}

func LoadDefineFactors() baseiface.ISingleton {
	var s = NewDefineFactors()
	InjectDefineFactors(s)
	return s

}

func InjectDefineFactors(s *DefineFactors) {

	// logrus.Debug("inject")
}
