package metafactroy

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconsts"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/dbcontent"
	"gitee.com/leijmdas/gobase/goconfig/common/dbcontent/database"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade/tableentity"
	meta "gitee.com/leijmdas/gobase/gomini/mini/metafile"

	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metacontext"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
	"github.com/jinzhu/copier"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

/*
@Title    文件名称: db_factroy.go
@Description  描述: 础数据库工厂

@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/
const meta_table_out = "/data/output/gometa/dbdict/"

type MetaFactroy struct {
	basedto.BaseEntitySingle
	meta.Meta
	DbClientDto  *baseconfig.DbClientDto
	funcGetDb    database.FuncGetDb `json:"-"`
	Table        string
	TableComment *string

	Pkey       string
	PkeyType   string
	StrPkey    string `json:"-"`
	StrPkeyLen string `json:"-"`

	mysql    FacatroyMysql   `json:"-"`
	postgres FactroyPostgres `json:"-"`
}

func getDb() *gorm.DB {
	db, _ := dbcontent.GetDB()
	return db
}

/*
@Title    函数名称: NewMetaFactroy
@Description  描述: 创建数据库工厂
@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Return  返回值: *MetaFactroy
*/
func NewMetaFactroy() *MetaFactroy {
	var factry = &MetaFactroy{}

	factry.funcGetDb = getDb
	factry.mysql.funcGetDb = factry.funcGetDb
	factry.postgres.funcGetDb = factry.funcGetDb

	return factry
}

func (this *MetaFactroy) IsMysql() bool {
	return this.DbClientDto.Dbtype == baseconsts.DB_TYPE_MYSQL
}

func (this *MetaFactroy) findPGTableComment() *metadb.MetadataTable {
	sql := `SELECT 
		relname as table_name,
		obj_description(oid) as table_comment
		FROM pg_class
		WHERE relkind = 'r' and relname='%s'
	`
	sql = fmt.Sprintf(sql, this.Table)
	var tables metadb.MetadataTable
	e := this.funcGetDb().Model(&metadb.MetadataPkinfo{}).Raw(sql).Find(&tables).Error
	if e != nil {
		fmt.Println("***=" + e.Error())

	}
	return &tables
}

func (this *MetaFactroy) findPGColumnComment() []metadb.MetadataColumn {
	s := `SELECT 
    c.relname as TableName,
    col_description ( a.attrelid, a.attnum ) AS ColumnComment,
    format_type ( a.atttypid, a.atttypmod ) AS ColumnType,
    a.attname AS ColumnName 
	FROM pg_class AS c,    pg_attribute AS a
	WHERE    a.attrelid = c.oid   AND a.attnum >0
 			and c.relname = '%s'  `
	s = fmt.Sprintf(s, this.Table)
	var cs []metadb.MetadataColumn
	e := this.funcGetDb().Model(&metadb.MetadataPkinfo{}).Raw(s).Find(&cs).Error
	if e != nil {
		fmt.Println("***=" + e.Error())
	}

	return cs
}

//dbcontent type->go type

func (this *MetaFactroy) FindPgPkey(table string) []metadb.MetadataPkinfo {
	s := `SELECT
	pg_constraint.conname AS pkname,
	pg_attribute.attname AS colname,
	pg_type.typname AS typename
		FROM pg_constraint
		INNER JOIN pg_class ON pg_constraint.conrelid = pg_class.oid
		INNER JOIN pg_attribute ON pg_attribute.attrelid = pg_class.oid
		AND pg_attribute.attnum = pg_constraint.conkey [ 1 ]
		INNER JOIN pg_type ON pg_type.oid = pg_attribute.atttypid
	WHERE	pg_class.relname = '%s' AND pg_constraint.contype = 'p' `

	var pkInfos []metadb.MetadataPkinfo
	s = fmt.Sprintf(s, table)
	logrus.Info("Find PKey: " + s)
	e := this.funcGetDb().Model(&metadb.MetadataPkinfo{}).Raw(s).Find(&pkInfos).Error
	if e != nil {
		fmt.Println(e.Error())
	}

	logrus.Info("FindPgPkey: \n" + s)
	if len(pkInfos) == 0 {
		fmt.Println(fmt.Sprintf("pkInfos len=0 无主键（可能表%s不存在！） ", table))
	}
	logrus.Infof("pkInfos: %d", len(pkInfos))
	return pkInfos
}

func (this *MetaFactroy) FindFields(table string, fieldsName string) *metadb.MetadataTable {

	var metatable = this.FindMetadata(table)
	metatable.FieldsName = metatable.ToFieldsString()
	if len(fieldsName) != 0 && fieldsName != "*" {
		metatable.FieldsName = fieldsName
	}
	return metatable
}

func (this *MetaFactroy) IniDb() *MetaFactroy {
	if this.DbClientDto != nil {
		return this
	}
	var cfg = ichubconfig.FindBeanIchubConfig()
	this.DbClientDto = cfg.ReadIchubDb()
	metacontext.FindBeanMetadataContext().DbType = this.DbClientDto.Dbtype
	return this
}
func (this *MetaFactroy) FindMetadata(table string) *metadb.MetadataTable {
	var c, found = metadb.InstMetadataCache.CacheGet(table)
	if found {
		return c
	}
	this.IniDb()
	var DbFindMeta = func() *metadb.MetadataTable {

		this.Table = table
		var metadataTable = metadb.NewMetadataTable()
		metadataTable.TableExist = true
		if this.IsMysql() {
			copier.Copy(&this.mysql, this)
			metadataTable.Columns = *this.mysql.FindColumns()

		} else {
			copier.Copy(&this.postgres, this)
			metadataTable.Columns = *this.postgres.FindColumns()
		}
		metadataTable.TableSchema = this.DbClientDto.Dbname
		metadataTable.TableName = table
		metadataTable.TableExist = len(metadataTable.Columns) > 0
		metadataTable.BuildGoFields()
		metadataTable.PkInfo = metadb.NewMetadataPkinfo()
		if this.IsMysql() {
			metadataTable.PkInfo.TypeName = this.mysql.PkeyType
			metadataTable.PkInfo.PkName = this.mysql.Pkey
		} else {
			metadataTable.PkInfo.TypeName = this.postgres.PkeyType
			metadataTable.PkInfo.PkName = this.postgres.Pkey
		}
		metadataTable.PkInfo.ColName = stringutils.Camel2Case(metadataTable.PkInfo.PkName)

		return metadataTable
	}
	var meta = DbFindMeta()
	meta.Parse2EsIndex()
	//设置cache
	metadb.InstMetadataCache.CacheSet(table, meta)
	if err := this.WriteMeta(meta); err != nil {
		goutils.Error(err)

	}
	return meta

}

//var mysql_alterTableComment = "ALTER TABLE %s COMMENT = '%s';"
//var mysql_alterColumnComment = `ALTER TABLE %s MODIFY COLUMN %s  %s COMMENT '%s';`

func (this *MetaFactroy) AlterColumnComment(metatable *metadb.MetadataTable, column *metadb.MetadataColumn) error {
	//	var sql = fmt.Sprintf(mysql_alterColumnComment, table, column.ColumnName, column.ColumnType, column.ColumnComment)
	if this.IsMysql() {

		err := metatable.AlterColumnComment(this.funcGetDb(), column)
		if err != nil {
			return err
		}
		return nil
	} else {
		//sql := fmt.Sprintf(postgres_alterTableComment, table, comment)
	}
	return errors.New("invalid dbtype")
}
func (this *MetaFactroy) AlterTableComment(table string, comment string) error {
	//	var sql = fmt.Sprintf(mysql_alterTableComment, table, comment)
	var mt = metadb.NewMetadataTable()
	mt.TableComment = comment
	mt.TableName = table
	if this.IsMysql() {

		err := mt.AlterTableComment(this.funcGetDb())
		if err != nil {
			goutils.Error(err)
			return err
		}
		return nil
	}
	return errors.New("invalid dbtype")
}
func (this *MetaFactroy) AlterMetaTable(md *metadb.MetadataTable) error {
	//	var sql = fmt.Sprintf(mysql_alterTableComment, table, comment)

	if this.IsMysql() {

		err := md.AlterTableComment(this.funcGetDb())
		if err != nil {
			goutils.Error(err)
			return err
		}
		return nil
	}
	return errors.New("invalid dbtype")
}

//ALTER TABLE table_name
//COMMENT ON COLUMN column_name IS '新的注释文本';

func (this *MetaFactroy) FindTableComment() {
	if this.IsMysql() {
		tbls := this.FindTables()
		for _, v := range tbls {
			if this.Table == v.TableName {
				this.TableComment = &v.TableComment
				return
			}
		}
	}

	if !this.IsMysql() {
		tc := this.findPGTableComment()
		this.TableComment = &tc.TableComment

	}

}

func (this *MetaFactroy) FindTables() []metadb.MetadataTable {
	var tables []metadb.MetadataTable
	this.IniDb()
	if this.IsMysql() {
		sql := fmt.Sprintf(mysql_findTable, this.DbClientDto.Dbname)
		err := this.funcGetDb().Raw(sql).Find(&tables).Error
		if err != nil {
			panic(err.Error() + " :" + sql)
			return []metadb.MetadataTable{}
		}
	} else {
		sql := postgres_findTable
		err := this.funcGetDb().Raw(sql).Find(&tables).Error
		if err != nil {
			panic(err.Error() + " :" + sql)

		}
	}
	return tables
}

func (this *MetaFactroy) FindGoType(fieldType string) (goType string) {

	goType = metacontext.FindBeanMetadataContext().FindGoType(fieldType)
	return
}

func (this *MetaFactroy) FindProtoType(fieldType string) (pbType string) {

	pbType = metacontext.FindBeanMetadataContext().FindProtoType(fieldType)
	return
}

func (self *MetaFactroy) MetaTableCode(table string) (string, error) {
	var dto, err = self.MetaTable(table, false)
	if err != nil {
		return "", err
	}
	return dto.ToGocode(false), nil
}
func (self *MetaFactroy) MetaTableCodeFile(table string) (string, error) {
	var metaTable, err = self.MetaTable(table, false)
	if err != nil {
		golog.Error("MetaFactroy [MetaTableCodeFile] ", err)
		return "", err
	}

	return metaTable.ToGocodeFile(false)
}
func (self *MetaFactroy) MetaTable(table string, trimPre bool) (*tableentity.TableEntity, error) {
	var meta = self.FindMetadata(table)
	if !meta.TableExist {
		return nil, errors.New("table " + table + " not exist")
	}
	var tableEntity = tableentity.NewTableEntity()
	tableEntity.InitEntity(table, meta.PkInfo)
	for _, column := range meta.Columns {
		var field = tableEntity.AppendField(column.ColumnName, column.DataType)
		field.GormTag = column.ToGormTag(meta.PkInfo.PkName)
	}
	tableEntity.SortFields()
	return tableEntity, nil
}
