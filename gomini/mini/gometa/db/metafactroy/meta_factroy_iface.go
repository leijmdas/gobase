package metafactroy

import "gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"

type MetaFactroyIface interface {
	FindMetadata(table string) *metadb.MetadataTable
	FindFields(table string, fields string) string
}
