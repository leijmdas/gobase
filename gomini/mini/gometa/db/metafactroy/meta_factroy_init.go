package metafactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: meta_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameMetaFactroy = "factroy.MetaFactroy"

// init register load
func init() {
	registerBeanMetaFactroy()
}

// register MetaFactroy
func registerBeanMetaFactroy() {
	basedi.RegisterLoadBean(singleNameMetaFactroy, LoadMetaFactroy)
}

func FindBeanMetaFactroy() *MetaFactroy {
	bean, ok := basedi.FindBean(singleNameMetaFactroy).(*MetaFactroy)
	if !ok {
		logrus.Errorf("FindBeanMetaFactroy: failed to cast bean to *MetaFactroy")
		return nil
	}
	return bean

}

func LoadMetaFactroy() baseiface.ISingleton {
	var s = NewMetaFactroy()
	InjectMetaFactroy(s)
	return s

}

func InjectMetaFactroy(s *MetaFactroy) {

	// logrus.Debug("inject")
}
