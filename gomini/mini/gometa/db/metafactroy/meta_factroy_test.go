package metafactroy

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestMetaFactroySuite struct {
	suite.Suite
	inst *MetaFactroy

	rootdir string
}

func (this *TestMetaFactroySuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanMetaFactroy()

	this.rootdir = utils.FindRootDir()

}
func (this *TestMetaFactroySuite) TearDownTest() {

	ichublog.InitLogrus()
	this.inst = FindBeanMetaFactroy()

	this.rootdir = utils.FindRootDir()

}

func TestMetaFactroySuites(t *testing.T) {
	suite.Run(t, new(TestMetaFactroySuite))
}

func (this *TestMetaFactroySuite) Test001_IsMysql() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test002_findPGTableComment() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test003_findPGColumnComment() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test004_FindPgPkey() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test005_FindFields() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test006_IniDb() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test007_FindMetadata() {
	logrus.Info(1)

	var metatable = FindBeanMetaFactroy().FindMetadata("employee")

	goutils.Info(metatable)
}

func (this *TestMetaFactroySuite) Test008_FindMetadata() {
	logrus.Info(1)

	var metatables = FindBeanMetaFactroy().FindTables()

	goutils.Info(jsonutils.ToJsonPretty(metatables))
}

func (this *TestMetaFactroySuite) Test009_AlterTableComment() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test010_AlterMetaTable() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test011_FindTableComment() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test012_FindTables() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test013_FindGoType() {
	logrus.Info(1)

}

func (this *TestMetaFactroySuite) Test014_FindProtoType() {
	logrus.Info(1)

}
