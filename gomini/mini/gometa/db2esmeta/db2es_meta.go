package db2esmeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
)

/*
 * @Description:
 * @PathFile: \goconfig\gometa\db2esmeta\db2es_meta.go
 */
type EsMapping struct {
	Settings string `json:"settings"`
	Mappings string `json:"mappings"`
}
type Db2esMeta struct {
	basedto.BaseEntity

	TableName string
	IndexName string
	Pkey      string

	EsMapping EsMapping
	dbfields  map[string]string
	esfields  map[string]string
}

func NewDb2esMeta() *Db2esMeta {
	return &Db2esMeta{
		dbfields: make(map[string]string),
		esfields: map[string]string{},
	}
}

func (this *Db2esMeta) ToEs() {
	this.dbfields = make(map[string]string)
}

const sets = `  {
    "index": {
      "routing": {
        "allocation": {
          "include": {
            "_tier_preference": "data_content"
          }
        }
      },
      "number_of_shards": "1"
    }
  }`

const setsempty = `  { 
  }`

// var DbFactry = metadatafactroy.NewMetadataFactroy()
func (this *Db2esMeta) FromDb(table *metadb.MetadataTable) {
	this.dbfields = make(map[string]string)
	this.EsMapping.Mappings = table.ToMappingStr() // table.ToMappingProp()
	this.EsMapping.Settings = setsempty

}

func (this *Db2esMeta) ToMappings() string {
	return jsonutils.ToJsonPretty(this.EsMapping)
}
