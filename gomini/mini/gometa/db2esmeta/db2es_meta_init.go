package db2esmeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
)

/*
	@Title   文件名称: db2es_meta_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-06-22 10:35:18
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-06-22 10:35:18
*/

const singleNameDb2esMeta = "db2esmeta.Db2esMeta"

// init register load
func init() {
	registerBeanDb2esMeta()
}

// register Db2esMeta
func registerBeanDb2esMeta() {
	basedi.RegisterLoadBean(singleNameDb2esMeta, LoadDb2esMeta)
}

func FindBeanDb2esMeta() *Db2esMeta {
	return basedi.FindBean(singleNameDb2esMeta).(*Db2esMeta)
}

func LoadDb2esMeta() baseiface.ISingleton {
	var inst = NewDb2esMeta()
	InjectDb2esMeta(inst)
	return inst

}

func InjectDb2esMeta(s *Db2esMeta) {

	golog.Debug("inject")
}
