package db2esmeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestDb2esMetaSuite struct {
	suite.Suite
	inst *Db2esMeta

	rootdir string
}

func (this *TestDb2esMetaSuite) SetupTest() {

	ichublog.InitLogrus()
	//this.inst = FindBeanDb2esMeta()
	this.rootdir = fileutils.FindRootDir()

}
func (suite *TestDb2esMetaSuite) TearDownTest() {
	logrus.Println(" teardown")
}
func (suite *TestDb2esMetaSuite) TearDownSuite() {
	logrus.Println(" teardown")
}

func TestDb2esMetaSuites(t *testing.T) {
	suite.Run(t, new(TestDb2esMetaSuite))
}

func (this *TestDb2esMetaSuite) Test001_ToEs() {

	var metadata = FindBeanDb2esMetafactroy().FindMetadata("sys_dept")
	goutils.Info(metadata, metadata.ToMappingStr())

}

func (this *TestDb2esMetaSuite) Test002_makeIndexFromTable() {

	var mappings = FindBeanDb2esMetafactroy().MakeDb2Es("sys_user")
	golog.Info(mappings)
}
