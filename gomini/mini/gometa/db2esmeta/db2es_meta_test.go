package db2esmeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"

	"testing"
)

func init() {
	fileutils.FindRootDir()
}

var DbFactry = metafactroy.NewMetaFactroy()

func Test0001_FindMetadata(t *testing.T) {

	var metadata = DbFactry.FindMetadata("sys_dept")
	golog.Info(metadata, metadata.ToFieldsString())
}
func Test0002_FindMetadataIndex(t *testing.T) {

	var metadata = DbFactry.FindMetadata("sys_dept")
	golog.Info(metadata, metadata.ToMappingStr())
}
