package db2esmeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"
)

type Db2esMetafactroy struct {
	basedto.BaseEntity
	*metafactroy.MetaFactroy
}

func NewDb2esMetafactroy() *Db2esMetafactroy {
	return &Db2esMetafactroy{
		MetaFactroy: metafactroy.NewMetaFactroy(),
	}
}

func (self Db2esMetafactroy) MakeDb2Es(table string) string {
	var meta = self.MetaFactroy.FindMetadata(table)
	var db2es = FindBeanDb2esMeta()
	db2es.FromDb(meta)
	var mappings = db2es.ToMappings()
	golog.Info(mappings)
	return mappings

}
func (self Db2esMetafactroy) FindDb2es(table string) *metadb.MetadataTable {
	var meta = self.MetaFactroy.FindMetadata(table)
	var db2es = FindBeanDb2esMeta()
	db2es.FromDb(meta)
	db2es.Pkey = meta.PkInfo.PkName
	return meta

}
