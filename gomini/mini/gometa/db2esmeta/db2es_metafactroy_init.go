package db2esmeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
)

/*
	@Title   文件名称: db2es_metafactroy_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-06-22 10:41:42
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-06-22 10:41:42
*/

const singleNameDb2esMetafactroy = "db2esmeta.Db2esMetafactroy"

// init register load
func init() {
	registerBeanDb2esMetafactroy()
}

// register Db2esMetafactroy
func registerBeanDb2esMetafactroy() {
	basedi.RegisterLoadBean(singleNameDb2esMetafactroy, LoadDb2esMetafactroy)
}

func FindBeanDb2esMetafactroy() *Db2esMetafactroy {
	return basedi.FindBean(singleNameDb2esMetafactroy).(*Db2esMetafactroy)
}

func LoadDb2esMetafactroy() baseiface.ISingleton {
	var inst = NewDb2esMetafactroy()
	InjectDb2esMetafactroy(inst)
	return inst

}

func InjectDb2esMetafactroy(s *Db2esMetafactroy) {

	// logrus.Debug("inject")
}
