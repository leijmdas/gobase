package dimeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: di_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameDiDto = "dimeta.DiDto"

// init register load
func init() {
	registerBeanDiDto()
}

// register DiDto
func registerBeanDiDto() {
	basedi.RegisterLoadBean(singleNameDiDto, LoadDiDto)
}

func FindBeanDiDto() *DiDto {
	bean, ok := basedi.FindBean(singleNameDiDto).(*DiDto)
	if !ok {
		logrus.Errorf("FindBeanDiDto: failed to cast bean to *DiDto")
		return nil
	}
	return bean

}

func LoadDiDto() baseiface.ISingleton {
	var s = NewDiDto()
	InjectDiDto(s)
	return s

}

func InjectDiDto(s *DiDto) {

	// logrus.Debug("inject")
}
