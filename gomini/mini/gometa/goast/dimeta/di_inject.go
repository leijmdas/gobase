package dimeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/goast/diconsts"

	"github.com/sirupsen/logrus"
	"go/ast"
	"reflect"
	"strings"
)

const s_ENTITY_NONE = ``
const s_ENTITY_NEW = `
	// 实例化默认New
	s.{{.MemberName}} = {{.PkgName}}.New{{.MemberType}}()`
const s_ENTITY_AUTO = `
	// 自动实例化
	s.{{.MemberName}} = {{.PkgName}}.FindBean{{.MemberType}}()`
const s_ENTITY_SINGLE = `
	// 简单实例化
	s.{{.MemberName}} = &{{.PkgName}}.{{.MemberType}}{}`
const s_ENTITY_BIND = `
	// 实例注册函数
   	s.Bind()	
	// 实例查找函数
	if find := s.FindBinding("{{.MemberName}}"); find != nil {
		logrus.Debug("find binded LoadBean{{.MemberType}}!")
		// 类型断言执行load
		s.{{.MemberName}}  = find().(*{{.PkgName}}.{{.MemberType}})
	} else {
		// 未找到绑定，实例化
		s.{{.MemberName}}  = &{{.PkgName}}.{{.MemberType}}{}
		logrus.Error("no binding LoadBean{{.MemberType}}!")
	}`

//s.SingleEntity = s.FindBinding("SingleEntity")().(*single.SingleEntity)

// di inject member
type DiInject struct {
	basedto.BaseEntity

	InjectMod int `json:"injectMod"`
	//auto new bind
	Tag        string `json:"tag"`
	InjectCode string `json:"injectCode"`

	GoType    string `json:"goType"`
	ImportLib string `json:"importLib"`

	MemberName string `json:"memberName"` //multi
	MemberType string `json:"memberType"` //MultiEntity
	PkgName    string `json:"pkgName"`    //multi
}

func NewDiInject() *DiInject {
	var di = &DiInject{
		InjectMod: diconsts.INJECT_ENTITY_NONE,
	}
	di.InitProxy(di)
	return di
}
func (this *DiInject) IsRootBaseEntity() bool {
	return this.MemberName == `BaseEntity` || this.MemberName == `BaseEntitySingle`

}
func (this *DiInject) SetInjectMod(isBaseEntity bool) {
	if isBaseEntity {
		this.InjectMod = diconsts.INJECT_ENTITY_AUTO
	} else {
		this.InjectMod = diconsts.INJECT_ENTITY_NONE
	}
}
func (this *DiInject) IsStruct() bool {
	return this.GoType == "struct"
}

func (this *DiInject) parseParams(s string) string {
	s = strings.ReplaceAll(s, "{{.MemberName}}", this.MemberName)
	if this.PkgName == "" {
		s = strings.ReplaceAll(s, "{{.PkgName}}.", this.PkgName)
	}
	s = strings.ReplaceAll(s, "{{.PkgName}}", this.PkgName)
	return strings.ReplaceAll(s, "{{.MemberType}}", this.MemberType)

}
func (this *DiInject) toSingle() string {

	return this.parseParams(s_ENTITY_SINGLE)

}
func (this *DiInject) toBind() string {
	return this.parseParams(s_ENTITY_BIND)

}

func (this *DiInject) toAuto() string {
	return this.parseParams(s_ENTITY_AUTO)

}

// baseentity Find
func (this *DiInject) toNew() string {

	return this.parseParams(s_ENTITY_NEW)

}

func (this *DiInject) Tag2InjectMod() {
	var val = len(this.Tag) == 0
	if this.Tag == diconsts.TAG_VALUE_TYPE_AUTO {
		this.InjectMod = diconsts.INJECT_ENTITY_AUTO
		val = true
	}
	if this.Tag == diconsts.TAG_VALUE_TYPE_BIND {
		this.InjectMod = diconsts.INJECT_ENTITY_BIND
		val = true
	}
	if this.Tag == diconsts.TAG_VALUE_TYPE_NEW {
		this.InjectMod = diconsts.INJECT_ENTITY_NEW
		val = true
	}
	if this.Tag == diconsts.TAG_VALUE_TYPE_SINGLE {
		this.InjectMod = diconsts.INJECT_ENTITY_SINGLE
		val = true
	}
	if this.Tag == diconsts.TAG_VALUE_TYPE_NONE {
		this.InjectMod = diconsts.INJECT_ENTITY_NONE
		val = true
	}
	if !val {
		panic("tag取值不对!" + this.ToPrettyString())
	}

}
func (this *DiInject) FilterInject() bool {
	return !this.IsStruct() || this.IsRootBaseEntity()
}
func (this *DiInject) ToInjectCode() string {
	if this.FilterInject() {
		return s_ENTITY_NONE
	}

	this.Tag2InjectMod()
	if this.InjectMod == diconsts.INJECT_ENTITY_AUTO {
		this.InjectCode = this.toAuto()
	} else if this.InjectMod == diconsts.INJECT_ENTITY_NEW {
		this.InjectCode = this.toNew()
	} else if this.InjectMod == diconsts.INJECT_ENTITY_BIND {
		this.InjectCode = this.toBind()
	} else if this.InjectMod == diconsts.INJECT_ENTITY_SINGLE {
		this.InjectCode = this.toSingle()
	}

	return this.InjectCode

}

func (di *DiInject) ParseTag(field *ast.Field) {

	if field.Tag != nil {
		// 提取标签的内容
		tag := reflect.StructTag(strings.Trim(field.Tag.Value, "`"))
		logrus.Info("Tag:", tag.Get(diconsts.TAG_GODI_NAME))
		di.Tag = tag.Get(diconsts.TAG_GODI_NAME)
		di.InjectCode = di.ToInjectCode()
	}
}
