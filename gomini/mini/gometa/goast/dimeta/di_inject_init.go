package dimeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: di_inject_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameDiInject = "dimeta.DiInject"

// init register load
func init() {
	registerBeanDiInject()
}

// register DiInject
func registerBeanDiInject() {
	basedi.RegisterLoadBean(singleNameDiInject, LoadDiInject)
}

func FindBeanDiInject() *DiInject {
	bean, ok := basedi.FindBean(singleNameDiInject).(*DiInject)
	if !ok {
		logrus.Errorf("FindBeanDiInject: failed to cast bean to *DiInject")
		return nil
	}
	return bean

}

func LoadDiInject() baseiface.ISingleton {
	var s = NewDiInject()
	InjectDiInject(s)
	return s

}

func InjectDiInject(s *DiInject) {

	// logrus.Debug("inject")
}
