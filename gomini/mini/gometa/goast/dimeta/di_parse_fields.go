package dimeta

import (
	"github.com/sirupsen/logrus"
	"go/ast"
)

type DiParseFields struct {
}

func NewParseFields() *DiParseFields {
	return &DiParseFields{}
}

func (this *DiParseFields) Parse(field *ast.Field) *DiInject {

	var di = NewDiInject()
	if len(field.Names) > 0 {
		this.ParseName(field, di)
	} else {
		this.ParseNoName(field, di)
	}
	return di
}

func (this *DiParseFields) ParseNoName(field *ast.Field, di *DiInject) {
	this.ParseSelectorExpr(field, di)

}
func (this *DiParseFields) ParseSelectorExpr(field *ast.Field, di *DiInject) bool {

	if expr, ok := field.Type.(*ast.SelectorExpr); ok {
		var name = expr.Sel.Name
		di.MemberType = name
		di.GoType = "struct"
		di.MemberName = name
		di.PkgName = expr.X.(*ast.Ident).Name
		return true
	}
	return false
}

// parse struct
func (this *DiParseFields) ParseStarExpr(field *ast.Field, di *DiInject) bool {
	if fieldType, ok := field.Type.(*ast.StarExpr); ok {

		di.GoType = "struct"
		if dTypeX, ok := fieldType.X.(*ast.SelectorExpr); ok {
			di.PkgName = dTypeX.X.(*ast.Ident).Name
			di.MemberType = dTypeX.Sel.Name
			return true
		}
		if starExpr, ok := fieldType.X.(*ast.Ident); ok {
			di.MemberType = starExpr.Name
			return true
		}
	}
	return false

}

func (this *DiParseFields) ParseIdent(field *ast.Field, di *DiInject) bool {
	if ident, ok := field.Type.(*ast.Ident); ok {
		di.GoType = ident.Name
		di.MemberType = ident.Name
		return true
	}
	return false

}

func (this *DiParseFields) ParseMapType(field *ast.Field, di *DiInject) bool {
	maptype, ok := field.Type.(*ast.MapType)
	if !ok {
		return false
	}

	di.GoType = "map"
	di.MemberType = "map[" + maptype.Key.(*ast.Ident).Name + "]"

	if id, ok := maptype.Value.(*ast.Ident); ok {
		di.MemberType += id.Name
	} else if star, ok := maptype.Value.(*ast.StarExpr); ok {
		if id, ok := star.X.(*ast.Ident); ok {
			di.MemberType += id.Name
		} else if sel, ok := star.X.(*ast.SelectorExpr); ok {
			di.MemberType = sel.Sel.Name
		}
	} else {
		logrus.Error("parse maptype err...")
	}

	return ok

}

func (this *DiParseFields) ParseArrayType(field *ast.Field, di *DiInject) bool {
	var arraytype, ok = field.Type.(*ast.ArrayType)
	if !ok {
		return false
	}

	di.GoType = "array"
	di.MemberType = "array"
	di.MemberName = field.Names[0].Name
	if elt, ok := arraytype.Elt.(*ast.Ident); ok {
		di.MemberType = "[]" + elt.String()
		di.MemberName = field.Names[0].Name
	} else {
		if star, ok := arraytype.Elt.(*ast.StarExpr); ok {
			if v, ok := star.X.(*ast.Ident); ok {
				di.MemberType = "[]" + v.Name
			} else {
				logrus.Error("parse arraytype err...")
			}
		} else {
			logrus.Error("parse arraytype err...")
		}
	}

	return ok
}

func (this *DiParseFields) ParseName(field *ast.Field, di *DiInject) bool {

	di.MemberName = field.Names[0].Name
	if this.ParseStarExpr(field, di) || this.ParseIdent(field, di) || this.ParseMapType(field, di) || this.ParseArrayType(field, di) {
		return true
	}
	logrus.Error("parse field error")
	return false
}
