package dimeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: _info_dto_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameInfoDto = "dimeta.FileInfoDto"

// init register load
func init() {
	registerBeanInfoDto()
}

// register FileInfoDto
func registerBeanInfoDto() {
	basedi.RegisterLoadBean(singleNameInfoDto, LoadInfoDto)
}

func FindBeanInfoDto() *FileInfoDto {
	bean, ok := basedi.FindBean(singleNameInfoDto).(*FileInfoDto)
	if !ok {
		logrus.Errorf("FindBeanInfoDto: failed to cast bean to *FileInfoDto")
		return nil
	}
	return bean

}

func LoadInfoDto() baseiface.ISingleton {
	var s = NewFileInfoDto()
	InjectInfoDto(s)
	return s

}

func InjectInfoDto(s *FileInfoDto) {

	// logrus.Debug("inject")
}
