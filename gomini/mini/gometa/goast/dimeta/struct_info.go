package dimeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/goast/diconsts"

	"gitee.com/leijmdas/gobase/gomini/mini/gometa/goast/diutils"
	"strings"
)

type StructInfo struct {
	basedto.BaseEntity

	IsBaseEntiyStruct bool `json:"isBaseEntiyStruct"`
	*DiDto

	MethodNames []string      `json:"methodNames,omitempty"`
	FuncDefines []*FuncDefine `json:"func_defines"`

	Fields []string `json:"fields"`
}

func NewStructInfo() *StructInfo {
	var si = &StructInfo{
		DiDto:       NewDiDto(),
		MethodNames: make([]string, 0),
		FuncDefines: make([]*FuncDefine, 0),
	}
	si.InitProxy(si)
	return si
}

func (this *StructInfo) NewFuncNameDefault() string {
	return "New" + this.StructName
}

func (this *StructInfo) CheckBaseEntity() bool {
	for _, v := range this.Fields {
		this.IsBaseEntiyStruct = v == diconsts.BaseEntity || v == diconsts.BaseEntitySingle
		if this.IsBaseEntiyStruct {
			return true
		}
	}
	this.IsBaseEntiyStruct = diutils.FindKey(this.StructName)
	return this.IsBaseEntiyStruct
}

func (s *StructInfo) ParsePkgName(rootdir, basepkg string) {

	s.FullPkg = strings.Replace(s.PathFile, rootdir, basepkg, -1)
	s.FullPkg = strings.Replace(s.FullPkg, "\\", "/", -1)
	var paths = strings.Split(s.FullPkg, "/")
	s.FullPkg = strings.Join(paths[0:len(paths)-2], "/") + "/" + s.PkgName

}

func (s *StructInfo) AddMethodName(methodName string) {
	s.MethodNames = append(s.MethodNames, methodName)
}

func (s *StructInfo) AddField(fieldName string) {
	s.Fields = append(s.Fields, fieldName)
}
