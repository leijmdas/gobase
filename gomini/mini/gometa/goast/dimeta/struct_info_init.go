package dimeta

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: struct_info_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameStructInfo = "dimeta.StructInfo"

// init register load
func init() {
	registerBeanStructInfo()
}

// register StructInfo
func registerBeanStructInfo() {
	basedi.RegisterLoadBean(singleNameStructInfo, LoadStructInfo)
}

func FindBeanStructInfo() *StructInfo {
	bean, ok := basedi.FindBean(singleNameStructInfo).(*StructInfo)
	if !ok {
		logrus.Errorf("FindBeanStructInfo: failed to cast bean to *StructInfo")
		return nil
	}
	return bean

}

func LoadStructInfo() baseiface.ISingleton {
	var s = NewStructInfo()
	InjectStructInfo(s)
	return s

}

func InjectStructInfo(s *StructInfo) {

	// logrus.Debug("inject")
}
