package goast

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: dimeta_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameDimetaFactroy = "goast.DimetaFactroy"

// init register load
func init() {
	registerBeanDimetaFactroy()
}

// register DimetaFactroy
func registerBeanDimetaFactroy() {
	basedi.RegisterLoadBean(singleNameDimetaFactroy, LoadDimetaFactroy)
}

func FindBeanDimetaFactroy() *DimetaFactroy {
	bean, ok := basedi.FindBean(singleNameDimetaFactroy).(*DimetaFactroy)
	if !ok {
		logrus.Errorf("FindBeanDimetaFactroy: failed to cast bean to *DimetaFactroy")
		return nil
	}
	return bean

}

func LoadDimetaFactroy() baseiface.ISingleton {
	var s = NewDiFactroy()
	InjectDimetaFactroy(s)
	return s

}

func InjectDimetaFactroy(s *DimetaFactroy) {

	// logrus.Debug("inject")
}
