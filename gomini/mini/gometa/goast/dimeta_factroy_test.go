package goast

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestDiFactoySuite struct {
	suite.Suite
	DiFactroy *DimetaFactroy
	rootdir   string
}

func (this *TestDiFactoySuite) SetupTest() {
	logrus.Info("difactry...")
	ichublog.InitLogrus()
	this.rootdir = fileutils.FindRootDir()
	this.DiFactroy = FindBeanDimetaFactroy()

	var i baseiface.ISingleton = nil
	logrus.Info(i)

}

func TestDiFactoySuites(t *testing.T) {
	suite.Run(t, new(TestDiFactoySuite))
}

// 查找所有GO文件
func (this *TestDiFactoySuite) Test001_FindAlls() {
	this.DiFactroy.FindFile("")

	logrus.Info("basedi=", jsonutils.ToJsonPretty(this.DiFactroy))
}

// AST分析所有文件
func (this *TestDiFactoySuite) Test002_ParseAll() {

	this.DiFactroy.GoastAll()
	var cf, found = this.DiFactroy.FindSome("Minipkg")
	if found {
		logrus.Info(cf.ToString())
	} else {
		logrus.Error("\r\n!found ", found)
	}
}
