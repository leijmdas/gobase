package diutils

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: di_utils_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-12 10:22:13)
	@Update 作者: leijmdas@163.com 时间(2024-05-12 10:22:13)

* *********************************************************/

const singleNameDiUtils = "diutils.DiUtils"

// init register load
func init() {
	registerBeanDiUtils()
}

// register DiUtils
func registerBeanDiUtils() {
	basedi.RegisterLoadBean(singleNameDiUtils, LoadDiUtils)
}

func FindBeanDiUtils() *DiUtils {
	bean, ok := basedi.FindBean(singleNameDiUtils).(*DiUtils)
	if !ok {
		logrus.Errorf("FindBeanDiUtils: failed to cast bean to *DiUtils")
		return nil
	}
	return bean

}

func LoadDiUtils() baseiface.ISingleton {
	var s = NewDiUtils()
	InjectDiUtils(s)
	return s

}

func InjectDiUtils(s *DiUtils) {

	// logrus.Debug("inject")
}
