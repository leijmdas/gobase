package index

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: esindex_factroy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameEsindexFactroy = "index.EsindexFactroy"

// init register load
func init() {
	registerBeanEsindexFactroy()
}

// register EsindexFactroy
func registerBeanEsindexFactroy() {
	basedi.RegisterLoadBean(singleNameEsindexFactroy, LoadEsindexFactroy)
}

func FindBeanEsindexFactroy() *EsindexFactroy {
	bean, ok := basedi.FindBean(singleNameEsindexFactroy).(*EsindexFactroy)
	if !ok {
		logrus.Errorf("FindBeanEsindexFactroy: failed to cast bean to *EsindexFactroy")
		return nil
	}
	return bean

}

func LoadEsindexFactroy() baseiface.ISingleton {
	var s = NewEsIndexFactroy()
	InjectEsindexFactroy(s)
	return s

}

func InjectEsindexFactroy(s *EsindexFactroy) {

	// logrus.Debug("inject")
}
