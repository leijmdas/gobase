package index

import (
	"context"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	ichubelastic "gitee.com/leijmdas/gobase/goconfig/common/goelastic"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade/indexentity"
	"github.com/sirupsen/logrus"
)

const mappings = "mappings"
const properties = "properties"

type Keyword struct {
	IgnoreAbove int    `json:"ignore_above"`
	Type        string `json:"type"`
}
type Fields struct {
	Keyword Keyword `json:"keyword,omitempty"`
}

func NewFields() *Fields {
	return &Fields{
		Keyword: Keyword{
			IgnoreAbove: 256,
			Type:        "keyword",
		},
	}
}

type MappingField struct {
	Fields *Fields `json:"fields,omitempty"`
	Type   string  `json:"type"`
}

func NewMappingField() *MappingField {
	return &MappingField{}
}

type PropertiesDto map[string]*MappingField

type EsindexMetadata struct {
	basedto.BaseEntity

	IndexName     string                      `json:"index_name"`
	Properties    PropertiesDto               `json:"-"`
	EsClient      *ichubelastic.ElasticClient `json:"-"`
	FieldTypeInfo map[string]string           `json:"field_type_info"`
}

func NewIndexMetadata() *EsindexMetadata {
	return &EsindexMetadata{
		Properties:    make(PropertiesDto),
		FieldTypeInfo: make(map[string]string),
		EsClient:      gocontext.FindBeanGoClientFactroy().IniEsClient().Esclient(),
	}
}
func (this *EsindexMetadata) SetIndexName(index string) {
	this.IndexName = index
}

func (this *EsindexMetadata) AddField(name, Type string) {
	this.Properties[name] = NewMappingField()
	this.Properties[name].Type = Type
	if Type == "text" {
		this.Properties[name].Fields = NewFields()
	}

}

func (this *EsindexMetadata) ToMapProp() map[string]any {

	var result = map[string]any{}
	result[properties] = this.Properties
	return result
}

func (this *EsindexMetadata) ToMapping() map[string]any {

	var result = map[string]any{}
	result[mappings] = this.ToMapProp()
	return result
}

func (this *EsindexMetadata) ToMappings() map[string]any {

	var result = map[string]any{}
	result[this.IndexName] = this.ToMapping()
	golog.Info(jsonutils.ToJsonPretty(result))
	return result
}

func (this *EsindexMetadata) ToMappingStr() string {

	var s = jsonutils.ToJsonPretty(this.ToMapping())
	logrus.Info(s)
	return s
}

func (this *EsindexMetadata) getMapping(indexName string) (map[string]any, error) {

	this.IndexName = indexName
	var do, err = this.EsClient.Client().GetMapping().Index(indexName).Type().Do(context.Background())

	return do, err

}
func (this *EsindexMetadata) ParsedType(indexName string) (map[string]any, error) {

	var mapping, err = this.getMapping(indexName)
	if err != nil {
		return nil, err
	}
	var mm = mapping[indexName]
	var ms = mm.(map[string]any)["mappings"]
	var props = ms.(map[string]any)["properties"]
	for k, v := range props.(map[string]any) {
		this.FieldTypeInfo[k] = v.(map[string]any)["type"].(string)
	}
	logrus.Info(jsonutils.ToJsonPretty(this.FieldTypeInfo))
	return props.(map[string]any), err

}
func (this *EsindexMetadata) IfLong(field string) bool {
	return this.FieldTypeInfo[field] == "long"

}
func (this *EsindexMetadata) IfDouble(field string) bool {
	return this.FieldTypeInfo[field] == "long"

}
func (this *EsindexMetadata) FindFieldType(field string) string {
	return this.FieldTypeInfo[field]

}

func (self *EsindexMetadata) MetaIndexCode(indexName string, sortByName bool) (string, error) {
	var dto, err = self.MetaIndex(indexName, sortByName)
	if err != nil {
		return "", err
	}
	return dto.ToGocode(sortByName), nil
}
func (self *EsindexMetadata) MetaIndexCodeFile(indexName string, sortByName bool) (string, error) {
	var dto, err = self.MetaIndex(indexName, sortByName)
	if err != nil {
		return "", err
	}
	return dto.ToGocodeFile(sortByName)
}
func (self *EsindexMetadata) MetaIndex(indexName string, sortByName bool) (*indexentity.IndexEntity, error) {
	var props, err = self.ParsedType(indexName)
	if err != nil {
		golog.Error(err)
		return nil, err
	}
	var dto = indexentity.NewIndexEntity()
	dto.EntityName = indexName
	for fieldName, indexType := range props {
		if typeinf, ok := indexType.(map[string]any); ok {
			dto.AppendField(fieldName, typeinf["type"].(string))
		}
	}
	return dto, nil
}
