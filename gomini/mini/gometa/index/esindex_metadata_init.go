package index

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: esindex_metadata_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameEsindexMetadata = "index.EsindexMetadata"

// init register load
func init() {
	registerBeanEsindexMetadata()
}

// register EsindexMetadata
func registerBeanEsindexMetadata() {
	basedi.RegisterLoadBean(singleNameEsindexMetadata, LoadEsindexMetadata)
}

func FindBeanEsindexMetadata() *EsindexMetadata {
	bean, ok := basedi.FindBean(singleNameEsindexMetadata).(*EsindexMetadata)
	if !ok {
		logrus.Errorf("FindBeanEsindexMetadata: failed to cast bean to *EsindexMetadata")
		return nil
	}
	return bean

}

func LoadEsindexMetadata() baseiface.ISingleton {
	var s = NewIndexMetadata()
	InjectEsindexMetadata(s)
	return s

}

func InjectEsindexMetadata(s *EsindexMetadata) {

	// logrus.Debug("inject")
}
