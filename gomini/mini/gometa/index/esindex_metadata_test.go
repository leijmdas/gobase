package index

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/golog/ichublog"
	"testing"
)

func init() {
	ichublog.InitLogrus()
	metadata = FindBeanEsindexMetadata()
}

var metadata = NewIndexMetadata()

func Test0001_addmapping(t *testing.T) {
	metadata.SetIndexName("ichub_sys_dept")
	metadata.AddField("id", "int")
	metadata.AddField("desc", "text")

	golog.Info(jsonutils.ToJsonPretty(metadata.ToMappings()))
}

func Test002_ParseMapping(t *testing.T) {
	var _, err = metadata.ParsedType("ichub_sys_dept")
	if err != nil {
		goutils.Error(err)
	}
	var t1 = metadata.FindFieldType("order_nm")
	golog.Info(t1, "\n field order_nm is long ?", metadata.IfLong("order_nm"))
}
func Test004_ParseMapping(t *testing.T) {
	var stru, err = FindBeanEsindexMetadata().MetaIndexCode("release_service_website", true)

	golog.Info(stru, err)

}
