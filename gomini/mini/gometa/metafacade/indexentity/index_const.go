package indexentity

const bodyEntity = `package esentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/goweb/common/webclient/eswebclient/webfacade"
	"github.com/gogf/gf/v2/util/gconv"
	jsoniter "github.com/json-iterator/go"
	"github.com/olivere/elastic/v7"
	"time"
)



/*
@Title    文件名称: {{file}}.go
@Description  描述: 统一返回结构

@Author  作者: raymond@163.com  时间({{date}})
@Update  作者: raymond@163.com  时间({{date}})
*/

type  {{struName}}Es struct {
	 basedto.BaseEntity 

{{buildStruBody}} 

}

type  {{struName}}EsDto struct {
	 basedto.BaseEntity 

{{buildStruBodyDto}} 

}
`

const funcEntity = `
func NewContactShopEs() *ContactShopEs {
	return &ContactShopEs{}
}
  
func (self *ContactShopEs) PkeyName() string {
	return "id"
}
func (self *ContactShopEs) PkeyValue() string {
	return gconv.String(self.Id)
}
func (self *ContactShopEs) TableName() string {
	return "{{index_name_suffix}}"
}
func (self *ContactShopEs) IndexName() string {
	return self.TableName()
}

func (self *ContactShopEs) GetMapping() string {
	return "{}"
}
func (self *ContactShopEs) Mapping() string {
	return self.GetMapping()
}
func (self *ContactShopEs) Unmarshal(data []byte) error {
	return jsoniter.Unmarshal(data, self)
}

func (self ContactShopEs) IndexID() string {
	return self.PkeyName()
}

func (self ContactShopEs) IndexAliasName() string {
	return self.IndexName() + "_alias"
}
 

func (self *ContactShopEs) EsFill(p any) error {
	var err = gconv.Struct(p, self)
	if err != nil {
		golog.Error("ContactShopEs err:", err)
	}
	return err
}

func (self *ContactShopEs) Init() {
}
func (self *ContactShopEs) Shutdown() {

}

func NewWebFacadeContactShopEsOf (q elastic.Query) *webfacade.WebFacade[*ContactShopEs] {
	var facade = webfacade.DefaultOf[*ContactShopEs](q)
	facade.Query(q)
	return facade
}

func NewWebFacadeContactShopEs () *webfacade.WebFacade[*ContactShopEs] {
	return  webfacade.Default[*ContactShopEs]() 
 
}

func (self *ContactShopEs) DefaultOf(q elastic.Query) *webfacade.WebFacade[*ContactShopEs] {
	return NewWebFacadeContactShopEsOf(q)

}
func (self *ContactShopEs) Default() *webfacade.WebFacade[*ContactShopEs] {
	return NewWebFacadeContactShopEs()
}

 
`
