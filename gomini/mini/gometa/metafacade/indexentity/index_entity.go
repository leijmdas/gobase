package indexentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade/metaentity"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade/metaentity/metaistrategy"
	"strings"
)

type IndexEntity struct {
	*metaentity.EntityDto
}

func NewIndexEntity() *IndexEntity {
	var entity = &IndexEntity{
		EntityDto: metaentity.NewEntityDto(),
	}
	entity.StrategyType = metaistrategy.MetaStrategyIndex
	entity.SetFuncEntity(funcEntity)
	entity.SetBodyEntity(bodyEntity)
	entity.InitProxy(entity)
	return entity
}

func (self *IndexEntity) ToGocode(sortByName bool) string {
	if sortByName {
		self.SortByName()
	} else {
		self.SortByType()
	}
	self.StruNameVar = self.ToGoEntityName()
	self.FileVar = self.ToGoEntityNameEsCase()
	self.BodyVar = self.buildBody()
	self.BodyDtoVar = self.buildBodyDto()
	self.FuncVar = self.buildFunc()

	return self.BuildCode()

}
func (self *IndexEntity) buildFunc() string {
	var entityName = self.ToGoEntityNameEs()
	//var funcs = strings.ReplaceAll(self.FuncEntity(), "ContactShopEs", entityName)
	var funcs = strings.ReplaceAll(self.FuncEntity(), "ContactShopEs", entityName)
	funcs = strings.ReplaceAll(funcs, "contact_shop", entityName)

	entityName = self.ToGoEntityName()
	funcs = strings.ReplaceAll(funcs, "{{index_name_suffix}}", self.EntityName) //stringutils.Camel2Case(entityName))

	return funcs
}
func (self *IndexEntity) FullName() string {

	self.FileName = self.DataCode + "esentity/" + self.ToGoEntityNameEsCase() + ".go"
	return self.FileName
}
func (self *IndexEntity) ToGocodeFile(sortByName bool) (string, error) {

	var code = self.ToGocode(sortByName)
	err := self.Write2File(self.FullName(), code)
	golog.Info(self.FullName())
	return code, err
}

func (self *IndexEntity) buildBody() string {
	var fields = []string{}
	for _, f := range self.Fields {
		var field = f.ToGoStruField()
		fields = append(fields, field)
	}
	return strings.Join(fields, "\r\n")
}
func (self *IndexEntity) buildBodyDto() string {
	var fields = []string{}
	for _, f := range self.Fields {
		var field = f.ToGoStruFieldDto()
		fields = append(fields, field)
	}
	return strings.Join(fields, "\r\n")
}
