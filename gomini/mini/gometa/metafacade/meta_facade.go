package metafacade

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metafactroy"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/index"
)

type MetaFacade struct {
	basedto.BaseEntitySingle
}

func NewMetaFacade() *MetaFacade {
	return &MetaFacade{}
}

func (self *MetaFacade) MetaEs(indexName string) error {
	var _, err = index.FindBeanEsindexMetadata().MetaIndexCodeFile(indexName, true)
	return err
}
func (self *MetaFacade) MetaEsSortType(indexName string) error {
	var _, err = index.FindBeanEsindexMetadata().MetaIndexCodeFile(indexName, false)
	return err
}
func (self *MetaFacade) MetaDb(tableName string) error {

	var _, err = metafactroy.FindBeanMetaFactroy().MetaTableCodeFile(tableName)
	return err
}
