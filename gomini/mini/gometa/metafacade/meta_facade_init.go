package metafacade

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"git.ichub.com/general/webcli120/goconfig/ichublog/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: meta_facade_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2025-01-18 18:39:33
	@Update  作者:    leijmdas@163.com  时间: 2025-01-18 18:39:33
*/

var singleNameMetaFacade = "*metafacade.MetaFacade-132c6ea9-ba7d-47cf-bb01-55862abc4da5"

// init register load
func init() {
	registerBeanMetaFacade()
}

// register MetaFacade
func registerBeanMetaFacade() {
	err := basedi.RegisterLoadBean(singleNameMetaFacade, LoadMetaFacade)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanMetaFacade
func FindBeanMetaFacade() *MetaFacade {

	if bean, ok := basedi.FindBeanOk(singleNameMetaFacade); ok {
		return bean.(*MetaFacade)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadMetaFacade() baseiface.ISingleton {
	var inst = NewMetaFacade()
	InjectMetaFacade(inst)
	return inst

}

func InjectMetaFacade(s *MetaFacade) {

}
