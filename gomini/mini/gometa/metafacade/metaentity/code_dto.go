package metaentity

import (
	"strings"
	"time"
)

type CodeDto struct {
	bodyEntity string
	funcEntity string
	FileName   string

	FileVar     string
	DateVar     string
	BodyVar     string
	BodyDtoVar  string
	StruNameVar string
	FuncVar     string
}

func (self *CodeDto) BuildCode() string {

	var code = strings.ReplaceAll(self.BodyEntity(), "{{struName}}", self.StruNameVar)
	code = strings.ReplaceAll(code, "{{file}}", self.FileVar)
	code = strings.ReplaceAll(code, "{{date}}", time.Now().Format(time.DateTime))
	code = strings.ReplaceAll(code, "{{buildStruBody}}", self.BodyVar)
	code = strings.ReplaceAll(code, "{{buildStruBodyDto}}", self.BodyDtoVar)
	code = strings.ReplaceAll(code, "{{gormMinusFlag}}", "`gorm:\"-\"`")

	return code + "\r\n" + self.FuncVar

}
func (e *CodeDto) BodyEntity() string {
	return e.bodyEntity
}

func (e *CodeDto) SetBodyEntity(bodyEntity string) {
	e.bodyEntity = bodyEntity
}

func (e *CodeDto) FuncEntity() string {
	return e.funcEntity
}

func (e *CodeDto) SetFuncEntity(funcEntity string) {
	e.funcEntity = funcEntity
}
