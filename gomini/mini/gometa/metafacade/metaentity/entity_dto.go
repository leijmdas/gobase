package metaentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedata"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"sort"
)

const (
	SortByType = "type"
	SortByName = "name"
)

type EntityDto struct {
	basedto.BaseEntity

	EntityName string
	Fields     []*EntityField

	PkeyField   string
	PkeyGofield string
	PkeyType    string

	SortBy string
	*basedata.DataPath

	StrategyType int
	CodeDto
}

func NewEntityDto() *EntityDto {
	var entity = &EntityDto{
		Fields:   make([]*EntityField, 0),
		DataPath: basedata.FindBeanDataPath(),
		SortBy:   SortByName,
	}

	entity.InitProxy(entity)
	return entity

}

func (self *EntityDto) ToGoEntityName() string {

	var entityName = self.EntityName

	return stringutils.Case2Camel(entityName)
}

func (self *EntityDto) ToGoEntityNameEs() string {

	return self.ToGoEntityName() + "Es"
}
func (self *EntityDto) ToGoEntityNameEsCase() string {

	return stringutils.Camel2Case(self.ToGoEntityNameEs())
}
func (self *EntityDto) SortByType() {
	self.SortBy = SortByType
}
func (self *EntityDto) SortByName() {
	self.SortBy = SortByName
}
func (self *EntityDto) Write2File(file string, code string) error {
	golog.Info(file, " EntityDtoc code:", code)
	return fileutils.WriteBytesFile(file, []byte(code))

}
func (self *EntityDto) SortFields() {
	var funSort = func(i, j int) bool {
		return self.Fields[i].SortField(self.SortBy, self.Fields[j].EntityGofield)
	}
	sort.Slice(self.Fields, funSort)
}
func (self *EntityDto) NewField(fileldName, fieldType string) *EntityField {

	return NewEntityField(self.StrategyType, fileldName, fieldType)
}
func (self *EntityDto) AppendField(fileldName, fieldType string) *EntityField {

	var field = self.NewField(fileldName, fieldType)
	field.ToGo()
	self.Fields = append(self.Fields, field)
	return field
}
