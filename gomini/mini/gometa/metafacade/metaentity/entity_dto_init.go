package metaentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	//"git.ichub.com/general/webcli120/goconfig/ichublog/golog"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: entity_dto_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    www.ichub.com

	@Author  作者:    leijmdas@163.com  时间: 2025-01-18 18:39:33
	@Update  作者:    leijmdas@163.com  时间: 2025-01-18 18:39:33
*/

var singleNameEntityDto = "*metaentity.EntityDto-64381682-9706-4120-aae4-55483ffd53ab"

// init register load
func init() {
	registerBeanEntityDto()
}

// register EntityDto
func registerBeanEntityDto() {
	err := basedi.RegisterLoadBean(singleNameEntityDto, LoadEntityDto)
	if err != nil {
		logrus.Error("register bean error!", err)
	}
}

// FindBeanEntityDto
func FindBeanEntityDto() *EntityDto {

	if bean, ok := basedi.FindBeanOk(singleNameEntityDto); ok {
		return bean.(*EntityDto)
	}

	logrus.Error("not find bean!")
	return nil
}

func LoadEntityDto() baseiface.ISingleton {
	var inst = NewEntityDto()
	InjectEntityDto(inst)
	return inst

}

func InjectEntityDto(s *EntityDto) {

}
