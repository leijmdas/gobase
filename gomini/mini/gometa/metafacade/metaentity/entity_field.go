package metaentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade/metaentity/metaistrategy"
)

type EntityField struct {
	FieldName string `json:"field_name"`
	FieldType string `json:"field_type"`
	metaistrategy.MetaIStrategy
	GormTag string `json:"gorm_tag"`
	EntityGofield
}

func NewEntityField(strategyType int, fieldName, fieldType string) *EntityField {
	var entityField = &EntityField{
		FieldName: fieldName,
		FieldType: fieldType,
	}
	entityField.SelectStrategy(strategyType)
	return entityField
}

func (self *EntityField) ToGo() {
	self.GoType = self.ToGoType()
	self.GoField = stringutils.Case2Camel(self.FieldName)

}
func (self *EntityField) SelectStrategy(strategy int) *EntityField {

	if strategy == metaistrategy.MetaStrategyTable {
		self.MetaIStrategy = NewTableStrategy(self)
	} else {
		self.MetaIStrategy = NewIndexStrategy(self)
	}
	self.ToGo()
	return self
}
func (self *EntityField) IfInt64() bool {
	return self.GoType == "int64"
}
func (self *EntityField) IfFloat64() bool {
	return self.GoType == "float64"
}
