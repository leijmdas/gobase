package metaentity

import (
	"strings"
)

type EntityGofield struct {
	GoField string `json:"go_field"`
	GoType  string `json:"go_type"`
}

func (self EntityGofield) SortField(sortBy string, that EntityGofield) bool {
	if i := strings.Compare(self.GoType, that.GoType); i != 0 {
		if sortBy == SortByType {
			return i > 0
		}
		return i <= 0
	}
	if sortBy == SortByType {
		return strings.Compare(self.GoField, that.GoField) < 0
	}
	return strings.Compare(self.GoField, that.GoField) >= 0

}
