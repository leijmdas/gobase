package metaentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"strings"
)

type IndexStrategy struct {
	*EntityField
}

func NewIndexStrategy(field *EntityField) *IndexStrategy {
	return &IndexStrategy{EntityField: field}
}

func (self *IndexStrategy) ToGoType() string {
	switch self.FieldType {

	case "boolean":
		return "bool"
	case "date":
		return "time.Time"
	case "date_nanos":
		return "time.Time"

	case "object":
		return "text"
	case "text", "keyword":
		return "string"

	case "integer":
		return "int32"
	case "long":
		return "int64"
	case "byte":
		return "int8"
	case "short":
		return "int16"

	case "float":
		return "float32"
	case "double":
		return "float64"

	case "binary":
		return "string"
	}
	return "string"
}

func (self *IndexStrategy) ToGoTag() string {
	if self.IfInt64() {
		return "`" + `json:"` + self.FieldName + `,string"` + "`"
	}
	return "`" + `json:"` + self.FieldName + `"` + "`"
}

func (self *IndexStrategy) ToGoStruField() string {
	var keys = []string{
		"\t",
		stringutils.Case2Camel(self.FieldName),
		self.ToGoType(),
		self.ToGoTag(),
	}
	return strings.Join(keys, " ")
}
func (self *IndexStrategy) ToGoStruFieldDto() string {
	var keys = []string{
		"\t",
		stringutils.Case2Camel(self.FieldName),
		self.ToGoType(),
		self.ToGoTag(),
	}
	return strings.Join(keys, " ")
}
