package metaistrategy

const (
	MetaStrategyIndex = iota
	MetaStrategyTable
)

type MetaIStrategy interface {
	ToGo()
	ToGoType() string
	ToGoTag() string
	ToGoStruField() string
	ToGoStruFieldDto() string
}
