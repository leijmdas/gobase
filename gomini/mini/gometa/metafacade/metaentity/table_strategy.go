package metaentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metacontext"
	"strings"
)

type TableStrategy struct {
	*EntityField
}

func NewTableStrategy(f *EntityField) *TableStrategy {
	return &TableStrategy{EntityField: f}
}

func (self *TableStrategy) ToGoType() string {
	var ctx = metacontext.FindBeanMetadataContext()
	return ctx.FindGoType(self.FieldType)
}
func (self *TableStrategy) ToGoStruField() string {
	var keys = []string{
		"\t",
		stringutils.Case2Camel(self.FieldName),
		self.ToGoType(),
		self.ToGoTag(),
	}
	return strings.Join(keys, " ")
}
func (self *TableStrategy) ToGoStruFieldDto() string {
	var keys = []string{
		"\t",
		stringutils.Case2Camel(self.FieldName),
		self.ToGoType(),
		self.ToGoTagDto(),
	}
	return strings.Join(keys, " ")
}
func (self *TableStrategy) ToGoTag() string {
	if self.IfInt64() {
		return "`" + `json:"` + self.FieldName + `,string" ` + self.GormTag + "`"
	}
	return "`" + `json:"` + self.FieldName + `" ` + self.GormTag + "`"
}
func (self *TableStrategy) ToGoTagDto() string {
	if self.IfInt64() {
		return "`" + `json:"` + self.FieldName + `,string"` + "`"
	}
	return "`" + `json:"` + self.FieldName + `"` + "`"
}
