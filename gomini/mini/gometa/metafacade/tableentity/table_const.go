package tableentity

const bodyEntity = `package dbentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/goweb/godb/generaldao"
	"github.com/jinzhu/gorm"
	"time"
)

/*
@Title    文件名称: {{file}}.go
@Description  描述: 统一返回结构

@Author  作者: raymond@163.com  时间({{date}})
@Update  作者: raymond@163.com  时间({{date}})
*/

type  {{struName}} struct {
	basedto.BaseEntity {{gormMinusFlag}}

{{buildStruBody}} 

}

type  {{struName}}Dto struct {
	 basedto.BaseEntity
{{buildStruBodyDto}} 

}
`

const funcEntity = `
func NewContactShop() *ContactShop {
	return &ContactShop{}
}

func (self *ContactShop) PkeyName() string {
	return "{{PkeyField}}"//"id"
}
func (self *ContactShop) PkeyValue() {{PkeyType}}  {
	return self.{{PkeyGofield}}//self.Id
}
func (self *ContactShop) TableName() string {
	return "contact_shop"
}

func (self *ContactShop) NewDao()  * generaldao.BaseDao[{{PkeyType}}, *ContactShop] {
	return  generaldao.NewBaseDao[{{PkeyType}}, *ContactShop]()
}

func NewDaoContactShop()  * generaldao.BaseDao[{{PkeyType}}, *ContactShop] {
	return  generaldao.NewBaseDao[{{PkeyType}}, *ContactShop]()
}
func (self *ContactShop) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(self).Error
	//self.execComment(db)

	return err
}


`
