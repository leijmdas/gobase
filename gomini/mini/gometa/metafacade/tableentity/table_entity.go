package tableentity

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/stringutils"
	"gitee.com/leijmdas/gobase/goconfig/common/golog"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metacontext"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/db/metadb"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade/metaentity"
	"gitee.com/leijmdas/gobase/gomini/mini/gometa/metafacade/metaentity/metaistrategy"
	"strings"
)

type TableEntity struct {
	*metaentity.EntityDto
}

func NewTableEntity() *TableEntity {

	var entity = &TableEntity{
		EntityDto: metaentity.NewEntityDto(),
	}
	entity.StrategyType = metaistrategy.MetaStrategyTable
	entity.SetFuncEntity(funcEntity)
	entity.SetBodyEntity(bodyEntity)
	entity.InitProxy(entity)
	return entity
}
func (self *TableEntity) FullName() string {

	self.FileName = self.DataCode + "dbentity/" + self.EntityName + ".go"
	return self.FileName
}
func (self *TableEntity) ToGocodeFile(sortByName bool) (string, error) {

	var code = self.ToGocode(sortByName)
	err := self.Write2File(self.FullName(), code)
	golog.Info("ToGocodeFile FullName:", self.FullName())
	return code, err
}

func (self *TableEntity) buildBody() string {
	var fields = []string{}
	for _, f := range self.Fields {
		var field = f.ToGoStruField()
		fields = append(fields, field)
	}
	return strings.Join(fields, "\r\n")
}
func (self *TableEntity) buildBodyDto() string {
	var fields = []string{}
	for _, f := range self.Fields {
		var field = f.ToGoStruFieldDto()
		fields = append(fields, field)
	}
	return strings.Join(fields, "\r\n")
}

func (self *TableEntity) ToGocode(sortByname bool) string {
	self.StruNameVar = self.ToGoEntityName()
	self.FileVar = self.EntityName
	self.BodyVar = self.buildBody()
	self.BodyDtoVar = self.buildBodyDto()
	self.FuncVar = self.buildFunc()

	return self.BuildCode()

}
func (self *TableEntity) buildFunc() string {
	var entityName = self.ToGoEntityName()
	var funcs = strings.ReplaceAll(self.FuncEntity(), "ContactShop", entityName)
	funcs = strings.ReplaceAll(funcs, "contact_shop", self.EntityName)
	funcs = strings.ReplaceAll(funcs, "{{PkeyField}}", self.PkeyField)
	funcs = strings.ReplaceAll(funcs, "{{PkeyGofield}}", self.PkeyGofield)
	funcs = strings.ReplaceAll(funcs, "{{PkeyType}}", self.PkeyType)

	return funcs
}
func (self *TableEntity) ToGoType(fieldType string) string {
	var ctx = metacontext.FindBeanMetadataContext()
	return ctx.FindGoType(fieldType)
}
func (self *TableEntity) InitEntity(table string, pkey *metadb.MetadataPkinfo) {
	self.EntityName = table
	self.PkeyField = pkey.ColName
	self.PkeyGofield = stringutils.Case2Camel(self.PkeyField)
	self.PkeyType = self.ToGoType(pkey.TypeName)
}
