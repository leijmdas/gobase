package goperfstat

const (
	DomainGeneral = "general" //通用域 general

	//性能统计
	TopicGeneralSyncSTAT = "GeneralEsSync.SYSTEM.STAT"
	// 连接信息
	Topic_DOMAIN_GENEAL_ASyncCONN     = "GeneralEsSync.SYSTEM.CONN"
	Topic_DOMAIN_GENEAL_ASync_DISCONN = "GeneralEsSync.SYSTEM.DISCONN"
	//性能统计通知消息
	Topic_Async_Notify_PRE = "GeneralDefault.*"
	Topic_Async_NotifyCMS  = "GeneralDefault.CMS"
)
