package goperfstat

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/gocache/gobigcache"

	"strings"
	"sync"
	"time"
)

const STAT_KEY_ALL = "TOPIC_ALL"
const STAT_LOG_INTERVAL time.Duration = 5 //* time.Second

// 未来增加统计条件

var ticker *time.Ticker // time.NewTicker(time.Second * STAT_LOG_INTERVAL)
var once sync.Once

type GoperfStat struct {
	basedto.BaseEntitySingle

	StatMap  map[string]*GostatInfo
	start    time.Time
	channel  chan *GostatInfo
	channelB chan *GostatInfo

	Lock sync.RWMutex
}

func TimerLog() {
	for {
		select {
		case <-ticker.C:
			FindBeanGoperfStat().safeStatLog()
		}
	}
}
func NewGoperfStat() *GoperfStat {
	var s = &GoperfStat{
		StatMap:  make(map[string]*GostatInfo),
		start:    time.Now(),
		channel:  make(chan *GostatInfo, 200),
		channelB: make(chan *GostatInfo, 200),
	}
	s.StatMap[STAT_KEY_ALL] = FindBeanGostatInfo()
	s.InitProxy(s)
	return s
}

func (self *GoperfStat) StartStats() {
	fmt.Println("start goperfstat")
	once.Do(func() {
		ticker = time.NewTicker(time.Second * STAT_LOG_INTERVAL)
		go TimerLog()
	})
	go func() {
		for {
			for g := range self.channel {
				self.Stat(g)
			}
			time.Sleep(time.Millisecond)
		}
	}()
	go func() {
		for {
			for g := range self.channelB {
				self.Stat(g)
			}
			time.Sleep(time.Millisecond)
		}
	}()
}
func (self *GoperfStat) Trigger(topic string, ok bool, t int64) {
	var gostatInfo = FindBeanGostatInfo()
	gostatInfo.Topic = topic
	gostatInfo.MsgType = "esapi"
	gostatInfo.Times = t
	if ok {
		gostatInfo.Oknum = 1
	} else {
		gostatInfo.Failnum = 1
	}
	if t%2 == 0 {
		self.channel <- gostatInfo
	} else {
		self.channelB <- gostatInfo
	}
}

func (self *GoperfStat) TopicType(topic string) (string, error) {
	if topic == "" {
		return "", errors.New("invalid topic: empty!")
	}
	ss := strings.Split(topic, ".")
	if len(ss) < 2 {
		return "", errors.New("invalid topic format!")
	}
	return ss[0], nil
}

func (self *GoperfStat) Stat(g *GostatInfo) error {
	if g.Topic == TopicGeneralSyncSTAT {
		return nil
	}
	self.Lock.Lock()
	defer self.Lock.Unlock()
	var topic = g.Topic
	topicType, err := self.TopicType(topic)
	if err != nil {
		goutils.Error(err)
		return err
	}
	// Use LoadOrStore for atomic insert or update
	if _, ok := self.StatMap[topic]; !ok {
		self.StatMap[topic] = FindBeanGostatInfo()
	}
	if _, ok := self.StatMap[topicType]; !ok {
		self.StatMap[topicType] = FindBeanGostatInfo()
	}

	self.StatMap[topic].Stat(topic, g.Oknum == 1, g.Times)
	self.StatMap[topicType].Stat(topicType, g.Oknum == 1, g.Times)
	self.StatMap[STAT_KEY_ALL].Stat(STAT_KEY_ALL, g.Oknum == 1, g.Times)
	return nil
}

func (self *GoperfStat) safeStatLog() {

	self.Lock.RLock()
	defer self.Lock.RUnlock()
	for k, v := range self.StatMap {
		if v.change {
			v.change = false
			goutils.Stat("performstat: ", k, "=>", v)
		}
	}

}
func (self *GoperfStat) FindStatMap() map[string]*GostatInfo {
	self.Lock.RLock()
	defer self.Lock.RUnlock()
	var json, _ = jsonutils.ToJsonBytes(self.StatMap)
	gobigcache.Set(BIGCACHE_KEY, json)
	var mapStat = make(map[string]*GostatInfo)
	for k, v := range self.StatMap {
		mapStat[k] = v
	}
	return mapStat
}
func (self *GoperfStat) ToStatInf() string {

	self.Lock.RLock()
	defer self.Lock.RUnlock()

	return jsonutils.ToJsonPretty(self.StatMap)
}

func (self *GoperfStat) ToStat() map[string]*GostatInfo {

	self.Lock.RLock()
	defer self.Lock.RUnlock()

	var StatMap = map[string]*GostatInfo{}
	jsonutils.FromJson(jsonutils.ToJsonStr(self.StatMap), &StatMap)
	return StatMap
}
