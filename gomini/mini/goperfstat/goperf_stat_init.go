package goperfstat

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: goperf_stat_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGoperfStat = "goperfstat.GoperfStat"

// init register load
func init() {
	registerBeanGoperfStat()
}

// register GoperfStat
func registerBeanGoperfStat() {
	basedi.RegisterLoadBean(singleNameGoperfStat, LoadGoperfStat)
}

func FindBeanGoperfStat() *GoperfStat {
	bean, ok := basedi.FindBean(singleNameGoperfStat).(*GoperfStat)
	if !ok {
		logrus.Errorf("FindBeanGoperfStat: failed to cast bean to *GoperfStat")
		return nil
	}
	return bean

}

func LoadGoperfStat() baseiface.ISingleton {
	var s = NewGoperfStat()
	InjectGoperfStat(s)
	return s

}

func InjectGoperfStat(s *GoperfStat) {

	// logrus.Debug("inject")
}
