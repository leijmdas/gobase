package goperfstat

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
)

const BIGCACHE_KEY = "bigcache_stat"

const STAT_MSG_TYPE_NATS = "nats"
const STAT_MSG_TYPE_ES = "es"

type GostatInfo struct {
	basedto.BaseEntity

	Domain  string `json:"domain"`
	MsgType string `json:"msgtype"`
	Topic   string `json:"topic"`

	Total int64 `json:"total"`
	Times int64
	Max   int64
	Min   int64
	Avg   int64

	Oknum   int64 `json:"oknum"`
	Failnum int64 `json:"failnum"`

	change bool `json:"change"`
}

func NewGostatInfo() *GostatInfo {
	return &GostatInfo{

		MsgType: STAT_MSG_TYPE_NATS,
		Domain:  DomainGeneral,

		change: false,
	}
}

func (self *GostatInfo) Stat(topic string, ok bool, t int64) {
	self.Topic = topic
	if !ok {
		self.Failnum += 1
	} else {
		self.Oknum += 1
	}

	self.Total += 1
	self.Times += t
	if self.Max < t {
		self.Max = t
	}
	if self.Min > t || self.Min == 0 {
		self.Min = t
	}
	if self.Total > 0 {
		self.Avg = self.Times / self.Total
	}
	self.change = true
}
