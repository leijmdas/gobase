package goperfstat

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gostat_info_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGostatInfo = "goperfstat.GostatInfo"

// init register load
func init() {
	registerBeanGostatInfo()
}

// register GostatInfo
func registerBeanGostatInfo() {
	basedi.RegisterLoadBean(singleNameGostatInfo, LoadGostatInfo)
}

func FindBeanGostatInfo() *GostatInfo {
	bean, ok := basedi.FindBean(singleNameGostatInfo).(*GostatInfo)
	if !ok {
		logrus.Errorf("FindBeanGostatInfo: failed to cast bean to *GostatInfo")
		return nil
	}
	return bean

}

func LoadGostatInfo() baseiface.ISingleton {
	var s = NewGostatInfo()
	InjectGostatInfo(s)
	return s

}

func InjectGostatInfo(s *GostatInfo) {

	// logrus.Debug("inject")
}
