package main

import (
	"fmt"
	goperfstat2 "gitee.com/leijmdas/gobase/gomini/mini/goperfstat"
)

func main() {
	// 启动统计
	goperfstat := goperfstat2.FindBeanGoperfStat()
	goperfstat.StartStats()
	goperfstat2.FindBeanGoperfStat().Trigger("XXX.xx", true, 0)

	fmt.Println("start perf stat!")

}
