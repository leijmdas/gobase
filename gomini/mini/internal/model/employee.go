package model

/*
   @Title    文件名称: Employeemodel.gomodel
   @Description  描述: 服务Employeemodel

   @Author  作者: leijianming@163.com  时间(2024-03-26 16:26:57)
   @Update  作者: leijianming@163.com  时间(2024-03-26 16:26:57)

*/

import (
	"encoding/json"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"github.com/jinzhu/gorm"
	"time"
)

/* 指定扩展结结构，单独存文件。生成时不会覆盖: */
//type EmployeeBase struct {ModelBase}

/*
 */
type Employee struct {
	basedto.BaseEntity `gorm:"-"`

	/*  员工编号  */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:'员工编号'" json:"id"`
	/*  所属部门  */
	DepartmentId int32 `gorm:"column:department_id;type:int(11);comment:'所属部门';default:0" json:"department_id"`
	/*  员工姓名  */
	Name string `gorm:"column:name;type:varchar(16);comment:'员工姓名'" json:"name"`
	/*  性别  */
	Gender string `gorm:"column:gender;type:char(4);comment:'性别'" json:"gender"`
	/*  出生日期  */
	Birthday time.Time `gorm:"column:birthday;type:date;comment:'出生日期'" json:"birthday"`
	/*  身份证号  */
	IdCard string `gorm:"column:id_card;type:char(18);comment:'身份证号'" json:"id_card"`
	/*  婚姻状况  */
	Wedlock string `gorm:"column:wedlock;type:varchar(8);comment:'婚姻状况'" json:"wedlock"`
	/*  民族  */
	NationId int32 `gorm:"column:nation_id;type:int(8);comment:'民族'" json:"nation_id"`
	/*  籍贯  */
	NativePlace string `gorm:"column:native_place;type:varchar(20);comment:'籍贯'" json:"native_place"`
	/*  政治面貌  */
	PoliticId int32 `gorm:"column:politic_id;type:int(8);comment:'政治面貌'" json:"politic_id"`
	/*  邮箱  */
	Email string `gorm:"column:email;type:varchar(20);comment:'邮箱'" json:"email"`
	/*  电话号码  */
	Phone string `gorm:"column:phone;type:varchar(11);comment:'电话号码'" json:"phone"`
	/*  联系地址  */
	Address string `gorm:"column:address;type:varchar(64);comment:'联系地址'" json:"address"`
	/*  职称ID  */
	JobLevelId int32 `gorm:"column:job_level_id;type:int(11);comment:'职称ID'" json:"job_level_id"`
	/*  职位ID  */
	PosId int32 `gorm:"column:pos_id;type:int(11);comment:'职位ID'" json:"pos_id"`
	/*  聘用形式  */
	EngageForm string `gorm:"column:engage_form;type:varchar(8);comment:'聘用形式'" json:"engage_form"`
	/*  最高学历  */
	TiptopDegree string `gorm:"column:tiptop_degree;type:varchar(8);comment:'最高学历'" json:"tiptop_degree"`
	/*  所属专业  */
	Specialty string `gorm:"column:specialty;type:varchar(32);comment:'所属专业'" json:"specialty"`
	/*  毕业院校  */
	School string `gorm:"column:school;type:varchar(32);comment:'毕业院校'" json:"school"`
	/*  入职日期  */
	BeginDate time.Time `gorm:"column:begin_date;type:date;comment:'入职日期'" json:"begin_date"`
	/*  在职状态  */
	WorkState string `gorm:"column:work_state;type:varchar(8);comment:'在职状态';default:\'在职\'" json:"work_state"`
	/*  工号  */
	Code string `gorm:"column:code;type:varchar(8);comment:'工号'" json:"code"`
	/*  合同期限  */
	ContractTerm float64 `gorm:"column:contract_term;type:double;comment:'合同期限'" json:"contract_term"`
	/*  转正日期  */
	ConversionTime time.Time `gorm:"column:conversion_time;type:date;comment:'转正日期'" json:"conversion_time"`
	/*  离职日期  */
	NotWokDate time.Time `gorm:"column:not_wok_date;type:date;comment:'离职日期'" json:"not_wok_date"`
	/*  合同起始日期  */
	BeginContract time.Time `gorm:"column:begin_contract;type:date;comment:'合同起始日期'" json:"begin_contract"`
	/*  合同终止日期  */
	EndContract time.Time `gorm:"column:end_contract;type:date;comment:'合同终止日期'" json:"end_contract"`
	/*  工龄  */
	WorkAge int32 `gorm:"column:work_age;type:int(11);comment:'工龄'" json:"work_age"`
	/*    */
	WorkId string `gorm:"column:work_id;type:varchar(16);comment:''" json:"work_id"`
}

func NewEmployee() *Employee {
	var m = &Employee{}
	m.InitProxy(m)
	return m
}

type EmployeeParams struct {

	/*  员工编号  */
	Id *int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:'员工编号'" json:"id"`
	/*  所属部门  */
	DepartmentId *int32 `gorm:"column:department_id;type:int(11);comment:'所属部门';default:0" json:"department_id"`
	/*  员工姓名  */
	Name *string `gorm:"column:name;type:varchar(16);comment:'员工姓名'" json:"name"`
	/*  性别  */
	Gender *string `gorm:"column:gender;type:char(4);comment:'性别'" json:"gender"`
	/*  出生日期  */
	Birthday *time.Time `gorm:"column:birthday;type:date;comment:'出生日期'" json:"birthday"`
	/*  身份证号  */
	IdCard *string `gorm:"column:id_card;type:char(18);comment:'身份证号'" json:"id_card"`
	/*  婚姻状况  */
	Wedlock *string `gorm:"column:wedlock;type:varchar(8);comment:'婚姻状况'" json:"wedlock"`
	/*  民族  */
	NationId *int32 `gorm:"column:nation_id;type:int(8);comment:'民族'" json:"nation_id"`
	/*  籍贯  */
	NativePlace *string `gorm:"column:native_place;type:varchar(20);comment:'籍贯'" json:"native_place"`
	/*  政治面貌  */
	PoliticId *int32 `gorm:"column:politic_id;type:int(8);comment:'政治面貌'" json:"politic_id"`
	/*  邮箱  */
	Email *string `gorm:"column:email;type:varchar(20);comment:'邮箱'" json:"email"`
	/*  电话号码  */
	Phone *string `gorm:"column:phone;type:varchar(11);comment:'电话号码'" json:"phone"`
	/*  联系地址  */
	Address *string `gorm:"column:address;type:varchar(64);comment:'联系地址'" json:"address"`
	/*  职称ID  */
	JobLevelId *int32 `gorm:"column:job_level_id;type:int(11);comment:'职称ID'" json:"job_level_id"`
	/*  职位ID  */
	PosId *int32 `gorm:"column:pos_id;type:int(11);comment:'职位ID'" json:"pos_id"`
	/*  聘用形式  */
	EngageForm *string `gorm:"column:engage_form;type:varchar(8);comment:'聘用形式'" json:"engage_form"`
	/*  最高学历  */
	TiptopDegree *string `gorm:"column:tiptop_degree;type:varchar(8);comment:'最高学历'" json:"tiptop_degree"`
	/*  所属专业  */
	Specialty *string `gorm:"column:specialty;type:varchar(32);comment:'所属专业'" json:"specialty"`
	/*  毕业院校  */
	School *string `gorm:"column:school;type:varchar(32);comment:'毕业院校'" json:"school"`
	/*  入职日期  */
	BeginDate *time.Time `gorm:"column:begin_date;type:date;comment:'入职日期'" json:"begin_date"`
	/*  在职状态  */
	WorkState *string `gorm:"column:work_state;type:varchar(8);comment:'在职状态';default:\'在职\'" json:"work_state"`
	/*  工号  */
	Code *string `gorm:"column:code;type:varchar(8);comment:'工号'" json:"code"`
	/*  合同期限  */
	ContractTerm *float64 `gorm:"column:contract_term;type:double;comment:'合同期限'" json:"contract_term"`
	/*  转正日期  */
	ConversionTime *time.Time `gorm:"column:conversion_time;type:date;comment:'转正日期'" json:"conversion_time"`
	/*  离职日期  */
	NotWokDate *time.Time `gorm:"column:not_wok_date;type:date;comment:'离职日期'" json:"not_wok_date"`
	/*  合同起始日期  */
	BeginContract *time.Time `gorm:"column:begin_contract;type:date;comment:'合同起始日期'" json:"begin_contract"`
	/*  合同终止日期  */
	EndContract *time.Time `gorm:"column:end_contract;type:date;comment:'合同终止日期'" json:"end_contract"`
	/*  工龄  */
	WorkAge *int32 `gorm:"column:work_age;type:int(11);comment:'工龄'" json:"work_age"`
	/*    */
	WorkId *string `gorm:"column:work_id;type:varchar(16);comment:''" json:"work_id"`
}
type EmployeeEntity struct {

	/*  员工编号  */
	Id int32 `gorm:"column:id;type:int(11);PRIMARY_KEY;comment:'员工编号'" json:"id"`
	/*  所属部门  */
	DepartmentId int32 `gorm:"column:department_id;type:int(11);comment:'所属部门';default:0" json:"department_id"`
	/*  员工姓名  */
	Name string `gorm:"column:name;type:varchar(16);comment:'员工姓名'" json:"name"`
	/*  性别  */
	Gender string `gorm:"column:gender;type:char(4);comment:'性别'" json:"gender"`
	/*  出生日期  */
	Birthday int64 `gorm:"column:birthday;type:date;comment:'出生日期'" json:"birthday,string"`
	/*  身份证号  */
	IdCard string `gorm:"column:id_card;type:char(18);comment:'身份证号'" json:"id_card"`
	/*  婚姻状况  */
	Wedlock string `gorm:"column:wedlock;type:varchar(8);comment:'婚姻状况'" json:"wedlock"`
	/*  民族  */
	NationId int32 `gorm:"column:nation_id;type:int(8);comment:'民族'" json:"nation_id"`
	/*  籍贯  */
	NativePlace string `gorm:"column:native_place;type:varchar(20);comment:'籍贯'" json:"native_place"`
	/*  政治面貌  */
	PoliticId int32 `gorm:"column:politic_id;type:int(8);comment:'政治面貌'" json:"politic_id"`
	/*  邮箱  */
	Email string `gorm:"column:email;type:varchar(20);comment:'邮箱'" json:"email"`
	/*  电话号码  */
	Phone string `gorm:"column:phone;type:varchar(11);comment:'电话号码'" json:"phone"`
	/*  联系地址  */
	Address string `gorm:"column:address;type:varchar(64);comment:'联系地址'" json:"address"`
	/*  职称ID  */
	JobLevelId int32 `gorm:"column:job_level_id;type:int(11);comment:'职称ID'" json:"job_level_id"`
	/*  职位ID  */
	PosId int32 `gorm:"column:pos_id;type:int(11);comment:'职位ID'" json:"pos_id"`
	/*  聘用形式  */
	EngageForm string `gorm:"column:engage_form;type:varchar(8);comment:'聘用形式'" json:"engage_form"`
	/*  最高学历  */
	TiptopDegree string `gorm:"column:tiptop_degree;type:varchar(8);comment:'最高学历'" json:"tiptop_degree"`
	/*  所属专业  */
	Specialty string `gorm:"column:specialty;type:varchar(32);comment:'所属专业'" json:"specialty"`
	/*  毕业院校  */
	School string `gorm:"column:school;type:varchar(32);comment:'毕业院校'" json:"school"`
	/*  入职日期  */
	BeginDate int64 `gorm:"column:begin_date;type:date;comment:'入职日期'" json:"begin_date,string"`
	/*  在职状态  */
	WorkState string `gorm:"column:work_state;type:varchar(8);comment:'在职状态';default:\'在职\'" json:"work_state"`
	/*  工号  */
	Code string `gorm:"column:code;type:varchar(8);comment:'工号'" json:"code"`
	/*  合同期限  */
	ContractTerm float64 `gorm:"column:contract_term;type:double;comment:'合同期限'" json:"contract_term"`
	/*  转正日期  */
	ConversionTime int64 `gorm:"column:conversion_time;type:date;comment:'转正日期'" json:"conversion_time,string"`
	/*  离职日期  */
	NotWokDate int64 `gorm:"column:not_wok_date;type:date;comment:'离职日期'" json:"not_wok_date,string"`
	/*  合同起始日期  */
	BeginContract int64 `gorm:"column:begin_contract;type:date;comment:'合同起始日期'" json:"begin_contract,string"`
	/*  合同终止日期  */
	EndContract int64 `gorm:"column:end_contract;type:date;comment:'合同终止日期'" json:"end_contract,string"`
	/*  工龄  */
	WorkAge int32 `gorm:"column:work_age;type:int(11);comment:'工龄'" json:"work_age"`
	/*    */
	WorkId string `gorm:"column:work_id;type:varchar(16);comment:''" json:"work_id"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *Employee) TableName() string {

	return "employee"
}

/*
迁移
*/
func (entity *Employee) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(dbmenu)

	return err
}

/*
exec pg comment sql
*/
func (entity *Employee) execComment(db *gorm.DB) {

	sql := "comment on table employee is '';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *Employee) String() string {
	s, err := json.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Employee) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Employee) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *Employee) UnmarshalBy(body []byte) error {
	return json.Unmarshal(body, entity)

}

/*
func (entity *Employee ) Model2PbMsg (pbentity *proto.EmployeeProto) * proto.EmployeeProto{

    entity.IniNil(true)


		pbentity.Id =  entity.GetId()
		pbentity.DepartmentId =  entity.GetDepartmentId()
		pbentity.Name =  entity.GetName()
		pbentity.Gender =  entity.GetGender()
		pbentity.Birthday =  entity.GetBirthday()
		pbentity.IdCard =  entity.GetIdCard()
		pbentity.Wedlock =  entity.GetWedlock()
		pbentity.NationId =  entity.GetNationId()
		pbentity.NativePlace =  entity.GetNativePlace()
		pbentity.PoliticId =  entity.GetPoliticId()
		pbentity.Email =  entity.GetEmail()
		pbentity.Phone =  entity.GetPhone()
		pbentity.Address =  entity.GetAddress()
		pbentity.JobLevelId =  entity.GetJobLevelId()
		pbentity.PosId =  entity.GetPosId()
		pbentity.EngageForm =  entity.GetEngageForm()
		pbentity.TiptopDegree =  entity.GetTiptopDegree()
		pbentity.Specialty =  entity.GetSpecialty()
		pbentity.School =  entity.GetSchool()
		pbentity.BeginDate =  entity.GetBeginDate()
		pbentity.WorkState =  entity.GetWorkState()
		pbentity.Code =  entity.GetCode()
		pbentity.ContractTerm =  entity.GetContractTerm()
		pbentity.ConversionTime =  entity.GetConversionTime()
		pbentity.NotWokDate =  entity.GetNotWokDate()
		pbentity.BeginContract =  entity.GetBeginContract()
		pbentity.EndContract =  entity.GetEndContract()
		pbentity.WorkAge =  entity.GetWorkAge()
		pbentity.WorkId =  entity.GetWorkId()

    return pbentity
}

func (entity *Employee ) PbMsg2Model (pbentity *proto.EmployeeProto) *Employee{
    entity.IniNil(true)


		 * entity.Id =  pbentity.GetId()
		 * entity.DepartmentId =  pbentity.GetDepartmentId()
		 * entity.Name =  pbentity.GetName()
		 * entity.Gender =  pbentity.GetGender()
		 * entity.Birthday =  pbentity.GetBirthday()
		 * entity.IdCard =  pbentity.GetIdCard()
		 * entity.Wedlock =  pbentity.GetWedlock()
		 * entity.NationId =  pbentity.GetNationId()
		 * entity.NativePlace =  pbentity.GetNativePlace()
		 * entity.PoliticId =  pbentity.GetPoliticId()
		 * entity.Email =  pbentity.GetEmail()
		 * entity.Phone =  pbentity.GetPhone()
		 * entity.Address =  pbentity.GetAddress()
		 * entity.JobLevelId =  pbentity.GetJobLevelId()
		 * entity.PosId =  pbentity.GetPosId()
		 * entity.EngageForm =  pbentity.GetEngageForm()
		 * entity.TiptopDegree =  pbentity.GetTiptopDegree()
		 * entity.Specialty =  pbentity.GetSpecialty()
		 * entity.School =  pbentity.GetSchool()
		 * entity.BeginDate =  pbentity.GetBeginDate()
		 * entity.WorkState =  pbentity.GetWorkState()
		 * entity.Code =  pbentity.GetCode()
		 * entity.ContractTerm =  pbentity.GetContractTerm()
		 * entity.ConversionTime =  pbentity.GetConversionTime()
		 * entity.NotWokDate =  pbentity.GetNotWokDate()
		 * entity.BeginContract =  pbentity.GetBeginContract()
		 * entity.EndContract =  pbentity.GetEndContract()
		 * entity.WorkAge =  pbentity.GetWorkAge()
		 * entity.WorkId =  pbentity.GetWorkId()
    return entity
}


func (entity *Employee) IniPbMsg() (pbentity *proto.EmployeeProto) {

	pbentity = &proto.EmployeeProto{}


		pbentity.Id =  0
		pbentity.DepartmentId =  0
		pbentity.Name =  ""
		pbentity.Gender =  ""
		pbentity.Birthday =  <no value>
		pbentity.IdCard =  ""
		pbentity.Wedlock =  ""
		pbentity.NationId =  0
		pbentity.NativePlace =  ""
		pbentity.PoliticId =  0
		pbentity.Email =  ""
		pbentity.Phone =  ""
		pbentity.Address =  ""
		pbentity.JobLevelId =  0
		pbentity.PosId =  0
		pbentity.EngageForm =  ""
		pbentity.TiptopDegree =  ""
		pbentity.Specialty =  ""
		pbentity.School =  ""
		pbentity.BeginDate =  <no value>
		pbentity.WorkState =  ""
		pbentity.Code =  ""
		pbentity.ContractTerm =  0
		pbentity.ConversionTime =  <no value>
		pbentity.NotWokDate =  <no value>
		pbentity.BeginContract =  <no value>
		pbentity.EndContract =  <no value>
		pbentity.WorkAge =  0
		pbentity.WorkId =  ""
	return
}

*/
