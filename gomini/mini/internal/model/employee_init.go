package model

import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: employee_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)
	@Update 作者: LEIJMDAS@163.COM 时间(2024-05-11 06:55:37)

* *********************************************************/

const singleNameEmployee = "model.Employee"

// init register load
func init() {
	registerBeanEmployee()
}

// register Employee
func registerBeanEmployee() {
	basedi.RegisterLoadBean(singleNameEmployee, LoadEmployee)
}

func FindBeanEmployee() *Employee {
	bean, ok := basedi.FindBean(singleNameEmployee).(*Employee)
	if !ok {
		logrus.Errorf("FindBeanEmployee: failed to cast bean to *Employee")
		return nil
	}
	return bean

}

func LoadEmployee() baseiface.ISingleton {
	var s = NewEmployee()
	InjectEmployee(s)
	return s

}

func InjectEmployee(s *Employee) {

	// logrus.Debug("inject")
}
