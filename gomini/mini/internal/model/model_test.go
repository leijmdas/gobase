package model

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/reflectutils"
	"gitee.com/leijmdas/gobase/gomini/mini/general/query/model"
	"reflect"
	"strings"
	"testing"
)

func Test001_tag(t *testing.T) {
	v := &model.Employee{}
	elem := reflect.TypeOf(v).Elem()
	for i := 0; i < elem.NumField(); i++ {
		fmt.Printf("结构体内第%v个字段 %v 对应的json tag是 %v , 还有otherTag？ = %v \n", i+1, elem.Field(i).Name, elem.Field(i).Tag.Get("json"), elem.Field(i).Tag.Get("otherTag"))
		fmt.Printf("结构体内第%v个字段 %v 对应的json tag是 %v , 还有otherTag？ = %v \n", i+1, elem.Field(i).Name, elem.Field(i).Tag.Get("gorm"), elem.Field(i).Tag.Get("otherTag"))

	}
	v1, _ := elem.FieldByName("Code")
	tag := v1.Tag.Get("gorm")
	tt := &GormTag{}

	fmt.Println(tt.Parse(tag))
	reftag := reflectutils.NewReflectTags()
	reftag.ParseGorm(v)
	reftag.Log()
}

type GormTag struct {
	Col     string
	Type    string
	Comment string
}

func (gt *GormTag) Parse(tagstr string) GormTag {
	ret := strings.Split(tagstr, ";")
	gt.Col = ret[0]
	gt.Type = ret[1]
	gt.Comment = ret[2]
	return *gt
}

type GoEnum int

const (
	GoEnum_A GoEnum = iota
	GoEnum_B
	GoEnum_C
)

func (ge GoEnum) String() string {

	return reflect.ValueOf(GoEnum_B).String()
}

func Test002_tag(t *testing.T) {
	fmt.Println(GoEnum_A.String())
	var tr = reflect.ValueOf(GoEnum_B)
	fmt.Println(tr)

}
