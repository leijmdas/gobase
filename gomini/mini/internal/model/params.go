package model

/*
   @Title    文件名称: Params.gomodel
   @Description  描述: 实体Params

   @Author  作者: leijianming@163.com  时间(2024-02-08 18:19:47)
   @Update  作者: leijianming@163.com  时间(2024-02-08 18:19:47)

*/

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "time"
)

/* 指定扩展结结构，单独存文件。生成时不会覆盖: */
//type ParamsBase struct {ModelBase}

/*
 */
type Params struct {
	//	ParamsBase
	//	ModelBase

	/*    */
	Id int64 `gorm:"column:id;type:bigint(20);PRIMARY_KEY;comment:''" json:"id,string"`
	/*    */
	ParamType string `gorm:"column:param_type;type:varchar(255);comment:''" json:"paramType"`
	/*    */
	ParamName string `gorm:"column:param_name;type:varchar(255);comment:''" json:"paramName"`
	/*    */
	ParentId int64 `gorm:"column:parent_id;type:bigint(20);comment:''" json:"parentId,string"`
}
type ParamsVo struct {

	/*    */
	Id int64 `gorm:"column:id;type:bigint(20);PRIMARY_KEY;comment:''" json:"id,string"`
	/*    */
	ParamType string `gorm:"column:param_type;type:varchar(255);comment:''" json:"paramType"`
	/*    */
	ParamName string `gorm:"column:param_name;type:varchar(255);comment:''" json:"paramName"`
	/*    */
	ParentId int64 `gorm:"column:parent_id;type:bigint(20);comment:''" json:"parentId,string"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *Params) TableName() string {

	return "params"
}

/*
迁移
*/
func (entity *Params) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(dbmenu)

	return err
}

/*
exec pg comment sql
*/
func (entity *Params) execComment(db *gorm.DB) {

	sql := "comment on table params is '';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *Params) String() string {
	s, err := json.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Params) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Params) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *Params) UnmarshalBy(body []byte) error {
	return json.Unmarshal(body, entity)

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *Params) Ini(iniPk bool) *Params {
	if iniPk {
		entity.Id = 0 //new(int64)
	}

	//entity.ParamType = new(string)

	//entity.ParamName = new(string)

	//entity.ParentId = new(int64)

	return entity

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *Params) IniNil(iniPk bool) *Params {
	if iniPk {

		entity.Id = 0 //new(int64)

	}

	//if entity.ParamType == nil {
	//entity.ParamType = new(string)
	//}

	//if entity.ParamName == nil {
	//entity.ParamName = new(string)
	//}

	//if entity.ParentId == nil {
	//entity.ParentId = new(int64)
	//}

	return entity

}

func (entity *Params) copy(iniPk bool, cpentity *Params) *Params {

	entity.Ini(iniPk)
	cpentity.IniNil(iniPk)

	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID =  *cpentity.ID

	entity.ParamType = cpentity.ParamType
	entity.ParamName = cpentity.ParamName
	entity.ParentId = cpentity.ParentId
	return entity
}

func (entity *Params) copyNotNil(iniPk bool, cpentity *Params) *Params {
	entity.IniNil(iniPk)
	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID = *cpentity.ID

	entity.ParamType = cpentity.ParamType
	entity.ParamName = cpentity.ParamName
	entity.ParentId = cpentity.ParentId
	return entity
}

/*
func (entity *Params ) Model2PbMsg (pbentity *proto.ParamsProto) * proto.ParamsProto{

    entity.IniNil(true)


		pbentity.Id =  utils.ToStr(entity.GetId())
		pbentity.ParamType =  entity.GetParamType()
		pbentity.ParamName =  entity.GetParamName()
		pbentity.ParentId =  utils.ToStr(entity.GetParentId())

    return pbentity
}

func (entity *Params ) PbMsg2Model (pbentity *proto.ParamsProto) *Params{
    entity.IniNil(true)


		 * entity.Id =  utils.Str2Int64(pbentity.GetId())
		 * entity.ParamType =  pbentity.GetParamType()
		 * entity.ParamName =  pbentity.GetParamName()
		 * entity.ParentId =  utils.Str2Int64(pbentity.GetParentId())
    return entity
}


func (entity *Params) IniPbMsg() (pbentity *proto.<no value>Proto) {

	pbentity = &proto.<no value>Proto{}


		pbentity.Id =  "0"
		pbentity.ParamType =  ""
		pbentity.ParamName =  ""
		pbentity.ParentId =  "0"
	return
}

*/

func (entity *Params) GetId() int64 {

	return entity.Id

}

func (entity *Params) SetId(Id int64) {
	entity.Id = Id
}

func (entity *Params) GetParamType() string {

	return entity.ParamType

}

func (entity *Params) SetParamType(ParamType string) {
	entity.ParamType = ParamType
}

func (entity *Params) GetParamName() string {

	return entity.ParamName

}

func (entity *Params) SetParamName(ParamName string) {
	entity.ParamName = ParamName
}

func (entity *Params) GetParentId() int64 {

	return entity.ParentId

}

func (entity *Params) SetParentId(ParentId int64) {
	entity.ParentId = ParentId
}
