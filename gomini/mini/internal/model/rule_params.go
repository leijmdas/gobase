package model

/*
   @Title    文件名称: RuleParams.gomodel
   @Description  描述: 实体RuleParams

   @Author  作者: leijianming@163.com  时间(2024-03-04 23:05:46)
   @Update  作者: leijianming@163.com  时间(2024-03-04 23:05:46)

*/

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "time"
)

/* 指定扩展结结构，单独存文件。生成时不会覆盖: */
//type RuleParamsBase struct {ModelBase}

/*
 */
type RuleParams struct {
	//	RuleParamsBase
	//	ModelBase

	/*    */
	Id int64 `gorm:"column:id;type:bigint(20);PRIMARY_KEY;comment:''" json:"id,string"`
	/*    */
	ParamType string `gorm:"column:param_type;type:varchar(255);comment:''" json:"param_type"`
	/*    */
	ParamName string `gorm:"column:param_name;type:varchar(255);comment:''" json:"param_name"`
	/*    */
	ParentId int64 `gorm:"column:parent_id;type:bigint(20);comment:''" json:"parent_id,string"`
	/*  中文  */
	Remark string `gorm:"column:remark;type:varchar(255);comment:'中文'" json:"remark"`
	/*  默认  */
	Inival string `gorm:"column:inival;type:varchar(255);comment:'默认'" json:"inival"`
	/*    */
	Option string `gorm:"column:option;type:varchar(255);comment:''" json:"option"`
	/*    */
	PolicyId int32 `gorm:"column:policy_id;type:int(11);comment:'';default:-1" json:"policy_id"`
}
type RuleParamsVo struct {

	/*    */
	Id int64 `gorm:"column:id;type:bigint(20);PRIMARY_KEY;comment:''" json:"id,string"`
	/*    */
	ParamType string `gorm:"column:param_type;type:varchar(255);comment:''" json:"param_type"`
	/*    */
	ParamName string `gorm:"column:param_name;type:varchar(255);comment:''" json:"param_name"`
	/*    */
	ParentId int64 `gorm:"column:parent_id;type:bigint(20);comment:''" json:"parent_id,string"`
	/*  中文  */
	Remark string `gorm:"column:remark;type:varchar(255);comment:'中文'" json:"remark"`
	/*  默认  */
	Inival string `gorm:"column:inival;type:varchar(255);comment:'默认'" json:"inival"`
	/*    */
	Option string `gorm:"column:option;type:varchar(255);comment:''" json:"option"`
	/*    */
	PolicyId int32 `gorm:"column:policy_id;type:int(11);comment:'';default:-1" json:"policy_id"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *RuleParams) TableName() string {

	return "rule_params"
}

/*
迁移
*/
func (entity *RuleParams) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(dbmenu)

	return err
}

/*
exec pg comment sql
*/
func (entity *RuleParams) execComment(db *gorm.DB) {

	sql := "comment on table rule_params is '';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *RuleParams) String() string {
	s, err := json.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *RuleParams) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *RuleParams) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *RuleParams) UnmarshalBy(body []byte) error {
	return json.Unmarshal(body, entity)

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *RuleParams) Ini(iniPk bool) *RuleParams {
	if iniPk {
		entity.Id = 0 //new(int64)
	}

	//entity.ParamType = new(string)

	//entity.ParamName = new(string)

	//entity.ParentId = new(int64)

	//entity.Remark = new(string)

	//entity.Inival = new(string)

	//entity.Option = new(string)

	//entity.PolicyId = new(int32)

	return entity

}

/*
iniPk bool:是否初始化主键Id
初始化指针
*/
func (entity *RuleParams) IniNil(iniPk bool) *RuleParams {
	if iniPk {

		entity.Id = 0 //new(int64)

	}

	//if entity.ParamType == nil {
	//entity.ParamType = new(string)
	//}

	//if entity.ParamName == nil {
	//entity.ParamName = new(string)
	//}

	//if entity.ParentId == nil {
	//entity.ParentId = new(int64)
	//}

	//if entity.Remark == nil {
	//entity.Remark = new(string)
	//}

	//if entity.Inival == nil {
	//entity.Inival = new(string)
	//}

	//if entity.Option == nil {
	//entity.Option = new(string)
	//}

	//if entity.PolicyId == nil {
	//entity.PolicyId = new(int32)
	//}

	return entity

}

func (entity *RuleParams) copy(iniPk bool, cpentity *RuleParams) *RuleParams {

	entity.Ini(iniPk)
	cpentity.IniNil(iniPk)

	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID =  *cpentity.ID

	entity.ParamType = cpentity.ParamType
	entity.ParamName = cpentity.ParamName
	entity.ParentId = cpentity.ParentId
	entity.Remark = cpentity.Remark
	entity.Inival = cpentity.Inival
	entity.Option = cpentity.Option
	entity.PolicyId = cpentity.PolicyId
	return entity
}

func (entity *RuleParams) copyNotNil(iniPk bool, cpentity *RuleParams) *RuleParams {
	entity.IniNil(iniPk)
	if iniPk {
		entity.Id = cpentity.Id
	}

	//*entity.ID = *cpentity.ID

	entity.ParamType = cpentity.ParamType
	entity.ParamName = cpentity.ParamName
	entity.ParentId = cpentity.ParentId
	entity.Remark = cpentity.Remark
	entity.Inival = cpentity.Inival
	entity.Option = cpentity.Option
	entity.PolicyId = cpentity.PolicyId
	return entity
}

/*
func (entity *RuleParams ) Model2PbMsg (pbentity *proto.RuleParamsProto) * proto.RuleParamsProto{

    entity.IniNil(true)


		pbentity.Id =  utils.ToStr(entity.GetId())
		pbentity.ParamType =  entity.GetParamType()
		pbentity.ParamName =  entity.GetParamName()
		pbentity.ParentId =  utils.ToStr(entity.GetParentId())
		pbentity.Remark =  entity.GetRemark()
		pbentity.Inival =  entity.GetInival()
		pbentity.Option =  entity.GetOption()
		pbentity.PolicyId =  entity.GetPolicyId()

    return pbentity
}

func (entity *RuleParams ) PbMsg2Model (pbentity *proto.RuleParamsProto) *RuleParams{
    entity.IniNil(true)


		 * entity.Id =  utils.Str2Int64(pbentity.GetId())
		 * entity.ParamType =  pbentity.GetParamType()
		 * entity.ParamName =  pbentity.GetParamName()
		 * entity.ParentId =  utils.Str2Int64(pbentity.GetParentId())
		 * entity.Remark =  pbentity.GetRemark()
		 * entity.Inival =  pbentity.GetInival()
		 * entity.Option =  pbentity.GetOption()
		 * entity.PolicyId =  pbentity.GetPolicyId()
    return entity
}


func (entity *RuleParams) IniPbMsg() (pbentity *proto.<no value>Proto) {

	pbentity = &proto.<no value>Proto{}


		pbentity.Id =  "0"
		pbentity.ParamType =  ""
		pbentity.ParamName =  ""
		pbentity.ParentId =  "0"
		pbentity.Remark =  ""
		pbentity.Inival =  ""
		pbentity.Option =  ""
		pbentity.PolicyId =  0
	return
}

*/

func (entity *RuleParams) GetId() int64 {

	return entity.Id

}

func (entity *RuleParams) SetId(Id int64) {
	entity.Id = Id
}

func (entity *RuleParams) GetParamType() string {

	return entity.ParamType

}

func (entity *RuleParams) SetParamType(ParamType string) {
	entity.ParamType = ParamType
}

func (entity *RuleParams) GetParamName() string {

	return entity.ParamName

}

func (entity *RuleParams) SetParamName(ParamName string) {
	entity.ParamName = ParamName
}

func (entity *RuleParams) GetParentId() int64 {

	return entity.ParentId

}

func (entity *RuleParams) SetParentId(ParentId int64) {
	entity.ParentId = ParentId
}

func (entity *RuleParams) GetRemark() string {

	return entity.Remark

}

func (entity *RuleParams) SetRemark(Remark string) {
	entity.Remark = Remark
}

func (entity *RuleParams) GetInival() string {

	return entity.Inival

}

func (entity *RuleParams) SetInival(Inival string) {
	entity.Inival = Inival
}

func (entity *RuleParams) GetOption() string {

	return entity.Option

}

func (entity *RuleParams) SetOption(Option string) {
	entity.Option = Option
}

func (entity *RuleParams) GetPolicyId() int32 {

	return entity.PolicyId

}

func (entity *RuleParams) SetPolicyId(PolicyId int32) {
	entity.PolicyId = PolicyId
}
