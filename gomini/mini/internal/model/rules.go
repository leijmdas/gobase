package model

/*
   @Title    文件名称: Rules.gomodel
   @Description  描述: 实体Rules

   @Author  作者: leijianming@163.com  时间(2024-03-09 06:06:14)
   @Update  作者: leijianming@163.com  时间(2024-03-09 06:06:14)

*/

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basemodel"
	"github.com/jinzhu/gorm"
	jsoniter "github.com/json-iterator/go"
	_ "time"
)

/* 指定扩展结结构，单独存文件。生成时不会覆盖: */
//type RulesBase struct {ModelBase}

/*
 */
type Rules struct {
	//	RulesBase
	//	ModelBase

	/*    */
	RuleId int64 `gorm:"column:rule_id;type:bigint(20);PRIMARY_KEY;comment:''" json:"rule_id,string"`
	/*    */
	RuleKey string `gorm:"column:rule_key;type:varchar(255);comment:''" json:"rule_key"`
	/*    */
	Name string `gorm:"column:name;type:varchar(255);comment:''" json:"name"`
	/*    */
	Domain string `gorm:"column:domain;type:varchar(255);comment:''" json:"domain"`
	/*    */
	Rule string `gorm:"column:rule;type:mediumtext;comment:''" json:"rule"`
	/*    */
	Param string `gorm:"column:param;type:tinytext;comment:''" json:"param"`
	/*    */
	Result string `gorm:"column:result;type:mediumtext;comment:''" json:"result"`
	/*    */
	Remark string `gorm:"column:remark;type:varchar(255);comment:''" json:"remark"`
	/*    */
	FuncType string `gorm:"column:func_type;type:varchar(255);comment:''" json:"func_type"`
	/*    */
	CreatedAt basemodel.IchubLocalTime `gorm:"column:created_at;type:datetime;comment:''" json:"created_at"`
	/*  创建者  */
	CreatedBy int64 `gorm:"column:created_by;type:bigint(20);comment:'创建者';default:-1" json:"created_by,string"`
}

func NewRules() *Rules {
	return &Rules{}
}

type RulesParams struct {

	/*    */
	RuleId *int64 `gorm:"column:rule_id;type:bigint(20);PRIMARY_KEY;comment:''" json:"rule_id,string"`
	/*    */
	RuleKey *string `gorm:"column:rule_key;type:varchar(255);comment:''" json:"rule_key"`
	/*    */
	Name *string `gorm:"column:name;type:varchar(255);comment:''" json:"name"`
	/*    */
	Domain *string `gorm:"column:domain;type:varchar(255);comment:''" json:"domain"`
	/*    */
	Rule *string `gorm:"column:rule;type:mediumtext;comment:''" json:"rule"`
	/*    */
	Param *string `gorm:"column:param;type:tinytext;comment:''" json:"param"`
	/*    */
	Result *string `gorm:"column:result;type:mediumtext;comment:''" json:"result"`
	/*    */
	Remark *string `gorm:"column:remark;type:varchar(255);comment:''" json:"remark"`
	/*    */
	FuncType *string `gorm:"column:func_type;type:varchar(255);comment:''" json:"func_type"`
	/*    */
	CreatedAt *basemodel.LocalTime `gorm:"column:created_at;type:datetime;comment:''" json:"created_at"`
	/*  创建者  */
	CreatedBy *int64 `gorm:"column:created_by;type:bigint(20);comment:'创建者';default:-1" json:"created_by,string"`
}
type RulesDto struct {

	/*    */
	RuleId int64 `gorm:"column:rule_id;type:bigint(20);PRIMARY_KEY;comment:''" json:"rule_id,string"`
	/*    */
	RuleKey string `gorm:"column:rule_key;type:varchar(255);comment:''" json:"rule_key"`
	/*    */
	Name string `gorm:"column:name;type:varchar(255);comment:''" json:"name"`
	/*    */
	Domain string `gorm:"column:domain;type:varchar(255);comment:''" json:"domain"`
	/*    */
	Rule string `gorm:"column:rule;type:mediumtext;comment:''" json:"rule"`
	/*    */
	Param string `gorm:"column:param;type:tinytext;comment:''" json:"param"`
	/*    */
	Result string `gorm:"column:result;type:mediumtext;comment:''" json:"result"`
	/*    */
	Remark string `gorm:"column:remark;type:varchar(255);comment:''" json:"remark"`
	/*    */
	FuncType string `gorm:"column:func_type;type:varchar(255);comment:''" json:"func_type"`
	/*    */
	CreatedAt basemodel.LocalTime `gorm:"column:created_at;type:datetime;comment:''" json:"created_at"`
	/*  创建者  */
	CreatedBy int64 `gorm:"column:created_by;type:bigint(20);comment:'创建者';default:-1" json:"created_by,string"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *Rules) TableName() string {

	return "rules"
}

/*
迁移
*/
func (entity *Rules) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(dbmenu)

	return err
}

/*
exec pg comment sql
*/
func (entity *Rules) execComment(db *gorm.DB) {

	sql := "comment on table rules is '';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *Rules) String() string {
	s, err := jsoniter.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Rules) ToString() string {
	s, err := jsoniter.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *Rules) Unmarshal(body string) error {
	return jsoniter.Unmarshal([]byte(body), entity)

}

func (entity *Rules) UnmarshalBy(body []byte) error {
	return jsoniter.Unmarshal(body, entity)

}

/*
iniPk bool:是否初始化主键RuleId
初始化指针
*/
func (entity *Rules) Ini(iniPk bool) *Rules {
	if iniPk {
		entity.RuleId = 0 //new(int64)
	}

	//entity.RuleKey = new(string)

	//entity.Name = new(string)

	//entity.Domain = new(string)

	//entity.Rule = new(string)

	//entity.Param = new(string)

	//entity.Result = new(string)

	//entity.Remark = new(string)

	//entity.FuncType = new(string)

	//entity.CreatedAt = new(basemodel.LocalTime)

	//entity.CreatedBy = new(int64)

	return entity

}

/*
iniPk bool:是否初始化主键RuleId
初始化指针
*/
func (entity *Rules) IniNil(iniPk bool) *Rules {
	if iniPk {

		entity.RuleId = 0 //new(int64)

	}

	//if entity.RuleKey == nil {
	//entity.RuleKey = new(string)
	//}

	//if entity.Name == nil {
	//entity.Name = new(string)
	//}

	//if entity.Domain == nil {
	//entity.Domain = new(string)
	//}

	//if entity.Rule == nil {
	//entity.Rule = new(string)
	//}

	//if entity.Param == nil {
	//entity.Param = new(string)
	//}

	//if entity.Result == nil {
	//entity.Result = new(string)
	//}

	//if entity.Remark == nil {
	//entity.Remark = new(string)
	//}

	//if entity.FuncType == nil {
	//entity.FuncType = new(string)
	//}

	//if entity.CreatedAt == nil {
	//entity.CreatedAt = new(basemodel.LocalTime)
	//}

	//if entity.CreatedBy == nil {
	//entity.CreatedBy = new(int64)
	//}

	return entity

}

func (entity *Rules) copy(iniPk bool, cpentity *Rules) *Rules {

	entity.Ini(iniPk)
	cpentity.IniNil(iniPk)

	if iniPk {
		entity.RuleId = cpentity.RuleId
	}

	//*entity.ID =  *cpentity.ID

	entity.RuleKey = cpentity.RuleKey
	entity.Name = cpentity.Name
	entity.Domain = cpentity.Domain
	entity.Rule = cpentity.Rule
	entity.Param = cpentity.Param
	entity.Result = cpentity.Result
	entity.Remark = cpentity.Remark
	entity.FuncType = cpentity.FuncType
	entity.CreatedAt = cpentity.CreatedAt
	entity.CreatedBy = cpentity.CreatedBy
	return entity
}

func (entity *Rules) copyNotNil(iniPk bool, cpentity *Rules) *Rules {
	entity.IniNil(iniPk)
	if iniPk {
		entity.RuleId = cpentity.RuleId
	}

	//*entity.ID = *cpentity.ID

	entity.RuleKey = cpentity.RuleKey
	entity.Name = cpentity.Name
	entity.Domain = cpentity.Domain
	entity.Rule = cpentity.Rule
	entity.Param = cpentity.Param
	entity.Result = cpentity.Result
	entity.Remark = cpentity.Remark
	entity.FuncType = cpentity.FuncType
	entity.CreatedAt = cpentity.CreatedAt
	entity.CreatedBy = cpentity.CreatedBy
	return entity
}

/*
func (entity *Rules ) Model2PbMsg (pbentity *proto.RulesProto) * proto.RulesProto{

    entity.IniNil(true)


		pbentity.RuleId =  utils.ToStr(entity.GetRuleId())
		pbentity.RuleKey =  entity.GetRuleKey()
		pbentity.Name =  entity.GetName()
		pbentity.Domain =  entity.GetDomain()
		pbentity.Rule =  entity.GetRule()
		pbentity.Param =  entity.GetParam()
		pbentity.Result =  entity.GetResult()
		pbentity.Remark =  entity.GetRemark()
		pbentity.FuncType =  entity.GetFuncType()
		pbentity.CreatedAt =  entity.GetCreatedAt()
		pbentity.CreatedBy =  utils.ToStr(entity.GetCreatedBy())

    return pbentity
}

func (entity *Rules ) PbMsg2Model (pbentity *proto.RulesProto) *Rules{
    entity.IniNil(true)


		 * entity.RuleId =  utils.Str2Int64(pbentity.GetRuleId())
		 * entity.RuleKey =  pbentity.GetRuleKey()
		 * entity.Name =  pbentity.GetName()
		 * entity.Domain =  pbentity.GetDomain()
		 * entity.Rule =  pbentity.GetRule()
		 * entity.Param =  pbentity.GetParam()
		 * entity.Result =  pbentity.GetResult()
		 * entity.Remark =  pbentity.GetRemark()
		 * entity.FuncType =  pbentity.GetFuncType()
		 * entity.CreatedAt =  pbentity.GetCreatedAt()
		 * entity.CreatedBy =  utils.Str2Int64(pbentity.GetCreatedBy())
    return entity
}


func (entity *Rules) IniPbMsg() (pbentity *proto.<no value>Proto) {

	pbentity = &proto.<no value>Proto{}


		pbentity.RuleId =  "0"
		pbentity.RuleKey =  ""
		pbentity.Name =  ""
		pbentity.Domain =  ""
		pbentity.Rule =  ""
		pbentity.Param =  ""
		pbentity.Result =  ""
		pbentity.Remark =  ""
		pbentity.FuncType =  ""
		pbentity.CreatedAt =  0
		pbentity.CreatedBy =  "0"
	return
}

*/

func (entity *Rules) GetRuleId() int64 {

	return entity.RuleId

}

func (entity *Rules) SetRuleId(RuleId int64) {
	entity.RuleId = RuleId
}

func (entity *Rules) GetRuleKey() string {

	return entity.RuleKey

}

func (entity *Rules) SetRuleKey(RuleKey string) {
	entity.RuleKey = RuleKey
}

func (entity *Rules) GetName() string {

	return entity.Name

}

func (entity *Rules) SetName(Name string) {
	entity.Name = Name
}

func (entity *Rules) GetDomain() string {

	return entity.Domain

}

func (entity *Rules) SetDomain(Domain string) {
	entity.Domain = Domain
}

func (entity *Rules) GetRule() string {

	return entity.Rule

}

func (entity *Rules) SetRule(Rule string) {
	entity.Rule = Rule
}

func (entity *Rules) GetParam() string {

	return entity.Param

}

func (entity *Rules) SetParam(Param string) {
	entity.Param = Param
}

func (entity *Rules) GetResult() string {

	return entity.Result

}

func (entity *Rules) SetResult(Result string) {
	entity.Result = Result
}

func (entity *Rules) GetRemark() string {

	return entity.Remark

}

func (entity *Rules) SetRemark(Remark string) {
	entity.Remark = Remark
}

func (entity *Rules) GetFuncType() string {

	return entity.FuncType

}

func (entity *Rules) SetFuncType(FuncType string) {
	entity.FuncType = FuncType
}

func (entity *Rules) GetCreatedAt() basemodel.IchubLocalTime {

	return entity.CreatedAt

}

func (entity *Rules) SetCreatedAt(CreatedAt basemodel.IchubLocalTime) {
	entity.CreatedAt = CreatedAt
}

func (entity *Rules) GetCreatedBy() int64 {

	return entity.CreatedBy

}

func (entity *Rules) SetCreatedBy(CreatedBy int64) {
	entity.CreatedBy = CreatedBy
}
