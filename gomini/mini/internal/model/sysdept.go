package model

/*
   @Title    文件名称: SysDept.gomodel
   @Description  描述: 实体SysDept

   @Author  作者: leijianming@163.com  时间(2024-01-24 17:15:32)
   @Update  作者: leijianming@163.com  时间(2024-01-24 17:15:32)

*/

import (
	"encoding/json"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basemodel"
	"github.com/jinzhu/gorm"
	jsoniter "github.com/json-iterator/go"
	"time"
	_ "time"
)

/* 指定扩展结结构，单独存文件。生成时不会覆盖: */
//type SysDeptBase struct {ModelBase}

/*
部门表
*/
type SysDept struct {
	//	SysDeptBase
	//	ModelBase

	/*  部门id  */
	DeptId int64 `gorm:"column:dept_id;type:bigint(20);PRIMARY_KEY;comment:'部门id'" json:"dept_id,string"`
	/*  父部门id  */
	ParentId int64 `gorm:"column:parent_id;type:bigint(20);comment:'父部门id';default:0" json:"parent_id,string"`
	/*  祖级列表  */
	Ancestors string `gorm:"column:ancestors;type:varchar(50);comment:'祖级列表'" json:"ancestors"`
	/*  部门名称  */
	DeptName string `gorm:"column:dept_name;type:varchar(30);comment:'部门名称'" json:"dept_name"`
	/*  显示顺序  */
	OrderNum int32 `gorm:"column:order_num;type:int(4);comment:'显示顺序';default:0" json:"order_nm"`
	/*  负责人  */
	Leader string `gorm:"column:leader;type:varchar(20);comment:'负责人'" json:"leader"`
	/*  联系电话  */
	Phone string `gorm:"column:phone;type:varchar(11);comment:'联系电话'" json:"phone"`
	/*  邮箱  */
	Email string `gorm:"column:email;type:varchar(50);comment:'邮箱'" json:"email"`
	/*  部门状态（0正常 1停用）  */
	Status string `gorm:"column:status;type:char(1);comment:'部门状态（0正常 1停用）';default:\'0\'" json:"status"`
	/*  删除标志（0代表存在 2代表删除）  */
	DelFlag string `gorm:"column:del_flag;type:char(1);comment:'删除标志（0代表存在 2代表删除）';default:\'0\'" json:"del_flag"`
	/*  创建者  */
	CreateBy string `gorm:"column:create_by;type:varchar(64);comment:'创建者'" json:"create_by"`
	/*  创建时间  */
	CreateTime time.Time `gorm:"column:create_time;type:datetime;comment:'创建时间'" json:"create_time"`
	/*  更新者  */
	UpdateBy string `gorm:"column:update_by;type:varchar(64);comment:'更新者'" json:"update_by"`
	/*  更新时间  */
	UpdateTime time.Time `gorm:"column:update_time;type:datetime;comment:'更新时间'" json:"-"`
}
type SysDeptVo struct {

	/*  部门id  */
	DeptId int64 `gorm:"column:dept_id;type:bigint(20);PRIMARY_KEY;comment:'部门id'" json:"dept_id,string"`
	/*  父部门id  */
	ParentId int64 `gorm:"column:parent_id;type:bigint(20);comment:'父部门id';default:0" json:"parent_id,string"`
	/*  祖级列表  */
	Ancestors string `gorm:"column:ancestors;type:varchar(50);comment:'祖级列表'" json:"ancestors"`
	/*  部门名称  */
	DeptName string `gorm:"column:dept_name;type:varchar(30);comment:'部门名称'" json:"deptName"`
	/*  显示顺序  */
	OrderNum int32 `gorm:"column:order_num;type:int(4);comment:'显示顺序';default:0" json:"orderNum"`
	/*  负责人  */
	Leader string `gorm:"column:leader;type:varchar(20);comment:'负责人'" json:"leader"`
	/*  联系电话  */
	Phone string `gorm:"column:phone;type:varchar(11);comment:'联系电话'" json:"phone"`
	/*  邮箱  */
	Email string `gorm:"column:email;type:varchar(50);comment:'邮箱'" json:"email"`
	/*  部门状态（0正常 1停用）  */
	Status string `gorm:"column:status;type:char(1);comment:'部门状态（0正常 1停用）';default:\'0\'" json:"status"`
	/*  删除标志（0代表存在 2代表删除）  */
	DelFlag string `gorm:"column:del_flag;type:char(1);comment:'删除标志（0代表存在 2代表删除）';default:\'0\'" json:"delFlag"`
	/*  创建者  */
	CreateBy string `gorm:"column:create_by;type:varchar(64);comment:'创建者'" json:"createBy"`
	/*  创建时间  */
	CreateTime basemodel.LocalTime `gorm:"column:create_time;type:datetime;comment:'创建时间'" json:"createTime"`
	/*  更新者  */
	UpdateBy string `gorm:"column:update_by;type:varchar(64);comment:'更新者'" json:"updateBy"`
	/*  更新时间  */
	UpdateTime basemodel.LocalTime `gorm:"column:update_time;type:datetime;comment:'更新时间'" json:"updateTime"`
}

/*
gorm默认生成的表名是结构名+'s',所以必须以结构方法指定！
*/
func (entity *SysDept) TableName() string {

	return "sys_dept"
}

/*
迁移
*/
func (entity *SysDept) AutoMigrate(db *gorm.DB) error {
	err := db.AutoMigrate(entity).Error
	// entity.execComment(dbmenu)

	return err
}

/*
exec pg comment sql
*/
func (entity *SysDept) execComment(db *gorm.DB) {

	sql := "comment on table sys_dept is '部门表';\n\t"
	sql = sql + ``
	db.Exec(sql)

}

/*
指定生成结果转json字符串
*/
func (entity *SysDept) String() string {
	s, err := json.Marshal(entity)
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *SysDept) ToString() string {
	s, err := json.MarshalIndent(entity, "", " ")
	if err != nil {
		fmt.Println(err.Error())
		return "{}"
	}
	return string(s)

}

func (entity *SysDept) Unmarshal(body string) error {
	return json.Unmarshal([]byte(body), entity)

}

func (entity *SysDept) UnmarshalBy(body []byte) error {
	return jsoniter.Unmarshal(body, entity)

}

/*
iniPk bool:是否初始化主键DeptId
初始化指针
*/
func (entity *SysDept) Ini(iniPk bool) *SysDept {
	if iniPk {
		entity.DeptId = 0 //new(int64)
	}

	//entity.ParentId = new(int64)

	//entity.Ancestors = new(string)

	//entity.DeptName = new(string)

	//entity.OrderNum = new(int32)

	//entity.Leader = new(string)

	//entity.Phone = new(string)

	//entity.Email = new(string)

	//entity.Status = new(string)

	//entity.DelFlag = new(string)

	//entity.CreateBy = new(string)

	//entity.CreateTime = new(basemodel.LocalTime)

	//entity.UpdateBy = new(string)

	//entity.UpdateTime = new(basemodel.LocalTime)

	return entity

}

/*
iniPk bool:是否初始化主键DeptId
初始化指针
*/
func (entity *SysDept) IniNil(iniPk bool) *SysDept {
	if iniPk {

		entity.DeptId = 0 //new(int64)

	}

	//if entity.ParentId == nil {
	//entity.ParentId = new(int64)
	//}

	//if entity.Ancestors == nil {
	//entity.Ancestors = new(string)
	//}

	//if entity.DeptName == nil {
	//entity.DeptName = new(string)
	//}

	//if entity.OrderNum == nil {
	//entity.OrderNum = new(int32)
	//}

	//if entity.Leader == nil {
	//entity.Leader = new(string)
	//}

	//if entity.Phone == nil {
	//entity.Phone = new(string)
	//}

	//if entity.Email == nil {
	//entity.Email = new(string)
	//}

	//if entity.Status == nil {
	//entity.Status = new(string)
	//}

	//if entity.DelFlag == nil {
	//entity.DelFlag = new(string)
	//}

	//if entity.CreateBy == nil {
	//entity.CreateBy = new(string)
	//}

	//if entity.CreateTime == nil {
	//entity.CreateTime = new(basemodel.LocalTime)
	//}

	//if entity.UpdateBy == nil {
	//entity.UpdateBy = new(string)
	//}

	//if entity.UpdateTime == nil {
	//entity.UpdateTime = new(basemodel.LocalTime)
	//}

	return entity

}

func (entity *SysDept) copy(iniPk bool, cpentity *SysDept) *SysDept {

	entity.Ini(iniPk)
	cpentity.IniNil(iniPk)

	if iniPk {
		entity.DeptId = cpentity.DeptId
	}

	//*entity.ID =  *cpentity.ID

	entity.ParentId = cpentity.ParentId
	entity.Ancestors = cpentity.Ancestors
	entity.DeptName = cpentity.DeptName
	entity.OrderNum = cpentity.OrderNum
	entity.Leader = cpentity.Leader
	entity.Phone = cpentity.Phone
	entity.Email = cpentity.Email
	entity.Status = cpentity.Status
	entity.DelFlag = cpentity.DelFlag
	entity.CreateBy = cpentity.CreateBy
	entity.CreateTime = cpentity.CreateTime
	entity.UpdateBy = cpentity.UpdateBy
	entity.UpdateTime = cpentity.UpdateTime
	return entity
}

func (entity *SysDept) copyNotNil(iniPk bool, cpentity *SysDept) *SysDept {
	entity.IniNil(iniPk)
	if iniPk {
		entity.DeptId = cpentity.DeptId
	}

	//*entity.ID = *cpentity.ID

	entity.ParentId = cpentity.ParentId
	entity.Ancestors = cpentity.Ancestors
	entity.DeptName = cpentity.DeptName
	entity.OrderNum = cpentity.OrderNum
	entity.Leader = cpentity.Leader
	entity.Phone = cpentity.Phone
	entity.Email = cpentity.Email
	entity.Status = cpentity.Status
	entity.DelFlag = cpentity.DelFlag
	entity.CreateBy = cpentity.CreateBy
	entity.CreateTime = cpentity.CreateTime
	entity.UpdateBy = cpentity.UpdateBy
	entity.UpdateTime = cpentity.UpdateTime
	return entity
}

/*
func (entity *SysDept ) Model2PbMsg (pbentity *proto.SysDeptProto) * proto.SysDeptProto{

    entity.IniNil(true)


		pbentity.DeptId =  utils.ToStr(entity.GetDeptId())
		pbentity.ParentId =  utils.ToStr(entity.GetParentId())
		pbentity.Ancestors =  entity.GetAncestors()
		pbentity.DeptName =  entity.GetDeptName()
		pbentity.OrderNum =  entity.GetOrderNum()
		pbentity.Leader =  entity.GetLeader()
		pbentity.Phone =  entity.GetPhone()
		pbentity.Email =  entity.GetEmail()
		pbentity.Status =  entity.GetStatus()
		pbentity.DelFlag =  entity.GetDelFlag()
		pbentity.CreateBy =  entity.GetCreateBy()
		pbentity.CreateTime =  entity.GetCreateTime()
		pbentity.UpdateBy =  entity.GetUpdateBy()
		pbentity.UpdateTime =  entity.GetUpdateTime()

    return pbentity
}

func (entity *SysDept ) PbMsg2Model (pbentity *proto.SysDeptProto) *SysDept{
    entity.IniNil(true)


		 * entity.DeptId =  utils.Str2Int64(pbentity.GetDeptId())
		 * entity.ParentId =  utils.Str2Int64(pbentity.GetParentId())
		 * entity.Ancestors =  pbentity.GetAncestors()
		 * entity.DeptName =  pbentity.GetDeptName()
		 * entity.OrderNum =  pbentity.GetOrderNum()
		 * entity.Leader =  pbentity.GetLeader()
		 * entity.Phone =  pbentity.GetPhone()
		 * entity.Email =  pbentity.GetEmail()
		 * entity.Status =  pbentity.GetStatus()
		 * entity.DelFlag =  pbentity.GetDelFlag()
		 * entity.CreateBy =  pbentity.GetCreateBy()
		 * entity.CreateTime =  pbentity.GetCreateTime()
		 * entity.UpdateBy =  pbentity.GetUpdateBy()
		 * entity.UpdateTime =  pbentity.GetUpdateTime()
    return entity
}


func (entity *SysDept) IniPbMsg() (pbentity *proto.<no value>Proto) {

	pbentity = &proto.<no value>Proto{}


		pbentity.DeptId =  "0"
		pbentity.ParentId =  "0"
		pbentity.Ancestors =  ""
		pbentity.DeptName =  ""
		pbentity.OrderNum =  0
		pbentity.Leader =  ""
		pbentity.Phone =  ""
		pbentity.Email =  ""
		pbentity.Status =  ""
		pbentity.DelFlag =  ""
		pbentity.CreateBy =  ""
		pbentity.CreateTime =  0
		pbentity.UpdateBy =  ""
		pbentity.UpdateTime =  0
	return
}

*/

func (entity *SysDept) GetDeptId() int64 {

	return entity.DeptId

}

func (entity *SysDept) SetDeptId(DeptId int64) {
	entity.DeptId = DeptId
}

func (entity *SysDept) GetParentId() int64 {

	return entity.ParentId

}

func (entity *SysDept) SetParentId(ParentId int64) {
	entity.ParentId = ParentId
}

func (entity *SysDept) GetAncestors() string {

	return entity.Ancestors

}

func (entity *SysDept) SetAncestors(Ancestors string) {
	entity.Ancestors = Ancestors
}

func (entity *SysDept) GetDeptName() string {

	return entity.DeptName

}

func (entity *SysDept) SetDeptName(DeptName string) {
	entity.DeptName = DeptName
}

func (entity *SysDept) GetOrderNum() int32 {

	return entity.OrderNum

}

func (entity *SysDept) SetOrderNum(OrderNum int32) {
	entity.OrderNum = OrderNum
}

func (entity *SysDept) GetLeader() string {

	return entity.Leader

}

func (entity *SysDept) SetLeader(Leader string) {
	entity.Leader = Leader
}

func (entity *SysDept) GetPhone() string {

	return entity.Phone

}

func (entity *SysDept) SetPhone(Phone string) {
	entity.Phone = Phone
}

func (entity *SysDept) GetEmail() string {

	return entity.Email

}

func (entity *SysDept) SetEmail(Email string) {
	entity.Email = Email
}

func (entity *SysDept) GetStatus() string {

	return entity.Status

}

func (entity *SysDept) SetStatus(Status string) {
	entity.Status = Status
}

func (entity *SysDept) GetDelFlag() string {

	return entity.DelFlag

}

func (entity *SysDept) SetDelFlag(DelFlag string) {
	entity.DelFlag = DelFlag
}

func (entity *SysDept) GetCreateBy() string {

	return entity.CreateBy

}

func (entity *SysDept) SetCreateBy(CreateBy string) {
	entity.CreateBy = CreateBy
}

func (entity *SysDept) GetCreateTime() basemodel.LocalTime {

	return basemodel.LocalTime{} //entity.CreateTime

}

func (entity *SysDept) SetCreateTime(CreateTime basemodel.LocalTime) {
	//entity.CreateTime = basemodel.LocalTime{}
}

func (entity *SysDept) GetUpdateBy() string {

	return entity.UpdateBy

}

func (entity *SysDept) SetUpdateBy(UpdateBy string) {
	entity.UpdateBy = UpdateBy
}

func (entity *SysDept) GetUpdateTime() time.Time {

	return entity.UpdateTime

}

func (entity *SysDept) SetUpdateTime(UpdateTime time.Time) {
	entity.UpdateTime = UpdateTime
}
