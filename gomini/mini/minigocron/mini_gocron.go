package main

import (
	"fmt"
	"github.com/go-co-op/gocron"
	"time"
)

func Do() {
	s := gocron.NewScheduler(time.UTC)
	job, _ := s.Every(1).Second().Do(jobFunc)
	go func() {
		for {
			fmt.Println("Count of finished job runs", job.FinishedRunCount())
			time.Sleep(time.Second)
		}
	}()
	s.StartAsync()
}
func main() {
	Do()
	for true {

	}
}
func jobFunc() {
	fmt.Println("任务执行啦！")
}
