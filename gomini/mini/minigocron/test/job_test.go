package test

import (
	"github.com/go-co-op/gocron"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTags(t *testing.T) {
	j, _ := gocron.NewScheduler(time.UTC).Every(1).Minute().Do(task)
	j.Tag("some")
	j.Tag("tag")
	j.Tag("more")
	j.Tag("tags")

	assert.ElementsMatch(t, j.Tags(), []string{"tags", "tag", "more", "some"})

	j.Untag("more")
	assert.ElementsMatch(t, j.Tags(), []string{"tags", "tag", "some"})
}

func TestHasTags(t *testing.T) {
	tests := []struct {
		name      string
		jobTags   []string
		matchTags []string
		expected  bool
	}{
		{
			"OneTagMatch",
			[]string{"tag1"},
			[]string{"tag1"},
			true,
		},
		{
			"OneTagNoMatch",
			[]string{"tag1"},
			[]string{"tag2"},
			false,
		},
		{
			"DuplicateJobTagsMatch",
			[]string{"tag1", "tag1"},
			[]string{"tag1"},
			true,
		},
		{
			"DuplicateInputTagsMatch",
			[]string{"tag1"},
			[]string{"tag1", "tag1"},
			true,
		},
		{
			"MultipleTagsMatch",
			[]string{"tag1", "tag2"},
			[]string{"tag2", "tag1"},
			true,
		},
		{
			"MultipleTagsNoMatch",
			[]string{"tag1", "tag2"},
			[]string{"tag2", "tag1", "tag3"},
			false,
		},
		{
			"MultipleDuplicateTagsMatch",
			[]string{"tag1", "tag1", "tag1", "tag2"},
			[]string{"tag1", "tag2"},
			true,
		},
		{
			"MultipleDuplicateTagsNoMatch",
			[]string{"tag1", "tag1", "tag1"},
			[]string{"tag1", "tag1", "tag3"},
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j, _ := gocron.NewScheduler(time.UTC).Every(1).Minute().Do(task)
			j.Tag(tt.jobTags...)
			//assert.Equal(t, tt.expected, j.hasTags(tt.matchTags...))
		})
	}
}

func TestJob_IsRunning(t *testing.T) {
	s := gocron.NewScheduler(time.UTC)
	j, err := s.Every(10).Seconds().Do(func() { time.Sleep(2 * time.Second) })
	require.NoError(t, err)
	assert.False(t, j.IsRunning())

	s.StartAsync()

	time.Sleep(time.Second)
	assert.True(t, j.IsRunning())

	time.Sleep(time.Second)
	s.Stop()

	assert.False(t, j.IsRunning())
}
