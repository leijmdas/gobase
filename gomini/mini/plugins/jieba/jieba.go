package jieba

import (
	"errors"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"github.com/micro/go-micro/v2/logger"
	"github.com/wangbin/jiebago"

	"io"
	"net/http"
	"os"
)

var seg jiebago.Segmenter

func init() {
	dictPath := fileutils.FindRootDir() + "/dict.txt"
	if !fileutils.CheckFileExist(dictPath) {

		err := downloadDictionary("https://cloud.ichub.com/cdn/dict.txt", dictPath)
		if err != nil {
			logger.Errorf("下载分词词库失败：", err.Error())
			return
		}
	}

	if err := seg.LoadDictionary(dictPath); err != nil {
		logger.Errorf("加载分词词库失败：", err.Error())
	}
}

func downloadDictionary(url, targetPath string) error {
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("下载词库失败: %w", err)
	}
	defer resp.Body.Close()

	out, err := os.Create(targetPath)
	if err != nil {
		return fmt.Errorf("创建本地文件失败: %w", err)
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return fmt.Errorf("复制内容到本地文件失败: %w", err)
	}

	return nil
}

//func initJieBa() {
//	m.Lock()
//	defer m.Unlock()
//	var err error
//	if inited {
//		err = fmt.Errorf("[initJieBa] db 已经初始化过")
//		logger.Infof(err.Error())
//		return
//	}
//	seg.LoadDictionary("dict.txt")
//}

// 使用结巴分词
// 【全模式】： 我 / 来到 / 北京 / 清华 / 清华大学 / 华大 / 大学 /
// 【精确模式】： 我 / 来到 / 北京 / 清华大学 /
// 【新词识别】： 他 / 来到 / 了 / 网易 / 杭研 / 大厦 /
// 【搜索引擎模式】： 小明 / 硕士 / 毕业 / 于 / 中国 / 科学 / 学院 / 科学院 / 中国科学院 / 计算 / 计算所 / ， / 后 / 在 / 日本 / 京都 / 大学 / 日本京都大学 / 深造 /
func Analyze(text string, mode int64) ([]string, error) {
	if len(text) == 0 {
		return nil, errors.New("text is empty")
	}
	switch mode {
	case 1: //全模式
		return chanToArray(seg.CutAll(text)), nil
	case 2: //新词识别
		return chanToArray(seg.Cut(text, true)), nil
	case 3: //搜索引擎模式
		return chanToArray(seg.CutForSearch(text, false)), nil
	default: //精确模式
		return chanToArray(seg.Cut(text, false)), nil
	}
}

func chanToArray(ch <-chan string) []string {
	var result []string
	for word := range ch {
		if word != "" {
			result = append(result, word)
		}
	}
	return result
}
