package jieba

import (
	"reflect"
	"testing"
)

func TestAnalyze(t *testing.T) {

	type args struct {
		text string
		mode int64
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "test1",
			args: args{
				text: "在遥远的山谷里，住着一群快乐的小动物，它们每晚都会聚集在村口的大树下，仰望那轮明亮的习亮，讲述着各自的故事。其中，有一只名叫灵灵的小狐狸，它对月亮有着特别的情感",
				mode: 0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Analyze(tt.args.text, tt.args.mode)
			if (err != nil) != tt.wantErr {
				t.Errorf("Analyze() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Analyze() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_chanToArray(t *testing.T) {
	type args struct {
		ch <-chan string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := chanToArray(tt.args.ch); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("chanToArray() = %v, want %v", got, tt.want)
			}
		})
	}
}
