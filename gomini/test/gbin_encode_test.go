package test

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/gogf/gf/v2/encoding/gbinary"
	"github.com/gogf/gf/v2/os/glog"
	"github.com/sirupsen/logrus"
	"testing"
)

type user struct {
	id   int
	name string
}

func init() {
	fileutils.FindRootDir()
}
func Test001_encode(t *testing.T) {
	var u = &user{}
	u.id = 111
	u.name = "elijmdas"
	gbinary.Encode(u.id)

	if buffer := gbinary.Encode(18, 300, 1.01); buffer != nil {
		glog.Error(context.Background(), buffer)
	} else {
		fmt.Println(buffer)
	}
}
func Test002_encodeStru(t *testing.T) {
	var cfg = ichubconfig.Default()
	cfg.Read()

	var mysql = cfg.ReadIchubMysql()
	var buf bytes.Buffer = bytes.Buffer{}
	mysql.Dbname = "hrms"
	buf.Write(gbinary.Encode(mysql.Dbname))
	buf.Write(gbinary.Encode(mysql.Dbtype))
	var m = baseconfig.NewDbClientDto()

	logrus.Info(buf.String())
	gbinary.Decode(buf.Bytes(), &m.Dbname)

	//buf.Bytes()
}
