package test

import (
	"context"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseconfig"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/sirupsen/logrus"
	"testing"
)

func Test001_httpclient(t *testing.T) {
	// 配置连接池参数

	//gcfg.SetConfig("client.http", g.Map{
	//	"maxIdleConns":        100, // 最大空闲连接数
	//	"maxIdleConnsPerHost": 10,  // 每个主机的最大空闲连接数
	//	"idleConnTimeout":     90,  // 空闲连接超时时间（秒）
	//	"timeout":             30,  // 请求超时时间（秒）
	//})

	// 创建 HTTP 客户端实例
	client := g.Client()

	// 发起请求
	response, err := client.Get(context.Background(), "http://huawei.akunlong.top:1002")
	if err != nil {
		fmt.Println("请求错误:", err)
		return
	}
	defer response.Close()

	// 打印响应内容
	fmt.Println(string(response.ReadAll()))
}

type MyTest struct {
	basedto.BaseEntity
	D baseconfig.DbClientDto `json:",inline"`
	G baseconfig.GatewayDto  `json:",inline"`
}
type MyTest1 struct {
	basedto.BaseEntity
	D baseconfig.DbClientDto
	G baseconfig.GatewayDto
}

func NewMyTest1() *MyTest1 {
	return &MyTest1{}
}
func NewMyTest() *MyTest {
	return &MyTest{}
}

func Test0021_struinline(t *testing.T) {
	logrus.Info(jsonutils.ToJsonPretty(NewMyTest()))
	logrus.Info(jsonutils.ToJsonPretty(NewMyTest1()))

}
