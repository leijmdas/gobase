package test

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/basedto"
	"gitee.com/leijmdas/gobase/goconfig/common/base/baseiface"
	"gitee.com/leijmdas/gobase/goconfig/common/base/fileutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"gitee.com/leijmdas/gobase/goconfig/common/base/jsonutils"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	"github.com/gogf/gf/container/gvar"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/os/gsession"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"
	"time"
)

func Test001_glistmap(t *testing.T) {

	// 创建一个glistmap实例
	m := gmap.NewListMap()

	// 按照一定顺序添加元素
	m.Set("key1", "value1")
	m.Set("key2", "value2")
	m.Set("key3", "value3")

	// 按照添加的顺序迭代输出
	m.Iterator(func(k interface{}, v interface{}) bool {
		fmt.Printf("\r\n%v:%v ", k, v)
		return true
	})

	goutils.Info(jsonutils.ToJsonPretty(m.Keys()))
	goutils.Info(jsonutils.ToJsonPretty(m.Values()))
}
func Test002_gfile(t *testing.T) {
	logrus.Info(gfile.Dir("/dd/dd/f.go"))
	logrus.Info(gfile.Abs("/dd/dd/f.go"))
	logrus.Info(gfile.Basename("/dd/dd/f.go"))

	logrus.Info(gfile.Exists(fileutils.FindRootDir() + "/config/app.yml"))

}
func Test003_gvar(t *testing.T) {
	var v = gvar.New(100.1)
	logrus.Info(v.Int64())
}

func Test004_checkInterface(t *testing.T) {
	var myVar *basedto.IchubResult

	// 获取类型信息
	rt := reflect.TypeOf(myVar)

	// 检查是否实现了 MyInterface 接口
	if rt.Implements(reflect.TypeOf((*baseiface.IbaseProxy)(nil)).Elem()) {
		fmt.Println("Type implements ibaseproxy")
	} else {
		fmt.Println("Type does not implement ibaseproxy")
	}

}
func IfIface[T any](inst any) bool {

	instType := reflect.TypeOf(inst)
	var ifaceType T
	return instType.Implements(reflect.TypeOf(ifaceType).Elem())

}
func Test005_checkInterface(t *testing.T) {
	var b = IfIface[*baseiface.IbaseProxy](basedto.ResultSuccessData(""))
	goutils.Info("\r\niface ibaseproxy is", b)

}
func Test006_gtime(t *testing.T) {
	logrus.Info(gtime.Date())
	logrus.Info(gsession.NewSessionId())

}
func Test007_gdi(t *testing.T) {
	logrus.Info(gtime.Date())
	logrus.Info(gsession.NewSessionId())

}
func Test008_gsession_file(t *testing.T) {
	//var s = gsession.NewSessionId()
	var cfg = ichubconfig.Default()
	cfg.Read()
	storage := gsession.NewStorageFile("", time.Second)
	manager1 := gsession.New(time.Second, storage)
	s1 := manager1.New(gctx.New())
	fmt.Println(s1.Set("key", cfg.ReadIchubWebClient()) == nil)
	fmt.Println(s1.Get("key", "val"))

}
func Test009_gsession_memory(t *testing.T) {

	var cfg = ichubconfig.Default()
	cfg.Read()
	storage := gsession.NewStorageMemory()
	manager1 := gsession.New(time.Second, storage)
	s1 := manager1.New(gctx.New())
	fmt.Println(s1.Set("key", cfg.ReadIchubWebClient()) == nil)
	fmt.Println(s1.Get("key", "val"))

}

var redisConfig = &gredis.Config{
	Address: "huawei.akunlong.top:6379",
	Db:      9,
	Pass:    "123456",
}

func Test010_gsession_redis(t *testing.T) {
	redis, err1 := gredis.New(redisConfig)
	if err1 != nil {
		logrus.Error(err1)
	}
	var cfg = ichubconfig.Default()
	cfg.Read()
	storage := gsession.NewStorageRedis(redis)
	manager := gsession.New(time.Second, storage)
	s := manager.New(gctx.New())
	fmt.Println(s.Set("key", cfg.ReadIchubRedis()) == nil)
	fmt.Println(s.Get("key", "val"))
	var v, err = s.Get("key")
	if err != nil {
		logrus.Error(err)
		return
	}
	logrus.Info(v)
}

func Test011_gsession_redisofGOcfg(t *testing.T) {

	var cfg = ichubconfig.Default()
	cfg.Read()
	var grediscli = gocontext.FindBeanGoClientFactroy().IniGredisClient().Gredis()

	storage := gsession.NewStorageRedis(grediscli)
	manager := gsession.New(24*time.Hour, storage)
	s := manager.New(gctx.New())
	fmt.Println(s.Set("key", cfg.ReadIchubRedis()) == nil)
	fmt.Println(s.Get("key", "val"))
	var v, err = s.Get("key")
	if err != nil {
		logrus.Error(err)
		return
	}
	logrus.Info(v)
}

func Test012_gsession_ofGOcfg(t *testing.T) {

}
