package test

import (
	"context"
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/gocontext"
	"gitee.com/leijmdas/gobase/goconfig/common/ichubconfig"
	_ "github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/sirupsen/logrus"

	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"testing"
)

var (
	config = gredis.Config{
		Address: "huawei.akunlong.top:6379",
		Db:      1,
		Pass:    "123456",
	}
	group = "cache"
	ctx   = gctx.New()
)

func Test001_cfg(t *testing.T) {
	var ctx = gctx.New()
	fmt.Println(g.Cfg().Get(ctx, "viewpath"))
	fmt.Println(g.Cfg().Get(ctx, "database.default.0.role"))
}

var cfg *ichubconfig.IchubConfig

func Test002_gredis(t *testing.T) {
	gredis.SetConfig(&config, group)

	var ctx = gctx.New()
	cfg = ichubconfig.Default()
	cfg.Read()
	_, err := g.Redis(group).Set(ctx, "key", cfg.ReadIchubWebClient())
	if err != nil {
		g.Log().Fatal(ctx, err)
	}
	value, err := g.Redis(group).Get(ctx, "key")
	if err != nil {
		g.Log().Fatal(ctx, err)
	}
	fmt.Println("value=", value.String())

}
func Test003_gredis_cfg(t *testing.T) {
	//gredis.SetConfig(&config, group)
	var redisConfig = &gredis.Config{
		Address: "huawei.akunlong.top:6379",
		Db:      9,
		Pass:    "123456",
	}
	//var ctx = gctx.New()
	redis, err1 := gredis.New(redisConfig)
	if err1 != nil {
		panic(err1)
	}
	cfg = ichubconfig.Default()
	cfg.Read()
	_, err := redis.Set(ctx, "key", cfg.ReadIchubWebClient())
	if err != nil {
		g.Log().Fatal(ctx, err)
	}
	value, err := redis.Get(ctx, "key")
	if err != nil {
		g.Log().Fatal(ctx, err)
	}
	fmt.Println("value=", value.String())

}
func Test003_gredis(t *testing.T) {
	var grediscli = gocontext.FindBeanGoClientFactroy().IniGredisClient().Gredis()
	//.SetConfig(&config, group)

	var r, e = grediscli.Set(context.Background(), "key", "1112")

	logrus.Info(r, e)
	var ret, _ = grediscli.Get(context.Background(), "key")
	logrus.Info(ret)

}
