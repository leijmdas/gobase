package test

//https://github.com/bytedance/gopkg/blob/develop/util/gopool/pool_test.go
import (
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	"github.com/bytedance/gopkg/util/gopool"
	"sync"
	"sync/atomic"
	"testing"
)

// https://gitee.com/gqlengine/gqlengine
func TestPool(t *testing.T) {
	p := gopool.NewPool("test", 100, gopool.NewConfig())
	var n int32
	var wg sync.WaitGroup
	for i := 0; i < 2000; i++ {
		wg.Add(1)
		p.Go(func() {
			defer wg.Done()
			atomic.AddInt32(&n, 1)
		})
	}
	wg.Wait()
	if n != 2000 {
		t.Error(n)
	}
	goutils.Info(n)
}
