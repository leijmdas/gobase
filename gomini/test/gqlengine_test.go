package test

//https://gitee.com/gqlengine/gqlengine

import (
	"github.com/gorilla/mux"
	"github.com/gqlengine/playground"
	"net/http"
	"testing"

	"github.com/gqlengine/gqlengine"
)

// MyInfo 定义了业务的数据结构
type MyInfo struct {
	gqlengine.IsGraphQLObject `gqlDesc:"my info"` // gqlDesc用于生成描述信息

	Unit          int
	Name          string
	MyStringField string // 定义一个字段，gqlengine会根据golang的基本类型自动匹配到graphql类型
	MyIntField    int    `gqlRequired:"true"` // gqlRequired用于标记该字段是必备非空字段
}

func NewMyInfo() *MyInfo {
	return &MyInfo{}
}

type HumanHeightParams struct {
	gqlengine.IsGraphQLArguments
	Name string `json:"name" gqlDesc:"human name" gqlRequired:"true"`
	Unit int    `json:"unit" gqlDesc:"height unit" gqlDefault:"1" gqlRequired:"true"`
}

// MySimpleQuery 是业务的代码的具体实现
func MySimpleQuery(params *HumanHeightParams) (*MyInfo, error) {
	var r = NewMyInfo()
	r.Unit = params.Unit
	r.Name = params.Name
	//panic("not implemented")
	return r, nil

}
func MyQuery(params *HumanHeightParams) (*MyInfo, error) {
	var r = NewMyInfo()
	r.Unit = params.Unit
	r.Name = params.Name
	//panic("not implemented")
	return r, nil

}

//func Test001_GqlEngine(t *testing.T) {
//	gqlengine := gqlengine.NewEngine(gqlengine.Options{
//		Tracing: true, // 使能GraphQL调用链路分析功能
//	})
//
//	gqlengine.NewQuery(MySimpleQuery) // 务必注册你的接口！！！
//
//	// 初始化engine
//	if err := gqlengine.Init(); err != nil {
//		panic(err)
//	}
//
//	println("open playground http://localhost:8000/api/graphql/")
//	// serve for HTTP
//	http.HandleFunc("/api/graphql", gqlengine.ServeHTTP)
//	if err := http.ListenAndServe(":8000", nil); err != nil {
//		panic(err)
//	}
//}

func Test002_Gql(t *testing.T) {
	engine := gqlengine.NewEngine(gqlengine.Options{
		Tracing: true, // 使能GraphQL调用链路分析功能
	})

	engine.NewQuery(MySimpleQuery) // 务必注册你的接口！！！
	engine.NewQuery(MyQuery)       // 务必注册你的接口！！！

	// 初始化engine
	if err := engine.Init(); err != nil {
		panic(err)
	}
	// init your gql gqlengine
	http.HandleFunc("/api/graphql", engine.ServeHTTP)

	// 为playground配置GraphQL端点（起码让playground知道该去哪里获得graphql数据吧;)）
	playground.SetEndpoints("/api/graphql", "/api/graphql/subscriptions")

	// recommends to use 'gorilla/mux' to serve the playground web assets
	r := mux.NewRouter()
	r.HandleFunc("/api/graphql", engine.ServeHTTP)
	r.HandleFunc("/api/graphql/subscriptions", engine.ServeWebsocket)
	r.PathPrefix("/api/graphql/playground").
		Handler(http.StripPrefix("/api/graphql/playground",
			http.FileServer(playground.WebBundle)))

	println("open playground http://localhost:9996/api/graphql/playground/")
	if err := http.ListenAndServe(":9996", r); err != nil {
		panic(err)
	}
}
