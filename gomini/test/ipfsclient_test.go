package test

import (
	"fmt"
	"gitee.com/leijmdas/gobase/goconfig/common/base/goutils"
	shell "github.com/ipfs/go-ipfs-api"
	"os"
	"strings"
	"testing"
)

func Test001_AddIpfsClient(t *testing.T) {
	// 连接到本地运行的IPFS节点
	sh := shell.NewShell("huawei.akunlong.top:2501")
	cid, err := sh.Add(strings.NewReader("你好，世界！"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "错误：%s\n", err)
		os.Exit(1)
	}
	fmt.Printf("已添加：%s\n", cid)
}
func Test002_ListIpfsClient(t *testing.T) {
	// 连接到本地运行的IPFS节点
	sh := shell.NewShell("huawei.akunlong.top:2501")
	cid, err := sh.List("")
	if err != nil {
		fmt.Fprintf(os.Stderr, "错误：%s\n", err)
		os.Exit(1)
	}
	goutils.Info(cid)
}
